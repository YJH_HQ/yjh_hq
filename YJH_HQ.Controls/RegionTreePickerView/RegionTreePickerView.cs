﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.Controls.RegionTreePickerView
{
    public partial class RegionTreePickerView : UserControl, YJH_HQ.Controls.EntityPicker.IEntityPickerView
    {
        public event Action OnSubmited;
        public Telerik.WinControls.UI.RadTreeNode selectedNode;

        public RegionTreePickerView()
        {
            InitializeComponent();

            //this.dgList.QueryMethod = "LMS.TradeService.Query";
            //this.dgList.GetQueryArgsFunc = () => { return new object[] { this.qpName.Text }; };
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.Load += (s, e) => { this.LoadData(); };
            this.RegionTree.SelectedNodeChanged += RegionTree_SelectedNodeChanged;
        }

        void RegionTree_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            selectedNode = e.Node;
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                if (selectedNode == null)
                {
                    RadMessageBox.Show("请选择城市或直辖市！", "提示");
                    return null;
                }
                if ((YJH.Enums.RegionScope)selectedNode.Tag != YJH.Enums.RegionScope.城市 && (YJH.Enums.RegionScope)selectedNode.Tag != YJH.Enums.RegionScope.直辖市)
                {
                    RadMessageBox.Show("请选择城市或直辖市！", "提示");
                    return null;
                }

                var id = new Guid(this.RegionTree.SelectedNode.Name);
                if (id == Guid.Empty)
                    return null;
                return new YJH.Entities.Region(dps.Common.Data.Entity.Retrieve(YJH.Entities.Region.EntityModelID, id));
            }
        }

        private void GetRegionTree()
        {
            dps.Data.Mapper.EntityList<YJH.Entities.Region> results = null;
            try
            {
                results = YJH.Services.RegionService.GetRegionTreeList();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("生成树出错！错误原因：" + ex.Message, "提示");
                return;
            }
            foreach (var region in results)
            {
                Telerik.WinControls.UI.RadTreeNode rootNode = new Telerik.WinControls.UI.RadTreeNode();
                rootNode.Text = region.Name;
                rootNode.Value = region.ParentID;
                rootNode.Tag = region.Scope;
                rootNode.Name = region.Instance.ID.ToString();
                this.RegionTree.Nodes.Add(rootNode);
                if (region != null && region.SubRegions.Count > 0)
                {
                    RecuriseNodes(region.SubRegions, rootNode);
                }
                rootNode.Expand();
            }
        }

        private void RecuriseNodes(dps.Data.Mapper.EntityList<YJH.Entities.Region> regions, Telerik.WinControls.UI.RadTreeNode parentNode)
        {
            foreach (var region in regions)
            {
                Telerik.WinControls.UI.RadTreeNode childRootNode = new Telerik.WinControls.UI.RadTreeNode();
                childRootNode.Text = region.Name;
                childRootNode.Value = region.ParentID;
                childRootNode.Tag = region.Scope;
                childRootNode.Name = region.Instance.ID.ToString();
                parentNode.Nodes.Add(childRootNode);
                if (region != null && region.SubRegions.Count > 0)
                {
                    RecuriseNodes(region.SubRegions, childRootNode);
                }
                if (region.Scope == YJH.Enums.RegionScope.国家)
                {
                    childRootNode.Expand();
                }
            }
        }

        public void LoadData()
        {
            //this.dgList.LoadData();
            this.GetRegionTree();

        }

        private void Search()
        {
            var s = this.RegionTree.Find(tn => tn.Text.IndexOf(this.qpName.Text) != -1);
            if (s != null)
            {
                this.RegionTree.SelectedNode = s;
            }
        }

        public void FocusInput()
        {
            //this.qpName.Focus();
        }

        public void MoveUp()
        {
            //LMS.UI.Controls.DataGridNavigationHelper.MoveUp(this.dgList.GridView);
        }

        public void MoveDown()
        {
            // LMS.UI.Controls.DataGridNavigationHelper.MoveDown(this.dgList.GridView);
        }
    }
}