﻿namespace YJH_HQ.Controls.RegionTreePickerView
{
    partial class RegionTreePickerView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.qpName = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RegionTree = new Telerik.WinControls.UI.RadTreeView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionTree)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btSearch);
            this.panel1.Controls.Add(this.qpName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(298, 32);
            this.panel1.TabIndex = 1;
            // 
            // btSearch
            // 
            this.btSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btSearch.Location = new System.Drawing.Point(231, 5);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(53, 22);
            this.btSearch.TabIndex = 2;
            this.btSearch.Text = "查询";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // qpName
            // 
            this.qpName.Location = new System.Drawing.Point(54, 6);
            this.qpName.Name = "qpName";
            this.qpName.Size = new System.Drawing.Size(163, 20);
            this.qpName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "名称：";
            // 
            // RegionTree
            // 
            this.RegionTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RegionTree.Location = new System.Drawing.Point(0, 32);
            this.RegionTree.Name = "RegionTree";
            this.RegionTree.Size = new System.Drawing.Size(298, 256);
            this.RegionTree.SpacingBetweenNodes = -1;
            this.RegionTree.TabIndex = 2;
            this.RegionTree.Text = "radTreeView1";
            // 
            // RegionTreePickerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RegionTree);
            this.Controls.Add(this.panel1);
            this.Name = "RegionTreePickerView";
            this.Size = new System.Drawing.Size(298, 288);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionTree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.WinControls.UI.RadTextBox qpName;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadTreeView RegionTree;
    }
}
