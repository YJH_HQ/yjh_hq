﻿
namespace YJH_HQ.Controls.RadTreeNode
{
    public class RadTreeNode : Telerik.WinControls.UI.RadTreeNode
    {
        public string AssemblyPath { get; private set; }

        public RadTreeNode(string text, System.Drawing.Image image, Telerik.WinControls.UI.RadTreeNode[] children)
            : this(text, null, image, children)
        {
        }

        public RadTreeNode(string text, string assemblyPath, System.Drawing.Image image)
            : this(text, assemblyPath, image, null)
        { }

        private RadTreeNode(string text, string assemblyPath, System.Drawing.Image image, Telerik.WinControls.UI.RadTreeNode[] children)
            : base(text, children)
        {
            AssemblyPath = assemblyPath;
            this.Image = image;
            this.Expanded = false;
        }
    }
}
