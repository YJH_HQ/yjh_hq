﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace YJH_HQ.Controls
{
    public class EnumColumn : GridViewDataColumn
    {

        private string _enumModelID;
        public string EnumModelID
        {
            get { return _enumModelID; }
            set { _enumModelID = value; }
        }

        public override Type GetCellType(GridViewRowInfo row)
        {
            if (row is GridViewDataRowInfo || row is GridViewNewRowInfo)
                return typeof(EnumCellElement);
            return base.GetCellType(row);
        }

    }
}
