﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace YJH_HQ.Controls.UCPageBase
{
    /// <summary>
    /// UserControl控件父页面，这里管控当前页面的按钮权限
    /// </summary>
    public class UCPageBase : UserControl
    {
        /// <summary>
        /// 当前页面所拥有的权限
        /// </summary>
        public IEnumerable<sys.Entities.Funcs> CurrentPageFuncs { get; private set; }

        /// <summary>
        /// 加载当前页面拥有的功能权限，与当前登录的用户功能权限比较，禁用没有的权限
        /// </summary>
        protected void LoadPermission()
        {
            //获取该页面的全路径名，再根据路径名获取该页面拥有的权限
            string fullName = this.GetType().FullName;
            List<sys.Entities.Funcs> list = sys.Services.OrgUnitService.GetFuncByParentName(fullName);

            CurrentPageFuncs = list;
            //注：每个页面中的操作按钮控件必须放在Telerik.WinControls.UI.RadCommandBar控件中
            //只有这样LoadPermission()方法才能控制操作按钮是否可用，否则这个方法无效
            var commandBarList = this.Controls.OfType<Telerik.WinControls.UI.RadCommandBar>();
            foreach (var item in commandBarList)
            {
                //先从Rows开始
                if (item.Rows.Count <= 0) continue;

                //一般Rows.Count为1，为了安全这里还是要遍历下
                foreach (var row in item.Rows)
                {
                    if (row.Strips.Count <= 0) continue;

                    //这里只取Strips[0]第一位，因为目前使用的没遇到过Strips.Count大于2的
                    CheckPermission(row.Strips[0].Items);
                }
            }
        }

        /// <summary>
        /// 验证是否具有权限
        /// </summary>
        private void CheckPermission(Telerik.WinControls.RadCommandBarBaseItemCollection list)
        {
            //这里只处理CommandBarButton、以及CommandBarDropDownButton下的RadMenuItem，如遇到别的类型则抛异常
            foreach (var item in list)
            {
                Type type = item.GetType();
                if (type == typeof(Telerik.WinControls.UI.CommandBarButton))
                {
                    if (!IsExist(item.Text))
                    {
                        item.Enabled = false;
                        item.PropertyChanged += item_PropertyChanged;
                    }
                }
                else if (type == typeof(Telerik.WinControls.UI.CommandBarDropDownButton))
                {
                    var dropDown = (Telerik.WinControls.UI.CommandBarDropDownButton)item;
                    foreach (var drop in dropDown.Items)
                        LoopMenuItem((Telerik.WinControls.UI.RadMenuItem)drop);
                }
                else
                {
                    throw new Exception(type.ToString() + "类型不在验证权限范围之内，请添加该类型验证。");
                }
            }
        }

        /// <summary>
        /// RadMenuItem下可以再添加RadMenuItem，所以这里要判断RadMenuItem.Items是否为空
        /// </summary>
        /// <param name="item"></param>
        private void LoopMenuItem(Telerik.WinControls.UI.RadMenuItem item)
        {
            if (item.Items.Count > 0)
            {
                foreach (var menu in item.Items)
                    LoopMenuItem((Telerik.WinControls.UI.RadMenuItem)menu);
            }
            else if (!IsExist(item.Text))
            {
                item.Enabled = false;
                item.PropertyChanged += item_PropertyChanged;
            }
        }

        /// <summary>
        /// 强制添加属性Changed事件，某种原因导致该控件Enabled启用的话，该事件可以扑捉到，及时禁用
        /// </summary>
        void item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Enabled")
                ((Telerik.WinControls.RadItem)sender).Enabled = false;
        }

        /// <summary>
        /// 根据text判断是否存在CurrentPageFuncs对象中
        /// </summary>
        private bool IsExist(string text)
        {
            //CurrentPageFuncs为null则返回false
            if (CurrentPageFuncs == null) return false;

            IEnumerable<sys.Entities.Funcs> query = CurrentPageFuncs.Where(s => s.Name == text);
            return query.Count() != 0;
        }

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (CurrentPageFuncs != null)
                CurrentPageFuncs = null;

            base.Dispose(disposing);
        }
    }
}