﻿namespace YJH_HQ.Controls.TipBar
{
    partial class TipBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Ctimer = new System.Windows.Forms.Timer(this.components);
            this.lbltip = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbltipmassage = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lbltip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbltipmassage)).BeginInit();
            this.SuspendLayout();
            // 
            // lbltip
            // 
            this.lbltip.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbltip.Image = global::YJH_HQ.Controls.Properties.Resources.InfoButtonImage;
            this.lbltip.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbltip.Location = new System.Drawing.Point(3, 4);
            this.lbltip.Name = "lbltip";
            this.lbltip.Size = new System.Drawing.Size(18, 18);
            this.lbltip.TabIndex = 1;
            this.lbltip.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbltip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.lbltipmassage);
            this.radGroupBox1.Controls.Add(this.lbltip);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(643, 25);
            this.radGroupBox1.TabIndex = 2;
            // 
            // lbltipmassage
            // 
            this.lbltipmassage.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbltipmassage.Location = new System.Drawing.Point(20, 4);
            this.lbltipmassage.Name = "lbltipmassage";
            this.lbltipmassage.Size = new System.Drawing.Size(55, 18);
            this.lbltipmassage.TabIndex = 2;
            this.lbltipmassage.Text = " 提示信息";
            this.lbltipmassage.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TipBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Name = "TipBar";
            this.Size = new System.Drawing.Size(643, 25);
            ((System.ComponentModel.ISupportInitialize)(this.lbltip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbltipmassage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lbltip;
        private System.Windows.Forms.Timer Ctimer;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel lbltipmassage;
    }
}
