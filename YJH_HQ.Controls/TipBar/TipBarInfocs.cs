﻿using System;
using System.Windows.Forms;

namespace YJH_HQ.Controls.TipBar
{
    public partial class TipBarInfocs : Telerik.WinControls.UI.RadForm
    {
        public TipBarInfocs(string tip)
        {
            InitializeComponent();
            this.tbxInfo.Text = tip;

            this.btnClose.Click += btnClose_Click;
            this.btnCopy.Click += btnCopy_Click;
        }

        #region ====事件====
        void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(this.tbxInfo.Text);
            MessageBox.Show("复制成功!", "提示!");
        }

        void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
