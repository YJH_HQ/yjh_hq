﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace YJH_HQ.Controls.TipBar
{
    public partial class TipBar : UserControl
    {
        #region ====字段====
        private string _tip;
        private string _exMessage;
        private int _clearTime;
        private TipType _tt;
        #endregion

        #region ====类型====
        public enum TipType
        {
            Information = 1,
            Warning = 2,
            Error = 3
        }
        #endregion

        #region ====属性====
        private string Tip
        {
            get
            {
                return this._tip;
            }
            set
            {
                this._tip = value;

                var curAss = Assembly.GetExecutingAssembly();
                this.lbltip.Image = global::YJH_HQ.Controls.Properties.Resources.InfoButtonImage;

                this.lbltip.Text = value;

            }
        }

        public int ClearTime
        {
            get { return this._clearTime; }
            set
            {
                this._clearTime = value;
                this.Ctimer.Interval = 10000;
            }
        }

        public Telerik.WinControls.UI.RadForm OwnedWindow
        {
            get;
            set;
        }
        #endregion

        public TipBar()
        {
            InitializeComponent();
            this._tip = " 提示信息";
            this._exMessage = null;
            this._clearTime = 10;
            this.Ctimer.Interval = this.Ctimer.Interval = 10000;
            this.Ctimer.Tick += Ctimer_Tick;
            this.lbltip.Click += lbltip_Click;
        }

        #region ====方法====
        private string GetCustomerExMessage(string exMessage)
        {
            string rst = "";

            if (!string.IsNullOrWhiteSpace(exMessage))
            {
                using (System.IO.StringReader sr = new System.IO.StringReader(exMessage))
                {
                    while (true)
                    {
                        string line = sr.ReadLine();

                        if (line != null)
                        {
                            line = line.Trim();
                        }

                        if (!string.IsNullOrWhiteSpace(line) && line.StartsWith("Message:"))
                        {
                            rst = line.Split(':')[1];
                            break;
                        }

                        //读完跳出
                        if (string.IsNullOrEmpty(line))
                        {
                            break;
                        }
                    }
                }
            }

            return rst;
        }

        public void ShowTip(string tip, TipType tt)
        {
            this.Ctimer.Stop();
            this._tip = tip;
            this._exMessage = null;
            this._tt = tt;
            var curAss = Assembly.GetExecutingAssembly();
            Image img;
            if (tt == TipType.Information)
            {
                img = global:: YJH_HQ.Controls.Properties.Resources.InfoButtonImage;

            }
            else if (tt == TipType.Warning)
            {
                img = global::YJH_HQ.Controls.Properties.Resources.Warning;

            }
            else
            {
                {
                    img = global::YJH_HQ.Controls.Properties.Resources.Error;
                }
            }
            this.lbltip.Image = img;

            this.lbltipmassage.Text = this.Tip;

            this.Ctimer.Start();
        }

        public void ShowTip(string tip, string exMessage, TipType tt)
        {
            this.Ctimer.Stop();
            this._tip = tip;
            this._exMessage = exMessage;

            var curAss = Assembly.GetExecutingAssembly();
            Image img;
            if (tt == TipType.Information)
            {
                img = global::YJH_HQ.Controls.Properties.Resources.InfoButtonImage;

            }
            else if (tt == TipType.Warning)
            {
                img = global::YJH_HQ.Controls.Properties.Resources.Warning;

            }
            else
            {
                img = global::YJH_HQ.Controls.Properties.Resources.Error;

            }
            this.lbltip.Image = img;

            this.lbltipmassage.Text = this.Tip + this.GetCustomerExMessage(exMessage);

            this.Ctimer.Start();
        }
        #endregion

        #region ====事件====
        void lbltip_Click(object sender, EventArgs e)
        {
            //if (this.Tip == "保存失败！" || this.Tip == "处理失败！")
            if (this._tt == TipType.Error)
            {
                this.Ctimer.Stop();

                string msgDetail = this.Tip + "\n" + this._exMessage;

                YJH_HQ.Controls.TipBar.TipBarInfocs tipbarinfo = new TipBarInfocs(msgDetail);
                tipbarinfo.Owner = FindForm(this);
                tipbarinfo.ShowDialog();

                this.Ctimer.Start();
            }
        }

        /// <summary>
        /// 查找承载用户控件的窗体
        /// </summary>
        public Telerik.WinControls.UI.RadFormControlBase FindForm(System.Windows.Forms.Control control)
        {
            if (control == null)
                return null;
            if (control.GetType().BaseType == typeof(Telerik.WinControls.UI.RadRibbonForm))
                return (Telerik.WinControls.UI.RadRibbonForm)control;
            else if (control.GetType().BaseType == typeof(Telerik.WinControls.UI.RadForm))
                return (Telerik.WinControls.UI.RadForm)control;
            return FindForm(control.Parent);
        }

        void Ctimer_Tick(object sender, EventArgs e)
        {
            this.lbltipmassage.Text = " 提示信息";
            var curAss = Assembly.GetExecutingAssembly();
            this.lbltip.Image = global::YJH_HQ.Controls.Properties.Resources.InfoButtonImage;

        }
        #endregion
    }
}
