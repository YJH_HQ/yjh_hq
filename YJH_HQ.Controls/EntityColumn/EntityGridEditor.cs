﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace YJH_HQ.Controls
{
    public class EntityGridEditor : BaseGridEditor
    {

        public override object Value
        {
            get
            {
                EntityGridEditorElement editor = (EntityGridEditorElement)this.EditorElement;
                return editor.SelectedEntity;
            }
            set
            {
                EntityGridEditorElement editor = (EntityGridEditorElement)this.EditorElement;
                editor.SelectedEntity = value as dps.Data.Mapper.EntityBase;
            }
        }

        protected override Telerik.WinControls.RadElement CreateEditorElement()
        {
            return new EntityGridEditorElement();
        }

        public override void BeginEdit()
        {
            base.BeginEdit();

            var editorElement = (EntityGridEditorElement)this.EditorElement;
            var cellElement = (GridDataCellElement)editorElement.Parent;
            var column = (EntityColumn)cellElement.ColumnInfo;
            editorElement.PickerView = column.PickerView;
            editorElement.DisplayMember = column.DisplayMember;
            editorElement.TextBox.Focus();
        }

        public override void OnValueChanged()
        {
            base.OnValueChanged();
        }

        public override bool EndEdit()
        {
            return base.EndEdit();
        }

    }
}
