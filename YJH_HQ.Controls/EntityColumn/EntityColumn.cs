﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;
using YJH_HQ.Controls.EntityPicker;

namespace YJH_HQ.Controls
{
    public class EntityColumn : GridViewDataColumn
    {

        private IEntityPickerView _pickerView;
        private string _displayMember;

        public IEntityPickerView PickerView
        {
            get { return _pickerView; }
            set { _pickerView = value; }
        }

        public string DisplayMember
        {
            get { return _displayMember; }
            set { _displayMember = value; }
        }

        [DefaultValue(typeof(dps.Data.Mapper.EntityBase))]
        public override Type DataType
        {
            get { return base.DataType; }
            set { base.DataType = value;}
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                if (this._pickerView != null)
                    return this._pickerView.SelectedEntity;
                else
                    return null;
            }
        }

        public EntityColumn()
            : this(string.Empty)
        { }

        public EntityColumn(string fieldName)
            : base(fieldName)
        {
            this.SetDefaultValueOverride(GridViewDataColumn.DataTypeProperty, typeof(dps.Data.Mapper.EntityBase));
        }

        public override IInputEditor GetDefaultEditor()
        {
            return new EntityGridEditor();
        }

        public override Type GetDefaultEditorType()
        {
            return typeof(EntityGridEditor);
        }

        public override Type GetCellType(GridViewRowInfo row)
        {
            if (row is GridViewDataRowInfo || row is GridViewNewRowInfo)
                return typeof(EntityCellElement);
            return base.GetCellType(row);
        }
    }
}
