﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YJH_HQ.Controls.EntityPicker;

namespace YJH_HQ.Controls
{

    public class EntityGridEditorElement : EntityPickerElement
    {

        public EntityGridEditorElement()
        {
            //this.CanFocus = true;
        }

        protected override void ShowPopup()
        {
            var rec = new Rectangle(this.PointToScreen(new Point(0,0)), new Size(this.Bounds.Width, this.Bounds.Height));
            base.Popup.Show(rec);
        }

    }

}
