﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.Controls.DataGrid
{
    public partial class DataGrid : UserControl
    {
        private int _pageIndex;
        private int _pageSize = 20;
        private int _totalPages = 1;
        private bool _distinct = false;

        public event Telerik.WinControls.UI.CellFormattingEventHandler CellFormatting;

        public RadGridView GridView
        {
            get { return this.gvList; }
        }

        //[Category("Data"), Editor("Telerik.WinControls.UI.Design.GridViewColumnCollectionEditor, Telerik.WinControls.UI.Design", typeof(UITypeEditor)), MergableProperty(false), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
         [Category("Data"), Editor("Telerik.WinControls.UI.Design.GridViewColumnCollectionEditor, Telerik.WinControls.UI.Design, Version=2014.1.402.40, Culture=neutral, PublicKeyToken=5bb2a467cbec794e", typeof(UITypeEditor)), MergableProperty(false), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public GridViewColumnCollection Columns
        {
            get { return this.gvList.MasterTemplate.Columns; }
        }

        private bool _ShowTotal = true;
        /// <summary>
        /// 是否显示记录总数
        /// </summary>
        [Category("布局"), Description("是否显示记录总数")]
        public bool ShowTotal
        {
            get { return _ShowTotal; }
            set
            {
                _ShowTotal = value;
                if (!_ShowTotal)
                {
                    this.labTotal.Visibility = ElementVisibility.Collapsed;
                    this.tbTotalCount.Visibility = ElementVisibility.Collapsed;
                }
                else
                {
                    this.labTotal.Visibility = ElementVisibility.Visible;
                    this.tbTotalCount.Visibility = ElementVisibility.Visible;
                }
            }
        }

        private string _appID;
        private string _serviceID;
        private string _method;
        public string QueryMethod
        {
            set
            {
                string[] sr = value.Split('.');
                if (sr.Length != 3)
                    throw new System.Exception("DataGrid.QueryMethod Error");

                _appID = sr[0];
                _serviceID = sr[1];
                _method = sr[2];
            }
        }

        private object[] _queryArgs;

        private Func<object[]> _getQueryArgsFunc;
        public Func<object[]> GetQueryArgsFunc
        {
            set { _getQueryArgsFunc = value; }
        }

        public bool Distinct
        {
            get { return this._distinct; }
            set { this._distinct = value; }
        }

        public Guid SelectedEntityID
        {
            get
            {
                if (this.gvList.SelectedRows.Count == 0)
                    return Guid.Empty;

                var row = this.gvList.SelectedRows[0].DataBoundItem as System.Data.DataRowView;
                if (row == null)
                    return Guid.Empty;
                return (Guid)row["ID"];
            }
        }

        public string TotalInfo
        {
            set
            {
                this.lblTotal.Text = value;
            }
        }

        public int PageIndex
        {
            get
            {
                return this._pageIndex;
            }
            set
            {
                this._pageIndex = value;
            }
        }

        public int PageSize
        {
            get
            {
                return this._pageSize;
            }
            set
            {
                this._pageSize = value;
            }
        }

        public System.Data.DataRowView SelectedRow
        {
            get
            {
                if (this.gvList.SelectedRows.Count == 0)
                    return null;

                return this.gvList.SelectedRows[0].DataBoundItem as System.Data.DataRowView;
            }
        }

        public DataGrid()
        {
            InitializeComponent();

            this.tbPageSize.SelectedIndex = 1;
            this.tbTotalCount.Text = "0";
            this.tbPageSize.SelectedIndexChanged += OnPageSizeChanged;
            this.btMoveFirst.Click += OnMoveFirst;
            this.btMoveLast.Click += OnMoveLast;
            this.btMovePre.Click += OnMovePre;
            this.btMoveNext.Click += OnMoveNext;

            this.lblTotal.Text = "";
        }

        private void OnMoveNext(object sender, EventArgs e)
        {
            this._pageIndex += 1;
            this.LoadDataInternal(false);
        }

        private void OnMovePre(object sender, EventArgs e)
        {
            this._pageIndex -= 1;
            this.LoadDataInternal(false);
        }

        private void OnMoveLast(object sender, EventArgs e)
        {
            this._pageIndex = _totalPages - 1;
            this.LoadDataInternal(false);
        }

        private void OnMoveFirst(object sender, EventArgs e)
        {
            this._pageIndex = 0;
            this.LoadDataInternal(false);
        }

        private void OnPageSizeChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            switch (this.tbPageSize.SelectedIndex)
            {
                case 0:
                    this._pageSize = 10;
                    break;
                case 1:
                    this._pageSize = 20;
                    break;
                case 2:
                    this._pageSize = 50;
                    break;
                default:
                    break;
            }
        }

        public void LoadData()
        {
            LoadDataInternal(true);
        }

        public void AddRow(System.Collections.Generic.Dictionary<string, object> data)
        {
            if (this.gvList.DataSource == null)
                throw new Exception("列表数据源为null，无法添加行！");

            dps.Common.Data.DataTable ds = this.gvList.DataSource as dps.Common.Data.DataTable;
            DataRow row = ds.NewRow();
            foreach (var k in data.Keys)
                row[k] = data[k];

            ds.Rows.Add(row);
        }

        public void RemoveSelectedRow()
        {
            if (this.gvList.SelectedRows.Count == 1)
                this.gvList.Rows.RemoveAt(this.gvList.SelectedRows[0].Index);
        }

        public void RefreshSelectedRow(System.Collections.Generic.Dictionary<string, object> data)
        {
            if (this.SelectedRow == null)
                throw new Exception("未找到已选择的行，无法刷新！");

            foreach (var k in data.Keys)
                this.SelectedRow[k] = data[k];

            this.gvList.SelectedRows[0].InvalidateRow();
        }

        private void LoadDataInternal(bool reload)
        {
            if (string.IsNullOrEmpty(_method))
                return;

            if (reload)
                _pageIndex = 0;

            if (_getQueryArgsFunc != null)
            {
                var args = _getQueryArgsFunc();
                if (this._queryArgs == null)
                {
                    if (args == null || args.Length == 0)
                        this._queryArgs = new object[2];
                    else
                        this._queryArgs = new object[args.Length + 2];
                }
                if (this._queryArgs.Length > 2)
                {
                    for (int i = 2; i < this._queryArgs.Length; i++)
                        this._queryArgs[i] = args[i - 2];
                }
            }
            else
            {
                if (this._queryArgs == null)
                    this._queryArgs = new object[2];
            }
            this._queryArgs[0] = _pageSize;
            this._queryArgs[1] = _pageIndex;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    var result = dps.Common.SysService.Invoke(_appID, _serviceID, _method, _queryArgs) as dps.Common.Data.DataTable;
                    if (result == null)
                        throw new System.Exception("查询结果不正确");

                    //计算总页数
                    _totalPages = result.TotalRows / _pageSize;
                    if (result.TotalRows % _pageSize > 0)
                        _totalPages++;
                    //UI线程更新信息
                    this.Invoke((MethodInvoker)delegate()
                    {
                        this.UpdateNavigationState(result.TotalRows);
                        this.gvList.DataSource = result;
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
            });
        }

        private void UpdateNavigationState(int totalRows)
        {
            this.tbTotalCount.Text = totalRows.ToString();
            if (_totalPages <= 1)
            {
                this.btMoveFirst.Enabled = false;
                this.btMovePre.Enabled = false;
                this.btMoveNext.Enabled = false;
                this.btMoveLast.Enabled = false;
            }
            else //多页
            {
                if (_pageIndex == 0)
                {
                    this.btMoveFirst.Enabled = false;
                    this.btMovePre.Enabled = false;
                    this.btMoveNext.Enabled = true;
                    this.btMoveLast.Enabled = true;
                }
                else if (_pageIndex == _totalPages - 1)
                {
                    this.btMoveFirst.Enabled = true;
                    this.btMovePre.Enabled = true;
                    this.btMoveNext.Enabled = false;
                    this.btMoveLast.Enabled = false;
                }
                else
                {
                    this.btMoveFirst.Enabled = true;
                    this.btMovePre.Enabled = true;
                    this.btMoveNext.Enabled = true;
                    this.btMoveLast.Enabled = true;
                }
            }
        }

        private void gvList_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (this.CellFormatting != null)
                this.CellFormatting(sender, e);
        }
    }
}
