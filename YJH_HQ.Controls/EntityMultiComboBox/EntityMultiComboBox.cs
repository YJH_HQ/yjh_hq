﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YJH_HQ.Controls
{
    public partial class EntityMultiComboBox : Telerik.WinControls.UI.RadMultiColumnComboBox
    {
        
        private string appID = null;
        private string serviceID = null;
        private string methodName = null;
        private string _entityModelID = null;
        private object[] _searchArgs = null;

        public EntityMultiComboBox()
        {
            InitializeComponent();

            this.KeyPress += EntityMultiComboBox_KeyPress;
        }

        private void EntityMultiComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.MultiColumnComboBoxElement.ShowPopup();
            }
        }

        public string EntityModelID
        {
            set
            {
                this._entityModelID = value;
            }
        }

        public object[] SearchArgs
        {
            set
            {
                this._searchArgs = value;
            }
        }

        public string SearchMethodPath
        {
            set
            {
                var path = value.Split('.');

                if (path.Length != 3)
                {
                    throw new Exception("查询方法路径格式不正确！");
                }

                appID = path[0];
                serviceID = path[1];
                methodName = path[2];
            }
        }

        public bool ShowHeaders
        {
            set
            {
                this.EditorControl.ShowColumnHeaders = value;
                this.EditorControl.ShowRowHeaderColumn = value;
            }
        }

        //public dps.Common.Data.Entity SelectedEntity
        //{
        //    get
        //    {
        //        return dps.Common.Data.Entity.Retrieve(this._entityModelID, Guid.Parse(this.SelectedValue.ToString()));
        //    }
        //    set
        //    {
        //        this.Text = value[this.DisplayMember].StringValue;
        //        this.SelectedValue = value.ID;
        //    }
        //}

        public void AddColumns(string columnName, string fieldName, string HeaderText, int width, bool isVisible)
        {
            var c = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            c.Name = columnName;
            c.FieldName = fieldName;
            c.HeaderText = HeaderText;
            c.Width = width;
            c.IsVisible = isVisible;
            this.EditorControl.Columns.Add(c);
        }

        protected override void OnDropDownOpening(CancelEventArgs e)
        {
            var args = new List<object>(this._searchArgs);
            args.Add(this.Text);

            this.DataSource = dps.Common.SysService.Invoke(this.appID, this.serviceID, this.methodName, args.ToArray());

            base.OnDropDownOpening(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
