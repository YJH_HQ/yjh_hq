﻿using System;
using Telerik.WinControls.UI;

namespace YJH_HQ.Controls.RadDropDownList
{
    public class RadDropDownList : Telerik.WinControls.UI.RadDropDownList
    {
        public RadDropDownList()
            : base()
        { }

        public void EnumSource(Type type)
        {
            EnumSource(type, false);
        }

        public void EnumSource(Type type, bool insertTopSelectItem)
        {
            EnumSource(type, insertTopSelectItem, "请选择");
        }

        /// <summary>
        /// 绑定扩展控件源数据
        /// </summary>
        /// <param name="type">枚举类型</param>
        /// <param name="insertTopSelectItem">是否要插入Top选择项（请选择）</param>
        /// <param name="insertTopSelectItemText">要插入Top选择项的Text，值为-1</param>
        public void EnumSource(Type type, bool insertTopSelectItem, string insertTopSelectItemText)
        {
            if (type.BaseType != typeof(Enum))
                throw new Exception("参数类型不是枚举。");

            if (insertTopSelectItem)
            {
                this.Items.Add(new RadListDataItem(insertTopSelectItemText, -1));
                this.Items[0].Selected = true;
            }

            foreach (var value in Enum.GetValues(type))
                this.Items.Add(new RadListDataItem(value.ToString(), (int)value));
        }

        /// <summary>
        /// 获取当前选中节点转换为指定的枚举对象
        /// </summary>
        /// <typeparam name="T">要转换的枚举对象</typeparam>
        /// <returns>返回-1则为“请选择”项</returns>
        public T GetSelectedItemToEnumObj<T>() where T : struct
        {
            T outT;
            Enum.TryParse<T>(this.SelectedItem.Value.ToString(), out outT);
            return outT;
        }
    }
}
