﻿using System;

namespace YJH_HQ.Controls.EntityPicker
{
    public interface IEntityPickerView : IDisposable
    {
        dps.Data.Mapper.EntityBase SelectedEntity { get; }

        void LoadData();

        /// <summary>
        /// 设置输入焦点
        /// </summary>
        void FocusInput();

        void MoveUp();

        void MoveDown();

        //void PageUp();

        //void PageDown();

        //void MoveFirst();

        //void MoveLast();
    }
}