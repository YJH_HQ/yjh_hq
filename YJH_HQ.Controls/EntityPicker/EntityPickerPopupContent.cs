﻿using System;
using System.Windows.Forms;

namespace YJH_HQ.Controls.EntityPicker
{
    public partial class EntityPickerPopupContent : UserControl
    {
        #region Statics
        //private static Dictionary<Type, IEntityPickerView> _views;

        //static EntityPickerView()
        //{
        //    _views = new Dictionary<Type, IEntityPickerView>();
        //}

        //static IEntityPickerView GetPickerView(Type type)
        //{
        //    IEntityPickerView view = null;
        //    if (!_views.TryGetValue(type, out view))
        //    {
        //        view = System.Activator.CreateInstance(type) as IEntityPickerView;
        //        if (view == null)
        //            throw new System.ArgumentException("指定了无效的实体选择视图类型，必须实现IEntityPickerView接口");
        //        _views.Add(type, view);
        //    }
        //    return view;
        //} 
        #endregion

        private IEntityPickerView _pickerView;
        private Type _pickerViewType;
        private EntityPickerPopup _owner;

        internal EntityPickerPopup Owner
        {
            set
            {
                _owner = value;
            }
        }

        public IEntityPickerView PickerView
        {
            get { return _pickerView; }
            set
            {
                if (_pickerView != value)
                {
                    _pickerView = value;
                    this.plView.Controls.Clear();

                    if (value != null)
                    {
                        var view = value as Control;
                        if (view != null)
                        {
                            this._owner.Width -= this.plView.Width - view.Width;
                            this._owner.Height -= this.plView.Height - view.Height;

                            view.Dock = DockStyle.Fill;
                            this.plView.Controls.Add(view);
                        }
                    }
                }
            }
        }

        public EntityPickerPopupContent()
        {
            InitializeComponent();
            base.SetStyle(ControlStyles.Selectable, false);
            this.Dock = DockStyle.Fill;
            this.btOK.Click += OnOKClick;
            this.btCancel.Click += OnCancelClick;
        }

        protected override bool ProcessKeyPreview(ref Message m)
        {
            Keys keyData = ((Keys)((int)((long)m.WParam)));// | ModifierKeys;
            //System.Diagnostics.Debug.WriteLine("EntityPickerView.ProcessKeyPreview :" + keyData.ToString() + "\t" + m.ToString());

            switch (keyData)
            {
                case Keys.Enter:
                    if (m.Msg == YJH_HQ.Controls.Popup.NativeMethods.WM_KEYUP && PickerView != null)
                    {
                        if (ModifierKeys == Keys.Shift)
                            PickerView.LoadData();
                        else if (ModifierKeys == Keys.None)
                            this.OnOKClick(this.btOK, EventArgs.Empty);
                        //System.Diagnostics.Debug.WriteLine("Enter load data");
                    }
                    return true;
                case Keys.Up:
                    if (m.Msg == YJH_HQ.Controls.Popup.NativeMethods.WM_KEYUP && PickerView != null)
                        PickerView.MoveUp();
                    return true;
                case Keys.Down:
                    if (m.Msg == YJH_HQ.Controls.Popup.NativeMethods.WM_KEYUP && PickerView != null)
                        PickerView.MoveDown();
                    return true;
                //case Keys.Space:
                //if (m.Msg == NativeMethods.WM_KEYUP)
                //    this.OnOKClick(this.btOK, EventArgs.Empty);
                //    return true;
                default:
                    break;
            }

            return base.ProcessKeyPreview(ref m);
        }

        private void OnCancelClick(object sender, EventArgs e)
        {
            _owner.Close();
        }

        private void OnOKClick(object sender, EventArgs e)
        {
            _owner.Owner.SelectedEntity = _pickerView.SelectedEntity;
            _owner.Close();
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            _owner.Owner.SelectedEntity = null;
            _owner.Close();
        }
    }
}
