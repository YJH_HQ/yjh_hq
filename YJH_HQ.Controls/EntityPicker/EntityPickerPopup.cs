﻿using System;
using System.Windows.Forms;

namespace YJH_HQ.Controls.EntityPicker
{
    public class EntityPickerPopup : YJH_HQ.Controls.Popup.Popup
    {
        private EntityPickerPopupContent _content = null;
        private EntityPickerElement _owner;
        internal EntityPickerElement Owner
        {
            get { return _owner; }
        }

        public IEntityPickerView PickerView
        {
            get { return _content.PickerView; }
            set { _content.PickerView = value; }
        }

        #region Ctor
        public EntityPickerPopup(EntityPickerElement owner)
            : base(new EntityPickerPopupContent())
        {
            _owner = owner;
            _content = (EntityPickerPopupContent)base.Content;
            _content.Owner = this;
        }
        #endregion

        #region Property
        public EntityPickerPopupContent Content
        {
            get { return this._content; }
        }
        #endregion

        #region Overrides
        protected override void OnOpened(EventArgs e)
        {
            base.OnOpened(e);
            this.PickerView.FocusInput();
        }

        protected override void OnClosed(ToolStripDropDownClosedEventArgs e)
        {
            base.OnClosed(e);

        }
        #endregion

    }
}