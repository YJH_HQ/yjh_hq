﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.Controls.EntityPicker
{
    public class EntityPicker : RadControl
    {
        private EntityPickerElement entityPickerElement = null;
        public event ToolStripDropDownClosedEventHandler PickerViewClosed;

        #region Properties
        public IEntityPickerView PickerView
        {
            get { return entityPickerElement.PickerView; }
            set { entityPickerElement.PickerView = value; }
        }

        public string DisplayMember
        {
            get { return entityPickerElement.DisplayMember; }
            set { entityPickerElement.DisplayMember = value; }
        }

        public Guid? SelectedEntityID
        {
            get { return entityPickerElement.SelectedEntityID; }
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get { return entityPickerElement.SelectedEntity; }
            set
            {
                entityPickerElement.SelectedEntity = value;
            }
        }

        public override string Text
        {
            get
            {
                if (this.SelectedEntityID.HasValue)
                    return this.SelectedEntity.ToString();
                else
                    return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }
        #endregion

        #region Constructors, initialization & disposal
        public EntityPicker()
        {
            this.TabStop = false;
            this.SetStyle(ControlStyles.Selectable, true);
            this.entityPickerElement.Popup.Closed += Popup_Closed;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //this.UnwireEvents();
                this.entityPickerElement.Dispose();
                this.entityPickerElement = null;
            }
            base.Dispose(disposing);
        }

        protected override void CreateChildItems(RadElement parent)
        {
            this.entityPickerElement = new EntityPickerElement();
            this.entityPickerElement.ArrowButton.Arrow.AutoSize = true;
            this.entityPickerElement.AutoSizeMode = RadAutoSizeMode.WrapAroundChildren;

            //this.WireEvents();

            this.RootElement.Children.Add(this.entityPickerElement);

            base.CreateChildItems(parent);
        }
        #endregion

        #region Theme
        public override string ThemeClassName
        {
            get
            {
                return typeof(Telerik.WinControls.UI.RadMultiColumnComboBox).FullName;
            }
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // EntityPicker
            // 
            this.ResumeLayout(false);
        }

        private void Popup_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            if (this.PickerViewClosed != null)
            {
                this.PickerViewClosed(sender, e);
            }
        }

    }
}