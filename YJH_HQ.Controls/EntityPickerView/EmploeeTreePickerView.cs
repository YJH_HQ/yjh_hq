﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace YJH_HQ.Controls.EntityPickerView
{
    public partial class EmploeeTreePickerView : UserControl, YJH_HQ.Controls.EntityPicker.IEntityPickerView
    {
        public Telerik.WinControls.UI.RadTreeNode selectedNode { set; get; }
        private dps.Data.Mapper.EntityList<sys.Entities.OrgUnit> _source;
        private System.Guid OUID;
        public EmploeeTreePickerView(System.Guid CurrentCompanyOuID)
        {
            InitializeComponent();
            OUID = CurrentCompanyOuID;
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.Load += (s, e) => { this.LoadData(); };
            this.OrgUnitTree.SelectedNodeChanged += OrgUnitTree_SelectedNodeChanged;
        }

        private void OrgUnitTree_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            selectedNode = e.Node;
        }

        public dps.Data.Mapper.EntityBase SelectedEntity
        {
            get
            {
                if (selectedNode == null)
                {
                    RadMessageBox.Show("请选择要加入的员工", "提示");
                    return null;
                }
                var orgVal = (sys.Entities.OrgUnit)selectedNode.Tag;
                if (orgVal.BaseType != "sys.Emploee")
                {
                    RadMessageBox.Show("请选择要加入的员工", "提示");
                    return null;
                }
                var id = new Guid(this.OrgUnitTree.SelectedNode.Value.ToString());
                if (id == Guid.Empty)
                    return null;
                var result1 = new sys.Entities.OrgUnit(dps.Common.Data.Entity.Retrieve(sys.Entities.OrgUnit.EntityModelID, id));
                return new sys.Entities.Emploee(dps.Common.Data.Entity.Retrieve(sys.Entities.Emploee.EntityModelID, result1.BaseID));
            }
        }

        public void LoadData()
        {
            this.LoadOrgUnitTreeAsync();
        }

        private void Search()
        {
            var s = this.OrgUnitTree.Find(tn => tn.Text.IndexOf(this.qpName.Text) != -1);
            if (s != null)
            {
                this.OrgUnitTree.SelectedNode = s;
            }
        }

        /// <summary>
        /// 加载组织结构树
        /// </summary>
        private void LoadOrgUnitTreeAsync()
        {
            System.Threading.ThreadPool.QueueUserWorkItem((s) =>
            {
                _source = sys.Services.OrgUnitService.GetNodesById(OUID);
                //_source = new dps.Data.Mapper.EntityList<sys.Entities.OrgUnit>(rootNodes);
                this.Invoke(new Action(() =>
                {
                    AddOrgUnitToTreeView(_source);
                }));
            });
        }
        /// <summary>
        /// 添加树节点
        /// </summary>
        /// <param name="source">根节点集合</param>
        private void AddOrgUnitToTreeView(dps.Data.Mapper.EntityList<sys.Entities.OrgUnit> source)
        {
            foreach (var item in source)
            {
                LoopAddOrgUnit(null, item);
            }

            this.OrgUnitTree.Nodes[0].Expand();
            this.OrgUnitTree.Nodes[0].Selected = true;
            //FlushAndUpdateNode();
        }

        /// <summary>
        /// 循环添加节点
        /// </summary>
        /// <param name="parent">父节点</param>
        /// <param name="orgunit">要添加到节点的数据</param>
        private Telerik.WinControls.UI.RadTreeNode LoopAddOrgUnit(Telerik.WinControls.UI.RadTreeNode parent, sys.Entities.OrgUnit orgunit)
        {
            //排除无锡分公司ouid=E81C4FF4-53EF-403B-8C6A-F8FDE203B9AD
            //if (!source.Exists(t => t.Instance.ID == orgunit.Instance.ID))
            //    source.Add(orgunit);
            var node = new Telerik.WinControls.UI.RadTreeNode(orgunit.Name.ToString());
            node.Image = GetNodeImage(orgunit.BaseType);
            node.Tag = orgunit;
            node.Value = orgunit.Instance.ID;
            if (parent == null)
                this.OrgUnitTree.Nodes.Add(node);
            else
            {
                if (orgunit.Instance.ID != System.Guid.Parse("E81C4FF4-53EF-403B-8C6A-F8FDE203B9AD"))
                {
                    parent.Nodes.Add(node);
                }
            }
            if (orgunit.SubItems.Count > 0)
            {
                foreach (var item in orgunit.SubItems)
                {
                    if (orgunit.Instance.ID == System.Guid.Parse("E81C4FF4-53EF-403B-8C6A-F8FDE203B9AD"))
                    {
                        continue;
                    }
                    else
                    {
                        LoopAddOrgUnit(node, item);
                    }
                }
            }
            return node;
        }
        /// <summary>
        /// 获取节点的图标
        /// </summary>
        /// <param name="baseType">基类类型</param>
        /// <returns></returns>
        private Image GetNodeImage(string baseType)
        {
            switch (baseType.ToLower())
            {
                case "sys.businessunit":
                    return Properties.Resources.Home16;
                case "sys.workgroup":
                    return Properties.Resources.Group16;
                case "sys.emploee":
                    return Properties.Resources.User16;
                default:
                    return null;
            }
        }

        public void FocusInput()
        {
            //this.qpName.Focus();
        }

        public void MoveUp()
        {
            //LMS.UI.Controls.DataGridNavigationHelper.MoveUp(this.dgList.GridView);
        }

        public void MoveDown()
        {
            // LMS.UI.Controls.DataGridNavigationHelper.MoveDown(this.dgList.GridView);
        }
    }
}
