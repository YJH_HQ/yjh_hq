﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI
{
    public partial class LoginForm : Telerik.WinControls.UI.ShapedForm
    {
        public LoginForm()
        {
            InitializeComponent();

            try
            {
                //绑定系统
                ddlSystem.EnumSource(typeof(sys.Enums.OrganizationType), true, "请选择");

                btnCancel.Click += btnCancel_Click;
                btnLogin.Click += btnLogin_Click;

                ddlSystem.SelectedIndex = 1;
                txtUser.Text = "";
                txtPass.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = txtUser.Text;
            string userPass = txtPass.Text;

            if ((int)ddlSystem.SelectedItem.Value == -1)
            {
                RadMessageBox.Show(this, "请先选择要登录的组织结构", "登录错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPass))
            {
                RadMessageBox.Show(this, "请输入用户名及密码", "登录错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            dps.Client.dpsClient.Default.DomainName = YJH_HQ.UI.Properties.Settings.Default.Domain;
            dps.Client.dpsClient.Default.UserName = userName;
            dps.Client.dpsClient.Default.PassWord = userPass;
            try
            {
                dps.Client.dpsClient.Default.Login();
                //登录成功后仍要验证该账户是否存在该选择的组织机构下
                int orgType = (int)ddlSystem.SelectedItem.Value;
                List<sys.Entities.OrgUnit> availableOrgList = sys.Services.LoginService.CheckAccountWithOrg(userName, orgType);
                //绑定登录的组织机构、以及该账户所属的组织机构
                YJH_HQ.UI.Globle globle = new Globle((sys.Enums.OrganizationType)orgType, availableOrgList);

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (dps.Common.Exceptions.SysException ex)
            {
                //只有有异常就执行LogOut操作
                dps.Client.dpsClient.Default.Logout();

                string errmsg = ex.ErrorCode.ToString();
                switch (ex.ErrorCode)
                {
                    case dps.Common.Exceptions.ErrorCode.Channel_ConnectToServer_Error:
                        errmsg = "连接服务器失败！";
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_Connection_Lost:
                        errmsg = "服务器连接丢失！";
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_Emploee_HasNot_Asign_OrgUnit:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_Is_Null:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_RegisterSession_Fail:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_Timeout:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_User_Audit_Fail:
                        errmsg = "用户名密码错误！";
                        break;
                    case dps.Common.Exceptions.ErrorCode.Channel_User_NotIn_System:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_Can_Not_Load_Assembly:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_Can_Not_Load_Type:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_Fail:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_NO_KnownType_Registed:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_SymmetricAlgorithm_Is_Null:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Deserialize_Variant_OutOfRange:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Expression_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_Method_Inner_Exception:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_Method_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_Service_File_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_Service_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_SessionKey_ThreadName_Not_Same:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Service_Unknow_Error:
                        errmsg = ex.Message;
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Session_NotSame_With_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Invoke_Session_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Model_EntityMemberModel_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Model_EntityModel_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Model_EntityModel_WithInPublish:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Model_ServiceModel_Not_Exits:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_BuildQuery_NoPermission:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_DeleteState_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_EntityOrgUnitPolicy_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_IncludeItem_MultiTargetsMember:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_IncludeItem_NotEntityRefOrEntitySet:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_InsertState_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_QueryForPage_NoSortItems_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_SqlException:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Persistent_UpdateState_Error:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Serialize_Fail:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Serialize_Not_Supported_Type:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Serialize_SymmetricAlgorithm_Is_Null:
                        break;
                    case dps.Common.Exceptions.ErrorCode.Unknown:
                        break;
                    default:
                        break;
                }
                RadMessageBox.Show(this, errmsg, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
