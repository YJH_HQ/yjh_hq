﻿
namespace YJH_HQ.UI
{
    public static class CommonService
    {
        /// <summary>
        /// 查找承载用户控件的窗体
        /// </summary>
        public static Telerik.WinControls.UI.RadFormControlBase FindForm(System.Windows.Forms.Control control)
        {
            if (control == null)
                return null;
            if (control.GetType().BaseType == typeof(Telerik.WinControls.UI.RadRibbonForm))
                return (Telerik.WinControls.UI.RadRibbonForm)control;
            else if (control.GetType().BaseType == typeof(Telerik.WinControls.UI.RadForm))
                return (Telerik.WinControls.UI.RadForm)control;
            return FindForm(control.Parent);
        }
    }
}
