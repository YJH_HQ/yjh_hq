﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH_HQ.UI
{
   public static class SystemService
    {

        private static sys.Entities.BusinessUnit _currentCompanyBu;

        /// <summary>
        /// 公司BusinessUnit.
        /// </summary>
        public static sys.Entities.BusinessUnit CurrentCompanyBu
        {
            get
            {
                if (dps.Client.dpsClient.Default.CurrentBizUnit == null)
                    return null;

                if (_currentCompanyBu == null)
                {
                    _currentCompanyBu = new sys.Entities.BusinessUnit(dps.Client.dpsClient.Default.CurrentBizUnit);
                }
                return _currentCompanyBu;
            }
        }

        private static sys.Entities.OrgUnit _currentCompanyOu;
        /// <summary>
        /// 公司 OrgUnit.
        /// </summary>
        public static sys.Entities.OrgUnit CurrentCompanyOu
        {
            get
            {

                if (dps.Client.dpsClient.Default.CurrentOrgUnit == null)
                    return null;

                if (_currentCompanyOu == null)
                {
                    _currentCompanyOu = new sys.Entities.OrgUnit(FindCompanyOU(dps.Client.dpsClient.Default.CurrentOrgUnit));
                }
                return _currentCompanyOu;
            }
        }

        private static sys.Entities.OrgUnit _currentEmpOu;
        /// <summary>
        /// 当前登录的Emploee对应的OrgUnit
        /// </summary>
        public static sys.Entities.OrgUnit CurrentEmpOu
        {
            get
            {
                if (dps.Client.dpsClient.Default.CurrentOrgUnit == null)
                    return null;

                if (_currentEmpOu == null)
                {
                    _currentEmpOu = new sys.Entities.OrgUnit(dps.Client.dpsClient.Default.CurrentOrgUnit);
                }
                return _currentEmpOu;
            }
        }

        private static sys.Entities.OrgUnit _currentDeptOu;
        /// <summary>
        /// 部门 OrgUnit.
        /// </summary>
        public static sys.Entities.OrgUnit CurrentDeptOu
        {
            get
            {
                if (_currentDeptOu == null)
                {
                    var workGroup = dps.Client.dpsClient.Default.CurrentOrgUnit["Parent"].EntityValue;
                    _currentDeptOu = new sys.Entities.OrgUnit(workGroup);
                }
                return _currentDeptOu;
            }
        }

        private static sys.Entities.Emploee _currentEmploee;
        /// <summary>
        /// 当前登陆人 Emploee.
        /// </summary>
        public static sys.Entities.Emploee CurrentEmploee
        {
            get
            {
                if (dps.Client.dpsClient.Default.CurrentOrgUnit == null)
                    return null;

                if (_currentEmploee == null)
                {
                    var emp = dps.Client.dpsClient.Default.CurrentOrgUnit["Base"].EntityValue;
                    _currentEmploee = new sys.Entities.Emploee(emp);
                }
                return _currentEmploee;
            }
        }

        public static void ResetCurrentEmpolee()
        {
            _currentEmploee = null;
        }

        /// <summary>
        /// 获取当前登陆人的公司orgUnit.
        /// </summary>
        /// <param name="ou"></param>
        /// <returns></returns>
        public static dps.Common.Data.Entity FindCompanyOU(dps.Common.Data.Entity ou)
        {
            if (ou["BaseType"].StringValue == "sys.BusinessUnit")
                return ou;
            else
                return FindCompanyOU(ou["Parent"].EntityValue);

        }

        //切换员工机构对应
        public static void ChangeCurrentOU(dps.Common.Data.Entity ou)
        {
            dps.Client.dpsClient.Default.ChangeCurrentOrgUnit(ou);
            _currentCompanyBu = new sys.Entities.BusinessUnit(dps.Client.dpsClient.Default.CurrentBizUnit);
            _currentCompanyOu = new sys.Entities.OrgUnit(FindCompanyOU(dps.Client.dpsClient.Default.CurrentOrgUnit));
            _currentEmploee = new sys.Entities.Emploee(dps.Common.Data.Entity.Retrieve(sys.Entities.Emploee.EntityModelID, new sys.Entities.OrgUnit(dps.Client.dpsClient.Default.CurrentOrgUnit).BaseID));
            _currentEmpOu = new sys.Entities.OrgUnit(dps.Client.dpsClient.Default.CurrentOrgUnit);
            _currentDeptOu = new sys.Entities.OrgUnit(dps.Client.dpsClient.Default.CurrentOrgUnit["Parent"].EntityValue);
        }
    }
}
