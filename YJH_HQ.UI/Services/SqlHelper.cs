﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH_HQ.UI.Services
{
    public class SqlHelper
    {
        //private static string ConStr = ConfigurationManager.ConnectionStrings["DPSConnection"].ConnectionString;
        private static string ConStr = "Data Source=f8x34i2s30.database.chinacloudapi.cn;Initial Catalog=YJH_DPSStore;Persist Security Info=True;User ID=yjhdps;Password=Ydps86571";
        /// <summary>
        /// 调用数据库存储过程
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static int TakeStoredProcedure(string sql, params SqlParameter[] parameters)
        {
            using (SqlConnection scon = new SqlConnection(ConStr))
            {
                scon.Open();
                using (SqlCommand cmd = scon.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.UpdatedRowSource = UpdateRowSource.None;
                    cmd.CommandTimeout = 600;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddRange(parameters);
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public static DataTable TableProcedure(string sql, params SqlParameter[] parameters)
        {
            using (SqlConnection connection = new SqlConnection(ConStr))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddRange(parameters);
                    cmd.CommandTimeout = 600;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "ds");
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds.Tables[0];
            }
        }
    }
}
