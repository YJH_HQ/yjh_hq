﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH_HQ.UI.Services
{
    public static class UIExtendService
    {
        public static void PostEdit(this Telerik.WinControls.UI.RadGridView gridView)
        {
            gridView.Rows.AddNew();
            gridView.Rows.Remove(gridView.CurrentRow);
        }

        public static void RefreshData(this Telerik.WinControls.UI.RadGridView gridView)
        {
            if (gridView.Rows.Count > 0)
            {
                foreach (var r in gridView.Rows)
                {
                    r.InvalidateRow();
                }
            }
        }
    }
}
