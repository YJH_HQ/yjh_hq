﻿using System;
using System.Net;
using System.Windows.Forms;

namespace YJH_HQ.UI
{
    static class Program
    {
        static bool isLogin = true;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //设置控件的本地化语言
            //RadMessageLocalizationProvider.CurrentProvider = new P2P.UIL.RadMessageCNLocalizationProvider();
            //RadGridLocalizationProvider.CurrentProvider = new P2P.UIL.GridViewCNLocalizationProvider();
            try
            {
                //设定本地缓存块
                dps.Common.Cache.ByteBuffer.Init(dps.Common.Messages.BufferHelper.BlockSize, 32);

                //设定通信方式
                IPEndPoint ep = new System.Net.IPEndPoint(IPAddress.Parse(YJH_HQ.UI.Properties.Settings.Default.AppServerIP), YJH_HQ.UI.Properties.Settings.Default.AppServerPort);
                dps.Client.dpsClient.Default.UseTcpChannel(ep);
                dps.Client.dpsClient.Default.ChannelStateChanged += Default_ChannelStateChanged;

                var loginForm = new LoginForm();
                if (loginForm.ShowDialog() == DialogResult.OK)
                {
                    isLogin = false;

                    Application.Run(new UI_Main());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        static void Default_ChannelStateChanged(bool obj)
        {
            if (!obj && !isLogin)
            {
                Application.Restart();
            }
        }
    }
}
