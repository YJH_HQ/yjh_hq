﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.PersonnelMatters
{
    public partial class UI_hr_job_adj : Telerik.WinControls.UI.RadForm
    {

        #region===属性===
        public YJH.Entities.hr_emploee hr_emploee = null;
        public YJH.Entities.hr_job_adj1 hr_job_adj1 = null;
        public List<YJH.Entities.hr_job_adj1> jobList = null;

        public string companyname = null;

        #endregion


        #region===构造===
        public UI_hr_job_adj(YJH.Entities.hr_emploee hr_emploee1, string companyname1)
        {
            InitializeComponent();
            hr_emploee = hr_emploee1;
            companyname = companyname1;

            this.btnNew.Click += BtnNew_Click;
            this.btnSave.Click += BtnSave_Click;
            this.gvJob.DefaultValuesNeeded += GvSalary_DefaultValuesNeeded;
            this.gvJob.UserAddedRow += GvSalary_UserAddedRow;
            this.gvJob.CellEndEdit += GvSalary_CellEndEdit;
            this.gvJob.CellFormatting += GvSalary_CellFormatting;

            SetCombox();

        }


        #endregion

        #region===方法===
        private void SetCombox()
        {
            this.tbxCompany_id.Text = companyname;
            this.tbxNumber.Text = hr_emploee.Number;
            this.tbxName.Text = hr_emploee.Name;

            //绑定部门
            var DeptIdcolumn = this.gvJob.Columns["clDeptId"] as GridViewComboBoxColumn;
            var DeptIdlist = YJH.Services.hr_emploeeService.GetDept(hr_emploee.Company_id);
            DeptIdcolumn.DisplayMember = "Name";
            DeptIdcolumn.ValueMember = "BaseID";
            DeptIdcolumn.DataSource = DeptIdlist;

            //绑定职务
            var job_idcolumn = this.gvJob.Columns["cljob_id"] as GridViewComboBoxColumn;
            var job_idTable = YJH.Services.hr_emploeeService.Gethr_Job1Table();
            job_idcolumn.DisplayMember = "Name";
            job_idcolumn.ValueMember = "ID";
            job_idcolumn.DataSource = job_idTable;

            jobList = YJH.Services.hr_emploeeService.Gethr_job_adj1List(hr_emploee.Instance.ID);
            this.gvJob.DataSource = jobList;


        }

        #endregion

        #region===事件===

        private void BtnSave_Click(object sender, EventArgs e)
        {
            foreach (YJH.Entities.hr_job_adj1 item in jobList)
            {
                if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                {
                    item.Company_id = hr_emploee.Company_id;
                    item.EmploeeId = hr_emploee.Instance.ID;
                }
            }
            try
            {
                YJH.Services.hr_emploeeService.Savehr_job_adj1(jobList);
                foreach (YJH.Entities.hr_job_adj1 item in jobList)
                {
                    item.Instance.AcceptChanges();
                }
                RadMessageBox.Show("保存成功！", "提示");
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("保存出错，错误原因：" + ex.Message, "提示");
            }
        }

        private void GvSalary_DefaultValuesNeeded(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells["clcreater1"].Value = Globle.CurrentEmployee.Base.Name;
            e.Row.Cells["clcreater"].Value = Globle.CurrentEmployee.Instance.ID;
            e.Row.Cells["clcreate_date"].Value = System.DateTime.Now;
        }

        private void GvSalary_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (this.gvJob.Rows.Count > 0)
            {
                foreach (var v in this.gvJob.Rows)
                {
                    v.Cells["clcreater1"].Value = (new sys.Entities.Emploee(dps.Common.Data.Entity.Retrieve(sys.Entities.Emploee.EntityModelID, (Guid)v.Cells["clcreater"].Value))).Base.Name;
                }
            }
        }

        private void GvSalary_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (this.gvJob.Rows.Count > 0)
            {
                if (e.Column.Name == "clDeptId")
                {
                    if (e.Row.Cells["clDeptId"].Value == null)
                    {
                        RadMessageBox.Show("请选择部门", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                }
                if (e.Column.Name == "cljob_id")
                {
                    GridViewRowInfo row = gvJob.Rows.First();
                    if (e.Row.Cells["cljob_id"].Value==null)
                    {
                        RadMessageBox.Show("请选择职务", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                }
                if (e.Column.Name == "clDateEffect")
                {
                    GridViewRowInfo row = gvJob.Rows.First();
                    if ((DateTime)e.Row.Cells["clDateEffect"].Value < (DateTime)row.Cells["clDateEffect"].Value)
                    {
                        RadMessageBox.Show("生效日期必须在" + ((DateTime)row.Cells["clDateEffect"].Value).ToShortDateString() + "之后", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                }
            }
        }

        private void GvSalary_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            this.gvJob.AllowAddNewRow = false;
            this.gvJob.AllowEditRow = false;
            this.gvJob.ReadOnly = true;
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            this.gvJob.AllowAddNewRow = true;
            this.gvJob.AllowEditRow = true;
            this.gvJob.ReadOnly = false;
        }

        #endregion


    }
}
