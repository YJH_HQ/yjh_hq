﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.PersonnelMatters
{
    public partial class UI_hr_salary_adj : Telerik.WinControls.UI.RadForm
    {
        #region===属性===
        public YJH.Entities.hr_emploee hr_emploee = null;
        public YJH.Entities.hr_emp_salary hr_emp_salary = null;
        public YJH.Entities.hr_salary_adj1 hr_salary_adj1 = null;
        public List<YJH.Entities.hr_salary_adj1> salaryList = null;

        public string companyname = null;
        public string deptname = null;
        public string jobname = null;
        #endregion


        #region===构造===
        public UI_hr_salary_adj(YJH.Entities.hr_emploee hr_emploee1, YJH.Entities.hr_emp_salary hr_emp_salary1, string companyname1, string deptname1, string jobname1)
        {
            InitializeComponent();
            hr_emploee = hr_emploee1;
            hr_emp_salary = hr_emp_salary1;
            companyname = companyname1;
            deptname = deptname1;
            jobname = jobname1;
            this.btnNew.Click += BtnNew_Click;
            this.btnSave.Click += BtnSave_Click;
            this.gvSalary.DefaultValuesNeeded += GvSalary_DefaultValuesNeeded;
            this.gvSalary.UserAddedRow += GvSalary_UserAddedRow;
            this.gvSalary.CellEndEdit += GvSalary_CellEndEdit;
            this.gvSalary.CellFormatting += GvSalary_CellFormatting;

            SetCombox();

        }


        #endregion

        #region===方法===
        private void SetCombox()
        {
            this.tbxCompany_id.Text = companyname;
            this.tbxDeptId.Text = deptname;
            this.tbxNumber.Text = hr_emploee.Number;
            this.tbxName.Text = hr_emploee.Name;
            this.tbxJob_id.Text = jobname;

            var InsuranceLevelcolumn = this.gvSalary.Columns["clInsuranceLevel"] as GridViewComboBoxColumn;
            var InsuranceLevellist = (from ot in Enum.GetValues(typeof(YJH.Enums.InsuranceLevel)).Cast<int>()
                                      select new
                                      {
                                          DisplayMember = Enum.GetName(typeof(YJH.Enums.InsuranceLevel), ot),
                                          ValueMember = ot
                                      }).ToList();
            InsuranceLevelcolumn.DisplayMember = "DisplayMember";
            InsuranceLevelcolumn.ValueMember = "ValueMember";
            InsuranceLevelcolumn.DataSource = InsuranceLevellist;

            var FundsLevelcolumn = this.gvSalary.Columns["clFundsLevel"] as GridViewComboBoxColumn;
            var FundsLevellist = (from ot in Enum.GetValues(typeof(YJH.Enums.FundsLevel)).Cast<int>()
                                  select new
                                  {
                                      DisplayMember = Enum.GetName(typeof(YJH.Enums.FundsLevel), ot),
                                      ValueMember = ot
                                  }).ToList();
            FundsLevelcolumn.DisplayMember = "DisplayMember";
            FundsLevelcolumn.ValueMember = "ValueMember";
            FundsLevelcolumn.DataSource = FundsLevellist;

            salaryList = YJH.Services.hr_emploeeService.Gethr_salary_adj1List(hr_emploee.Instance.ID);
            this.gvSalary.DataSource = salaryList;


        }

        #endregion

        #region===事件===

        private void BtnSave_Click(object sender, EventArgs e)
        {
            foreach (YJH.Entities.hr_salary_adj1 item in salaryList)
            {
                if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                {
                    item.Company_id = hr_emploee.Company_id;
                    item.DeptId = hr_emploee.Dept_id;
                    item.EmploeeId = hr_emploee.Instance.ID;
                    item.Job_id = hr_emploee.Job_id;
                }
            }
            try
            {
                YJH.Services.hr_emploeeService.Savehr_salary_adj1(salaryList);
                foreach (YJH.Entities.hr_salary_adj1 item in salaryList)
                {
                    item.Instance.AcceptChanges();
                }
                RadMessageBox.Show("保存成功！", "提示");
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("保存出错，错误原因：" + ex.Message, "提示");
            }
        }

        private void GvSalary_DefaultValuesNeeded(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells["clcreater1"].Value = Globle.CurrentEmployee.Base.Name;
            e.Row.Cells["clcreater"].Value = Globle.CurrentEmployee.Instance.ID;
            e.Row.Cells["clcreate_date"].Value = System.DateTime.Now;
        }

        private void GvSalary_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (this.gvSalary.Rows.Count > 0)
            {
                foreach (var v in this.gvSalary.Rows)
                {
                    v.Cells["clcreater1"].Value = (new sys.Entities.Emploee(dps.Common.Data.Entity.Retrieve(sys.Entities.Emploee.EntityModelID, (Guid)v.Cells["clcreater"].Value))).Base.Name;
                }
            }
        }

        private void GvSalary_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (this.gvSalary.Rows.Count > 0)
            {
                if (e.Column.Name == "clDateEffect")
                {
                    GridViewRowInfo row = gvSalary.Rows.First();
                    if ((DateTime)e.Row.Cells["clDateEffect"].Value < (DateTime)row.Cells["clDateEffect"].Value)
                    {
                        RadMessageBox.Show("生效日期必须在"+((DateTime)row.Cells["clDateEffect"].Value).ToShortDateString()+"之后", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                }
            }
        }

        private void GvSalary_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            this.gvSalary.AllowAddNewRow = false;
            this.gvSalary.AllowEditRow = false;
            this.gvSalary.ReadOnly = true;
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            this.gvSalary.AllowAddNewRow = true;
            this.gvSalary.AllowEditRow = true;
            this.gvSalary.ReadOnly = false;
        }

        #endregion

    }
}
