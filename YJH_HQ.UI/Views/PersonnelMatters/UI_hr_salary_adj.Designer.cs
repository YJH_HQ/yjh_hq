﻿namespace YJH_HQ.UI.PersonnelMatters
{
    partial class UI_hr_salary_adj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn6 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn7 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSave = new Telerik.WinControls.UI.CommandBarButton();
            this.gvSalary = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxJob_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxDeptId = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxCompany_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSalary.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxJob_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxDeptId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1224, 30);
            this.radCommandBar1.TabIndex = 3;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnDelete,
            this.btnSave});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新建";
            this.btnNew.AccessibleName = "新建";
            this.btnNew.DisplayName = "commandBarButton1";
            this.btnNew.DrawText = true;
            this.btnNew.Image = null;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton2";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = null;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleDescription = "保存";
            this.btnSave.AccessibleName = "保存";
            this.btnSave.DisplayName = "commandBarButton1";
            this.btnSave.DrawText = true;
            this.btnSave.Image = null;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "保存";
            this.btnSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // gvSalary
            // 
            this.gvSalary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvSalary.Location = new System.Drawing.Point(0, 86);
            // 
            // gvSalary
            // 
            this.gvSalary.MasterTemplate.AllowAddNewRow = false;
            this.gvSalary.MasterTemplate.AllowDeleteRow = false;
            this.gvSalary.MasterTemplate.AllowEditRow = false;
            this.gvSalary.MasterTemplate.AutoGenerateColumns = false;
            gridViewDecimalColumn1.FieldName = "wageBasic";
            gridViewDecimalColumn1.HeaderText = "现基本薪资";
            gridViewDecimalColumn1.Name = "clwageBasic";
            gridViewDecimalColumn1.Width = 80;
            gridViewDecimalColumn2.FieldName = "wageKPIPer";
            gridViewDecimalColumn2.HeaderText = "现绩效薪资占比";
            gridViewDecimalColumn2.Name = "clwageKPIPer";
            gridViewDecimalColumn2.Width = 100;
            gridViewDecimalColumn3.FieldName = "sub_comm";
            gridViewDecimalColumn3.HeaderText = "通讯补贴";
            gridViewDecimalColumn3.Name = "clsub_comm";
            gridViewDecimalColumn3.Width = 80;
            gridViewDecimalColumn4.FieldName = "sub_traffic";
            gridViewDecimalColumn4.HeaderText = "交通补贴";
            gridViewDecimalColumn4.Name = "clsub_traffic";
            gridViewDecimalColumn4.Width = 80;
            gridViewDecimalColumn5.FieldName = "sub_meals";
            gridViewDecimalColumn5.HeaderText = "餐费补贴";
            gridViewDecimalColumn5.Name = "clsub_meals";
            gridViewDecimalColumn5.Width = 80;
            gridViewDecimalColumn6.FieldName = "sub_house";
            gridViewDecimalColumn6.HeaderText = "房屋补贴";
            gridViewDecimalColumn6.Name = "clsub_house";
            gridViewDecimalColumn6.Width = 80;
            gridViewDecimalColumn7.FieldName = "sub_vehicle";
            gridViewDecimalColumn7.HeaderText = "车辆补贴";
            gridViewDecimalColumn7.Name = "clsub_vehicle";
            gridViewDecimalColumn7.Width = 80;
            gridViewComboBoxColumn1.FieldName = "InsuranceLevel";
            gridViewComboBoxColumn1.HeaderText = "现社保基数级别";
            gridViewComboBoxColumn1.Name = "clInsuranceLevel";
            gridViewComboBoxColumn1.Width = 100;
            gridViewComboBoxColumn2.FieldName = "FundsLevel";
            gridViewComboBoxColumn2.HeaderText = "现公积金基数级别";
            gridViewComboBoxColumn2.Name = "clFundsLevel";
            gridViewComboBoxColumn2.Width = 120;
            gridViewDateTimeColumn1.FieldName = "DateEffect";
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn1.HeaderText = "生效日期";
            gridViewDateTimeColumn1.Name = "clDateEffect";
            gridViewDateTimeColumn1.Width = 120;
            gridViewTextBoxColumn1.HeaderText = "创建人";
            gridViewTextBoxColumn1.Name = "clcreater1";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewDateTimeColumn2.FieldName = "create_date";
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn2.HeaderText = "创建时间";
            gridViewDateTimeColumn2.Name = "clcreate_date";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.Width = 120;
            gridViewTextBoxColumn2.FieldName = "Remark";
            gridViewTextBoxColumn2.HeaderText = "备注";
            gridViewTextBoxColumn2.Name = "clRemark";
            gridViewTextBoxColumn2.Width = 120;
            gridViewTextBoxColumn3.FieldName = "creater";
            gridViewTextBoxColumn3.HeaderText = "creater";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "clcreater";
            gridViewTextBoxColumn3.Width = 80;
            this.gvSalary.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewDecimalColumn4,
            gridViewDecimalColumn5,
            gridViewDecimalColumn6,
            gridViewDecimalColumn7,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.gvSalary.Name = "gvSalary";
            this.gvSalary.ReadOnly = true;
            this.gvSalary.ShowGroupPanel = false;
            this.gvSalary.Size = new System.Drawing.Size(1224, 424);
            this.gvSalary.TabIndex = 4;
            this.gvSalary.Text = "radGridView1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxJob_id);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxNumber);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxDeptId);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.tbxCompany_id);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1224, 56);
            this.radGroupBox1.TabIndex = 5;
            // 
            // tbxJob_id
            // 
            this.tbxJob_id.Enabled = false;
            this.tbxJob_id.Location = new System.Drawing.Point(794, 21);
            this.tbxJob_id.Name = "tbxJob_id";
            this.tbxJob_id.Size = new System.Drawing.Size(101, 20);
            this.tbxJob_id.TabIndex = 56;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(754, 21);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(42, 18);
            this.radLabel5.TabIndex = 55;
            this.radLabel5.Text = "职务：";
            // 
            // tbxName
            // 
            this.tbxName.Enabled = false;
            this.tbxName.Location = new System.Drawing.Point(649, 21);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(101, 20);
            this.tbxName.TabIndex = 56;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(608, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 55;
            this.radLabel4.Text = "姓名：";
            // 
            // tbxNumber
            // 
            this.tbxNumber.Enabled = false;
            this.tbxNumber.Location = new System.Drawing.Point(503, 21);
            this.tbxNumber.Name = "tbxNumber";
            this.tbxNumber.Size = new System.Drawing.Size(101, 20);
            this.tbxNumber.TabIndex = 56;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(458, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 18);
            this.radLabel3.TabIndex = 55;
            this.radLabel3.Text = "工号：";
            // 
            // tbxDeptId
            // 
            this.tbxDeptId.Enabled = false;
            this.tbxDeptId.Location = new System.Drawing.Point(349, 21);
            this.tbxDeptId.Name = "tbxDeptId";
            this.tbxDeptId.Size = new System.Drawing.Size(101, 20);
            this.tbxDeptId.TabIndex = 56;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(286, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 55;
            this.radLabel1.Text = "所属部门：";
            // 
            // tbxCompany_id
            // 
            this.tbxCompany_id.Enabled = false;
            this.tbxCompany_id.Location = new System.Drawing.Point(82, 21);
            this.tbxCompany_id.Name = "tbxCompany_id";
            this.tbxCompany_id.Size = new System.Drawing.Size(198, 20);
            this.tbxCompany_id.TabIndex = 56;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(18, 21);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 55;
            this.radLabel2.Text = "所属公司：";
            // 
            // UI_hr_salary_adj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1224, 510);
            this.Controls.Add(this.gvSalary);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UI_hr_salary_adj";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "调薪";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSalary.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxJob_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxDeptId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private Telerik.WinControls.UI.CommandBarButton btnSave;
        private Telerik.WinControls.UI.RadGridView gvSalary;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbxJob_id;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxNumber;
        private Telerik.WinControls.UI.RadTextBox tbxDeptId;
        private Telerik.WinControls.UI.RadTextBox tbxCompany_id;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
    }
}