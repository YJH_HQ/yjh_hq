﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using YJH_HQ.UI.Properties;
using Telerik.WinControls;
using YJH_HQ.UI.Services;

namespace YJH_HQ.UI.Views.PersonnelMatters
{
    public partial class UC_hr_emploee : UserControl
    {
        #region===属性===
        private YJH.Entities.hr_emploee _entity;
        public YJH.Entities.hr_emploee Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData(null);
            }
        }

        private YJH.Entities.hr_emp_salary _entitySalary;
        public YJH.Entities.hr_emp_salary EntitySalary
        {
            get
            {
                return _entitySalary;
            }
            set
            {
                _entitySalary = value;
            }
        }

        //RadTreeNodeCollection nc =null;

        #endregion

        #region===构造===
        public UC_hr_emploee()
        {
            InitializeComponent();

            //this.ddlCompany_id.TextChanged += ddlCompany_id_TextChanged;
            this.btnNew.Click += btnNew_Click;
            this.btnSave.Click += btnSave_Click;
            this.btnSalary.Click += BtnSalary_Click;
            this.btnJob.Click += BtnJob_Click;
            this.btnSearch.Click += btnSearch_Click;
            this.tvTree.DoubleClick += tvTree_DoubleClick;
            this.tvTree.Click += tvTree_Click;
            this.ddlCondition.SelectedIndexChanged += ddlCondition_SelectedIndexChanged;
            this.ddlStatus.SelectedIndexChanged += DdlStatus_SelectedIndexChanged;

            SetCombox();
        }

        #endregion

        #region===方法===
        private void SetCombox()
        {
            this.New();

            //绑定分公司
            var buList = YJH.Services.hr_emploeeService.GetBu();
            this.ddlCompany_id.DisplayMember = "Name";
            this.ddlCompany_id.ValueMember = "ID";
            this.ddlCompany_id.DataSource = buList;
            this.ddlCompany_id.SelectedIndex = -1;

            //绑定职务
            var hr_jobList = YJH.Services.hr_emploeeService.Gethr_Job1();
            this.ddlJob_id.DisplayMember = "Name";
            this.ddlJob_id.ValueMember = "ID";
            this.ddlJob_id.DataSource = hr_jobList;

            //加载树的顶节点
            sys.Entities.OrgUnit ou = YJH.Services.hr_emploeeService.GetTreeFirstNode(Globle.CurrentBusinessUnit.Instance.ID);
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Tag = ou;
            node.Value = ou.Instance.ID;
            node.Name = "BU";
            node.Text = ou.Name.ToString();
            node.Image = YJH_HQ.UI.Properties.Resources.Home16;
            this.tvTree.Nodes.Add(node);
        }

        private void BindData(Telerik.WinControls.UI.RadTreeNode node)
        {
            try
            {
                if (node == null)
                    return;
                if (!string.IsNullOrWhiteSpace(node.Name))
                {
                    if (node.Name == "BU")
                    {
                        //if (Entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached && Entity.Instance.PersistentState != dps.Common.Data.PersistentState.Modified)
                        //{
                        //    sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)this.tvTree.SelectedNode.Tag;
                        //    Entity.Company_id = ou.BaseID;
                        //    this.tbxCompany_id.Text = ou.Name.ToString();
                        //    var DeptList = YJH.Services.hr_emploeeService.GetDept(ou.BaseID);
                        //    this.ddlDept_id.DisplayMember = "Name";
                        //    this.ddlDept_id.ValueMember = "BaseID";
                        //    this.ddlDept_id.DataSource = DeptList;
                        //    this.ddlDept_id.SelectedIndex = -1;
                        //}
                        //else
                        //{
                        this.New();
                        //}
                    }
                    else {
                        this.tbxCompany_id.Text = "";
                        this.ddlDept_id.Text = "";
                        this.ddlDept_id.DataSource = null;
                        //RadMessageBox.Show("请选择分公司", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return;
                    }
                }
                else
                {
                    #region =====BindData=======
                    _entity = (YJH.Entities.hr_emploee)node.Tag;

                    #region ===控制任职状态====
                    if (_entity.Status == 10)
                    {
                        this.dtjobJoinDate.Enabled = true;
                        if (this._entity.jobJoinDate.HasValue)
                            this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                        else
                            this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobTrialDate.Enabled = true;
                        if (this._entity.jobTrialDate.HasValue)
                            this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                        else
                            this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobFormalDate.Enabled = false;
                        this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveApplyDate.Enabled = false;
                        this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveDate.Enabled = false;
                        this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                        this.ddljobLeaveClass.Text = "";
                        this.ddljobLeaveClass.Enabled = false;
                        this.ddljobLeaveType.Text = "";
                        this.ddljobLeaveType.Enabled = false;
                        this.tbxjobLeaveReason.Text = "";
                        this.tbxjobLeaveReason.Enabled = false;

                    }
                    else if (_entity.Status == 20)
                    {
                        this.dtjobJoinDate.Enabled = false;
                        if (this._entity.jobJoinDate.HasValue)
                            this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                        this.dtjobTrialDate.Enabled = false;
                        if (this._entity.jobTrialDate.HasValue)
                            this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                        this.dtjobFormalDate.Enabled = true;
                        if (this._entity.jobFormalDate.HasValue)
                            this.dtjobFormalDate.Value = (DateTime)this._entity.jobFormalDate;
                        else
                            this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveApplyDate.Enabled = false;
                        this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveDate.Enabled = false;
                        this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                        this.ddljobLeaveClass.Text = "";
                        this.ddljobLeaveClass.Enabled = false;
                        this.ddljobLeaveType.Text = "";
                        this.ddljobLeaveType.Enabled = false;
                        this.tbxjobLeaveReason.Text = "";
                        this.tbxjobLeaveReason.Enabled = false;

                    }
                    else if (_entity.Status == 30)
                    {
                        this.dtjobJoinDate.Enabled = false;
                        if (this._entity.jobJoinDate.HasValue)
                            this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                        else
                            this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobTrialDate.Enabled = false;
                        if (this._entity.jobTrialDate.HasValue)
                            this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                        else
                            this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobFormalDate.Enabled = false;
                        if (this._entity.jobFormalDate.HasValue)
                            this.dtjobFormalDate.Value = (DateTime)this._entity.jobFormalDate;
                        else
                            this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveApplyDate.Enabled = true;
                        if (this._entity.jobLeaveApplyDate.HasValue)
                            this.dtjobLeaveApplyDate.Value = (DateTime)this._entity.jobLeaveApplyDate;
                        else
                            this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveDate.Enabled = true;
                        if (this._entity.jobLeaveDate.HasValue)
                            this.dtjobLeaveDate.Value = (DateTime)this._entity.jobLeaveDate;
                        else
                            this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                        this.ddljobLeaveClass.Text = "";
                        this.ddljobLeaveClass.Enabled = true;
                        this.ddljobLeaveType.Text = "";
                        this.ddljobLeaveType.Enabled = true;
                        this.tbxjobLeaveReason.Text = "";
                        this.tbxjobLeaveReason.Enabled = true;
                    }
                    else
                    {
                        this.dtjobJoinDate.Enabled = false;
                        this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobTrialDate.Enabled = false;
                        this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobFormalDate.Enabled = false;
                        this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveApplyDate.Enabled = false;
                        this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                        this.dtjobLeaveDate.Enabled = false;
                        this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                        this.ddljobLeaveClass.Text = "";
                        this.ddljobLeaveClass.Enabled = false;
                        this.ddljobLeaveType.Text = "";
                        this.ddljobLeaveType.Enabled = false;
                        this.tbxjobLeaveReason.Text = "";
                        this.tbxjobLeaveReason.Enabled = false;
                    }
                    #endregion

                    this.ddlDept_id.Enabled = false;
                    this.ddlJob_id.Enabled = false;
                    this.radGroupBox7.Enabled = false;
                    this.radGroupBox8.Enabled = false;
                    tbxNumber.Enabled = false;
                    tbxName.Enabled = false;
                    tbxIdentityId.Enabled = false;
                    //绑定hr_emploee
                    Bind_hr_emploee();
                    //绑定hr_emploee_salary
                    Bind_hr_emploee_salary();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "绑定失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }

        }

        //绑定hr_emploee
        private void Bind_hr_emploee()
        {
            sys.Entities.BusinessUnit bu = new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, _entity.Company_id));
            //this.ddlCompany_id.SelectedValue = bu.ID;
            this.tbxCompany_id.Text = bu.Name;
            //绑定部门
            var DeptList = YJH.Services.hr_emploeeService.GetDept(bu.Instance.ID);
            this.ddlDept_id.DisplayMember = "Name";
            this.ddlDept_id.ValueMember = "BaseID";
            this.ddlDept_id.DataSource = DeptList;
            this.ddlDept_id.SelectedIndex = -1;

            this.ddlDept_id.SelectedValue = (new sys.Entities.WorkGroup((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.WorkGroup.EntityModelID, _entity.Dept_id))).Instance.ID;
            if (_entity.Job_id.HasValue)
            {
                YJH.Entities.hr_job1 job = new YJH.Entities.hr_job1((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(YJH.Entities.hr_job1.EntityModelID, (Guid)_entity.Job_id));
                this.ddlJob_id.SelectedItem = job;
                this.ddlJob_id.Text = job.Name;
            }
            if (_entity.Type == 10)
            {
                this.ddlType.Text = "正式工";
            }
            else if (_entity.Type == 20)
            {
                this.ddlType.Text = "实习生";
            }
            else if (_entity.Type == 30)
            {
                this.ddlType.Text = "见习生";
            }
            else if (_entity.Type == 40)
            {
                this.ddlType.Text = "兼职";
            }
            else
            {
                this.ddlType.Text = "临时";
            }

            if (_entity.Status == 10)
            {
                this.ddlStatus.Text = "试用";
            }
            else if (_entity.Status == 20)
            {
                this.ddlStatus.Text = "正式";
            }
            else if (_entity.Status == 30)
            {
                this.ddlStatus.Text = "离职";
            }
            else
            {
                this.ddlStatus.Text = "其它";
            }
            this.tbxNumber.Text = _entity.Number;
            this.tbxName.Text = _entity.Name;
            if (_entity.Sex == 0)
            {
                this.ddlSex.Text = "男";
            }
            else if (_entity.Sex == 1)
            {
                this.ddlSex.Text = "女";
            }
            this.tbxTel.Text = _entity.Tel;
            if (this._entity.jobFirstDate.HasValue)
            {
                this.dtjobFirstDate.Value = (DateTime)this._entity.jobFirstDate;
            }
            this.tbxIdentityId.Text = _entity.IdentityId;
            if (this._entity.Polity == 10)
            {
                this.ddlPolity.Text = "群众";
            }
            else if (this._entity.Polity == 20)
            {
                this.ddlPolity.Text = "团员";
            }
            else if (this._entity.Polity == 30)
            {
                this.ddlPolity.Text = "党员";
            }
            else if (this._entity.Polity == 40)
            {
                this.ddlPolity.Text = "其它";
            }

            if (this._entity.Degree == 10)
            {
                this.ddlDegree.Text = "大专以下";
            }
            else if (this._entity.Degree == 20)
            {
                this.ddlDegree.Text = "大专";
            }
            else if (this._entity.Degree == 30)
            {
                this.ddlDegree.Text = "本科";
            }
            else if (this._entity.Degree == 40)
            {
                this.ddlDegree.Text = "研究生";
            }
            else if (this._entity.Degree == 50)
            {
                this.ddlDegree.Text = "硕士及以上";
            }
            this.tbxSchool.Text = this._entity.School;
            this.tbxNative.Text = this._entity.Native;
            if (this._entity.NativeType == 10)
            {
                this.ddlNativeType.Text = "城镇";
            }
            else if (this._entity.NativeType == 20)
            {
                this.ddlNativeType.Text = "农村";
            }
            else if (this._entity.NativeType == 30)
            {
                this.ddlNativeType.Text = "其它";
            }
            this.tbxaddrNative.Text = this._entity.addrNative;
            this.tbxaddrLive.Text = this._entity.addrLive;
            this.tbxremarkOfFamily.Text = this._entity.RemarkOfFamily;
            this.tbxProfessional.Text = this._entity.Professional;

            this.tbxcreater.Text = (new sys.Entities.Emploee((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.Emploee.EntityModelID, this._entity.creater))).Base.Name;
            this.tbxcreate_date.Text = this._entity.create_date.ToString("yyyy-MM-dd HH:mm");

            if (this._entity.jobJoinDate.HasValue)
            {
                this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
            }
            if (this._entity.jobTrialDate.HasValue)
            {
                this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
            }
            if (this._entity.jobFormalDate.HasValue)
            {
                this.dtjobFormalDate.Value = (DateTime)this._entity.jobFormalDate;
            }
            if (this._entity.jobLeaveApplyDate.HasValue)
            {
                this.dtjobLeaveApplyDate.Value = (DateTime)this._entity.jobLeaveApplyDate;
            }
            if (this._entity.jobLeaveDate.HasValue)
            {
                this.dtjobLeaveDate.Value = (DateTime)this._entity.jobLeaveDate;
            }

            if (this._entity.jobLeaveClass == 10)
            {
                this.ddljobLeaveClass.Text = "个人因素";
            }
            else if (this._entity.jobLeaveClass == 20)
            {
                this.ddljobLeaveClass.Text = "组织管理";
            }
            else if (this._entity.jobLeaveClass == 30)
            {
                this.ddljobLeaveClass.Text = "工人因素";
            }
            else if (this._entity.jobLeaveClass == 40)
            {
                this.ddljobLeaveClass.Text = "薪资福利";
            }
            else if (this._entity.jobLeaveClass == 50)
            {
                this.ddljobLeaveClass.Text = "其它";
            }

            if (this._entity.jobLeaveType == 10)
            {
                this.ddljobLeaveType.Text = "辞职";
            }
            else if (this._entity.jobLeaveType == 20)
            {
                this.ddljobLeaveType.Text = "职工提出解除劳动合同";
            }
            else if (this._entity.jobLeaveType == 30)
            {
                this.ddljobLeaveType.Text = "单位开除并解除劳动合同";
            }
            else if (this._entity.jobLeaveType == 40)
            {
                this.ddljobLeaveType.Text = "合同到期不续签";
            }

            this.tbxjobLeaveReason.Text = this._entity.jobLeaveReason;
        }

        //绑定hr_emploee_salary
        private void Bind_hr_emploee_salary()
        {
            _entitySalary = YJH.Services.hr_emploeeService.Gethr_emploee_salary(this._entity.Instance.ID);
            this.tbxwageProbation.Text = this._entitySalary.wageProbation.ToString();
            if (this._entitySalary.Type == 10)
            {
                this.ddlType1.Text = "月薪";
            }
            else if (this._entitySalary.Type == 20)
            {
                this.ddlType1.Text = "年薪";
            }
            this.tbxwageBasic.Text = this._entitySalary.wageBasic.ToString();
            this.tbxwageKPIPer.Text = this._entitySalary.wageKPIPer.ToString();
            this.cbIsInsurance.Checked = this._entitySalary.IsInsurance;
            this.tbxInsuranceNumber.Text = this._entitySalary.InsuranceNumber;
            if (this._entitySalary.InsuranceLevel == 1)
            {
                this.ddlInsuranceLevel.Text = "2700";
            }
            else if (this._entitySalary.InsuranceLevel == 2)
            {
                this.ddlInsuranceLevel.Text = "3500";
            }
            else if (this._entitySalary.InsuranceLevel == 3)
            {
                this.ddlInsuranceLevel.Text = "5000";
            }
            this.cbIsFunds.Checked = this._entitySalary.IsFunds;
            this.tbxFundsNumber.Text = this._entitySalary.FundsNumber;
            if (this._entitySalary.FundsLevel == 1)
            {
                this.ddlFundsLevel.Text = "1800";
            }
            else if (this._entitySalary.FundsLevel == 2)
            {
                this.ddlFundsLevel.Text = "3500";
            }
            else if (this._entitySalary.FundsLevel == 3)
            {
                this.ddlFundsLevel.Text = "5000";
            }
            this.tbxsub_comm.Text = this._entitySalary.sub_comm.ToString();
            this.tbxsub_traffic.Text = this._entitySalary.sub_traffic.ToString();
            this.tbxsub_meals.Text = this._entitySalary.sub_meals.ToString();
            this.tbxsub_house.Text = this._entitySalary.sub_house.ToString();
            this.tbxsub_vehicle.Text = this._entitySalary.sub_vehicle.ToString();
        }

        private void FlushData()
        {
            //填充hr_emploee
            //this._entity.Company_id = (Guid)this.ddlCompany_id.SelectedValue;
            this._entity.Dept_id = (Guid)this.ddlDept_id.SelectedValue;
            if (this.ddlType.Text == "正式工")
            {
                this._entity.Type = 10;
            }
            else if (this.ddlType.Text == "实习生")
            {
                this._entity.Type = 20;
            }
            else if (this.ddlType.Text == "见习生")
            {
                this._entity.Type = 30;
            }
            else if (this.ddlType.Text == "兼职")
            {
                this._entity.Type = 40;
            }
            else
            {
                this._entity.Type = 50;
            }

            if (!string.IsNullOrWhiteSpace(this.ddlJob_id.Text))
            {
                this._entity.Job_id = ((YJH.Entities.hr_job1)this.ddlJob_id.SelectedValue).Instance.ID;
            }

            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                _entity.Status = ConvertStatus(this.ddlStatus.Text);
            }

            this._entity.Number = this.tbxNumber.Text;
            this._entity.Name = this.tbxName.Text;
            if (this.ddlSex.Text == "男")
            {
                this._entity.Sex = 0;
            }
            else if (this.ddlSex.Text == "女")
            {
                this._entity.Sex = 1;
            }

            this._entity.Tel = this.tbxTel.Text;
            if (!string.IsNullOrWhiteSpace(this.dtjobFirstDate.Text) && this.dtjobFirstDate.Value != null)
            {
                this._entity.jobFirstDate = this.dtjobFirstDate.Value;
            }
            this._entity.IdentityId = this.tbxIdentityId.Text;
            if (!string.IsNullOrWhiteSpace(this.ddlPolity.Text))
            {
                if (this.ddlPolity.Text == "群众")
                {
                    this._entity.Polity = 10;
                }
                else if (this.ddlPolity.Text == "团员")
                {
                    this._entity.Polity = 20;
                }
                else if (this.ddlPolity.Text == "党员")
                {
                    this._entity.Polity = 30;
                }
                else
                {
                    this._entity.Polity = 40;
                }
            }
            if (!string.IsNullOrWhiteSpace(this.ddlDegree.Text))
            {
                if (this.ddlDegree.Text == "大专以下")
                {
                    this._entity.Degree = 10;
                }
                else if (this.ddlDegree.Text == "大专")
                {
                    this._entity.Degree = 20;
                }
                else if (this.ddlDegree.Text == "本科")
                {
                    this._entity.Degree = 30;
                }
                else if (this.ddlDegree.Text == "研究生")
                {
                    this._entity.Degree = 40;
                }
                else
                {
                    this._entity.Degree = 50;
                }
            }
            this._entity.School = this.tbxSchool.Text;
            this._entity.Native = this.tbxNative.Text;
            if (!string.IsNullOrWhiteSpace(this.ddlNativeType.Text))
            {
                if (this.ddlNativeType.Text == "城镇")
                {
                    this._entity.NativeType = 10;
                }
                else if (this.ddlNativeType.Text == "农村")
                {
                    this._entity.NativeType = 20;
                }
                else
                {
                    this._entity.NativeType = 30;
                }
            }
            this._entity.addrNative = this.tbxaddrNative.Text;
            this._entity.addrLive = this.tbxaddrLive.Text;
            this._entity.Professional = this.tbxProfessional.Text;
            if (!string.IsNullOrWhiteSpace(this.dtjobJoinDate.Text) && this.dtjobJoinDate.Value != null)
            {
                this._entity.jobJoinDate = this.dtjobJoinDate.Value;
            }
            if (!string.IsNullOrWhiteSpace(this.dtjobTrialDate.Text) && this.dtjobTrialDate.Value != null)
            {
                this._entity.jobTrialDate = this.dtjobTrialDate.Value;
            }
            if (!string.IsNullOrWhiteSpace(this.dtjobFormalDate.Text) && this.dtjobFormalDate.Value != null)
            {
                this._entity.jobFormalDate = this.dtjobFormalDate.Value;
            }
            if (!string.IsNullOrWhiteSpace(this.dtjobLeaveApplyDate.Text) && this.dtjobLeaveApplyDate.Value != null)
            {
                this._entity.jobLeaveApplyDate = this.dtjobLeaveApplyDate.Value;
            }
            if (!string.IsNullOrWhiteSpace(this.dtjobLeaveDate.Text) && this.dtjobLeaveDate.Value != null)
            {
                this._entity.jobLeaveDate = this.dtjobLeaveDate.Value;
            }

            if (!string.IsNullOrWhiteSpace(this.ddljobLeaveClass.Text))
            {
                if (this.ddljobLeaveClass.Text == "个人因素")
                {
                    this._entity.jobLeaveClass = 10;
                }
                else if (this.ddljobLeaveClass.Text == "组织管理")
                {
                    this._entity.jobLeaveClass = 20;
                }
                else if (this.ddljobLeaveClass.Text == "工人因素")
                {
                    this._entity.jobLeaveClass = 30;
                }
                else if (this.ddljobLeaveClass.Text == "薪资福利")
                {
                    this._entity.jobLeaveClass = 40;
                }
                else
                {
                    this._entity.jobLeaveClass = 50;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ddljobLeaveType.Text))
            {
                if (this.ddljobLeaveType.Text == "辞职")
                {
                    this._entity.jobLeaveType = 10;
                }
                else if (this.ddljobLeaveType.Text == "职工提出解除劳动合同")
                {
                    this._entity.jobLeaveType = 20;
                }
                else if (this.ddljobLeaveType.Text == "单位开除并解除劳动合同")
                {
                    this._entity.jobLeaveType = 30;
                }
                else
                {
                    this._entity.jobLeaveType = 40;
                }
            }

            this._entity.jobLeaveReason = this.tbxjobLeaveReason.Text;

            //填充hr_emploee_salary
            this._entitySalary.wageProbation = Convert.ToDecimal(this.tbxwageProbation.Text);
            if (this.ddlType1.Text == "月薪")
            {
                this._entitySalary.Type = 10;
            }
            else
            {
                this._entitySalary.Type = 20;
            }
            this._entitySalary.wageBasic = Convert.ToDecimal(this.tbxwageBasic.Text);
            this._entitySalary.wageKPIPer = Convert.ToDecimal(this.tbxwageKPIPer.Text);
            this._entitySalary.IsInsurance = this.cbIsInsurance.Checked;
            this._entitySalary.InsuranceNumber = this.tbxInsuranceNumber.Text;
            if (!string.IsNullOrWhiteSpace(this.ddlInsuranceLevel.Text))
            {
                if (this.ddlInsuranceLevel.Text == "2700")
                {
                    this._entitySalary.InsuranceLevel = 1;
                }
                else if (this.ddlInsuranceLevel.Text == "3500")
                {
                    this._entitySalary.InsuranceLevel =2;
                }
                else
                {
                    this._entitySalary.InsuranceLevel = 3;
                }
            }
            this._entitySalary.IsFunds = this.cbIsFunds.Checked;
            this._entitySalary.FundsNumber = this.tbxFundsNumber.Text;

            if (!string.IsNullOrWhiteSpace(this.ddlFundsLevel.Text))
            {
                if (this.ddlFundsLevel.Text == "1800")
                {
                    this._entitySalary.FundsLevel = 1;
                }
                else if (this.ddlFundsLevel.Text == "3500")
                {
                    this._entitySalary.FundsLevel = 2;
                }
                else
                {
                    this._entitySalary.FundsLevel = 3;
                }
            }
            this._entitySalary.sub_comm = Convert.ToDecimal(this.tbxsub_comm.Text);
            this._entitySalary.sub_traffic = Convert.ToDecimal(this.tbxsub_traffic.Text);
            this._entitySalary.sub_meals = Convert.ToDecimal(this.tbxsub_meals.Text);
            this._entitySalary.sub_house = Convert.ToDecimal(this.tbxsub_house.Text);
            this._entitySalary.sub_vehicle = Convert.ToDecimal(this.tbxsub_vehicle.Text);
        }

        private void New()
        {
            Entity = new YJH.Entities.hr_emploee();
            this.tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            Entity.creater = Globle.CurrentEmployee.Instance.ID;
            this.tbxcreate_date.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            Entity.create_date = System.DateTime.Now;
            if (this.tvTree.SelectedNode != null && this.tvTree.SelectedNode.Name == "BU")
            {
                sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)this.tvTree.SelectedNode.Tag;
                Entity.Company_id = ou.BaseID;
                this.tbxCompany_id.Text = ou.Name.ToString();
                //绑定部门
                var DeptList = YJH.Services.hr_emploeeService.GetDept(ou.BaseID);
                this.ddlDept_id.DisplayMember = "Name";
                this.ddlDept_id.ValueMember = "BaseID";
                this.ddlDept_id.DataSource = DeptList;
                this.ddlDept_id.SelectedIndex = -1;
            }
            else {
                this.tbxCompany_id.Text = "";
                this.ddlDept_id.Text = "";
                this.ddlDept_id.DataSource = null;
                return;
            }
            this.ddlDept_id.Text = "";
            this.ddlType.Text = "正式工";
            this.ddlJob_id.Text = "";
            this.ddlJob_id.SelectedItem = null;
            Entity.Status = 10;
            this.ddlStatus.Text = "试用";
            this.tbxNumber.Text = "";
            this.tbxName.Text = "";
            this.ddlSex.Text = "男";
            this.tbxTel.Text = "";
            this.dtjobFirstDate.Value = System.DateTime.Now.Date;
            this.tbxIdentityId.Text = "";
            this.ddlPolity.Text = "群众";
            this.ddlDegree.Text = "大专以下";
            this.tbxSchool.Text = "";
            this.tbxNative.Text = "";
            this.ddlNativeType.Text = "城镇";
            this.tbxaddrNative.Text = "";
            this.tbxaddrLive.Text = "";
            this.tbxremarkOfFamily.Text = "";
            this.tbxProfessional.Text = "";

            this.dtjobJoinDate.Enabled = true;
            this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
            this.dtjobTrialDate.Enabled = true;
            this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
            this.dtjobFormalDate.Enabled = false;
            this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
            this.dtjobLeaveApplyDate.Enabled = false;
            this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
            this.dtjobLeaveDate.Enabled = false;
            this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
            this.ddljobLeaveClass.Text = "";
            this.ddljobLeaveClass.Enabled = false;
            this.ddljobLeaveType.Text = "";
            this.ddljobLeaveType.Enabled = false;
            this.tbxjobLeaveReason.Text = "";
            this.tbxjobLeaveReason.Enabled = false;

            EntitySalary = new YJH.Entities.hr_emp_salary();
            EntitySalary.EmploeeId = Entity.Instance.ID;
            this.tbxwageProbation.Text = "0.00";
            this.ddlType1.Text = "月薪";
            this.tbxwageBasic.Text = "0.00";
            this.tbxwageKPIPer.Text = "0.00";
            this.cbIsInsurance.Checked = true;
            this.tbxInsuranceNumber.Text = "";
            this.ddlInsuranceLevel.Text = "2700";
            this.cbIsFunds.Checked = true;
            this.tbxFundsNumber.Text = "";
            this.ddlFundsLevel.Text = "1800";
            this.tbxsub_comm.Text = "0.00";
            this.tbxsub_traffic.Text = "0.00";
            this.tbxsub_meals.Text = "0.00";
            this.tbxsub_house.Text = "0.00";
            this.tbxsub_vehicle.Text = "0.00";

            this.ddlDept_id.Enabled = true;
            this.ddlJob_id.Enabled = true;
            this.radGroupBox7.Enabled = true;
            this.radGroupBox8.Enabled = true;
            tbxNumber.Enabled = true;
            tbxName.Enabled = true;
            tbxIdentityId.Enabled = true;
        }

        private void Save()
        {
            //if (string.IsNullOrWhiteSpace(this.ddlCompany_id.Text))
            //{
            //    RadMessageBox.Show("请选择分公司", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            //    return;
            //}
            if (string.IsNullOrWhiteSpace(this.tbxCompany_id.Text))
            {
                RadMessageBox.Show("请选择分公司", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.ddlDept_id.Text))
            {
                RadMessageBox.Show("请选择部门", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.ddlType.Text))
            {
                RadMessageBox.Show("请选择用工类型", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.ddlJob_id.Text))
            {
                RadMessageBox.Show("请选择职务", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.tbxNumber.Text))
            {
                RadMessageBox.Show("工号不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.tbxName.Text))
            {
                RadMessageBox.Show("姓名不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(this.ddlSex.Text))
            {
                RadMessageBox.Show("请选择性别", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if(string.IsNullOrWhiteSpace(this.tbxIdentityId.Text))
            {
                RadMessageBox.Show("身份证号不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            try
            {
                this.FlushData();
                var state = _entity.Instance.PersistentState;
                YJH.Services.hr_emploeeService.Save(_entity, _entitySalary);
                _entity.Instance.AcceptChanges();
                _entitySalary.Instance.AcceptChanges();
                this.ddlDept_id.Enabled = false;
                this.ddlJob_id.Enabled = false;
                this.radGroupBox7.Enabled = false;
                this.radGroupBox8.Enabled = false;
                tbxNumber.Enabled = false;
                tbxName.Enabled = false;
                tbxIdentityId.Enabled = false;
                if (state == dps.Common.Data.PersistentState.Detached)
                {
                    Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                    node.Tag = _entity;
                    node.Text = _entity.Name;
                    node.Value = _entity.Instance.ID;
                    node.Name = "";
                    node.Image = YJH_HQ.UI.Properties.Resources.User16;
                    var worknode= this.tvTree.Find(f => (Guid)f.Value == (YJH.Services.hr_emploeeService.GetWorkOU(_entity.Dept_id)).Instance.ID);
                    if (worknode != null)
                    {
                        worknode.Nodes.Add(node);
                        node.Selected = true;
                    }
                }

                RadMessageBox.Show("保存成功!", "提示", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private int ConvertStatus(string text)
        {
            int s=0;
            if (text == "试用")
            {
                s = 10;
            }
            else if (text == "正式")
            {
                s = 20;
            }
            else if (text == "离职")
            {
                s = 30;
            }
            else
            {
                s = 40;
            }
            return s;
        }

        #endregion

        #region===事件===
        private void BtnSalary_Click(object sender, EventArgs e)
        {
            if(_entity.Status!=20)
            {
                RadMessageBox.Show("只有工作状态为[正式]才能调薪", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            YJH_HQ.UI.PersonnelMatters.UI_hr_salary_adj sa = new UI.PersonnelMatters.UI_hr_salary_adj(_entity, _entitySalary,this.tbxCompany_id.Text,this.ddlDept_id.Text,this.ddlJob_id.Text);
            sa.FormClosed += Sa_FormClosed;
            sa.ShowDialog();
        }
        private void Sa_FormClosed(object sender, FormClosedEventArgs e)
        {
            Bind_hr_emploee_salary();
        }

        private void BtnJob_Click(object sender, EventArgs e)
        {
            if (_entity.Status != 20)
            {
                RadMessageBox.Show("只有工作状态为[正式]才能调岗", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            YJH_HQ.UI.PersonnelMatters.UI_hr_job_adj job = new UI.PersonnelMatters.UI_hr_job_adj(_entity, this.tbxCompany_id.Text);
            job.FormClosed += Job_FormClosed;
            job.ShowDialog();
        }

        private void Job_FormClosed(object sender, FormClosedEventArgs e)
        {
            _entity = new YJH.Entities.hr_emploee((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(YJH.Entities.hr_emploee.EntityModelID, _entity.Instance.ID));
            Bind_hr_emploee();
        }

        private void DdlStatus_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (_entity.Status>ConvertStatus(this.ddlStatus.Text))
            {
                if (_entity.Status == 10)
                {
                    RadMessageBox.Show("只能选择工作状态为试用/正式/离职/其它", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    this.ddlStatus.Text = "试用";
                }
                else if (_entity.Status == 20)
                {
                    RadMessageBox.Show("只能选择工作状态为正式/离职/其它", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    this.ddlStatus.Text = "正式";
                }
                else if (_entity.Status == 30)
                {
                    RadMessageBox.Show("只能选择工作状态为离职/其它", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    this.ddlStatus.Text = "离职";
                }
                else
                {
                    RadMessageBox.Show("只能选择工作状态为其它", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    this.ddlStatus.Text = "其它";
                }
                return;
            }

            #region ===控制任职状态====
            if (this.ddlStatus.Text == "试用")
            {
                this.dtjobJoinDate.Enabled = true;
                if (this._entity.jobJoinDate.HasValue)
                    this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                else
                    this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                this.dtjobTrialDate.Enabled = true;
                if (this._entity.jobTrialDate.HasValue)
                    this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                else
                    this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                this.dtjobFormalDate.Enabled = false;
                this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveApplyDate.Enabled = false;
                this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveDate.Enabled = false;
                this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                this.ddljobLeaveClass.Text = "";
                this.ddljobLeaveClass.Enabled = false;
                this.ddljobLeaveType.Text = "";
                this.ddljobLeaveType.Enabled = false;
                this.tbxjobLeaveReason.Text = "";
                this.tbxjobLeaveReason.Enabled = false;

            }
            else if (this.ddlStatus.Text == "正式")
            {
                this.dtjobJoinDate.Enabled = false;
                if (this._entity.jobJoinDate.HasValue)
                    this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                this.dtjobTrialDate.Enabled = false;
                if (this._entity.jobTrialDate.HasValue)
                    this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                this.dtjobFormalDate.Enabled = true;
                if (this._entity.jobFormalDate.HasValue)
                    this.dtjobFormalDate.Value = (DateTime)this._entity.jobFormalDate;
                else
                    this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveApplyDate.Enabled = false;
                this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveDate.Enabled = false;
                this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                this.ddljobLeaveClass.Text = "";
                this.ddljobLeaveClass.Enabled = false;
                this.ddljobLeaveType.Text = "";
                this.ddljobLeaveType.Enabled = false;
                this.tbxjobLeaveReason.Text = "";
                this.tbxjobLeaveReason.Enabled = false;

            }
            else if (this.ddlStatus.Text == "离职")
            {
                this.dtjobJoinDate.Enabled = false;
                if (this._entity.jobJoinDate.HasValue)
                    this.dtjobJoinDate.Value = (DateTime)this._entity.jobJoinDate;
                else
                    this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                this.dtjobTrialDate.Enabled = false;
                if (this._entity.jobTrialDate.HasValue)
                    this.dtjobTrialDate.Value = (DateTime)this._entity.jobTrialDate;
                else
                    this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                this.dtjobFormalDate.Enabled = false;
                if (this._entity.jobFormalDate.HasValue)
                    this.dtjobFormalDate.Value = (DateTime)this._entity.jobFormalDate;
                else
                    this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveApplyDate.Enabled = true;
                if (this._entity.jobLeaveApplyDate.HasValue)
                    this.dtjobLeaveApplyDate.Value = (DateTime)this._entity.jobLeaveApplyDate;
                else
                    this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveDate.Enabled = true;
                if (this._entity.jobLeaveDate.HasValue)
                    this.dtjobLeaveDate.Value = (DateTime)this._entity.jobLeaveDate;
                else
                    this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                this.ddljobLeaveClass.Text = "";
                this.ddljobLeaveClass.Enabled = true;
                this.ddljobLeaveType.Text = "";
                this.ddljobLeaveType.Enabled = true;
                this.tbxjobLeaveReason.Text = "";
                this.tbxjobLeaveReason.Enabled = true;
            }
            else
            {
                this.dtjobJoinDate.Enabled = false;
                this.dtjobJoinDate.DateTimePickerElement.SetToNullValue();
                this.dtjobTrialDate.Enabled = false;
                this.dtjobTrialDate.DateTimePickerElement.SetToNullValue();
                this.dtjobFormalDate.Enabled = false;
                this.dtjobFormalDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveApplyDate.Enabled = false;
                this.dtjobLeaveApplyDate.DateTimePickerElement.SetToNullValue();
                this.dtjobLeaveDate.Enabled = false;
                this.dtjobLeaveDate.DateTimePickerElement.SetToNullValue();
                this.ddljobLeaveClass.Text = "";
                this.ddljobLeaveClass.Enabled = false;
                this.ddljobLeaveType.Text = "";
                this.ddljobLeaveType.Enabled = false;
                this.tbxjobLeaveReason.Text = "";
                this.tbxjobLeaveReason.Enabled = false;
            }
            #endregion
        }

        void ddlCondition_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.ddlCondition.Text == "无")
            {
                this.tbxInfo.Text = "";
            }
        }

        void tvTree_Click(object sender, EventArgs e)
        {
            if (this.tvTree.SelectedNode != null)
            {
                //if (string.IsNullOrWhiteSpace(this.tvTree.SelectedNode.Name))
                //{
                this.BindData(this.tvTree.SelectedNode);
                //}
            }
        }

        void tvTree_DoubleClick(object sender, EventArgs e)
        {
            if (this.tvTree.SelectedNode != null)
            {
                if (this.tvTree.SelectedNode.Nodes.Count() > 0)
                    return;
                if (!string.IsNullOrWhiteSpace(this.tvTree.SelectedNode.Name))
                {
                    sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)this.tvTree.SelectedNode.Tag;
                    int i = 0;
                    foreach (var o1 in ou.SubItems)
                    {
                        if (o1.BaseType == "sys.WorkGroup" || o1.BaseType == "sys.BusinessUnit")
                        {
                            i++;
                        }
                    }
                    if (i == 0)
                    {
                        List<YJH.Entities.hr_emploee> hrempList = YJH.Services.hr_emploeeService.Gethr_EmploeeList(ou.BaseID);
                        foreach (YJH.Entities.hr_emploee emp in hrempList)
                        {
                            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                            node.Tag = emp;
                            node.Text = emp.Name;
                            node.Value = emp.Instance.ID;
                            node.Name = "";
                            node.Image = YJH_HQ.UI.Properties.Resources.User16;
                            this.tvTree.SelectedNode.Nodes.Add(node);
                        }
                    }
                    else
                    {
                        //foreach (sys.Entities.OrgUnit o in ou.SubItems.OrderByDescending(f => f.BaseType))
                        //{
                        //    if (o.BaseType == "sys.WorkGroup" || o.BaseType == "sys.BusinessUnit")
                        //    {
                        //        if (o.Instance.ID != Guid.Parse("5FFBC61A-9809-42FE-93F5-E7C73948C8F0"))
                        //        {
                        //            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                        //            node.Tag = o;
                        //            node.Text = o.Name.ToString();
                        //            node.Value = o.Instance.ID;

                        //            if (o.BaseType == "sys.WorkGroup")
                        //            {
                        //                node.Name = "Work";
                        //                node.Image = YJH_HQ.UI.Properties.Resources.Group16;
                        //            }
                        //            else
                        //            {
                        //                node.Name = "BU";
                        //                node.Image = YJH_HQ.UI.Properties.Resources.Home16;
                        //            }

                        //            this.tvTree.SelectedNode.Nodes.Add(node);
                        //        }
                        //    }
                        //}
                        System.Collections.Generic.IEnumerable<sys.Entities.OrgUnit> orgWorkList = ou.SubItems.Where(f => f.BaseType == "sys.WorkGroup");
                        foreach (sys.Entities.OrgUnit o in orgWorkList)
                        {
                            if (o.Instance.ID != Guid.Parse("5FFBC61A-9809-42FE-93F5-E7C73948C8F0"))
                            {
                                Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                                node.Tag = o;
                                node.Text = o.Name.ToString();
                                node.Value = o.Instance.ID;
                                node.Name = "Work";
                                node.Image = YJH_HQ.UI.Properties.Resources.Group16;
                                this.tvTree.SelectedNode.Nodes.Add(node);
                            }
                        }
                        System.Collections.Generic.IEnumerable<sys.Entities.OrgUnit> orgBUList = ou.SubItems.Where(f => f.BaseType == "sys.BusinessUnit");
                        foreach (sys.Entities.OrgUnit o in orgBUList.OrderBy(f=>(((sys.Entities.BusinessUnit)f.Base).OrgCode)))
                        {
                            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                            node.Tag = o;
                            node.Text = o.Name.ToString();
                            node.Value = o.Instance.ID;
                            node.Name = "BU";
                            node.Image = YJH_HQ.UI.Properties.Resources.Home16;
                            this.tvTree.SelectedNode.Nodes.Add(node);
                        }

                    }
                }
            }
        }

        //void ddlCompany_id_TextChanged(object sender, EventArgs e)
        //{
        //    //绑定部门
        //    if (!string.IsNullOrWhiteSpace(this.ddlCompany_id.Text))
        //    {
        //        var DeptList = YJH.Services.hr_emploeeService.GetDept((Guid)this.ddlCompany_id.SelectedValue);
        //        this.ddlDept_id.DisplayMember = "Name";
        //        this.ddlDept_id.ValueMember = "BaseID";
        //        this.ddlDept_id.DataSource = DeptList;
        //        this.ddlDept_id.SelectedIndex = -1;
        //    }
        //}

        void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ddlCondition.Text != "无" && string.IsNullOrWhiteSpace(this.tbxInfo.Text))
                {
                    RadMessageBox.Show("请输入要查询的条件", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                //nc = this.tvTree.Nodes;
                this.tvTree.Nodes.Clear();
                if (this.ddlCondition.Text == "工号" || this.ddlCondition.Text == "姓名" || this.ddlCondition.Text == "手机" || this.ddlCondition.Text == "身份证")
                {
                    //加载树的顶节点
                    sys.Entities.OrgUnit ou = YJH.Services.hr_emploeeService.GetTreeFirstNode(Globle.CurrentBusinessUnit.Instance.ID);
                    Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                    node.Tag = ou;
                    node.Value = ou.Instance.ID;
                    node.Name = "BU";
                    node.Text = ou.Name.ToString();
                    node.Image = YJH_HQ.UI.Properties.Resources.Home16;

                    this.tvTree.Nodes.Add(node);
                    node.Selected = true;
                    //加载树的员式子节点
                    List<YJH.Entities.hr_emploee> hrempList = YJH.Services.hr_emploeeService.Gethr_EmploeeList2(this.ddlCondition.Text, this.tbxInfo.Text);
                    foreach (YJH.Entities.hr_emploee emp in hrempList)
                    {
                        Telerik.WinControls.UI.RadTreeNode childnode = new Telerik.WinControls.UI.RadTreeNode();
                        childnode.Tag = emp;
                        childnode.Text = emp.Name;
                        childnode.Value = emp.Instance.ID;
                        childnode.Name = "";
                        childnode.Image = YJH_HQ.UI.Properties.Resources.User16;
                        this.tvTree.SelectedNode.Nodes.Add(childnode);
                    }
                    this.tvTree.ExpandAll();
                }
                else
                {
                    //加载树的顶节点
                    sys.Entities.OrgUnit ou = YJH.Services.hr_emploeeService.GetTreeFirstNode(Globle.CurrentBusinessUnit.Instance.ID);
                    Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                    node.Tag = ou;
                    node.Value = ou.Instance.ID;
                    node.Name = "BU";
                    node.Text = ou.Name.ToString();
                    node.Image = YJH_HQ.UI.Properties.Resources.Home16;
                    this.tvTree.Nodes.Add(node);
                    //this.tvTree.Nodes.AddRange(nc);
                }

            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "查询失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            /*try
            {
                DataTable dt = dt = SqlHelper.TableProcedure("[dbo].[dlemp]");
                foreach (DataRow dr in dt.Rows)
                {
                    _entity = new YJH.Entities.hr_emploee();
                    _entity.Instance.ID = Guid.Parse(dr["ID"].ToString());
                    _entity.Number = dr["user_id"].ToString();
                    _entity.Name = dr["user_name"].ToString();
                    _entity.Sex = 0;
                    _entity.Company_id = Guid.Parse(dr["BUID"].ToString());
                    _entity.Dept_id = Guid.Parse(dr["wID"].ToString());
                    _entity.Type = 10;
                    _entity.Status = 10;
                    _entity.Polity = 10;
                    _entity.Degree = 10;
                    _entity.IdentityId = "待填";
                    _entity.NativeType = 10;
                    _entity.jobLeaveClass = 0;
                    _entity.jobLeaveType = 0;
                    _entity.creater = Guid.Parse("93767E98-FAA4-4D78-BC63-1FF3C9C5D5B7");
                    _entity.create_date = DateTime.Now;
                    _entity.IsCreateAccount = false;
                    _entity.Job_id = Guid.Parse("9703CAD3-28F0-49EB-87FC-58188AA4D3A1");
                    _entitySalary = new YJH.Entities.hr_emp_salary();
                    _entitySalary.EmploeeId = Guid.Parse(dr["ID"].ToString());
                    _entitySalary.Type = 10;
                    _entitySalary.wageProbation = 0;
                    _entitySalary.wageBasic = 0;
                    _entitySalary.wageKPIPer = 0;
                    _entitySalary.IsInsurance = true;
                    _entitySalary.InsuranceLevel = 2700;
                    _entitySalary.IsFunds = true;
                    _entitySalary.FundsLevel = 1800;
                    _entitySalary.sub_comm = 0;
                    _entitySalary.sub_house = 0;
                    _entitySalary.sub_meals = 0;
                    _entitySalary.sub_traffic = 0;
                    _entitySalary.sub_vehicle = 0;
                    YJH.Services.hr_emploeeService.Save(_entity, _entitySalary);
                    _entity.Instance.AcceptChanges();
                    _entitySalary.Instance.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            */
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            if (this.tvTree.SelectedNode == null)
            {
                RadMessageBox.Show("请先选择分公司再新建", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (this.tvTree.SelectedNode.Name == "Work" || string.IsNullOrWhiteSpace(this.tvTree.SelectedNode.Name))
            {
                RadMessageBox.Show("请先选择分公司再新建", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            this.New();
        }

        #endregion

    }
}
