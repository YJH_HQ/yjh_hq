﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH.Entities;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_BillView : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        //临时存放扫街单号
        private string no;
        private YJH.Entities.bm_process_store_bill _entity;
        public YJH.Entities.bm_process_store_bill Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ====构造====

        public UI_BillView()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddlprovince.SelectedIndexChanged += ddlprovince_SelectedIndexChanged;
            this.ddlcity.SelectedIndexChanged += ddlcity_SelectedIndexChanged;
            this.ddlarea.SelectedIndexChanged += ddlarea_SelectedIndexChanged;
            this.ddlexpand_no.SelectedValueChanged += ddlexpand_no_SelectedValueChanged;

            BindControl();
        }

        #endregion

        #region ====方法====
        private void BindControl()
        {
            //省
            List<ba_district> list = YJH.Services.StoreMngService.Getprovince();
            ddlprovince.ValueMember = "ownerid";
            ddlprovince.DisplayMember = "city_name";
            ddlprovince.DataSource = list;
            ddlprovince.SelectedIndex = 9;

            //扫街人员
            this.epexpand_emp.PickerView = new YJH_HQ.Controls.EntityPickerView.EmploeeTreePickerView(Globle.CurrentBusinessUnitOU.Instance.ID);
            this.epexpand_emp.DisplayMember = "Name";

            List<bm_process_store_expand> ex = YJH.Services.BillService.GetExpandList();
            this.ddlexpand_no.ValueMember = "expand_no";
            ddlexpand_no.DisplayMember = "community_name";
            ddlexpand_no.DataSource = ex;

            this.ddlwater_supply.SelectedIndex = 1;
            this.ddlpower_supply.SelectedIndex = 1;
            this.ddlnetwork_supply.SelectedIndex = 1;
            this.ddltel_supply.SelectedIndex = 1;
            this.ddltoilet_supply.SelectedIndex = 1;
            this.ddlage_level.SelectedIndex = 2;
            this.ddlhouse_property.SelectedIndex = 0;
            this.ddlarea_audit.SelectedIndex = 0;
            this.ddlcommunity_type.SelectedIndex = 0;
            this.ddlsize_audit.SelectedIndex = 0;
            this.ddlstructure.SelectedIndex = 0;
            this.ddlstructure_audit.SelectedIndex = 0;
            this.ddlrate_audit.SelectedIndex = 0;
            this.ddlpeople_audit.SelectedIndex = 0;
            this.ddlparking_audit.SelectedIndex = 0;
            this.ddldoor_audit.SelectedIndex = 0;
            this.ddlhousehold_audit.SelectedIndex = 0;
            this.ddlfacade_audit.SelectedIndex = 0;
            
            this.tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            this.tbxcreate_date.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            this.tbxstatus.Text = "新单";
        }

        private void BindData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                return;

            if (_entity.status == 0)
                this.tbxstatus.Text = "新单";
            else if (_entity.status == 1)
                this.tbxstatus.Text = "审核";
            else
                this.tbxstatus.Text = "生效";
            this.tbxexpand_id.Text = _entity.expand_id;
            this.tbxstore_id.Text = _entity.store_id;
            //this.ddlexpand_no.Text = YJH.Services.BillService.GetExpand(_entity.expand_id).community_name;
            YJH.Entities.bm_process_store_expand pand = YJH.Services.BillService.GetExpand(_entity.expand_id);
            this.ddlexpand_no.Text = pand.community_name;
            no = pand.expand_no;

            //拓展人员
            this.epexpand_emp.SelectedEntity = YJH.Services.ExpandService.GetEmploee(_entity.expand_emp);
            this.ddlprovince.Text = _entity.province;
            this.ddlcity.Text = _entity.city;
            this.ddlarea.Text = _entity.area;
            this.ddlstreet.Text = _entity.street;
            this.tbxcommunity_name.Text = _entity.community_name;
            this.tbxaddr.Text = _entity.addr;
            this.tbxproperty_addr.Text = _entity.property_addr;
            this.tbxnear.Text = _entity.near;
            this.tbxproperty_rights.Text = _entity.property_rights;
            this.tbxlessor.Text = _entity.lessor;
            this.tbxcertificate_infor.Text = _entity.certificate_infor;
            this.tbxpeople_type.Text = _entity.people_type;
            this.tbxfloor_household_num.Text = _entity.floor_household_num.ToString();
            if (_entity.age_level == 1)
                this.ddlage_level.Text = "10-20岁";
            else if (_entity.age_level == 2)
                this.ddlage_level.Text = "0-30岁";
            else if (_entity.age_level == 3)
                this.ddlage_level.Text = "30-40岁";
            else if (_entity.age_level == 4)
                this.ddlage_level.Text = "40-50岁";
            else
                this.ddlage_level.Text = "老年人";
            if (_entity.water_supply == 0)
                this.ddlwater_supply.Text = "无";
            else
                this.ddlwater_supply.Text = "有";
            if (_entity.power_supply == 0)
                this.ddlpower_supply.Text = "无";
            else
                this.ddlpower_supply.Text = "有";
            if (_entity.toilet_supply == 0)
                this.ddltoilet_supply.Text = "无";
            else
                this.ddltoilet_supply.Text = "有";
            if (_entity.tel_supply == 0)
                this.ddltel_supply.Text = "无";
            else
                this.ddltel_supply.Text = "有";
            if (_entity.network_supply == 0)
                this.ddlnetwork_supply.Text = "无";
            else
                this.ddlnetwork_supply.Text = "有";
            this.tbxlinkman.Text = _entity.linkman;
            this.tbxlink_tel.Text = _entity.link_tel;
            this.tbxbank_near.Text = _entity.bank_near;
            this.tbxexit_distance.Text = _entity.exit_distance.ToString();
            this.tbxexit_main.Text = _entity.exit_main;
            this.tbxexit_second.Text = _entity.exit_second;
            this.tbxvisual_range.Text = _entity.visual_range;
            this.tbxinfrastructure.Text = _entity.infrastructure;
            if (_entity.house_property == 0)
                this.ddlhouse_property.Text = "门店";
            else if (_entity.house_property == 1)
                this.ddlhouse_property.Text = "社区配套用房";
            else
                this.ddlhouse_property.Text = "移动箱房";
            this.tbxbus_main.Text = _entity.bus_main;
            this.tbxoperating_original.Text = _entity.operating_original;
            this.tbxother_community.Text = _entity.other_community;

            this.tbxfloor_space.Text = _entity.floor_space.ToString();
            this.tbxuse_area.Text = _entity.use_area.ToString();
            this.ddlarea_audit.Text = GetJH(_entity.area_audit);

            this.tbxdoor_width.Text = _entity.door_width.ToString();
            this.tbxdoor_inside_width.Text = _entity.door_inside_width.ToString();
            this.ddlsize_audit.Text = GetJH(_entity.size_audit);
            this.tbxdoor_depth.Text = _entity.door_depth.ToString();
            this.tbxfloor.Text = _entity.floor.ToString();

            this.tbxroom_num.Text = _entity.room_num.ToString();
            if (_entity.community_type == 1)
                this.ddlcommunity_type.Text = "安置房";
            else if (_entity.community_type == 2)
                this.ddlcommunity_type.Text = "老小区";
            else
                this.ddlcommunity_type.Text = "商品房";

            if (_entity.structure == 1)
                this.ddlstructure.Text = "电梯高层";
            else if (_entity.structure == 2)
                this.ddlstructure.Text = "无电梯小高层";
            else
                this.ddlstructure.Text = "平房";
            this.tbxbuilding_num.Text = _entity.building_num.ToString();
            this.tbxbuilding_floor.Text = _entity.building_floor.ToString();
            this.ddlstructure_audit.Text = GetJH(_entity.structure_audit);

            this.tbxoccupancy_rate.Text = _entity.occupancy_rate.ToString();
            this.ddlrate_audit.Text = GetJH(_entity.rate_audit);

            this.tbxhousehold_num.Text = _entity.household_num.ToString();
            this.ddlhousehold_audit.Text = GetJH(_entity.household_audit);

            this.tbxpeople_num.Text = _entity.people_num.ToString();
            this.ddlpeople_audit.Text = GetJH(_entity.people_audit);

            this.tbxparking_num.Text = _entity.parking_num.ToString();
            this.ddlparking_audit.Text = GetJH(_entity.parking_audit);

            this.tbxfacade_length.Text = _entity.facade_length.ToString();
            this.ddlfacade_audit.Text = GetJH(_entity.facade_audit);

            this.tbxdoor_length.Text = _entity.door_length.ToString();
            this.ddldoor_audit.Text = GetJH(_entity.door_audit);

            this.tbxcreater.Text = _entity.creater;
            this.tbxcreate_date.Text = _entity.create_date.ToString("yyyy-MM-dd HH:mm");
            if (_entity.auditer != null)
            {
                this.tbxauditer.Text = _entity.auditer;
                this.tbxaudit_date.Text = _entity.audit_date.Value.ToString("yyyy-MM-dd HH:mm");
            }
            if (_entity.effecter != null)
            {
                this.tbxeffecter.Text = _entity.effecter;
                this.tbxeffecter_date.Text = _entity.effect_date.Value.ToString("yyyy-MM-dd HH:mm");
            }
        }

        private string GetJH(int i)
        {
            if (i == 0)
                return "未稽核";
            else if (i == 1)
                return "稽核通过";
            else
                return "稽核退回";
        }

        private int SetJH(string s)
        {
            if (s == "未稽核")
                return 0;
            else if (s == "稽核通过")
                return 1;
            else
                return 2;
        }

        private void FlushData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                string no = YJH.Services.BillService.GetNO();
                if (!string.IsNullOrWhiteSpace(no))
                {
                    _entity.expand_id = (Convert.ToInt32(no) + 1).ToString().PadLeft(7, '0');
                }
                else
                {
                    _entity.expand_id = "0010001";
                }

                _entity.status = 0;
                _entity.creater = this.tbxcreater.Text;
                _entity.create_date = System.DateTime.Now;
            }

            //拓展人员
            _entity.expand_emp = ((sys.Entities.Emploee)this.epexpand_emp.SelectedEntity).Account;
            _entity.province = this.ddlprovince.Text;
            _entity.city = this.ddlcity.Text;
            _entity.area = this.ddlarea.Text;
            _entity.street = this.ddlstreet.Text;
            _entity.community_name = this.tbxcommunity_name.Text;
            _entity.addr = this.tbxaddr.Text;
            _entity.property_addr = this.tbxproperty_addr.Text;
            _entity.near = this.tbxnear.Text;
            _entity.property_rights = this.tbxproperty_rights.Text;
            _entity.lessor = this.tbxlessor.Text;
            _entity.certificate_infor = this.tbxcertificate_infor.Text;
            _entity.people_type = this.tbxpeople_type.Text;
            _entity.floor_household_num = Convert.ToInt32(this.tbxfloor_household_num.Text);

            if (this.ddlage_level.Text == "10-20岁")
                _entity.age_level = 1;
            else if (this.ddlage_level.Text == "20-30岁")
                _entity.age_level = 2;
            else if (this.ddlage_level.Text == "30-40岁")
                _entity.age_level = 3;
            else if (this.ddlage_level.Text == "40-50岁")
                _entity.age_level = 4;
            else
                _entity.age_level = 5; ;

            if (this.ddlwater_supply.Text == "无")
                _entity.water_supply = 0;
            else
                _entity.water_supply = 1;

            if (this.ddlpower_supply.Text == "无")
                _entity.power_supply = 0;
            else
                _entity.power_supply = 1;

            if (this.ddltoilet_supply.Text == "无")
                _entity.toilet_supply = 0;
            else
                _entity.toilet_supply = 1;

            if (this.ddltel_supply.Text == "无")
                _entity.tel_supply = 0;
            else
                _entity.tel_supply = 1;

            if (this.ddlnetwork_supply.Text == "无")
                _entity.network_supply = 0;
            else
                _entity.network_supply = 1;

            _entity.linkman = this.tbxlinkman.Text;
            _entity.link_tel = this.tbxlink_tel.Text;
            _entity.bank_near = this.tbxbank_near.Text;
            _entity.exit_distance = Convert.ToDecimal(this.tbxexit_distance.Text);
            _entity.exit_main = this.tbxexit_main.Text;
            _entity.exit_second = this.tbxexit_second.Text;
            _entity.visual_range = this.tbxvisual_range.Text;
            _entity.infrastructure = this.tbxinfrastructure.Text;

            if (this.ddlhouse_property.Text == "门店")
                _entity.house_property = 0;
            else if (this.ddlhouse_property.Text == "社区配套用房")
                _entity.house_property = 1;
            else
                _entity.house_property = 2;

            _entity.bus_main = this.tbxbus_main.Text;
            _entity.operating_original = this.tbxoperating_original.Text;
            _entity.other_community = this.tbxother_community.Text;

            _entity.floor_space = Convert.ToDecimal(this.tbxfloor_space.Text);
            _entity.use_area = Convert.ToDecimal(this.tbxuse_area.Text);

            _entity.area_audit = SetJH(this.ddlarea_audit.Text);

            _entity.door_width = Convert.ToDecimal(this.tbxdoor_width.Text);
            _entity.door_inside_width = Convert.ToDecimal(this.tbxdoor_inside_width.Text);
            _entity.size_audit = SetJH(this.ddlsize_audit.Text);
            _entity.door_depth = Convert.ToDecimal(this.tbxdoor_depth.Text);
            _entity.floor = Convert.ToDecimal(this.tbxfloor.Text);

            _entity.room_num = Convert.ToDecimal(this.tbxroom_num.Text);
            if (this.ddlcommunity_type.Text == "安置房")
                _entity.community_type = 1;
            else if (this.ddlcommunity_type.Text == "老小区")
                _entity.community_type = 2;
            else
                _entity.community_type = 3;

            if (this.ddlstructure.Text == "电梯高层")
                _entity.structure = 1;
            else if (this.ddlstructure.Text == "无电梯小高层")
                _entity.structure = 2;
            else
                _entity.structure = 3;

            _entity.building_num = Convert.ToInt32(this.tbxbuilding_num.Text);
            _entity.building_floor = Convert.ToDecimal(this.tbxbuilding_floor.Text);
            _entity.structure_audit = SetJH(this.ddlstructure_audit.Text);

            _entity.occupancy_rate = Convert.ToDecimal(this.tbxoccupancy_rate.Text);
            _entity.rate_audit = SetJH(this.ddlrate_audit.Text);

            _entity.household_num = Convert.ToInt32(this.tbxhousehold_num.Text);
            _entity.household_audit = SetJH(this.ddlhousehold_audit.Text);

            _entity.people_num = Convert.ToInt32(this.tbxpeople_num.Text);
            _entity.people_audit = SetJH(this.ddlpeople_audit.Text);

            _entity.parking_num = Convert.ToInt32(this.tbxparking_num.Text);
            _entity.parking_audit = SetJH(this.ddlparking_audit.Text);

            _entity.facade_length = Convert.ToDecimal(this.tbxfacade_length.Text);
            _entity.facade_audit = SetJH(this.ddlfacade_audit.Text);

            _entity.door_length = Convert.ToDecimal(this.tbxdoor_length.Text);
            _entity.door_audit = SetJH(this.ddldoor_audit.Text);

        }

        private void OnLoad()
        {
            this.LoadPermission();
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.bm_process_store_bill entity)
        {
            sys.Entities.Emploee e = YJH.Services.ExpandService.GetEmploee(_entity.expand_emp);
            YJH.Entities.bm_process_store_expand ex = YJH.Services.BillService.GetExpand(entity.expand_id);
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("expand_id", entity.expand_id);
                data.Add("store_id", entity.store_id);
                data.Add("expand_no", ex.expand_no + " " + ex.community_name);
                data.Add("status", entity.status);
                data.Add("expand_emp", e.Base.Name);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("community_name", entity.community_name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                //data.Add("auditer", entity.auditer);
                //data.Add("audit_date", entity.audit_date);
                //data.Add("effecter", entity.effecter);
                //data.Add("effect_date", entity.effect_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("expand_id", entity.expand_id);
                data.Add("store_id", entity.store_id);
                data.Add("expand_no", ex.expand_no + " " + ex.community_name);
                data.Add("status", entity.status);
                data.Add("expand_emp", e.Base.Name);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("community_name", entity.community_name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                //data.Add("auditer", entity.auditer);
                //data.Add("audit_date", entity.audit_date);
                //data.Add("effecter", entity.effecter);
                //data.Add("effect_date", entity.effect_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        void ddlexpand_no_SelectedValueChanged(object sender, EventArgs e)
        {
            YJH.Entities.bm_process_store_expand ex = YJH.Services.BillService.GetExpandNo(this.ddlexpand_no.SelectedValue.ToString());
            this.ddlprovince.Text = ex.province;
            this.ddlcity.Text = ex.city;
            this.ddlarea.Text = ex.area;
            this.ddlstreet.Text = ex.street;
            this.tbxcommunity_name.Text = ex.community_name;
            this.tbxlinkman.Text = ex.owner;
            this.tbxlink_tel.Text = ex.landlord_tel;
            this.tbxaddr.Text = ex.addr;
            if (ex.room_property == 0)
                this.ddlhouse_property.Text = "门店";
            else if (ex.room_property == 1)
                this.ddlhouse_property.Text = "社区配套用房";
            else
                this.ddlhouse_property.Text = "移动箱房";

        }

        private void Save()
        {
            try
            {
                if (this.tbxstatus.Text != "新单")
                    return;
                if (Validate())
                {
                    this.FlushData();
                    //记录保存前的实体状态
                    var entityState = _entity.Instance.PersistentState;
                    YJH.Services.BillService.Save(_entity, this.ddlexpand_no.SelectedValue!=null?this.ddlexpand_no.SelectedValue.ToString():no);
                    _entity.Instance.AcceptChanges();
                    //刷新列表
                    this.RefreshList(entityState, _entity);
                    this.Close();
                    //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        bool Validate()
        {
            if (string.IsNullOrWhiteSpace(this.epexpand_emp.Text))
            {
                MessageBox.Show("请选择扫街人员");
                return false;
            }
            else
            {
                return true;
            }
        }

        void ddlarea_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载街道
            if (ddlarea.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlarea.SelectedValue.ToString()));
                ddlstreet.ValueMember = "ownerid";
                ddlstreet.DisplayMember = "city_name";
                ddlstreet.DataSource = list;
                ddlstreet.SelectedIndex = -1;
            }
        }

        void ddlcity_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载区
            if (ddlcity.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlcity.SelectedValue.ToString()));
                ddlarea.ValueMember = "ownerid";
                ddlarea.DisplayMember = "city_name";
                ddlarea.DataSource = list;
                ddlarea.SelectedIndex = -1;
            }
        }

        void ddlprovince_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载市
            if (ddlprovince.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlprovince.SelectedValue.ToString()));
                ddlcity.ValueMember = "ownerid";
                ddlcity.DisplayMember = "city_name";
                ddlcity.DataSource = list;
                ddlcity.SelectedIndex = -1;
            }
        }
        #endregion
    }
}
