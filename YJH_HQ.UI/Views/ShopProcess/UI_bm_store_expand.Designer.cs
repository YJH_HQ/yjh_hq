﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UI_bm_store_expand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxexpand_no = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel45 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel44 = new Telerik.WinControls.UI.RadLabel();
            this.rwb = new Telerik.WinControls.UI.RadWaitingBar();
            this.lb3 = new Telerik.WinControls.UI.RadLabel();
            this.lb2 = new Telerik.WinControls.UI.RadLabel();
            this.lb1 = new Telerik.WinControls.UI.RadLabel();
            this.btnDownLoad3 = new Telerik.WinControls.UI.RadButton();
            this.btnUpLoad3 = new Telerik.WinControls.UI.RadButton();
            this.btnDownLoad2 = new Telerik.WinControls.UI.RadButton();
            this.btnUpLoad2 = new Telerik.WinControls.UI.RadButton();
            this.btnDownLoad1 = new Telerik.WinControls.UI.RadButton();
            this.btnUpLoad1 = new Telerik.WinControls.UI.RadButton();
            this.ddlrecorder = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel40 = new Telerik.WinControls.UI.RadLabel();
            this.tbxproperty_rights = new Telerik.WinControls.UI.RadTextBox();
            this.ddlreviewer = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlexpander = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel38 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel39 = new Telerik.WinControls.UI.RadLabel();
            this.tbxeffect_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxeffecter = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel36 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel37 = new Telerik.WinControls.UI.RadLabel();
            this.tbxaudit_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxaudit = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel34 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel35 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxcreater = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.tbxdecorationStatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.tbxdecorationdate = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.tbxdecorationPredate = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcontractRemark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.tbxrecorderdate = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcontractEndDate = new Telerik.WinControls.UI.RadTextBox();
            this.tbxcontractStartDate = new Telerik.WinControls.UI.RadTextBox();
            this.dtpreviewer_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.tbxuse_area = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.tbxfloor_space = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.tbxlink_tel = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbxaddr = new Telerik.WinControls.UI.RadTextBox();
            this.tbxstatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstore_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.ddlstoreProperty = new YJH_HQ.Controls.RadDropDownList.RadDropDownList();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.ddlstreet = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.ddlarea = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.ddlcity = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlprovince = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstore_act_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxstore_plan_date = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.tbxRemark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstore_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxlinkman = new Telerik.WinControls.UI.RadTextBox();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rwb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxproperty_rights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlreviewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlexpander)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffect_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationPredate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrecorderdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpreviewer_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxuse_area)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxfloor_space)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlink_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaddr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstoreProperty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_act_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_plan_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlinkman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(66, 36);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "拓展单号：";
            // 
            // tbxexpand_no
            // 
            this.tbxexpand_no.Enabled = false;
            this.tbxexpand_no.Location = new System.Drawing.Point(137, 34);
            this.tbxexpand_no.MaxLength = 11;
            this.tbxexpand_no.Name = "tbxexpand_no";
            this.tbxexpand_no.ReadOnly = true;
            this.tbxexpand_no.Size = new System.Drawing.Size(108, 20);
            this.tbxexpand_no.TabIndex = 0;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel45);
            this.radGroupBox1.Controls.Add(this.radLabel44);
            this.radGroupBox1.Controls.Add(this.rwb);
            this.radGroupBox1.Controls.Add(this.lb3);
            this.radGroupBox1.Controls.Add(this.lb2);
            this.radGroupBox1.Controls.Add(this.lb1);
            this.radGroupBox1.Controls.Add(this.btnDownLoad3);
            this.radGroupBox1.Controls.Add(this.btnUpLoad3);
            this.radGroupBox1.Controls.Add(this.btnDownLoad2);
            this.radGroupBox1.Controls.Add(this.btnUpLoad2);
            this.radGroupBox1.Controls.Add(this.btnDownLoad1);
            this.radGroupBox1.Controls.Add(this.btnUpLoad1);
            this.radGroupBox1.Controls.Add(this.ddlrecorder);
            this.radGroupBox1.Controls.Add(this.radLabel40);
            this.radGroupBox1.Controls.Add(this.tbxproperty_rights);
            this.radGroupBox1.Controls.Add(this.ddlreviewer);
            this.radGroupBox1.Controls.Add(this.ddlexpander);
            this.radGroupBox1.Controls.Add(this.radLabel38);
            this.radGroupBox1.Controls.Add(this.radLabel39);
            this.radGroupBox1.Controls.Add(this.tbxeffect_date);
            this.radGroupBox1.Controls.Add(this.tbxeffecter);
            this.radGroupBox1.Controls.Add(this.radLabel36);
            this.radGroupBox1.Controls.Add(this.radLabel37);
            this.radGroupBox1.Controls.Add(this.tbxaudit_date);
            this.radGroupBox1.Controls.Add(this.tbxaudit);
            this.radGroupBox1.Controls.Add(this.radLabel18);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel34);
            this.radGroupBox1.Controls.Add(this.radLabel35);
            this.radGroupBox1.Controls.Add(this.tbxcreate_date);
            this.radGroupBox1.Controls.Add(this.tbxcreater);
            this.radGroupBox1.Controls.Add(this.radLabel31);
            this.radGroupBox1.Controls.Add(this.tbxdecorationStatus);
            this.radGroupBox1.Controls.Add(this.radLabel33);
            this.radGroupBox1.Controls.Add(this.tbxdecorationdate);
            this.radGroupBox1.Controls.Add(this.radLabel32);
            this.radGroupBox1.Controls.Add(this.tbxdecorationPredate);
            this.radGroupBox1.Controls.Add(this.radLabel29);
            this.radGroupBox1.Controls.Add(this.tbxcontractRemark);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.tbxrecorderdate);
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel21);
            this.radGroupBox1.Controls.Add(this.tbxcontractEndDate);
            this.radGroupBox1.Controls.Add(this.tbxcontractStartDate);
            this.radGroupBox1.Controls.Add(this.dtpreviewer_date);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.tbxuse_area);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.tbxfloor_space);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.tbxlink_tel);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.tbxaddr);
            this.radGroupBox1.Controls.Add(this.tbxstatus);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.tbxstore_id);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.ddlstoreProperty);
            this.radGroupBox1.Controls.Add(this.radLabel28);
            this.radGroupBox1.Controls.Add(this.radLabel27);
            this.radGroupBox1.Controls.Add(this.ddlstreet);
            this.radGroupBox1.Controls.Add(this.radLabel26);
            this.radGroupBox1.Controls.Add(this.ddlarea);
            this.radGroupBox1.Controls.Add(this.radLabel25);
            this.radGroupBox1.Controls.Add(this.radLabel24);
            this.radGroupBox1.Controls.Add(this.ddlcity);
            this.radGroupBox1.Controls.Add(this.ddlprovince);
            this.radGroupBox1.Controls.Add(this.radLabel20);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.tbxstore_act_date);
            this.radGroupBox1.Controls.Add(this.tbxstore_plan_date);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.tbxRemark);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxstore_name);
            this.radGroupBox1.Controls.Add(this.radLabel30);
            this.radGroupBox1.Controls.Add(this.tbxlinkman);
            this.radGroupBox1.Controls.Add(this.tbxexpand_no);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(939, 504);
            this.radGroupBox1.TabIndex = 3;
            // 
            // radLabel45
            // 
            this.radLabel45.ForeColor = System.Drawing.Color.Red;
            this.radLabel45.Location = new System.Drawing.Point(863, 115);
            this.radLabel45.Name = "radLabel45";
            this.radLabel45.Size = new System.Drawing.Size(11, 18);
            this.radLabel45.TabIndex = 108;
            this.radLabel45.Text = "*";
            // 
            // radLabel44
            // 
            this.radLabel44.ForeColor = System.Drawing.Color.Red;
            this.radLabel44.Location = new System.Drawing.Point(654, 115);
            this.radLabel44.Name = "radLabel44";
            this.radLabel44.Size = new System.Drawing.Size(11, 18);
            this.radLabel44.TabIndex = 107;
            this.radLabel44.Text = "*";
            // 
            // rwb
            // 
            this.rwb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rwb.Location = new System.Drawing.Point(401, 189);
            this.rwb.Name = "rwb";
            this.rwb.Size = new System.Drawing.Size(130, 24);
            this.rwb.TabIndex = 0;
            this.rwb.Text = "上传中";
            this.rwb.Visible = false;
            // 
            // lb3
            // 
            this.lb3.ForeColor = System.Drawing.Color.Red;
            this.lb3.Location = new System.Drawing.Point(270, 436);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(42, 18);
            this.lb3.TabIndex = 106;
            this.lb3.Text = "未上传";
            // 
            // lb2
            // 
            this.lb2.ForeColor = System.Drawing.Color.Red;
            this.lb2.Location = new System.Drawing.Point(270, 406);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(42, 18);
            this.lb2.TabIndex = 105;
            this.lb2.Text = "未上传";
            // 
            // lb1
            // 
            this.lb1.ForeColor = System.Drawing.Color.Red;
            this.lb1.Location = new System.Drawing.Point(270, 380);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(42, 18);
            this.lb1.TabIndex = 104;
            this.lb1.Text = "未上传";
            // 
            // btnDownLoad3
            // 
            this.btnDownLoad3.Image = global::YJH_HQ.UI.Properties.Resources.Storage16;
            this.btnDownLoad3.Location = new System.Drawing.Point(196, 434);
            this.btnDownLoad3.Name = "btnDownLoad3";
            this.btnDownLoad3.Size = new System.Drawing.Size(53, 24);
            this.btnDownLoad3.TabIndex = 102;
            this.btnDownLoad3.Text = "下载";
            this.btnDownLoad3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // btnUpLoad3
            // 
            this.btnUpLoad3.Image = global::YJH_HQ.UI.Properties.Resources.Manual;
            this.btnUpLoad3.Location = new System.Drawing.Point(137, 434);
            this.btnUpLoad3.Name = "btnUpLoad3";
            this.btnUpLoad3.Size = new System.Drawing.Size(53, 24);
            this.btnUpLoad3.TabIndex = 101;
            this.btnUpLoad3.Text = "上传";
            this.btnUpLoad3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // btnDownLoad2
            // 
            this.btnDownLoad2.Image = global::YJH_HQ.UI.Properties.Resources.Storage16;
            this.btnDownLoad2.Location = new System.Drawing.Point(196, 404);
            this.btnDownLoad2.Name = "btnDownLoad2";
            this.btnDownLoad2.Size = new System.Drawing.Size(53, 24);
            this.btnDownLoad2.TabIndex = 99;
            this.btnDownLoad2.Text = "下载";
            this.btnDownLoad2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // btnUpLoad2
            // 
            this.btnUpLoad2.Image = global::YJH_HQ.UI.Properties.Resources.Manual;
            this.btnUpLoad2.Location = new System.Drawing.Point(137, 404);
            this.btnUpLoad2.Name = "btnUpLoad2";
            this.btnUpLoad2.Size = new System.Drawing.Size(53, 24);
            this.btnUpLoad2.TabIndex = 98;
            this.btnUpLoad2.Text = "上传";
            this.btnUpLoad2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // btnDownLoad1
            // 
            this.btnDownLoad1.Image = global::YJH_HQ.UI.Properties.Resources.Storage16;
            this.btnDownLoad1.Location = new System.Drawing.Point(196, 378);
            this.btnDownLoad1.Name = "btnDownLoad1";
            this.btnDownLoad1.Size = new System.Drawing.Size(53, 24);
            this.btnDownLoad1.TabIndex = 96;
            this.btnDownLoad1.Text = "下载";
            this.btnDownLoad1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // btnUpLoad1
            // 
            this.btnUpLoad1.Image = global::YJH_HQ.UI.Properties.Resources.Manual;
            this.btnUpLoad1.Location = new System.Drawing.Point(137, 378);
            this.btnUpLoad1.Name = "btnUpLoad1";
            this.btnUpLoad1.Size = new System.Drawing.Size(53, 24);
            this.btnUpLoad1.TabIndex = 95;
            this.btnUpLoad1.Text = "上传";
            this.btnUpLoad1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ddlrecorder
            // 
            this.ddlrecorder.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlrecorder.Enabled = false;
            this.ddlrecorder.Location = new System.Drawing.Point(540, 169);
            this.ddlrecorder.Name = "ddlrecorder";
            this.ddlrecorder.Size = new System.Drawing.Size(108, 20);
            this.ddlrecorder.TabIndex = 94;
            // 
            // radLabel40
            // 
            this.radLabel40.Location = new System.Drawing.Point(485, 90);
            this.radLabel40.Name = "radLabel40";
            this.radLabel40.Size = new System.Drawing.Size(54, 18);
            this.radLabel40.TabIndex = 93;
            this.radLabel40.Text = "产权方：";
            // 
            // tbxproperty_rights
            // 
            this.tbxproperty_rights.Location = new System.Drawing.Point(540, 88);
            this.tbxproperty_rights.Name = "tbxproperty_rights";
            this.tbxproperty_rights.Size = new System.Drawing.Size(317, 20);
            this.tbxproperty_rights.TabIndex = 92;
            // 
            // ddlreviewer
            // 
            this.ddlreviewer.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlreviewer.Location = new System.Drawing.Point(749, 114);
            this.ddlreviewer.Name = "ddlreviewer";
            this.ddlreviewer.Size = new System.Drawing.Size(108, 20);
            this.ddlreviewer.TabIndex = 91;
            // 
            // ddlexpander
            // 
            this.ddlexpander.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlexpander.Location = new System.Drawing.Point(540, 114);
            this.ddlexpander.Name = "ddlexpander";
            this.ddlexpander.Size = new System.Drawing.Size(108, 20);
            this.ddlexpander.TabIndex = 90;
            // 
            // radLabel38
            // 
            this.radLabel38.Location = new System.Drawing.Point(474, 356);
            this.radLabel38.Name = "radLabel38";
            this.radLabel38.Size = new System.Drawing.Size(65, 18);
            this.radLabel38.TabIndex = 88;
            this.radLabel38.Text = "审批时间：";
            // 
            // radLabel39
            // 
            this.radLabel39.Location = new System.Drawing.Point(485, 329);
            this.radLabel39.Name = "radLabel39";
            this.radLabel39.Size = new System.Drawing.Size(54, 18);
            this.radLabel39.TabIndex = 89;
            this.radLabel39.Text = "审批人：";
            // 
            // tbxeffect_date
            // 
            this.tbxeffect_date.Enabled = false;
            this.tbxeffect_date.Location = new System.Drawing.Point(540, 352);
            this.tbxeffect_date.Name = "tbxeffect_date";
            this.tbxeffect_date.ReadOnly = true;
            this.tbxeffect_date.Size = new System.Drawing.Size(108, 20);
            this.tbxeffect_date.TabIndex = 86;
            // 
            // tbxeffecter
            // 
            this.tbxeffecter.Enabled = false;
            this.tbxeffecter.Location = new System.Drawing.Point(540, 327);
            this.tbxeffecter.Name = "tbxeffecter";
            this.tbxeffecter.ReadOnly = true;
            this.tbxeffecter.Size = new System.Drawing.Size(108, 20);
            this.tbxeffecter.TabIndex = 87;
            // 
            // radLabel36
            // 
            this.radLabel36.Location = new System.Drawing.Point(270, 353);
            this.radLabel36.Name = "radLabel36";
            this.radLabel36.Size = new System.Drawing.Size(65, 18);
            this.radLabel36.TabIndex = 84;
            this.radLabel36.Text = "稽核时间：";
            // 
            // radLabel37
            // 
            this.radLabel37.Location = new System.Drawing.Point(281, 329);
            this.radLabel37.Name = "radLabel37";
            this.radLabel37.Size = new System.Drawing.Size(54, 18);
            this.radLabel37.TabIndex = 85;
            this.radLabel37.Text = "稽核人：";
            // 
            // tbxaudit_date
            // 
            this.tbxaudit_date.Enabled = false;
            this.tbxaudit_date.Location = new System.Drawing.Point(341, 352);
            this.tbxaudit_date.Name = "tbxaudit_date";
            this.tbxaudit_date.ReadOnly = true;
            this.tbxaudit_date.Size = new System.Drawing.Size(108, 20);
            this.tbxaudit_date.TabIndex = 82;
            // 
            // tbxaudit
            // 
            this.tbxaudit.Enabled = false;
            this.tbxaudit.Location = new System.Drawing.Point(341, 325);
            this.tbxaudit.Name = "tbxaudit";
            this.tbxaudit.ReadOnly = true;
            this.tbxaudit.Size = new System.Drawing.Size(108, 20);
            this.tbxaudit.TabIndex = 83;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(71, 436);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(60, 18);
            this.radLabel18.TabIndex = 68;
            this.radLabel18.Text = "拓展PPT：";
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(66, 406);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(65, 18);
            this.radLabel17.TabIndex = 68;
            this.radLabel17.Text = "考察情况：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(66, 380);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 81;
            this.radLabel2.Text = "扫街情况：";
            // 
            // radLabel34
            // 
            this.radLabel34.Location = new System.Drawing.Point(66, 356);
            this.radLabel34.Name = "radLabel34";
            this.radLabel34.Size = new System.Drawing.Size(65, 18);
            this.radLabel34.TabIndex = 79;
            this.radLabel34.Text = "录入时间：";
            // 
            // radLabel35
            // 
            this.radLabel35.Location = new System.Drawing.Point(77, 329);
            this.radLabel35.Name = "radLabel35";
            this.radLabel35.Size = new System.Drawing.Size(54, 18);
            this.radLabel35.TabIndex = 80;
            this.radLabel35.Text = "录入人：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(137, 352);
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.ReadOnly = true;
            this.tbxcreate_date.Size = new System.Drawing.Size(108, 20);
            this.tbxcreate_date.TabIndex = 77;
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(137, 325);
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.ReadOnly = true;
            this.tbxcreater.Size = new System.Drawing.Size(108, 20);
            this.tbxcreater.TabIndex = 78;
            // 
            // radLabel31
            // 
            this.radLabel31.Location = new System.Drawing.Point(583, 225);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(65, 18);
            this.radLabel31.TabIndex = 76;
            this.radLabel31.Text = "装修情况：";
            // 
            // tbxdecorationStatus
            // 
            this.tbxdecorationStatus.Enabled = false;
            this.tbxdecorationStatus.Location = new System.Drawing.Point(654, 223);
            this.tbxdecorationStatus.Name = "tbxdecorationStatus";
            this.tbxdecorationStatus.ReadOnly = true;
            this.tbxdecorationStatus.Size = new System.Drawing.Size(203, 20);
            this.tbxdecorationStatus.TabIndex = 75;
            // 
            // radLabel33
            // 
            this.radLabel33.Location = new System.Drawing.Point(305, 225);
            this.radLabel33.Name = "radLabel33";
            this.radLabel33.Size = new System.Drawing.Size(113, 18);
            this.radLabel33.TabIndex = 74;
            this.radLabel33.Text = "装修实际完成日期：";
            // 
            // tbxdecorationdate
            // 
            this.tbxdecorationdate.Enabled = false;
            this.tbxdecorationdate.Location = new System.Drawing.Point(424, 223);
            this.tbxdecorationdate.Name = "tbxdecorationdate";
            this.tbxdecorationdate.ReadOnly = true;
            this.tbxdecorationdate.Size = new System.Drawing.Size(130, 20);
            this.tbxdecorationdate.TabIndex = 72;
            // 
            // radLabel32
            // 
            this.radLabel32.Location = new System.Drawing.Point(18, 225);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(113, 18);
            this.radLabel32.TabIndex = 71;
            this.radLabel32.Text = "装修计划完成日期：";
            // 
            // tbxdecorationPredate
            // 
            this.tbxdecorationPredate.Enabled = false;
            this.tbxdecorationPredate.Location = new System.Drawing.Point(137, 223);
            this.tbxdecorationPredate.Name = "tbxdecorationPredate";
            this.tbxdecorationPredate.ReadOnly = true;
            this.tbxdecorationPredate.Size = new System.Drawing.Size(130, 20);
            this.tbxdecorationPredate.TabIndex = 68;
            // 
            // radLabel29
            // 
            this.radLabel29.Location = new System.Drawing.Point(66, 198);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(65, 18);
            this.radLabel29.TabIndex = 67;
            this.radLabel29.Text = "合同情况：";
            // 
            // tbxcontractRemark
            // 
            this.tbxcontractRemark.Enabled = false;
            this.tbxcontractRemark.Location = new System.Drawing.Point(137, 196);
            this.tbxcontractRemark.Name = "tbxcontractRemark";
            this.tbxcontractRemark.ReadOnly = true;
            this.tbxcontractRemark.Size = new System.Drawing.Size(720, 20);
            this.tbxcontractRemark.TabIndex = 66;
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(654, 171);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(89, 18);
            this.radLabel22.TabIndex = 64;
            this.radLabel22.Text = "合同签约日期：";
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(462, 171);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(77, 18);
            this.radLabel23.TabIndex = 65;
            this.radLabel23.Text = "合同签约人：";
            // 
            // tbxrecorderdate
            // 
            this.tbxrecorderdate.Enabled = false;
            this.tbxrecorderdate.Location = new System.Drawing.Point(749, 169);
            this.tbxrecorderdate.Name = "tbxrecorderdate";
            this.tbxrecorderdate.ReadOnly = true;
            this.tbxrecorderdate.Size = new System.Drawing.Size(108, 20);
            this.tbxrecorderdate.TabIndex = 62;
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(246, 171);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(89, 18);
            this.radLabel16.TabIndex = 60;
            this.radLabel16.Text = "合同结束日期：";
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(42, 171);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(89, 18);
            this.radLabel21.TabIndex = 61;
            this.radLabel21.Text = "合同开始日期：";
            // 
            // tbxcontractEndDate
            // 
            this.tbxcontractEndDate.Enabled = false;
            this.tbxcontractEndDate.Location = new System.Drawing.Point(341, 169);
            this.tbxcontractEndDate.Name = "tbxcontractEndDate";
            this.tbxcontractEndDate.ReadOnly = true;
            this.tbxcontractEndDate.Size = new System.Drawing.Size(109, 20);
            this.tbxcontractEndDate.TabIndex = 58;
            // 
            // tbxcontractStartDate
            // 
            this.tbxcontractStartDate.Enabled = false;
            this.tbxcontractStartDate.Location = new System.Drawing.Point(137, 169);
            this.tbxcontractStartDate.Name = "tbxcontractStartDate";
            this.tbxcontractStartDate.ReadOnly = true;
            this.tbxcontractStartDate.Size = new System.Drawing.Size(108, 20);
            this.tbxcontractStartDate.TabIndex = 59;
            // 
            // dtpreviewer_date
            // 
            this.dtpreviewer_date.Location = new System.Drawing.Point(749, 142);
            this.dtpreviewer_date.Name = "dtpreviewer_date";
            this.dtpreviewer_date.Size = new System.Drawing.Size(109, 20);
            this.dtpreviewer_date.TabIndex = 57;
            this.dtpreviewer_date.TabStop = false;
            this.dtpreviewer_date.Text = "2015年12月14日";
            this.dtpreviewer_date.Value = new System.DateTime(2015, 12, 14, 14, 16, 49, 193);
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(679, 142);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(65, 18);
            this.radLabel15.TabIndex = 56;
            this.radLabel15.Text = "考察日期：";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(401, 144);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(49, 18);
            this.radLabel14.TabIndex = 55;
            this.radLabel14.Text = "(平方米)";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(196, 144);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(49, 18);
            this.radLabel13.TabIndex = 54;
            this.radLabel13.Text = "(平方米)";
            // 
            // tbxuse_area
            // 
            this.tbxuse_area.Location = new System.Drawing.Point(341, 142);
            this.tbxuse_area.MaxLength = 2;
            this.tbxuse_area.Name = "tbxuse_area";
            this.tbxuse_area.Size = new System.Drawing.Size(53, 20);
            this.tbxuse_area.TabIndex = 53;
            this.tbxuse_area.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(270, 144);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(65, 18);
            this.radLabel10.TabIndex = 52;
            this.radLabel10.Text = "使用面积：";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(66, 144);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(65, 18);
            this.radLabel12.TabIndex = 51;
            this.radLabel12.Text = "建筑面积：";
            // 
            // tbxfloor_space
            // 
            this.tbxfloor_space.Location = new System.Drawing.Point(137, 142);
            this.tbxfloor_space.MaxLength = 2;
            this.tbxfloor_space.Name = "tbxfloor_space";
            this.tbxfloor_space.Size = new System.Drawing.Size(53, 20);
            this.tbxfloor_space.TabIndex = 50;
            this.tbxfloor_space.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(690, 114);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(54, 18);
            this.radLabel7.TabIndex = 47;
            this.radLabel7.Text = "考察人：";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(485, 114);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(54, 18);
            this.radLabel9.TabIndex = 46;
            this.radLabel9.Text = "扫街人：";
            // 
            // tbxlink_tel
            // 
            this.tbxlink_tel.Location = new System.Drawing.Point(341, 114);
            this.tbxlink_tel.Name = "tbxlink_tel";
            this.tbxlink_tel.Size = new System.Drawing.Size(109, 20);
            this.tbxlink_tel.TabIndex = 44;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(89, 90);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(42, 18);
            this.radLabel6.TabIndex = 43;
            this.radLabel6.Text = "地址：";
            // 
            // tbxaddr
            // 
            this.tbxaddr.Location = new System.Drawing.Point(137, 88);
            this.tbxaddr.Name = "tbxaddr";
            this.tbxaddr.Size = new System.Drawing.Size(313, 20);
            this.tbxaddr.TabIndex = 42;
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(749, 34);
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.ReadOnly = true;
            this.tbxstatus.Size = new System.Drawing.Size(108, 20);
            this.tbxstatus.TabIndex = 40;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(702, 36);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(42, 18);
            this.radLabel5.TabIndex = 41;
            this.radLabel5.Text = "状态：";
            // 
            // tbxstore_id
            // 
            this.tbxstore_id.Enabled = false;
            this.tbxstore_id.Location = new System.Drawing.Point(540, 34);
            this.tbxstore_id.Name = "tbxstore_id";
            this.tbxstore_id.ReadOnly = true;
            this.tbxstore_id.Size = new System.Drawing.Size(108, 20);
            this.tbxstore_id.TabIndex = 38;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(474, 36);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "生效店号：";
            // 
            // ddlstoreProperty
            // 
            this.ddlstoreProperty.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Tag = "1";
            radListDataItem1.Text = "门店";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Tag = "2";
            radListDataItem2.Text = "社区配套用房";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Tag = "3";
            radListDataItem3.Text = "移动箱房";
            radListDataItem3.TextWrap = true;
            this.ddlstoreProperty.Items.Add(radListDataItem1);
            this.ddlstoreProperty.Items.Add(radListDataItem2);
            this.ddlstoreProperty.Items.Add(radListDataItem3);
            this.ddlstoreProperty.Location = new System.Drawing.Point(540, 142);
            this.ddlstoreProperty.Name = "ddlstoreProperty";
            this.ddlstoreProperty.Size = new System.Drawing.Size(108, 20);
            this.ddlstoreProperty.TabIndex = 33;
            // 
            // radLabel28
            // 
            this.radLabel28.Location = new System.Drawing.Point(474, 144);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(65, 18);
            this.radLabel28.TabIndex = 11;
            this.radLabel28.Text = "网点性质：";
            // 
            // radLabel27
            // 
            this.radLabel27.Location = new System.Drawing.Point(703, 60);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(42, 18);
            this.radLabel27.TabIndex = 32;
            this.radLabel27.Text = "街道：";
            // 
            // ddlstreet
            // 
            this.ddlstreet.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlstreet.Location = new System.Drawing.Point(749, 60);
            this.ddlstreet.Name = "ddlstreet";
            this.ddlstreet.Size = new System.Drawing.Size(108, 20);
            this.ddlstreet.TabIndex = 31;
            // 
            // radLabel26
            // 
            this.radLabel26.Location = new System.Drawing.Point(497, 60);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(42, 18);
            this.radLabel26.TabIndex = 30;
            this.radLabel26.Text = "市区：";
            // 
            // ddlarea
            // 
            this.ddlarea.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlarea.Location = new System.Drawing.Point(540, 60);
            this.ddlarea.Name = "ddlarea";
            this.ddlarea.Size = new System.Drawing.Size(108, 20);
            this.ddlarea.TabIndex = 29;
            // 
            // radLabel25
            // 
            this.radLabel25.Location = new System.Drawing.Point(293, 63);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(42, 18);
            this.radLabel25.TabIndex = 28;
            this.radLabel25.Text = "城市：";
            // 
            // radLabel24
            // 
            this.radLabel24.Location = new System.Drawing.Point(89, 63);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(42, 18);
            this.radLabel24.TabIndex = 27;
            this.radLabel24.Text = "省份：";
            // 
            // ddlcity
            // 
            this.ddlcity.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlcity.Location = new System.Drawing.Point(341, 60);
            this.ddlcity.Name = "ddlcity";
            this.ddlcity.Size = new System.Drawing.Size(109, 20);
            this.ddlcity.TabIndex = 26;
            // 
            // ddlprovince
            // 
            this.ddlprovince.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlprovince.Location = new System.Drawing.Point(137, 61);
            this.ddlprovince.Name = "ddlprovince";
            this.ddlprovince.Size = new System.Drawing.Size(108, 20);
            this.ddlprovince.TabIndex = 25;
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(246, 252);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(89, 18);
            this.radLabel20.TabIndex = 14;
            this.radLabel20.Text = "实际开业日期：";
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(42, 252);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(89, 18);
            this.radLabel19.TabIndex = 14;
            this.radLabel19.Text = "计划开店日期：";
            // 
            // tbxstore_act_date
            // 
            this.tbxstore_act_date.Enabled = false;
            this.tbxstore_act_date.Location = new System.Drawing.Point(341, 250);
            this.tbxstore_act_date.Name = "tbxstore_act_date";
            this.tbxstore_act_date.ReadOnly = true;
            this.tbxstore_act_date.Size = new System.Drawing.Size(108, 20);
            this.tbxstore_act_date.TabIndex = 13;
            // 
            // tbxstore_plan_date
            // 
            this.tbxstore_plan_date.Enabled = false;
            this.tbxstore_plan_date.Location = new System.Drawing.Point(137, 250);
            this.tbxstore_plan_date.Name = "tbxstore_plan_date";
            this.tbxstore_plan_date.ReadOnly = true;
            this.tbxstore_plan_date.Size = new System.Drawing.Size(108, 20);
            this.tbxstore_plan_date.TabIndex = 13;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(270, 117);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 10;
            this.radLabel11.Text = "联系电话：";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(89, 279);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(42, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "备注：";
            // 
            // tbxRemark
            // 
            this.tbxRemark.AutoSize = false;
            this.tbxRemark.Location = new System.Drawing.Point(137, 277);
            this.tbxRemark.Multiline = true;
            this.tbxRemark.Name = "tbxRemark";
            this.tbxRemark.Size = new System.Drawing.Size(720, 40);
            this.tbxRemark.TabIndex = 7;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(77, 117);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "联系人：";
            // 
            // tbxstore_name
            // 
            this.tbxstore_name.Location = new System.Drawing.Point(341, 34);
            this.tbxstore_name.Name = "tbxstore_name";
            this.tbxstore_name.Size = new System.Drawing.Size(108, 20);
            this.tbxstore_name.TabIndex = 1;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(270, 36);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(65, 18);
            this.radLabel30.TabIndex = 2;
            this.radLabel30.Text = "店铺名称：";
            // 
            // tbxlinkman
            // 
            this.tbxlinkman.Location = new System.Drawing.Point(137, 115);
            this.tbxlinkman.Name = "tbxlinkman";
            this.tbxlinkman.Size = new System.Drawing.Size(108, 20);
            this.tbxlinkman.TabIndex = 1;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(939, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // UI_bm_store_expand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(939, 534);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_bm_store_expand";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "拓店信息";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rwb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownLoad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpLoad1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxproperty_rights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlreviewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlexpander)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffect_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdecorationPredate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrecorderdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpreviewer_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxuse_area)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxfloor_space)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlink_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaddr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstoreProperty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_act_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_plan_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstore_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlinkman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox tbxexpand_no;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxlinkman;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadTextBox tbxstore_act_date;
        private Telerik.WinControls.UI.RadTextBox tbxstore_plan_date;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox tbxRemark;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxstore_name;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadDropDownList ddlstreet;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadDropDownList ddlarea;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadDropDownList ddlcity;
        private Telerik.WinControls.UI.RadDropDownList ddlprovince;
        private Controls.RadDropDownList.RadDropDownList ddlstoreProperty;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadTextBox tbxstore_id;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox tbxaddr;
        private Telerik.WinControls.UI.RadTextBox tbxlink_tel;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadDateTimePicker dtpreviewer_date;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox tbxuse_area;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox tbxfloor_space;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbxrecorderdate;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadTextBox tbxcontractEndDate;
        private Telerik.WinControls.UI.RadTextBox tbxcontractStartDate;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadTextBox tbxcontractRemark;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadTextBox tbxdecorationdate;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadTextBox tbxdecorationPredate;
        private Telerik.WinControls.UI.RadLabel radLabel34;
        private Telerik.WinControls.UI.RadLabel radLabel35;
        private Telerik.WinControls.UI.RadTextBox tbxcreate_date;
        private Telerik.WinControls.UI.RadTextBox tbxcreater;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadTextBox tbxdecorationStatus;
        private Telerik.WinControls.UI.RadLabel radLabel38;
        private Telerik.WinControls.UI.RadLabel radLabel39;
        private Telerik.WinControls.UI.RadTextBox tbxeffect_date;
        private Telerik.WinControls.UI.RadTextBox tbxeffecter;
        private Telerik.WinControls.UI.RadLabel radLabel36;
        private Telerik.WinControls.UI.RadLabel radLabel37;
        private Telerik.WinControls.UI.RadTextBox tbxaudit_date;
        private Telerik.WinControls.UI.RadTextBox tbxaudit;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlexpander;
        private Telerik.WinControls.UI.RadDropDownList ddlreviewer;
        private Telerik.WinControls.UI.RadLabel radLabel40;
        private Telerik.WinControls.UI.RadTextBox tbxproperty_rights;
        private Telerik.WinControls.UI.RadDropDownList ddlrecorder;
        private Telerik.WinControls.UI.RadButton btnUpLoad1;
        private Telerik.WinControls.UI.RadButton btnDownLoad1;
        private Telerik.WinControls.UI.RadButton btnDownLoad3;
        private Telerik.WinControls.UI.RadButton btnUpLoad3;
        private Telerik.WinControls.UI.RadButton btnDownLoad2;
        private Telerik.WinControls.UI.RadButton btnUpLoad2;
        private Telerik.WinControls.UI.RadLabel lb3;
        private Telerik.WinControls.UI.RadLabel lb2;
        private Telerik.WinControls.UI.RadLabel lb1;
        private Telerik.WinControls.UI.RadWaitingBar rwb;
        private Telerik.WinControls.UI.RadLabel radLabel45;
        private Telerik.WinControls.UI.RadLabel radLabel44;
    }
}