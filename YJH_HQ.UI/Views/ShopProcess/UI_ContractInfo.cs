﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Entities;
using YJH.Services;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_ContractInfo : Telerik.WinControls.UI.RadForm
    {
        public Guid id;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        public UI_ContractInfo()
        {
            InitializeComponent();
            this.Load += ContractInfo_Load;
            this.btSave.Click += btSave_Click;
        }

        private bool ValueDate()
        {
            if (string.IsNullOrWhiteSpace(this.dtpcontractStartDate.Text))
            {
                RadMessageBox.Show("请选择合同开始日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.dtpcontractEndDate.Text))
            {
                RadMessageBox.Show("请选择合同结束日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlrecorder.Text))
            {
                RadMessageBox.Show("请选择合同签约人", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.dtprecorderdate.Text))
            {
                RadMessageBox.Show("请选择合同签约日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        void btSave_Click(object sender, EventArgs e)
        {
              if(ValueDate())
              {
                  try
                  {
                      bm_store_expand entity = bm_store_expandService.GetByID(id);
                      if (entity == null || entity.status != 30)
                          return;
                      //entity.contractStartDate = dtpcontractStartDate.Value;
                      //entity.contractEndDate = dtpcontractEndDate.Value;
                      //entity.recorderID = Guid.Parse(ddlrecorder.SelectedValue.ToString());
                      //entity.recorderdate = dtprecorderdate.Value;
                      //entity.contractRemark = tbxcontractRemark.Text;
                      //var entityState = entity.Instance.PersistentState;
                      //bm_store_expandService.Save(entity);
                      YJH.Entities.bm_store_contract entity1 = new bm_store_contract();
                      entity1.expandID = entity.Instance.ID;
                      entity1.contract_no = tbxcontract_no.Text;
                      entity1.startDate = dtpcontractStartDate.Value;
                      entity1.endDate = dtpcontractEndDate.Value;
                      entity1.recorderID = Guid.Parse(ddlrecorder.SelectedValue.ToString());
                      entity1.recorderdate = dtprecorderdate.Value;
                      entity1.contractRemark = tbxcontractRemark.Text;
                      entity1.creater = Globle.CurrentEmployee.Base.Name;
                      entity1.status = 10;
                      bm_store_expandService.Savebm_store_contract(entity1);
                      RadMessageBox.Show(this, "合同签订保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                      //this.RefreshList(entityState, entity);
                      this.Close();
                  }
                  catch (Exception ex)
                  {
                      RadMessageBox.Show(this, "合同签订失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                  }
              }
        }

        void ContractInfo_Load(object sender, EventArgs e)
        {
            dtpcontractEndDate.SetToNullValue();
            dtpcontractEndDate.SetToNullValue();
            dtprecorderdate.SetToNullValue();
            tbxcontractRemark.Text = "";
            ddlrecorder.DisplayMember = "Name";
            ddlrecorder.ValueMember = "ID";
            ddlrecorder.DataSource = bm_store_expandService.GetEmp();
            ddlrecorder.SelectedIndex = -1;
        }
    }
}
