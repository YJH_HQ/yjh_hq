﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Documents;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Entities;
using YJH.Enums;
using YJH.Services;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.Transactions;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_bm_store_expand : Telerik.WinControls.UI.RadForm
    {
        #region===字段===
        public bool IsNew = true;
        public YJH.Entities.bm_store_expand entity;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        #endregion
        #region===构造===
        public UI_bm_store_expand()
        {
            InitializeComponent();
            this.Load += UI_bm_store_expand_Load;
            this.btSave.Click += btSave_Click;
            this.ddlprovince.SelectedIndexChanging += ddlprovince_SelectedIndexChanging;
            this.ddlcity.SelectedIndexChanged += ddlcity_SelectedIndexChanged;
            this.ddlarea.SelectedIndexChanged += ddlarea_SelectedIndexChanged;
            this.btnUpLoad1.Click += (s, e) => { this.UpLoad(1); };
            this.btnDownLoad1.Click += (s, e) => { this.DownLoad(1); };
            this.btnUpLoad2.Click += (s, e) => { this.UpLoad(2); };
            this.btnDownLoad2.Click += (s, e) => { this.DownLoad(2); };
            this.btnUpLoad3.Click += (s, e) => { this.UpLoad(3); };
            this.btnDownLoad3.Click += (s, e) => { this.DownLoad(3); };
        }
        #endregion
        #region===事件===
        void ddlarea_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载街道
            if (ddlarea.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlarea.SelectedValue.ToString()));
                ddlstreet.ValueMember = "ownerid";
                ddlstreet.DisplayMember = "city_name";
                ddlstreet.DataSource = list;
                ddlstreet.SelectedIndex = -1;
            }
        }

        void ddlcity_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载区
            if (ddlcity.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlcity.SelectedValue.ToString()));
                ddlarea.ValueMember = "ownerid";
                ddlarea.DisplayMember = "city_name";
                ddlarea.DataSource = list;
                ddlarea.SelectedIndex = -1;
            }
        }

        void ddlprovince_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            //加载市
            if (ddlprovince.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlprovince.SelectedValue.ToString()));
                ddlcity.ValueMember = "ownerid";
                ddlcity.DisplayMember = "city_name";
                ddlcity.DataSource = list;
                ddlcity.SelectedIndex = -1;
            }
        }

        void UI_bm_store_expand_Load(object sender, EventArgs e)
        {
            var list = (from ot in Enum.GetValues(typeof(storeProperty)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(storeProperty), ot),
                            ValueMember = ot
                        }).ToList();
            ddlstoreProperty.DisplayMember = "DisplayMember";
            ddlstoreProperty.ValueMember = "ValueMember";
            ddlstoreProperty.DataSource = list;

            ddlexpander.DisplayMember = "Name";
            ddlexpander.ValueMember = "ID";
            ddlexpander.DataSource = bm_store_expandService.GetEmp();
            ddlreviewer.DisplayMember = "Name";
            ddlreviewer.ValueMember = "ID";
            ddlreviewer.DataSource = bm_store_expandService.GetEmp();
            ddlrecorder.DisplayMember = "Name";
            ddlrecorder.ValueMember = "ID";
            ddlrecorder.DataSource = bm_store_expandService.GetEmp();
            //省
            List<ba_district> list1 = YJH.Services.StoreMngService.Getprovince();
            ddlprovince.ValueMember = "ownerid";
            ddlprovince.DisplayMember = "city_name";
            ddlprovince.DataSource = list1;
            ddlprovince.SelectedIndex = 9;

            if(IsNew)
                New();
            else
                BindDate();
            rwb.Visible = false;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(ValueDate())
                {
                    FulshDate();
                    var entityState = entity.Instance.PersistentState;
                    bm_store_expandService.Save(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK,RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this,"保存失败" + ex.Message, "提示",MessageBoxButtons.OK,RadMessageIcon.Error);
            }
        }

        #endregion

        #region===方法===
        public void New()
        {
            tbxstatus.Text = "新建";
            ddlstoreProperty.SelectedIndex = 0;
            tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            dtpreviewer_date.Value = OrgUnitService.GetDate();
            tbxfloor_space.Text = "0";
            tbxuse_area.Text = "0";
            ddlexpander.SelectedIndex = -1;
            ddlreviewer.SelectedIndex = -1;
            ddlrecorder.SelectedIndex = -1;
            tbxcreate_date.Text = OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            tbxstore_name.Focus();
        }

        public void FulshDate()
        {
            string maxno = bm_store_expandService.GetMaxNo();
            if (maxno == "" || maxno == null)
                maxno = "0010001";
            else
            {
                int no = int.Parse(maxno.Substring(2, 5)) + 1;
                maxno = "00" + no.ToString();
            }
            if(IsNew)
            {
                entity = new YJH.Entities.bm_store_expand();
                entity.expand_no = maxno;
                entity.status = 10;
                entity.creater = tbxcreater.Text;
                entity.create_date = DateTime.Parse(tbxcreate_date.Text);
            }
            entity.province = ddlprovince.Text.Trim();
            entity.city = ddlcity.Text.Trim();
            entity.area = ddlarea.Text.Trim();
            entity.street = ddlstreet.Text.Trim();
            entity.addr = tbxaddr.Text.Trim();
            entity.store_name = tbxstore_name.Text.Trim();
            entity.property_rights = tbxproperty_rights.Text.Trim();
            entity.linkman = tbxlinkman.Text.Trim();
            entity.link_tel = tbxlink_tel.Text.Trim();
            entity.reviewerID = Guid.Parse(ddlreviewer.SelectedValue.ToString());
            entity.expanderID = Guid.Parse(ddlexpander.SelectedValue.ToString());
            entity.floor_space = Decimal.Parse(tbxfloor_space.Text.Trim());
            entity.use_area = Decimal.Parse(tbxuse_area.Text.Trim());
            if (ddlstoreProperty.SelectedValue != null)
                entity.storeProperty = int.Parse(ddlstoreProperty.SelectedValue.ToString());
            entity.reviewer_date = dtpreviewer_date.Value;
            entity.Remark = tbxRemark.Text;
        }

        public void BindDate()
        {
            tbxexpand_no.Text = entity.expand_no;
            tbxstore_name.Text = entity.store_name;
            tbxstore_id.Text = entity.store_id;
            tbxstatus.Text = Enum.GetName(typeof(expandstatus), entity.status);
            List<ba_district> list0 = YJH.Services.StoreMngService.Getprovince();
            ddlprovince.ValueMember = "ownerid";
            ddlprovince.DisplayMember = "city_name";
            ddlprovince.DataSource = list0;
            ddlprovince.Text = entity.province;
            //List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlprovince.SelectedValue.ToString()));
            //ddlcity.ValueMember = "ownerid";
            //ddlcity.DisplayMember = "city_name";
            //ddlcity.DataSource = list;
            ddlcity.Text = entity.city;
            //List<ba_district> list1 = YJH.Services.StoreMngService.Getchild(int.Parse(ddlcity.SelectedValue.ToString()));
            //ddlarea.ValueMember = "ownerid";
            //ddlarea.DisplayMember = "city_name";
            //ddlarea.DataSource = list1;
            ddlarea.Text = entity.area;
            //List<ba_district> list2 = YJH.Services.StoreMngService.Getchild(int.Parse(ddlarea.SelectedValue.ToString()));
            //ddlstreet.ValueMember = "ownerid";
            //ddlstreet.DisplayMember = "city_name";
            //ddlstreet.DataSource = list2;
            ddlstreet.Text = entity.street;
            tbxaddr.Text = entity.addr;
            tbxproperty_rights.Text = entity.property_rights;
            tbxlinkman.Text = entity.linkman;
            tbxlink_tel.Text = entity.link_tel;
            ddlexpander.SelectedValue = entity.expanderID;
            ddlreviewer.SelectedValue = entity.reviewerID;
            tbxfloor_space.Text = entity.floor_space.ToString();
            tbxuse_area.Text = entity.use_area.ToString();
            ddlstoreProperty.SelectedValue = entity.storeProperty;
            dtpreviewer_date.Value = (DateTime)entity.reviewer_date;
            if(entity.status > 30)
            {
                YJH.Entities.bm_store_contract ec = bm_store_expandService.Getbm_store_contractByExID(entity.Instance.ID);
                if(ec != null)
                {
                    tbxcontractStartDate.Text = ec.startDate.ToString("yyyy-MM-dd");
                    tbxcontractEndDate.Text = ec.endDate.ToString("yyyy-MM-dd");
                    if (ec.recorderID != null)
                        ddlrecorder.SelectedValue = ec.recorderID;
                    if (ec.recorderdate != null)
                        tbxrecorderdate.Text = ec.recorderdate.Value.ToString("yyyy-MM-dd");
                    tbxcontractRemark.Text = ec.contractRemark;
                }
            }
            if (entity.decorationPredate != null)
                tbxdecorationPredate.Text = entity.decorationPredate.Value.ToString("yyyy-MM-dd HH:mm");
            if (entity.decorationdate != null)
                tbxdecorationdate.Text = entity.decorationdate.Value.ToString("yyyy-MM-dd HH:mm");
            tbxdecorationStatus.Text = entity.decorationStatus;
            if (entity.store_plan_date != null)
                tbxstore_plan_date.Text = entity.store_plan_date.Value.ToString("yyyy-MM-dd");
            if (entity.store_act_date != null)
                tbxstore_act_date.Text = entity.store_act_date.Value.ToString("yyyy-MM-dd");
            tbxRemark.Text = entity.Remark;
            tbxcreater.Text = entity.creater;
            if (entity.create_date != null)
                tbxcreate_date.Text = entity.create_date.Value.ToString("yyyy-MM-dd HH:mm");
            tbxaudit.Text = entity.auditer;
            if (entity.audit_date != null)
                tbxaudit_date.Text = entity.audit_date.Value.ToString("yyyy-MM-dd HH:mm");
            tbxeffecter.Text = entity.effecter;
            if (entity.effect_date != null)
                tbxeffect_date.Text = entity.effect_date.Value.ToString("yyyy-MM-dd HH:mm");
            if(entity.status > 10)
            {
                tbxstore_name.Enabled = false;
                ddlarea.Enabled = false;
                ddlcity.Enabled = false;
                ddlprovince.Enabled = false;
                ddlstreet.Enabled = false;
                tbxaddr.Enabled = false;
                tbxproperty_rights.Enabled = false;
                tbxlinkman.Enabled = false;
                tbxlink_tel.Enabled = false;
                ddlreviewer.Enabled = false;
                ddlexpander.Enabled = false;
                tbxRemark.Enabled = false;
                tbxfloor_space.Enabled = false;
                tbxuse_area.Enabled = false;
                ddlstoreProperty.Enabled = false;
                dtpreviewer_date.Enabled = false;
            }
            if (entity.attachmentExpand != null)
            {
                lb1.Text = "已上传";
                lb1.ForeColor = Color.Green;
            }
            if (entity.attachmentReview != null)
            {
                lb2.Text = "已上传";
                lb2.ForeColor = Color.Green;
            }
            if (entity.attachmentPic != null)
            {
                lb3.Text = "已上传";
                lb3.ForeColor = Color.Green;
            }
            tbxaddr.Focus();
        }
        public bool ValueDate()
        {
            if (string.IsNullOrWhiteSpace(this.ddlexpander.Text))
            {
                RadMessageBox.Show("请选择扫街人员","提示",MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlreviewer.Text))
            {
                RadMessageBox.Show("请选择考察人员", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.bm_store_expand entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("expand_no", entity.expand_no);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("store_name", entity.store_name);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("expand_no", entity.expand_no);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("store_name", entity.store_name);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                data.Add("auditer", entity.auditer);
                if (entity.audit_date != null)
                    data.Add("audit_date", entity.audit_date);
                data.Add("effecter", entity.effecter);
                if (entity.effect_date != null)
                    data.Add("effect_date", entity.effect_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }
        #endregion  

        void UpLoad(int type)
        {
            if (entity == null)
            {
                RadMessageBox.Show("请先保存单据后再上传", "提示", MessageBoxButtons.OK);
                return;
            }
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "打开";
            ofd.Filter = "所有文件|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                rwb.Text = "上传中";
                rwb.Visible = true;
                rwb.ShowText = true;
                rwb.StartWaiting();

                AreaUpLoad(ofd.FileName, type);
            }
           
            
        }

        void DownLoad(int type)
        {
            if(entity == null)
            {
                RadMessageBox.Show("请先保存单据后再下载", "提示", MessageBoxButtons.OK);
                return;
            }
            else
            {
                if (type == 1 && entity.attachmentExpand == null)
                {
                    RadMessageBox.Show("未上传", "提示", MessageBoxButtons.OK);
                    return;
                }
                else if (type == 2 && entity.attachmentReview == null)
                {
                    RadMessageBox.Show("未上传", "提示", MessageBoxButtons.OK);
                    return;
                }
                else if (type == 3 && entity.attachmentPic == null)
                {
                    RadMessageBox.Show("未上传", "提示", MessageBoxButtons.OK);
                    return;
                }
            }

            SaveFileDialog ofd = new SaveFileDialog();
            ofd.Title = "打开";
            ofd.Filter = "所有文件|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                rwb.Text = "下载中";
                rwb.Visible = true;
                rwb.ShowText = true;
                rwb.StartWaiting();

                AreaDownLoad(ofd.FileName, type);
            }
        }

        #region===上传下载===
        public void AreaUpLoad(string fileNamePath,int type)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    string fileName = fileNamePath.Substring(fileNamePath.LastIndexOf("\\") + 1);
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                     ConfigurationManager.ConnectionStrings[1].ConnectionString);
                    // Retrieve storage account from connection string.

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("yjh-hq");
                    CloudBlockBlob blockBlob0;
                    string filetop = "";
                    if (type == 1)
                    {
                        if (!string.IsNullOrEmpty(entity.attachmentExpand))
                        {
                            blockBlob0 = container.GetBlockBlobReference(entity.attachmentExpand);
                            // Delete the blob.
                            blockBlob0.Delete();
                        }
                        filetop = "attachmentExpand/";
                    }
                    else if(type == 2)
                    {
                        if (!string.IsNullOrEmpty(entity.attachmentReview))
                        {
                            blockBlob0 = container.GetBlockBlobReference(entity.attachmentReview);
                            // Delete the blob.
                            blockBlob0.Delete();
                        }
                        filetop = "attachmentReview/";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(entity.attachmentPic))
                        {
                            blockBlob0 = container.GetBlockBlobReference(entity.attachmentPic);
                            // Delete the blob.
                            blockBlob0.Delete();
                        }
                        filetop = "attachmentPic/";
                    }
                    DateTime d = OrgUnitService.GetDate();
                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference("expand/" + filetop +d.ToString("yyyyMMddHHmmss") + d.Millisecond.ToString() + fileName);


                    // Create or overwrite the "myblob" blob with contents from a local file.
                    using (var fileStream = System.IO.File.OpenRead(fileNamePath))
                    {
                        blockBlob.UploadFromStream(fileStream);
                        if(type == 1)
                            entity.attachmentExpand = "expand/" + filetop + d.ToString("yyyyMMddHHmmss") + d.Millisecond.ToString() + fileName;
                        else if(type == 2)
                            entity.attachmentReview = "expand/" + filetop + d.ToString("yyyyMMddHHmmss") + d.Millisecond.ToString() + fileName;
                        else
                            entity.attachmentPic = "expand/" + filetop + d.ToString("yyyyMMddHHmmss") + d.Millisecond.ToString() + fileName;
                        bm_store_expandService.Save(entity);
                        entity.Instance.AcceptChanges();
                    }
                    RadMessageBox.Show(this, "上传成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    rwb.StopWaiting();
                    rwb.Visible = false;
                    if (type == 1)
                    {
                        lb1.Text = "已上传";
                        lb1.ForeColor = Color.Green;
                    }
                    else if(type == 2)
                    {
                        lb2.Text = "已上传";
                        lb2.ForeColor = Color.Green;
                    }
                    else if (type == 3)
                    {
                        lb3.Text = "已上传";
                        lb3.ForeColor = Color.Green;
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "文件上传失败，请稍候重试", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    rwb.StopWaiting();
                    rwb.Visible = false;
                }
                ts.Complete();
            }
        }

        public void AreaDownLoad(string fileNamePath,int type)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    // Retrieve storage account from connection string.
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                         ConfigurationManager.ConnectionStrings[1].ConnectionString);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("yjh-hq");

                    // Retrieve reference to a blob named "photo1.jpg".
                    string url = "";
                    if (type == 1)
                        url = entity.attachmentExpand;
                    else if (type == 2)
                        url = entity.attachmentReview;
                    else
                        url = entity.attachmentPic;
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(url);

                    // Save blob contents to a file.
                    using (var fileStream = System.IO.File.OpenWrite(fileNamePath))
                    {
                        blockBlob.DownloadToStream(fileStream);
                    }
                    RadMessageBox.Show(this, "下载成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    rwb.StopWaiting();
                    rwb.Visible = false;
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "文件下载失败，请稍候重试", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    rwb.StopWaiting();
                    rwb.Visible = false;
                } 
                ts.Complete();
            }
        }
        #endregion
    }
}
