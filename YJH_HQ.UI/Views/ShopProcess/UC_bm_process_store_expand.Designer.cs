﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UC_bm_process_store_expand
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.dgExpand = new YJH_HQ.Controls.DataGrid.DataGrid();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnModify = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(959, 56);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "查询条件";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(136, 25);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(165, 20);
            this.tbxName.TabIndex = 40;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(117, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "扫街单号/小区名称：";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnSearch.Location = new System.Drawing.Point(318, 25);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSearch.Size = new System.Drawing.Size(86, 20);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "查询";
            // 
            // dgExpand
            // 
            gridViewTextBoxColumn1.FieldName = "expand_no";
            gridViewTextBoxColumn1.HeaderText = "扫街单号";
            gridViewTextBoxColumn1.Name = "clexpand_no";
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.HeaderText = "扫街状态";
            gridViewTextBoxColumn2.Name = "cl_status";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "expand_id";
            gridViewTextBoxColumn3.HeaderText = "拓展单号";
            gridViewTextBoxColumn3.Name = "clexpand_id";
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.FieldName = "expand_emp";
            gridViewTextBoxColumn4.HeaderText = "店面开发人员";
            gridViewTextBoxColumn4.Name = "clexpand_emp";
            gridViewTextBoxColumn4.Width = 100;
            gridViewTextBoxColumn5.Expression = "";
            gridViewTextBoxColumn5.FieldName = "province";
            gridViewTextBoxColumn5.HeaderText = "省份";
            gridViewTextBoxColumn5.Name = "clprovince";
            gridViewTextBoxColumn5.Width = 60;
            gridViewTextBoxColumn6.FieldName = "city";
            gridViewTextBoxColumn6.HeaderText = "城市";
            gridViewTextBoxColumn6.Name = "clcity";
            gridViewTextBoxColumn6.Width = 60;
            gridViewTextBoxColumn7.FieldName = "area";
            gridViewTextBoxColumn7.HeaderText = "市区";
            gridViewTextBoxColumn7.Name = "clarea";
            gridViewTextBoxColumn7.Width = 60;
            gridViewTextBoxColumn8.FieldName = "street";
            gridViewTextBoxColumn8.HeaderText = "街道";
            gridViewTextBoxColumn8.Name = "clstreet";
            gridViewTextBoxColumn8.Width = 60;
            gridViewTextBoxColumn9.FieldName = "community_name";
            gridViewTextBoxColumn9.HeaderText = "小区名称";
            gridViewTextBoxColumn9.Name = "clcommunity_name";
            gridViewTextBoxColumn9.Width = 120;
            gridViewTextBoxColumn10.FieldName = "creater";
            gridViewTextBoxColumn10.HeaderText = "录入人";
            gridViewTextBoxColumn10.Name = "clcreater";
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.FieldName = "create_date";
            gridViewTextBoxColumn11.FormatString = "{0:yyyy-MM-dd HH:mm}";
            gridViewTextBoxColumn11.HeaderText = "录入时间";
            gridViewTextBoxColumn11.Name = "clcreate_date";
            gridViewTextBoxColumn11.Width = 120;
            gridViewTextBoxColumn12.FieldName = "status";
            gridViewTextBoxColumn12.HeaderText = "状态";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "clstatus";
            this.dgExpand.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.dgExpand.Distinct = false;
            this.dgExpand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgExpand.Location = new System.Drawing.Point(0, 86);
            this.dgExpand.Name = "dgExpand";
            this.dgExpand.PageIndex = 0;
            this.dgExpand.PageSize = 20;
            this.dgExpand.ShowTotal = true;
            this.dgExpand.Size = new System.Drawing.Size(959, 527);
            this.dgExpand.TabIndex = 9;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnModify,
            this.btnDelete});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新单";
            this.btnNew.AccessibleName = "新单";
            this.btnNew.DisplayName = "commandBarButton3";
            this.btnNew.DrawText = true;
            this.btnNew.Image = global::YJH_HQ.UI.Properties.Resources.ok;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新单";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnModify
            // 
            this.btnModify.AccessibleDescription = "打开";
            this.btnModify.AccessibleName = "打开";
            this.btnModify.DisplayName = "commandBarButton1";
            this.btnModify.DrawText = true;
            this.btnModify.Image = global::YJH_HQ.UI.Properties.Resources.Notepad16;
            this.btnModify.Name = "btnModify";
            this.btnModify.Text = "修改";
            this.btnModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModify.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton1";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = global::YJH_HQ.UI.Properties.Resources.Delete16;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(959, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // UC_bm_process_store_expand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.dgExpand);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_bm_process_store_expand";
            this.Size = new System.Drawing.Size(959, 613);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Controls.DataGrid.DataGrid dgExpand;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnModify;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
    }
}
