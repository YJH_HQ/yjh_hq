﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UC_bm_process_store_expand : UserControl
    {
        #region ===字段及属性===

        #endregion

        #region ===构造方法===
        public UC_bm_process_store_expand()
        {
            InitializeComponent();
            this.dgExpand.QueryMethod = "YJH.ExpandService.Query";
            this.dgExpand.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += UC_bm_process_store_expand_Load;
            this.btnModify.Click += (s, e) => { this.OnOpen(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnDelete.Click += (s, e) => { this.Delete(); };
            this.btnSearch.Click += (s, e) => { this.dgExpand.LoadData(); };
            this.dgExpand.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
            this.dgExpand.GridView.CellFormatting += GridView_CellFormatting;
            this.LoadPermission();
        }

        #endregion

        #region ===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            //this.btnModify.Enabled = funcs.Exists(t => t.Name == "修改");
            //this.btnSend.Enabled = funcs.Exists(t => t.Name == "新单");
        }

        //修改
        private void OnOpen()
        {
            var id = this.dgExpand.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.bm_process_store_expand(dps.Common.Data.Entity.Retrieve(YJH.Entities.bm_process_store_expand.EntityModelID, id));
            var view = new UI_ExpandView();
            view.dgList = this.dgExpand;
            view.Entity = entity;
            view.ShowDialog();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = this.tbxName.Text;
            return args;
        }

        /// <summary>
        /// 新建
        /// </summary>
        private void New()
        {
            var entity = new YJH.Entities.bm_process_store_expand();
            var view = new UI_ExpandView();
            view.dgList = this.dgExpand;
            view.Entity = entity;
            view.ShowDialog();
        }
        //删除
        private void Delete()
        {
            var id = this.dgExpand.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (this.dgExpand.GridView.SelectedRows[0].Cells["cl_status"].Value.ToString() != "待拓展")
            {
                RadMessageBox.Show("只能删除待拓展的数据!");
                return;
            } 
            if (RadMessageBox.Show("确定删除数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                YJH.Services.ExpandService.Delete(id);
                this.dgExpand.GridView.SelectedRows[0].Delete();
                this.dgExpand.GridView.Refresh();
                RadMessageBox.Show("删除成功!");
            }
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载vava
        /// </summary>
        /// <remarks>建立人：张殿元 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_bm_process_store_expand_Load(object sender, EventArgs e)
        {
            this.dgExpand.LoadData();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgExpand == null)
            {
                return;
            }
            switch (this.dgExpand.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString())
            {
                case "0":
                    this.dgExpand.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "待拓展";
                    break;
                case "1":
                    this.dgExpand.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "已拓展";
                    break;
                case "2":
                    this.dgExpand.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "已放弃";
                    break;
            }
        }
        #endregion
    }
}
