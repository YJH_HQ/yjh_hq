﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UI_ContractInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxcontract_no = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcontractRemark = new Telerik.WinControls.UI.RadTextBox();
            this.dtprecorderdate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpcontractEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpcontractStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.ddlrecorder = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontract_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtprecorderdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxcontract_no);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxcontractRemark);
            this.radGroupBox1.Controls.Add(this.dtprecorderdate);
            this.radGroupBox1.Controls.Add(this.dtpcontractEndDate);
            this.radGroupBox1.Controls.Add(this.dtpcontractStartDate);
            this.radGroupBox1.Controls.Add(this.ddlrecorder);
            this.radGroupBox1.Controls.Add(this.radLabel29);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(382, 255);
            this.radGroupBox1.TabIndex = 3;
            // 
            // tbxcontract_no
            // 
            this.tbxcontract_no.Location = new System.Drawing.Point(135, 29);
            this.tbxcontract_no.Name = "tbxcontract_no";
            this.tbxcontract_no.Size = new System.Drawing.Size(110, 20);
            this.tbxcontract_no.TabIndex = 106;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(54, 31);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 105;
            this.radLabel3.Text = "合同编号：";
            // 
            // tbxcontractRemark
            // 
            this.tbxcontractRemark.AutoSize = false;
            this.tbxcontractRemark.Location = new System.Drawing.Point(135, 157);
            this.tbxcontractRemark.Multiline = true;
            this.tbxcontractRemark.Name = "tbxcontractRemark";
            this.tbxcontractRemark.Size = new System.Drawing.Size(214, 65);
            this.tbxcontractRemark.TabIndex = 104;
            // 
            // dtprecorderdate
            // 
            this.dtprecorderdate.Location = new System.Drawing.Point(135, 131);
            this.dtprecorderdate.Name = "dtprecorderdate";
            this.dtprecorderdate.Size = new System.Drawing.Size(110, 20);
            this.dtprecorderdate.TabIndex = 103;
            this.dtprecorderdate.TabStop = false;
            this.dtprecorderdate.Text = "2015年12月16日";
            this.dtprecorderdate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // dtpcontractEndDate
            // 
            this.dtpcontractEndDate.Location = new System.Drawing.Point(135, 79);
            this.dtpcontractEndDate.Name = "dtpcontractEndDate";
            this.dtpcontractEndDate.Size = new System.Drawing.Size(110, 20);
            this.dtpcontractEndDate.TabIndex = 102;
            this.dtpcontractEndDate.TabStop = false;
            this.dtpcontractEndDate.Text = "2015年12月16日";
            this.dtpcontractEndDate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // dtpcontractStartDate
            // 
            this.dtpcontractStartDate.Location = new System.Drawing.Point(135, 52);
            this.dtpcontractStartDate.Name = "dtpcontractStartDate";
            this.dtpcontractStartDate.Size = new System.Drawing.Size(110, 20);
            this.dtpcontractStartDate.TabIndex = 101;
            this.dtpcontractStartDate.TabStop = false;
            this.dtpcontractStartDate.Text = "2015年12月16日";
            this.dtpcontractStartDate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // ddlrecorder
            // 
            this.ddlrecorder.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlrecorder.Location = new System.Drawing.Point(135, 105);
            this.ddlrecorder.Name = "ddlrecorder";
            this.ddlrecorder.Size = new System.Drawing.Size(110, 20);
            this.ddlrecorder.TabIndex = 100;
            // 
            // radLabel29
            // 
            this.radLabel29.Location = new System.Drawing.Point(54, 157);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(65, 18);
            this.radLabel29.TabIndex = 99;
            this.radLabel29.Text = "合同情况：";
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(30, 133);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(89, 18);
            this.radLabel22.TabIndex = 96;
            this.radLabel22.Text = "合同签约日期：";
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(42, 107);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(77, 18);
            this.radLabel23.TabIndex = 97;
            this.radLabel23.Text = "合同签约人：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(30, 81);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(89, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "合同结束日期：";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(30, 55);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(89, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "合同开始日期：";
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(382, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // UI_ContractInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(382, 285);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_ContractInfo";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "合同信息";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontract_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtprecorderdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker dtprecorderdate;
        private Telerik.WinControls.UI.RadDateTimePicker dtpcontractEndDate;
        private Telerik.WinControls.UI.RadDateTimePicker dtpcontractStartDate;
        private Telerik.WinControls.UI.RadDropDownList ddlrecorder;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbxcontractRemark;
        private Telerik.WinControls.UI.RadTextBox tbxcontract_no;
        private Telerik.WinControls.UI.RadLabel radLabel3;
    }
}