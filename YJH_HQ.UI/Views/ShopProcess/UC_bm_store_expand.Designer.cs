﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UC_bm_store_expand
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddlstatus = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnModify = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btnAudit = new Telerik.WinControls.UI.CommandBarButton();
            this.btnEffect = new Telerik.WinControls.UI.CommandBarButton();
            this.btnContract = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDecoration = new Telerik.WinControls.UI.CommandBarButton();
            this.btnPlan = new Telerik.WinControls.UI.CommandBarButton();
            this.btnFinish = new Telerik.WinControls.UI.CommandBarButton();
            this.btnCancel = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.dgExpand = new YJH_HQ.Controls.DataGrid.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddlstatus);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 55);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1137, 56);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "查询条件";
            // 
            // ddlstatus
            // 
            this.ddlstatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Tag = "0";
            radListDataItem1.Text = "全部";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem1.TextWrap = true;
            radListDataItem2.Tag = "10";
            radListDataItem2.Text = "新建";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem2.TextWrap = true;
            radListDataItem3.Tag = "20";
            radListDataItem3.Text = "稽核";
            radListDataItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem3.TextWrap = true;
            radListDataItem4.Tag = "30";
            radListDataItem4.Text = "审批";
            radListDataItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem4.TextWrap = true;
            radListDataItem5.Tag = "40";
            radListDataItem5.Text = "合同已签";
            radListDataItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem5.TextWrap = true;
            radListDataItem6.Tag = "50";
            radListDataItem6.Text = "装修完成";
            radListDataItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem6.TextWrap = true;
            radListDataItem7.Tag = "60";
            radListDataItem7.Text = "计划开店";
            radListDataItem7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem7.TextWrap = true;
            radListDataItem8.Tag = "70";
            radListDataItem8.Text = "开业";
            radListDataItem8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem8.TextWrap = true;
            this.ddlstatus.Items.Add(radListDataItem1);
            this.ddlstatus.Items.Add(radListDataItem2);
            this.ddlstatus.Items.Add(radListDataItem3);
            this.ddlstatus.Items.Add(radListDataItem4);
            this.ddlstatus.Items.Add(radListDataItem5);
            this.ddlstatus.Items.Add(radListDataItem6);
            this.ddlstatus.Items.Add(radListDataItem7);
            this.ddlstatus.Items.Add(radListDataItem8);
            this.ddlstatus.Location = new System.Drawing.Point(372, 25);
            this.ddlstatus.Name = "ddlstatus";
            this.ddlstatus.Size = new System.Drawing.Size(82, 20);
            this.ddlstatus.TabIndex = 42;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(324, 26);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 41;
            this.radLabel1.Text = "状态：";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(136, 25);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(165, 20);
            this.tbxName.TabIndex = 40;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(117, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "拓展单号/店铺名称：";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnSearch.Location = new System.Drawing.Point(1037, 25);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSearch.Size = new System.Drawing.Size(86, 20);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "查询";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnModify,
            this.btnDelete,
            this.btnAudit,
            this.btnEffect,
            this.btnContract,
            this.btnDecoration,
            this.btnPlan,
            this.btnFinish,
            this.btnCancel});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新单";
            this.btnNew.AccessibleName = "新单";
            this.btnNew.DisplayName = "commandBarButton3";
            this.btnNew.DrawText = true;
            this.btnNew.Image = global::YJH_HQ.UI.Properties.Resources.Add16;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnModify
            // 
            this.btnModify.AccessibleDescription = "打开";
            this.btnModify.AccessibleName = "打开";
            this.btnModify.DisplayName = "commandBarButton1";
            this.btnModify.DrawText = true;
            this.btnModify.Image = global::YJH_HQ.UI.Properties.Resources.Notepad16;
            this.btnModify.Name = "btnModify";
            this.btnModify.Text = "编辑";
            this.btnModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModify.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton1";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = global::YJH_HQ.UI.Properties.Resources.Delete16;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnAudit
            // 
            this.btnAudit.AccessibleDescription = "稽核";
            this.btnAudit.AccessibleName = "稽核";
            this.btnAudit.DisplayName = "commandBarButton1";
            this.btnAudit.DrawText = true;
            this.btnAudit.Image = global::YJH_HQ.UI.Properties.Resources.ToGenerate_Orders;
            this.btnAudit.Name = "btnAudit";
            this.btnAudit.Text = "稽核";
            this.btnAudit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAudit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnEffect
            // 
            this.btnEffect.AccessibleDescription = "审批";
            this.btnEffect.AccessibleName = "审批";
            this.btnEffect.DisplayName = "commandBarButton1";
            this.btnEffect.DrawText = true;
            this.btnEffect.Image = global::YJH_HQ.UI.Properties.Resources.Acceptance;
            this.btnEffect.Name = "btnEffect";
            this.btnEffect.Text = "审批";
            this.btnEffect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEffect.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnContract
            // 
            this.btnContract.AccessibleDescription = "合同签订";
            this.btnContract.AccessibleName = "合同签订";
            this.btnContract.DisplayName = "commandBarButton1";
            this.btnContract.DrawText = true;
            this.btnContract.Image = global::YJH_HQ.UI.Properties.Resources.AuditButtonImage;
            this.btnContract.Name = "btnContract";
            this.btnContract.Text = "合同签订";
            this.btnContract.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnContract.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDecoration
            // 
            this.btnDecoration.AccessibleDescription = "装修";
            this.btnDecoration.AccessibleName = "装修";
            this.btnDecoration.DisplayName = "commandBarButton1";
            this.btnDecoration.DrawText = true;
            this.btnDecoration.Image = global::YJH_HQ.UI.Properties.Resources.Selected;
            this.btnDecoration.Name = "btnDecoration";
            this.btnDecoration.Text = "装修";
            this.btnDecoration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDecoration.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnPlan
            // 
            this.btnPlan.AccessibleDescription = "计划开店";
            this.btnPlan.AccessibleName = "计划开店";
            this.btnPlan.DisplayName = "commandBarButton1";
            this.btnPlan.DrawText = true;
            this.btnPlan.Image = global::YJH_HQ.UI.Properties.Resources.copy;
            this.btnPlan.Name = "btnPlan";
            this.btnPlan.Text = "计划开店";
            this.btnPlan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPlan.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnFinish
            // 
            this.btnFinish.AccessibleDescription = "开业";
            this.btnFinish.AccessibleName = "开业";
            this.btnFinish.DisplayName = "commandBarButton1";
            this.btnFinish.DrawText = true;
            this.btnFinish.Image = global::YJH_HQ.UI.Properties.Resources.ok;
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Text = "开业";
            this.btnFinish.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFinish.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleDescription = "取消";
            this.btnCancel.AccessibleName = "取消";
            this.btnCancel.DisplayName = "commandBarButton1";
            this.btnCancel.DrawText = true;
            this.btnCancel.Image = global::YJH_HQ.UI.Properties.Resources.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Text = "取消";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1137, 55);
            this.radCommandBar1.TabIndex = 5;
            // 
            // dgExpand
            // 
            gridViewTextBoxColumn1.FieldName = "ID";
            gridViewTextBoxColumn1.HeaderText = "column1";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "clID";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "expand_no";
            gridViewTextBoxColumn2.HeaderText = "拓展单号";
            gridViewTextBoxColumn2.Name = "clexpand_no";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "province";
            gridViewTextBoxColumn3.HeaderText = "省份";
            gridViewTextBoxColumn3.Name = "clprovince";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 70;
            gridViewTextBoxColumn4.FieldName = "city";
            gridViewTextBoxColumn4.HeaderText = "城市";
            gridViewTextBoxColumn4.Name = "clcity";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 70;
            gridViewTextBoxColumn5.FieldName = "area";
            gridViewTextBoxColumn5.HeaderText = "市区";
            gridViewTextBoxColumn5.Name = "clarea";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 70;
            gridViewTextBoxColumn6.FieldName = "street";
            gridViewTextBoxColumn6.HeaderText = "街道";
            gridViewTextBoxColumn6.Name = "clstreet";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.FieldName = "store_name";
            gridViewTextBoxColumn7.HeaderText = "店铺名称";
            gridViewTextBoxColumn7.Name = "clstore_name";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn8.FieldName = "status";
            gridViewTextBoxColumn8.FormatString = "{0:G}";
            gridViewTextBoxColumn8.HeaderText = "状态";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "clstatus";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 60;
            gridViewTextBoxColumn9.FieldName = "creater";
            gridViewTextBoxColumn9.HeaderText = "录入人";
            gridViewTextBoxColumn9.Name = "clcreater";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 60;
            gridViewDateTimeColumn1.FieldName = "create_date";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd HH:mm}";
            gridViewDateTimeColumn1.HeaderText = "录入时间";
            gridViewDateTimeColumn1.Name = "clcreate_date";
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 120;
            gridViewTextBoxColumn10.FieldName = "auditer";
            gridViewTextBoxColumn10.HeaderText = "稽核人";
            gridViewTextBoxColumn10.Name = "clauditer";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.Width = 60;
            gridViewDateTimeColumn2.FieldName = "audit_date";
            gridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd HH:mm}";
            gridViewDateTimeColumn2.HeaderText = "稽核时间";
            gridViewDateTimeColumn2.Name = "claudit_date";
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 120;
            gridViewTextBoxColumn11.FieldName = "effecter";
            gridViewTextBoxColumn11.HeaderText = "审批人";
            gridViewTextBoxColumn11.Name = "cleffecter";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.Width = 60;
            gridViewDateTimeColumn3.FieldName = "effect_date";
            gridViewDateTimeColumn3.FormatString = "{0:yyyy-MM-dd HH:mm}";
            gridViewDateTimeColumn3.HeaderText = "审批时间";
            gridViewDateTimeColumn3.Name = "cleffect_date";
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn3.Width = 120;
            gridViewTextBoxColumn12.HeaderText = "状态";
            gridViewTextBoxColumn12.Name = "cl_status";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 100;
            this.dgExpand.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn10,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn11,
            gridViewDateTimeColumn3,
            gridViewTextBoxColumn12});
            this.dgExpand.Distinct = false;
            this.dgExpand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgExpand.Location = new System.Drawing.Point(0, 111);
            this.dgExpand.Name = "dgExpand";
            this.dgExpand.PageIndex = 0;
            this.dgExpand.PageSize = 20;
            this.dgExpand.ShowTotal = true;
            this.dgExpand.Size = new System.Drawing.Size(1137, 502);
            this.dgExpand.TabIndex = 9;
            // 
            // UC_bm_store_expand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.dgExpand);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_bm_store_expand";
            this.Size = new System.Drawing.Size(1137, 613);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Controls.DataGrid.DataGrid dgExpand;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnModify;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private Telerik.WinControls.UI.RadDropDownList ddlstatus;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.CommandBarButton btnAudit;
        private Telerik.WinControls.UI.CommandBarButton btnEffect;
        private Telerik.WinControls.UI.CommandBarButton btnContract;
        private Telerik.WinControls.UI.CommandBarButton btnDecoration;
        private Telerik.WinControls.UI.CommandBarButton btnPlan;
        private Telerik.WinControls.UI.CommandBarButton btnFinish;
        private Telerik.WinControls.UI.CommandBarButton btnCancel;
    }
}

