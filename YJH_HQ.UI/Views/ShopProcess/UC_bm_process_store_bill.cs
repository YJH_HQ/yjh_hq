﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH_HQ.UI.Views.Store;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UC_bm_process_store_bill : UserControl
    {
       #region ===字段及属性===

        #endregion

        #region ===构造方法===
        public UC_bm_process_store_bill()
        {
            InitializeComponent();
            this.dgBill.QueryMethod = "YJH.BillService.Query";
            this.dgBill.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += UC_bm_process_store_bill_Load;
            this.btnModify.Click += (s, e) => { this.OnOpen(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnDelete.Click += (s, e) => { this.Delete(); };
            this.btnAudit.Click += (s, e) => { this.Audit(); };
            this.btnEffecter.Click += (s, e) => { this.Effecter(); };
            this.btnSearch.Click += (s, e) => { this.dgBill.LoadData(); };
            this.dgBill.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
            this.dgBill.GridView.CellFormatting += GridView_CellFormatting;
            this.LoadPermission();
        }

        #endregion

        #region ===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            //this.btnModify.Enabled = funcs.Exists(t => t.Name == "修改");
            //this.btnSend.Enabled = funcs.Exists(t => t.Name == "新单");
        }

        //修改
        private void OnOpen()
        {
            var id = this.dgBill.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.bm_process_store_bill(dps.Common.Data.Entity.Retrieve(YJH.Entities.bm_process_store_bill.EntityModelID, id));
            var view = new UI_BillView();
            view.dgList = this.dgBill;
            view.Entity = entity;
            view.ShowDialog();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = this.tbxName.Text;
            return args;
        }

        /// <summary>
        /// 新建
        /// </summary>
        private void New()
        {
            var entity = new YJH.Entities.bm_process_store_bill();
            var view = new UI_BillView();
            view.dgList = this.dgBill;
            view.Entity = entity;
            view.ShowDialog();
        }
        //删除
        private void Delete()
        {
            var id = this.dgBill.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (this.dgBill.GridView.SelectedRows[0].Cells["cl_status"].Value.ToString() != "新单")
            {
                RadMessageBox.Show("只能删除新单的数据!");
                return;
            } 
            if (RadMessageBox.Show("确定删除数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                YJH.Services.BillService.Delete(id);
                this.dgBill.GridView.SelectedRows[0].Delete();
                this.dgBill.GridView.Refresh();
                RadMessageBox.Show("删除成功!");
            }
        }

        //审核
        private void Audit()
        {
            var id = this.dgBill.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (this.dgBill.GridView.SelectedRows[0].Cells["cl_status"].Value.ToString() != "新单")
            {
                RadMessageBox.Show("只能选择新单的数据!");
                return;
            }
            if (RadMessageBox.Show("确定审核数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var entity = new YJH.Entities.bm_process_store_bill(dps.Common.Data.Entity.Retrieve(YJH.Entities.bm_process_store_bill.EntityModelID, id));
                entity.auditer = Globle.CurrentEmployee.Base.Name;
                entity.audit_date = System.DateTime.Now;
                entity.status = 1;
                YJH.Services.BillService.Audit(entity);

                var data = new Dictionary<string, object>();
                data.Add("auditer", entity.auditer);
                data.Add("audit_date", entity.audit_date);
                data.Add("status", entity.status);
                this.dgBill.RefreshSelectedRow(data);
                RadMessageBox.Show("审核成功!");
            }
        }

        //稽核
        private void Effecter()
        {
            var id = this.dgBill.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (this.dgBill.GridView.SelectedRows[0].Cells["cl_status"].Value.ToString() != "审核")
            {
                RadMessageBox.Show("只能选择审核的数据!");
                return;
            }
            if (RadMessageBox.Show("确定稽核数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var entity = new YJH.Entities.bm_process_store_bill(dps.Common.Data.Entity.Retrieve(YJH.Entities.bm_process_store_bill.EntityModelID, id));

                entity.area_audit = 1;
                entity.size_audit = 1;
                entity.structure_audit = 1;
                entity.rate_audit = 1;
                entity.people_audit = 1;
                entity.parking_audit = 1;
                entity.door_audit = 1;
                entity.household_audit = 1;
                entity.facade_audit = 1;

                entity.effecter = Globle.CurrentEmployee.Base.Name;
                entity.effect_date = System.DateTime.Now;
                entity.status = 2;
                YJH.Services.BillService.Effecter(entity);

                var data = new Dictionary<string, object>();
                data.Add("effecter", entity.effecter);
                data.Add("effect_date", entity.effect_date);
                data.Add("status", entity.status);
                this.dgBill.RefreshSelectedRow(data);
                RadMessageBox.Show("稽核成功!");
            }
        }

        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载vava
        /// </summary>
        /// <remarks>建立人：张殿元 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_bm_process_store_bill_Load(object sender, EventArgs e)
        {
            this.dgBill.LoadData();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgBill == null)
            {
                return;
            }
            switch (this.dgBill.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString())
            {
                case "0":
                    this.dgBill.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "新单";
                    break;
                case "1":
                    this.dgBill.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "审核";
                    break;
                case "2":
                    this.dgBill.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = "生效";
                    break;
            }
        }
        #endregion
    }
}
