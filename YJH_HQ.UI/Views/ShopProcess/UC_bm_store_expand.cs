﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;
using YJH.Entities;
using YJH.Enums;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UC_bm_store_expand : UserControl
    {
        #region===字段===
        public int status = 0;
        #endregion

        #region===构造===
        public UC_bm_store_expand()
        {
            InitializeComponent();
            this.Load += UC_bm_store_expand_Load;
            LoadPermission();
            this.btnNew.Click += btnNew_Click;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.dgExpand.GridView.CellFormatting += GridView_CellFormatting;
            this.btnDelete.Click += btnDelete_Click;
            this.btnModify.Click += (s, e) => { this.Edit(); };
            this.dgExpand.GridView.DoubleClick += GridView_DoubleClick;
            btnAudit.Click += btnAudit_Click;
            btnEffect.Click += btnEffect_Click;
            btnContract.Click += btnContract_Click;
            btnDecoration.Click += btnDecoration_Click;
            btnPlan.Click += btnPlan_Click;
            btnFinish.Click += btnFinish_Click;
            btnCancel.Click += btnCancel_Click;
        }
        #endregion

        #region===事件===
        void btnCancel_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() == "10")
                return;
            if (RadMessageBox.Show("确认取消吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_expand entity = bm_store_expandService.GetByID(Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        bm_store_expandService.Cancel(entity);
                        RadMessageBox.Show(this, "取消成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "取消失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        void btnFinish_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "60")
            {
                RadMessageBox.Show(this, "只允许开业状态为计划开店的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_Store_Act ci = new UI_Store_Act();
            ci.StartPosition = FormStartPosition.CenterScreen;
            ci.dgList = this.dgExpand;
            ci.id = Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString());
            ci.ShowDialog();
        }
        void btnPlan_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "50")
            {
                RadMessageBox.Show(this, "只允许计划开店状态为装修完成的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_Store_Plan ci = new UI_Store_Plan();
            ci.StartPosition = FormStartPosition.CenterScreen;
            ci.dgList = this.dgExpand;
            ci.id = Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString());
            ci.ShowDialog();
        }
        void btnDecoration_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "40")
            {
                RadMessageBox.Show(this, "只允许装修状态为合同已签的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_Decoration ci = new UI_Decoration();
            ci.StartPosition = FormStartPosition.CenterScreen;
            ci.dgList = this.dgExpand;
            ci.id = Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString());
            ci.ShowDialog();
        }
        void btnContract_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "30")
            {
                RadMessageBox.Show(this, "只允许合同签订状态为审批的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_ContractInfo ci = new UI_ContractInfo();
            ci.StartPosition = FormStartPosition.CenterScreen;
            ci.dgList = this.dgExpand;
            ci.id = Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString());
            ci.ShowDialog();
        }
        void btnEffect_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "20")
            {
                RadMessageBox.Show(this, "只允许审批状态为稽核的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认审批吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_expand entity = bm_store_expandService.GetByID(Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        entity.effecter = Globle.CurrentEmployee.Base.Name;
                        entity.status = 30;
                        bm_store_expandService.Effect(entity);
                        RadMessageBox.Show(this, "审批成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "审批失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        void btnAudit_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "10")
            {
                RadMessageBox.Show(this, "只允许稽核状态为新建的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认稽核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_expand entity = bm_store_expandService.GetByID(Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        entity.auditer = Globle.CurrentEmployee.Base.Name;
                        entity.status = 20;
                        bm_store_expandService.Audit(entity);
                        RadMessageBox.Show(this, "稽核成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "稽核失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void GridView_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }
        
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
            if (dgExpand.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "10")
            {
                RadMessageBox.Show(this, "只允许删除状态为新建的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_expandService.Delete(Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "删除成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    Search();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "删除失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            UI_bm_store_expand ex = new UI_bm_store_expand();
            ex.StartPosition = FormStartPosition.CenterScreen;
            ex.dgList = this.dgExpand;
            ex.ShowDialog();
        }

        void UC_bm_store_expand_Load(object sender, EventArgs e)
        {
            ddlstatus.SelectedIndex = 0;
            Search();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgExpand == null)
            {
                return;
            }
            if (this.dgExpand.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString() != "")
                this.dgExpand.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = Enum.GetName(typeof(YJH.Enums.expandstatus), int.Parse(this.dgExpand.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString()));
        }
        #endregion

        #region===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
            this.btnAudit.Enabled = funcs.Exists(t => t.Name == "稽核");
            this.btnEffect.Enabled = funcs.Exists(t => t.Name == "审批");
            this.btnContract.Enabled = funcs.Exists(t => t.Name == "合同签订");
            this.btnDecoration.Enabled = funcs.Exists(t => t.Name == "装修");
            this.btnPlan.Enabled = funcs.Exists(t => t.Name == "计划开店");
            this.btnFinish.Enabled = funcs.Exists(t => t.Name == "开业");
            this.btnCancel.Enabled = funcs.Exists(t => t.Name == "取消");
        }

        void Edit()
        {
             var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            if(funcs.Exists(t => t.Name == "编辑"))
            {
                if (dgExpand.GridView.SelectedRows.Count == 0)
                return;
                bm_store_expand entity = bm_store_expandService.GetByID(Guid.Parse(dgExpand.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                if (entity != null)
                {
                    UI_bm_store_expand ex = new UI_bm_store_expand();
                    ex.StartPosition = FormStartPosition.CenterScreen;
                    ex.dgList = this.dgExpand;
                    ex.entity = entity;
                    ex.IsNew = false;
                    ex.ShowDialog();
                }
            }
            
        }
        void Search()
        {
            dgExpand.GridView.DataSource = bm_store_expandService.Search(tbxName.Text.Trim(), int.Parse(ddlstatus.SelectedItem.Tag.ToString()));
        }
        #endregion
    }
}
