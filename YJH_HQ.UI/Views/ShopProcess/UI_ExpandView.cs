﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Entities;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_ExpandView : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private YJH.Entities.bm_process_store_expand _entity;
        public YJH.Entities.bm_process_store_expand Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ====构造====

        public UI_ExpandView()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddlprovince.SelectedIndexChanged += ddlprovince_SelectedIndexChanged;
            this.ddlcity.SelectedIndexChanged += ddlcity_SelectedIndexChanged;
            this.ddlarea.SelectedIndexChanged += ddlarea_SelectedIndexChanged;

            //省
            List<ba_district> list = YJH.Services.StoreMngService.Getprovince();
            ddlprovince.ValueMember = "ownerid";
            ddlprovince.DisplayMember = "city_name";
            ddlprovince.DataSource = list;
            ddlprovince.SelectedIndex = 9;

            //扫街人员
            this.epexpand_emp.PickerView = new YJH_HQ.Controls.EntityPickerView.EmploeeTreePickerView(Globle.CurrentBusinessUnitOU.Instance.ID);
            this.epexpand_emp.DisplayMember = "Name";

            this.dtexpand_date.Value = System.DateTime.Now;
            this.dtcontract_enddate.Value = System.DateTime.Now;

            this.tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            this.tbxcreate_date.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            this.tbxstatus.Text = "待拓展";
        }
        #endregion

        #region ====方法====
        private void BindData()
        {
            if (_entity.Instance.PersistentState ==dps.Common.Data.PersistentState.Detached)
                return;
            this.tbxexpand_no.Text = _entity.expand_no;
            if (_entity.status == 0)
            {
                this.tbxstatus.Text = "待拓展";
            }
            else if (_entity.status == 1)
            {
                this.tbxstatus.Text = "已拓展";
            }
            else
            {
                this.tbxstatus.Text = "已放弃";
            }
            this.dtexpand_date.Value = _entity.expand_date;
            //扫街人员
            this.epexpand_emp.SelectedEntity = YJH.Services.ExpandService.GetEmploee(_entity.expand_emp);
            this.tbxcommunity_name.Text = _entity.community_name;
            this.tbxaddr.Text = _entity.addr;
            this.ddlprovince.Text = _entity.province;
            this.ddlcity.Text = _entity.city;
            this.ddlarea.Text = _entity.area;
            this.ddlstreet.Text = _entity.street;
            this.tbxowner.Text = _entity.owner;
            this.tbxlandlord_tel.Text = _entity.landlord_tel;
            this.tbxcompetitor.Text = _entity.competitor;
            if (_entity.room_property == 0)
            {
                this.ddlroom_property.Text = "门店";
            }
            else if (_entity.room_property == 1)
            {
                this.ddlroom_property.Text = "社区配套用房";
            }
            else
            {
                this.ddlroom_property.Text = "移动箱房";
            }
            this.dtcontract_enddate.Value = _entity.contract_enddate;
            this.tbxexpand_id.Text = _entity.expand_id;
            this.cbis_bill.Checked = _entity.is_bill == 0 ? false : true;
            this.tbxcreater.Text = _entity.creater;
            this.tbxcreate_date.Text = _entity.create_date.ToString("yyyy-MM-dd HH:mm");
            this.tbxdescription.Text = _entity.description;
            this.tbxformat_near.Text = _entity.format_near;
            this.tbxappraise.Text = _entity.appraise;
        }

        private void FlushData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                string no = YJH.Services.ExpandService.GetNO();
                if (!string.IsNullOrWhiteSpace(no))
                {
                    _entity.expand_no = (Convert.ToInt32(no) + 1).ToString().PadLeft(7,'0');
                }
                else
                {
                    _entity.expand_no = "0010001";
                }

                _entity.status = 0;
                _entity.creater = this.tbxcreater.Text;
                _entity.create_date = System.DateTime.Now;
            }
            _entity.expand_date = Convert.ToDateTime(this.dtexpand_date.Value.ToString());
            //扫街人员
            _entity.expand_emp = ((sys.Entities.Emploee)this.epexpand_emp.SelectedEntity).Account;
            _entity.community_name = this.tbxcommunity_name.Text;
            _entity.addr = this.tbxaddr.Text;
            _entity.province = this.ddlprovince.Text;
            _entity.city = this.ddlcity.Text;
            _entity.area = this.ddlarea.Text;
            _entity.street = this.ddlstreet.Text;
            _entity.owner = this.tbxowner.Text;
            _entity.landlord_tel = this.tbxlandlord_tel.Text;
            _entity.competitor = this.tbxcompetitor.Text;
            if (this.ddlroom_property.Text == "门店")
            {
                _entity.room_property = 0;
            }
            else if (this.ddlroom_property.Text == "社区配套用房")
            {
                _entity.room_property = 1;
            }
            else
            {
                _entity.room_property = 2;
            }
            _entity.contract_enddate = Convert.ToDateTime(this.dtcontract_enddate.Value.ToString());
            //this.tbxexpand_id.Text = _entity.expand_id;
            _entity.is_bill = this.cbis_bill.Checked == false ? 0 : 1;
            _entity.description = this.tbxdescription.Text;
            _entity.format_near = this.tbxformat_near.Text;
            _entity.appraise = this.tbxappraise.Text;
        }

        private void OnLoad()
        {
            this.tbxexpand_no.Focus();
            this.LoadPermission();
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.bm_process_store_expand entity)
        {
            sys.Entities.Emploee e = YJH.Services.ExpandService.GetEmploee(_entity.expand_emp);
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("expand_no", entity.expand_no);
                data.Add("status", entity.status);
                data.Add("expand_id", entity.expand_id);
                data.Add("expand_emp", e.Base.Name);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("community_name", entity.community_name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("expand_no", entity.expand_no);
                data.Add("status", entity.status);
                data.Add("expand_id", entity.expand_id); 
                data.Add("expand_emp", e.Base.Name);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("community_name", entity.community_name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        private void Save()
        {
            try
            {
                if (Validate())
                {
                    this.FlushData();
                    //记录保存前的实体状态
                    var entityState = _entity.Instance.PersistentState;
                    YJH.Services.ExpandService.Save(_entity);
                    _entity.Instance.AcceptChanges();
                    //刷新列表
                    this.RefreshList(entityState, _entity);
                    this.Close();
                    //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        bool Validate()
        {
            if (string.IsNullOrWhiteSpace(this.epexpand_emp.Text))
            {
                MessageBox.Show("请选择扫街人员");
                return false;
            }
            else
            {
                return true;
            }
        }

        void ddlarea_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载街道
            if (ddlarea.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlarea.SelectedValue.ToString()));
                ddlstreet.ValueMember = "ownerid";
                ddlstreet.DisplayMember = "city_name";
                ddlstreet.DataSource = list;
                ddlstreet.SelectedIndex = -1;
            }
        }

        void ddlcity_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载区
            if (ddlcity.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlcity.SelectedValue.ToString()));
                ddlarea.ValueMember = "ownerid";
                ddlarea.DisplayMember = "city_name";
                ddlarea.DataSource = list;
                ddlarea.SelectedIndex = -1;
            }
        }

        void ddlprovince_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载市
            if (ddlprovince.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlprovince.SelectedValue.ToString()));
                ddlcity.ValueMember = "ownerid";
                ddlcity.DisplayMember = "city_name";
                ddlcity.DataSource = list;
                ddlcity.SelectedIndex = -1;
            }
        }
        #endregion
    }
}
