﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_bm_store_contract : Telerik.WinControls.UI.RadForm
    {
        #region===字段===
        public bool IsNew = false;
        public YJH.Entities.bm_store_contract entity;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        #endregion
        #region===构造===
        public UI_bm_store_contract()
        {
            InitializeComponent();
            this.Load += UI_bm_store_contract_Load;
            this.btSave.Click += btSave_Click;
        }
        #endregion

        #region===事件===
        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValueDate())
                {
                    FulshDate();
                    var entityState = entity.Instance.PersistentState;
                    bm_store_contractService.Save(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
        void UI_bm_store_contract_Load(object sender, EventArgs e)
        {
            ddlrecorder.DisplayMember = "Name";
            ddlrecorder.ValueMember = "ID";
            ddlrecorder.DataSource = bm_store_expandService.GetEmp();

            ddlexpand.DisplayMember = "expand_no";
            ddlexpand.ValueMember = "ID";
            ddlexpand.DataSource = bm_store_contractService.GetExpand();
            if (IsNew)
                New();
            else
                BindDate();
        }
        #endregion

        #region===方法===
        public void New()
        {
            entity = new YJH.Entities.bm_store_contract();
            tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            tbxcreate_date.Text = OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            ddlexpand.SelectedIndex = -1;
            ddlrecorder.SelectedIndex = -1;
            dtpcontractStartDate.SetToNullValue();
            dtpcontractEndDate.SetToNullValue();
            dtprecorderdate.SetToNullValue();
            tbxstatus.Text = "合同待签";
            tbxcontract_no.Focus();
        }

        public void BindDate(){
            tbxcontract_no.Text = entity.contract_no;
            ddlexpand.SelectedValue = entity.expandID;
            tbxstatus.Text = Enum.GetName(typeof(YJH.Enums.contractstatus), entity.status);
            dtpcontractStartDate.Value = entity.startDate;
            dtpcontractEndDate.Value = entity.endDate;
            if (entity.recorderID != null)
                ddlrecorder.SelectedValue = entity.recorderID;
            if (entity.recorderdate != null)
                dtprecorderdate.Value = entity.recorderdate.Value;
            tbxcontractRemark.Text = entity.contractRemark;
            tbxcreater.Text = entity.creater;
            tbxcreate_date.Text = entity.create_date.ToString("yyyy-MM-dd HH:mm");
            if (entity.auditer != null)
                tbxaudit.Text = entity.auditer;
            if(entity.audit_date != null)
                tbxaudit_date.Text = entity.audit_date.Value.ToString("yyyy-MM-dd HH:mm");
            if (entity.effecter != null)
                tbxeffecter.Text = entity.effecter;
            if (entity.effect_date != null)
                tbxeffect_date.Text = entity.effect_date.Value.ToString("yyyy-MM-dd HH:mm");
            ddlexpand.Enabled = false;
            if(entity.status > 10)
            {
                ddlrecorder.Enabled = false;
                dtpcontractEndDate.Enabled = false;
                dtpcontractStartDate.Enabled = false;
                dtprecorderdate.Enabled = false;
                ddlrecorder.Enabled = false;
                tbxcontractRemark.Enabled = false;
            }
        }

        public void FulshDate()
        {
            entity.contract_no = tbxcontract_no.Text;
            entity.expandID = Guid.Parse(ddlexpand.SelectedValue.ToString());
            entity.startDate = dtpcontractStartDate.Value;
            entity.endDate = dtpcontractEndDate.Value;
            if (ddlrecorder.SelectedValue != null)
                entity.recorderID = Guid.Parse(ddlrecorder.SelectedValue.ToString());
            if (dtprecorderdate.Value != null)
                entity.recorderdate = dtprecorderdate.Value;
            entity.contractRemark = tbxcontractRemark.Text;
            entity.status = 10;
            entity.creater = tbxcreater.Text;
            entity.create_date = DateTime.Parse(tbxcreate_date.Text);
        }

        public bool ValueDate()
        {
            if (string.IsNullOrWhiteSpace(this.ddlexpand.Text))
            {
                RadMessageBox.Show("请选择拓展单", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxcontract_no.Text))
            {
                RadMessageBox.Show("合同编号不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.dtpcontractStartDate.Text))
            {
                RadMessageBox.Show("请选择合同开始日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.dtpcontractEndDate.Text))
            {
                RadMessageBox.Show("请选择合同结束日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.bm_store_contract entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("contract_no", entity.contract_no);
                data.Add("expand_no", entity.expand.expand_no);
                data.Add("startDate", entity.startDate);
                data.Add("endDate", entity.endDate);
                data.Add("recorder", entity.recorder.Name);
                data.Add("recorderdate", entity.recorderdate);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("contract_no", entity.contract_no);
                data.Add("expand_no", entity.expand.expand_no);
                data.Add("startDate", entity.startDate);
                data.Add("endDate", entity.endDate);
                data.Add("recorder", entity.recorder.Name);
                data.Add("recorderdate", entity.recorderdate);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                data.Add("auditer", entity.auditer);
                if (entity.audit_date != null)
                    data.Add("audit_date", entity.audit_date);
                data.Add("effecter", entity.effecter);
                if (entity.effect_date != null)
                    data.Add("effect_date", entity.effect_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }
        #endregion
    }
}
