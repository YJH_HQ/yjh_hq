﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;
using YJH.Entities;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UC_bm_store_contract : UserControl
    {
        #region===字段===
        #endregion
        #region===构造===
        public UC_bm_store_contract()
        {
            InitializeComponent();
            this.Load += UC_bm_store_contract_Load;
            this.dgContract.GridView.CellFormatting += GridView_CellFormatting;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnAudit.Click += btnAudit_Click;
            this.btnEffect.Click += btnEffect_Click;
            this.btnCancel.Click += btnCancel_Click;
            this.btnNew.Click += btnNew_Click;
            this.btnModify.Click += (s, e) => { this.Edit(); };
            this.dgContract.GridView.DoubleClick += GridView_DoubleClick;
            this.btnDelete.Click += btnDelete_Click;
        }
        #endregion

        #region===事件===
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            if (dgContract.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "10")
            {
                RadMessageBox.Show(this, "只允许删除状态为合同待签的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_contractService.Delete(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "删除成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    Search();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "删除失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        void GridView_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }
        void btnNew_Click(object sender, EventArgs e)
        {
            UI_bm_store_contract ui = new UI_bm_store_contract();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.IsNew = true;
            ui.dgList = this.dgContract;
            ui.ShowDialog();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            if (dgContract.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() == "10")
                return;
            if (RadMessageBox.Show("确认取消吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_contract entity = bm_store_contractService.GetByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        bm_store_contractService.Cancel(entity);
                        RadMessageBox.Show(this, "取消成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "取消失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void btnEffect_Click(object sender, EventArgs e)
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            if (dgContract.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "20")
            {
                RadMessageBox.Show(this, "只允许审批状态为稽核通过的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认审批吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_contract entity = bm_store_contractService.GetByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        entity.effecter = Globle.CurrentEmployee.Base.Name;
                        entity.status = 30;
                        bm_store_contractService.Effect(entity);
                        RadMessageBox.Show(this, "审批成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "审批失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgContract == null)
            {
                return;
            }
            if (this.dgContract.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString() != "")
                this.dgContract.GridView.Rows[e.RowIndex].Cells["cl_status"].Value = Enum.GetName(typeof(YJH.Enums.contractstatus), int.Parse(this.dgContract.GridView.Rows[e.RowIndex].Cells["clstatus"].Value.ToString()));
        }
        void btnAudit_Click(object sender, EventArgs e)
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            if (dgContract.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "10")
            {
                RadMessageBox.Show(this, "只允许稽核状态为合同待签的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认稽核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    bm_store_contract entity = bm_store_contractService.GetByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    if (entity != null)
                    {
                        entity.auditer = Globle.CurrentEmployee.Base.Name;
                        entity.status = 20;
                        bm_store_contractService.Audit(entity);
                        RadMessageBox.Show(this, "稽核成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        Search();
                    }
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "稽核失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        void UC_bm_store_contract_Load(object sender, EventArgs e)
        {
            ddlstatus.SelectedIndex = 0;
            Search();
        }
        #endregion
        #region===方法===
        public void Edit()
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            bm_store_contract entity = bm_store_contractService.GetByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            if (entity != null)
            {
                UI_bm_store_contract ex = new UI_bm_store_contract();
                ex.StartPosition = FormStartPosition.CenterScreen;
                ex.dgList = this.dgContract;
                ex.entity = entity;
                ex.IsNew = false;
                ex.ShowDialog();
            }
        }
        public void Search()
        {
            this.dgContract.GridView.DataSource = bm_store_contractService.Search(tbxName.Text.Trim(),int.Parse(ddlstatus.SelectedItem.Tag.ToString()));
        }
        #endregion
    }
}
