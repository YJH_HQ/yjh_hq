﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UI_bm_store_contract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel38 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel39 = new Telerik.WinControls.UI.RadLabel();
            this.tbxeffect_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxeffecter = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel36 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel37 = new Telerik.WinControls.UI.RadLabel();
            this.tbxaudit_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxaudit = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel34 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel35 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxcreater = new Telerik.WinControls.UI.RadTextBox();
            this.tbxstatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.ddlexpand = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcontract_no = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcontractRemark = new Telerik.WinControls.UI.RadTextBox();
            this.dtprecorderdate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpcontractEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpcontractStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.ddlrecorder = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffect_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlexpand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontract_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtprecorderdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel38);
            this.radGroupBox1.Controls.Add(this.radLabel39);
            this.radGroupBox1.Controls.Add(this.tbxeffect_date);
            this.radGroupBox1.Controls.Add(this.tbxeffecter);
            this.radGroupBox1.Controls.Add(this.radLabel36);
            this.radGroupBox1.Controls.Add(this.radLabel37);
            this.radGroupBox1.Controls.Add(this.tbxaudit_date);
            this.radGroupBox1.Controls.Add(this.tbxaudit);
            this.radGroupBox1.Controls.Add(this.radLabel34);
            this.radGroupBox1.Controls.Add(this.radLabel35);
            this.radGroupBox1.Controls.Add(this.tbxcreate_date);
            this.radGroupBox1.Controls.Add(this.tbxcreater);
            this.radGroupBox1.Controls.Add(this.tbxstatus);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.ddlexpand);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxcontract_no);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxcontractRemark);
            this.radGroupBox1.Controls.Add(this.dtprecorderdate);
            this.radGroupBox1.Controls.Add(this.dtpcontractEndDate);
            this.radGroupBox1.Controls.Add(this.dtpcontractStartDate);
            this.radGroupBox1.Controls.Add(this.ddlrecorder);
            this.radGroupBox1.Controls.Add(this.radLabel29);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(672, 239);
            this.radGroupBox1.TabIndex = 3;
            // 
            // radLabel38
            // 
            this.radLabel38.Location = new System.Drawing.Point(462, 191);
            this.radLabel38.Name = "radLabel38";
            this.radLabel38.Size = new System.Drawing.Size(65, 18);
            this.radLabel38.TabIndex = 121;
            this.radLabel38.Text = "审批时间：";
            // 
            // radLabel39
            // 
            this.radLabel39.Location = new System.Drawing.Point(473, 166);
            this.radLabel39.Name = "radLabel39";
            this.radLabel39.Size = new System.Drawing.Size(54, 18);
            this.radLabel39.TabIndex = 122;
            this.radLabel39.Text = "审批人：";
            // 
            // tbxeffect_date
            // 
            this.tbxeffect_date.Enabled = false;
            this.tbxeffect_date.Location = new System.Drawing.Point(533, 190);
            this.tbxeffect_date.Name = "tbxeffect_date";
            this.tbxeffect_date.ReadOnly = true;
            this.tbxeffect_date.Size = new System.Drawing.Size(108, 20);
            this.tbxeffect_date.TabIndex = 119;
            // 
            // tbxeffecter
            // 
            this.tbxeffecter.Enabled = false;
            this.tbxeffecter.Location = new System.Drawing.Point(533, 164);
            this.tbxeffecter.Name = "tbxeffecter";
            this.tbxeffecter.ReadOnly = true;
            this.tbxeffecter.Size = new System.Drawing.Size(108, 20);
            this.tbxeffecter.TabIndex = 120;
            // 
            // radLabel36
            // 
            this.radLabel36.Location = new System.Drawing.Point(275, 190);
            this.radLabel36.Name = "radLabel36";
            this.radLabel36.Size = new System.Drawing.Size(65, 18);
            this.radLabel36.TabIndex = 117;
            this.radLabel36.Text = "稽核时间：";
            // 
            // radLabel37
            // 
            this.radLabel37.Location = new System.Drawing.Point(286, 166);
            this.radLabel37.Name = "radLabel37";
            this.radLabel37.Size = new System.Drawing.Size(54, 18);
            this.radLabel37.TabIndex = 118;
            this.radLabel37.Text = "稽核人：";
            // 
            // tbxaudit_date
            // 
            this.tbxaudit_date.Enabled = false;
            this.tbxaudit_date.Location = new System.Drawing.Point(356, 190);
            this.tbxaudit_date.Name = "tbxaudit_date";
            this.tbxaudit_date.ReadOnly = true;
            this.tbxaudit_date.Size = new System.Drawing.Size(108, 20);
            this.tbxaudit_date.TabIndex = 115;
            // 
            // tbxaudit
            // 
            this.tbxaudit.Enabled = false;
            this.tbxaudit.Location = new System.Drawing.Point(356, 164);
            this.tbxaudit.Name = "tbxaudit";
            this.tbxaudit.ReadOnly = true;
            this.tbxaudit.Size = new System.Drawing.Size(108, 20);
            this.tbxaudit.TabIndex = 116;
            // 
            // radLabel34
            // 
            this.radLabel34.Location = new System.Drawing.Point(54, 193);
            this.radLabel34.Name = "radLabel34";
            this.radLabel34.Size = new System.Drawing.Size(65, 18);
            this.radLabel34.TabIndex = 113;
            this.radLabel34.Text = "录入时间：";
            // 
            // radLabel35
            // 
            this.radLabel35.Location = new System.Drawing.Point(65, 166);
            this.radLabel35.Name = "radLabel35";
            this.radLabel35.Size = new System.Drawing.Size(54, 18);
            this.radLabel35.TabIndex = 114;
            this.radLabel35.Text = "录入人：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(135, 190);
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.ReadOnly = true;
            this.tbxcreate_date.Size = new System.Drawing.Size(108, 20);
            this.tbxcreate_date.TabIndex = 111;
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(135, 164);
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.ReadOnly = true;
            this.tbxcreater.Size = new System.Drawing.Size(108, 20);
            this.tbxcreater.TabIndex = 112;
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(533, 19);
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.Size = new System.Drawing.Size(108, 20);
            this.tbxstatus.TabIndex = 110;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(485, 21);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(42, 18);
            this.radLabel5.TabIndex = 109;
            this.radLabel5.Text = "状态：";
            // 
            // ddlexpand
            // 
            this.ddlexpand.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlexpand.Location = new System.Drawing.Point(356, 19);
            this.ddlexpand.Name = "ddlexpand";
            this.ddlexpand.Size = new System.Drawing.Size(110, 20);
            this.ddlexpand.TabIndex = 108;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(286, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 107;
            this.radLabel4.Text = "拓展单：";
            // 
            // tbxcontract_no
            // 
            this.tbxcontract_no.Location = new System.Drawing.Point(135, 19);
            this.tbxcontract_no.Name = "tbxcontract_no";
            this.tbxcontract_no.Size = new System.Drawing.Size(110, 20);
            this.tbxcontract_no.TabIndex = 106;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(54, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 105;
            this.radLabel3.Text = "合同编号：";
            // 
            // tbxcontractRemark
            // 
            this.tbxcontractRemark.AutoSize = false;
            this.tbxcontractRemark.Location = new System.Drawing.Point(135, 93);
            this.tbxcontractRemark.MaxLength = 2;
            this.tbxcontractRemark.Multiline = true;
            this.tbxcontractRemark.Name = "tbxcontractRemark";
            this.tbxcontractRemark.Size = new System.Drawing.Size(506, 65);
            this.tbxcontractRemark.TabIndex = 104;
            // 
            // dtprecorderdate
            // 
            this.dtprecorderdate.Location = new System.Drawing.Point(356, 67);
            this.dtprecorderdate.Name = "dtprecorderdate";
            this.dtprecorderdate.Size = new System.Drawing.Size(110, 20);
            this.dtprecorderdate.TabIndex = 103;
            this.dtprecorderdate.TabStop = false;
            this.dtprecorderdate.Text = "2015年12月16日";
            this.dtprecorderdate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // dtpcontractEndDate
            // 
            this.dtpcontractEndDate.Location = new System.Drawing.Point(356, 43);
            this.dtpcontractEndDate.Name = "dtpcontractEndDate";
            this.dtpcontractEndDate.Size = new System.Drawing.Size(110, 20);
            this.dtpcontractEndDate.TabIndex = 102;
            this.dtpcontractEndDate.TabStop = false;
            this.dtpcontractEndDate.Text = "2015年12月16日";
            this.dtpcontractEndDate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // dtpcontractStartDate
            // 
            this.dtpcontractStartDate.Location = new System.Drawing.Point(135, 42);
            this.dtpcontractStartDate.Name = "dtpcontractStartDate";
            this.dtpcontractStartDate.Size = new System.Drawing.Size(110, 20);
            this.dtpcontractStartDate.TabIndex = 101;
            this.dtpcontractStartDate.TabStop = false;
            this.dtpcontractStartDate.Text = "2015年12月16日";
            this.dtpcontractStartDate.Value = new System.DateTime(2015, 12, 16, 9, 4, 53, 908);
            // 
            // ddlrecorder
            // 
            this.ddlrecorder.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlrecorder.Location = new System.Drawing.Point(135, 67);
            this.ddlrecorder.Name = "ddlrecorder";
            this.ddlrecorder.Size = new System.Drawing.Size(110, 20);
            this.ddlrecorder.TabIndex = 100;
            // 
            // radLabel29
            // 
            this.radLabel29.Location = new System.Drawing.Point(54, 93);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(65, 18);
            this.radLabel29.TabIndex = 99;
            this.radLabel29.Text = "合同情况：";
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(251, 69);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(89, 18);
            this.radLabel22.TabIndex = 96;
            this.radLabel22.Text = "合同签约日期：";
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(42, 69);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(77, 18);
            this.radLabel23.TabIndex = 97;
            this.radLabel23.Text = "合同签约人：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(251, 45);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(89, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "合同结束日期：";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(30, 45);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(89, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "合同开始日期：";
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(672, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // UI_bm_store_contract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(672, 269);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_bm_store_contract";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "合同信息";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffect_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxeffecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaudit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlexpand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontract_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcontractRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtprecorderdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpcontractStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrecorder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker dtprecorderdate;
        private Telerik.WinControls.UI.RadDateTimePicker dtpcontractEndDate;
        private Telerik.WinControls.UI.RadDateTimePicker dtpcontractStartDate;
        private Telerik.WinControls.UI.RadDropDownList ddlrecorder;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbxcontractRemark;
        private Telerik.WinControls.UI.RadTextBox tbxcontract_no;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList ddlexpand;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel38;
        private Telerik.WinControls.UI.RadLabel radLabel39;
        private Telerik.WinControls.UI.RadTextBox tbxeffect_date;
        private Telerik.WinControls.UI.RadTextBox tbxeffecter;
        private Telerik.WinControls.UI.RadLabel radLabel36;
        private Telerik.WinControls.UI.RadLabel radLabel37;
        private Telerik.WinControls.UI.RadTextBox tbxaudit_date;
        private Telerik.WinControls.UI.RadTextBox tbxaudit;
        private Telerik.WinControls.UI.RadLabel radLabel34;
        private Telerik.WinControls.UI.RadLabel radLabel35;
        private Telerik.WinControls.UI.RadTextBox tbxcreate_date;
        private Telerik.WinControls.UI.RadTextBox tbxcreater;
    }
}