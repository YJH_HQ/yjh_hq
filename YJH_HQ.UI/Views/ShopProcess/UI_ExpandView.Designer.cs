﻿namespace YJH_HQ.UI.Views.ShopProcess
{
    partial class UI_ExpandView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxexpand_no = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.epexpand_emp = new YJH_HQ.Controls.EntityPicker.EntityPicker();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.tbxdescription = new Telerik.WinControls.UI.RadTextBox();
            this.dtexpand_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.cbis_bill = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.dtcontract_enddate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.ddlroom_property = new YJH_HQ.Controls.RadDropDownList.RadDropDownList();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.ddlstreet = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.ddlarea = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.ddlcity = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlprovince = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxcommunity_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadTextBox();
            this.tbxcreater = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.tbxexpand_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.tbxaddr = new Telerik.WinControls.UI.RadTextBox();
            this.tbxlandlord_tel = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.tbxappraise = new Telerik.WinControls.UI.RadTextBox();
            this.tbxformat_near = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxowner = new Telerik.WinControls.UI.RadTextBox();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxcompetitor = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epexpand_emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtexpand_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbis_bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtcontract_enddate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlroom_property)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcommunity_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaddr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlandlord_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxappraise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxformat_near)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxowner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcompetitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(38, 31);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "扫街单号：";
            // 
            // tbxexpand_no
            // 
            this.tbxexpand_no.Enabled = false;
            this.tbxexpand_no.Location = new System.Drawing.Point(107, 29);
            this.tbxexpand_no.MaxLength = 11;
            this.tbxexpand_no.Name = "tbxexpand_no";
            this.tbxexpand_no.Size = new System.Drawing.Size(106, 20);
            this.tbxexpand_no.TabIndex = 0;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxcompetitor);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.epexpand_emp);
            this.radGroupBox1.Controls.Add(this.radLabel32);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel31);
            this.radGroupBox1.Controls.Add(this.tbxdescription);
            this.radGroupBox1.Controls.Add(this.dtexpand_date);
            this.radGroupBox1.Controls.Add(this.cbis_bill);
            this.radGroupBox1.Controls.Add(this.radLabel29);
            this.radGroupBox1.Controls.Add(this.dtcontract_enddate);
            this.radGroupBox1.Controls.Add(this.ddlroom_property);
            this.radGroupBox1.Controls.Add(this.radLabel28);
            this.radGroupBox1.Controls.Add(this.radLabel27);
            this.radGroupBox1.Controls.Add(this.ddlstreet);
            this.radGroupBox1.Controls.Add(this.radLabel26);
            this.radGroupBox1.Controls.Add(this.ddlarea);
            this.radGroupBox1.Controls.Add(this.radLabel25);
            this.radGroupBox1.Controls.Add(this.radLabel24);
            this.radGroupBox1.Controls.Add(this.ddlcity);
            this.radGroupBox1.Controls.Add(this.ddlprovince);
            this.radGroupBox1.Controls.Add(this.tbxcommunity_name);
            this.radGroupBox1.Controls.Add(this.radLabel20);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.tbxcreate_date);
            this.radGroupBox1.Controls.Add(this.tbxcreater);
            this.radGroupBox1.Controls.Add(this.radLabel18);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.tbxexpand_id);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.tbxaddr);
            this.radGroupBox1.Controls.Add(this.tbxlandlord_tel);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.tbxappraise);
            this.radGroupBox1.Controls.Add(this.tbxformat_near);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxstatus);
            this.radGroupBox1.Controls.Add(this.radLabel30);
            this.radGroupBox1.Controls.Add(this.tbxowner);
            this.radGroupBox1.Controls.Add(this.tbxexpand_no);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(811, 414);
            this.radGroupBox1.TabIndex = 3;
            // 
            // epexpand_emp
            // 
            this.epexpand_emp.DisplayMember = null;
            this.epexpand_emp.Location = new System.Drawing.Point(467, 29);
            this.epexpand_emp.Name = "epexpand_emp";
            this.epexpand_emp.PickerView = null;
            this.epexpand_emp.SelectedEntity = null;
            this.epexpand_emp.Size = new System.Drawing.Size(108, 20);
            this.epexpand_emp.TabIndex = 37;
            this.epexpand_emp.TabStop = false;
            // 
            // radLabel32
            // 
            this.radLabel32.Location = new System.Drawing.Point(38, 170);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(65, 18);
            this.radLabel32.TabIndex = 14;
            this.radLabel32.Text = "商铺描述：";
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(400, 31);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(65, 18);
            this.radLabel17.TabIndex = 13;
            this.radLabel17.Text = "扫街人员：";
            // 
            // radLabel31
            // 
            this.radLabel31.Location = new System.Drawing.Point(221, 31);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(65, 18);
            this.radLabel31.TabIndex = 35;
            this.radLabel31.Text = "扫街时间：";
            // 
            // tbxdescription
            // 
            this.tbxdescription.AutoSize = false;
            this.tbxdescription.Location = new System.Drawing.Point(107, 170);
            this.tbxdescription.Multiline = true;
            this.tbxdescription.Name = "tbxdescription";
            this.tbxdescription.Size = new System.Drawing.Size(649, 60);
            this.tbxdescription.TabIndex = 13;
            // 
            // dtexpand_date
            // 
            this.dtexpand_date.Location = new System.Drawing.Point(287, 29);
            this.dtexpand_date.Name = "dtexpand_date";
            this.dtexpand_date.Size = new System.Drawing.Size(106, 20);
            this.dtexpand_date.TabIndex = 36;
            this.dtexpand_date.TabStop = false;
            this.dtexpand_date.Value = new System.DateTime(((long)(0)));
            // 
            // cbis_bill
            // 
            this.cbis_bill.Location = new System.Drawing.Point(287, 144);
            this.cbis_bill.Name = "cbis_bill";
            this.cbis_bill.Size = new System.Drawing.Size(115, 18);
            this.cbis_bill.TabIndex = 35;
            this.cbis_bill.Text = "是否填写调查量表";
            // 
            // radLabel29
            // 
            this.radLabel29.Location = new System.Drawing.Point(581, 114);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(65, 18);
            this.radLabel29.TabIndex = 9;
            this.radLabel29.Text = "到期时间：";
            // 
            // dtcontract_enddate
            // 
            this.dtcontract_enddate.Location = new System.Drawing.Point(648, 114);
            this.dtcontract_enddate.Name = "dtcontract_enddate";
            this.dtcontract_enddate.Size = new System.Drawing.Size(108, 20);
            this.dtcontract_enddate.TabIndex = 34;
            this.dtcontract_enddate.TabStop = false;
            this.dtcontract_enddate.Value = new System.DateTime(((long)(0)));
            // 
            // ddlroom_property
            // 
            radListDataItem4.Tag = "1";
            radListDataItem4.Text = "门店";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Tag = "2";
            radListDataItem5.Text = "社区配套用房";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Tag = "3";
            radListDataItem6.Text = "移动箱房";
            radListDataItem6.TextWrap = true;
            this.ddlroom_property.Items.Add(radListDataItem4);
            this.ddlroom_property.Items.Add(radListDataItem5);
            this.ddlroom_property.Items.Add(radListDataItem6);
            this.ddlroom_property.Location = new System.Drawing.Point(467, 114);
            this.ddlroom_property.Name = "ddlroom_property";
            this.ddlroom_property.Size = new System.Drawing.Size(108, 20);
            this.ddlroom_property.TabIndex = 33;
            // 
            // radLabel28
            // 
            this.radLabel28.Location = new System.Drawing.Point(400, 114);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(65, 18);
            this.radLabel28.TabIndex = 11;
            this.radLabel28.Text = "网点性质：";
            // 
            // radLabel27
            // 
            this.radLabel27.Location = new System.Drawing.Point(604, 84);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(42, 18);
            this.radLabel27.TabIndex = 32;
            this.radLabel27.Text = "街道：";
            // 
            // ddlstreet
            // 
            this.ddlstreet.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlstreet.Location = new System.Drawing.Point(648, 84);
            this.ddlstreet.Name = "ddlstreet";
            this.ddlstreet.Size = new System.Drawing.Size(108, 20);
            this.ddlstreet.TabIndex = 31;
            // 
            // radLabel26
            // 
            this.radLabel26.Location = new System.Drawing.Point(423, 84);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(42, 18);
            this.radLabel26.TabIndex = 30;
            this.radLabel26.Text = "市区：";
            // 
            // ddlarea
            // 
            this.ddlarea.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlarea.Location = new System.Drawing.Point(467, 84);
            this.ddlarea.Name = "ddlarea";
            this.ddlarea.Size = new System.Drawing.Size(108, 20);
            this.ddlarea.TabIndex = 29;
            // 
            // radLabel25
            // 
            this.radLabel25.Location = new System.Drawing.Point(244, 84);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(42, 18);
            this.radLabel25.TabIndex = 28;
            this.radLabel25.Text = "城市：";
            // 
            // radLabel24
            // 
            this.radLabel24.Location = new System.Drawing.Point(62, 84);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(42, 18);
            this.radLabel24.TabIndex = 27;
            this.radLabel24.Text = "省份：";
            // 
            // ddlcity
            // 
            this.ddlcity.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlcity.Location = new System.Drawing.Point(287, 84);
            this.ddlcity.Name = "ddlcity";
            this.ddlcity.Size = new System.Drawing.Size(109, 20);
            this.ddlcity.TabIndex = 26;
            // 
            // ddlprovince
            // 
            this.ddlprovince.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlprovince.Location = new System.Drawing.Point(107, 84);
            this.ddlprovince.Name = "ddlprovince";
            this.ddlprovince.Size = new System.Drawing.Size(106, 20);
            this.ddlprovince.TabIndex = 25;
            // 
            // tbxcommunity_name
            // 
            this.tbxcommunity_name.Location = new System.Drawing.Point(107, 56);
            this.tbxcommunity_name.MaxLength = 10;
            this.tbxcommunity_name.Name = "tbxcommunity_name";
            this.tbxcommunity_name.Size = new System.Drawing.Size(106, 20);
            this.tbxcommunity_name.TabIndex = 18;
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(581, 144);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(65, 18);
            this.radLabel20.TabIndex = 14;
            this.radLabel20.Text = "录入时间：";
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(411, 144);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(54, 18);
            this.radLabel19.TabIndex = 14;
            this.radLabel19.Text = "录入人：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(648, 142);
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.Size = new System.Drawing.Size(108, 20);
            this.tbxcreate_date.TabIndex = 13;
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(467, 142);
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.Size = new System.Drawing.Size(108, 20);
            this.tbxcreater.TabIndex = 13;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(38, 56);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(65, 18);
            this.radLabel18.TabIndex = 15;
            this.radLabel18.Text = "小区名称：";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(38, 144);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(65, 18);
            this.radLabel15.TabIndex = 12;
            this.radLabel15.Text = "拓展单号：";
            // 
            // tbxexpand_id
            // 
            this.tbxexpand_id.Enabled = false;
            this.tbxexpand_id.Location = new System.Drawing.Point(107, 142);
            this.tbxexpand_id.Name = "tbxexpand_id";
            this.tbxexpand_id.Size = new System.Drawing.Size(106, 20);
            this.tbxexpand_id.TabIndex = 11;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(221, 56);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(65, 18);
            this.radLabel13.TabIndex = 12;
            this.radLabel13.Text = "门店地址：";
            // 
            // tbxaddr
            // 
            this.tbxaddr.Location = new System.Drawing.Point(287, 56);
            this.tbxaddr.Name = "tbxaddr";
            this.tbxaddr.Size = new System.Drawing.Size(288, 20);
            this.tbxaddr.TabIndex = 11;
            // 
            // tbxlandlord_tel
            // 
            this.tbxlandlord_tel.Location = new System.Drawing.Point(287, 114);
            this.tbxlandlord_tel.Mask = "d";
            this.tbxlandlord_tel.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxlandlord_tel.Name = "tbxlandlord_tel";
            this.tbxlandlord_tel.Size = new System.Drawing.Size(109, 20);
            this.tbxlandlord_tel.TabIndex = 11;
            this.tbxlandlord_tel.TabStop = false;
            this.tbxlandlord_tel.Text = "0";
            this.tbxlandlord_tel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(221, 114);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 10;
            this.radLabel11.Text = "房东电话：";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(38, 301);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "开发评估：";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(38, 235);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 8;
            this.radLabel7.Text = "周边业态：";
            // 
            // tbxappraise
            // 
            this.tbxappraise.AutoSize = false;
            this.tbxappraise.Location = new System.Drawing.Point(107, 301);
            this.tbxappraise.MaxLength = 2;
            this.tbxappraise.Multiline = true;
            this.tbxappraise.Name = "tbxappraise";
            this.tbxappraise.Size = new System.Drawing.Size(649, 60);
            this.tbxappraise.TabIndex = 7;
            // 
            // tbxformat_near
            // 
            this.tbxformat_near.AutoSize = false;
            this.tbxformat_near.Location = new System.Drawing.Point(107, 235);
            this.tbxformat_near.MaxLength = 2;
            this.tbxformat_near.Multiline = true;
            this.tbxformat_near.Name = "tbxformat_near";
            this.tbxformat_near.Size = new System.Drawing.Size(649, 60);
            this.tbxformat_near.TabIndex = 7;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(62, 114);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "业主：";
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(648, 29);
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.Size = new System.Drawing.Size(108, 20);
            this.tbxstatus.TabIndex = 1;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(581, 31);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(65, 18);
            this.radLabel30.TabIndex = 2;
            this.radLabel30.Text = "扫街状态：";
            // 
            // tbxowner
            // 
            this.tbxowner.Location = new System.Drawing.Point(107, 114);
            this.tbxowner.MaxLength = 2;
            this.tbxowner.Name = "tbxowner";
            this.tbxowner.Size = new System.Drawing.Size(106, 20);
            this.tbxowner.TabIndex = 1;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(811, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxcompetitor
            // 
            this.tbxcompetitor.Location = new System.Drawing.Point(648, 56);
            this.tbxcompetitor.Name = "tbxcompetitor";
            this.tbxcompetitor.Size = new System.Drawing.Size(108, 20);
            this.tbxcompetitor.TabIndex = 3;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(581, 58);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "竞争对手：";
            // 
            // UI_ExpandView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(811, 444);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_ExpandView";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "门店信息";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epexpand_emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtexpand_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbis_bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtcontract_enddate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlroom_property)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcommunity_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxexpand_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxaddr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlandlord_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxappraise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxformat_near)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxowner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcompetitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox tbxexpand_no;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxowner;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadTextBox tbxcreate_date;
        private Telerik.WinControls.UI.RadTextBox tbxcreater;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadTextBox tbxexpand_id;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox tbxaddr;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxlandlord_tel;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox tbxappraise;
        private Telerik.WinControls.UI.RadTextBox tbxformat_near;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox tbxcommunity_name;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadDropDownList ddlstreet;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadDropDownList ddlarea;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadDropDownList ddlcity;
        private Telerik.WinControls.UI.RadDropDownList ddlprovince;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadTextBox tbxdescription;
        private Telerik.WinControls.UI.RadDateTimePicker dtexpand_date;
        private Telerik.WinControls.UI.RadCheckBox cbis_bill;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadDateTimePicker dtcontract_enddate;
        private Controls.RadDropDownList.RadDropDownList ddlroom_property;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Controls.EntityPicker.EntityPicker epexpand_emp;
        private Telerik.WinControls.UI.RadTextBox tbxcompetitor;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}