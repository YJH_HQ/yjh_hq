﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Entities;
using YJH.Services;

namespace YJH_HQ.UI.Views.ShopProcess
{
    public partial class UI_Store_Plan : Telerik.WinControls.UI.RadForm
    {
        public Guid id;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        public UI_Store_Plan()
        {
            InitializeComponent();
            this.Load += UI_Store_Plan_Load;
            this.btSave.Click += btSave_Click;
        }

        private bool ValueDate()
        {
            if (string.IsNullOrWhiteSpace(this.dtpstore_plan_date.Text))
            {
                RadMessageBox.Show("请选择计划开店日期", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }
        void btSave_Click(object sender, EventArgs e)
        {
            if (ValueDate())
            {
                try
                {
                    bm_store_expand entity = bm_store_expandService.GetByID(id);
                    if (entity == null || entity.status != 50)
                        return;
                    entity.store_plan_date = dtpstore_plan_date.Value;
                    entity.status = 60;
                    var entityState = entity.Instance.PersistentState;
                    bm_store_expandService.Save(entity);
                    RadMessageBox.Show(this, "计划开店成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "计划开店失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void UI_Store_Plan_Load(object sender, EventArgs e)
        {
            dtpstore_plan_date.SetToNullValue();
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.bm_store_expand entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("expand_no", entity.expand_no);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("store_name", entity.store_name);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("expand_no", entity.expand_no);
                data.Add("province", entity.province);
                data.Add("city", entity.city);
                data.Add("area", entity.area);
                data.Add("street", entity.street);
                data.Add("store_name", entity.store_name);
                data.Add("status", entity.status);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                data.Add("auditer", entity.auditer);
                if (entity.audit_date != null)
                    data.Add("audit_date", entity.audit_date);
                data.Add("effecter", entity.effecter);
                if (entity.effect_date != null)
                    data.Add("effect_date", entity.effect_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }
    }
}
