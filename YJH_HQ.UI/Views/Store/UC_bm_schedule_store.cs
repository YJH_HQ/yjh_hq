﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using YJH_HQ.UI.Properties;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_bm_schedule_store : UserControl
    {
        public string sid = "";
        private List<YJH.Entities.bm_process_store> list1 = new List<YJH.Entities.bm_process_store>();
        private List<YJH.Entities.bm_schedule_store> list = new List<YJH.Entities.bm_schedule_store>();
        private List<YJH.Entities.bm_schedule_store> Parentlist = new List<YJH.Entities.bm_schedule_store>();
        private List<YJH.Entities.bm_schedule_type> listtype = new List<YJH.Entities.bm_schedule_type>();
        private List<YJH.Entities.ba_class> listtype1 = new List<YJH.Entities.ba_class>();
        public UC_bm_schedule_store()
        {
            InitializeComponent();
            this.Load += UC_bm_schedule_store_Load;
            this.lbStore.SelectedIndexChanged += lbStore_SelectedIndexChanged;
            this.btSave.Click += btSave_Click;
            this.bmTree.NodeMouseDoubleClick += bmTree_NodeMouseDoubleClick;
        }

        void bmTree_NodeMouseDoubleClick(object sender, RadTreeViewEventArgs e)
        {
            if(e.Node.Checked == true)
            {
                UI_schedule_emp_id emp = new UI_schedule_emp_id();
                emp.store_id = sid;
                emp.process_id = e.Node.Tag.ToString();
                emp.ShowDialog();
            }
        }

        void btSave_Click(object sender, EventArgs e)
        {
            if (lbStore.SelectedItems.Count == 0)
            {
                RadMessageBox.Show("请先选择门店", "提示");
                return;
            }
            List<string> list = new List<string>();
            var checkedTreeNodes = bmTree.FindNodes(t => t.Checked);
            foreach (var item in checkedTreeNodes)
            {
                list.Add(item.Tag.ToString());
            }

            try
            {
                YJH.Services.StoreMngService.SaveProcess(list, sid, Globle.CurrentEmployee.Base.Name);
                RadMessageBox.Show("保存成功", "提示");
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败", "提示");
            }
        }

        void lbStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lbStore.SelectedItems.Count > 0)
            {
                sid = lbStore.SelectedItem.Value.ToString().Substring(0, 7);
                rgbtree.Text = lbStore.SelectedItem.Text.ToString() + "拓店进程";
                bmTree.Nodes.Clear();
                BuildTree();
                // 获取
                list = YJH.Services.StoreMngService.Getbm_schedule_store(sid);
                foreach (var schedule in list)
                {
                    var node = bmTree.Find(s => s.Tag.ToString() == schedule.process_id);
                    if (node != null)
                        node.Checked = true;
                }

                rgbTypeTree.Text = lbStore.SelectedItem.Text.ToString() + "类别进度";
                bmtypetree.Nodes.Clear();
                BuildTypeTree();
                // 获取
                listtype = YJH.Services.StoreMngService.Getbm_schedule_type(sid);
                foreach (var type in listtype)
                {
                    var node = bmTree.Find(s => s.Tag.ToString() == type.class_id);
                    if (node != null)
                        node.Checked = true;
                }
            }
        }

        void UC_bm_schedule_store_Load(object sender, EventArgs e)
        {
            bmTree.CheckBoxes = true;
            BindList();
            LoadPermission();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.bmTree.Enabled = funcs.Exists(t => t.Name == "查看");
            this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        void BindList()
        {
            List<YJH.Entities.ba_store> entitylist = YJH.Services.StoreMngService.GetStore();
            foreach (YJH.Entities.ba_store item in entitylist)
            {
                RadListDataItem v = new RadListDataItem();
                v.Text = item.store_id + " " + item.store_name;
                v.Value = item.store_id;
                v.Image = Resources.Home16;
                v.ImageAlignment = ContentAlignment.MiddleLeft;
                lbStore.Items.Add(v);
            }
        }

        public void BuildTree()
        {
            list1 = YJH.Services.StoreMngService.Getbm_processByUser(Globle.CurrentEmployee.Base.Name);
            foreach (var item in list1.Where(t => t.process_parent == null).OrderBy(t => t.process_order_id))
                LoopAddTreeNode(item, null);
        }

        private void LoopAddTreeNode(YJH.Entities.bm_process_store entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Text = entity.process_name;
            node.Value = entity.Instance.ID;
            node.Tag = entity.process_id;
            node.Name = entity.process_level.ToString();
            if (ParentNode == null)
                this.bmTree.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in list1.Where(t => t.process_parent == entity.process_id).OrderBy(t => t.process_order_id))
                LoopAddTreeNode(item, node);
        }

        //构造类型树
        public void BuildTypeTree()
        {
            listtype1 = YJH.Services.StoreMngService.Getba_class();
            foreach (var item in listtype1.Where(t => t.item_parent == null).OrderBy(t => t.item_order_id))
                LoopAddTreeNode1(item, null);
        }

        private void LoopAddTreeNode1(YJH.Entities.ba_class entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Text = entity.class_name;
            node.Value = entity.Instance.ID;
            node.Tag = entity.class_id;
            node.Name = entity.item_level.ToString();
            if (ParentNode == null)
                this.bmtypetree.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in listtype1.Where(t => t.item_parent == entity.class_id).OrderBy(t => t.item_order_id))
                LoopAddTreeNode1(item, node);
        }
    }
}
