﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UC_bm_schedule_store
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn1 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "Column 0");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn2 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "Column 1");
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbStore = new Telerik.WinControls.UI.RadListView();
            this.rgbtree = new Telerik.WinControls.UI.RadGroupBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.bmTree = new Telerik.WinControls.UI.RadTreeView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.rgbTypeTree = new Telerik.WinControls.UI.RadGroupBox();
            this.bmtypetree = new Telerik.WinControls.UI.RadTreeView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbtree)).BeginInit();
            this.rgbtree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bmTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbTypeTree)).BeginInit();
            this.rgbTypeTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bmtypetree)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.lbStore);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox1.HeaderText = "门店列表";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(200, 580);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "门店列表";
            // 
            // lbStore
            // 
            this.lbStore.AllowEdit = false;
            this.lbStore.AllowRemove = false;
            listViewDetailColumn1.HeaderText = "Column 0";
            listViewDetailColumn2.HeaderText = "Column 1";
            this.lbStore.Columns.AddRange(new Telerik.WinControls.UI.ListViewDetailColumn[] {
            listViewDetailColumn1,
            listViewDetailColumn2});
            this.lbStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStore.Location = new System.Drawing.Point(2, 18);
            this.lbStore.Name = "lbStore";
            this.lbStore.SelectLastAddedItem = false;
            this.lbStore.Size = new System.Drawing.Size(196, 560);
            this.lbStore.TabIndex = 1;
            this.lbStore.Text = "radListView1";
            // 
            // rgbtree
            // 
            this.rgbtree.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.rgbtree.Controls.Add(this.radPanel1);
            this.rgbtree.Controls.Add(this.radCommandBar2);
            this.rgbtree.Dock = System.Windows.Forms.DockStyle.Top;
            this.rgbtree.HeaderText = "拓展进度";
            this.rgbtree.Location = new System.Drawing.Point(200, 0);
            this.rgbtree.Name = "rgbtree";
            this.rgbtree.Size = new System.Drawing.Size(759, 313);
            this.rgbtree.TabIndex = 1;
            this.rgbtree.Text = "拓展进度";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.bmTree);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(2, 48);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(755, 263);
            this.radPanel1.TabIndex = 7;
            this.radPanel1.Text = "radPanel1";
            // 
            // bmTree
            // 
            this.bmTree.CheckBoxes = true;
            this.bmTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bmTree.Location = new System.Drawing.Point(0, 0);
            this.bmTree.Name = "bmTree";
            this.bmTree.ShowLines = true;
            this.bmTree.Size = new System.Drawing.Size(755, 263);
            this.bmTree.SpacingBetweenNodes = -1;
            this.bmTree.TabIndex = 1;
            this.bmTree.Text = "radTreeView1";
            this.bmTree.TriStateMode = true;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(2, 18);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(755, 30);
            this.radCommandBar2.TabIndex = 6;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // rgbTypeTree
            // 
            this.rgbTypeTree.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.rgbTypeTree.Controls.Add(this.bmtypetree);
            this.rgbTypeTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgbTypeTree.HeaderText = "类别进度";
            this.rgbTypeTree.Location = new System.Drawing.Point(200, 313);
            this.rgbTypeTree.Name = "rgbTypeTree";
            this.rgbTypeTree.Size = new System.Drawing.Size(759, 267);
            this.rgbTypeTree.TabIndex = 2;
            this.rgbTypeTree.Text = "类别进度";
            // 
            // bmtypetree
            // 
            this.bmtypetree.CheckBoxes = true;
            this.bmtypetree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bmtypetree.Location = new System.Drawing.Point(2, 18);
            this.bmtypetree.Name = "bmtypetree";
            this.bmtypetree.ShowLines = true;
            this.bmtypetree.Size = new System.Drawing.Size(755, 247);
            this.bmtypetree.SpacingBetweenNodes = -1;
            this.bmtypetree.TabIndex = 2;
            this.bmtypetree.Text = "radTreeView1";
            this.bmtypetree.TriStateMode = true;
            // 
            // UC_bm_schedule_store
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rgbTypeTree);
            this.Controls.Add(this.rgbtree);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_bm_schedule_store";
            this.Size = new System.Drawing.Size(959, 580);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbtree)).EndInit();
            this.rgbtree.ResumeLayout(false);
            this.rgbtree.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bmTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbTypeTree)).EndInit();
            this.rgbTypeTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bmtypetree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox rgbtree;
        private Telerik.WinControls.UI.RadListView lbStore;
        private Telerik.WinControls.UI.RadTreeView bmTree;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadGroupBox rgbTypeTree;
        private Telerik.WinControls.UI.RadTreeView bmtypetree;
    }
}
