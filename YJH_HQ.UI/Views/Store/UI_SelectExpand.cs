﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_SelectExpand : Telerik.WinControls.UI.RadForm
    {
        public YJH.Entities.ba_store entity;

        public UI_SelectExpand()
        {
            InitializeComponent();
            this.btSave.Click += btSave_Click;
            this.Load += UI_SelectExpand_Load;
        }

        void UI_SelectExpand_Load(object sender, EventArgs e)
        {
            ddlExpand.DisplayMember = "expand_no";
            ddlExpand.ValueMember = "ID";
            ddlExpand.DataSource = YJH.Services.StoreMngService.GetExpand();
        }

        void btSave_Click(object sender, EventArgs e)
        {
            if(ddlExpand.SelectedValue == null)
            {
                RadMessageBox.Show("请选择拓展单", "提示", MessageBoxButtons.OK);
                return;
            }
            try
            {
                YJH.Services.StoreMngService.SendData(entity, Guid.Parse(ddlExpand.SelectedValue.ToString()));
                entity.Instance.AcceptChanges();
                RadMessageBox.Show("发送成功", "提示", MessageBoxButtons.OK);
                this.Close();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("发送失败" + ex.Message, "提示", MessageBoxButtons.OK);
            }
           
        }
    }
}
