﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UI_schedule_emp_id
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.orgTree = new Telerik.WinControls.UI.RadTreeView();
            this.tbxname = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(77, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "当前经办人：";
            // 
            // orgTree
            // 
            this.orgTree.Location = new System.Drawing.Point(12, 36);
            this.orgTree.Name = "orgTree";
            this.orgTree.Size = new System.Drawing.Size(240, 181);
            this.orgTree.SpacingBetweenNodes = -1;
            this.orgTree.TabIndex = 11;
            this.orgTree.Text = "radTreeView1";
            // 
            // tbxname
            // 
            this.tbxname.Location = new System.Drawing.Point(130, 12);
            this.tbxname.Name = "tbxname";
            this.tbxname.Size = new System.Drawing.Size(122, 20);
            this.tbxname.TabIndex = 12;
            // 
            // UI_schedule_emp_id
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 259);
            this.Controls.Add(this.tbxname);
            this.Controls.Add(this.orgTree);
            this.Controls.Add(this.radLabel1);
            this.Name = "UI_schedule_emp_id";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "双击选择经办人";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTreeView orgTree;
        private Telerik.WinControls.UI.RadTextBox tbxname;
    }
}