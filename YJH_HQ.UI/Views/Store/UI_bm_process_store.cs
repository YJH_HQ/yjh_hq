﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_bm_process_store : Telerik.WinControls.UI.RadForm
    {
        public string parent = "";
        public bool IsNew = true;
        public Guid id = Guid.Empty;
        public bool IsChild = true;
        RadTreeNode currentnode = new RadTreeNode();
        UC_bm_process_store bmstore = new UC_bm_process_store();
        public UI_bm_process_store(UC_bm_process_store BM)
        {
            InitializeComponent();
            bmstore = BM;
            this.Load += UI_Ba_Class_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                YJH.Entities.bm_process_store entity = YJH.Services.StoreMngService.Getbm_processByID(id);
                if (IsNew)
                {
                    YJH.Entities.bm_process_store newentity = new YJH.Entities.bm_process_store();
                    newentity.process_order_id = 0;
                    newentity.process_no = 0;
                    newentity.process_id = tbxprocess_id.Text.Trim();
                    newentity.process_name = tbxprocess_name.Text.Trim();
                    newentity.process_type = ddlprocess_type.SelectedIndex;
                    
                    if (cbxprocess_status.Checked)
                        newentity.process_status = 1;
                    else
                        newentity.process_status = 0;
                    if (cbxprocess_additional.Checked)
                        newentity.process_additional = 1;
                    else
                        newentity.process_additional = 0;
                    if (IsChild)
                    {
                        newentity.process_level = entity.process_level + 1;
                        newentity.process_parent = entity.process_id;
                        if (entity.process_level == 1)
                            newentity.process_isfinal = 1;
                        else
                            newentity.process_isfinal = 0;
                    }
                    else
                    {
                        newentity.process_level = entity.process_level;
                        newentity.process_parent = entity.process_parent;
                        if (entity.process_level == 2)
                            newentity.process_isfinal = 1;
                        else
                            newentity.process_isfinal = 0;
                    }
                    bool result = YJH.Services.StoreMngService.IsHasProess_id(newentity);
                    if(result)
                    {
                        RadMessageBox.Show("该过程号已存在，请勿重复添加", "提示");
                        return;
                    }
                    YJH.Services.StoreMngService.Savebm_process_store(newentity);
                    RadMessageBox.Show("保存成功", "提示");
                    currentnode.Text = newentity.process_name;
                    currentnode.Value = newentity.Instance.ID;
                    currentnode.Tag = newentity.process_id;
                    if (IsChild)
                    {
                        currentnode.Name = newentity.process_level.ToString();
                        bmstore.isChild = true;
                        bmstore.RefeshTree(currentnode);
                    }
                    else
                    {
                        currentnode.Name = newentity.process_level.ToString();
                        bmstore.isChild = false;
                        bmstore.RefeshTree(currentnode);
                    }
                    this.Close();
                }
                else
                {
                    if (id != Guid.Empty)
                    {
                        entity.process_id = tbxprocess_id.Text.Trim();
                        entity.process_name = tbxprocess_name.Text.Trim();
                        entity.process_type = ddlprocess_type.SelectedIndex;
                        if (cbxprocess_status.Checked)
                            entity.process_status = 1;
                        else
                            entity.process_status = 0;
                        if (cbxprocess_additional.Checked)
                            entity.process_additional = 1;
                        else
                            entity.process_additional = 0;
                        
                        bool result = YJH.Services.StoreMngService.IsHasProess_id(entity);
                        if (result)
                        {
                            RadMessageBox.Show("该过程号已存在，请勿重复添加", "提示");
                            return;
                        }
                        YJH.Services.StoreMngService.Savebm_process_store(entity);
                        RadMessageBox.Show("保存成功", "提示");
                        currentnode.Text = entity.process_name;
                        currentnode.Value = entity.Instance.ID;
                        currentnode.Tag = entity.process_id;
                        currentnode.Name = entity.process_level.ToString();
                        bmstore.selectNode.Value = currentnode.Value;
                        bmstore.selectNode.Text = currentnode.Text;
                        bmstore.selectNode.Tag = currentnode.Tag;
                        bmstore.selectNode.Name = currentnode.Name;
                        this.Close();
                    }
                    else
                    {
                        RadMessageBox.Show("保存失败", "提示");
                        return;
                    }
                }
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败", "提示");
                return;
            }
        }

        void UI_Ba_Class_Load(object sender, EventArgs e)
        {
            foreach (var item in Enum.GetNames(typeof(YJH.Enums.process_type)))
            {
                RadListDataItem i = new RadListDataItem();
                i.Text = item;
                ddlprocess_type.Items.Add(i);
            }
            if (IsNew)
            {
                tbxprocess_id.Text = "";
                tbxprocess_name.Text = "";
                ddlprocess_type.SelectedIndex = 0;
                cbxprocess_additional.Checked = false;
                cbxprocess_status.Checked = true;
            }
            else
            {
                if (id != Guid.Empty)
                {
                    YJH.Entities.bm_process_store entity = YJH.Services.StoreMngService.Getbm_processByID(id);
                    if (entity != null)
                    {
                        tbxprocess_id.Text = entity.process_id;
                        tbxprocess_name.Text = entity.process_name;
                        ddlprocess_type.SelectedIndex = entity.process_type;
                        if (entity.process_additional == 0)
                            cbxprocess_additional.Checked = false;
                        else
                            cbxprocess_additional.Checked = true;
                        if (entity.process_status == 1)
                            cbxprocess_status.Checked = true;
                        else
                            cbxprocess_status.Checked = false;
                    }
                }
            }
        }
    }
}
