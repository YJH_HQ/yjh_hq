﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UC_UserMng
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn1 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "Column 0");
            Telerik.WinControls.UI.ListViewDetailColumn listViewDetailColumn2 = new Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "Column 1");
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.lbStore = new Telerik.WinControls.UI.RadListView();
            this.radContextMenu1 = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.btnDelete = new Telerik.WinControls.UI.RadMenuItem();
            this.btnBindUser = new Telerik.WinControls.UI.RadMenuItem();
            this.btnRejob = new Telerik.WinControls.UI.RadMenuItem();
            this.btnEnabled = new Telerik.WinControls.UI.RadMenuItem();
            this.dgUser = new Telerik.WinControls.UI.RadGridView();
            this.radContextMenuManager1 = new Telerik.WinControls.UI.RadContextMenuManager();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgUser.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbStore
            // 
            this.lbStore.AllowEdit = false;
            this.lbStore.AllowRemove = false;
            listViewDetailColumn1.HeaderText = "Column 0";
            listViewDetailColumn2.HeaderText = "Column 1";
            this.lbStore.Columns.AddRange(new Telerik.WinControls.UI.ListViewDetailColumn[] {
            listViewDetailColumn1,
            listViewDetailColumn2});
            this.lbStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStore.Location = new System.Drawing.Point(2, 18);
            this.lbStore.Name = "lbStore";
            this.lbStore.SelectLastAddedItem = false;
            this.lbStore.Size = new System.Drawing.Size(208, 593);
            this.lbStore.TabIndex = 0;
            this.lbStore.Text = "radListView1";
            // 
            // radContextMenu1
            // 
            this.radContextMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnDelete,
            this.btnBindUser,
            this.btnRejob,
            this.btnEnabled});
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnBindUser
            // 
            this.btnBindUser.AccessibleDescription = "绑定现有员工";
            this.btnBindUser.AccessibleName = "绑定现有员工";
            this.btnBindUser.Name = "btnBindUser";
            this.btnBindUser.Text = "绑定现有员工";
            this.btnBindUser.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnRejob
            // 
            this.btnRejob.AccessibleDescription = "岗位变更";
            this.btnRejob.AccessibleName = "岗位变更";
            this.btnRejob.Name = "btnRejob";
            this.btnRejob.Text = "岗位变更";
            this.btnRejob.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnEnabled
            // 
            this.btnEnabled.AccessibleDescription = "启用/禁用";
            this.btnEnabled.AccessibleName = "启用/禁用";
            this.btnEnabled.Name = "btnEnabled";
            this.btnEnabled.Text = "主职/兼职";
            this.btnEnabled.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dgUser
            // 
            this.dgUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgUser.Location = new System.Drawing.Point(2, 18);
            // 
            // dgUser
            // 
            this.dgUser.MasterTemplate.AllowAddNewRow = false;
            this.dgUser.MasterTemplate.AllowCellContextMenu = false;
            this.dgUser.MasterTemplate.AllowColumnChooser = false;
            this.dgUser.MasterTemplate.AllowColumnReorder = false;
            this.dgUser.MasterTemplate.AllowDeleteRow = false;
            this.dgUser.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.FieldName = "ID";
            gridViewTextBoxColumn1.HeaderText = "column1";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "clID";
            gridViewTextBoxColumn2.FieldName = "Number";
            gridViewTextBoxColumn2.HeaderText = "用户编号";
            gridViewTextBoxColumn2.Name = "clNumber";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "姓名";
            gridViewTextBoxColumn3.Name = "clName";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "job";
            gridViewTextBoxColumn4.HeaderText = "岗位";
            gridViewTextBoxColumn4.Name = "cljob";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.FieldName = "status";
            gridViewTextBoxColumn5.HeaderText = "状态";
            gridViewTextBoxColumn5.Name = "clstatus";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 60;
            gridViewTextBoxColumn6.FieldName = "hr_jobID";
            gridViewTextBoxColumn6.HeaderText = "column1";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "clhr_jobID";
            this.dgUser.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.dgUser.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgUser.Name = "dgUser";
            this.radContextMenuManager1.SetRadContextMenu(this.dgUser, this.radContextMenu1);
            this.dgUser.ReadOnly = true;
            this.dgUser.ShowGroupPanel = false;
            this.dgUser.ShowItemToolTips = false;
            this.dgUser.Size = new System.Drawing.Size(743, 593);
            this.dgUser.TabIndex = 1;
            this.dgUser.Text = "radGridView1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.lbStore);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox1.HeaderText = "门店列表";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(212, 613);
            this.radGroupBox1.TabIndex = 2;
            this.radGroupBox1.Text = "门店列表";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.dgUser);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.HeaderText = "人员信息";
            this.radGroupBox2.Location = new System.Drawing.Point(212, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(747, 613);
            this.radGroupBox2.TabIndex = 3;
            this.radGroupBox2.Text = "人员信息";
            // 
            // UC_UserMng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_UserMng";
            this.Size = new System.Drawing.Size(959, 613);
            ((System.ComponentModel.ISupportInitialize)(this.lbStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgUser.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadListView lbStore;
        private Telerik.WinControls.UI.RadContextMenu radContextMenu1;
        private Telerik.WinControls.UI.RadMenuItem btnDelete;
        private Telerik.WinControls.UI.RadMenuItem btnBindUser;
        private Telerik.WinControls.UI.RadGridView dgUser;
        private Telerik.WinControls.UI.RadContextMenuManager radContextMenuManager1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadMenuItem btnRejob;
        private Telerik.WinControls.UI.RadMenuItem btnEnabled;


    }
}
