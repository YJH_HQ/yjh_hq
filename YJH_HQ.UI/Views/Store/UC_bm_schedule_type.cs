﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH_HQ.UI.Properties;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_bm_schedule_type : UserControl
    {
        public string sid = "";
        private List<YJH.Entities.ba_class> list1 = new List<YJH.Entities.ba_class>();
        private List<YJH.Entities.bm_schedule_type> list = new List<YJH.Entities.bm_schedule_type>();
        public UC_bm_schedule_type()
        {
            InitializeComponent();
            this.Load += UC_bm_schedule_type_Load;
            this.lbStore.SelectedIndexChanged += lbStore_SelectedIndexChanged;
            this.btSave.Click += btSave_Click;
        }

        void UC_bm_schedule_type_Load(object sender, EventArgs e)
        {
            bmTree.CheckBoxes = true;
            BindList();
            LoadPermission();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.bmTree.Enabled = funcs.Exists(t => t.Name == "查看");
            this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        void btSave_Click(object sender, EventArgs e)
        {
            if (lbStore.SelectedItems.Count == 0)
            {
                RadMessageBox.Show("请先选择门店", "提示");
                return;
            }
            List<string> list = new List<string>();
            var checkedTreeNodes = bmTree.FindNodes(t => t.Checked);
            foreach (var item in checkedTreeNodes)
            {
                list.Add(item.Tag.ToString());
            }
            try
            {
                YJH.Services.StoreMngService.Savebm_schedule_type(list, sid, Globle.CurrentEmployee.Base.Name);
                RadMessageBox.Show("保存成功", "提示");
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败", "提示");
            }
        }

        void lbStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lbStore.SelectedItems.Count > 0)
            {
                sid = lbStore.SelectedItem.Value.ToString().Substring(0, 7);
                rgbtree.Text = lbStore.SelectedItem.Text.ToString() + "类别进度";
                bmTree.Nodes.Clear();
                BuildTree();
                // 获取
                list = YJH.Services.StoreMngService.Getbm_schedule_type(sid);
                foreach (var schedule in list)
                {
                    var node = bmTree.Find(s => s.Tag.ToString() == schedule.class_id);
                    if (node != null)
                        node.Checked = true;
                }
            }
        }

        void BindList()
        {
            List<YJH.Entities.ba_store> entitylist = YJH.Services.StoreMngService.GetStore();
            foreach (YJH.Entities.ba_store item in entitylist)
            {
                RadListDataItem v = new RadListDataItem();
                v.Text = item.store_id + " " + item.store_name;
                v.Value = item.store_id;
                v.Image = Resources.Home16;
                v.ImageAlignment = ContentAlignment.MiddleLeft;
                lbStore.Items.Add(v);
            }
        }

        //构造树
        public void BuildTree()
        {
            list1 = YJH.Services.StoreMngService.Getba_class();
            foreach (var item in list1.Where(t => t.item_parent == null).OrderBy(t => t.item_order_id))
                LoopAddTreeNode(item, null);
        }

        private void LoopAddTreeNode(YJH.Entities.ba_class entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Text = entity.class_name;
            node.Value = entity.Instance.ID;
            node.Tag = entity.class_id;
            node.Name = entity.item_level.ToString();
            if (ParentNode == null)
                this.bmTree.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in list1.Where(t => t.item_parent == entity.class_id).OrderBy(t => t.item_order_id))
                LoopAddTreeNode(item, node);
        }
    }
}
