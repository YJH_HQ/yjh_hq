﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_ChangeJob : Telerik.WinControls.UI.RadForm
    {
        public string storeid = "";
        public Guid userid = Guid.Empty;
        public UI_ChangeJob()
        {
            InitializeComponent();
            RadListDataItem item = new RadListDataItem();
            item.Text = "店长";
            item.Value = Guid.Parse("175B09FE-3749-4550-95A7-87950FF078A5");
            ddljob.Items.Add(item);
            RadListDataItem item1 = new RadListDataItem();
            item1.Text = "储备店长";
            item1.Value = Guid.Parse("9370A75F-CF97-4156-8138-0789AE9224F2");
            ddljob.Items.Add(item1);
            RadListDataItem item2 = new RadListDataItem();
            item2.Text = "实习店长";
            item2.Value = Guid.Parse("489FC822-BD0B-4642-BA30-2413AA8872E3");
            ddljob.Items.Add(item2);
            RadListDataItem item3 = new RadListDataItem();
            item3.Text = "店员";
            item3.Value = Guid.Parse("535167AA-FFE3-4CB1-B34F-8F8BFEEF3FF5");
            ddljob.Items.Add(item3);
            RadListDataItem item4 = new RadListDataItem();
            item4.Text = "社区服务员";
            item4.Value = Guid.Parse("9C618AE1-BCE0-4699-BE55-435047861829");
            ddljob.Items.Add(item4);
            RadListDataItem item5 = new RadListDataItem();
            item5.Text = "兼职快递员";
            item5.Value = Guid.Parse("D049D391-A522-4CC8-9D7B-A3805BFBEEAE");
            ddljob.Items.Add(item5);
            ddljob.SelectedIndex = 0;
            this.btnCancel.Click += btnCancel_Click;
            this.btnConfirm.Click += btnConfirm_Click;
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                YJH.Services.StoreMngService.ChangeJob(userid, storeid, Guid.Parse(ddljob.SelectedItem.Value.ToString()));
                RadMessageBox.Show("变更成功", "提示");
                this.Close();
            }
            catch (Exception)
            {
                RadMessageBox.Show("变更失败", "提示");
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
