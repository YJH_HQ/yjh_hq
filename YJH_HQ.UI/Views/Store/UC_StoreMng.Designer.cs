﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UC_StoreMng
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            YJH_HQ.Controls.EnumColumn enumColumn1 = new YJH_HQ.Controls.EnumColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnModify = new Telerik.WinControls.UI.CommandBarButton();
            this.btnAdudit = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSend = new Telerik.WinControls.UI.CommandBarButton();
            this.btnCancel = new Telerik.WinControls.UI.CommandBarButton();
            this.btnUpdate = new Telerik.WinControls.UI.CommandBarButton();
            this.btnNewDowload = new Telerik.WinControls.UI.CommandBarButton();
            this.btnUpdateAllStore = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxStoreName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.dgStore = new YJH_HQ.Controls.DataGrid.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStoreName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(959, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnModify,
            this.btnAdudit,
            this.btnSend,
            this.btnCancel,
            this.btnUpdate,
            this.btnNewDowload,
            this.btnUpdateAllStore});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnModify
            // 
            this.btnModify.AccessibleDescription = "打开";
            this.btnModify.AccessibleName = "打开";
            this.btnModify.DisplayName = "commandBarButton1";
            this.btnModify.DrawText = true;
            this.btnModify.Image = global::YJH_HQ.UI.Properties.Resources.Notepad16;
            this.btnModify.Name = "btnModify";
            this.btnModify.Text = "修改";
            this.btnModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModify.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnAdudit
            // 
            this.btnAdudit.AccessibleDescription = "审核";
            this.btnAdudit.AccessibleName = "审核";
            this.btnAdudit.DisplayName = "commandBarButton2";
            this.btnAdudit.DrawText = true;
            this.btnAdudit.Image = global::YJH_HQ.UI.Properties.Resources.AuditButtonImage;
            this.btnAdudit.Name = "btnAdudit";
            this.btnAdudit.Text = "审核";
            this.btnAdudit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdudit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSend
            // 
            this.btnSend.AccessibleDescription = "发送";
            this.btnSend.AccessibleName = "发送";
            this.btnSend.DisplayName = "commandBarButton3";
            this.btnSend.DrawText = true;
            this.btnSend.Image = global::YJH_HQ.UI.Properties.Resources.ok;
            this.btnSend.Name = "btnSend";
            this.btnSend.Text = "发送";
            this.btnSend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSend.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleDescription = "取消";
            this.btnCancel.AccessibleName = "取消";
            this.btnCancel.DisplayName = "commandBarButton1";
            this.btnCancel.DrawText = true;
            this.btnCancel.Image = global::YJH_HQ.UI.Properties.Resources.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Text = "取消";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnUpdate
            // 
            this.btnUpdate.AccessibleDescription = "生成数据更新";
            this.btnUpdate.AccessibleName = "生成数据更新";
            this.btnUpdate.DisplayName = "commandBarButton1";
            this.btnUpdate.DrawText = true;
            this.btnUpdate.Image = global::YJH_HQ.UI.Properties.Resources.Adjustment;
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Text = "生成数据更新";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnNewDowload
            // 
            this.btnNewDowload.AccessibleDescription = "生成新通讯基础表";
            this.btnNewDowload.AccessibleName = "生成新通讯基础表";
            this.btnNewDowload.DisplayName = "commandBarButton1";
            this.btnNewDowload.DrawText = true;
            this.btnNewDowload.Image = global::YJH_HQ.UI.Properties.Resources.Adjustment;
            this.btnNewDowload.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnNewDowload.Name = "btnNewDowload";
            this.btnNewDowload.Text = "生成新通讯基础表";
            this.btnNewDowload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewDowload.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btnUpdateAllStore
            // 
            this.btnUpdateAllStore.AccessibleDescription = "生成全店数据更新";
            this.btnUpdateAllStore.AccessibleName = "生成全店数据更新";
            this.btnUpdateAllStore.DisplayName = "commandBarButton1";
            this.btnUpdateAllStore.DrawText = true;
            this.btnUpdateAllStore.Image = global::YJH_HQ.UI.Properties.Resources.Dispatch;
            this.btnUpdateAllStore.Name = "btnUpdateAllStore";
            this.btnUpdateAllStore.Text = "生成全店数据更新";
            this.btnUpdateAllStore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateAllStore.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxStoreName);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(959, 56);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "查询条件";
            // 
            // tbxStoreName
            // 
            this.tbxStoreName.Location = new System.Drawing.Point(116, 25);
            this.tbxStoreName.Name = "tbxStoreName";
            this.tbxStoreName.Size = new System.Drawing.Size(165, 20);
            this.tbxStoreName.TabIndex = 40;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(94, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "门店名称/店号：";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnSearch.Location = new System.Drawing.Point(298, 25);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSearch.Size = new System.Drawing.Size(86, 20);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "查询";
            // 
            // dgStore
            // 
            gridViewTextBoxColumn1.FieldName = "store_id";
            gridViewTextBoxColumn1.HeaderText = "店号";
            gridViewTextBoxColumn1.Name = "clstore_id";
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "store_name";
            gridViewTextBoxColumn2.HeaderText = "店名";
            gridViewTextBoxColumn2.Name = "clstore_name";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.FieldName = "versionnumber";
            gridViewTextBoxColumn3.HeaderText = "版本号";
            gridViewTextBoxColumn3.Name = "clversionnumber";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "store_type";
            gridViewTextBoxColumn4.HeaderText = "店铺类型";
            gridViewTextBoxColumn4.Name = "clstore_type";
            gridViewTextBoxColumn4.Width = 60;
            gridViewTextBoxColumn5.Expression = "";
            gridViewTextBoxColumn5.FieldName = "store_is_order";
            gridViewTextBoxColumn5.HeaderText = "是否采购";
            gridViewTextBoxColumn5.Name = "clstore_is_order";
            gridViewTextBoxColumn5.Width = 60;
            gridViewTextBoxColumn6.FieldName = "store_is_distribution";
            gridViewTextBoxColumn6.HeaderText = "是否配送";
            gridViewTextBoxColumn6.Name = "clstore_is_distribution";
            gridViewTextBoxColumn6.Width = 60;
            gridViewTextBoxColumn7.FieldName = "store_is_return";
            gridViewTextBoxColumn7.HeaderText = "是否退仓";
            gridViewTextBoxColumn7.Name = "clstore_is_return";
            gridViewTextBoxColumn7.Width = 60;
            gridViewTextBoxColumn8.FieldName = "store_is_income";
            gridViewTextBoxColumn8.HeaderText = "是否要货";
            gridViewTextBoxColumn8.Name = "clstore_is_income";
            gridViewTextBoxColumn8.Width = 60;
            gridViewTextBoxColumn9.FieldName = "store_is_inventory";
            gridViewTextBoxColumn9.HeaderText = "是否盘点";
            gridViewTextBoxColumn9.Name = "clstore_is_inventory";
            gridViewTextBoxColumn9.Width = 60;
            gridViewTextBoxColumn10.FieldName = "store_is_stock";
            gridViewTextBoxColumn10.HeaderText = "是否管理库存";
            gridViewTextBoxColumn10.Name = "clstore_is_stock";
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.FieldName = "store_is_pos";
            gridViewTextBoxColumn11.HeaderText = "是否启用pos";
            gridViewTextBoxColumn11.Name = "clstore_is_pos";
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.FieldName = "store_group_contract";
            gridViewTextBoxColumn12.HeaderText = "合同组";
            gridViewTextBoxColumn12.Name = "clstore_group_contract";
            gridViewTextBoxColumn13.FieldName = "store_group_ems";
            gridViewTextBoxColumn13.HeaderText = "快递组";
            gridViewTextBoxColumn13.Name = "clstore_group_ems";
            gridViewTextBoxColumn14.FieldName = "store_group_servies";
            gridViewTextBoxColumn14.HeaderText = "增值服务组";
            gridViewTextBoxColumn14.Name = "clstore_group_servies";
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.FieldName = "store_group_price";
            gridViewTextBoxColumn15.HeaderText = "价格组";
            gridViewTextBoxColumn15.Name = "clstore_group_price";
            gridViewTextBoxColumn16.FieldName = "store_parent_org";
            gridViewTextBoxColumn16.HeaderText = "上级组织";
            gridViewTextBoxColumn16.Name = "clstore_parent_org";
            gridViewTextBoxColumn16.Width = 60;
            gridViewTextBoxColumn17.FieldName = "store_business_org";
            gridViewTextBoxColumn17.HeaderText = "上级业务组织";
            gridViewTextBoxColumn17.Name = "clstore_business_org";
            gridViewTextBoxColumn17.Width = 80;
            gridViewTextBoxColumn18.Expression = "";
            gridViewTextBoxColumn18.FieldName = "store_issms";
            gridViewTextBoxColumn18.HeaderText = "是否短信";
            gridViewTextBoxColumn18.Name = "clstore_issms";
            gridViewTextBoxColumn18.Width = 100;
            gridViewTextBoxColumn19.FieldName = "store_tel";
            gridViewTextBoxColumn19.HeaderText = "门店电话";
            gridViewTextBoxColumn19.Name = "clstore_tel";
            gridViewTextBoxColumn19.Width = 100;
            gridViewTextBoxColumn20.FieldName = "store_fixedtel";
            gridViewTextBoxColumn20.HeaderText = "门店固定电话";
            gridViewTextBoxColumn20.Name = "clstore_fixedtel";
            gridViewTextBoxColumn20.Width = 100;
            gridViewTextBoxColumn21.FieldName = "store_address";
            gridViewTextBoxColumn21.HeaderText = "门店地址";
            gridViewTextBoxColumn21.Name = "clstore_address";
            gridViewTextBoxColumn21.Width = 150;
            gridViewTextBoxColumn22.FieldName = "store_zipcode";
            gridViewTextBoxColumn22.HeaderText = "门店邮编";
            gridViewTextBoxColumn22.Name = "clstore_zipcode";
            gridViewTextBoxColumn22.Width = 60;
            gridViewTextBoxColumn23.FieldName = "store_buslicensecode";
            gridViewTextBoxColumn23.HeaderText = "营业执照编号";
            gridViewTextBoxColumn23.Name = "clstore_buslicensecode";
            gridViewTextBoxColumn23.Width = 100;
            gridViewTextBoxColumn24.FieldName = "store_employeecount";
            gridViewTextBoxColumn24.HeaderText = "人员编制";
            gridViewTextBoxColumn24.Name = "clstore_employeecount";
            gridViewTextBoxColumn24.Width = 60;
            gridViewTextBoxColumn25.FieldName = "store_busstarttime";
            gridViewTextBoxColumn25.HeaderText = "门店启用时间";
            gridViewTextBoxColumn25.Name = "clstore_busstarttime";
            gridViewTextBoxColumn25.Width = 80;
            gridViewTextBoxColumn26.FieldName = "store_busendtime";
            gridViewTextBoxColumn26.HeaderText = "门店关闭时间";
            gridViewTextBoxColumn26.Name = "clstore_busendtime";
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn27.FieldName = "BUID";
            gridViewTextBoxColumn27.HeaderText = "公司ID";
            gridViewTextBoxColumn27.IsVisible = false;
            gridViewTextBoxColumn27.Name = "clBUID";
            gridViewTextBoxColumn28.FieldName = "AuditBy";
            gridViewTextBoxColumn28.HeaderText = "审核人";
            gridViewTextBoxColumn28.Name = "clAuditBy";
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn29.FieldName = "AuditDate";
            gridViewTextBoxColumn29.HeaderText = "审核时间";
            gridViewTextBoxColumn29.Name = "clAuditDate";
            gridViewTextBoxColumn29.Width = 100;
            enumColumn1.EnumModelID = "YJH.StoreState";
            enumColumn1.FieldName = "State";
            enumColumn1.HeaderText = "状态";
            enumColumn1.Name = "clState";
            enumColumn1.Width = 60;
            this.dgStore.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            enumColumn1});
            this.dgStore.Distinct = false;
            this.dgStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgStore.Location = new System.Drawing.Point(0, 86);
            this.dgStore.Name = "dgStore";
            this.dgStore.PageIndex = 0;
            this.dgStore.PageSize = 20;
            this.dgStore.ShowTotal = true;
            this.dgStore.Size = new System.Drawing.Size(959, 527);
            this.dgStore.TabIndex = 9;
            // 
            // UC_StoreMng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.dgStore);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_StoreMng";
            this.Size = new System.Drawing.Size(959, 613);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStoreName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnModify;
        private Telerik.WinControls.UI.CommandBarButton btnAdudit;
        private Telerik.WinControls.UI.CommandBarButton btnSend;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Controls.DataGrid.DataGrid dgStore;
        private Telerik.WinControls.UI.RadTextBox tbxStoreName;
        private Telerik.WinControls.UI.CommandBarButton btnCancel;
        private Telerik.WinControls.UI.CommandBarButton btnUpdate;
        private Telerik.WinControls.UI.CommandBarButton btnNewDowload;
        private Telerik.WinControls.UI.CommandBarButton btnUpdateAllStore;
    }
}
