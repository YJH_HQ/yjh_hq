﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH.Services;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_StoreMng : UserControl
    {
        #region ===字段及属性===
      
        #endregion

        #region ===构造方法===
        public UC_StoreMng()
        {
            InitializeComponent();
            this.dgStore.QueryMethod = "YJH.StoreMngService.Query";
            this.dgStore.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += UC_StoreMng_Load;         
            this.btnModify.Click += (s, e) => { this.OnOpen(); };
            this.btnAdudit.Click += (s, e) => { this.Audit(); };
            this.btnSend.Click += (s, e) => { this.SendData(); };
            this.btnCancel.Click += (s, e) => { this.Cancel(); };
            this.btnSearch.Click += (s, e) => { this.dgStore.LoadData(); };
            this.dgStore.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
            this.LoadPermission();
            this.btnUpdate.Click += btnUpdate_Click;
            this.btnUpdateAllStore.Click += btnUpdateAllStore_Click;
            this.btnNewDowload.Click += btnNewDowload_Click;
        }

        void btnUpdateAllStore_Click(object sender, EventArgs e)
        {
            UI_UpdateView u = new UI_UpdateView();
            u.IsAll = true;
            u.StartPosition = FormStartPosition.CenterScreen;
            u.ShowDialog();
        }

        void btnNewDowload_Click(object sender, EventArgs e)
        {
            try
            {
                StoreMngService.SetNewDowload();
                RadMessageBox.Show("生成成功", "提示");
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("生成失败" + ex.Message,"提示");
            }
        }

        void btnUpdate_Click(object sender, EventArgs e)
        {
            var id = this.dgStore.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ba_store(dps.Common.Data.Entity.Retrieve(YJH.Entities.ba_store.EntityModelID, id));

            if (string.IsNullOrEmpty(entity.store_id))
            {
                RadMessageBox.Show("请先录入门店号和其它信息后再生成！", "提示");
                return;
            }
            UI_UpdateView u = new UI_UpdateView();
            u.IsAll = false;
            u.entity = entity;
            u.StartPosition = FormStartPosition.CenterScreen;
            u.ShowDialog();
            
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "修改");
            this.btnAdudit.Enabled = funcs.Exists(t => t.Name == "审核");
            this.btnSend.Enabled = funcs.Exists(t => t.Name == "发送");
            this.btnCancel.Enabled = funcs.Exists(t => t.Name == "取消");
            this.btnUpdate.Enabled = funcs.Exists(t => t.Name == "生成数据更新");
        }


        private void OnOpen()
        {
            var id = this.dgStore.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ba_store(dps.Common.Data.Entity.Retrieve(YJH.Entities.ba_store.EntityModelID, id));
            var view = new UI_StoreMngView();
            view.dgList = this.dgStore;
            view.Entity = entity;
            view.ShowDialog();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = this.tbxStoreName.Text;
            return args;
        }

        /// <summary>
        /// 审核
        /// </summary>
        private void Audit()
        {
            var id = this.dgStore.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ba_store(dps.Common.Data.Entity.Retrieve(YJH.Entities.ba_store.EntityModelID, id));

            if (string.IsNullOrEmpty(entity.store_id))
            {
                RadMessageBox.Show("请先录入门店号和其它信息后再审核！", "提示");
                return;
            }
            if (entity.State != YJH.Enums.StoreState.新建)
                return;
            if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                entity.State = YJH.Enums.StoreState.审核;
                entity.AuditBy = Globle.CurrentEmployee.Base.Name;
                entity.AuditDate = System.DateTime.Now;
                YJH.Services.StoreMngService.Audit(entity);
                entity.Instance.AcceptChanges();

                var data = new Dictionary<string, object>();
                data.Add("AuditBy", entity.AuditBy);
                data.Add("AuditDate",entity.AuditDate);
                data.Add("State", entity.State);
                this.dgStore.RefreshSelectedRow(data);
            }
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        private void SendData()
        {
            var id = this.dgStore.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ba_store(dps.Common.Data.Entity.Retrieve(YJH.Entities.ba_store.EntityModelID, id));
            if (entity.State != YJH.Enums.StoreState.审核)
                return;
            //if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            //{
                entity.State = YJH.Enums.StoreState.生效;
                UI_SelectExpand u = new UI_SelectExpand();
                u.StartPosition = FormStartPosition.CenterScreen;
                u.entity = entity;
                u.ShowDialog();
                var data = new Dictionary<string, object>();
                data.Add("State", entity.State);
                this.dgStore.RefreshSelectedRow(data);
           // }
        }

        private void Cancel()
        {
            var id = this.dgStore.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ba_store(dps.Common.Data.Entity.Retrieve(YJH.Entities.ba_store.EntityModelID, id));
            if (entity.State == YJH.Enums.StoreState.新建)
                return;
            if (RadMessageBox.Show("确认取消吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                entity=YJH.Services.StoreMngService.Cancel(entity);
                //entity.Instance.AcceptChanges();

                var data = new Dictionary<string, object>();
                if (entity.State == YJH.Enums.StoreState.新建)
                {
                    data.Add("AuditBy", "");
                    data.Add("AuditDate", System.DBNull.Value);
                }
                data.Add("State", entity.State);
                this.dgStore.RefreshSelectedRow(data);
            }
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <remarks>建立人：张殿元 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_StoreMng_Load(object sender, EventArgs e)
        {
            this.dgStore.LoadData();
        }
        #endregion
    }
}
