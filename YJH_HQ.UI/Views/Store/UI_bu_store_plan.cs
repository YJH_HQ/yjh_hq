﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH.Entities;
using YJH.Services;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_bu_store_plan : Telerik.WinControls.UI.RadForm
    {
        private List<YJH.Entities.ba_store> listStore = null;
        public UI_bu_store_plan()
        {
            InitializeComponent();
            this.Load += UI_bu_store_plan_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime d = OrgUnitService.GetDate();
                foreach (GridViewRowInfo item in rgvplandetail.Rows)
                {
                    bu_store_plan plan = new bu_store_plan();
                    plan.plan_id = d.Year.ToString() + d.Month.ToString() + d.Day.ToString();
                    plan.plan_month = d.Month;
                    plan.store_id = item.Cells[0].Value.ToString();
                    if (item.Cells[1].Value != null)
                        plan.open_date = DateTime.Parse(item.Cells[1].Value.ToString());
                    if (item.Cells[2].Value.ToString() == "True")
                        plan.status = 20;
                    else
                        plan.status = 10;
                    YJH.Services.StorePlanService.Save(plan);
                }
                RadMessageBox.Show("保存成功", "提示");
                this.Close();
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败","提示");
            }
            
        }

        void UI_bu_store_plan_Load(object sender, EventArgs e)
        {
            SetColumnStore();
        }

        private void SetColumnStore()
        {
            listStore = YJH.Services.StorePlanService.GetStores();
            var columnStore = this.rgvplandetail.Columns["clstore_id"] as GridViewComboBoxColumn;
            var list = (from ot in listStore
                        select new
                        {
                            DisplayMember = ot.store_name,
                            ValueMember = ot.store_id
                        }).ToList();
            columnStore.DisplayMember = "DisplayMember";
            columnStore.ValueMember = "ValueMember";
            columnStore.DataSource = list;
        }
    }
}
