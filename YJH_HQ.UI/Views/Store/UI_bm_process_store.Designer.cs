﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UI_bm_process_store
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxprocess_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxprocess_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ddlprocess_type = new Telerik.WinControls.UI.RadDropDownList();
            this.cbxprocess_status = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxprocess_additional = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprocess_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprocess_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprocess_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxprocess_status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxprocess_additional)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(296, 30);
            this.radCommandBar2.TabIndex = 5;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxprocess_name
            // 
            this.tbxprocess_name.Location = new System.Drawing.Point(114, 78);
            this.tbxprocess_name.Name = "tbxprocess_name";
            this.tbxprocess_name.Size = new System.Drawing.Size(106, 20);
            this.tbxprocess_name.TabIndex = 8;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(43, 80);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(65, 18);
            this.radLabel30.TabIndex = 9;
            this.radLabel30.Text = "过程名称：";
            // 
            // tbxprocess_id
            // 
            this.tbxprocess_id.Location = new System.Drawing.Point(114, 54);
            this.tbxprocess_id.MaxLength = 11;
            this.tbxprocess_id.Name = "tbxprocess_id";
            this.tbxprocess_id.Size = new System.Drawing.Size(106, 20);
            this.tbxprocess_id.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(54, 56);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(54, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "过程号：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(43, 104);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "业务类型：";
            // 
            // ddlprocess_type
            // 
            this.ddlprocess_type.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlprocess_type.Location = new System.Drawing.Point(114, 104);
            this.ddlprocess_type.Name = "ddlprocess_type";
            this.ddlprocess_type.Size = new System.Drawing.Size(105, 20);
            this.ddlprocess_type.TabIndex = 12;
            // 
            // cbxprocess_status
            // 
            this.cbxprocess_status.Location = new System.Drawing.Point(114, 130);
            this.cbxprocess_status.Name = "cbxprocess_status";
            this.cbxprocess_status.Size = new System.Drawing.Size(68, 18);
            this.cbxprocess_status.TabIndex = 13;
            this.cbxprocess_status.Text = "是否启用";
            // 
            // cbxprocess_additional
            // 
            this.cbxprocess_additional.Location = new System.Drawing.Point(114, 154);
            this.cbxprocess_additional.Name = "cbxprocess_additional";
            this.cbxprocess_additional.Size = new System.Drawing.Size(115, 18);
            this.cbxprocess_additional.TabIndex = 14;
            this.cbxprocess_additional.Text = "是否有过程附加档";
            // 
            // UI_bm_process_store
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 202);
            this.Controls.Add(this.cbxprocess_additional);
            this.Controls.Add(this.cbxprocess_status);
            this.Controls.Add(this.ddlprocess_type);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbxprocess_name);
            this.Controls.Add(this.radLabel30);
            this.Controls.Add(this.tbxprocess_id);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radCommandBar2);
            this.Name = "UI_bm_process_store";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "拓店信息";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprocess_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprocess_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprocess_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxprocess_status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxprocess_additional)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxprocess_name;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox tbxprocess_id;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlprocess_type;
        private Telerik.WinControls.UI.RadCheckBox cbxprocess_status;
        private Telerik.WinControls.UI.RadCheckBox cbxprocess_additional;
    }
}