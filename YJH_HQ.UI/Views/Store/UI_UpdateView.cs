﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_UpdateView : Telerik.WinControls.UI.RadForm
    {
        public YJH.Entities.ba_store entity;
        public bool IsAll = false;
        public UI_UpdateView()
        {
            InitializeComponent();
            this.btnCancel.Click += btnCancel_Click;
            this.btnSave.Click += btnSave_Click;
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxvnumber.Text.Trim()))
                return;
            if (RadMessageBox.Show("确认生成吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    if(IsAll == false)
                        YJH.Services.StoreMngService.UpdateStore(entity.store_id,tbxvnumber.Text.Trim());
                    else
                        YJH.Services.StoreMngService.UpdateAllStore(tbxvnumber.Text.Trim());
                    MessageBox.Show("生成成功", "提示");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("生成失败", ex.Message);
                }

            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbxvnumber_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
