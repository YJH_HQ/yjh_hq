﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_bu_store_plan : UserControl
    {
        public UC_bu_store_plan()
        {
            InitializeComponent();
            this.Load += UC_bu_store_plan_Load;
            this.btnNew.Click += btnNew_Click;
            this.btnSearch.Click += (s, e) => { this.Search(); };
        }

        void Search()
        {
            DataTable dt = YJH.Services.StorePlanService.Query(tbxInfo.Text);
            dt.Columns.Add("num", typeof(int));
            var query = from d in dt.AsEnumerable()
                        group d by new { pid = d.Field<String>("plan_id"), month = d.Field<int>("plan_month") } into g
                        select new
                        {
                            plan_id = g.Key.pid,
                            plan_month = g.Key.month,
                            num = g.Count()
                        };
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("plan_id");
            dt1.Columns.Add("plan_month");
            dt1.Columns.Add("num");
            foreach (var q in query)
            {
                DataRow dr = dt1.NewRow();
                dr["plan_id"] = q.plan_id;
                dr["plan_month"] = q.plan_month;
                dr["num"] = q.num;
                dt1.Rows.Add(dr);
            }
            dgPlan.DataSource = dt1;
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            UI_bu_store_plan plan = new UI_bu_store_plan();
            plan.StartPosition = FormStartPosition.CenterScreen;
            plan.ShowDialog();
            Search();
        }

        void UC_bu_store_plan_Load(object sender, EventArgs e)
        {
            
        }
    }
}
