﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UI_UserTreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.UserTree = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.cbxisenabled = new Telerik.WinControls.UI.RadCheckBox();
            this.ddljob = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnCancel = new Telerik.WinControls.UI.RadButton();
            this.btnConfirm = new Telerik.WinControls.UI.RadButton();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxisenabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddljob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.UserTree);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "门店人员列表";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(362, 249);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "门店人员列表";
            // 
            // UserTree
            // 
            this.UserTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserTree.Location = new System.Drawing.Point(2, 18);
            this.UserTree.Name = "UserTree";
            this.UserTree.Size = new System.Drawing.Size(358, 229);
            this.UserTree.SpacingBetweenNodes = -1;
            this.UserTree.TabIndex = 0;
            this.UserTree.Text = "radTreeView1";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.cbxisenabled);
            this.radPanel1.Controls.Add(this.ddljob);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.btnCancel);
            this.radPanel1.Controls.Add(this.btnConfirm);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 249);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(362, 72);
            this.radPanel1.TabIndex = 4;
            // 
            // cbxisenabled
            // 
            this.cbxisenabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxisenabled.Location = new System.Drawing.Point(237, 7);
            this.cbxisenabled.Name = "cbxisenabled";
            this.cbxisenabled.Size = new System.Drawing.Size(68, 18);
            this.cbxisenabled.TabIndex = 5;
            this.cbxisenabled.Text = "是否主职";
            this.cbxisenabled.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // ddljob
            // 
            this.ddljob.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddljob.Location = new System.Drawing.Point(65, 7);
            this.ddljob.Name = "ddljob";
            this.ddljob.Size = new System.Drawing.Size(125, 20);
            this.ddljob.TabIndex = 4;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 7);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "岗位：";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(273, 37);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(195, 36);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(72, 24);
            this.btnConfirm.TabIndex = 0;
            this.btnConfirm.Text = "确定";
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radGroupBox1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(0, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(362, 249);
            this.radPanel2.TabIndex = 5;
            this.radPanel2.Text = "radPanel2";
            // 
            // UI_UserTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 321);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.Name = "UI_UserTreeView";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "绑定现有人员";
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxisenabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddljob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btnCancel;
        private Telerik.WinControls.UI.RadButton btnConfirm;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadTreeView UserTree;
        private Telerik.WinControls.UI.RadCheckBox cbxisenabled;
        private Telerik.WinControls.UI.RadDropDownList ddljob;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}