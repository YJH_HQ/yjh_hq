﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH_HQ.UI.Properties;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_UserMng : UserControl
    {
        private string sid = "";
        public UC_UserMng()
        {
            InitializeComponent();
            LoadPermission();
            BindData();
            //this.btnNew.Click += btnNew_Click;
            //this.btnEdit.Click += btnEdit_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.btnBindUser.Click += btnBindUser_Click;
            lbStore.SelectedIndexChanged += lbStore_SelectedIndexChanged;
            this.btnRejob.Click += btnRejob_Click;
            this.btnEnabled.Click += btnEnabled_Click;
        }

        void btnEnabled_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                if (dgUser.SelectedRows.Count > 0)
                {
                    try
                    {
                        bool en;
                        if (dgUser.SelectedRows[0].Cells["cljob"].Value.ToString() != "店长")
                        {
                            RadMessageBox.Show("只能修改店长的状态", "提示");
                            return;
                        }
                        if (dgUser.SelectedRows[0].Cells["clstatus"].Value.ToString() == "兼职")
                        {
                            en = true;
                            YJH.Services.StoreMngService.ChangeEnabled(Guid.Parse(dgUser.SelectedRows[0].Cells["clID"].Value.ToString()), sid, en);
                            RadMessageBox.Show("修改成功", "提示");
                        }
                        else
                        {
                            en = false;
                            YJH.Services.StoreMngService.ChangeEnabled(Guid.Parse(dgUser.SelectedRows[0].Cells["clID"].Value.ToString()), sid, en);
                            RadMessageBox.Show("修改成功", "提示");
                        }
                        dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show("执行失败，原因：" + ex.Message, "提示");
                    }
                    dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
                }
                else
                    RadMessageBox.Show("请选择需要修改的记录", "提示");
            }
        }

        void btnRejob_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                if (dgUser.SelectedRows.Count > 0)
                {
                    UI_ChangeJob j = new UI_ChangeJob();
                    j.StartPosition = FormStartPosition.CenterScreen;
                    j.storeid = sid;
                    j.userid = Guid.Parse(dgUser.SelectedRows[0].Cells["clID"].Value.ToString());
                    j.ShowDialog();
                    dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
                }
                else
                    RadMessageBox.Show("请选择需要变更的记录", "提示");
            }
            
        }

        void lbStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                sid = lbStore.SelectedItem.Value.ToString().Substring(0, 7);
                dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
            }
            catch (Exception ex)
            {
            }
            
        }

        void btnBindUser_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                UI_UserTreeView t = new UI_UserTreeView();
                t.sid = sid;
                t.StartPosition = FormStartPosition.CenterScreen;
                t.ShowDialog();
                dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
            }
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                if (dgUser.SelectedRows.Count > 0)
                {
                    if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        try
                        {
                            YJH.Services.StoreMngService.DeleteUser(Guid.Parse(dgUser.SelectedRows[0].Cells["clID"].Value.ToString()), sid);
                            dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
                            RadMessageBox.Show("删除成功", "提示");
                        }
                        catch (Exception)
                        {
                            RadMessageBox.Show("删除失败", "提示");
                        }
                        
                    }
                }
                else
                    RadMessageBox.Show("请选择需要删除的记录", "提示");
            }
        }

        /*void btnEdit_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                if(dgUser.SelectedRows.Count > 0)
                {
                    UI_UserView u = new UI_UserView();
                    u.store_id = sid;
                    u.state = 20;
                    u.userid = Guid.Parse(dgUser.SelectedRows[0].Cells["clID"].Value.ToString());
                    u.StartPosition = FormStartPosition.CenterScreen;
                    u.ShowDialog();
                    dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
                }
                else
                    RadMessageBox.Show("请选择需要修改的记录", "提示");
            }
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            if (sid == "")
                RadMessageBox.Show("请先选择门店", "提示");
            else
            {
                UI_UserView u = new UI_UserView();
                u.store_id = sid;
                u.state = 10;
                u.StartPosition = FormStartPosition.CenterScreen;
                u.ShowDialog();
                dgUser.DataSource = YJH.Services.StoreMngService.GetUser(sid);
            }
        }*/

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
            this.btnBindUser.Enabled = funcs.Exists(t => t.Name == "绑定员工");
            this.btnEnabled.Enabled = funcs.Exists(t => t.Name == "主职/兼职");
            this.btnRejob.Enabled = funcs.Exists(t => t.Name == "岗位变更");
        }

        void BindData()
        {
            List<YJH.Entities.ba_store> entitylist = YJH.Services.StoreMngService.GetStore();
            foreach (YJH.Entities.ba_store item in entitylist)
            {
                RadListDataItem v = new RadListDataItem();
                v.Text = item.store_id + " " + item.store_name;
                v.Value = item.store_id;
                v.Image = Resources.Home16;
                v.ImageAlignment = ContentAlignment.MiddleLeft;
                lbStore.Items.Add(v);
            }
        }
    }
}
