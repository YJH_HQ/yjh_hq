﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_Ba_Class : Telerik.WinControls.UI.RadForm
    {
        public string parent = "";
        public bool IsNew = true;
        public Guid id = Guid.Empty;
        public bool IsChild = true;
        RadTreeNode currentnode = new RadTreeNode();
        UC_Ba_Class baclass = new UC_Ba_Class();
        public UI_Ba_Class(UC_Ba_Class BC)
        {
            InitializeComponent();
            baclass = BC;
            this.Load += UI_Ba_Class_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                YJH.Entities.ba_class entity = YJH.Services.StoreMngService.Getba_classByID(id);
                if (IsNew)
                {
                    YJH.Entities.ba_class newentity = new YJH.Entities.ba_class();
                    newentity.item_order_id = 0;
                    newentity.item_no = 0;
                    newentity.class_id = tbxclass_id.Text.Trim();
                    newentity.class_name = tbxclass_name.Text.Trim();
                    newentity.item_type = ddlitem_type.SelectedIndex;
                    
                    if (cbxitem_status.Checked)
                        newentity.item_status = 1;
                    else
                        newentity.item_status = 0;
                    if (cbxclass_additional.Checked)
                        newentity.class_additional = 1;
                    else
                        newentity.class_additional = 0;
                    if (IsChild)
                    {
                        newentity.item_level = entity.item_level + 1;
                        newentity.item_parent = entity.class_id;
                        if (entity.item_level == 2)
                            newentity.item_isfinal = 1;
                        else
                            newentity.item_isfinal = 0;
                    }
                    else
                    {
                        newentity.item_level = entity.item_level;
                        newentity.item_parent = entity.item_parent;
                        if (entity.item_level == 3)
                            newentity.item_isfinal = 1;
                        else
                            newentity.item_isfinal = 0;
                    }
                    bool result = YJH.Services.StoreMngService.IsHasClass_id(newentity);
                    if (result)
                    {
                        RadMessageBox.Show("该类别号已存在，请勿重复添加", "提示");
                        return;
                    }
                    YJH.Services.StoreMngService.SaveBa_Class(newentity);
                    RadMessageBox.Show("保存成功", "提示");
                    currentnode.Text = newentity.class_name;
                    currentnode.Value = newentity.Instance.ID;
                    currentnode.Tag = newentity.class_id;
                    if (IsChild)
                    {
                        currentnode.Name = newentity.item_level.ToString();
                        baclass.isChild = true;
                        baclass.RefeshTree(currentnode);
                    }
                    else
                    {
                        currentnode.Name = newentity.item_level.ToString();
                        baclass.isChild = false;
                        baclass.RefeshTree(currentnode);
                    }
                    this.Close();
                }
                else
                {
                    if (id != Guid.Empty)
                    {
                        entity.class_id = tbxclass_id.Text.Trim();
                        entity.class_name = tbxclass_name.Text.Trim();
                        entity.item_type = ddlitem_type.SelectedIndex;
                        if (cbxitem_status.Checked)
                            entity.item_status = 1;
                        else
                            entity.item_status = 0;
                        if (cbxclass_additional.Checked)
                            entity.class_additional = 1;
                        else
                            entity.class_additional = 0;
                       
                        bool result = YJH.Services.StoreMngService.IsHasClass_id(entity);
                        if (result)
                        {
                            RadMessageBox.Show("该类别号已存在，请勿重复添加", "提示");
                            return;
                        }
                        YJH.Services.StoreMngService.SaveBa_Class(entity);
                        RadMessageBox.Show("保存成功", "提示");
                        currentnode.Text = entity.class_name;
                        currentnode.Value = entity.Instance.ID;
                        currentnode.Tag = entity.class_id;
                        currentnode.Name = entity.item_level.ToString();
                        baclass.selectNode.Value = currentnode.Value;
                        baclass.selectNode.Text = currentnode.Text;
                        baclass.selectNode.Tag = currentnode.Tag;
                        baclass.selectNode.Name = currentnode.Name;
                        this.Close();
                    }
                    else
                    {
                        RadMessageBox.Show("保存失败", "提示");
                        return;
                    }
                }
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败", "提示");
                return;
            }
        }

        void UI_Ba_Class_Load(object sender, EventArgs e)
        {
            foreach (var item in Enum.GetNames(typeof(YJH.Enums.item_type)))
            {
                RadListDataItem i = new RadListDataItem();
                i.Text = item;
                ddlitem_type.Items.Add(i);
            }
            if(IsNew)
            {
                tbxclass_id.Text = "";
                tbxclass_name.Text = "";
                ddlitem_type.SelectedIndex = 0;
                cbxclass_additional.Checked = false;
                cbxitem_status.Checked = true;
            }
            else
            {
                if(id != Guid.Empty)
                {
                    YJH.Entities.ba_class entity = YJH.Services.StoreMngService.Getba_classByID(id);
                    if(entity != null)
                    {
                        tbxclass_id.Text = entity.class_id;
                        tbxclass_name.Text = entity.class_name;
                        ddlitem_type.SelectedIndex = entity.item_type;
                        if(entity.class_additional == 0)
                            cbxclass_additional.Checked = false;
                        else
                            cbxclass_additional.Checked = true;
                        if(entity.item_status == 1)
                            cbxitem_status.Checked = true;
                        else
                            cbxitem_status.Checked = false;
                    }
                }
            }
        }
    }
}
