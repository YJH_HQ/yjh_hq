﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH.Entities;
using YJH_HQ.Controls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_StoreMngView : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private string no;//临时的拓展单号
        private YJH.Entities.ba_store _entity;
        public YJH.Entities.ba_store Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion


        #region ====构造====

        public UI_StoreMngView()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddlprovince.SelectedIndexChanged += ddlprovince_SelectedIndexChanged;
            this.ddlcity.SelectedIndexChanged += ddlcity_SelectedIndexChanged;
            this.ddlarea.SelectedIndexChanged += ddlarea_SelectedIndexChanged;

            //RadMultiColumnComboBoxElement multiColumnComboElement = this.cbExpand_id.MultiColumnComboBoxElement;
            //multiColumnComboElement.DropDownSizingMode = SizingMode.UpDownAndRightBottom; 
            //multiColumnComboElement.DropDownMinSize = new Size(200,100); 
            //GridViewTextBoxColumn column = new GridViewTextBoxColumn("expand_id");
            //column.HeaderText = "拓展单号";
            //column.Width = 80;
            //multiColumnComboElement.Columns.Add(column);
            //column = new GridViewTextBoxColumn("community_name");
            //column.HeaderText = "小区名称";
            //column.Width = 100;
            //multiColumnComboElement.Columns.Add(column);
            //System.Data.DataTable dt = YJH.Services.StoreMngService.GetExpand_IDTable();
            //this.cbExpand_id.ValueMember = "expand_id";
            //this.cbExpand_id.DisplayMember = "community_name";
            //this.cbExpand_id.DataSource = dt;

        }
        #endregion


        #region ====方法====
        private void BindData()
        {
            if (_entity == null)
                return;
            this.tbxStore_id.Text = _entity.store_id;
            this.tbxStore_name.Text = _entity.store_name;
            this.tbxStore_type.Text = _entity.store_type.ToString();
            this.cbxStore_is_order.Checked = _entity.store_is_order == 1 ? true : false;
            this.cbxStore_is_distribution.Checked = _entity.store_is_distribution == 1 ? true : false;
            this.cbxStore_is_return.Checked = _entity.store_is_return == 1 ? true : false;
            this.cbxStore_is_income.Checked = _entity.store_is_income == 1 ? true : false;
            this.cbxStore_is_inventory.Checked = _entity.store_is_inventory == 1 ? true : false;
            this.cbxStore_is_stock.Checked = _entity.store_is_stock == 1 ? true : false;
            this.cbxStore_is_pos.Checked = _entity.store_is_pos == 1 ? true : false;
            this.tbxStore_group_contract.Text = _entity.store_group_contract;
            this.tbxStore_group_ems.Text = _entity.store_group_ems;
            this.tbxStore_group_servies.Text = _entity.store_group_servies;
            this.tbxStore_group_price.Text = _entity.store_group_price;
            this.tbxStore_parent_org.Text = _entity.store_parent_org;
            this.tbxStore_business_org.Text = _entity.store_business_org;
            this.cbxStore_issms.Checked = _entity.store_issms == 1 ? true : false;
            this.tbxStore_tel.Text = _entity.store_tel;
            this.tbxStore_fixedtel.Text = _entity.store_fixedtel;
            this.tbxStore_address.Text = _entity.store_address;
            this.tbxStore_zipcode.Text = _entity.store_zipcode;
            this.tbxStore_buslicensecode.Text = _entity.store_buslicensecode;
            this.tbxStore_employeecount.Text = _entity.store_employeecount.ToString();
            //this.dpkStore_busstarttime.Value =_entity.store_busstarttime!=null? DateTime.Parse(_entity.store_busstarttime):System.DateTime.Now;
            //this.dpkStore_busendtime.Value = _entity.store_busendtime != null ? DateTime.Parse(_entity.store_busendtime) : System.DateTime.Now;
            this.tbxStartTime.Text = _entity.store_busstarttime;
            this.tbxEndTime.Text = _entity.store_busendtime;
            this.cmbState.SelectedValue = (int)_entity.State;
            this.tbxAuditBy.Text = _entity.AuditBy;
            this.tbxAuditDate.Text = _entity.AuditDate.ToString();
            if (_entity.store_id != null)
            {
                //YJH.Entities.bm_process_store_bill bill = YJH.Services.StoreMngService.GetBill(_entity.store_id);
                //if (bill != null)
                //{
                //    this.cbExpand_id.Text = bill.community_name;
                //    this.no = bill.expand_id;
                //}
            }
        }

        private void FlushData()
        {
            _entity.store_id = this.tbxStore_id.Text;
            _entity.store_name = this.tbxStore_name.Text;
            _entity.store_type = Convert.ToInt32(this.tbxStore_type.Text);
            _entity.store_is_order = this.cbxStore_is_order.Checked == true ? 1 : 0;
            _entity.store_is_distribution = this.cbxStore_is_distribution.Checked == true ? 1 : 0;
            _entity.store_is_return = this.cbxStore_is_return.Checked == true ? 1 : 0;
            _entity.store_is_income = this.cbxStore_is_income.Checked == true ? 1 : 0;
            _entity.store_is_inventory = this.cbxStore_is_inventory.Checked == true ? 1 : 0;
            _entity.store_is_stock = this.cbxStore_is_stock.Checked == true ? 1 : 0;
            _entity.store_is_pos = this.cbxStore_is_pos.Checked == true ? 1 : 0;
            _entity.store_group_contract = this.tbxStore_group_contract.Text;
            _entity.store_group_ems = this.tbxStore_group_ems.Text;
            _entity.store_group_servies = this.tbxStore_group_servies.Text;
            _entity.store_group_price = this.tbxStore_group_price.Text;
            _entity.store_parent_org = this.tbxStore_parent_org.Text;
            _entity.store_business_org = this.tbxStore_business_org.Text;
            // _entity.store_issms = this.cbxStore_issms.Checked == true ? Convert.ToByte(1) : Convert.ToByte(0);
            if (this.cbxStore_issms.Checked == true)
                _entity.store_issms = 1;
            else
                _entity.store_issms = 0;
            _entity.store_tel = this.tbxStore_tel.Text;
            _entity.store_fixedtel = this.tbxStore_fixedtel.Text;
            _entity.store_address = this.tbxStore_address.Text;
            _entity.store_zipcode = this.tbxStore_zipcode.Text;
            _entity.store_buslicensecode = this.tbxStore_buslicensecode.Text;
            _entity.store_employeecount = Convert.ToInt32(this.tbxStore_employeecount.Text);
            //_entity.store_busstarttime = this.dpkStore_busstarttime.Value.ToShortDateString();
            //_entity.store_busendtime = this.dpkStore_busendtime.Value.ToShortDateString();
            _entity.store_busstarttime = this.tbxStartTime.Text;
            _entity.store_busendtime = this.tbxEndTime.Text;
            _entity.State = (YJH.Enums.StoreState)this.cmbState.SelectedValue;
            _entity.data_check = 0;
        }

        private void OnLoad()
        {
            this.tbxStore_id.Focus();
            this.LoadPermission();
            //省
            List<ba_district> list = YJH.Services.StoreMngService.Getprovince();
            ddlprovince.ValueMember = "ownerid";
            ddlprovince.DisplayMember = "city_name";
            ddlprovince.DataSource = list;
            ddlprovince.SelectedIndex = 9;
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ba_store entity)
        {
            //刷新列表
            //if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            //{
            //    var data = new Dictionary<string, object>();
            //    data.Add("ID", entity.Instance.ID);
            //    data.Add("Name", entity.Name);
            //    this.dgList.AddRow(data);
            //}
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("store_id", entity.store_id);
                data.Add("store_name", entity.store_name);
                data.Add("store_type", entity.store_type);
                data.Add("store_is_order", entity.store_is_order);
                data.Add("store_is_distribution", entity.store_is_distribution);
                data.Add("store_is_return", entity.store_is_return);
                data.Add("store_is_income", entity.store_is_income);
                data.Add("store_is_inventory", entity.store_is_inventory);
                data.Add("store_is_stock", entity.store_is_stock);
                data.Add("store_is_pos", entity.store_is_pos);
                data.Add("store_group_contract", entity.store_group_contract);
                data.Add("store_group_ems", entity.store_group_ems);
                data.Add("store_group_servies", entity.store_group_servies);
                data.Add("store_group_price", entity.store_group_price);
                data.Add("store_parent_org", entity.store_parent_org);
                data.Add("store_business_org", entity.store_business_org);
                data.Add("store_issms", entity.store_issms);
                data.Add("store_tel", entity.store_tel);
                data.Add("store_fixedtel", entity.store_fixedtel);
                data.Add("store_address", entity.store_address);
                data.Add("store_zipcode", entity.store_zipcode);
                data.Add("store_buslicensecode", entity.store_buslicensecode);
                data.Add("store_employeecount", entity.store_employeecount);
                data.Add("store_busstarttime", entity.store_busstarttime);
                data.Add("store_busendtime", entity.store_busendtime);
                //data.Add("AuditBy", entity.AuditBy);
                //data.Add("AuditDate", DBNull.Value);
                data.Add("State", entity.State);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        private void Save()
        {
            try
            {
                if (_entity.State != YJH.Enums.StoreState.新建)
                    return;

                //if (string.IsNullOrWhiteSpace(no))
                //{
                //    if (this.cbExpand_id.SelectedValue == null)
                //    {
                //        RadMessageBox.Show("请选择小区名称", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //        return;
                //    }
                //}

                if (_entity.store_id != this.tbxStore_id.Text && !string.IsNullOrEmpty(this.tbxStore_id.Text))
                {
                    if (!YJH.Services.StoreMngService.Store_idIsNull(this.tbxStore_id.Text))
                    {
                        RadMessageBox.Show("店号已存在,请重新输入", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                        //this.tipbar.ShowTip("行业类型名称不能为空！", TipBar.TipType.Warning);
                        return;
                    }
                }
                this.FlushData();

                if (string.IsNullOrEmpty(_entity.store_id))
                {
                    RadMessageBox.Show("店号不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    //this.tipbar.ShowTip("行业类型名称不能为空！", TipBar.TipType.Warning);
                    return;
                }
                //记录保存前的实体状态
                var entityState = _entity.Instance.PersistentState;
                if (string.IsNullOrWhiteSpace(no))
                {
                    //YJH.Services.StoreMngService.Save(_entity, this.cbExpand_id.SelectedValue.ToString(), no, 1);
                }
                else
                {
                    //if (this.cbExpand_id.SelectedValue != null)
                    //{
                    //    YJH.Services.StoreMngService.Save(_entity, this.cbExpand_id.SelectedValue.ToString(), no, 2);
                    //}
                    //else
                    //{
                    //    YJH.Services.StoreMngService.Save(_entity, "", no, 3);
                    //}
                   
                }
                YJH.Services.StoreMngService.Save(_entity, "", no, 3);
                _entity.Instance.AcceptChanges();
                //刷新列表
                this.RefreshList(entityState, _entity);
                this.Close();
                //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        void ddlarea_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载街道
            if (ddlarea.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlarea.SelectedValue.ToString()));
                ddlstreet.ValueMember = "ownerid";
                ddlstreet.DisplayMember = "city_name";
                ddlstreet.DataSource = list;
                ddlstreet.SelectedIndex = -1;
            }
        }

        void ddlcity_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载区
            if (ddlcity.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlcity.SelectedValue.ToString()));
                ddlarea.ValueMember = "ownerid";
                ddlarea.DisplayMember = "city_name";
                ddlarea.DataSource = list;
                ddlarea.SelectedIndex = -1;
            }
        }

        void ddlprovince_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            //加载市
            if (ddlprovince.SelectedItems.Count > 0)
            {
                List<ba_district> list = YJH.Services.StoreMngService.Getchild(int.Parse(ddlprovince.SelectedValue.ToString()));
                ddlcity.ValueMember = "ownerid";
                ddlcity.DisplayMember = "city_name";
                ddlcity.DataSource = list;
                ddlcity.SelectedIndex = -1;
            }
        }
        #endregion
    }
}
