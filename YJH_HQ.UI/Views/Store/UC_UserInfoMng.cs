﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UC_UserInfoMng : UserControl
    {
        public UC_UserInfoMng()
        {
            InitializeComponent();
            this.dgUser.QueryMethod = "YJH.StoreMngService.GetQuery";
            this.dgUser.GetQueryArgsFunc = this.GetQueryArgs;
            this.LoadPermission();
            this.Load += UC_UserInfoMng_Load;
            this.btnNew.Click += btnNew_Click;
            this.btnEdit.Click += btnEdit_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.btnSearch.Click += btnSearch_Click;
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            this.dgUser.LoadData();
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgUser.GridView.SelectedRows.Count > 0)
            {
                if (RadMessageBox.Show("确认删除吗？删除会导致该员工所有在职门店账号均无法使用！", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    try
                    {
                        YJH.Services.StoreMngService.DeleteAllUser(Guid.Parse(dgUser.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                        this.dgUser.LoadData();
                        RadMessageBox.Show("删除成功", "提示");
                    }
                    catch (Exception)
                    {
                        RadMessageBox.Show("删除失败", "提示");
                    }

                }
            }
            else
                RadMessageBox.Show("请选择需要删除的记录", "提示");
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgUser.GridView.SelectedRows.Count > 0)
            {
                UI_UserView u = new UI_UserView();
                u.state = 20;
                u.userid = Guid.Parse(dgUser.GridView.SelectedRows[0].Cells["clID"].Value.ToString());
                u.StartPosition = FormStartPosition.CenterScreen;
                u.ShowDialog();
                this.dgUser.LoadData();
            }
            else
                RadMessageBox.Show("请选择需要编辑的记录", "提示");
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            UI_UserView u = new UI_UserView();
            u.state = 10;
            u.StartPosition = FormStartPosition.CenterScreen;
            u.ShowDialog();
            this.dgUser.LoadData();
        }

        void UC_UserInfoMng_Load(object sender, EventArgs e)
        {
            this.dgUser.LoadData();
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnEdit.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = tbxInfo.Text.Trim();
            return args;
        }
    }
}
