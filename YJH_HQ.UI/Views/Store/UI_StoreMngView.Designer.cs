﻿namespace YJH_HQ.UI.Views.Store
{
    partial class UI_StoreMngView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_id = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.ddlstreet = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.ddlarea = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.ddlcity = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlprovince = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbxmarslat = new Telerik.WinControls.UI.RadTextBox();
            this.tbxmarslon = new Telerik.WinControls.UI.RadTextBox();
            this.tbxbaidulat = new Telerik.WinControls.UI.RadTextBox();
            this.tbxbaidulon = new Telerik.WinControls.UI.RadTextBox();
            this.tbxEndTime = new Telerik.WinControls.UI.RadTextBox();
            this.tbxStartTime = new Telerik.WinControls.UI.RadTextBox();
            this.cbxStore_issms = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxStore_is_pos = new Telerik.WinControls.UI.RadCheckBox();
            this.cmbState = new YJH_HQ.Controls.EnumPicker();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.tbxAuditDate = new Telerik.WinControls.UI.RadTextBox();
            this.tbxAuditBy = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_employeecount = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_buslicensecode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_zipcode = new Telerik.WinControls.UI.RadTextBox();
            this.tbxStore_fixedtel = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxStore_address = new Telerik.WinControls.UI.RadTextBox();
            this.tbxStore_tel = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_business_org = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_parent_org = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_group_price = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_group_servies = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_group_ems = new Telerik.WinControls.UI.RadTextBox();
            this.cbxStore_is_stock = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxStore_is_inventory = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxStore_is_income = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxStore_is_return = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxStore_is_distribution = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxStore_type = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxStore_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.cbxStore_is_order = new Telerik.WinControls.UI.RadCheckBox();
            this.tbxStore_group_contract = new Telerik.WinControls.UI.RadTextBox();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxmarslat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxmarslon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbaidulat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbaidulon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_issms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_pos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAuditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAuditBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_employeecount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_buslicensecode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_zipcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_fixedtel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_address)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_business_org)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_parent_org)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_servies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_ems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_stock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_inventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_income)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_return)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_distribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_order)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(66, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "店号：";
            // 
            // tbxStore_id
            // 
            this.tbxStore_id.Location = new System.Drawing.Point(114, 19);
            this.tbxStore_id.MaxLength = 11;
            this.tbxStore_id.Name = "tbxStore_id";
            this.tbxStore_id.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_id.TabIndex = 0;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel27);
            this.radGroupBox1.Controls.Add(this.ddlstreet);
            this.radGroupBox1.Controls.Add(this.radLabel26);
            this.radGroupBox1.Controls.Add(this.ddlarea);
            this.radGroupBox1.Controls.Add(this.radLabel25);
            this.radGroupBox1.Controls.Add(this.radLabel24);
            this.radGroupBox1.Controls.Add(this.ddlcity);
            this.radGroupBox1.Controls.Add(this.ddlprovince);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.tbxmarslat);
            this.radGroupBox1.Controls.Add(this.tbxmarslon);
            this.radGroupBox1.Controls.Add(this.tbxbaidulat);
            this.radGroupBox1.Controls.Add(this.tbxbaidulon);
            this.radGroupBox1.Controls.Add(this.tbxEndTime);
            this.radGroupBox1.Controls.Add(this.tbxStartTime);
            this.radGroupBox1.Controls.Add(this.cbxStore_issms);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_pos);
            this.radGroupBox1.Controls.Add(this.cmbState);
            this.radGroupBox1.Controls.Add(this.radLabel21);
            this.radGroupBox1.Controls.Add(this.radLabel20);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.tbxAuditDate);
            this.radGroupBox1.Controls.Add(this.tbxAuditBy);
            this.radGroupBox1.Controls.Add(this.radLabel18);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.tbxStore_employeecount);
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.tbxStore_buslicensecode);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.tbxStore_zipcode);
            this.radGroupBox1.Controls.Add(this.tbxStore_fixedtel);
            this.radGroupBox1.Controls.Add(this.tbxStore_address);
            this.radGroupBox1.Controls.Add(this.tbxStore_tel);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.tbxStore_business_org);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.tbxStore_parent_org);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.tbxStore_group_price);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.tbxStore_group_servies);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxStore_group_ems);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_stock);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_inventory);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_income);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_return);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_distribution);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxStore_type);
            this.radGroupBox1.Controls.Add(this.tbxStore_name);
            this.radGroupBox1.Controls.Add(this.radLabel30);
            this.radGroupBox1.Controls.Add(this.cbxStore_is_order);
            this.radGroupBox1.Controls.Add(this.tbxStore_group_contract);
            this.radGroupBox1.Controls.Add(this.tbxStore_id);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(676, 384);
            this.radGroupBox1.TabIndex = 3;
            // 
            // radLabel27
            // 
            this.radLabel27.Location = new System.Drawing.Point(270, 313);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(42, 18);
            this.radLabel27.TabIndex = 32;
            this.radLabel27.Text = "街道：";
            // 
            // ddlstreet
            // 
            this.ddlstreet.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlstreet.Location = new System.Drawing.Point(319, 311);
            this.ddlstreet.Name = "ddlstreet";
            this.ddlstreet.Size = new System.Drawing.Size(107, 20);
            this.ddlstreet.TabIndex = 31;
            // 
            // radLabel26
            // 
            this.radLabel26.Location = new System.Drawing.Point(77, 313);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(30, 18);
            this.radLabel26.TabIndex = 30;
            this.radLabel26.Text = "区：";
            // 
            // ddlarea
            // 
            this.ddlarea.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlarea.Location = new System.Drawing.Point(114, 311);
            this.ddlarea.Name = "ddlarea";
            this.ddlarea.Size = new System.Drawing.Size(106, 20);
            this.ddlarea.TabIndex = 29;
            // 
            // radLabel25
            // 
            this.radLabel25.Location = new System.Drawing.Point(282, 289);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(30, 18);
            this.radLabel25.TabIndex = 28;
            this.radLabel25.Text = "市：";
            // 
            // radLabel24
            // 
            this.radLabel24.Location = new System.Drawing.Point(77, 289);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(30, 18);
            this.radLabel24.TabIndex = 27;
            this.radLabel24.Text = "省：";
            // 
            // ddlcity
            // 
            this.ddlcity.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlcity.Location = new System.Drawing.Point(318, 287);
            this.ddlcity.Name = "ddlcity";
            this.ddlcity.Size = new System.Drawing.Size(108, 20);
            this.ddlcity.TabIndex = 26;
            // 
            // ddlprovince
            // 
            this.ddlprovince.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlprovince.Location = new System.Drawing.Point(114, 287);
            this.ddlprovince.Name = "ddlprovince";
            this.ddlprovince.Size = new System.Drawing.Size(106, 20);
            this.ddlprovince.TabIndex = 25;
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(42, 263);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(65, 18);
            this.radLabel23.TabIndex = 24;
            this.radLabel23.Text = "火星经度：";
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(247, 261);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(65, 18);
            this.radLabel22.TabIndex = 23;
            this.radLabel22.Text = "火星纬度：";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(247, 237);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(65, 18);
            this.radLabel10.TabIndex = 23;
            this.radLabel10.Text = "百度纬度：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(43, 235);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 22;
            this.radLabel2.Text = "百度经度：";
            // 
            // tbxmarslat
            // 
            this.tbxmarslat.Location = new System.Drawing.Point(318, 261);
            this.tbxmarslat.Name = "tbxmarslat";
            this.tbxmarslat.Size = new System.Drawing.Size(108, 20);
            this.tbxmarslat.TabIndex = 20;
            // 
            // tbxmarslon
            // 
            this.tbxmarslon.Location = new System.Drawing.Point(114, 261);
            this.tbxmarslon.Name = "tbxmarslon";
            this.tbxmarslon.Size = new System.Drawing.Size(106, 20);
            this.tbxmarslon.TabIndex = 21;
            // 
            // tbxbaidulat
            // 
            this.tbxbaidulat.Location = new System.Drawing.Point(318, 235);
            this.tbxbaidulat.Name = "tbxbaidulat";
            this.tbxbaidulat.Size = new System.Drawing.Size(108, 20);
            this.tbxbaidulat.TabIndex = 20;
            // 
            // tbxbaidulon
            // 
            this.tbxbaidulon.Location = new System.Drawing.Point(114, 235);
            this.tbxbaidulon.Name = "tbxbaidulon";
            this.tbxbaidulon.Size = new System.Drawing.Size(106, 20);
            this.tbxbaidulon.TabIndex = 19;
            // 
            // tbxEndTime
            // 
            this.tbxEndTime.Location = new System.Drawing.Point(527, 49);
            this.tbxEndTime.MaxLength = 10;
            this.tbxEndTime.Name = "tbxEndTime";
            this.tbxEndTime.Size = new System.Drawing.Size(105, 20);
            this.tbxEndTime.TabIndex = 18;
            // 
            // tbxStartTime
            // 
            this.tbxStartTime.Location = new System.Drawing.Point(318, 47);
            this.tbxStartTime.MaxLength = 10;
            this.tbxStartTime.Name = "tbxStartTime";
            this.tbxStartTime.Size = new System.Drawing.Size(108, 20);
            this.tbxStartTime.TabIndex = 2;
            // 
            // cbxStore_issms
            // 
            this.cbxStore_issms.Location = new System.Drawing.Point(567, 185);
            this.cbxStore_issms.Name = "cbxStore_issms";
            this.cbxStore_issms.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_issms.TabIndex = 5;
            this.cbxStore_issms.Text = "是否短信";
            // 
            // cbxStore_is_pos
            // 
            this.cbxStore_is_pos.Location = new System.Drawing.Point(482, 185);
            this.cbxStore_is_pos.Name = "cbxStore_is_pos";
            this.cbxStore_is_pos.Size = new System.Drawing.Size(86, 18);
            this.cbxStore_is_pos.TabIndex = 4;
            this.cbxStore_is_pos.Text = "是否启用pos";
            // 
            // cmbState
            // 
            this.cmbState.AutoCompleteDisplayMember = "LocalizedName.Value";
            this.cmbState.AutoCompleteValueMember = "Value";
            this.cmbState.DisplayMember = "LocalizedName.Value";
            this.cmbState.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cmbState.Enabled = false;
            this.cmbState.EnumModelID = "YJH.StoreState";
            this.cmbState.Location = new System.Drawing.Point(526, 21);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(106, 20);
            this.cmbState.TabIndex = 17;
            this.cmbState.Text = "enumPicker1";
            this.cmbState.ValueMember = "Value";
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(479, 23);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(42, 18);
            this.radLabel21.TabIndex = 14;
            this.radLabel21.Text = "状态：";
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(456, 211);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(65, 18);
            this.radLabel20.TabIndex = 14;
            this.radLabel20.Text = "审核时间：";
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(258, 211);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(54, 18);
            this.radLabel19.TabIndex = 14;
            this.radLabel19.Text = "审核人：";
            // 
            // tbxAuditDate
            // 
            this.tbxAuditDate.Location = new System.Drawing.Point(526, 211);
            this.tbxAuditDate.Name = "tbxAuditDate";
            this.tbxAuditDate.ReadOnly = true;
            this.tbxAuditDate.Size = new System.Drawing.Size(106, 20);
            this.tbxAuditDate.TabIndex = 13;
            // 
            // tbxAuditBy
            // 
            this.tbxAuditBy.Location = new System.Drawing.Point(318, 211);
            this.tbxAuditBy.Name = "tbxAuditBy";
            this.tbxAuditBy.ReadOnly = true;
            this.tbxAuditBy.Size = new System.Drawing.Size(108, 20);
            this.tbxAuditBy.TabIndex = 13;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(456, 49);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(65, 18);
            this.radLabel18.TabIndex = 15;
            this.radLabel18.Text = "下班时间：";
            // 
            // radLabel17
            // 
            this.radLabel17.Location = new System.Drawing.Point(247, 45);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(65, 18);
            this.radLabel17.TabIndex = 13;
            this.radLabel17.Text = "上班时间：";
            // 
            // tbxStore_employeecount
            // 
            this.tbxStore_employeecount.Location = new System.Drawing.Point(114, 209);
            this.tbxStore_employeecount.Mask = "d4";
            this.tbxStore_employeecount.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxStore_employeecount.Name = "tbxStore_employeecount";
            this.tbxStore_employeecount.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_employeecount.TabIndex = 13;
            this.tbxStore_employeecount.TabStop = false;
            this.tbxStore_employeecount.Text = "0000";
            this.tbxStore_employeecount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(42, 211);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(65, 18);
            this.radLabel16.TabIndex = 12;
            this.radLabel16.Text = "人员编制：";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(19, 45);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(89, 18);
            this.radLabel15.TabIndex = 12;
            this.radLabel15.Text = "营业执照编号：";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(456, 100);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(65, 18);
            this.radLabel14.TabIndex = 12;
            this.radLabel14.Text = "门店邮编：";
            // 
            // tbxStore_buslicensecode
            // 
            this.tbxStore_buslicensecode.Location = new System.Drawing.Point(114, 45);
            this.tbxStore_buslicensecode.Name = "tbxStore_buslicensecode";
            this.tbxStore_buslicensecode.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_buslicensecode.TabIndex = 11;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(43, 100);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(65, 18);
            this.radLabel13.TabIndex = 12;
            this.radLabel13.Text = "门店地址：";
            // 
            // tbxStore_zipcode
            // 
            this.tbxStore_zipcode.Location = new System.Drawing.Point(526, 100);
            this.tbxStore_zipcode.Name = "tbxStore_zipcode";
            this.tbxStore_zipcode.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_zipcode.TabIndex = 11;
            // 
            // tbxStore_fixedtel
            // 
            this.tbxStore_fixedtel.Location = new System.Drawing.Point(318, 74);
            this.tbxStore_fixedtel.Mask = "d";
            this.tbxStore_fixedtel.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxStore_fixedtel.Name = "tbxStore_fixedtel";
            this.tbxStore_fixedtel.Size = new System.Drawing.Size(108, 20);
            this.tbxStore_fixedtel.TabIndex = 12;
            this.tbxStore_fixedtel.TabStop = false;
            this.tbxStore_fixedtel.Text = "0";
            this.tbxStore_fixedtel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxStore_address
            // 
            this.tbxStore_address.Location = new System.Drawing.Point(114, 100);
            this.tbxStore_address.Name = "tbxStore_address";
            this.tbxStore_address.Size = new System.Drawing.Size(312, 20);
            this.tbxStore_address.TabIndex = 11;
            // 
            // tbxStore_tel
            // 
            this.tbxStore_tel.Location = new System.Drawing.Point(114, 72);
            this.tbxStore_tel.Mask = "d";
            this.tbxStore_tel.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxStore_tel.Name = "tbxStore_tel";
            this.tbxStore_tel.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_tel.TabIndex = 11;
            this.tbxStore_tel.TabStop = false;
            this.tbxStore_tel.Text = "0";
            this.tbxStore_tel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(223, 74);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(89, 18);
            this.radLabel12.TabIndex = 10;
            this.radLabel12.Text = "门店固定电话：";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(43, 72);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 10;
            this.radLabel11.Text = "门店电话：";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(432, 159);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(89, 18);
            this.radLabel9.TabIndex = 8;
            this.radLabel9.Text = "上级业务组织：";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(247, 159);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "上级组织：";
            // 
            // tbxStore_business_org
            // 
            this.tbxStore_business_org.Location = new System.Drawing.Point(526, 159);
            this.tbxStore_business_org.MaxLength = 2;
            this.tbxStore_business_org.Name = "tbxStore_business_org";
            this.tbxStore_business_org.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_business_org.TabIndex = 7;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(54, 159);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(54, 18);
            this.radLabel7.TabIndex = 8;
            this.radLabel7.Text = "价格组：";
            // 
            // tbxStore_parent_org
            // 
            this.tbxStore_parent_org.Location = new System.Drawing.Point(318, 159);
            this.tbxStore_parent_org.MaxLength = 2;
            this.tbxStore_parent_org.Name = "tbxStore_parent_org";
            this.tbxStore_parent_org.Size = new System.Drawing.Size(108, 20);
            this.tbxStore_parent_org.TabIndex = 7;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(444, 128);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(77, 18);
            this.radLabel6.TabIndex = 8;
            this.radLabel6.Text = "增值服务组：";
            // 
            // tbxStore_group_price
            // 
            this.tbxStore_group_price.Location = new System.Drawing.Point(114, 159);
            this.tbxStore_group_price.MaxLength = 2;
            this.tbxStore_group_price.Name = "tbxStore_group_price";
            this.tbxStore_group_price.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_group_price.TabIndex = 7;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(258, 128);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(54, 18);
            this.radLabel5.TabIndex = 6;
            this.radLabel5.Text = "快递组：";
            // 
            // tbxStore_group_servies
            // 
            this.tbxStore_group_servies.Location = new System.Drawing.Point(526, 128);
            this.tbxStore_group_servies.MaxLength = 2;
            this.tbxStore_group_servies.Name = "tbxStore_group_servies";
            this.tbxStore_group_servies.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_group_servies.TabIndex = 7;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(54, 128);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "合同组：";
            // 
            // tbxStore_group_ems
            // 
            this.tbxStore_group_ems.Location = new System.Drawing.Point(318, 129);
            this.tbxStore_group_ems.MaxLength = 2;
            this.tbxStore_group_ems.Name = "tbxStore_group_ems";
            this.tbxStore_group_ems.Size = new System.Drawing.Size(108, 20);
            this.tbxStore_group_ems.TabIndex = 5;
            // 
            // cbxStore_is_stock
            // 
            this.cbxStore_is_stock.Location = new System.Drawing.Point(390, 185);
            this.cbxStore_is_stock.Name = "cbxStore_is_stock";
            this.cbxStore_is_stock.Size = new System.Drawing.Size(91, 18);
            this.cbxStore_is_stock.TabIndex = 3;
            this.cbxStore_is_stock.Text = "是否管理库存";
            // 
            // cbxStore_is_inventory
            // 
            this.cbxStore_is_inventory.Location = new System.Drawing.Point(320, 185);
            this.cbxStore_is_inventory.Name = "cbxStore_is_inventory";
            this.cbxStore_is_inventory.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_is_inventory.TabIndex = 3;
            this.cbxStore_is_inventory.Text = "是否盘点";
            // 
            // cbxStore_is_income
            // 
            this.cbxStore_is_income.Location = new System.Drawing.Point(253, 185);
            this.cbxStore_is_income.Name = "cbxStore_is_income";
            this.cbxStore_is_income.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_is_income.TabIndex = 3;
            this.cbxStore_is_income.Text = "是否要货";
            // 
            // cbxStore_is_return
            // 
            this.cbxStore_is_return.Location = new System.Drawing.Point(186, 185);
            this.cbxStore_is_return.Name = "cbxStore_is_return";
            this.cbxStore_is_return.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_is_return.TabIndex = 3;
            this.cbxStore_is_return.Text = "是否退仓";
            // 
            // cbxStore_is_distribution
            // 
            this.cbxStore_is_distribution.Location = new System.Drawing.Point(118, 185);
            this.cbxStore_is_distribution.Name = "cbxStore_is_distribution";
            this.cbxStore_is_distribution.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_is_distribution.TabIndex = 3;
            this.cbxStore_is_distribution.Text = "是否配送";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(456, 76);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "店铺类型：";
            // 
            // tbxStore_type
            // 
            this.tbxStore_type.Location = new System.Drawing.Point(526, 74);
            this.tbxStore_type.Mask = "d2";
            this.tbxStore_type.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxStore_type.Name = "tbxStore_type";
            this.tbxStore_type.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_type.TabIndex = 3;
            this.tbxStore_type.TabStop = false;
            this.tbxStore_type.Text = "00";
            this.tbxStore_type.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxStore_name
            // 
            this.tbxStore_name.Location = new System.Drawing.Point(318, 21);
            this.tbxStore_name.Name = "tbxStore_name";
            this.tbxStore_name.Size = new System.Drawing.Size(108, 20);
            this.tbxStore_name.TabIndex = 1;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(270, 21);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(42, 18);
            this.radLabel30.TabIndex = 2;
            this.radLabel30.Text = "店名：";
            // 
            // cbxStore_is_order
            // 
            this.cbxStore_is_order.Location = new System.Drawing.Point(53, 185);
            this.cbxStore_is_order.Name = "cbxStore_is_order";
            this.cbxStore_is_order.Size = new System.Drawing.Size(68, 18);
            this.cbxStore_is_order.TabIndex = 2;
            this.cbxStore_is_order.Text = "是否采购";
            // 
            // tbxStore_group_contract
            // 
            this.tbxStore_group_contract.Location = new System.Drawing.Point(114, 128);
            this.tbxStore_group_contract.MaxLength = 2;
            this.tbxStore_group_contract.Name = "tbxStore_group_contract";
            this.tbxStore_group_contract.Size = new System.Drawing.Size(106, 20);
            this.tbxStore_group_contract.TabIndex = 1;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(676, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // UI_StoreMngView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(676, 414);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_StoreMngView";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "门店信息";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlstreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlarea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlcity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlprovince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxmarslat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxmarslon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbaidulat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbaidulon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_issms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_pos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAuditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAuditBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_employeecount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_buslicensecode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_zipcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_fixedtel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_address)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_business_org)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_parent_org)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_servies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_ems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_stock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_inventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_income)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_return)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_distribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxStore_is_order)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxStore_group_contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox tbxStore_id;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxStore_group_contract;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_order;
        private Controls.EnumPicker cmbState;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadTextBox tbxAuditDate;
        private Telerik.WinControls.UI.RadTextBox tbxAuditBy;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxStore_employeecount;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadTextBox tbxStore_buslicensecode;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox tbxStore_zipcode;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxStore_fixedtel;
        private Telerik.WinControls.UI.RadTextBox tbxStore_address;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxStore_tel;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox tbxStore_business_org;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox tbxStore_parent_org;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox tbxStore_group_price;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox tbxStore_group_servies;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxStore_group_ems;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_stock;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_inventory;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_income;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_return;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_distribution;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxStore_type;
        private Telerik.WinControls.UI.RadTextBox tbxStore_name;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_is_pos;
        private Telerik.WinControls.UI.RadCheckBox cbxStore_issms;
        private Telerik.WinControls.UI.RadTextBox tbxEndTime;
        private Telerik.WinControls.UI.RadTextBox tbxStartTime;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox tbxmarslat;
        private Telerik.WinControls.UI.RadTextBox tbxmarslon;
        private Telerik.WinControls.UI.RadTextBox tbxbaidulat;
        private Telerik.WinControls.UI.RadTextBox tbxbaidulon;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadDropDownList ddlstreet;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadDropDownList ddlarea;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadDropDownList ddlcity;
        private Telerik.WinControls.UI.RadDropDownList ddlprovince;
    }
}