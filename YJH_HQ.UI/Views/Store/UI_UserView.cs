﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_UserView : Telerik.WinControls.UI.RadForm
    {
        public int state = 10;//10:新建 20:编辑 30:删除 40:绑定
        public Guid userid = Guid.Empty;
        private YJH.Entities.user_store entity = null;
        public UI_UserView()
        {
            InitializeComponent();
            
            this.btSave.Click += btSave_Click;
            this.Load += UI_UserView_Load;
        }

        void UI_UserView_Load(object sender, EventArgs e)
        {
            //RadListDataItem item = new RadListDataItem();
            //item.Text = "店长";
            //item.Value = 10;
            //ddljob.Items.Add(item);
            //RadListDataItem item1 = new RadListDataItem();
            //item1.Text = "店员";
            //item1.Value = 20;
            //ddljob.Items.Add(item1);
            //RadListDataItem item2 = new RadListDataItem();
            //item2.Text = "社服";
            //item2.Value = 30;
            //ddljob.Items.Add(item2);
            //ddljob.SelectedIndex = 0;
            //this.cbxisenabled.Checked = true;

            if (state == 20)
            {
                entity = YJH.Services.StoreMngService.GetUserByID(userid);
                tbxid.Text = entity.user_id;
                tbxname.Text = entity.user_name;
                /*if(entity.user_job == 10)
                    ddljob.SelectedIndex = 0;
                else if(entity.user_job == 20)
                    ddljob.SelectedIndex = 1;
                else if (entity.user_job == 30)
                    ddljob.SelectedIndex = 2;
                else if (entity.user_job == 40)
                    ddljob.SelectedIndex = 3;
                if(entity.user_status == 10)
                    this.cbxisenabled.Checked = true;
                else
                    this.cbxisenabled.Checked = false;*/
            }
            else if(state == 10)
            {
                tbxid.Text = "";
                tbxname.Text = "";
                //ddljob.SelectedIndex = 0;
                //this.cbxisenabled.Checked = true;
            }
        }

        void btSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxid.Text.Trim())){
                RadMessageBox.Show("请填写用户编号", "提示");
                return;
            }
            if (string.IsNullOrWhiteSpace(tbxname.Text.Trim()))
            {
                RadMessageBox.Show("请填写姓名", "提示");
                return;
            }
            try
            {
                if (state == 10)
                {
                    entity = new YJH.Entities.user_store();
                    entity.user_id = tbxid.Text.Trim();
                    entity.user_name = tbxname.Text.Trim();
                    entity.user_pass = "123456";
                    entity.user_job = 10;
                    entity.user_status = 10;
                    /*if (cbxisenabled.Checked == true)
                        entity.user_status = 10;
                    else
                        entity.user_status = 20;*/
                    YJH.Services.StoreMngService.SaveUser(entity);

                    RadMessageBox.Show("保存成功!", "提示");
                }
                else if (state == 20)
                {
                    entity.user_id = tbxid.Text.Trim();
                    entity.user_name = tbxname.Text.Trim();
                    entity.user_job = 10;
                    entity.user_status = 10;
                    /*if (cbxisenabled.Checked == true)
                        entity.user_status = 10;
                    else
                        entity.user_status = 20;*/
                    YJH.Services.StoreMngService.EditUser(entity);

                    RadMessageBox.Show("修改成功!", "提示");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("执行失败!" + ex.Message, "提示");
            }
            
        }
    }
}
