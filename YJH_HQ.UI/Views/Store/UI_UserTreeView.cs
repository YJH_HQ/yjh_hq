﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH_HQ.UI.Properties;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_UserTreeView : Telerik.WinControls.UI.RadForm
    {
        public string sid = "";
        public UI_UserTreeView()
        {
            InitializeComponent();
            loadTree();
            this.btnCancel.Click += btnCancel_Click;
            this.btnConfirm.Click += btnConfirm_Click;
            RadListDataItem item = new RadListDataItem();
            item.Text = "店长";
            item.Value = Guid.Parse("175B09FE-3749-4550-95A7-87950FF078A5");
            ddljob.Items.Add(item);
            RadListDataItem item1 = new RadListDataItem();
            item1.Text = "储备店长";
            item1.Value = Guid.Parse("9370A75F-CF97-4156-8138-0789AE9224F2");
            ddljob.Items.Add(item1);
            RadListDataItem item2 = new RadListDataItem();
            item2.Text = "实习店长";
            item2.Value = Guid.Parse("489FC822-BD0B-4642-BA30-2413AA8872E3");
            ddljob.Items.Add(item2);
            RadListDataItem item3 = new RadListDataItem();
            item3.Text = "店员";
            item3.Value = Guid.Parse("535167AA-FFE3-4CB1-B34F-8F8BFEEF3FF5");
            ddljob.Items.Add(item3);
            RadListDataItem item4 = new RadListDataItem();
            item4.Text = "社区服务员";
            item4.Value = Guid.Parse("9C618AE1-BCE0-4699-BE55-435047861829");
            ddljob.Items.Add(item4);
            RadListDataItem item5 = new RadListDataItem();
            item5.Text = "兼职快递员";
            item5.Value = Guid.Parse("D049D391-A522-4CC8-9D7B-A3805BFBEEAE");
            ddljob.Items.Add(item5);
            ddljob.SelectedIndex = 0;
            this.cbxisenabled.Checked = true;
            this.ddljob.SelectedValueChanged += ddljob_SelectedValueChanged;
        }

        void ddljob_SelectedValueChanged(object sender, EventArgs e)
        {
            if(Guid.Parse(ddljob.SelectedValue.ToString()) != Guid.Parse("175B09FE-3749-4550-95A7-87950FF078A5"))
            {
                cbxisenabled.Checked = true;
                cbxisenabled.Enabled = false;
            }
            else
                cbxisenabled.Enabled = true;
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            if(UserTree.SelectedNodes.Count > 0)
            {
                if(UserTree.SelectedNodes[0].Parent == null)
                    RadMessageBox.Show("请先选择人员后操作", "提示");
                else
                {
                    if (Guid.Parse(ddljob.SelectedValue.ToString()) != Guid.Parse("175B09FE-3749-4550-95A7-87950FF078A5"))
                    {
                        if (YJH.Services.StoreMngService.IsExitEmp(Guid.Parse(UserTree.SelectedNodes[0].Tag.ToString())))
                        {
                            RadMessageBox.Show("该人员已经绑定过门店，请勿重复添加，绑定门店：" + YJH.Services.StoreMngService.GetStoreByEmp(Guid.Parse(UserTree.SelectedNodes[0].Tag.ToString())), "提示");
                            return;
                        }
                    }
                    else
                    {
                        if (YJH.Services.StoreMngService.IsHasDZ(sid, Guid.Parse(UserTree.SelectedNodes[0].Tag.ToString())))
                        {
                            RadMessageBox.Show("该门店已经绑定过店长" +
                            "或该店长在其他门店已存在非店长职务，请先删除后添加", "提示");
                            return;
                        }
                    }
                    bool isexit = YJH.Services.StoreMngService.IsExitUser(Guid.Parse(UserTree.SelectedNodes[0].Tag.ToString()), sid);
                    if (!isexit)
                    {
                        try
                        {
                            YJH.Services.StoreMngService.BindUser(Guid.Parse(UserTree.SelectedNodes[0].Tag.ToString()), sid,Guid.Parse(ddljob.SelectedItem.Value.ToString()),cbxisenabled.Checked);
                            RadMessageBox.Show("绑定成功", "提示");
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            RadMessageBox.Show("绑定失败" + ex.Message, "提示");
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("该店铺下已存在该人员，请勿重复添加", "提示");
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择人员后操作", "提示");
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadTree()
        {
            /*List<YJH.Entities.ba_store> storelist = YJH.Services.StoreMngService.GetStore();
            foreach (YJH.Entities.ba_store store in storelist)
            {
                RadTreeNode node = new RadTreeNode();
                node.Text = store.store_id + " " + store.store_name;
                node.Tag = store.Instance.ID;
                node.Image = Resources.Home16;
                this.UserTree.Nodes.Add(node);
                DataTable dt = YJH.Services.StoreMngService.GetUser(store.store_id);
                foreach (DataRow dr in dt.Rows)
                {
                    RadTreeNode node1 = new RadTreeNode();
                    node1.Text = dr["user_name"].ToString();
                    node1.Tag = dr["ID"];
                    node1.Image = Resources.User16;
                    node.Nodes.Add(node1);
                }
            }*/
            RadTreeNode node2 = new RadTreeNode();
            node2.Text = "待分配人员";
            node2.Image = Resources.Home16;
            this.UserTree.Nodes.Add(node2);
            DataTable dt = YJH.Services.StoreMngService.WaitingUser();
            foreach (DataRow dr in dt.Rows)
            {
                RadTreeNode node1 = new RadTreeNode();
                node1.Text = dr["Number"].ToString() + "  " + dr["Name"].ToString();
                node1.Tag = dr["ID"];
                node1.Image = Resources.User16;
                node2.Nodes.Add(node1);
            }
            

        }
    }
}
