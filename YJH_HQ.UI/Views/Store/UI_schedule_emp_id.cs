﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH_HQ.UI.Views.Systems.OrgUnitManager;

namespace YJH_HQ.UI.Views.Store
{
    public partial class UI_schedule_emp_id : Telerik.WinControls.UI.RadForm
    {
        public string store_id = "";
        public string process_id = "";
        public UI_schedule_emp_id()
        {
            InitializeComponent();
            this.Load += UI_schedule_emp_id_Load;
            this.orgTree.NodeExpandedChanging += orgTree_NodeExpandedChanging;
            this.orgTree.NodeMouseDoubleClick += orgTree_NodeMouseDoubleClick;
        }

        void orgTree_NodeMouseDoubleClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            if(e.Node.Nodes.Count > 0)
            {
                RadMessageBox.Show("请选择员工", "提示");
                return;
            }
            else
            {
                try
                {
                    YJH.Services.StoreMngService.SaveProcessemp(store_id, process_id, e.Node.Text);
                    RadMessageBox.Show("保存成功", "提示");
                    this.Close();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show("保存失败", "提示");
                }
                
            }
        }

        private string OrganizationType(string type)
        {
            if (type == sys.Enums.OrganizationType.业务组织机构.ToString())
                return "IsBusiness";
            if (type == sys.Enums.OrganizationType.行政组织机构.ToString())
                return "IsAdmin";
            if (type == sys.Enums.OrganizationType.采购组织机构.ToString())
                return "IsPurchase";
            if (type == sys.Enums.OrganizationType.物流组织机构.ToString())
                return "IsDistribution";
            if (type == sys.Enums.OrganizationType.财务组织机构.ToString())
                return "IsFinance";

            throw new Exception("组织类型异常");
        }
        private OrgUnitTreeNode AppendNode(sys.Entities.OrgUnit ou, Guid id)
        {
            var treeNode = new OrgUnitTreeNode(ou.BaseType, ou, ou.Name.ToString());

            //添加临时节点，这里的临时节点Text为null，Tag为父节点的ID
            Telerik.WinControls.UI.RadTreeNode tempNode = new Telerik.WinControls.UI.RadTreeNode(null);
            tempNode.Tag = ou.Instance.ID;
            //员工节点是最后一级不需要添加临时节点
            if (ou.BaseType != OrgUnitTreeNode.EMPLOEE)
                treeNode.Nodes.Add(tempNode);

            if (id == Guid.Empty)
                this.orgTree.Nodes.Add(treeNode);
            else
            {
                //根据id查找父节点，并向其添加子节点
                var preNode = this.orgTree.FindNodes(n => n.Tag != null && n.Text != null && ((OrgUnitTreeNode)n).EntityBase.Instance.ID == id);
                if (preNode != null && preNode.Count() > 0)
                    preNode[0].Nodes.Add(treeNode);
            }
            return treeNode;
        }
        private void BindOUTree(Guid id)
        {
            IList<sys.Entities.OrgUnit> source = null;
                source = sys.Services.OrgUnitService.GetOrganization(id, OrganizationType(Globle.CurrentOrganizationType.ToString()));

            if (source == null || source.Count == 0) return;
            foreach (sys.Entities.OrgUnit ou in source)
                AppendNode(ou, id);
        }

        void orgTree_NodeExpandedChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            //默认false为打开状态
            if (!e.Node.Expanded)
            {
                //查找当前节点下的临时节点，有的话直接删除
                var nodes = e.Node.FindNodes(n => n.Text == null && (Guid)n.Tag == ((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
                for (int i = 0; i < nodes.Count(); i++)
                    nodes[0].Remove();

                //子节点数为0并且有临时节点和当前节点不为员工，根据这个条件组合判断出当前节点是否需要重新加载子节点
                if (e.Node.Nodes.Count() <= 0 && nodes.Count() > 0 && ((OrgUnitTreeNode)e.Node).Type != OrgUnitTreeNode.EMPLOEE)
                    BindOUTree(((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
            }
        }

        void UI_schedule_emp_id_Load(object sender, EventArgs e)
        {
            YJH.Entities.bm_schedule_store entity =  YJH.Services.StoreMngService.GetscheduleByspid(store_id, process_id);
            tbxname.Text = entity.emp_id;
            //该用户不是管理员权限则只能看到当前公司的信息，首先绑定的是当前登录人所登录的公司
            var unitOU = Globle.CurrentBusinessUnitOU;
            var treeNode = new OrgUnitTreeNode(unitOU.BaseType, unitOU, unitOU.Name.ToString());
            //添加临时节点，这里的临时节点Text为null，Tag为父节点的ID
            Telerik.WinControls.UI.RadTreeNode tempNode = new Telerik.WinControls.UI.RadTreeNode(null);
            tempNode.Tag = unitOU.Instance.ID;
            //员工节点是最后一级不需要添加临时节点
            if (unitOU.BaseType != OrgUnitTreeNode.EMPLOEE)
                treeNode.Nodes.Add(tempNode);

            orgTree.Nodes.Add(treeNode);
        }
    }
}
