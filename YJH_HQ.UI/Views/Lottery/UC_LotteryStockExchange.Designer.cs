﻿namespace YJH_HQ.UI.Views.Lottery
{
    partial class UC_LotteryStockExchange
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn11 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn12 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn21 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn22 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn23 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn24 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnModify = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btnAdudit = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSend = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.dtEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.labelCount = new Telerik.WinControls.UI.RadLabel();
            this.rpMain = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.dgMain = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMain)).BeginInit();
            this.rpMain.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMain.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1029, 55);
            this.radCommandBar1.TabIndex = 4;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnModify,
            this.btnDelete,
            this.btnAdudit,
            this.btnSend});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新建";
            this.btnNew.AccessibleName = "新建";
            this.btnNew.DisplayName = "commandBarButton1";
            this.btnNew.DrawText = true;
            this.btnNew.Image = null;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnModify
            // 
            this.btnModify.AccessibleDescription = "打开";
            this.btnModify.AccessibleName = "打开";
            this.btnModify.DisplayName = "commandBarButton1";
            this.btnModify.DrawText = true;
            this.btnModify.Image = null;
            this.btnModify.Name = "btnModify";
            this.btnModify.Text = "修改";
            this.btnModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModify.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton2";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = null;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnAdudit
            // 
            this.btnAdudit.AccessibleDescription = "审核";
            this.btnAdudit.AccessibleName = "审核";
            this.btnAdudit.DisplayName = "commandBarButton2";
            this.btnAdudit.DrawText = true;
            this.btnAdudit.Image = null;
            this.btnAdudit.Name = "btnAdudit";
            this.btnAdudit.Text = "审核";
            this.btnAdudit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSend
            // 
            this.btnSend.AccessibleDescription = "发送";
            this.btnSend.AccessibleName = "发送";
            this.btnSend.DisplayName = "commandBarButton3";
            this.btnSend.DrawText = true;
            this.btnSend.Image = null;
            this.btnSend.Name = "btnSend";
            this.btnSend.Text = "发送";
            this.btnSend.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.dtEndDate);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.dtStartDate);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 55);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1029, 56);
            this.radGroupBox1.TabIndex = 6;
            this.radGroupBox1.Text = "查询条件";
            // 
            // dtEndDate
            // 
            this.dtEndDate.AutoSize = false;
            this.dtEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDate.Location = new System.Drawing.Point(427, 26);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(120, 20);
            this.dtEndDate.TabIndex = 44;
            this.dtEndDate.TabStop = false;
            this.dtEndDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(403, 26);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(18, 18);
            this.radLabel2.TabIndex = 43;
            this.radLabel2.Text = "至";
            // 
            // dtStartDate
            // 
            this.dtStartDate.AutoSize = false;
            this.dtStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDate.Location = new System.Drawing.Point(279, 26);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Size = new System.Drawing.Size(120, 20);
            this.dtStartDate.TabIndex = 42;
            this.dtStartDate.TabStop = false;
            this.dtStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(201, 26);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(75, 18);
            this.radLabel1.TabIndex = 41;
            this.radLabel1.Text = "查询日期段：";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(59, 26);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(125, 20);
            this.ddtStore.TabIndex = 40;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "门店：";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnSearch.Location = new System.Drawing.Point(617, 26);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSearch.Size = new System.Drawing.Size(86, 20);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "查询";
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = false;
            this.labelCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCount.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelCount.Location = new System.Drawing.Point(0, 503);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(1029, 25);
            this.labelCount.TabIndex = 7;
            this.labelCount.Text = "合计：";
            this.labelCount.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rpMain
            // 
            this.rpMain.BackColor = System.Drawing.SystemColors.Window;
            this.rpMain.Controls.Add(this.radPageViewPage1);
            this.rpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpMain.Location = new System.Drawing.Point(0, 111);
            this.rpMain.Name = "rpMain";
            this.rpMain.SelectedPage = this.radPageViewPage1;
            this.rpMain.Size = new System.Drawing.Size(1029, 392);
            this.rpMain.TabIndex = 8;
            this.rpMain.Text = "radPageView1";
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.dgMain);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(40F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1008, 344);
            this.radPageViewPage1.Text = "列表";
            // 
            // dgMain
            // 
            this.dgMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(240)))), ((int)(((byte)(249)))));
            this.dgMain.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgMain.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.dgMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgMain.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgMain.Location = new System.Drawing.Point(0, 0);
            // 
            // dgMain
            // 
            this.dgMain.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dgMain.MasterTemplate.AllowAddNewRow = false;
            this.dgMain.MasterTemplate.AllowDeleteRow = false;
            this.dgMain.MasterTemplate.AllowEditRow = false;
            this.dgMain.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn46.EnableExpressionEditor = false;
            gridViewTextBoxColumn46.FieldName = "No";
            gridViewTextBoxColumn46.HeaderText = "序号";
            gridViewTextBoxColumn46.Name = "col_No";
            gridViewTextBoxColumn46.ReadOnly = true;
            gridViewTextBoxColumn46.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn6.EnableExpressionEditor = false;
            gridViewComboBoxColumn6.FieldName = "leh_store_id";
            gridViewComboBoxColumn6.HeaderText = "门店号";
            gridViewComboBoxColumn6.Name = "col_leh_store_id";
            gridViewComboBoxColumn6.ReadOnly = true;
            gridViewComboBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn6.Width = 70;
            gridViewTextBoxColumn47.EnableExpressionEditor = false;
            gridViewTextBoxColumn47.FieldName = "ID";
            gridViewTextBoxColumn47.HeaderText = "ID";
            gridViewTextBoxColumn47.IsVisible = false;
            gridViewTextBoxColumn47.Name = "col_id";
            gridViewTextBoxColumn48.EnableExpressionEditor = false;
            gridViewTextBoxColumn48.FieldName = "leh_exchange_id";
            gridViewTextBoxColumn48.HeaderText = "置换单号";
            gridViewTextBoxColumn48.Name = "col_leh_exchange_id";
            gridViewTextBoxColumn48.ReadOnly = true;
            gridViewTextBoxColumn48.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn48.Width = 120;
            gridViewDecimalColumn11.EnableExpressionEditor = false;
            gridViewDecimalColumn11.FieldName = "leh_exchange_num_total";
            gridViewDecimalColumn11.HeaderText = "置换总数";
            gridViewDecimalColumn11.Name = "col_leh_exchange _num_total";
            gridViewDecimalColumn11.Width = 60;
            gridViewDecimalColumn12.EnableExpressionEditor = false;
            gridViewDecimalColumn12.FieldName = "leh_exchange_amt_total";
            gridViewDecimalColumn12.HeaderText = "置换总额";
            gridViewDecimalColumn12.Name = "col_leh_ exchange_amt_total";
            gridViewDecimalColumn12.Width = 60;
            gridViewTextBoxColumn49.EnableExpressionEditor = false;
            gridViewTextBoxColumn49.FieldName = "leh_company";
            gridViewTextBoxColumn49.HeaderText = "所属公司";
            gridViewTextBoxColumn49.Name = "col_leh_company";
            gridViewTextBoxColumn49.Width = 60;
            gridViewTextBoxColumn50.EnableExpressionEditor = false;
            gridViewTextBoxColumn50.FieldName = "leh_status1";
            gridViewTextBoxColumn50.HeaderText = "置换状态";
            gridViewTextBoxColumn50.Name = "col_leh_status";
            gridViewTextBoxColumn50.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn50.Width = 60;
            gridViewTextBoxColumn51.EnableExpressionEditor = false;
            gridViewTextBoxColumn51.FieldName = "creater";
            gridViewTextBoxColumn51.HeaderText = "发单人";
            gridViewTextBoxColumn51.Name = "col_creater";
            gridViewTextBoxColumn51.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn51.Width = 60;
            gridViewDateTimeColumn21.EnableExpressionEditor = false;
            gridViewDateTimeColumn21.FieldName = "create_date";
            gridViewDateTimeColumn21.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn21.FormatString = "{0:yyyy/MM/dd HH:mm}";
            gridViewDateTimeColumn21.HeaderText = "发单日期";
            gridViewDateTimeColumn21.Name = "col_create_date";
            gridViewDateTimeColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn21.Width = 100;
            gridViewTextBoxColumn52.EnableExpressionEditor = false;
            gridViewTextBoxColumn52.FieldName = "aduiter";
            gridViewTextBoxColumn52.HeaderText = "审核人";
            gridViewTextBoxColumn52.Name = "col_aduiter";
            gridViewTextBoxColumn52.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn52.Width = 60;
            gridViewDateTimeColumn22.EnableExpressionEditor = false;
            gridViewDateTimeColumn22.FieldName = "audit_date";
            gridViewDateTimeColumn22.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            gridViewDateTimeColumn22.FormatString = "{0:yyyy/MM/dd HH:mm}";
            gridViewDateTimeColumn22.HeaderText = "审核日期";
            gridViewDateTimeColumn22.Name = "col_audit_date";
            gridViewDateTimeColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn22.Width = 100;
            gridViewTextBoxColumn53.EnableExpressionEditor = false;
            gridViewTextBoxColumn53.FieldName = "effectiver";
            gridViewTextBoxColumn53.HeaderText = "执行人";
            gridViewTextBoxColumn53.Name = "col_effectiver";
            gridViewTextBoxColumn53.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn53.Width = 60;
            gridViewDateTimeColumn23.EnableExpressionEditor = false;
            gridViewDateTimeColumn23.FieldName = "effective_date";
            gridViewDateTimeColumn23.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            gridViewDateTimeColumn23.FormatString = "{0:yyyy/MM/dd HH:mm}";
            gridViewDateTimeColumn23.HeaderText = "生效日期";
            gridViewDateTimeColumn23.Name = "col_effective_date";
            gridViewDateTimeColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn23.Width = 100;
            gridViewTextBoxColumn54.EnableExpressionEditor = false;
            gridViewTextBoxColumn54.FieldName = "send_status1";
            gridViewTextBoxColumn54.HeaderText = "通讯状态";
            gridViewTextBoxColumn54.Name = "col_send_status";
            gridViewTextBoxColumn54.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn54.Width = 60;
            gridViewDateTimeColumn24.EnableExpressionEditor = false;
            gridViewDateTimeColumn24.FieldName = "send_date";
            gridViewDateTimeColumn24.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            gridViewDateTimeColumn24.FormatString = "{0:yyyy/MM/dd HH:mm}";
            gridViewDateTimeColumn24.HeaderText = "通讯日期";
            gridViewDateTimeColumn24.Name = "col_send_date";
            gridViewDateTimeColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn24.Width = 100;
            this.dgMain.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn46,
            gridViewComboBoxColumn6,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48,
            gridViewDecimalColumn11,
            gridViewDecimalColumn12,
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewDateTimeColumn21,
            gridViewTextBoxColumn52,
            gridViewDateTimeColumn22,
            gridViewTextBoxColumn53,
            gridViewDateTimeColumn23,
            gridViewTextBoxColumn54,
            gridViewDateTimeColumn24});
            this.dgMain.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgMain.Name = "dgMain";
            this.dgMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgMain.ShowGroupPanel = false;
            this.dgMain.Size = new System.Drawing.Size(1008, 344);
            this.dgMain.TabIndex = 11;
            this.dgMain.Text = "radGridView1";
            // 
            // UC_LotteryStockExchange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rpMain);
            this.Controls.Add(this.labelCount);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_LotteryStockExchange";
            this.Size = new System.Drawing.Size(1029, 528);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpMain)).EndInit();
            this.rpMain.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMain.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnModify;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private Telerik.WinControls.UI.CommandBarButton btnAdudit;
        private Telerik.WinControls.UI.CommandBarButton btnSend;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDateTimePicker dtEndDate;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker dtStartDate;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Telerik.WinControls.UI.RadLabel labelCount;
        private Telerik.WinControls.UI.RadPageView rpMain;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadGridView dgMain;
    }
}

