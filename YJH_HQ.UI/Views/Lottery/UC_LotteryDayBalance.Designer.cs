﻿namespace YJH_HQ.UI.Views.Lottery
{
    partial class UC_LotteryDayBalance
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.dtDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnBalance = new Telerik.WinControls.UI.RadButton();
            this.rgvBalanceLog = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvBalanceLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvBalanceLog.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.btnBalance);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.dtDate);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(700, 57);
            this.radGroupBox1.TabIndex = 2;
            this.radGroupBox1.Text = "查询条件";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(272, 24);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(125, 20);
            this.ddtStore.TabIndex = 37;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(229, 24);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 36;
            this.radLabel3.Text = "门店：";
            // 
            // dtDate
            // 
            this.dtDate.AutoSize = false;
            this.dtDate.CustomFormat = "yyyy-MM-dd";
            this.dtDate.Enabled = false;
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDate.Location = new System.Drawing.Point(83, 24);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(120, 20);
            this.dtDate.TabIndex = 32;
            this.dtDate.TabStop = false;
            this.dtDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(10, 24);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(63, 18);
            this.radLabel1.TabIndex = 31;
            this.radLabel1.Text = "日结日期：";
            // 
            // btnBalance
            // 
            this.btnBalance.Location = new System.Drawing.Point(430, 24);
            this.btnBalance.Name = "btnBalance";
            this.btnBalance.Size = new System.Drawing.Size(86, 20);
            this.btnBalance.TabIndex = 38;
            this.btnBalance.Text = "结算";
            // 
            // rgvBalanceLog
            // 
            this.rgvBalanceLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgvBalanceLog.Location = new System.Drawing.Point(0, 57);
            // 
            // rgvBalanceLog
            // 
            this.rgvBalanceLog.MasterTemplate.AllowAddNewRow = false;
            this.rgvBalanceLog.MasterTemplate.AllowDeleteRow = false;
            this.rgvBalanceLog.MasterTemplate.AllowEditRow = false;
            this.rgvBalanceLog.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "store";
            gridViewTextBoxColumn1.HeaderText = "门店";
            gridViewTextBoxColumn1.Name = "col_store";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "module";
            gridViewTextBoxColumn2.HeaderText = "发生类型";
            gridViewTextBoxColumn2.Name = "col_Module";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "starttime";
            gridViewTextBoxColumn3.FormatString = "{0:yyyy-MM-dd hh:mm:ss}";
            gridViewTextBoxColumn3.HeaderText = "日结开始时间";
            gridViewTextBoxColumn3.Name = "col_StartTime";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 120;
            gridViewTextBoxColumn4.FieldName = "endtime";
            gridViewTextBoxColumn4.FormatString = "{0:yyyy-MM-dd hh:mm:ss}";
            gridViewTextBoxColumn4.HeaderText = "日结结束时间";
            gridViewTextBoxColumn4.Name = "col_EndTime";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 120;
            gridViewTextBoxColumn5.FieldName = "result";
            gridViewTextBoxColumn5.HeaderText = "发生结果";
            gridViewTextBoxColumn5.Name = "col_Result";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 200;
            this.rgvBalanceLog.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.rgvBalanceLog.Name = "rgvBalanceLog";
            this.rgvBalanceLog.ReadOnly = true;
            this.rgvBalanceLog.ShowGroupPanel = false;
            this.rgvBalanceLog.Size = new System.Drawing.Size(700, 500);
            this.rgvBalanceLog.TabIndex = 3;
            this.rgvBalanceLog.Text = "radGridView1";
            // 
            // UC_LotteryDayBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rgvBalanceLog);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_LotteryDayBalance";
            this.Size = new System.Drawing.Size(700, 557);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvBalanceLog.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvBalanceLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton btnBalance;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker dtDate;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView rgvBalanceLog;
    }
}
