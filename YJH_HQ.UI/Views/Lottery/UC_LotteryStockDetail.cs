﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryStockDetail : UserControl
    {
        UC_LotteryStockExchange lotterySotck;
        //int no = 1;
        DataTable tempdt = null;
        public bool IsEdit = false;
        public string sotreid = "";
        public string exchangeid = "";
        #region ===构造===

        public UC_LotteryStockDetail(UC_LotteryStockExchange lse)
        {
            InitializeComponent();
            lotterySotck = lse;
            this.Load += UC_LotteryStockDetail_Load;
            this.dgDetail.CellValidating += dgDetail_CellValidating;
            this.dgDetail.CellFormatting += dgDetail_CellFormatting;
            this.ddtStore.SelectedIndexChanged += ddtStore_SelectedIndexChanged;
            this.dgDetail.UserDeletedRow += dgDetail_UserDeletedRow;
        }
        #endregion
        void UC_LotteryStockDetail_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.SetColumnLotteryClass();
            this.SetColumnLotteryType();
            SetColumnLotterySeries();
            if(lotterySotck.IsNew == true)
            {
                tempdt = new DataTable();
                tempdt.Columns.Add("ID");
                tempdt.Columns.Add("led_no");
                tempdt.Columns.Add("led_exchange_num", typeof(decimal));
                tempdt.Columns.Add("led_exchange_amt", typeof(decimal));
                tempdt.Columns.Add("lottery_class");
                tempdt.Columns.Add("lottery_series");
                tempdt.Columns.Add("lottery_type");
                DataRow dr = tempdt.NewRow();
                //dr.Cells[1].Value = no;
                //no++;
                dr["led_exchange_num"] = 0.00;
                dr["led_exchange_amt"] = 0.00;
                dr["lottery_class"] = "01";
                dr["lottery_series"] = "0001";
                dr["lottery_type"] = "0010";
                tempdt.Rows.Add(dr);
                dgDetail.DataSource = tempdt;
            }
            else if(IsEdit == false && lotterySotck.IsNew == false)
            {
                this.ddtStore.Enabled = false;
                this.ddtStore.SelectedValue = sotreid;
                this.dgDetail.ReadOnly = true;
                this.lbTop.Text = ddtStore.Text + "彩票置换单";
                this.lbexchangeid.Text = "单号：" + exchangeid;
                DataTable ddt = YJH.Services.LotteryExchangeService.Getlottery_exchange_detail(exchangeid);
                decimal c = 0;
                decimal d = 0;
                foreach (DataRow dr in ddt.Rows)
                {
                    c += decimal.Parse(dr["led_exchange_num"].ToString());
                    d += decimal.Parse(dr["led_exchange_amt"].ToString());
                }
                lbDetailCount.Text = "合计：置换总数 " + c.ToString("f0") + "  置换总额 " + d.ToString();
                this.dgDetail.DataSource = ddt;
                
            }
            else if(IsEdit == true && lotterySotck.IsNew == false)
            {
                this.ddtStore.Enabled = false;
                this.ddtStore.SelectedValue = sotreid;
                this.dgDetail.ReadOnly = false;
                this.lbTop.Text = ddtStore.Text + "彩票置换单";
                this.lbexchangeid.Text = "单号：" + exchangeid;
                DataTable ddt = YJH.Services.LotteryExchangeService.Getlottery_exchange_detail(exchangeid);
                decimal c = 0;
                decimal d = 0;
                foreach (DataRow dr in ddt.Rows)
                {
                    c += decimal.Parse(dr["led_exchange_num"].ToString());
                    d += decimal.Parse(dr["led_exchange_amt"].ToString());
                }
                lbDetailCount.Text = "合计：置换总数 " + c.ToString("f0") + "  置换总额 " + d.ToString();
                this.dgDetail.DataSource = ddt;
            }
            lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
        }
        #region ===事件===
        void dgDetail_UserDeletedRow(object sender, GridViewRowEventArgs e)
        {
           if(dgDetail.Rows.Count == 0)
           {
               lotterySotck.dt = null;
           }
           else
           {
               dgDetail.Refresh();
               lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
           }
        }
        void ddtStore_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            lotterySotck.storeID = ddtStore.SelectedValue.ToString();
        }
        void dgDetail_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            //if(dgDetail.Rows.Count > 0)
            //{
            //    if((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_led_exchange_num" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166); 
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_led_exchange _amt" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247,90,166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166); 
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_lottery_type" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166);
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_lottery_series" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166);
            //    }
            //}
        }
        void dgDetail_CellValidating(object sender, CellValidatingEventArgs e)
        {
            if (dgDetail.Rows.Count > 0)
            {
                if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_led_exchange_num" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请输入置换数量";
                    lotterySotck.dt = null;
                }
                else if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_led_exchange _amt" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请输入置换金额";
                    lotterySotck.dt = null;
                }
                else if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_lottery_class" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请选择彩票种类";
                    lotterySotck.dt = null;
                }
                else if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_lottery_type" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请选择彩票系列";
                    lotterySotck.dt = null;
                }
                else if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_lottery_series" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请选择彩票类型";
                    lotterySotck.dt = null;
                }
                else
                {
                    lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
                }
            }
        }
        #endregion
        #region ===方法===
        /// <summary>
        /// 初始化彩票种类列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryClass()
        {
            var columnClass = this.dgDetail.Columns["col_lottery_class"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryClass)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryClass), ot),
                            ValueMember = ot
                        }).ToList();
            columnClass.DisplayMember = "DisplayMember";
            columnClass.ValueMember = "ValueMember";
            columnClass.DataSource = list;
        }
        /// <summary>
        /// 初始化彩票类型列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryType()
        {
            var columnType = this.dgDetail.Columns["col_lottery_type"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryType)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryType), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }

        /// <summary>
        /// 初始化彩票系列列
        /// </summary>
        /// <remarks>建立人：陆佳伟 建立日期：2015/1/27 最后修改日期：2015/1/27</remarks>
        private void SetColumnLotterySeries()
        {
            var columnType = this.dgDetail.Columns["col_lottery_series"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotterySeries)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotterySeries), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }
        #endregion
    }
}
