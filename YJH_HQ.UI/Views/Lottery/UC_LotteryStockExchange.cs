﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH.Enums;
using YJH.Services;
using YJH.Entities;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryStockExchange : UserControl
    {
        #region ===字段===
        public System.Data.DataTable dt = null;
        public string storeID = null;
        public bool IsNew = false;
        private DataTable MainDt = null;
        private int no = 1;
        #endregion
        #region ===构造===
        public UC_LotteryStockExchange()
        {
            InitializeComponent();
            this.Load += UC_LotteryStockExchange_Load;
            this.btnNew.Click += btnNew_Click;
            this.rpMain.SelectedPageChanged += rpMain_SelectedPageChanged;
            this.btnModify.Click += btnModify_Click;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.dgMain.CellFormatting += dgMain_CellFormatting;
            this.btnAdudit.Click += btnAdudit_Click;
            this.dgMain.DoubleClick += dgMain_DoubleClick;
            this.btnSend.Click += btnSend_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.rpMain.PageRemoving += rpMain_PageRemoving;
        }
        #endregion

        #region ===事件===
        void rpMain_PageRemoving(object sender, RadPageViewCancelEventArgs e)
        {
            if(((Telerik.WinControls.UI.RadPageViewEventArgs)(e)).Page.TabIndex == 0)
            {
                e.Cancel = true;
            }
            if(rpMain.Pages.Count == 2)
            {
                no = 1;
            }
        }
        void dgMain_DoubleClick(object sender, EventArgs e)
        {
            foreach (var item in rpMain.Pages)
            {
                if (item.TabIndex != 0)
                {
                    if (dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString() == item.Tag.ToString())
                    {
                        rpMain.SelectedPage = item;
                        return;
                    }
                }
            }
            RadPageViewPage page = new RadPageViewPage();
            page.Text = "单据" + no;
            no++;
            UC_LotteryStockDetail lsd = new UC_LotteryStockDetail(this);
            if (dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已修改")
                lsd.IsEdit = true;
            else
                lsd.IsEdit = false;
            lsd.sotreid = dgMain.SelectedRows[0].Cells["col_leh_store_id"].Value.ToString();
            lsd.exchangeid = dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString();
            page.Controls.Add(lsd);
            page.Tag = dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString();
            this.rpMain.Pages.Add(page);
            this.rpMain.SelectedPage = page;
        }
        void dgMain_CellFormatting(object sender, CellFormattingEventArgs e)
        {
        }
        void Search()
        {
            MainDt = YJH.Services.LotteryExchangeService.Getlottery_exchange_head(ddtStore.SelectedValue.ToString(), dtStartDate.Value, dtEndDate.Value);
            decimal a = 0;
            decimal b = 0;
            MainDt.Columns.Add("leh_status1");
            MainDt.Columns.Add("send_status1");
            foreach (DataRow dr in MainDt.Rows)
            {
                dr["leh_status1"] = Enum.GetName(typeof(YJH.Enums.leh_status), int.Parse(dr["leh_status"].ToString()));
                dr["send_status1"] = Enum.GetName(typeof(YJH.Enums.send_status), int.Parse(dr["send_status"].ToString()));
                a += decimal.Parse(dr["leh_exchange_num_total"].ToString());
                b += decimal.Parse(dr["leh_exchange_amt_total"].ToString());
            }
            labelCount.Text = "合计：置换总数 " + a.ToString("f0") + "  置换总额 " + b.ToString();
            this.dgMain.DataSource = MainDt;
        }
        void btnNew_Click(object sender, EventArgs e)
        {
            IsNew = true;
            RadPageViewPage page = new RadPageViewPage();
            page.Text = "单据" + no;
            no++;
            UC_LotteryStockDetail lsd = new UC_LotteryStockDetail(this);
            page.Controls.Add(lsd);
            var n = YJH.Services.LotteryExchangeService.Getleh_exchange_id();
            page.Tag = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + (Convert.ToInt32(n) + 1).ToString().PadLeft(3, '0');
            this.rpMain.Pages.Add(page);
            this.rpMain.SelectedPage = page;
            
            /*if(IsNew == false)
            {
                IsNew = true;
                
            }
            else
            {
                RadMessageBox.Show("请先保存新建的置换单后再操作", "提示");
            }*/
        }
        void btnAdudit_Click(object sender, EventArgs e)
        {
            if(dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已修改")
                {
                    if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.LotteryExchangeService.Audit(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()), Globle.CurrentEmployee.Base.Name);
                        RadMessageBox.Show("审核成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要审核的记录", "提示");
            }
        }
        void btnSend_Click(object sender, EventArgs e)
        {
            if (dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已审核")
                {
                    if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.LotteryExchangeService.Send(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()), Globle.CurrentEmployee.Base.Name);
                        RadMessageBox.Show("发送成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要发送的记录", "提示");
            }
        }
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已审核" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已修改")
                {
                    if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.LotteryExchangeService.Delete(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()));
                        RadMessageBox.Show("删除成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要删除的记录", "提示");
            }
        }
        void btnModify_Click(object sender, EventArgs e)
        {
            if(btnModify.Text == "保存")
            {
                if (dt != null)
                {
                    if (RadMessageBox.Show("确认保存吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        try
                        {
                            lottery_exchange_head head = null;
                            if(IsNew == true)
                            {
                                head = new lottery_exchange_head();
                                var n = YJH.Services.LotteryExchangeService.Getleh_exchange_id();
                                if (n != "")
                                {
                                    head.leh_exchange_id = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + (Convert.ToInt32(n) + 1).ToString().PadLeft(3, '0');
                                }
                                else
                                {
                                    head.leh_exchange_id = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + "001";
                                }
                                head.leh_store_id = storeID;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    head.leh_exchange_num_total += decimal.Parse(dr["led_exchange_num"].ToString());
                                    head.leh_exchange_amt_total += decimal.Parse(dr["led_exchange_amt"].ToString());
                                }
                                head.leh_company = Globle.CurrentBusinessUnit.OrgCode.ToString();
                                head.leh_status = (int)leh_status.已录入;
                                head.creater = Globle.CurrentEmployee.Base.Name;
                                head.create_date = OrgUnitService.GetDate();
                                head.send_status = (int)send_status.未通讯;
                                List<lottery_exchange_detail> details = new List<lottery_exchange_detail>();
                                int i = 1;
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    lottery_exchange_detail detail = new lottery_exchange_detail();
                                    detail.led_exchange_amt = decimal.Parse(dr1["led_exchange_amt"].ToString());
                                    detail.led_exchange_num = decimal.Parse(dr1["led_exchange_num"].ToString());
                                    detail.led_exchange_id = head.leh_exchange_id;
                                    detail.led_no = i;
                                    i++;
                                    detail.lottery_class = dr1["lottery_class"].ToString().PadLeft(2, '0');
                                    detail.lottery_series = dr1["lottery_series"].ToString().PadLeft(4, '0');
                                    detail.lottery_type = dr1["lottery_type"].ToString().PadLeft(4, '0');
                                    details.Add(detail);
                                }
                                YJH.Services.LotteryExchangeService.Save(details, head);
                                IsNew = false;
                            }
                            else
                            {
                                List<lottery_exchange_detail> details = new List<lottery_exchange_detail>();
                                int i = 1;
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if(dr1.RowState != DataRowState.Deleted)
                                    {
                                        lottery_exchange_detail detail = new lottery_exchange_detail();
                                        detail.led_exchange_amt = decimal.Parse(dr1["led_exchange_amt"].ToString());
                                        detail.led_exchange_num = decimal.Parse(dr1["led_exchange_num"].ToString());
                                        detail.led_exchange_id = dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString();
                                        detail.led_no = i;
                                        i++;
                                        detail.lottery_class = dr1["lottery_class"].ToString().PadLeft(2, '0');
                                        detail.lottery_series = dr1["lottery_series"].ToString().PadLeft(4, '0');
                                        detail.lottery_type = dr1["lottery_type"].ToString().PadLeft(4, '0');
                                        details.Add(detail);
                                    }
                                }
                                YJH.Services.LotteryExchangeService.SaveEdit(details, Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()));
                            }
                           
                            
                            RadMessageBox.Show("保存成功", "提示");
                            IsNew = false;
                            rpMain.Pages.Remove(rpMain.SelectedPage);
                        }
                        catch (Exception ex)
                        {
                            RadMessageBox.Show(ex.Message, "提示");
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show("请填写必要项","提示");
                }
            }
            else
            {
                if (dgMain.SelectedRows.Count > 0)
                {
                    if (dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已修改")
                    {
                        foreach (var item in rpMain.Pages)
                        {
                            if (item.TabIndex != 0)
                            {
                                if (dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString() == item.Tag.ToString())
                                {
                                    rpMain.SelectedPage = item;
                                    return;
                                }
                            }
                        }
                        RadPageViewPage page = new RadPageViewPage();
                        page.Text = "单据" + no;
                        no++;
                        UC_LotteryStockDetail lsd = new UC_LotteryStockDetail(this);
                        lsd.IsEdit = true;
                        lsd.sotreid = dgMain.SelectedRows[0].Cells["col_leh_store_id"].Value.ToString();
                        lsd.exchangeid = dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString();
                        page.Controls.Add(lsd);
                        page.Tag = dgMain.SelectedRows[0].Cells["col_leh_exchange_id"].Value.ToString();
                        this.rpMain.Pages.Add(page);
                        this.rpMain.SelectedPage = page;
                    }
                }
                else
                {
                    RadMessageBox.Show("请先选择需要修改的记录", "提示");
                }
            }
        }

        void UC_LotteryStockExchange_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;
            Search();
        }
        void rpMain_SelectedPageChanged(object sender, EventArgs e)
        {
            if (((Telerik.WinControls.UI.RadPageView)(sender)).SelectedPage.TabIndex == 0)
            {
                btnModify.Text = "修改";
                this.btnNew.Enabled = true;
                this.btnAdudit.Enabled = true;
                this.btnDelete.Enabled = true;
                this.btnSend.Enabled = true;
            }
            else
            {
                btnModify.Text = "保存";
                this.btnNew.Enabled = false;
                this.btnAdudit.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSend.Enabled = false;
            }
        }

        public void Save(System.Data.DataTable dt)
        {

        }
        #endregion

        
    }
}
