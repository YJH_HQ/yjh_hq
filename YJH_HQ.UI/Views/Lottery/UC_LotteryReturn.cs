﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH_HQ.UI.Services;
using YJH.Common;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryReturn : UserControl
    {
        #region ===字段及属性===
        private List<YJH.Entities.lottery_return> returnList = null;
        private List<YJH.Entities.ba_store> listStore = null;
        private Dictionary<int, Dictionary<int, string>> accountDic = null;
        //private List<LotteryAccount> accountList = null;
        #endregion

        #region ===构造方法===
        public UC_LotteryReturn()
        {
            InitializeComponent();
            this.Load += UC_LotteryReturn_Load;
            //this.rgvReturn.UserAddedRow += rgvReturn_UserAddedRow;
            //this.rgvReturn.CellEndEdit += rgvReturn_CellEndEdit;
            this.btnNew.Click += (s, e) => { this.NewEntity(); };
            this.btnModify.Click += (s, e) => { this.ModifyEntity(); };
            this.btnSave.Click += (s, e) => { this.SaveList(); };
            this.btnDelete.Click += (s, e) => { this.DeleteEntity(); };
            this.btnAdudit.Click += (s, e) => { this.Audit(); };
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnSend.Click += (s, e) => { this.SendData(); };
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_LotteryReturn_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;

            this.InitDictionary();
            this.SetColumnStore();
            this.SetColumnAccount();
            this.SetColumnLotteryClass();
            this.SetColumnLotteryType();
            this.SetColumnState();
            this.SetEntityState();
            this.SetGridState(true);
            this.Search();
        }
        /// <summary>
        /// 行添加后触发
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/23 最后修改日期：2014/12/23</remarks>
        private void rgvReturn_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            var entity = e.Row.DataBoundItem as YJH.Entities.lottery_return;
            entity.send_status = 10;
            entity.entity_status = 10;
            entity.creater = Globle.CurrentEmployee.Base.Name;
            entity.create_date = DateTime.Now;
        }
        /// <summary>
        /// 列结束编辑时触发
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/23 最后修改日期：2014/12/23</remarks>
        private void rgvReturn_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (e.Column.Name == "col_type")
            {
                if (e.Row.Cells["col_type"].Value != null)
                {
                    switch (Convert.ToInt32(e.Row.Cells["col_type"].Value))
                    {
                        case 10:
                            e.Row.Cells["col_account"].Value = 20;
                            break;
                        case 20:
                            e.Row.Cells["col_account"].Value = 50;
                            break;
                    }
                }
            }
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 查询
        /// </summary>
        private void Search()
        {
            var start = this.dtStartDate.Value.Date;
            var end = this.dtEndDate.Value.Date;
            if (start > end)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            var store = this.ddtStore.SelectedValue.ToString();

            returnList = YJH.Services.LotteryReturnService.Search(store, start.Date, end.AddDays(1).Date);
            this.Invoke(new Action(delegate()
            {
                this.rgvReturn.DataSource = returnList;
                this.CountMoney();
                this.CheckGridReadOnly();
            }));
        }
        /// <summary>
        /// 新建行
        /// </summary>
        private void NewEntity()
        {
            this.SetGridState(false);
        }
        /// <summary>
        /// 修改行
        /// </summary>
        private void ModifyEntity()
        {
            if (this.rgvReturn.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            var entity = this.rgvReturn.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_return;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            this.SetGridState(false);
        }
        /// <summary>
        /// 删除行
        /// </summary>
        private void DeleteEntity()
        {
            if (this.rgvReturn.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            var entity = this.rgvReturn.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_return;
            if (entity == null)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            if (entity.entity_status >= 40)
            {
                RadMessageBox.Show("该记录已被发送，无法删除，请联系管理员！", "提示");
                return;
            }
            this.SetGridState(true);
            if (RadMessageBox.Show("确认删除此行吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    YJH.Services.LotteryReturnService.DeleteEntity(entity.Instance.ID);
                    returnList.Remove(entity);
                    this.rgvReturn.DataSource = null;
                    this.rgvReturn.DataSource = returnList;
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show("删除出错，错误原因：" + ex.Message, "提示");
                }
            }
        }
        /// <summary>
        /// 保存集合
        /// </summary>
        private void SaveList()
        {
            this.rgvReturn.PostEdit();
            if (!CheckGrid())
            {
                return;
            }
            this.SetGridState(true);
            foreach (YJH.Entities.lottery_return item in returnList)
            {
                if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                {
                    item.send_status = 10;
                    item.entity_status = 10;
                    item.creater = Globle.CurrentEmployee.Base.Name;
                    item.create_date = DateTime.Now;
                }
                else if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Modified)
                {
                    item.updater = Globle.CurrentEmployee.Base.Name;
                    item.update_date = DateTime.Now.Date;
                    item.entity_status = 20;
                }
                var data = accountDic[item.lottery_class][item.lottery_type];
                item.lottery_account = data;
            }
            try
            {
                YJH.Services.LotteryReturnService.SaveList(returnList);
                RadMessageBox.Show("保存成功！", "提示");
                this.SetAcceptChanges();
                this.CountMoney();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("保存出错，错误原因：" + ex.Message, "提示");
            }
        }
        /// <summary>
        /// 审核
        /// </summary>
        private void Audit()
        {
            if (this.rgvReturn.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            this.SetGridState(true);
            var entity = this.rgvReturn.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_return;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            if (entity.entity_status >= 30)
            {
                RadMessageBox.Show("该记录已是审核或生效状态，无法再审核！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var newEntity = YJH.Services.LotteryReturnService.AuditData(entity, Globle.CurrentEmployee.Base.Name);
                entity.audit_date = newEntity.audit_date;
                entity.auditer = newEntity.auditer;
                entity.entity_status = newEntity.entity_status;
                RadMessageBox.Show("审核完毕！", "提示");
                this.SetAcceptChanges();
                this.CheckGridReadOnly();
            }
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        private void SendData()
        {
            if (this.rgvReturn.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            var entity = this.rgvReturn.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_return;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            if (entity.entity_status == 40)
            {
                return;
            }
            if (entity.entity_status != 30)
            {
                RadMessageBox.Show("请审核完该条记录再进行发送操作！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var newEntity = YJH.Services.LotteryReturnService.SendData(entity, Globle.CurrentEmployee.Base.Name);
                RadMessageBox.Show("发送完毕！", "提示");
                entity.effectiver = newEntity.effectiver;
                entity.effective_date = newEntity.effective_date;
                entity.entity_status = newEntity.entity_status;
                entity.send_status = newEntity.send_status;
                this.SetAcceptChanges();
            }
        }
        /// <summary>
        /// 设定列表状态
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetGridState(bool IsReadOnly)
        {
            if (!IsReadOnly)
            {
                this.rgvReturn.AllowAddNewRow = true;
                this.rgvReturn.AllowEditRow = true;
                this.rgvReturn.ReadOnly = false;
            }
            else
            {
                this.rgvReturn.AllowAddNewRow = false;
                this.rgvReturn.AllowEditRow = false;
                this.rgvReturn.ReadOnly = true;
            }
        }
        /// <summary>
        /// 初始化彩票种类列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryClass()
        {
            var columnClass = this.rgvReturn.Columns["col_class"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryClass)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryClass), ot),
                            ValueMember = ot
                        }).ToList();
            columnClass.DisplayMember = "DisplayMember";
            columnClass.ValueMember = "ValueMember";
            columnClass.DataSource = list;
        }
        /// <summary>
        /// 初始化彩票类型列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryType()
        {
            var columnType = this.rgvReturn.Columns["col_type"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryType)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryType), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }
        /// <summary>
        /// 初始化通讯状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnState()
        {
            var columnState = this.rgvReturn.Columns["col_status"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.CumState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.CumState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 初始化门店下拉列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnStore()
        {
            listStore = YJH.Services.LotteryReturnService.GetStores();
            var columnStore = this.rgvReturn.Columns["col_store"] as GridViewComboBoxColumn;
            var list = (from ot in listStore
                        select new
                        {
                            DisplayMember = ot.store_name,
                            ValueMember = ot.store_id
                        }).ToList();
            columnStore.DisplayMember = "DisplayMember";
            columnStore.ValueMember = "ValueMember";
            columnStore.DataSource = list;
        }
        /// <summary>
        /// 初始化实体状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetEntityState()
        {
            var columnState = this.rgvReturn.Columns["col_entitystatus"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 设置彩票账户
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/24 最后修改日期：2014/12/24</remarks>
        private void SetColumnAccount()
        {
            //accountList = new List<LotteryAccount>();
            //accountList.Add(new LotteryAccount() { code = "0002", name = "机打返利账户" });
            //accountList.Add(new LotteryAccount() { code = "0005", name = "刮卡返利账户" });
            //var columnAccount = this.rgvReturn.Columns["col_account"] as GridViewComboBoxColumn;
            //var list = (from ot in accountList
            //            select new
            //            {
            //                DisplayMember = ot.name,
            //                ValueMember = ot.code
            //            }).ToList();
            //columnAccount.DisplayMember = "DisplayMember";
            //columnAccount.ValueMember = "ValueMember";
            //columnAccount.DataSource = list;
            var table = YJH.Services.LotteryReturnService.GetAccountList("01");
            var columnAccount = this.rgvReturn.Columns["col_account"] as GridViewComboBoxColumn;
            columnAccount.DisplayMember = "accountname";
            columnAccount.ValueMember = "accountcode";
            columnAccount.DataSource = table;
        }
        /// <summary>
        /// 检查列表
        /// </summary>
        private bool CheckGrid()
        {
            bool canSave = true;
            foreach (var row in this.rgvReturn.Rows)
            {
                foreach (GridViewCellInfo cell in row.Cells)
                {
                    switch (cell.ColumnInfo.Name)
                    {
                        case "col_store":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_rebate":
                            decimal money;
                            if (!decimal.TryParse(cell.Value.ToString(), out money))
                            {
                                canSave = false;
                            }
                            else
                            {
                                if (money <= 0)
                                {
                                    canSave = false;
                                }
                            }
                            break;
                        case "col_class":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_type":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        //case "col_account":
                        //    if (cell.Value == null)
                        //    {
                        //        canSave = false;
                        //    }
                        //    else if (Convert.ToInt32(cell.Value) == 0)
                        //    {
                        //        canSave = false;
                        //    }
                        //    break;
                        //case "col_creater":
                        //    if (cell.Value == null)
                        //    {
                        //        canSave = false;
                        //    }
                        //    break;
                        //case "col_createdate":
                        //    DateTime result;
                        //    if (!DateTime.TryParse(cell.Value.ToString(), out result))
                        //    {
                        //        canSave = false;
                        //    }
                        //    else
                        //    {
                        //        if (result == DateTime.MinValue)
                        //        {
                        //            canSave = false;
                        //        }
                        //    }
                        //    break;
                    }
                    if (!canSave)
                    {
                        RadMessageBox.Show("列表第" + (row.Index + 1) + "行，第" + (cell.ColumnInfo.Index + 1) + "列" + cell.ColumnInfo.HeaderText + "必须按要求填写");
                        break;
                    }
                }
                if (!canSave)
                {
                    break;
                }
            }
            return canSave;
        }
        /// <summary>
        /// 合计金额
        /// </summary>
        private void CountMoney()
        {
            if (returnList == null || returnList.Count <= 0)
            {
                this.labelCount.Text = "合计：0元";
            }
            else
            {
                decimal money = 0;
                foreach (var item in returnList)
                {
                    money += item.rebate_amt;
                }
                this.labelCount.Text = "合计：" + money.ToString("0.00") + "元";
            }
        }
        /// <summary>
        /// 初始化字典查询(彩票账户)
        /// </summary>
        private void InitDictionary()
        {
            accountDic = new Dictionary<int, Dictionary<int, string>>();
            var temp = new Dictionary<int, string>();
            temp.Add(10, "0002");
            temp.Add(20, "0005");
            accountDic.Add(1, temp); //暂时体彩                      
        }
        /// <summary>
        /// 把实体状态置成不改动
        /// </summary>
        private void SetAcceptChanges()
        {
            foreach (YJH.Entities.lottery_return item in returnList)
            {
                item.Instance.AcceptChanges();
            }
            this.rgvReturn.RefreshData();
        }
        /// <summary>
        /// 检查列表某些列在特殊情况下只读
        /// </summary>
        private void CheckGridReadOnly()
        {
            foreach (var row in this.rgvReturn.Rows)
            {
                if (Convert.ToInt32(row.Cells["col_entitystatus"].Value) >= 30)
                {
                    row.Cells["col_store"].ReadOnly = true;
                    row.Cells["col_rebate"].ReadOnly = true;
                    row.Cells["col_class"].ReadOnly = true;
                    row.Cells["col_type"].ReadOnly = true;
                }
            }
        }
        #endregion
    }
}
