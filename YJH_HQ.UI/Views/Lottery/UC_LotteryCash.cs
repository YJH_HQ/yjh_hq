﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH_HQ.UI.Services;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryCash : UserControl
    {
        #region ===字段及属性===
        private List<YJH.Entities.lottery_cash> cashList = null;
        private List<YJH.Entities.ba_store> listStore = null;
        #endregion

        #region ===构造方法===
        public UC_LotteryCash()
        {
            InitializeComponent();
            this.Load += UC_LotteryExchange_Load;
            this.rgvCash.UserAddedRow += rgvCash_UserAddedRow;
            this.btnNew.Click += (s, e) => { this.NewEntity(); };
            this.btnModify.Click += (s, e) => { this.ModifyEntity(); };
            this.btnSave.Click += (s, e) => { this.SaveList(); };
            this.btnDelete.Click += (s, e) => { this.DeleteEntity(); };
            this.btnAdudit.Click += (s, e) => { this.Audit(); };
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnSend.Click += (s, e) => { this.SendData(); };
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_LotteryExchange_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;

            this.SetColumnStore();
            this.SetColumnLotteryClass();
            this.SetColumnLotteryType();
            this.SetColumnState();
            this.SetEntityState();
            this.SetGridState(true);
            this.Search();
        }
        /// <summary>
        /// 行添加后触发
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/23 最后修改日期：2014/12/23</remarks>
        private void rgvCash_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            var entity = e.Row.DataBoundItem as YJH.Entities.lottery_cash;
            entity.send_status = 10;
            entity.entity_status = 10;
            entity.creater = Globle.CurrentEmployee.Base.Name;
            entity.create_date = DateTime.Now;
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 查询
        /// </summary>
        private void Search()
        {
            var start = this.dtStartDate.Value.Date;
            var end = this.dtEndDate.Value.Date;
            if (start > end)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            var store = this.ddtStore.SelectedValue.ToString();

            cashList = YJH.Services.LotteryCashService.Search(store, start.Date, end.AddDays(1).Date);
            this.rgvCash.DataSource = cashList;
            this.CountMoney();
        }
        /// <summary>
        /// 新建行
        /// </summary>
        private void NewEntity()
        {
            this.SetGridState(false);
        }
        /// <summary>
        /// 修改行
        /// </summary>
        private void ModifyEntity()
        {
            if (this.rgvCash.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            var entity = this.rgvCash.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_cash;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            this.SetGridState(false);
        }
        /// <summary>
        /// 删除行
        /// </summary>
        private void DeleteEntity()
        {
            if (this.rgvCash.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            var entity = this.rgvCash.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_cash;
            if (entity == null)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            this.SetGridState(true);
            if (RadMessageBox.Show("确认删除此行吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    YJH.Services.LotteryCashService.DeleteEntity(entity.Instance.ID);
                    cashList.Remove(entity);
                    this.rgvCash.DataSource = null;
                    this.rgvCash.DataSource = cashList;
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show("删除出错，错误原因：" + ex.Message, "提示");
                }
            }
        }
        /// <summary>
        /// 保存集合
        /// </summary>
        private void SaveList()
        {
            this.rgvCash.PostEdit();
            if (!CheckGrid())
            {
                return;
            }
            this.SetGridState(true);
            foreach (YJH.Entities.lottery_cash item in cashList)
            {
                if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Modified)
                {
                    item.updater = Globle.CurrentEmployee.Base.Name;
                    item.update_date = DateTime.Now.Date;
                    item.entity_status = 20;
                }
            }
            try
            {
                YJH.Services.LotteryCashService.SaveList(cashList);
                RadMessageBox.Show("保存成功！", "提示");
                this.Search();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("保存出错，错误原因：" + ex.Message, "提示");
            }
        }
        /// <summary>
        /// 审核
        /// </summary>
        private void Audit()
        {
            if (this.rgvCash.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            this.SetGridState(true);
            var entity = this.rgvCash.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_cash;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                entity.auditer = Globle.CurrentEmployee.Base.Name;
                entity.audit_date = DateTime.Now.Date;
                entity.entity_status = 30;
                YJH.Services.LotteryCashService.SaveList(cashList);
                RadMessageBox.Show("审核完毕！", "提示");
                this.Search();
            }
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        private void SendData()
        {
            if (this.rgvCash.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            var entity = this.rgvCash.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_cash;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            if (entity.entity_status == 40)
            {
                return;
            }
            if (entity.entity_status != 30)
            {
                RadMessageBox.Show("请审核完该条记录再进行发送操作！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                entity.effectiver = Globle.CurrentEmployee.Base.Name;
                entity.effective_date = DateTime.Now.Date;
                entity.entity_status = 40;
                entity.send_status = 20;
                YJH.Services.LotteryCashService.SaveList(cashList);
                RadMessageBox.Show("发送完毕！", "提示");
                this.Search();
            }
        }
        /// <summary>
        /// 设定列表状态
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetGridState(bool IsReadOnly)
        {
            if (!IsReadOnly)
            {
                this.rgvCash.AllowAddNewRow = true;
                this.rgvCash.AllowEditRow = true;
                this.rgvCash.ReadOnly = false;
            }
            else
            {
                this.rgvCash.AllowAddNewRow = false;
                this.rgvCash.AllowEditRow = false;
                this.rgvCash.ReadOnly = true;
            }
        }
        /// <summary>
        /// 初始化彩票种类列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryClass()
        {
            var columnClass = this.rgvCash.Columns["col_class"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryClass)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryClass), ot),
                            ValueMember = ot
                        }).ToList();
            columnClass.DisplayMember = "DisplayMember";
            columnClass.ValueMember = "ValueMember";
            columnClass.DataSource = list;
        }
        /// <summary>
        /// 初始化彩票类型列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryType()
        {
            var columnType = this.rgvCash.Columns["col_type"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryType)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryType), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }
        /// <summary>
        /// 初始化通讯状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnState()
        {
            var columnState = this.rgvCash.Columns["col_status"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.CumState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.CumState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 初始化门店下拉列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnStore()
        {
            listStore = YJH.Services.LotteryChargeService.GetStores();
            var columnStore = this.rgvCash.Columns["col_store"] as GridViewComboBoxColumn;
            var list = (from ot in listStore
                        select new
                        {
                            DisplayMember = ot.store_name,
                            ValueMember = ot.store_id
                        }).ToList();
            columnStore.DisplayMember = "DisplayMember";
            columnStore.ValueMember = "ValueMember";
            columnStore.DataSource = list;
        }
        /// <summary>
        /// 初始化实体状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetEntityState()
        {
            var columnState = this.rgvCash.Columns["col_entitystatus"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 检查列表
        /// </summary>
        private bool CheckGrid()
        {
            bool canSave = true;
            foreach (var row in this.rgvCash.Rows)
            {
                foreach (GridViewCellInfo cell in row.Cells)
                {
                    switch (cell.ColumnInfo.Name)
                    {
                        case "col_store":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_imprest":
                            decimal money;
                            if (!decimal.TryParse(cell.Value.ToString(), out money))
                            {
                                canSave = false;
                            }
                            else
                            {
                                if (money <= 0)
                                {
                                    canSave = false;
                                }
                            }
                            break;
                        case "col_class":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_type":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_creater":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_createdate":
                            DateTime result;
                            if (!DateTime.TryParse(cell.Value.ToString(), out result))
                            {
                                canSave = false;
                            }
                            else
                            {
                                if (result == DateTime.MinValue)
                                {
                                    canSave = false;
                                }
                            }
                            break;
                    }
                    if (!canSave)
                    {
                        RadMessageBox.Show("列表第" + (row.Index + 1) + "行，第" + (cell.ColumnInfo.Index + 1) + "列" + cell.ColumnInfo.HeaderText + "必须按要求填写");
                        break;
                    }
                }
                if (!canSave)
                {
                    break;
                }
            }
            return canSave;
        }
        /// <summary>
        /// 合计金额
        /// </summary>
        private void CountMoney()
        {
            if (cashList == null || cashList.Count <= 0)
            {
                this.labelCount.Text = "合计：0元";
            }
            else
            {
                decimal money = 0;
                foreach (var item in cashList)
                {
                    money += item.imprest_amt;
                }
                this.labelCount.Text = "合计：" + money.ToString("0.00") + "元";
            }
        }
        #endregion
    }
}
