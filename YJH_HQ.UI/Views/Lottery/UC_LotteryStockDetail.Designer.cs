﻿namespace YJH_HQ.UI.Views.Lottery
{
    partial class UC_LotteryStockDetail
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            this.lbTop = new Telerik.WinControls.UI.RadLabel();
            this.lbexchangeid = new Telerik.WinControls.UI.RadLabel();
            this.dgDetail = new Telerik.WinControls.UI.RadGridView();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.lbDetailCount = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lbTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbexchangeid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbDetailCount)).BeginInit();
            this.SuspendLayout();
            // 
            // lbTop
            // 
            this.lbTop.Location = new System.Drawing.Point(325, 3);
            this.lbTop.Name = "lbTop";
            this.lbTop.Size = new System.Drawing.Size(72, 18);
            this.lbTop.TabIndex = 0;
            this.lbTop.Text = "____单据编号";
            // 
            // lbexchangeid
            // 
            this.lbexchangeid.Location = new System.Drawing.Point(12, 21);
            this.lbexchangeid.Name = "lbexchangeid";
            this.lbexchangeid.Size = new System.Drawing.Size(42, 18);
            this.lbexchangeid.TabIndex = 1;
            this.lbexchangeid.Text = "单号：";
            // 
            // dgDetail
            // 
            this.dgDetail.BackColor = System.Drawing.SystemColors.Control;
            this.dgDetail.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDetail.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.dgDetail.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dgDetail.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgDetail.Location = new System.Drawing.Point(0, 0);
            // 
            // dgDetail
            // 
            this.dgDetail.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "ID";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "col_ID";
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "led_no";
            gridViewTextBoxColumn2.HeaderText = "明细编号";
            gridViewTextBoxColumn2.Name = "col_led_no";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 60;
            gridViewComboBoxColumn1.EnableExpressionEditor = false;
            gridViewComboBoxColumn1.FieldName = "lottery_class";
            gridViewComboBoxColumn1.HeaderText = "彩票种类";
            gridViewComboBoxColumn1.Name = "col_lottery_class";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 60;
            gridViewComboBoxColumn2.EnableExpressionEditor = false;
            gridViewComboBoxColumn2.FieldName = "lottery_series";
            gridViewComboBoxColumn2.HeaderText = "彩票系列";
            gridViewComboBoxColumn2.Name = "col_lottery_series";
            gridViewComboBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn2.Width = 60;
            gridViewComboBoxColumn3.EnableExpressionEditor = false;
            gridViewComboBoxColumn3.FieldName = "lottery_type";
            gridViewComboBoxColumn3.HeaderText = "彩票类型";
            gridViewComboBoxColumn3.Name = "col_lottery_type";
            gridViewComboBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn3.Width = 60;
            gridViewDecimalColumn1.EnableExpressionEditor = false;
            gridViewDecimalColumn1.FieldName = "led_exchange_num";
            gridViewDecimalColumn1.HeaderText = "置换数量";
            gridViewDecimalColumn1.Name = "col_led_exchange_num";
            gridViewDecimalColumn1.Width = 60;
            gridViewDecimalColumn2.EnableExpressionEditor = false;
            gridViewDecimalColumn2.FieldName = "led_exchange_amt";
            gridViewDecimalColumn2.HeaderText = "置换金额";
            gridViewDecimalColumn2.Name = "col_led_exchange_amt";
            gridViewDecimalColumn2.Width = 60;
            this.dgDetail.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewComboBoxColumn2,
            gridViewComboBoxColumn3,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2});
            this.dgDetail.Name = "dgDetail";
            this.dgDetail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgDetail.ShowGroupPanel = false;
            this.dgDetail.Size = new System.Drawing.Size(782, 500);
            this.dgDetail.TabIndex = 2;
            this.dgDetail.Text = "radGridView1";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(646, 21);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(125, 20);
            this.ddtStore.TabIndex = 42;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(603, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 18);
            this.radLabel3.TabIndex = 41;
            this.radLabel3.Text = "门店：";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.lbexchangeid);
            this.radPanel1.Controls.Add(this.ddtStore);
            this.radPanel1.Controls.Add(this.lbTop);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(782, 45);
            this.radPanel1.TabIndex = 43;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radPanel3);
            this.radPanel2.Controls.Add(this.dgDetail);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(0, 45);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(782, 500);
            this.radPanel2.TabIndex = 44;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.lbDetailCount);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel3.Location = new System.Drawing.Point(0, 471);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(782, 29);
            this.radPanel3.TabIndex = 3;
            // 
            // lbDetailCount
            // 
            this.lbDetailCount.Location = new System.Drawing.Point(3, 5);
            this.lbDetailCount.Name = "lbDetailCount";
            this.lbDetailCount.Size = new System.Drawing.Size(42, 18);
            this.lbDetailCount.TabIndex = 0;
            this.lbDetailCount.Text = "合计：";
            // 
            // UC_LotteryStockDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.Name = "UC_LotteryStockDetail";
            this.Size = new System.Drawing.Size(782, 545);
            ((System.ComponentModel.ISupportInitialize)(this.lbTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbexchangeid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbDetailCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lbTop;
        private Telerik.WinControls.UI.RadLabel lbexchangeid;
        private Telerik.WinControls.UI.RadGridView dgDetail;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadLabel lbDetailCount;
    }
}
