﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YJH_HQ.UI.Services;
using System.Data.SqlClient;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryDayBalance : UserControl
    {
        #region ===字段及属性===
        DataTable table = null;
        #endregion

        #region ===构造方法===
        public UC_LotteryDayBalance()
        {
            InitializeComponent();
            this.Load += UC_LotteryDayBalance_Load;
            this.btnBalance.Click += (s, e) => { this.Balance(); };
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UC_LotteryDayBalance_Load(object sender, EventArgs e)
        {
            this.dtDate.Value = DateTime.Now;
            table = YJH.Services.BalanceLogService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 日结
        /// </summary>
        private void Balance()
        {                        
            try
            {
                foreach (DataRow row in table.Rows)
                {
                    if (row["store_id"].ToString() != "")
                    {
                        SqlHelper.TakeStoredProcedure("p_lottery_day_banlance_hq", new SqlParameter("@store_id", row["store_id"].ToString()), 
                            new SqlParameter("@input_today", DateTime.Now.ToString("yyyy-MM-dd")));
                    }                    
                }               
                this.Invoke(new Action(delegate()
                {
                    string storeID = "";
                    if (!string.IsNullOrWhiteSpace(this.ddtStore.SelectedValue.ToString()))
                    {
                        storeID = this.ddtStore.SelectedValue.ToString();
                    }                    
                    var data = YJH.Services.BalanceLogService.Search(storeID);
                    this.rgvBalanceLog.DataSource = data;
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
