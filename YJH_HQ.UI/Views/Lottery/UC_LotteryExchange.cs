﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH_HQ.UI.Services;

namespace YJH_HQ.UI.Views.Lottery
{
    public partial class UC_LotteryExchange : UserControl
    {
        #region ===字段及属性===
        private List<YJH.Entities.lottery_exchange> exchangeList = null;
        private List<YJH.Entities.ba_store> listStore = null;
        #endregion

        #region ===构造方法===
        public UC_LotteryExchange()
        {
            InitializeComponent();
            this.Load += UC_LotteryExchange_Load;
            //this.rgvExchange.UserAddedRow += rgvExchange_UserAddedRow;
            this.btnNew.Click += (s, e) => { this.NewEntity(); };
            this.btnModify.Click += (s, e) => { this.ModifyEntity(); };
            this.btnSave.Click += (s, e) => { this.SaveList(); };
            this.btnDelete.Click += (s, e) => { this.DeleteEntity(); };
            this.btnAdudit.Click += (s, e) => { this.Audit(); };
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnSend.Click += (s, e) => { this.SendData(); };
        }
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_LotteryExchange_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;

            this.SetColumnStore();
            this.SetColumnLotteryClass();
            this.SetColumnLotteryType();
            this.SetColumnState();
            this.SetEntityState();
            this.SetGridState(true);
            this.Search();
        }
        /// <summary>
        /// 行添加后触发
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/23 最后修改日期：2014/12/23</remarks>
        private void rgvExchange_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            var entity = e.Row.DataBoundItem as YJH.Entities.lottery_exchange;
            entity.send_status = 10;
            entity.entity_status = 10;
            entity.creater = Globle.CurrentEmployee.Base.Name;
            entity.create_date = DateTime.Now;
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 查询
        /// </summary>
        private void Search()
        {
            var start = this.dtStartDate.Value.Date;
            var end = this.dtEndDate.Value.Date;
            if (start > end)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            var store = this.ddtStore.SelectedValue.ToString();
            exchangeList = YJH.Services.LotteryExchangeService.Search(store, start.Date, end.AddDays(1).Date);
            this.Invoke(new Action(delegate()
            {
                this.rgvExchange.DataSource = exchangeList;
                this.CountMoney();
                this.CheckGridReadOnly();
            }));
        }
        /// <summary>
        /// 新建行
        /// </summary>
        private void NewEntity()
        {
            this.SetGridState(false);
        }
        /// <summary>
        /// 修改行
        /// </summary>
        private void ModifyEntity()
        {
            if (this.rgvExchange.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            var entity = this.rgvExchange.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_exchange;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行修改操作！", "提示");
                return;
            }
            this.SetGridState(false);
        }
        /// <summary>
        /// 删除行
        /// </summary>
        private void DeleteEntity()
        {
            if (this.rgvExchange.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            var entity = this.rgvExchange.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_exchange;
            if (entity == null)
            {
                RadMessageBox.Show("未能选中行进行删除操作！", "提示");
                return;
            }
            if (entity.entity_status >= 40)
            {
                RadMessageBox.Show("该记录已被发送，无法删除，请联系管理员！", "提示");
                return;
            }
            this.SetGridState(true);
            if (RadMessageBox.Show("确认删除此行吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    YJH.Services.LotteryExchangeService.DeleteEntity(entity.Instance.ID);
                    exchangeList.Remove(entity);
                    this.rgvExchange.DataSource = null;
                    this.rgvExchange.DataSource = exchangeList;
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show("删除出错，错误原因：" + ex.Message, "提示");
                }
            }
        }
        /// <summary>
        /// 保存集合
        /// </summary>
        private void SaveList()
        {
            this.rgvExchange.PostEdit();
            if (!CheckGrid())
            {
                return;
            }
            this.SetGridState(true);
            foreach (YJH.Entities.lottery_exchange item in exchangeList)
            {
                if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                {
                    item.send_status = 10;
                    item.entity_status = 10;
                    item.creater = Globle.CurrentEmployee.Base.Name;
                    item.create_date = DateTime.Now;
                }
                else if (item.Instance.PersistentState == dps.Common.Data.PersistentState.Modified)
                {
                    item.updater = Globle.CurrentEmployee.Base.Name;
                    item.update_date = DateTime.Now.Date;
                    item.entity_status = 20;
                }
            }
            try
            {
                YJH.Services.LotteryExchangeService.SaveList(exchangeList);
                RadMessageBox.Show("保存成功！", "提示");
                this.SetAcceptChanges();
                this.CountMoney();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("保存出错，错误原因：" + ex.Message, "提示");
            }
        }
        /// <summary>
        /// 审核
        /// </summary>
        private void Audit()
        {
            if (this.rgvExchange.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            this.SetGridState(true);
            var entity = this.rgvExchange.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_exchange;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行审核操作！", "提示");
                return;
            }
            if (entity.entity_status >= 30)
            {
                RadMessageBox.Show("该记录已是审核或生效状态，无法再审核！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var newEntity = YJH.Services.LotteryExchangeService.AuditData(entity, Globle.CurrentEmployee.Base.Name);
                RadMessageBox.Show("审核完毕！", "提示");
                entity.audit_date = newEntity.audit_date;
                entity.auditer = newEntity.auditer;
                entity.entity_status = newEntity.entity_status;
                this.SetAcceptChanges();
                this.CheckGridReadOnly();
            }
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        private void SendData()
        {
            if (this.rgvExchange.SelectedRows.Count <= 0)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            var entity = this.rgvExchange.SelectedRows[0].DataBoundItem as YJH.Entities.lottery_exchange;
            if (entity == null)
            {
                RadMessageBox.Show("请选中一行进行发送操作！", "提示");
                return;
            }
            if (entity.entity_status == 40)
            {
                return;
            }
            if (entity.entity_status != 30)
            {
                RadMessageBox.Show("请审核完该条记录再进行发送操作！", "提示");
                return;
            }
            if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    var newEntity = YJH.Services.LotteryExchangeService.SendData(entity, Globle.CurrentEmployee.Base.Name);
                    RadMessageBox.Show("发送完毕！", "提示");
                    entity.effectiver = newEntity.effectiver;
                    entity.effective_date = newEntity.effective_date;
                    entity.entity_status = newEntity.entity_status;
                    entity.send_status = newEntity.send_status;
                    this.SetAcceptChanges();
                }
                catch(Exception ex)
                {
                    RadMessageBox.Show("发送出错，错误原因：" + ex.Message, "提示");
                }
            }
        }
        /// <summary>
        /// 设定列表状态
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetGridState(bool IsReadOnly)
        {
            if (!IsReadOnly)
            {
                this.rgvExchange.AllowAddNewRow = true;
                this.rgvExchange.AllowEditRow = true;
                this.rgvExchange.ReadOnly = false;
            }
            else
            {
                this.rgvExchange.AllowAddNewRow = false;
                this.rgvExchange.AllowEditRow = false;
                this.rgvExchange.ReadOnly = true;
            }
        }
        /// <summary>
        /// 初始化彩票种类列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryClass()
        {
            var columnClass = this.rgvExchange.Columns["col_class"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryClass)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryClass), ot),
                            ValueMember = ot
                        }).ToList();
            columnClass.DisplayMember = "DisplayMember";
            columnClass.ValueMember = "ValueMember";
            columnClass.DataSource = list;
        }
        /// <summary>
        /// 初始化彩票类型列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryType()
        {
            var columnType = this.rgvExchange.Columns["col_type"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryType)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryType), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }
        /// <summary>
        /// 初始化通讯状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnState()
        {
            var columnState = this.rgvExchange.Columns["col_status"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.CumState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.CumState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 初始化门店下拉列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnStore()
        {
            listStore = YJH.Services.LotteryChargeService.GetStores();
            var columnStore = this.rgvExchange.Columns["col_store"] as GridViewComboBoxColumn;
            var list = (from ot in listStore
                        select new
                        {
                            DisplayMember = ot.store_name,
                            ValueMember = ot.store_id
                        }).ToList();
            columnStore.DisplayMember = "DisplayMember";
            columnStore.ValueMember = "ValueMember";
            columnStore.DataSource = list;
        }
        /// <summary>
        /// 初始化实体状态列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetEntityState()
        {
            var columnState = this.rgvExchange.Columns["col_entitystatus"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryState)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryState), ot),
                            ValueMember = ot
                        }).ToList();
            columnState.DisplayMember = "DisplayMember";
            columnState.ValueMember = "ValueMember";
            columnState.DataSource = list;
        }
        /// <summary>
        /// 检查列表
        /// </summary>
        private bool CheckGrid()
        {
            bool canSave = true;
            foreach (var row in this.rgvExchange.Rows)
            {
                foreach (GridViewCellInfo cell in row.Cells)
                {
                    switch (cell.ColumnInfo.Name)
                    {
                        case "col_store":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_recp":
                            decimal money;
                            if (!decimal.TryParse(cell.Value.ToString(), out money))
                            {
                                canSave = false;
                            }
                            else
                            {
                                if (money <= 0)
                                {
                                    canSave = false;
                                }
                            }
                            break;
                        case "col_class":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        case "col_type":
                            if (cell.Value == null)
                            {
                                canSave = false;
                            }
                            else if (Convert.ToInt32(cell.Value) == 0)
                            {
                                canSave = false;
                            }
                            break;
                        //case "col_creater":
                        //    if (cell.Value == null)
                        //    {
                        //        canSave = false;
                        //    }
                        //    break;
                        //case "col_createdate":
                        //    DateTime result;
                        //    if (!DateTime.TryParse(cell.Value.ToString(), out result))
                        //    {
                        //        canSave = false;
                        //    }
                        //    else
                        //    {
                        //        if (result == DateTime.MinValue)
                        //        {
                        //            canSave = false;
                        //        }
                        //    }
                        //    break;
                    }
                    if (!canSave)
                    {
                        RadMessageBox.Show("列表第" + (row.Index + 1) + "行，第" + (cell.ColumnInfo.Index + 1) + "列" + cell.ColumnInfo.HeaderText + "必须按要求填写");
                        break;
                    }
                }
                if (!canSave)
                {
                    break;
                }
            }
            return canSave;
        }
        /// <summary>
        /// 合计金额
        /// </summary>
        private void CountMoney()
        {
            if (exchangeList == null || exchangeList.Count <= 0)
            {
                this.labelCount.Text = "合计：0元";
            }
            else
            {
                decimal money = 0;
                foreach (var item in exchangeList)
                {
                    money += item.recp_amt;
                }
                this.labelCount.Text = "合计：" + money.ToString("0.00") + "元";
            }
        }
        /// <summary>
        /// 把实体状态置成不改动
        /// </summary>
        private void SetAcceptChanges()
        {
            foreach (YJH.Entities.lottery_exchange item in exchangeList)
            {
                item.Instance.AcceptChanges();
            }
            this.rgvExchange.RefreshData();
        }
        /// <summary>
        /// 检查列表某些列在特殊情况下只读
        /// </summary>
        private void CheckGridReadOnly()
        {
            foreach (var row in this.rgvExchange.Rows)
            {
                if (Convert.ToInt32(row.Cells["col_entitystatus"].Value) >= 30)
                {
                    row.Cells["col_store"].ReadOnly = true;
                    row.Cells["col_recp"].ReadOnly = true;
                    row.Cells["col_class"].ReadOnly = true;
                    row.Cells["col_type"].ReadOnly = true;
                }
            }
        }
        #endregion
    }
}
