﻿namespace YJH_HQ.UI.Views.Lottery
{
    partial class UC_LotteryCash
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn4 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnModify = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btnAdudit = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSend = new Telerik.WinControls.UI.CommandBarButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.dtEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.labelCount = new Telerik.WinControls.UI.RadLabel();
            this.rgvCash = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvCash.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(959, 30);
            this.radCommandBar1.TabIndex = 5;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnModify,
            this.btnDelete,
            this.btnSave,
            this.btnAdudit,
            this.btnSend});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新建";
            this.btnNew.AccessibleName = "新建";
            this.btnNew.DisplayName = "commandBarButton1";
            this.btnNew.DrawText = true;
            this.btnNew.Image = null;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnModify
            // 
            this.btnModify.AccessibleDescription = "打开";
            this.btnModify.AccessibleName = "打开";
            this.btnModify.DisplayName = "commandBarButton1";
            this.btnModify.DrawText = true;
            this.btnModify.Image = null;
            this.btnModify.Name = "btnModify";
            this.btnModify.Text = "修改";
            this.btnModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModify.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton2";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = null;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleDescription = "保存";
            this.btnSave.AccessibleName = "保存";
            this.btnSave.DisplayName = "commandBarButton1";
            this.btnSave.DrawText = true;
            this.btnSave.Image = null;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "保存";
            this.btnSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnAdudit
            // 
            this.btnAdudit.AccessibleDescription = "审核";
            this.btnAdudit.AccessibleName = "审核";
            this.btnAdudit.DisplayName = "commandBarButton2";
            this.btnAdudit.DrawText = true;
            this.btnAdudit.Image = null;
            this.btnAdudit.Name = "btnAdudit";
            this.btnAdudit.Text = "审核";
            this.btnAdudit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSend
            // 
            this.btnSend.AccessibleDescription = "发送";
            this.btnSend.AccessibleName = "发送";
            this.btnSend.DisplayName = "commandBarButton3";
            this.btnSend.DrawText = true;
            this.btnSend.Image = null;
            this.btnSend.Name = "btnSend";
            this.btnSend.Text = "发送";
            this.btnSend.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.dtEndDate);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.dtStartDate);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(959, 56);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "查询条件";
            // 
            // dtEndDate
            // 
            this.dtEndDate.AutoSize = false;
            this.dtEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDate.Location = new System.Drawing.Point(427, 26);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(120, 20);
            this.dtEndDate.TabIndex = 44;
            this.dtEndDate.TabStop = false;
            this.dtEndDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(403, 26);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(18, 18);
            this.radLabel2.TabIndex = 43;
            this.radLabel2.Text = "至";
            // 
            // dtStartDate
            // 
            this.dtStartDate.AutoSize = false;
            this.dtStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDate.Location = new System.Drawing.Point(279, 26);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Size = new System.Drawing.Size(120, 20);
            this.dtStartDate.TabIndex = 42;
            this.dtStartDate.TabStop = false;
            this.dtStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(201, 26);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(75, 18);
            this.radLabel1.TabIndex = 41;
            this.radLabel1.Text = "查询日期段：";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(59, 26);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(125, 20);
            this.ddtStore.TabIndex = 40;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 39;
            this.radLabel3.Text = "门店：";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnSearch.Location = new System.Drawing.Point(617, 26);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSearch.Size = new System.Drawing.Size(86, 20);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "查询";
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = false;
            this.labelCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCount.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelCount.Location = new System.Drawing.Point(0, 588);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(959, 25);
            this.labelCount.TabIndex = 8;
            this.labelCount.Text = "合计：";
            this.labelCount.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rgvCash
            // 
            this.rgvCash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgvCash.Location = new System.Drawing.Point(0, 86);
            // 
            // rgvCash
            // 
            this.rgvCash.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.rgvCash.MasterTemplate.AllowAddNewRow = false;
            this.rgvCash.MasterTemplate.AllowDeleteRow = false;
            this.rgvCash.MasterTemplate.AllowEditRow = false;
            this.rgvCash.MasterTemplate.AutoGenerateColumns = false;
            gridViewComboBoxColumn1.FieldName = "store_id";
            gridViewComboBoxColumn1.HeaderText = "门店";
            gridViewComboBoxColumn1.Name = "col_store";
            gridViewComboBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn1.Width = 70;
            gridViewDecimalColumn1.FieldName = "imprest_amt";
            gridViewDecimalColumn1.HeaderText = "备用金(元)";
            gridViewDecimalColumn1.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            gridViewDecimalColumn1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn1.Name = "col_imprest";
            gridViewDecimalColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDecimalColumn1.Width = 80;
            gridViewComboBoxColumn2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            gridViewComboBoxColumn2.FieldName = "lottery_class";
            gridViewComboBoxColumn2.HeaderText = "彩票种类";
            gridViewComboBoxColumn2.Name = "col_class";
            gridViewComboBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn2.Width = 60;
            gridViewComboBoxColumn3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            gridViewComboBoxColumn3.FieldName = "lottery_type";
            gridViewComboBoxColumn3.HeaderText = "彩票类型";
            gridViewComboBoxColumn3.Name = "col_type";
            gridViewComboBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn3.Width = 60;
            gridViewTextBoxColumn1.FieldName = "creater";
            gridViewTextBoxColumn1.HeaderText = "制单人";
            gridViewTextBoxColumn1.Name = "col_creater";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 60;
            gridViewDateTimeColumn1.CustomFormat = "";
            gridViewDateTimeColumn1.FieldName = "create_date";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn1.HeaderText = "制单日期";
            gridViewDateTimeColumn1.Name = "col_createdate";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 80;
            gridViewTextBoxColumn2.FieldName = "updater";
            gridViewTextBoxColumn2.HeaderText = "修改人";
            gridViewTextBoxColumn2.Name = "col_updater";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 60;
            gridViewDateTimeColumn2.CustomFormat = "";
            gridViewDateTimeColumn2.FieldName = "update_date";
            gridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn2.HeaderText = "修改日期";
            gridViewDateTimeColumn2.Name = "col_updatedate";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "auditer";
            gridViewTextBoxColumn3.HeaderText = "审核人";
            gridViewTextBoxColumn3.Name = "col_auditer";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 60;
            gridViewDateTimeColumn3.CustomFormat = "";
            gridViewDateTimeColumn3.FieldName = "audit_date";
            gridViewDateTimeColumn3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn3.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn3.HeaderText = "审核日期";
            gridViewDateTimeColumn3.Name = "col_auditdate";
            gridViewDateTimeColumn3.ReadOnly = true;
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "effectiver";
            gridViewTextBoxColumn4.HeaderText = "发送人";
            gridViewTextBoxColumn4.Name = "col_effectiver";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 60;
            gridViewDateTimeColumn4.CustomFormat = "";
            gridViewDateTimeColumn4.FieldName = "effective_date";
            gridViewDateTimeColumn4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn4.FormatString = "{0:yyyy-MM-dd}";
            gridViewDateTimeColumn4.HeaderText = "生效日期";
            gridViewDateTimeColumn4.Name = "col_effectivedate";
            gridViewDateTimeColumn4.ReadOnly = true;
            gridViewDateTimeColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn4.Width = 80;
            gridViewComboBoxColumn4.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            gridViewComboBoxColumn4.FieldName = "entity_status";
            gridViewComboBoxColumn4.HeaderText = "业务状态";
            gridViewComboBoxColumn4.Name = "col_entitystatus";
            gridViewComboBoxColumn4.ReadOnly = true;
            gridViewComboBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn4.Width = 60;
            gridViewComboBoxColumn5.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            gridViewComboBoxColumn5.FieldName = "send_status";
            gridViewComboBoxColumn5.HeaderText = "通讯状态";
            gridViewComboBoxColumn5.Name = "col_status";
            gridViewComboBoxColumn5.ReadOnly = true;
            gridViewComboBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewComboBoxColumn5.Width = 60;
            this.rgvCash.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewDecimalColumn1,
            gridViewComboBoxColumn2,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn2,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn3,
            gridViewDateTimeColumn3,
            gridViewTextBoxColumn4,
            gridViewDateTimeColumn4,
            gridViewComboBoxColumn4,
            gridViewComboBoxColumn5});
            this.rgvCash.Name = "rgvCash";
            this.rgvCash.ShowGroupPanel = false;
            this.rgvCash.Size = new System.Drawing.Size(959, 502);
            this.rgvCash.TabIndex = 9;
            this.rgvCash.Text = "radGridView1";
            // 
            // UC_LotteryCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rgvCash);
            this.Controls.Add(this.labelCount);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_LotteryCash";
            this.Size = new System.Drawing.Size(959, 613);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvCash.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvCash)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnModify;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private Telerik.WinControls.UI.CommandBarButton btnSave;
        private Telerik.WinControls.UI.CommandBarButton btnAdudit;
        private Telerik.WinControls.UI.CommandBarButton btnSend;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDateTimePicker dtEndDate;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker dtStartDate;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Telerik.WinControls.UI.RadLabel labelCount;
        private Telerik.WinControls.UI.RadGridView rgvCash;
    }
}
