﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_stock : UserControl
    {
        public UC_ad_asset_stock()
        {
            InitializeComponent();
            this.Load += UC_ad_asset_stock_Load;
            this.btnSearch.Click += (s, e) => { this.Search(); };
        }

        void UC_ad_asset_stock_Load(object sender, EventArgs e)
        {
            Search();
        }
        private void Search()
        {
            this.dgasset_stock.GridView.DataSource = assetsendService.Searchstock(tbxName.Text.Trim());
        }
    }
}
