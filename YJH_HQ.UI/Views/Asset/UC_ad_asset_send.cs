﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_send : UserControl
    {
        public UC_ad_asset_send()
        {
            InitializeComponent();
            LoadPermission();
            this.Load += UC_ad_asset_send_Load;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnModify.Click += (s, e) => { this.Edit(); };
            this.dgasset_send.GridView.DoubleClick += GridView_DoubleClick;
            this.btnDelete.Click += btnDelete_Click;
        }

        void UC_ad_asset_send_Load(object sender, EventArgs e)
        {
            
            Search();
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgasset_send.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要删除的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    assetsendService.Delete(Guid.Parse(this.dgasset_send.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "删除成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    Search();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "删除失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (dgasset_send.GridView.SelectedRows.Count == 0)
            {
                return;
            }
            UI_ad_asset_send ui = new UI_ad_asset_send();
            ui.dgList = dgasset_send;
            ui.IsNew = false;
            ui.entity = assetsendService.GetByID(Guid.Parse(dgasset_send.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }

        private void Search()
        {
            dgasset_send.GridView.DataSource = assetsendService.Search(tbxName.Text.Trim());
        }

        private void New()
        {
            UI_ad_asset_send ui = new UI_ad_asset_send();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.IsNew = true;
            ui.dgList = this.dgasset_send;
            ui.ShowDialog();
        }

        private void Edit()
        {
            if (dgasset_send.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要编辑的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_ad_asset_send ui = new UI_ad_asset_send();
            ui.dgList = dgasset_send;
            ui.IsNew = false;
            ui.entity = assetsendService.GetByID(Guid.Parse(dgasset_send.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
        }
    }
}
