﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_class : Telerik.WinControls.UI.RadForm
    {
        public string parent = "";
        public bool IsNew = true;
        public Guid id = Guid.Empty;
        public bool IsChild = true;
        RadTreeNode currentnode = new RadTreeNode();
        UC_ad_asset_class baclass = new UC_ad_asset_class();
        public UI_ad_asset_class(UC_ad_asset_class BC)
        {
            InitializeComponent();
            baclass = BC;
            this.Load += UI_ad_asset_class_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                YJH.Entities.ad_asset_class entity = YJH.Services.assetService.GetassetclassByID(id);
                if (IsNew)
                {
                    YJH.Entities.ad_asset_class newentity = new YJH.Entities.ad_asset_class();
                    newentity.Company_id = Guid.Parse("2B474195-706A-4E0D-BDF4-FAE6A4D88687");
                    newentity.class_id = tbxclass_id.Text.Trim();
                    newentity.class_name = tbxclass_name.Text.Trim();
                    newentity.status = int.Parse(ddlstatus.SelectedValue.ToString());

                    if (cbxitem_status.Checked)
                        newentity.IsDisabled = true;
                    else
                        newentity.IsDisabled = false;
                    if (IsChild)
                    {
                        newentity.parent_idID = entity.Instance.ID;
                    }
                    else
                    {
                        newentity.parent_idID = entity.parent_idID;
                    }
                    newentity.creater = Globle.CurrentEmployee.Base.Name;
                    bool result = YJH.Services.assetService.IsHasClass_id(newentity);
                    if (result)
                    {
                        RadMessageBox.Show("该类别号已存在，请勿重复添加", "提示");
                        return;
                    }
                    YJH.Services.assetService.Saveassetclass(newentity);
                    RadMessageBox.Show("保存成功", "提示");
                    currentnode.Text = newentity.class_name;
                    currentnode.Value = newentity.Instance.ID;
                    currentnode.Tag = newentity.class_id;
                    if (IsChild)
                    {
                        currentnode.Name = newentity.parent_idID.ToString();
                        baclass.isChild = true;
                        baclass.RefeshTree(currentnode);
                    }
                    else
                    {
                        currentnode.Name = newentity.parent_idID.ToString();
                        baclass.isChild = false;
                        baclass.RefeshTree(currentnode);
                    }
                    this.Close();
                }
                else
                {
                    if (id != Guid.Empty)
                    {
                        entity.class_id = tbxclass_id.Text.Trim();
                        entity.class_name = tbxclass_name.Text.Trim();
                        entity.status = int.Parse(ddlstatus.SelectedValue.ToString());
                        if (cbxitem_status.Checked)
                            entity.IsDisabled = true;
                        else
                            entity.IsDisabled = false;

                        bool result = YJH.Services.assetService.IsHasClass_id(entity);
                        if (result)
                        {
                            RadMessageBox.Show("该类别号已存在，请勿重复添加", "提示");
                            return;
                        }
                        YJH.Services.assetService.Saveassetclass(entity);
                        RadMessageBox.Show("保存成功", "提示");
                        currentnode.Text = entity.class_name;
                        currentnode.Value = entity.Instance.ID;
                        currentnode.Tag = entity.class_id;
                        currentnode.Name = entity.parent_idID.ToString();
                        baclass.selectNode.Value = currentnode.Value;
                        baclass.selectNode.Text = currentnode.Text;
                        baclass.selectNode.Tag = currentnode.Tag;
                        baclass.selectNode.Name = currentnode.Name;
                        this.Close();
                    }
                    else
                    {
                        RadMessageBox.Show("保存失败", "提示");
                        return;
                    }
                }
            }
            catch (Exception)
            {
                RadMessageBox.Show("保存失败", "提示");
                return;
            }
        }

        void UI_ad_asset_class_Load(object sender, EventArgs e)
        {
            RadListDataItem l1 = new RadListDataItem();
            l1.Text = "固定资产";
            l1.Value = 10;
            ddlstatus.Items.Add(l1);
            RadListDataItem l2 = new RadListDataItem();
            l2.Text = "行政资产";
            l2.Value = 20;
            ddlstatus.Items.Add(l2);
            if (IsNew)
            {
                tbxclass_id.Text = "";
                tbxclass_name.Text = "";
                ddlstatus.SelectedIndex = 1;
                cbxitem_status.Checked = true;
            }
            else
            {
                if (id != Guid.Empty)
                {
                    YJH.Entities.ad_asset_class entity = YJH.Services.assetService.GetassetclassByID(id);
                    if (entity != null)
                    {
                        tbxclass_id.Text = entity.class_id;
                        tbxclass_name.Text = entity.class_name;
                        ddlstatus.SelectedValue = entity.status;
                        if (entity.IsDisabled == true)
                            cbxitem_status.Checked = true;
                        else
                            cbxitem_status.Checked = false;
                    }
                }
            }
        }
    }
}
