﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Data;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_send : Telerik.WinControls.UI.RadForm
    {
        public bool IsNew = false;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        public YJH.Entities.ad_asset_send entity;
        public UI_ad_asset_send()
        {
            InitializeComponent();
            this.Load += UI_ad_asset_send_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (VaileDate())
                {
                    FulshDate();
                    if(assetsendService.IsSendRece(entity.item_id,entity.send_qty) == false)
                    {
                        RadMessageBox.Show(this, "库存不足，请先添加库存后再操作", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    var entityState = entity.Instance.PersistentState;
                    assetsendService.Save(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        void UI_ad_asset_send_Load(object sender, EventArgs e)
        {
            cbxitem_id.AutoSizeDropDownToBestFit = true;
            cbxitem_id.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Append;

            cbxitem_id.DisplayMember = "item_name";
            cbxitem_id.ValueMember = "item_id";
            cbxitem_id.DataSource = YJH.Services.assetreceService.GetItem_id();
            FilterDescriptor descriptor = new FilterDescriptor(this.cbxitem_id.DisplayMember, FilterOperator.StartsWith, string.Empty);
            cbxitem_id.EditorControl.FilterDescriptors.Add(descriptor);
            cbxitem_id.AutoCompleteMode = AutoCompleteMode.Append;
            ddlgeter.DisplayMember = "Name";
            ddlgeter.ValueMember = "ID";
            ddlgeter.DataSource = assetsendService.GetEmp();
            ddlget_dept.DisplayMember = "Name";
            ddlget_dept.ValueMember = "ID";
            ddlget_dept.DataSource = assetService.GetWorkGroup();
            if (IsNew)
                New();
            else
                BinData();
        }
        private void New()
        {
            entity = new YJH.Entities.ad_asset_send();
            tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            tbxcreate_date.Text = YJH.Services.OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            tbxforWhat.Text = "";
            tbxsend_id.Text = "";
            tbxsend_date.Text = "";
            tbxsend_qty.Text = "0";
            ddlget_dept.SelectedIndex = -1;
            ddlgeter.SelectedIndex = -1;
            cbxitem_id.SelectedIndex = -1;
            entity.send_date = YJH.Services.OrgUnitService.GetDate();
        }

        private void BinData()
        {
            tbxcreate_date.Text = entity.create_date.ToString("yyyy-MM-dd HH:mm");
            tbxcreater.Text = entity.creater;
            tbxforWhat.Text = entity.forWhat;
            tbxsend_date.Text = entity.send_date.ToString("yyyy-MM-dd HH:mm");
            tbxsend_id.Text = entity.send_id;
            tbxsend_qty.Text = entity.send_qty.ToString("f2");
            ddlgeter.Text = entity.geter;
            ddlget_dept.SelectedValue = entity.get_dept;
            cbxitem_id.SelectedValue = entity.item_id;
        }

        private void FulshDate()
        {
            entity.Company_id = Guid.Parse("2B474195-706A-4E0D-BDF4-FAE6A4D88687");
            entity.create_date = DateTime.Parse(tbxcreate_date.Text);
            entity.creater = tbxcreater.Text;
            entity.item_id = cbxitem_id.SelectedValue.ToString();
            entity.forWhat = tbxforWhat.Text;
            entity.get_dept = Guid.Parse(ddlget_dept.SelectedValue.ToString());
            entity.geter = ddlgeter.Text;
            entity.send_qty = Decimal.Parse(tbxsend_qty.Text);
            if (IsNew)
            {
                string send_id = YJH.Services.assetsendService.Getsend_id(YJH.Services.OrgUnitService.GetDate().Date);
                if (send_id == "" || send_id == null)
                    entity.send_id = YJH.Services.OrgUnitService.GetDate().ToString("yyyyMMdd") + "001";
                else
                    entity.send_id = send_id.Substring(0, 8) + (int.Parse(send_id.Substring(8, 3)) + 1).ToString().PadLeft(3, '0');
            }
        }

        private bool VaileDate()
        {
            if (string.IsNullOrWhiteSpace(this.cbxitem_id.Text))
            {
                RadMessageBox.Show("品项不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxsend_qty.Text))
            {
                RadMessageBox.Show("数量不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (decimal.Parse(tbxsend_qty.Text) <= 0)
            {
                RadMessageBox.Show("数量必须大于零", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlgeter.Text))
            {
                RadMessageBox.Show("领用人不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlget_dept.Text))
            {
                RadMessageBox.Show("领用部门不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_asset_send entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("send_id", entity.send_id);
                data.Add("item_id", entity.item_id);
                data.Add("send_qty", entity.send_qty);
                data.Add("send_date", entity.send_date);
                data.Add("geter", entity.geter);
                data.Add("get_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.get_dept).Name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("send_id", entity.send_id);
                data.Add("item_id", entity.item_id);
                data.Add("send_qty", entity.send_qty);
                data.Add("send_date", entity.send_date);
                data.Add("geter", entity.geter);
                data.Add("get_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.get_dept).Name);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }
    }
}
