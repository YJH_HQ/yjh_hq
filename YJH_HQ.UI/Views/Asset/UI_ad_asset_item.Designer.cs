﻿namespace YJH_HQ.UI.Views.Asset
{
    partial class UI_ad_asset_item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxitem_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxitem_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ddlclass_id = new Telerik.WinControls.UI.RadDropDownList();
            this.cbxIsDisabled = new Telerik.WinControls.UI.RadCheckBox();
            this.tbxspec = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlclass_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxIsDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxspec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(383, 30);
            this.radCommandBar2.TabIndex = 5;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxitem_name
            // 
            this.tbxitem_name.Location = new System.Drawing.Point(82, 89);
            this.tbxitem_name.Name = "tbxitem_name";
            this.tbxitem_name.Size = new System.Drawing.Size(265, 20);
            this.tbxitem_name.TabIndex = 8;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(34, 91);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(42, 18);
            this.radLabel30.TabIndex = 9;
            this.radLabel30.Text = "品名：";
            // 
            // tbxitem_id
            // 
            this.tbxitem_id.Location = new System.Drawing.Point(82, 65);
            this.tbxitem_id.MaxLength = 11;
            this.tbxitem_id.Name = "tbxitem_id";
            this.tbxitem_id.Size = new System.Drawing.Size(106, 20);
            this.tbxitem_id.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(34, 67);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "编码：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(194, 67);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "类别：";
            // 
            // ddlclass_id
            // 
            this.ddlclass_id.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlclass_id.Location = new System.Drawing.Point(242, 65);
            this.ddlclass_id.Name = "ddlclass_id";
            this.ddlclass_id.Size = new System.Drawing.Size(105, 20);
            this.ddlclass_id.TabIndex = 12;
            // 
            // cbxIsDisabled
            // 
            this.cbxIsDisabled.Location = new System.Drawing.Point(82, 139);
            this.cbxIsDisabled.Name = "cbxIsDisabled";
            this.cbxIsDisabled.Size = new System.Drawing.Size(68, 18);
            this.cbxIsDisabled.TabIndex = 13;
            this.cbxIsDisabled.Text = "是否启用";
            // 
            // tbxspec
            // 
            this.tbxspec.Location = new System.Drawing.Point(82, 113);
            this.tbxspec.Name = "tbxspec";
            this.tbxspec.Size = new System.Drawing.Size(265, 20);
            this.tbxspec.TabIndex = 16;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(34, 115);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "规格：";
            // 
            // UI_ad_asset_item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 196);
            this.Controls.Add(this.tbxspec);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.cbxIsDisabled);
            this.Controls.Add(this.ddlclass_id);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbxitem_name);
            this.Controls.Add(this.radLabel30);
            this.Controls.Add(this.tbxitem_id);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radCommandBar2);
            this.Name = "UI_ad_asset_item";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "资产品项信息";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlclass_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxIsDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxspec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxitem_name;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox tbxitem_id;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlclass_id;
        private Telerik.WinControls.UI.RadCheckBox cbxIsDisabled;
        private Telerik.WinControls.UI.RadTextBox tbxspec;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}