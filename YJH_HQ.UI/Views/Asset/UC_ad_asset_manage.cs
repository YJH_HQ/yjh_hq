﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_manage : UserControl
    {
        public UC_ad_asset_manage()
        {
            InitializeComponent();
            LoadPermission();
            this.btnNew.Click += btnNew_Click;
            this.btnSearch.Click += (s,e) => {this.Search();};
            this.Load += UC_ad_asset_manage_Load;
            this.btnModify.Click += (s, e) => { this.Edit(); };
            this.dgContract.GridView.DoubleClick += (s, e) => { this.Edit(); };
            this.dgContract.GridView.CellFormatting += GridView_CellFormatting;
            this.btnAudit.Click += btnAudit_Click;
        }

        void btnAudit_Click(object sender, EventArgs e)
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            if(dgContract.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "10")
            {
                RadMessageBox.Show(this, "只允许调出状态为正式的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_ad_asset_manageout ui = new UI_ad_asset_manageout();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.dgList = dgContract;
            ui.entity = assetService.GetAssetMangerByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.ShowDialog();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.Row.Cells["clstatus"].Value.ToString() == "10")
                e.Row.Cells["cl_status"].Value = "正式";
            else if(e.Row.Cells["clstatus"].Value.ToString() == "20")
                e.Row.Cells["cl_status"].Value = "调出";
            else if (e.Row.Cells["clstatus"].Value.ToString() == "30")
                e.Row.Cells["cl_status"].Value = "报废";
        }

        void UC_ad_asset_manage_Load(object sender, EventArgs e)
        {
            ddlstatus.SelectedIndex = 0;
            Search();
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            UI_ad_asset_manage ui = new UI_ad_asset_manage();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.IsNew = true;
            ui.dgList = dgContract;
            ui.ShowDialog();
        }

        private void Search()
        {
            dgContract.GridView.DataSource = assetService.GetAssetManger(tbxName.Text.Trim(),int.Parse(ddlstatus.SelectedItems[0].Tag.ToString()));
        }

        private void Edit()
        {
            if (dgContract.GridView.SelectedRows.Count == 0)
                return;
            UI_ad_asset_manage ui = new UI_ad_asset_manage();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.IsNew = false;
            ui.dgList = dgContract;
            ui.entity = assetService.GetAssetMangerByID(Guid.Parse(dgContract.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.ShowDialog();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
            this.btnAudit.Enabled = funcs.Exists(t => t.Name == "调出");
        }
    }
}
