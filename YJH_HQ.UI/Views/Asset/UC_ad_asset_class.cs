﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_class : UserControl
    {
        public bool isChild = false;
        public RadTreeNode selectNode = null;
        private List<YJH.Entities.ad_asset_class> list1 = new List<YJH.Entities.ad_asset_class>();
        public UC_ad_asset_class()
        {
            InitializeComponent();
            this.Load += UC_ad_asset_class_Load;
            this.btnNew.Click += btnNew_Click;
            this.btnNewChild.Click += btnNewChild_Click;
            this.btnEdit.Click += (s, e) => { OnEdit(); };
            this.dgassetclass.DoubleClick += BaClassTree_DoubleClick;
            this.btnDelete.Click += btnDelete_Click;
            this.dgassetclass.SelectedNodeChanged += BaClassTree_SelectedNodeChanged;
        }

        void UC_ad_asset_class_Load(object sender, EventArgs e)
        {
            LoadPermission();
            BuildTree();
        }

        #region === 事件 ===
        void BaClassTree_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            selectNode = e.Node;
            YJH.Entities.ad_asset_class entity = YJH.Services.assetService.GetassetclassByID(Guid.Parse(selectNode.Value.ToString()));
        }
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgassetclass.SelectedNodes.Count < 1)
            {
                RadMessageBox.Show("请选择需要删除的项", "提示");
                return;
            }
            try
            {
                YJH.Entities.ad_asset_class entity = YJH.Services.assetService.GetassetclassByID(Guid.Parse(selectNode.Value.ToString()));

                if (RadMessageBox.Show("确认删除此项及以下所有类别吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    YJH.Services.assetService.DeleteBa_Class(entity);
                    RadMessageBox.Show("删除成功", "提示");
                    selectNode.Remove();
                }
            }
            catch (Exception)
            {
                RadMessageBox.Show("删除失败", "提示");
            }
            
        }

        void BaClassTree_DoubleClick(object sender, EventArgs e)
        {
            OnEdit();
        }

        void OnEdit()
        {
            if(dgassetclass.SelectedNodes.Count < 1)
            {
                RadMessageBox.Show("请选择需要编辑的项", "提示");
                return;
            }
            UI_ad_asset_class bc = new UI_ad_asset_class(this);
            bc.id = Guid.Parse(selectNode.Value.ToString());
            bc.IsNew = false;
            bc.ShowDialog();
        }
        //新建子级
        void btnNewChild_Click(object sender, EventArgs e)
        {
            if (dgassetclass.SelectedNodes.Count < 1)
            {
                RadMessageBox.Show("请先选择一个节点", "提示");
                return;
            }
            UI_ad_asset_class bc = new UI_ad_asset_class(this);
            bc.parent = selectNode.Tag.ToString();
            bc.id = Guid.Parse(selectNode.Value.ToString());
            bc.IsNew = true;
            bc.IsChild = true;
            bc.ShowDialog();
        }

        //新建同级
        void btnNew_Click(object sender, EventArgs e)
        {
            if (dgassetclass.SelectedNodes.Count < 1)
            {
                RadMessageBox.Show("请先选择一个节点", "提示");
                return;
            }
            UI_ad_asset_class bc = new UI_ad_asset_class(this);
            bc.id = Guid.Parse(selectNode.Value.ToString());
            if (selectNode.Parent != null)
                bc.parent = selectNode.Parent.Tag.ToString();
            else
                bc.parent = "0";
            bc.IsNew = true;
            bc.IsChild = false;
            bc.ShowDialog();
        }

        void UC_Ba_Class_Load(object sender, EventArgs e)
        {
            LoadPermission();
            BuildTree();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnEdit.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
        }
        //构造树
        public void BuildTree()
        {
            list1 = YJH.Services.assetService.Getassetclass();
            foreach (var item in list1.Where(t => t.parent_idID == null).OrderBy(t => t.class_id))
                LoopAddTreeNode(item, null);
        }

        private void LoopAddTreeNode(YJH.Entities.ad_asset_class entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Text = entity.class_name;
            node.Value = entity.Instance.ID;
            node.Tag = entity.class_id;
            node.Name = entity.parent_idID.ToString();
            if (ParentNode == null)
                this.dgassetclass.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in list1.Where(t => t.parent_idID == entity.Instance.ID).OrderBy(t => t.class_name))
                LoopAddTreeNode(item, node);
        }
       
        /// <summary>
        /// 动态添加节点
        /// </summary>
        /// <remarks>建立人：LJW 建立日期：2015/7/30 最后修改日期：2015/7/30</remarks>
        public void RefeshTree(RadTreeNode newNode)
        {
            if (isChild)
            {
                selectNode.Nodes.Add(newNode);
                selectNode.Expand();
            }
            else
            {
                if (newNode.Name == "1")
                {
                    dgassetclass.Nodes.Add(newNode);
                }
                else
                {
                    selectNode.Parent.Nodes.Add(newNode);
                }
            }
        }
        #endregion
    }
}
