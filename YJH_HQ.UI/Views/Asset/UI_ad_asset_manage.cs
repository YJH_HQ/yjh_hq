﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_manage : Telerik.WinControls.UI.RadForm
    {
        public YJH.Entities.ad_asset_manage entity;
        public bool IsNew = false;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private DataTable dt;
        public UI_ad_asset_manage()
        {
            InitializeComponent();
            this.Load += UI_ad_asset_manage_Load;
            this.btSave.Click += btSave_Click;
            this.ddlclass_id.SelectedValueChanged += ddlclass_id_SelectedValueChanged;
            this.ddlspec.SelectedValueChanged += ddlspec_SelectedValueChanged;
        }

        void ddlspec_SelectedValueChanged(object sender, EventArgs e)
        {
            if(ddlspec.SelectedValue != null && ddlspec.Text != "")
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if(dr["class_id"].ToString() == ddlclass_id.SelectedValue.ToString() && dr["spec"].ToString() == ddlspec.Text.Trim())
                    {
                        tbxitem_no.Text = dr["item_no"].ToString();
                        tbxprice.Text = dr["price"].ToString();
                        tbxsup_name.Text = dr["sup_name"].ToString();
                    }
                }
            }
            else
            {
                tbxitem_no.Text = "";
                tbxprice.Text = "";
                tbxsup_name.Text = "";
            }
        }

        void ddlclass_id_SelectedValueChanged(object sender, EventArgs e)
        {
            if(ddlclass_id.SelectedValue != null)
            {
                ddlspec.Enabled = true;
                tbxprice.Enabled = true;
                tbxitem_no.Enabled = true;
                tbxsup_name.Enabled = true;
                try
                {
                    DataView dv = new DataView(assetService.Getspec(ddlclass_id.SelectedValue.ToString()));
                    dt = dv.ToTable(true, "class_id", "spec", "item_no", "price", "sup_name");
                    ddlspec.ValueMember = "class_id";
                    ddlspec.DisplayMember = "spec";
                    ddlspec.DataSource = dt;
                    ddlspec.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    ddlspec.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
                
            }
            else
            {
                ddlspec.Enabled = false;
                ddlspec.Text = "";
                tbxprice.Enabled = false;
                tbxprice.Text = "";
                tbxitem_no.Enabled = false;
                tbxitem_no.Text = "";
                tbxsup_name.Enabled = false;
                tbxsup_name.Text = "";
            }
        }


        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (VaileDate())
                {
                    FulshDate();
                    var entityState = entity.Instance.PersistentState;
                    assetService.SaveAssetManger(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_asset_manage entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("classname", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("spec", entity.spec);
                data.Add("price", entity.price);
                data.Add("sup_name", entity.sup_name);
                data.Add("item_no", entity.item_no);
                data.Add("reg_date", entity.reg_date);
                data.Add("keep_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.keep_dept).Name);
                data.Add("keeper", entity.keeper);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date.ToString("yyyy-MM-dd HH:mm"));
                data.Add("status", entity.status);
                //if (entity.status == 10)
                //    data.Add("status", "正式");
                //else if(entity.status == 20)
                //    data.Add("status", "调出");
                //else if (entity.status == 30)
                //    data.Add("status", "报废");
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("classname", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("spec", entity.spec);
                data.Add("price", entity.price);
                data.Add("sup_name", entity.sup_name);
                data.Add("item_no", entity.item_no);
                data.Add("reg_date", entity.reg_date);
                data.Add("keep_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.keep_dept).Name);
                data.Add("keeper", entity.keeper);
                if(entity.parent_id != null)
                    data.Add("parent_id", sys.Services.WorkGroupService.GetWorkGroupByID(Guid.Parse(entity.parent_id.ToString())).Name);
                if(entity.destroy_date != null)
                    data.Add("destroy_date", entity.destroy_date.ToString());
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date.ToString("yyyy-MM-dd HH:mm"));
                data.Add("status", entity.status);
                //if (entity.status == 10)
                //    data.Add("status", "正式");
                //else if (entity.status == 20)
                //    data.Add("status", "调出");
                //else if (entity.status == 30)
                //    data.Add("status", "报废");
                this.dgList.RefreshSelectedRow(data);
            }
        }

        void UI_ad_asset_manage_Load(object sender, EventArgs e)
        {
            ddlclass_id.ValueMember = "class_id";
            ddlclass_id.DisplayMember = "class_name";
            ddlclass_id.DataSource = assetService.GetEnableassetclass();
            ddlkeeper.DisplayMember = "Name";
            ddlkeeper.ValueMember = "ID";
            ddlkeeper.DataSource = bm_store_expandService.GetEmp();
            ddlkeep_dept.DisplayMember = "Name";
            ddlkeep_dept.ValueMember = "ID";
            ddlkeep_dept.DataSource = assetService.GetWorkGroup();
            if (IsNew)
                New();
            else
                BindDate();
        }

        private void New()
        {
            entity = new YJH.Entities.ad_asset_manage();
            ddlclass_id.SelectedIndex = -1;
            ddlspec.Text = "";
            tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            tbxcreate_date.Text = OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            tbxitem_no.Text = "";
            ddlkeep_dept.SelectedIndex = -1;
            tbxparent_id.Text = "";
            tbxprice.Text = "0";
            tbxreg_date.Text = OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            tbxstatus.Text = "正式";
            tbxsup_name.Text = "";
            dtpdestroy_date.SetToNullValue();
            ddlkeeper.SelectedIndex = -1;
        }

        private void BindDate()
        {
            tbxcreate_date.Text = entity.create_date.ToString("yyyy-MM-dd HH:mm");
            tbxcreater.Text = entity.creater;
            tbxitem_no.Text = entity.item_no;
            if(entity.parent_id != null)
                tbxparent_id.Text = sys.Services.WorkGroupService.GetWorkGroupByID(Guid.Parse(entity.parent_id.ToString())).Name;
            tbxprice.Text = entity.price.ToString("f2");
            tbxreg_date.Text = entity.reg_date.ToString("yyyy-MM-dd HH:mm");
            if (entity.status == 10)
                tbxstatus.Text = "正式";
            else if(entity.status == 20)
                tbxstatus.Text = "调出";
            else if (entity.status == 30)
                tbxstatus.Text = "报废";
            tbxsup_name.Text = entity.sup_name;
            ddlclass_id.SelectedValue = entity.class_id;
            ddlkeep_dept.SelectedValue = entity.keep_dept;
            ddlkeeper.Text = entity.keeper;
            ddlspec.Text = entity.spec;
            if (entity.destroy_date != null)
                dtpdestroy_date.Value = entity.destroy_date.Value;
            if(entity.status > 10)
            {
                ddlspec.Enabled = false;
                ddlkeeper.Enabled = false;
                ddlkeep_dept.Enabled = false;
                ddlclass_id.Enabled = false;
                tbxitem_no.Enabled = false;
                tbxprice.Enabled = false;
                tbxsup_name.Enabled = false;
            }
            else
            {
                dtpdestroy_date.Enabled = true;
            }
        }

        private void FulshDate()
        {
            entity.class_id = ddlclass_id.SelectedValue.ToString();
            entity.Company_id = Guid.Parse("2B474195-706A-4E0D-BDF4-FAE6A4D88687");
            entity.spec = ddlspec.Text;
            entity.create_date = DateTime.Parse(tbxcreate_date.Text);
            entity.creater = tbxcreater.Text;
            if (dtpdestroy_date.Value != null && dtpdestroy_date.Value != System.DateTime.MinValue)
                entity.destroy_date = dtpdestroy_date.Value;
            entity.item_no = tbxitem_no.Text;
            entity.keep_dept = Guid.Parse(ddlkeep_dept.SelectedValue.ToString());
            entity.keeper = ddlkeeper.Text;
            entity.price = Decimal.Parse(tbxprice.Text);
            entity.reg_date = DateTime.Parse(tbxreg_date.Text);
            entity.sup_name = tbxsup_name.Text;
            if (tbxstatus.Text == "正式")
                entity.status = 10;
            else if (tbxstatus.Text == "调出")
                entity.status = 20;
            else
                entity.status = 30;
            if(entity.status == 10)
            {
                if (entity.destroy_date != null)
                    entity.status = 30;
            }
        }
        private bool VaileDate()
        {
            if (string.IsNullOrWhiteSpace(this.ddlclass_id.Text))
            {
                RadMessageBox.Show("请选择类别", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlspec.Text))
            {
                RadMessageBox.Show("规格不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxitem_no.Text))
            {
                RadMessageBox.Show("编码不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxprice.Text))
            {
                RadMessageBox.Show("价值不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxsup_name.Text))
            {
                RadMessageBox.Show("供应商不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxreg_date.Text))
            {
                RadMessageBox.Show("注册日期不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlkeeper.Text))
            {
                RadMessageBox.Show("请选择保管人", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlkeep_dept.Text))
            {
                RadMessageBox.Show("保管部门不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
