﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_rece : UserControl
    {
        private YJH.Entities.ad_asset_rece entity;
        public UC_ad_asset_rece()
        {
            InitializeComponent();
            LoadPermission();
            this.Load += UC_ad_asset_rece_Load;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnModify.Click += (s, e) => { this.Edit(); };
            this.dgasset_rece.GridView.DoubleClick += GridView_DoubleClick;
            this.btnDelete.Click += btnDelete_Click;
            this.btnAudit.Click += btnAudit_Click;
        }

        void btnAudit_Click(object sender, EventArgs e)
        {
            if (dgasset_rece.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要入库的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (dgasset_rece.GridView.SelectedRows[0].Cells["clstatus"].Value.ToString() != "新建")
            {
                RadMessageBox.Show(this, "只允许入库状态为新建的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认入库吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    assetreceService.Audit(Guid.Parse(dgasset_rece.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "入库成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    Search();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "入库失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgasset_rece.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要删除的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    assetreceService.Delete(Guid.Parse(this.dgasset_rece.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "删除成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    Search();
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "删除失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (dgasset_rece.GridView.SelectedRows.Count == 0)
            {
                return;
            }
            UI_ad_asset_rece ui = new UI_ad_asset_rece();
            ui.dgList = dgasset_rece;
            ui.IsNew = false;
            ui.entity = assetreceService.GetByID(Guid.Parse(dgasset_rece.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }

        void UC_ad_asset_rece_Load(object sender, EventArgs e)
        {
            ddlstatus.SelectedIndex = 0;
            Search();
        }

        private void Search()
        {
            dgasset_rece.GridView.DataSource = YJH.Services.assetreceService.Search(tbxName.Text.Trim(), int.Parse(ddlstatus.SelectedItems[0].Tag.ToString()));
        }

        private void New()
        {
            UI_ad_asset_rece ui = new UI_ad_asset_rece();
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.entity = entity;
            ui.IsNew = true;
            ui.dgList = this.dgasset_rece;
            ui.ShowDialog();
        }

        private void Edit()
        {
            if (dgasset_rece.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要编辑的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_ad_asset_rece ui = new UI_ad_asset_rece();
            ui.dgList = dgasset_rece;
            ui.IsNew = false;
            ui.entity = assetreceService.GetByID(Guid.Parse(dgasset_rece.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
            this.btnAudit.Enabled = funcs.Exists(t => t.Name == "入库");
            this.btnCancel.Enabled = funcs.Exists(t => t.Name == "取消");
        }
    }
}
