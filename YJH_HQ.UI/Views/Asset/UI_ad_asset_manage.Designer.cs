﻿namespace YJH_HQ.UI.Views.Asset
{
    partial class UI_ad_asset_manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxprice = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxitem_no = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ddlclass_id = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddlspec = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxsup_name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbxreg_date = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.ddlkeeper = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxparent_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.dtpdestroy_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbxcreater = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.ddlkeep_dept = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlclass_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlspec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsup_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxreg_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlkeeper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxparent_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpdestroy_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlkeep_dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(424, 30);
            this.radCommandBar2.TabIndex = 5;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxprice
            // 
            this.tbxprice.Enabled = false;
            this.tbxprice.Location = new System.Drawing.Point(278, 84);
            this.tbxprice.Name = "tbxprice";
            this.tbxprice.Size = new System.Drawing.Size(106, 20);
            this.tbxprice.TabIndex = 8;
            this.tbxprice.Text = "0";
            this.tbxprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(230, 86);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(42, 18);
            this.radLabel30.TabIndex = 9;
            this.radLabel30.Text = "价值：";
            // 
            // tbxitem_no
            // 
            this.tbxitem_no.Enabled = false;
            this.tbxitem_no.Location = new System.Drawing.Point(96, 84);
            this.tbxitem_no.MaxLength = 11;
            this.tbxitem_no.Name = "tbxitem_no";
            this.tbxitem_no.Size = new System.Drawing.Size(106, 20);
            this.tbxitem_no.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(48, 86);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "编码：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(48, 62);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "类别：";
            // 
            // ddlclass_id
            // 
            this.ddlclass_id.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlclass_id.Location = new System.Drawing.Point(96, 60);
            this.ddlclass_id.Name = "ddlclass_id";
            this.ddlclass_id.Size = new System.Drawing.Size(105, 20);
            this.ddlclass_id.TabIndex = 12;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(230, 62);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "规格：";
            // 
            // ddlspec
            // 
            this.ddlspec.Enabled = false;
            this.ddlspec.Location = new System.Drawing.Point(278, 60);
            this.ddlspec.Name = "ddlspec";
            this.ddlspec.Size = new System.Drawing.Size(106, 20);
            this.ddlspec.TabIndex = 18;
            // 
            // tbxsup_name
            // 
            this.tbxsup_name.Enabled = false;
            this.tbxsup_name.Location = new System.Drawing.Point(96, 108);
            this.tbxsup_name.MaxLength = 11;
            this.tbxsup_name.Name = "tbxsup_name";
            this.tbxsup_name.Size = new System.Drawing.Size(288, 20);
            this.tbxsup_name.TabIndex = 19;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(36, 110);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(54, 18);
            this.radLabel5.TabIndex = 20;
            this.radLabel5.Text = "供应商：";
            // 
            // tbxreg_date
            // 
            this.tbxreg_date.Enabled = false;
            this.tbxreg_date.Location = new System.Drawing.Point(96, 132);
            this.tbxreg_date.MaxLength = 11;
            this.tbxreg_date.Name = "tbxreg_date";
            this.tbxreg_date.Size = new System.Drawing.Size(106, 20);
            this.tbxreg_date.TabIndex = 21;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(25, 134);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(65, 18);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "注册日期：";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(25, 158);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 24;
            this.radLabel7.Text = "保管部门：";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(218, 134);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(54, 18);
            this.radLabel8.TabIndex = 26;
            this.radLabel8.Text = "保管人：";
            // 
            // ddlkeeper
            // 
            this.ddlkeeper.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlkeeper.Location = new System.Drawing.Point(278, 134);
            this.ddlkeeper.Name = "ddlkeeper";
            this.ddlkeeper.Size = new System.Drawing.Size(106, 20);
            this.ddlkeeper.TabIndex = 27;
            // 
            // tbxparent_id
            // 
            this.tbxparent_id.Enabled = false;
            this.tbxparent_id.Location = new System.Drawing.Point(278, 158);
            this.tbxparent_id.MaxLength = 11;
            this.tbxparent_id.Name = "tbxparent_id";
            this.tbxparent_id.Size = new System.Drawing.Size(106, 20);
            this.tbxparent_id.TabIndex = 28;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(207, 159);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(65, 18);
            this.radLabel9.TabIndex = 29;
            this.radLabel9.Text = "调出部门：";
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(96, 181);
            this.tbxstatus.MaxLength = 11;
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.Size = new System.Drawing.Size(106, 20);
            this.tbxstatus.TabIndex = 30;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(48, 183);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(42, 18);
            this.radLabel10.TabIndex = 31;
            this.radLabel10.Text = "状态：";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(207, 183);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 32;
            this.radLabel11.Text = "报废日期：";
            // 
            // dtpdestroy_date
            // 
            this.dtpdestroy_date.Enabled = false;
            this.dtpdestroy_date.Location = new System.Drawing.Point(278, 181);
            this.dtpdestroy_date.Name = "dtpdestroy_date";
            this.dtpdestroy_date.Size = new System.Drawing.Size(106, 20);
            this.dtpdestroy_date.TabIndex = 33;
            this.dtpdestroy_date.TabStop = false;
            this.dtpdestroy_date.Value = new System.DateTime(((long)(0)));
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(96, 205);
            this.tbxcreater.MaxLength = 11;
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.Size = new System.Drawing.Size(106, 20);
            this.tbxcreater.TabIndex = 34;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(36, 207);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(54, 18);
            this.radLabel12.TabIndex = 35;
            this.radLabel12.Text = "建立人：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(278, 205);
            this.tbxcreate_date.MaxLength = 11;
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.Size = new System.Drawing.Size(106, 20);
            this.tbxcreate_date.TabIndex = 36;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(207, 207);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(65, 18);
            this.radLabel13.TabIndex = 37;
            this.radLabel13.Text = "建立时间：";
            // 
            // ddlkeep_dept
            // 
            this.ddlkeep_dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlkeep_dept.Location = new System.Drawing.Point(96, 157);
            this.ddlkeep_dept.Name = "ddlkeep_dept";
            this.ddlkeep_dept.Size = new System.Drawing.Size(106, 20);
            this.ddlkeep_dept.TabIndex = 38;
            // 
            // UI_ad_asset_manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 269);
            this.Controls.Add(this.ddlkeep_dept);
            this.Controls.Add(this.tbxcreate_date);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.tbxcreater);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.dtpdestroy_date);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.tbxstatus);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.tbxparent_id);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.ddlkeeper);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.tbxreg_date);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.tbxsup_name);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.ddlspec);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.ddlclass_id);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbxprice);
            this.Controls.Add(this.radLabel30);
            this.Controls.Add(this.tbxitem_no);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radCommandBar2);
            this.Name = "UI_ad_asset_manage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "固定资产登记";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxprice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxitem_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlclass_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlspec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsup_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxreg_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlkeeper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxparent_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpdestroy_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlkeep_dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxprice;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox tbxitem_no;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlclass_id;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList ddlspec;
        private Telerik.WinControls.UI.RadTextBox tbxsup_name;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox tbxreg_date;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList ddlkeeper;
        private Telerik.WinControls.UI.RadTextBox tbxparent_id;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadDateTimePicker dtpdestroy_date;
        private Telerik.WinControls.UI.RadTextBox tbxcreater;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox tbxcreate_date;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadDropDownList ddlkeep_dept;
    }
}