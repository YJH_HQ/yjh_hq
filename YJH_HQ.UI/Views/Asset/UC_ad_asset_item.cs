﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UC_ad_asset_item : UserControl
    {
        #region===字段===
        #endregion
        #region===构造===
        public UC_ad_asset_item()
        {
            InitializeComponent();
            LoadPermission();
            this.Load += UC_ad_asset_item_Load;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.btnNew.Click += btnNew_Click;
            this.btnModify.Click += btnModify_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.dgasset_item.GridView.DoubleClick += GridView_DoubleClick;
        }
        #endregion
        #region===事件===
        void UC_ad_asset_item_Load(object sender, EventArgs e)
        {
            ddlstatus.SelectedIndex = 0;
            this.Search();
        }
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgasset_item.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要删除的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    assetService.DeleteassetitemByID(Guid.Parse(dgasset_item.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
                    RadMessageBox.Show(this, "删除成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
                catch (Exception ex)
                {
                    RadMessageBox.Show(this, "删除失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
        void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (dgasset_item.GridView.SelectedRows.Count == 0)
            {
                return;
            }
            UI_ad_asset_item ui = new UI_ad_asset_item();
            ui.dgList = dgasset_item;
            ui.IsNew = false;
            ui.entity = assetService.GetassetitemByID(Guid.Parse(dgasset_item.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }
        void btnModify_Click(object sender, EventArgs e)
        {
            if (dgasset_item.GridView.SelectedRows.Count == 0)
            {
                RadMessageBox.Show(this, "请选择需要编辑的记录", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            UI_ad_asset_item ui = new UI_ad_asset_item();
            ui.dgList = dgasset_item;
            ui.IsNew = false;
            ui.entity = assetService.GetassetitemByID(Guid.Parse(dgasset_item.GridView.SelectedRows[0].Cells["clID"].Value.ToString()));
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            UI_ad_asset_item ui = new UI_ad_asset_item();
            ui.dgList = dgasset_item;
            ui.IsNew = true;
            ui.StartPosition = FormStartPosition.CenterScreen;
            ui.ShowDialog();
        }
        #endregion
        #region===方法===
        private void Search()
        {
            this.dgasset_item.GridView.DataSource = assetService.asset_itemSearch(tbxName.Text.Trim(),int.Parse(ddlstatus.SelectedItems[0].Tag.ToString()));
        }

        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            this.btnNew.Enabled = funcs.Exists(t => t.Name == "新建");
            this.btnModify.Enabled = funcs.Exists(t => t.Name == "编辑");
            this.btnDelete.Enabled = funcs.Exists(t => t.Name == "删除");
            this.btnAudit.Enabled = funcs.Exists(t => t.Name == "启用/禁用");
        }
        #endregion
    }
}
