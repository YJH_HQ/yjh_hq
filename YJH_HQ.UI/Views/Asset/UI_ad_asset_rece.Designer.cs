﻿namespace YJH_HQ.UI.Views.Asset
{
    partial class UI_ad_asset_rece
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxsup_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.tbxrece_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.cbxitem_id = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ddlrece_type = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlorder_unit = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbxrece_qty = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxrece_price = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.tbxrece_amt = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.tbxrece_date = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstatus = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreater = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.ddlwarehouse = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsup_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrece_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlorder_unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_amt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlwarehouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(425, 30);
            this.radCommandBar2.TabIndex = 5;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxsup_id
            // 
            this.tbxsup_id.Location = new System.Drawing.Point(96, 88);
            this.tbxsup_id.Name = "tbxsup_id";
            this.tbxsup_id.Size = new System.Drawing.Size(290, 20);
            this.tbxsup_id.TabIndex = 8;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(49, 90);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(42, 18);
            this.radLabel30.TabIndex = 9;
            this.radLabel30.Text = "厂商：";
            // 
            // tbxrece_id
            // 
            this.tbxrece_id.Enabled = false;
            this.tbxrece_id.Location = new System.Drawing.Point(95, 62);
            this.tbxrece_id.MaxLength = 11;
            this.tbxrece_id.Name = "tbxrece_id";
            this.tbxrece_id.Size = new System.Drawing.Size(106, 20);
            this.tbxrece_id.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(49, 64);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "单号：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(232, 64);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "品项：";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(49, 118);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "类型：";
            // 
            // cbxitem_id
            // 
            this.cbxitem_id.AutoSizeDropDownToBestFit = true;
            this.cbxitem_id.DisplayMember = "item_name";
            // 
            // cbxitem_id.NestedRadGridView
            // 
            this.cbxitem_id.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cbxitem_id.EditorControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbxitem_id.EditorControl.Font = new System.Drawing.Font("宋体", 9F);
            this.cbxitem_id.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbxitem_id.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxitem_id.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cbxitem_id.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cbxitem_id.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cbxitem_id.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "item_id";
            gridViewTextBoxColumn1.HeaderText = "编码";
            gridViewTextBoxColumn1.Name = "clitem_id";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "item_name";
            gridViewTextBoxColumn2.HeaderText = "品名";
            gridViewTextBoxColumn2.Name = "clitem_name";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 80;
            this.cbxitem_id.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.cbxitem_id.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cbxitem_id.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cbxitem_id.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "column1";
            this.cbxitem_id.EditorControl.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.cbxitem_id.EditorControl.Name = "NestedRadGridView";
            this.cbxitem_id.EditorControl.ReadOnly = true;
            this.cbxitem_id.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbxitem_id.EditorControl.ShowGroupPanel = false;
            this.cbxitem_id.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cbxitem_id.EditorControl.TabIndex = 0;
            this.cbxitem_id.Location = new System.Drawing.Point(281, 62);
            this.cbxitem_id.Name = "cbxitem_id";
            this.cbxitem_id.Size = new System.Drawing.Size(105, 20);
            this.cbxitem_id.TabIndex = 18;
            this.cbxitem_id.TabStop = false;
            this.cbxitem_id.ValueMember = "item_id";
            // 
            // ddlrece_type
            // 
            this.ddlrece_type.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Tag = "10";
            radListDataItem1.Text = "退货入库";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem1.TextWrap = true;
            radListDataItem2.Tag = "20";
            radListDataItem2.Text = "采购入库";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            radListDataItem2.TextWrap = true;
            this.ddlrece_type.Items.Add(radListDataItem1);
            this.ddlrece_type.Items.Add(radListDataItem2);
            this.ddlrece_type.Location = new System.Drawing.Point(97, 116);
            this.ddlrece_type.Name = "ddlrece_type";
            this.ddlrece_type.Size = new System.Drawing.Size(105, 20);
            this.ddlrece_type.TabIndex = 19;
            // 
            // ddlorder_unit
            // 
            this.ddlorder_unit.Location = new System.Drawing.Point(281, 116);
            this.ddlorder_unit.Name = "ddlorder_unit";
            this.ddlorder_unit.Size = new System.Drawing.Size(105, 20);
            this.ddlorder_unit.TabIndex = 21;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(232, 118);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 18);
            this.radLabel3.TabIndex = 20;
            this.radLabel3.Text = "单位：";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(26, 144);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 18);
            this.radLabel5.TabIndex = 23;
            this.radLabel5.Text = "存放货位：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(232, 146);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(42, 18);
            this.radLabel6.TabIndex = 25;
            this.radLabel6.Text = "数量：";
            // 
            // tbxrece_qty
            // 
            this.tbxrece_qty.Location = new System.Drawing.Point(280, 142);
            this.tbxrece_qty.Mask = "n2";
            this.tbxrece_qty.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxrece_qty.Name = "tbxrece_qty";
            this.tbxrece_qty.Size = new System.Drawing.Size(106, 20);
            this.tbxrece_qty.TabIndex = 26;
            this.tbxrece_qty.TabStop = false;
            this.tbxrece_qty.Text = "0.00";
            this.tbxrece_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxrece_price
            // 
            this.tbxrece_price.Location = new System.Drawing.Point(97, 168);
            this.tbxrece_price.Mask = "n2";
            this.tbxrece_price.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxrece_price.Name = "tbxrece_price";
            this.tbxrece_price.Size = new System.Drawing.Size(105, 20);
            this.tbxrece_price.TabIndex = 28;
            this.tbxrece_price.TabStop = false;
            this.tbxrece_price.Text = "0.00";
            this.tbxrece_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(49, 170);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(42, 18);
            this.radLabel7.TabIndex = 27;
            this.radLabel7.Text = "单价：";
            // 
            // tbxrece_amt
            // 
            this.tbxrece_amt.Location = new System.Drawing.Point(281, 168);
            this.tbxrece_amt.Mask = "n2";
            this.tbxrece_amt.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxrece_amt.Name = "tbxrece_amt";
            this.tbxrece_amt.Size = new System.Drawing.Size(105, 20);
            this.tbxrece_amt.TabIndex = 30;
            this.tbxrece_amt.TabStop = false;
            this.tbxrece_amt.Text = "0.00";
            this.tbxrece_amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(232, 170);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(42, 18);
            this.radLabel8.TabIndex = 29;
            this.radLabel8.Text = "金额：";
            // 
            // tbxrece_date
            // 
            this.tbxrece_date.Enabled = false;
            this.tbxrece_date.Location = new System.Drawing.Point(97, 194);
            this.tbxrece_date.Name = "tbxrece_date";
            this.tbxrece_date.Size = new System.Drawing.Size(105, 20);
            this.tbxrece_date.TabIndex = 32;
            this.tbxrece_date.TabStop = false;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(26, 196);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(65, 18);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "入库时间：";
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(281, 194);
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.Size = new System.Drawing.Size(105, 20);
            this.tbxstatus.TabIndex = 34;
            this.tbxstatus.TabStop = false;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(232, 196);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(42, 18);
            this.radLabel10.TabIndex = 33;
            this.radLabel10.Text = "状态：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(282, 220);
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.Size = new System.Drawing.Size(104, 20);
            this.tbxcreate_date.TabIndex = 38;
            this.tbxcreate_date.TabStop = false;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(209, 222);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 37;
            this.radLabel11.Text = "建立时间：";
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(97, 220);
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.Size = new System.Drawing.Size(105, 20);
            this.tbxcreater.TabIndex = 36;
            this.tbxcreater.TabStop = false;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(37, 222);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(54, 18);
            this.radLabel12.TabIndex = 35;
            this.radLabel12.Text = "建立人：";
            // 
            // ddlwarehouse
            // 
            this.ddlwarehouse.Location = new System.Drawing.Point(97, 142);
            this.ddlwarehouse.Name = "ddlwarehouse";
            this.ddlwarehouse.Size = new System.Drawing.Size(105, 20);
            this.ddlwarehouse.TabIndex = 39;
            // 
            // UI_ad_asset_rece
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 286);
            this.Controls.Add(this.ddlwarehouse);
            this.Controls.Add(this.tbxcreate_date);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.tbxcreater);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.tbxstatus);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.tbxrece_date);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.tbxrece_amt);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.tbxrece_price);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.tbxrece_qty);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.ddlorder_unit);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.ddlrece_type);
            this.Controls.Add(this.cbxitem_id);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbxsup_id);
            this.Controls.Add(this.radLabel30);
            this.Controls.Add(this.tbxrece_id);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radCommandBar2);
            this.Name = "UI_ad_asset_rece";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "入库信息";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsup_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlrece_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlorder_unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_amt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxrece_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlwarehouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxsup_id;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox tbxrece_id;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cbxitem_id;
        private Telerik.WinControls.UI.RadDropDownList ddlrece_type;
        private Telerik.WinControls.UI.RadDropDownList ddlorder_unit;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxrece_qty;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxrece_price;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxrece_amt;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxrece_date;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxcreate_date;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxcreater;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadDropDownList ddlwarehouse;
    }
}