﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_manageout : Telerik.WinControls.UI.RadForm
    {
        public YJH.Entities.ad_asset_manage entity;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        public UI_ad_asset_manageout()
        {
            InitializeComponent();
            this.Load += UI_ad_asset_manageout_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlkeeper.Text == "")
                {
                    RadMessageBox.Show(this, "请选择保管人", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
                if(ddlparent_dept.Text == "")
                {
                    RadMessageBox.Show(this, "请选择调出部门", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
                YJH.Entities.ad_asset_manage newentity = new YJH.Entities.ad_asset_manage();
                newentity.class_id = entity.class_id;
                newentity.Company_id = entity.Company_id;
                newentity.create_date = OrgUnitService.GetDate();
                newentity.creater = Globle.CurrentEmployee.Base.Name;
                newentity.item_no = entity.item_no;
                newentity.keep_dept = Guid.Parse(ddlparent_dept.SelectedValue.ToString());
                newentity.keeper = ddlkeeper.Text;
                newentity.price = entity.price;
                newentity.reg_date = entity.reg_date;
                newentity.spec = entity.spec;
                newentity.status = 10;
                newentity.sup_name = entity.sup_name;
                var newentityState = newentity.Instance.PersistentState;
                assetService.SaveAssetManger(newentity);
                entity.parent_id = Guid.Parse(ddlparent_dept.SelectedValue.ToString());
                entity.status = 20;
                var entityState = entity.Instance.PersistentState;
                assetService.SaveAssetManger(entity);
                RadMessageBox.Show(this, "调出成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                this.RefreshList(entityState, entity);
                this.RefreshList(newentityState, newentity);
                
                this.Close();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "调出失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_asset_manage entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company", sys.Services.BusinessUnitService.GetBuByID(entity.Company_id).Name);
                data.Add("classname", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("spec", entity.spec);
                data.Add("price", entity.price);
                data.Add("sup_name", entity.sup_name);
                data.Add("item_no", entity.item_no);
                data.Add("reg_date", entity.reg_date);
                data.Add("keep_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.keep_dept).Name);
                data.Add("keeper", entity.keeper);
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date.ToString("yyyy-MM-dd HH:mm"));
                data.Add("status", entity.status);
                //if (entity.status == 10)
                //    data.Add("status", "正式");
                //else if(entity.status == 20)
                //    data.Add("status", "调出");
                //else if (entity.status == 30)
                //    data.Add("status", "报废");
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("Company", sys.Services.BusinessUnitService.GetBuByID(entity.Company_id).Name);
                data.Add("classname", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("spec", entity.spec);
                data.Add("price", entity.price);
                data.Add("sup_name", entity.sup_name);
                data.Add("item_no", entity.item_no);
                data.Add("reg_date", entity.reg_date);
                data.Add("keep_dept", sys.Services.WorkGroupService.GetWorkGroupByID(entity.keep_dept).Name);
                data.Add("keeper", entity.keeper);
                if (entity.parent_id != null)
                    data.Add("parent_id", sys.Services.WorkGroupService.GetWorkGroupByID(Guid.Parse(entity.parent_id.ToString())).Name);
                if (entity.destroy_date != null)
                    data.Add("destroy_date", entity.destroy_date.ToString());
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date.ToString("yyyy-MM-dd HH:mm"));
                data.Add("status", entity.status);
                //if (entity.status == 10)
                //    data.Add("status", "正式");
                //else if (entity.status == 20)
                //    data.Add("status", "调出");
                //else if (entity.status == 30)
                //    data.Add("status", "报废");
                this.dgList.RefreshSelectedRow(data);
            }
        }

        void UI_ad_asset_manageout_Load(object sender, EventArgs e)
        {
            ddlparent_dept.DisplayMember = "Name";
            ddlparent_dept.ValueMember = "ID";
            ddlparent_dept.DataSource = assetService.GetWorkGroup();
            ddlkeeper.DisplayMember = "Name";
            ddlkeeper.ValueMember = "ID";
            ddlkeeper.DataSource = bm_store_expandService.GetEmp();
            ddlkeeper.SelectedIndex = -1;
            ddlparent_dept.SelectedIndex = -1;
        }
    }
}
