﻿using sys.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Entities;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_item : Telerik.WinControls.UI.RadForm
    {
        #region===字段===
        public ad_asset_item entity;
        public bool IsNew = false;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        #endregion
        #region===构造===
        public UI_ad_asset_item()
        {
            InitializeComponent();
            this.Load += UI_ad_asset_item_Load;
            this.btSave.Click += btSave_Click;
        }
        #endregion
        #region===事件===
        void UI_ad_asset_item_Load(object sender, EventArgs e)
        {
            ddlclass_id.ValueMember = "class_id";
            ddlclass_id.DisplayMember = "class_name";
            ddlclass_id.DataSource = assetService.GetEnableassetclass();
            if (IsNew)
                New();
            else
                BindDate();
        }
        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(VaileDate())
                {
                    FulshDate();
                    var entityState = entity.Instance.PersistentState;
                    assetService.Saveassetitem(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
        #endregion
        #region===方法===
        private void New()
        {
            entity = new ad_asset_item();
            tbxitem_name.Text = "";
            tbxitem_id.Text = "";
            tbxspec.Text = "";
            ddlclass_id.SelectedIndex = -1;
            cbxIsDisabled.Checked = true;
        }

        private void BindDate()
        {
            tbxitem_id.Text = entity.item_id;
            tbxitem_name.Text = entity.item_name;
            tbxspec.Text = entity.spec;
            cbxIsDisabled.Checked = entity.IsDisabled;
            ddlclass_id.SelectedValue = entity.class_id;
        }

        private void FulshDate()
        {
            entity.class_id = ddlclass_id.SelectedValue.ToString();
            entity.Company_id = Guid.Parse("2B474195-706A-4E0D-BDF4-FAE6A4D88687");
            entity.IsDisabled = cbxIsDisabled.Checked;
            entity.item_id = tbxitem_id.Text;
            entity.item_name = tbxitem_name.Text;
            entity.spec = tbxspec.Text;
        }
        private bool VaileDate()
        {
            if (string.IsNullOrWhiteSpace(this.tbxitem_id.Text))
            {
                RadMessageBox.Show("编码不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlclass_id.Text))
            {
                RadMessageBox.Show("请选择类别", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxitem_name.Text))
            {
                RadMessageBox.Show("品名不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxspec.Text))
            {
                RadMessageBox.Show("规格不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_asset_item entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("item_id", entity.item_id);
                data.Add("class_id", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("item_name", entity.item_name);
                data.Add("spec", entity.spec);
                if(entity.IsDisabled)
                    data.Add("Disabled", "启用");
                else
                    data.Add("Disabled", "禁用");
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("item_id", entity.item_id);
                data.Add("class_id", assetService.GetClassNameByClassid(entity.class_id));
                data.Add("item_name", entity.item_name);
                data.Add("spec", entity.spec);
                if (entity.IsDisabled)
                    data.Add("Disabled", "启用");
                else
                    data.Add("Disabled", "禁用");
                this.dgList.RefreshSelectedRow(data);
            }
        }
        #endregion
    }
}
