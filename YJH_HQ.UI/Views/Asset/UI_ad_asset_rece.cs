﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Data;
using YJH.Services;

namespace YJH_HQ.UI.Views.Asset
{
    public partial class UI_ad_asset_rece : Telerik.WinControls.UI.RadForm
    {
        public bool IsNew = false;
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        public YJH.Entities.ad_asset_rece entity;
        public UI_ad_asset_rece()
        {
            InitializeComponent();
            this.Load += UI_ad_asset_rece_Load;
            this.btSave.Click += btSave_Click;
        }

        void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (VaileDate())
                {
                    FulshDate();
                    var entityState = entity.Instance.PersistentState;
                    assetreceService.Save(entity);
                    RadMessageBox.Show(this, "保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    this.RefreshList(entityState, entity);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        void UI_ad_asset_rece_Load(object sender, EventArgs e)
        {
            cbxitem_id.AutoSizeDropDownToBestFit = true;
            cbxitem_id.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Append;
            
            cbxitem_id.DisplayMember = "item_name";
            cbxitem_id.ValueMember = "item_id";
            cbxitem_id.DataSource = YJH.Services.assetreceService.GetItem_id();
            FilterDescriptor descriptor = new FilterDescriptor(this.cbxitem_id.DisplayMember, FilterOperator.StartsWith, string.Empty);
            cbxitem_id.EditorControl.FilterDescriptors.Add(descriptor);
            cbxitem_id.AutoCompleteMode = AutoCompleteMode.Append;
            if (IsNew)
                New();
            else
                BinData();
        }

        private void New()
        {
            entity = new YJH.Entities.ad_asset_rece();
            tbxcreater.Text = Globle.CurrentEmployee.Base.Name;
            tbxcreate_date.Text = YJH.Services.OrgUnitService.GetDate().ToString("yyyy-MM-dd HH:mm");
            tbxrece_amt.Text = "0";
            tbxrece_date.Text = "";
            tbxrece_id.Text = "";
            tbxrece_price.Text = "0";
            tbxrece_qty.Text = "0";
            tbxstatus.Text = "新建";
            tbxsup_id.Text = "";
            ddlwarehouse.Text = "";
            ddlorder_unit.Text = "";
            ddlrece_type.SelectedIndex = 0;
            cbxitem_id.Text = "";
        }

        private void BinData()
        {
            tbxrece_id.Text = entity.rece_id;
            tbxcreate_date.Text = entity.create_date.ToString("yyyy-MM-dd HH:mm");
            tbxcreater.Text = entity.creater;
            if (entity.status == 10)
                tbxstatus.Text = "新建";
            else
            {
                tbxstatus.Text = "已入库";
                tbxrece_date.Text = DateTime.Parse(entity.rece_date.ToString()).ToString("yyyy-MM-dd HH:mm");
                tbxrece_amt.Enabled = false;
                tbxrece_qty.Enabled = false;
                tbxrece_price.Enabled = false;
                tbxsup_id.Enabled = false;
                ddlwarehouse.Enabled = false;
                ddlrece_type.Enabled = false;
                ddlorder_unit.Enabled = false;
                cbxitem_id.Enabled = false;
            }
            tbxrece_amt.Text = entity.rece_amt.ToString("f2");
            tbxrece_price.Text = entity.rece_price.ToString("f2");
            tbxrece_qty.Text = entity.rece_qty.ToString("f2");
            tbxsup_id.Text = entity.sup_id;
            ddlwarehouse.Text = entity.warehouse;
            if (entity.rece_type == 10)
                ddlrece_type.SelectedIndex = 0;
            else
                ddlrece_type.SelectedIndex = 1;
            ddlorder_unit.Text = entity.order_unit;
            cbxitem_id.SelectedValue = entity.item_id;
            
        }

        private void FulshDate()
        {
            entity.Company_id = Guid.Parse("2B474195-706A-4E0D-BDF4-FAE6A4D88687");
            entity.create_date = DateTime.Parse(tbxcreate_date.Text);
            entity.creater = tbxcreater.Text;
            entity.item_id = cbxitem_id.SelectedValue.ToString();
            entity.order_unit = ddlorder_unit.Text;
            entity.rece_amt = Decimal.Parse(tbxrece_amt.Text);
            entity.rece_price = Decimal.Parse(tbxrece_price.Text);
            entity.rece_qty = Decimal.Parse(tbxrece_qty.Text);
            entity.rece_type = int.Parse(ddlrece_type.SelectedItems[0].Tag.ToString());
            if (tbxstatus.Text == "新建")
                entity.status = 10;
            else
                entity.status = 20;
            entity.sup_id = tbxsup_id.Text;
            entity.warehouse = ddlwarehouse.Text;
            if (IsNew)
            {
                string rece_id = YJH.Services.assetreceService.Getrece_id(YJH.Services.OrgUnitService.GetDate().Date);
                if (rece_id == "" || rece_id == null)
                    entity.rece_id = YJH.Services.OrgUnitService.GetDate().ToString("yyyyMMdd") + "001";
                else
                    entity.rece_id = rece_id.Substring(0, 8) + (int.Parse(rece_id.Substring(8, 3)) + 1).ToString().PadLeft(3,'0');
            }
        }
        private bool VaileDate()
        {
            if (string.IsNullOrWhiteSpace(this.cbxitem_id.Text))
            {
                RadMessageBox.Show("品项不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxsup_id.Text))
            {
                RadMessageBox.Show("厂商不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlorder_unit.Text))
            {
                RadMessageBox.Show("单位不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlwarehouse.Text))
            {
                RadMessageBox.Show("存放货位不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxrece_qty.Text))
            {
                RadMessageBox.Show("数量不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (decimal.Parse(this.tbxrece_qty.Text) <= 0)
            {
                RadMessageBox.Show("数量必须大于零", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxrece_price.Text))
            {
                RadMessageBox.Show("单价不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxrece_amt.Text))
            {
                RadMessageBox.Show("金额不能为空", "提示", MessageBoxButtons.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_asset_rece entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("rece_id", entity.rece_id);
                data.Add("item_id", entity.item_id);
                data.Add("sup_id", entity.sup_id);
                if (entity.rece_type == 10)
                    data.Add("rece_type", "退货入库");
                else
                    data.Add("rece_type", "采购入库");
                data.Add("order_unit", entity.order_unit);
                data.Add("warehouse", entity.warehouse);
                data.Add("rece_qty", entity.rece_qty);
                data.Add("rece_price", entity.rece_price);
                data.Add("rece_amt", entity.rece_amt);
                if (entity.status == 10)
                    data.Add("status", "新建");
                else
                    data.Add("status", "已入库");
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("rece_id", entity.rece_id);
                data.Add("item_id", entity.item_id);
                data.Add("sup_id", entity.sup_id);
                if (entity.rece_type == 10)
                    data.Add("rece_type", "退货入库");
                else
                    data.Add("rece_type", "采购入库");
                data.Add("order_unit", entity.order_unit);
                data.Add("warehouse", entity.warehouse);
                data.Add("rece_qty", entity.rece_qty);
                data.Add("rece_price", entity.rece_price);
                data.Add("rece_amt", entity.rece_amt);
                if(entity.rece_date != null)
                    data.Add("rece_date", entity.rece_date);
                if (entity.status == 10)
                    data.Add("status", "新建");
                else
                    data.Add("status", "已入库");
                data.Add("creater", entity.creater);
                data.Add("create_date", entity.create_date);
                this.dgList.RefreshSelectedRow(data);
            }
        }
    }
}
