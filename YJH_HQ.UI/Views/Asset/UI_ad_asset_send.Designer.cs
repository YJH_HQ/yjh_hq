﻿namespace YJH_HQ.UI.Views.Asset
{
    partial class UI_ad_asset_send
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.tbxsend_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.cbxitem_id = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbxsend_qty = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxsend_date = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreate_date = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcreater = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddlget_dept = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlgeter = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.tbxforWhat = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlget_dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlgeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxforWhat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(411, 30);
            this.radCommandBar2.TabIndex = 5;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tbxsend_id
            // 
            this.tbxsend_id.Enabled = false;
            this.tbxsend_id.Location = new System.Drawing.Point(87, 62);
            this.tbxsend_id.MaxLength = 11;
            this.tbxsend_id.Name = "tbxsend_id";
            this.tbxsend_id.Size = new System.Drawing.Size(105, 20);
            this.tbxsend_id.TabIndex = 6;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(41, 64);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "单号：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(224, 64);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "品项：";
            // 
            // cbxitem_id
            // 
            this.cbxitem_id.AutoSizeDropDownToBestFit = true;
            this.cbxitem_id.DisplayMember = "item_name";
            // 
            // cbxitem_id.NestedRadGridView
            // 
            this.cbxitem_id.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cbxitem_id.EditorControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbxitem_id.EditorControl.Font = new System.Drawing.Font("宋体", 9F);
            this.cbxitem_id.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbxitem_id.EditorControl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxitem_id.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cbxitem_id.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cbxitem_id.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cbxitem_id.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "item_id";
            gridViewTextBoxColumn1.HeaderText = "编码";
            gridViewTextBoxColumn1.Name = "clitem_id";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "item_name";
            gridViewTextBoxColumn2.HeaderText = "品名";
            gridViewTextBoxColumn2.Name = "clitem_name";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 80;
            this.cbxitem_id.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.cbxitem_id.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cbxitem_id.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cbxitem_id.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "column1";
            this.cbxitem_id.EditorControl.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.cbxitem_id.EditorControl.Name = "NestedRadGridView";
            this.cbxitem_id.EditorControl.ReadOnly = true;
            this.cbxitem_id.EditorControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbxitem_id.EditorControl.ShowGroupPanel = false;
            this.cbxitem_id.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cbxitem_id.EditorControl.TabIndex = 0;
            this.cbxitem_id.Location = new System.Drawing.Point(274, 62);
            this.cbxitem_id.Name = "cbxitem_id";
            this.cbxitem_id.Size = new System.Drawing.Size(104, 20);
            this.cbxitem_id.TabIndex = 18;
            this.cbxitem_id.TabStop = false;
            this.cbxitem_id.ValueMember = "item_id";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(41, 88);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(42, 18);
            this.radLabel6.TabIndex = 25;
            this.radLabel6.Text = "数量：";
            // 
            // tbxsend_qty
            // 
            this.tbxsend_qty.Location = new System.Drawing.Point(87, 88);
            this.tbxsend_qty.Mask = "n2";
            this.tbxsend_qty.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxsend_qty.Name = "tbxsend_qty";
            this.tbxsend_qty.Size = new System.Drawing.Size(105, 20);
            this.tbxsend_qty.TabIndex = 26;
            this.tbxsend_qty.TabStop = false;
            this.tbxsend_qty.Text = "0.00";
            this.tbxsend_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxsend_date
            // 
            this.tbxsend_date.Enabled = false;
            this.tbxsend_date.Location = new System.Drawing.Point(274, 88);
            this.tbxsend_date.Name = "tbxsend_date";
            this.tbxsend_date.Size = new System.Drawing.Size(104, 20);
            this.tbxsend_date.TabIndex = 32;
            this.tbxsend_date.TabStop = false;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(201, 90);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(65, 18);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "出库时间：";
            // 
            // tbxcreate_date
            // 
            this.tbxcreate_date.Enabled = false;
            this.tbxcreate_date.Location = new System.Drawing.Point(274, 187);
            this.tbxcreate_date.Name = "tbxcreate_date";
            this.tbxcreate_date.Size = new System.Drawing.Size(104, 20);
            this.tbxcreate_date.TabIndex = 38;
            this.tbxcreate_date.TabStop = false;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(201, 187);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 18);
            this.radLabel11.TabIndex = 37;
            this.radLabel11.Text = "建立时间：";
            // 
            // tbxcreater
            // 
            this.tbxcreater.Enabled = false;
            this.tbxcreater.Location = new System.Drawing.Point(87, 187);
            this.tbxcreater.Name = "tbxcreater";
            this.tbxcreater.Size = new System.Drawing.Size(105, 20);
            this.tbxcreater.TabIndex = 36;
            this.tbxcreater.TabStop = false;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(29, 189);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(54, 18);
            this.radLabel12.TabIndex = 35;
            this.radLabel12.Text = "建立人：";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(201, 114);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 41;
            this.radLabel3.Text = "领用部门：";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(29, 114);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 39;
            this.radLabel4.Text = "领用人：";
            // 
            // ddlget_dept
            // 
            this.ddlget_dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlget_dept.Location = new System.Drawing.Point(274, 114);
            this.ddlget_dept.Name = "ddlget_dept";
            this.ddlget_dept.Size = new System.Drawing.Size(104, 20);
            this.ddlget_dept.TabIndex = 44;
            // 
            // ddlgeter
            // 
            this.ddlgeter.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlgeter.Location = new System.Drawing.Point(87, 114);
            this.ddlgeter.Name = "ddlgeter";
            this.ddlgeter.Size = new System.Drawing.Size(105, 20);
            this.ddlgeter.TabIndex = 43;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(41, 148);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(42, 18);
            this.radLabel8.TabIndex = 46;
            this.radLabel8.Text = "用途：";
            // 
            // tbxforWhat
            // 
            this.tbxforWhat.AutoSize = false;
            this.tbxforWhat.Location = new System.Drawing.Point(87, 140);
            this.tbxforWhat.Multiline = true;
            this.tbxforWhat.Name = "tbxforWhat";
            this.tbxforWhat.Size = new System.Drawing.Size(291, 40);
            this.tbxforWhat.TabIndex = 45;
            // 
            // UI_ad_asset_send
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 238);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.tbxforWhat);
            this.Controls.Add(this.ddlget_dept);
            this.Controls.Add(this.ddlgeter);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.tbxcreate_date);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.tbxcreater);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.tbxsend_date);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.tbxsend_qty);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.cbxitem_id);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.tbxsend_id);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radCommandBar2);
            this.Name = "UI_ad_asset_send";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "出库信息";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxitem_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxsend_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreate_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlget_dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlgeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxforWhat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadTextBox tbxsend_id;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cbxitem_id;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxsend_qty;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxsend_date;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxcreate_date;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxcreater;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList ddlget_dept;
        private Telerik.WinControls.UI.RadDropDownList ddlgeter;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox tbxforWhat;
    }
}