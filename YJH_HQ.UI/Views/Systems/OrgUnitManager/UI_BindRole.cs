﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UI_BindRole : Telerik.WinControls.UI.RadForm
    {
        #region ==== 构造 ====
        public UI_BindRole()
        {
            InitializeComponent();

            this.Load += UI_BindRole_Load;
            this.btnSave.Click += btnSave_Click;
            this.btnCancel.Click += btnCancel_Click;
        }
        #endregion

        #region ==== 事件 ====
        void UI_BindRole_Load(object sender, EventArgs e)
        {
            ObservableCollection<sys.Entities.Roles> source = sys.Services.RoleService.Search(false);
            this.lvRole.DataSource = source;
            this.lvRole.DataMember = "Name";
            this.lvRole.DisplayMember = "Name";
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            if (this.lvRole.CheckedItems.Count <= 0)
            {
                this.tpbInfo.ShowTip("请选择角色", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            foreach (var item in lvRole.CheckedItems)
            {
                var role = item.DataBoundItem as sys.Entities.Roles;
                //var 
                this.FuncsIDs = this.FuncsIDs.Union(role.RoleFuncs.Where(t => t.Funcs.FuncType == sys.Enums.FuncType.Funcs).Select(t => t.FuncsID)).ToList();
                //this.RoleIDs.Add(role.Instance.ID);
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
        #endregion

        #region ==== 属性 ====
        private List<Guid> _FuncsIDs = new List<Guid>();
        /// <summary>
        /// 当前选中的功能对应的权限
        /// </summary>
        public List<Guid> FuncsIDs
        {
            get { return _FuncsIDs; }
            set { _FuncsIDs = value; }
        }
        #endregion
    }
}
