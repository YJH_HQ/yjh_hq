﻿using System;
using System.Collections.Generic;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UI_ExistEmployee : Telerik.WinControls.UI.RadForm
    {
        private IList<sys.Entities.OrgUnit> _source;
        public sys.Entities.Emploee currentEmployee;
        private sys.Entities.OrgUnit _currentSelectedOU;

        #region ctor
        /// <summary>
        /// 绑定已存在员工
        /// </summary>
        /// <param name="IsShowBindAccount">是否显示已经绑定账号的员工</param>
        public UI_ExistEmployee(sys.Entities.OrgUnit currentSelectedOU)
        {
            InitializeComponent();

            _currentSelectedOU = currentSelectedOU;

            this.Load += UI_ExistEmployee_Load;
            this.btnConfirm.Click += btnConfirm_Click;
            this.btnCancel.Click += btnCancel_Click;
            this.tvOrgUnits.NodeExpandedChanging += tvOrgUnits_NodeExpandedChanging;
        }
        #endregion

        #region events
        void UI_ExistEmployee_Load(object sender, EventArgs e)
        {
            this.BindRootNodes();
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            var selectNode = this.tvOrgUnits.SelectedNode;
            if (selectNode == null)
            {
                this.tpbInfo.ShowTip("请选择员工！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            var selectOu = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (selectOu.Type != OrgUnitTreeNode.EMPLOEE)
            {
                this.tpbInfo.ShowTip("只允许选择员工！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }

            //绑定规则：部门与要绑定的员工直属公司必须是不同的
            Guid currentSelectedOUID = Guid.Empty;
            if (_currentSelectedOU.BaseType == "sys.BusinessUnit")
                currentSelectedOUID = _currentSelectedOU.BaseID;
            if (_currentSelectedOU.BaseType == "sys.WorkGroup")
                currentSelectedOUID = ((sys.Entities.WorkGroup)_currentSelectedOU.Base).BusinessUnitID;
            if (_currentSelectedOU.BaseType == "sys.Emploee")
                currentSelectedOUID = ((sys.Entities.Emploee)_currentSelectedOU.Base).BusinessUnitID;

            if (((sys.Entities.Emploee)((sys.Entities.OrgUnit)selectOu.EntityBase).Base).BusinessUnitID == currentSelectedOUID)
            {
                this.tpbInfo.ShowTip("只允许选择不同公司下的员工！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }

            currentEmployee = new sys.Entities.Emploee((dps.Common.Data.Entity)selectOu.EntityBase.Instance["Base"].Value);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
        #endregion

        #region load tree
        /// <summary>
        /// 绑定根级节点
        /// </summary>
        private void BindRootNodes()
        {
            IList<sys.Entities.OrgUnit> source = sys.Services.OrgUnitService.GetCompanyTreeList();
            if (source == null || source.Count == 0) return;

            foreach (sys.Entities.OrgUnit ou in source)
                AppendNode(ou, Guid.Empty);
        }

        /// <summary>
        /// 根据ID查询子节点，并绑定目录树
        /// </summary>
        /// <param name="id">Empty为获取顶级节点</param>
        private void BindOUTree(Guid id)
        {
            IList<sys.Entities.OrgUnit> source = sys.Services.OrgUnitService.GetOrgUnitTreeList(id);
            if (source == null || source.Count == 0) return;

            foreach (sys.Entities.OrgUnit ou in source)
                AppendNode(ou, id);
        }

        private void AppendNode(sys.Entities.OrgUnit ou, Guid id)
        {
            var treeNode = new OrgUnitTreeNode(ou.BaseType, ou, ou.Name.ToString());

            //添加临时节点，这里的临时节点Text为null，Tag为父节点的ID
            Telerik.WinControls.UI.RadTreeNode tempNode = new Telerik.WinControls.UI.RadTreeNode(null);
            tempNode.Tag = ou.Instance.ID;
            //员工节点是最后一级不需要添加临时节点
            if (ou.BaseType != OrgUnitTreeNode.EMPLOEE)
                treeNode.Nodes.Add(tempNode);

            if (id == Guid.Empty)
                this.tvOrgUnits.Nodes.Add(treeNode);
            else
            {
                //根据id查找父节点，并向其添加子节点
                var preNode = this.tvOrgUnits.FindNodes(n => n.Tag != null && n.Text != null && ((OrgUnitTreeNode)n).EntityBase.Instance.ID == id);
                if (preNode != null && preNode.Length > 0)
                    preNode[0].Nodes.Add(treeNode);
            }
        }

        /// <summary>
        /// 伸展、关闭节点事件
        /// </summary>
        private void tvOrgUnits_NodeExpandedChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            //默认false为打开状态
            if (!e.Node.Expanded)
            {
                //查找当前节点下的临时节点，有的话直接删除
                var nodes = e.Node.FindNodes(n => n.Text == null && (Guid)n.Tag == ((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
                for (int i = 0; i < nodes.Length; i++)
                    nodes[0].Remove();

                //子节点数为0并且有临时节点和当前节点不为员工，根据这个条件组合判断出当前节点是否需要重新加载子节点
                if (e.Node.Nodes.Count <= 0 && nodes.Length > 0 && ((OrgUnitTreeNode)e.Node).Type != OrgUnitTreeNode.EMPLOEE)
                    BindOUTree(((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
            }
        }
        #endregion
    }
}