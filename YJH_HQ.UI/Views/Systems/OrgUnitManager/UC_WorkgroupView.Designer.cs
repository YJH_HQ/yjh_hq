﻿namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    partial class UC_WorkgroupView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.IsEnabled = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.IsDistribution = new Telerik.WinControls.UI.RadCheckBox();
            this.IsPurchase = new Telerik.WinControls.UI.RadCheckBox();
            this.IsFinance = new Telerik.WinControls.UI.RadCheckBox();
            this.IsAdmin = new Telerik.WinControls.UI.RadCheckBox();
            this.IsBusiness = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFinance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBusiness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.IsEnabled);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.IsDistribution);
            this.radGroupBox1.Controls.Add(this.IsPurchase);
            this.radGroupBox1.Controls.Add(this.IsFinance);
            this.radGroupBox1.Controls.Add(this.IsAdmin);
            this.radGroupBox1.Controls.Add(this.IsBusiness);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "部门信息";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(2, 17, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(445, 177);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "部门信息";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(95, 45);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(198, 20);
            this.tbxName.TabIndex = 0;
            // 
            // IsEnabled
            // 
            this.IsEnabled.Location = new System.Drawing.Point(94, 77);
            this.IsEnabled.Name = "IsEnabled";
            this.IsEnabled.Size = new System.Drawing.Size(15, 15);
            this.IsEnabled.TabIndex = 33;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(26, 77);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(63, 18);
            this.radLabel13.TabIndex = 31;
            this.radLabel13.Text = "是否启用：";
            // 
            // IsDistribution
            // 
            this.IsDistribution.Location = new System.Drawing.Point(344, 109);
            this.IsDistribution.Name = "IsDistribution";
            this.IsDistribution.Size = new System.Drawing.Size(66, 18);
            this.IsDistribution.TabIndex = 36;
            this.IsDistribution.Text = "物流组织";
            // 
            // IsPurchase
            // 
            this.IsPurchase.Location = new System.Drawing.Point(265, 109);
            this.IsPurchase.Name = "IsPurchase";
            this.IsPurchase.Size = new System.Drawing.Size(66, 18);
            this.IsPurchase.TabIndex = 37;
            this.IsPurchase.Text = "采购组织";
            // 
            // IsFinance
            // 
            this.IsFinance.Location = new System.Drawing.Point(186, 109);
            this.IsFinance.Name = "IsFinance";
            this.IsFinance.Size = new System.Drawing.Size(66, 18);
            this.IsFinance.TabIndex = 35;
            this.IsFinance.Text = "财务组织";
            // 
            // IsAdmin
            // 
            this.IsAdmin.Location = new System.Drawing.Point(107, 109);
            this.IsAdmin.Name = "IsAdmin";
            this.IsAdmin.Size = new System.Drawing.Size(66, 18);
            this.IsAdmin.TabIndex = 34;
            this.IsAdmin.Text = "行政组织";
            // 
            // IsBusiness
            // 
            this.IsBusiness.Location = new System.Drawing.Point(27, 109);
            this.IsBusiness.Name = "IsBusiness";
            this.IsBusiness.Size = new System.Drawing.Size(66, 18);
            this.IsBusiness.TabIndex = 32;
            this.IsBusiness.Text = "业务组织";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(26, 45);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(63, 18);
            this.radLabel1.TabIndex = 32;
            this.radLabel1.Text = "部门名称：";
            // 
            // UC_WorkgroupView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_WorkgroupView";
            this.Size = new System.Drawing.Size(445, 177);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFinance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBusiness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.RadCheckBox IsEnabled;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadCheckBox IsDistribution;
        private Telerik.WinControls.UI.RadCheckBox IsPurchase;
        private Telerik.WinControls.UI.RadCheckBox IsFinance;
        private Telerik.WinControls.UI.RadCheckBox IsAdmin;
        private Telerik.WinControls.UI.RadCheckBox IsBusiness;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}