﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UC_OrgUnitManager : UserControl
    {
        private List<YJH.Entities.bm_process_store> list1 = new List<YJH.Entities.bm_process_store>();
        public UC_OrgUnitManager()
        {
            InitializeComponent();
            this.Load += OnLoad;
            this.btnBindExistEmp.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        }

        /// <summary>
        /// 当前用户的组织机构权限
        /// </summary>
        private List<sys.Entities.Funcs> _currentUserFuncList = null;
        /// <summary>
        /// 标识当前用户的组织机构是否存在管理员的权限
        /// </summary>
        private bool _isAdmin = false;
        private bool _isSaveEmp = false;
        private Telerik.WinControls.UI.RadTreeNode _adminTreeNode = null;

        private void OnLoad(object sender, EventArgs e)
        {
            this.tvOrgUnits.NodeExpandedChanging += tvOrgUnits_NodeExpandedChanging;
            this.tvOrgUnits.SelectedNodeChanged += tvOrgUnits_SelectedNodeChanged;

            //绑定树之前先获取权限
            if (_currentUserFuncList == null)
                _currentUserFuncList = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            if (_currentUserFuncList != null && !_currentUserFuncList.Exists(f => f.Name == "管理员") && _currentUserFuncList.Exists(f => f.Name == "查看"))
            {
                //该用户不是管理员权限则只能看到当前公司的信息，首先绑定的是当前登录人所登录的公司
                var unitOU = Globle.CurrentBusinessUnitOU;
                var treeNode = new OrgUnitTreeNode(unitOU.BaseType, unitOU, unitOU.Name.ToString());
                //添加临时节点，这里的临时节点Text为null，Tag为父节点的ID
                Telerik.WinControls.UI.RadTreeNode tempNode = new Telerik.WinControls.UI.RadTreeNode(null);
                tempNode.Tag = unitOU.Instance.ID;
                //员工节点是最后一级不需要添加临时节点
                if (unitOU.BaseType != OrgUnitTreeNode.EMPLOEE)
                    treeNode.Nodes.Add(tempNode);

                tvOrgUnits.Nodes.Add(treeNode);
                tvOrgUnits.SelectedNode = treeNode;

                _isSaveEmp = _currentUserFuncList.Exists(f => f.Name == "保存员工");
            }
            else
            {
                _isAdmin = true;
                BindOUTree(Guid.Empty);
            }

            #region == 绑定BarButton点击事件 ==
            this.btnNewCompany.Click += btnNewCompany_Click;
            this.btnNewGroup.Click += btnNewGroup_Click;
            this.btnNewEmploee.Click += btnNewEmploee_Click;
            this.btnExistEmployee.Click += btnExistEmployee_Click;
            this.btnSave.Click += btnSave_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.btnResetPassword.Click += btnResetPassword_Click;
            this.btnBindExistEmp.Click += btnBindExistEmp_Click;
            this.btnBindRole.Click += btnBindRole_Click;
            #endregion

            if (this.tvOrgUnits.Nodes.Count > 0)
                this.tvOrgUnits.Nodes[0].Selected = true;

            this.LoadFuncTree();
            //加载完权限后，判断当前用户权限是否是非管理员，如不是管理员则要禁用“组织机构管理”下的“管理员”功能权限
            if (!_isAdmin)
            {
                var node = tvPermissions.Find("组织机构管理");
                if (node != null && node.Nodes.Count > 0)
                {
                    var nodes = node.FindNodes(f => f.Text == "管理员");
                    if (nodes != null && nodes.Length > 0)
                    {
                        nodes[0].Enabled = false;
                        _adminTreeNode = nodes[0];
                    }
                }
            }

            this.tvPermissions.NodeCheckedChanged += tvPermissions_NodeCheckedChanged;
        }

        #region ==== 视图 ====
        private UC_BusinessUnitView _buView;
        private UC_BusinessUnitView BusinessUnitView
        {
            get
            {
                if (_buView == null)
                {
                    _buView = new UC_BusinessUnitView();
                    _buView.Dock = DockStyle.Fill;
                }

                return _buView;
            }
        }

        private UC_WorkgroupView _workView;
        private UC_WorkgroupView WorkgroupView
        {
            get
            {
                if (_workView == null)
                {
                    _workView = new UC_WorkgroupView();
                    _workView.Dock = DockStyle.Fill;
                }

                return _workView;
            }
        }

        private UC_EmploeeView _emploeeView;
        private UC_EmploeeView EmploeeView
        {
            get
            {
                if (_emploeeView == null)
                {
                    _emploeeView = new UC_EmploeeView();
                    _emploeeView.Dock = DockStyle.Fill;
                }

                return _emploeeView;
            }
        }

        private IView view = null;
        #endregion

        #region ==== 组织机构目录树 ====

        /// <summary>
        /// 根据ID查询子节点，并绑定目录树
        /// </summary>
        /// <param name="id">Empty为获取顶级节点</param>
        private void BindOUTree(Guid id)
        {
            IList<sys.Entities.OrgUnit> source = null;
            if (_isAdmin)
                source = sys.Services.OrgUnitService.GetOrgUnitTreeList(id);
            else
                source = sys.Services.OrgUnitService.GetOrganization(id, OrganizationType(Globle.CurrentOrganizationType.ToString()));

            if (source == null || source.Count == 0) return;
            foreach (sys.Entities.OrgUnit ou in source)
                AppendNode(ou, id);
        }
        private string OrganizationType(string type)
        {
            if (type == sys.Enums.OrganizationType.业务组织机构.ToString())
                return "IsBusiness";
            if (type == sys.Enums.OrganizationType.行政组织机构.ToString())
                return "IsAdmin";
            if (type == sys.Enums.OrganizationType.采购组织机构.ToString())
                return "IsPurchase";
            if (type == sys.Enums.OrganizationType.物流组织机构.ToString())
                return "IsDistribution";
            if (type == sys.Enums.OrganizationType.财务组织机构.ToString())
                return "IsFinance";

            throw new Exception("组织类型异常");
        }

        private OrgUnitTreeNode AppendNode(sys.Entities.OrgUnit ou, Guid id)
        {
            var treeNode = new OrgUnitTreeNode(ou.BaseType, ou, ou.Name.ToString());

            //添加临时节点，这里的临时节点Text为null，Tag为父节点的ID
            Telerik.WinControls.UI.RadTreeNode tempNode = new Telerik.WinControls.UI.RadTreeNode(null);
            tempNode.Tag = ou.Instance.ID;
            //员工节点是最后一级不需要添加临时节点
            if (ou.BaseType != OrgUnitTreeNode.EMPLOEE)
                treeNode.Nodes.Add(tempNode);

            if (id == Guid.Empty)
                this.tvOrgUnits.Nodes.Add(treeNode);
            else
            {
                //根据id查找父节点，并向其添加子节点
                var preNode = this.tvOrgUnits.FindNodes(n => n.Tag != null && n.Text != null && ((OrgUnitTreeNode)n).EntityBase.Instance.ID == id);
                if (preNode != null && preNode.Count() > 0)
                    preNode[0].Nodes.Add(treeNode);
            }
            return treeNode;
        }

        /// <summary>
        /// 伸展、关闭节点事件
        /// </summary>
        private void tvOrgUnits_NodeExpandedChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            //默认false为打开状态
            if (!e.Node.Expanded)
            {
                //查找当前节点下的临时节点，有的话直接删除
                var nodes = e.Node.FindNodes(n => n.Text == null && (Guid)n.Tag == ((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
                for (int i = 0; i < nodes.Count(); i++)
                    nodes[0].Remove();

                //子节点数为0并且有临时节点和当前节点不为员工，根据这个条件组合判断出当前节点是否需要重新加载子节点
                if (e.Node.Nodes.Count() <= 0 && nodes.Count() > 0 && ((OrgUnitTreeNode)e.Node).Type != OrgUnitTreeNode.EMPLOEE)
                    BindOUTree(((OrgUnitTreeNode)e.Node).EntityBase.Instance.ID);
            }
        }

        private void tvOrgUnits_SelectedNodeChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            OrgUnitTreeNode treeNode = (OrgUnitTreeNode)e.Node;

            //首先清空容器，其次设置禁止、启用新建部分功能，最后向容器内添加控件并赋值
            this.pvInfo.Controls.Clear();
            //changed后重置按钮Enabled
            btnNewCompany.Enabled = false;
            btnNewGroup.Enabled = false;
            btnNewEmploee.Enabled = false;
            btnExistEmployee.Enabled = false;
            btnSave.Enabled = false;
            btnDelete.Enabled = false;
            btnResetPassword.Enabled = false;
            btnBindRole.Enabled = false;
            pvInfo.Enabled = false;
            pvPermission.Enabled = false;

            switch (treeNode.Type)
            {
                case OrgUnitTreeNode.BUSINESS_UNIT:
                    if (_isAdmin)
                    {
                        btnNewCompany.Enabled = true;
                        btnNewGroup.Enabled = true;
                        btnSave.Enabled = true;
                        btnDelete.Enabled = true;
                        pvInfo.Enabled = true;
                    }
                    pvProcess.Enabled = false;
                    view = BusinessUnitView;
                    break;
                case OrgUnitTreeNode.WORK_GROUP:
                    if (_isAdmin)
                    {
                        btnNewCompany.Enabled = true;
                        btnNewGroup.Enabled = true;
                        btnNewEmploee.Enabled = true;
                        btnExistEmployee.Enabled = true;
                        btnSave.Enabled = true;
                        btnDelete.Enabled = true;
                        pvInfo.Enabled = true;
                    }
                    else if (_isSaveEmp)
                    {
                        btnNewEmploee.Enabled = true;
                        btnExistEmployee.Enabled = true;
                    }
                    pvProcess.Enabled = false;
                    view = WorkgroupView;
                    break;
                case OrgUnitTreeNode.EMPLOEE:
                    if (_isAdmin || _isSaveEmp)
                    {
                        btnSave.Enabled = true;
                        btnResetPassword.Enabled = true;
                        btnBindRole.Enabled = true;
                        pvInfo.Enabled = true;
                        pvPermission.Enabled = true;
                        if (_currentUserFuncList.Exists(f => f.Name == "删除员工"))
                            btnDelete.Enabled = true;
                    }
                    pvProcess.Enabled = true;
                    bmTree.Nodes.Clear();
                    BuildProcessTree();
                    var list = YJH.Services.StoreMngService.Getbm_schedule_storeByUser(treeNode.Text);
                    foreach (var u in list)
                    {
                        var node = bmTree.Find(s => s.Tag.ToString() == u.process_id);
                        if (node != null)
                            node.Checked = true;
                    }
                    //只有选中员工的时候才清空权限
                    var checkedNodes = tvPermissions.CheckedNodes;
                    foreach (var node in checkedNodes)
                        node.Checked = false;

                    view = EmploeeView;

                    // 员工权限管理
                    IsChanged = false;
                    // 加载当前选中员工的所属权限
                    System.Threading.Tasks.Task.Factory.StartNew(new Action(() =>
                    {
                        // 获取权限
                        _userFunc = sys.Services.FuncsService.GetFuncByOrgUnitID((Guid)treeNode.Tag);
                    })).ContinueWith(new Action<System.Threading.Tasks.Task>((task) =>
                    {
                        // 先撤销权限的选择事件,加载完之后在启用
                        this.tvPermissions.NodeCheckedChanged -= tvPermissions_NodeCheckedChanged;
                        // 加载
                        IAsyncResult ias = null;
                        foreach (var func in _userFunc)
                        {
                            var node = tvPermissions.Find(s => (Guid)s.Value == func.FuncsID);
                            if (node != null)
                                ias = this.BeginInvoke(new Action(() => node.Checked = true));
                        }

                        if (ias != null)
                            this.EndInvoke(ias);

                        // 启用权限的选择事件
                        this.tvPermissions.NodeCheckedChanged += tvPermissions_NodeCheckedChanged;
                    }));

                    List<sys.Entities.UserFunc> _userView = null;
                    System.Threading.Tasks.Task.Factory.StartNew(new Action(() =>
                    {
                        _userView = sys.Services.FuncsService.GetViewByOrgUnitID((Guid)treeNode.Tag);
                    })).ContinueWith(new Action<System.Threading.Tasks.Task>((task) =>
                    {
                        // 先撤销权限的选择事件,加载完之后在启用
                        this.tvPermissions.NodeCheckedChanged -= tvPermissions_NodeCheckedChanged;
                        // 加载
                        IAsyncResult ias = null;
                        foreach (var views in _userView)
                        {
                            var node = tvPermissions.Find(s => (Guid)s.Value == views.FuncsID);
                            if (node != null && node.Nodes.Count == 0)
                                ias = this.BeginInvoke(new Action(() => node.Checked = true));
                        }

                        if (ias != null)
                            this.EndInvoke(ias);

                        // 启用权限的选择事件
                        this.tvPermissions.NodeCheckedChanged += tvPermissions_NodeCheckedChanged;
                    }));
                    break;
            }

            this.pvInfo.Controls.Add((UserControl)view);
            view.BindData(treeNode.EntityBase);

            //区分是否是管理员
            this.pvInfo.Enabled = _isAdmin ? true : treeNode.Type == OrgUnitTreeNode.EMPLOEE ? true : false;
        }

        public void BuildProcessTree()
        {
            list1 = YJH.Services.StoreMngService.Getbm_process();
            foreach (var item in list1.Where(t => t.process_parent == null).OrderBy(t => t.process_order_id))
                LoopAddTreeNode(item, null);
        }

        private void LoopAddTreeNode(YJH.Entities.bm_process_store entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Text = entity.process_name;
            node.Value = entity.Instance.ID;
            node.Tag = entity.process_id;
            node.Name = entity.process_level.ToString();
            if (ParentNode == null)
                this.bmTree.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in list1.Where(t => t.process_parent == entity.process_id).OrderBy(t => t.process_order_id))
                LoopAddTreeNode(item, node);
        }
        #endregion

        #region ==== BarButton点击事件 ====
        /// <summary>
        /// 新建公司
        /// </summary>
        private void btnNewCompany_Click(object sender, EventArgs e)
        {
            CreateInfo(OrgUnitTreeNode.BUSINESS_UNIT);
        }

        /// <summary>
        /// 新建部门
        /// </summary>
        private void btnNewGroup_Click(object sender, EventArgs e)
        {
            if (this.tvOrgUnits.SelectedNode == null)
                return;
            OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (currentTreeNode.EntityBase.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                this.tpbInfo.ShowTip("请保存当前节点", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            CreateInfo(OrgUnitTreeNode.WORK_GROUP);
        }

        private Guid GetParentNodeID(OrgUnitTreeNode node)
        {
            Guid id;

            if (node.Type != OrgUnitTreeNode.BUSINESS_UNIT)
            {
                OrgUnitTreeNode no = node.Parent as OrgUnitTreeNode;
                id = GetParentNodeID(no);
            }
            else
            {
                var bu = (sys.Entities.BusinessUnit)((sys.Entities.OrgUnit)node.EntityBase).Base;

                return (Guid)bu.Instance["ID"].Value;
            }

            return id;
        }

        /// <summary>
        /// 新建员工
        /// </summary>
        private void btnNewEmploee_Click(object sender, EventArgs e)
        {
            if (this.tvOrgUnits.SelectedNode == null)
                return;
            OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (currentTreeNode.EntityBase.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                this.tpbInfo.ShowTip("请保存当前节点", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            CreateInfo(OrgUnitTreeNode.EMPLOEE);
        }

        private void btnExistEmployee_Click(object sender, EventArgs e)
        {
            if (this.tvOrgUnits.SelectedNode == null)
                return;
            OrgUnitTreeNode node = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (node.Type == OrgUnitTreeNode.EMPLOEE)
                return;
            if (node.EntityBase.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                this.tpbInfo.ShowTip("请保存当前节点", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            try
            {
                UI_ExistEmployee ui = new UI_ExistEmployee((sys.Entities.OrgUnit)node.EntityBase);
                ui.Owner = CommonService.FindForm(this);
                if (ui.ShowDialog() == DialogResult.OK)
                {
                    var treenode = CreateInfo(OrgUnitTreeNode.EMPLOEE);

                    var ou = treenode.EntityBase as sys.Entities.OrgUnit;
                    ou.Base = ui.currentEmployee;
                    view.BindData(treenode.EntityBase);
                    treenode.Text = ui.currentEmployee.Base.Name;
                }
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("绑定出错\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        private OrgUnitTreeNode CreateInfo(string createType)
        {
            OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;

            //保存信息并且获取当前公司的组织机构ID
            sys.Entities.OrgUnit ou = null;
            //上级组织结构ID
            Guid parentID = Guid.Empty;
            //直接公司ID
            Guid buID = Guid.Empty;

            if (currentTreeNode != null)
            {
                buID = GetParentNodeID(currentTreeNode);
                parentID = (Guid)((sys.Entities.OrgUnit)currentTreeNode.EntityBase).Instance["ID"].Value;
            }

            switch (createType)
            {
                case OrgUnitTreeNode.BUSINESS_UNIT:
                    sys.Entities.BusinessUnit bu = new sys.Entities.BusinessUnit();
                    bu.Name = "新公司";

                    ou = new sys.Entities.OrgUnit(bu);
                    break;
                case OrgUnitTreeNode.WORK_GROUP:
                    sys.Entities.WorkGroup group = new sys.Entities.WorkGroup();
                    group.BusinessUnitID = buID;
                    group.Name = "新部门";

                    ou = new sys.Entities.OrgUnit(group);
                    break;
                case OrgUnitTreeNode.EMPLOEE:
                    sys.Entities.Person person = new sys.Entities.Person();
                    person.Name = "新员工";
                    person.Gender = sys.Enums.Gender.Male;

                    sys.Entities.Emploee emp = new sys.Entities.Emploee();
                    emp.Base = person;
                    emp.BusinessUnitID = buID;

                    ou = new sys.Entities.OrgUnit(emp);
                    break;
                default:
                    return null;
            }
            ou.ParentID = parentID;
            ou.IsEnabled = true;
            ou.IsDeleted = false;
            var treenode = AppendNode(ou, parentID);
            this.tvOrgUnits.SelectedNode = treenode;
            return treenode;
        }

        /// <summary>
        /// 保存
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            IView view = this.pvInfo.Controls[0] as IView;

            try
            {
                string errorResult;
                string text;
                dps.Data.Mapper.EntityBase entity = view.GetData(out errorResult, out text);
                if (!string.IsNullOrWhiteSpace(errorResult))
                {
                    this.tpbInfo.ShowTip(errorResult, YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                    return;
                }

                if (view.ViewType == sys.Enums.BaseType.Emploee)
                {
                    var account = entity.Instance["Base"]["Account"].Value;
                    if (account != null)
                    {
                        //验证账号是否存在
                        sys.Entities.Emploee emploee = (sys.Entities.Emploee)((sys.Entities.OrgUnit)entity).Base;
                        if (sys.Services.UserService.IsExistSameAccountByEmploee(emploee))
                        {
                            this.tpbInfo.ShowTip("保存失败，账号/姓名已存在！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                            return;
                        }

                        sys.Entities.User user = sys.Services.UserService.GetUserByEmploeeAccount(account.ToString());
                        if (user == null)
                            sys.Services.UserService.CreateUser(account.ToString().Replace("sys\\", ""));
                        else
                        {
                            user.Account = account.ToString();
                            dps.Common.SysService.Invoke("sys", "Persistent", "Save", user.Instance);
                        }
                    }
                }

                if (view.ViewType == sys.Enums.BaseType.BusinessUnit)
                {
                    sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)entity;
                    sys.Entities.BusinessUnit bu = (sys.Entities.BusinessUnit)ou.Base;
                    if (entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                    {
                        if ((bool)ou.IsStore)
                        {
                            YJH.Entities.ba_store store = new YJH.Entities.ba_store();
                            store.store_name = bu.Name;
                            store.BUID = bu.Instance.ID;
                            store.State = YJH.Enums.StoreState.新建;
                            sys.Services.BusinessUnitService.SaveStore(store);
                        }
                    }
                    else
                    {
                        if ((bool)ou.IsStore)
                            sys.Services.BusinessUnitService.UpdataStore(bu.ID, bu.Name);
                    }
                    
                }

                dps.Common.SysService.Invoke("sys", "Persistent", "Save", entity.Instance);
                entity.Instance.AcceptChanges();

              

                // 保存权限
                System.Threading.Tasks.Task.Factory.StartNew(new Action(SaveUserFunc));
                System.Threading.Tasks.Task.Factory.StartNew(new Action(SaveUserProcess));

                ChangeTreeNode(entity.Instance.ID, text);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip(ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
                return;
            }

            this.tpbInfo.ShowTip("保存成功！", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
        }

        /// <summary>
        /// 保存员工权限
        /// </summary>
        private void SaveUserFunc()
        {
            //只有选中的是员工并且权限已经更改
            OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (currentTreeNode.Type == OrgUnitTreeNode.EMPLOEE && IsChanged)
            {
                //当前用户的组织机构ID
                Guid ouID = (Guid)currentTreeNode.Tag;

                List<Guid> funcIds = new List<Guid>();
                var checkedTreeNodes = tvPermissions.FindNodes(t => t.Checked);
                foreach (var item in checkedTreeNodes)
                {
                    funcIds.Add((Guid)item.Value);
                    LoopParent(item, ref funcIds);
                }

                //如果当前是非管理员权限则要把用户已经选中的“组织机构管理”中的“管理员”权限移除
                if (_adminTreeNode != null && !_isAdmin)
                    funcIds.Remove((Guid)_adminTreeNode.Value);

                sys.Services.UserFuncService.Save(ouID, funcIds);
                IsChanged = false;
            }
        }
        /// <summary>
        /// 保存员工拓店过程权限
        /// </summary>
        private void SaveUserProcess()
        {
            //只有选中的是员工并且权限已经更改
            OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (currentTreeNode.Type == OrgUnitTreeNode.EMPLOEE)
            {
                List<string> processids = new List<string>();
                var checkedTreeNodes = bmTree.FindNodes(t => t.Checked);
                foreach (var item in checkedTreeNodes)
                {
                    processids.Add(item.Tag.ToString());
                }
                YJH.Services.StoreMngService.SaveUserProcess(processids, currentTreeNode.Text);
            }
        }
        private void LoopParent(Telerik.WinControls.UI.RadTreeNode node, ref List<Guid> funcIds)
        {
            var parent = node.Parent;

            do
            {
                if (parent == null)
                    break;

                if (!parent.Checked)
                {
                    Guid value = (Guid)parent.Value;
                    if (!funcIds.Contains(value))
                        funcIds.Add(value);
                }

                parent = parent.Parent;
            }
            while (true);
        }

        /// <summary>
        /// 保存成功后，修改组织机构数Text
        /// </summary>
        /// <param name="id">要修改的节点ID</param>
        private void ChangeTreeNode(Guid id, string text)
        {
            var changeNode = this.tvOrgUnits.FindNodes(n => n.Tag != null && n.Text != null && ((OrgUnitTreeNode)n).EntityBase.Instance.ID == id);
            if (changeNode.Count() > 0)
                changeNode[0].Text = text;
        }

        /// <summary>
        /// 删除
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                OrgUnitTreeNode currentTreeNode = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;

                //if (currentTreeNode.Type == OrgUnitTreeNode.BUSINESS_UNIT)
                //{
                //    this.tpbInfo.ShowTip("不允许删除公司！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                //    return;
                //}

                if (currentTreeNode.Nodes.Count > 0)
                {
                    this.tpbInfo.ShowTip("删除当前组织单元前请先删除所有下级组织单元！", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                    return;
                }

                if (Telerik.WinControls.RadMessageBox.Show(this, "确认删除当前组织单元吗？", "确认删除", MessageBoxButtons.OKCancel, Telerik.WinControls.RadMessageIcon.Question) != DialogResult.OK)
                    return;

                switch (currentTreeNode.Type)
                {
                    case OrgUnitTreeNode.BUSINESS_UNIT:
                        if (!(bool)((sys.Entities.OrgUnit)currentTreeNode.EntityBase).IsStore)
                            sys.Services.BusinessUnitService.DeleteCompany(((sys.Entities.OrgUnit)currentTreeNode.EntityBase));
                        break;
                    case OrgUnitTreeNode.WORK_GROUP:
                        sys.Services.WorkGroupService.DeleteWorkGroup(((sys.Entities.OrgUnit)currentTreeNode.EntityBase));
                        break;
                    case OrgUnitTreeNode.EMPLOEE:
                        sys.Services.EmploeeService.DeleteEmploee(((sys.Entities.OrgUnit)currentTreeNode.EntityBase));
                        break;
                }

                if (currentTreeNode.Type == OrgUnitTreeNode.BUSINESS_UNIT && (bool)((sys.Entities.OrgUnit)currentTreeNode.EntityBase).IsStore)
                {
                    YJH.Entities.ba_store st = sys.Services.BusinessUnitService.SelectStore(((sys.Entities.OrgUnit)currentTreeNode.EntityBase).BaseID);
                    if (st.State == YJH.Enums.StoreState.新建)
                    {
                        sys.Services.BusinessUnitService.DeleteCompanyAndStore(((sys.Entities.OrgUnit)currentTreeNode.EntityBase));
                    }
                    else
                    { 
                        return;
                    }
                }     
                var node = this.tvOrgUnits.FindNodes(n => n.Selected == true);
                if (node.Count() > 0) node[0].Remove();
                this.tpbInfo.ShowTip("删除成功", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("删除失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            var ou = (this.tvOrgUnits.SelectedNode as OrgUnitTreeNode).EntityBase as sys.Entities.OrgUnit;

            sys.Entities.Emploee emploee = (sys.Entities.Emploee)ou.Base;
            if (string.IsNullOrWhiteSpace(emploee.Account))
            {
                this.tpbInfo.ShowTip("当前员工未绑定账号，不允许重置密码", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            if (Telerik.WinControls.RadMessageBox.Show(this, "确定重置选中员工的账户密码吗？", "提示信息", MessageBoxButtons.OKCancel, Telerik.WinControls.RadMessageIcon.Question) == DialogResult.Cancel)
                return;
            try
            {
                sys.Services.UserService.ChangePassword(emploee.Account);
                this.tpbInfo.ShowTip("密码重置成功", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("密码重置失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        void btnBindExistEmp_Click(object sender, EventArgs e)
        {
            if (this.tvOrgUnits.SelectedNode == null)
                return;
            OrgUnitTreeNode node = this.tvOrgUnits.SelectedNode as OrgUnitTreeNode;
            if (node.Type != OrgUnitTreeNode.EMPLOEE)
                return;
            if (node.EntityBase.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
                return;
            try
            {
                UI_ExistEmployee ui = new UI_ExistEmployee((sys.Entities.OrgUnit)node.EntityBase);
                ui.Owner = CommonService.FindForm(this);
                if (ui.ShowDialog() == DialogResult.OK)
                {
                    var ou = node.EntityBase as sys.Entities.OrgUnit;
                    ou.Base = ui.currentEmployee;
                    view.BindData(node.EntityBase);
                    node.Text = ui.currentEmployee.Base.Name;
                }
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("绑定出错\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        void btnBindRole_Click(object sender, EventArgs e)
        {
            try
            {
                UI_BindRole ui = new UI_BindRole();
                ui.Owner = CommonService.FindForm(this);
                if (ui.ShowDialog() == DialogResult.OK)
                {
                    foreach (var funcId in ui.FuncsIDs)
                    {
                        var nodes = tvPermissions.FindNodes(n => (Guid)n.Value == funcId);
                        if (nodes == null)
                            continue;
                        nodes[0].Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("操作失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }
        #endregion

        #region ==== 功能权限 ====
        //判断用户权限是否人为的更改
        private bool IsChanged = false;
        // 功能 数据
        private List<sys.Entities.Funcs> funcSource;
        //用户权限
        private List<sys.Entities.UserFunc> _userFunc;

        //待保存权限
        private List<sys.Entities.UserFunc> _changeFunc = new List<sys.Entities.UserFunc>();

        private void LoadFuncTree()
        {
            try
            {
                funcSource = sys.Services.FuncsService.Search("", 0);
                if (funcSource == null)
                    return;

                foreach (var item in funcSource.Where(t => t.Parent == null).OrderBy(t => t.Sort))
                    LoopAddTreeNode(item, null);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("查询失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        private void LoopAddTreeNode(sys.Entities.Funcs entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Tag = entity;
            node.Text = entity.Name;
            node.Value = entity.Instance.ID;
            if (ParentNode == null)
                this.tvPermissions.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);

            foreach (var item in funcSource.Where(t => t.ParentID == entity.Instance.ID).OrderBy(t => t.Sort))
                LoopAddTreeNode(item, node);
        }

        private void tvPermissions_NodeCheckedChanged(object sender, Telerik.WinControls.UI.TreeNodeCheckedEventArgs e)
        {
            IsChanged = true;
        }
        #endregion
    }
}