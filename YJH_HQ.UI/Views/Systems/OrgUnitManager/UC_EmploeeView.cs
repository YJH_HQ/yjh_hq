﻿using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UC_EmploeeView : UserControl, IView
    {
        public UC_EmploeeView()
        {
            InitializeComponent();

            ddlGender.EnumSource(typeof(sys.Enums.Gender), true, "请选择");

            this.ddlEmploee.SelectedValueChanged += DdlEmploee_SelectedValueChanged;
           
        }

        private void DdlEmploee_SelectedValueChanged(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.ddlEmploee.Text) && this.ddlEmploee.SelectedItem!=null)
            {
                YJH.Entities.hr_emploee emp = new YJH.Entities.hr_emploee((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(YJH.Entities.hr_emploee.EntityModelID, (this.ddlEmploee.SelectedValue as YJH.Entities.hr_emploee).Instance.ID));
                this.tbName.Text = emp.Name;
                this.tbAccount.Text = emp.Number;
                this.tbxIDCard.Text = emp.IdentityId;
                this.tbxAddress.Text = emp.addrLive;
                this.tbxTel.Text = emp.Tel;
                this.ddlGender.SelectedValue = emp.Sex;
                this.IsEnabled.Checked = true;
            }
        }

        public sys.Enums.BaseType ViewType
        {
            get { return sys.Enums.BaseType.Emploee; }
        }

        private dps.Data.Mapper.EntityBase _entityBase;
        public void BindData(dps.Data.Mapper.EntityBase _base)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_base;
            sys.Entities.Emploee emploee = (sys.Entities.Emploee)(ou).Base;
            sys.Entities.Person person = emploee.Base;

            tbName.Text = person.Name;
            if (string.IsNullOrEmpty(emploee.Account))
                this.tbAccount.Text = null;
            else
            {
                if (emploee.Account.Split('\\').Length == 2)
                    this.tbAccount.Text = emploee.Account.Split('\\')[1];
                else
                    this.tbAccount.Text = emploee.Account;
            }
            tbxIDCard.Text = person.IdentityCardNO;
            tbxAddress.Text = person.HomeAddress;
            this.ddlGender.SelectedValue = (int)person.Gender;
            if (person.Birthday != null)
                this.dpkBirthday.Value = person.Birthday.Value;
            else
                this.dpkBirthday.SetToNullValue();
            this.tbxTel.Text = person.Tel;

            IsEnabled.Checked = ou.IsEnabled;

            if(_base.Instance.PersistentState==dps.Common.Data.PersistentState.Detached)
            {
                this.ddlEmploee.Enabled = true ;
                var empList = sys.Services.OrgUnitService.Gethr_emploeeList(ou.Parent.BaseID);
                this.ddlEmploee.DisplayMember = "Name";
                this.ddlEmploee.ValueMember = "ID";
                this.ddlEmploee.DataSource = empList;
            }
            else
            {
                this.ddlEmploee.Enabled = false;
                this.ddlEmploee.DataSource = null;
            }
            _entityBase = _base;
        }

        public dps.Data.Mapper.EntityBase GetData(out string errorResult, out string text)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_entityBase;

            errorResult = Check();
            text = this.tbName.Text;
            if (!string.IsNullOrWhiteSpace(errorResult))
                return null;

            var entity = (dps.Common.Data.Entity)_entityBase.Instance["Base"].Value;
            _entityBase.Instance["Base"]["Account"].Value = string.IsNullOrWhiteSpace(this.tbAccount.Text) ? null : string.Format("sys\\{0}", this.tbAccount.Text);

            entity["Name"].Value = this.tbName.Text;
            entity["IdentityCardNO"].Value = this.tbxIDCard.Text;
            entity["HomeAddress"].Value = this.tbxAddress.Text;
            if (!string.IsNullOrWhiteSpace(this.dpkBirthday.Text))
                entity["Birthday"].Value = this.dpkBirthday.Value;
            else
                entity["Birthday"].Value = null;
            entity["Tel"].Value = this.tbxTel.Text;
            entity["Gender"].Value = this.ddlGender.SelectedValue;

            ou.IsEnabled = IsEnabled.Checked;

            //新增模式
            if (ou.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                int serialNumber = sys.Services.OrgUnitService.GetMaxSerialNumber() + 1;
                ou.SerialNumber = serialNumber;
                ou.RoleNumber = ou.ParentID.HasValue ? ou.Parent.RoleNumber + "|" + serialNumber : "0";
                ou.Hierarchy = ou.ParentID.HasValue ? ou.Parent.Hierarchy + 1 : 0;

                if(!string.IsNullOrWhiteSpace(this.ddlEmploee.Text))
                    sys.Services.OrgUnitService.SaveHr_emploee((this.ddlEmploee.SelectedValue as YJH.Entities.hr_emploee).Instance.ID);
            }
            return _entityBase;
        }

        private string Check()
        {
            if (string.IsNullOrWhiteSpace(this.tbName.Text))
                return "姓名不能为空！";
            if (this.tbName.Text.Length > 50)
                return "姓名长度不能大于50！";
            if (this.tbxIDCard.Text.Length > 20)
                return "身份证号长度不能大于20！";
            if (this.tbxAddress.Text.Length > 20)
                return "家庭住址长度不能大于20！";
            if (this.tbxTel.Text.Length > 20)
                return "联系电话长度不能大于20！";

            return null;
        }
    }
}