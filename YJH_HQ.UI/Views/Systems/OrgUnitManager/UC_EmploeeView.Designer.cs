﻿namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    partial class UC_EmploeeView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.IsEnabled = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddlGender = new YJH_HQ.Controls.RadDropDownList.RadDropDownList();
            this.btnResetPassword = new Telerik.WinControls.UI.RadButton();
            this.tbxTel = new Telerik.WinControls.UI.RadTextBox();
            this.dpkBirthday = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbxAddress = new Telerik.WinControls.UI.RadTextBox();
            this.tbxIDCard = new Telerik.WinControls.UI.RadTextBox();
            this.labAddress = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnExist = new Telerik.WinControls.UI.RadButton();
            this.tbAccount = new Telerik.WinControls.UI.RadTextBox();
            this.tbName = new Telerik.WinControls.UI.RadTextBox();
            this.ddlEmploee = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnResetPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dpkBirthday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxIDCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddlEmploee);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.IsEnabled);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddlGender);
            this.radGroupBox1.Controls.Add(this.btnResetPassword);
            this.radGroupBox1.Controls.Add(this.tbxTel);
            this.radGroupBox1.Controls.Add(this.dpkBirthday);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.tbxAddress);
            this.radGroupBox1.Controls.Add(this.tbxIDCard);
            this.radGroupBox1.Controls.Add(this.labAddress);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.btnExist);
            this.radGroupBox1.Controls.Add(this.tbAccount);
            this.radGroupBox1.Controls.Add(this.tbName);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "员工信息";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(2, 17, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(374, 331);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "员工信息";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(27, 20);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "已有员工：";
            // 
            // IsEnabled
            // 
            this.IsEnabled.Location = new System.Drawing.Point(96, 299);
            this.IsEnabled.Name = "IsEnabled";
            this.IsEnabled.Size = new System.Drawing.Size(15, 15);
            this.IsEnabled.TabIndex = 30;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(27, 297);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(65, 18);
            this.radLabel13.TabIndex = 29;
            this.radLabel13.Text = "是否启用：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(27, 82);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(65, 18);
            this.radLabel6.TabIndex = 8;
            this.radLabel6.Text = "登录账号：";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(27, 50);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "员工姓名：";
            // 
            // ddlGender
            // 
            this.ddlGender.Location = new System.Drawing.Point(99, 188);
            this.ddlGender.Name = "ddlGender";
            this.ddlGender.Size = new System.Drawing.Size(173, 20);
            this.ddlGender.TabIndex = 18;
            this.ddlGender.Text = "radDropDownList1";
            // 
            // btnResetPassword
            // 
            this.btnResetPassword.Location = new System.Drawing.Point(280, 82);
            this.btnResetPassword.Name = "btnResetPassword";
            this.btnResetPassword.Size = new System.Drawing.Size(67, 24);
            this.btnResetPassword.TabIndex = 5;
            this.btnResetPassword.Text = "重置密码";
            this.btnResetPassword.Visible = false;
            // 
            // tbxTel
            // 
            this.tbxTel.Location = new System.Drawing.Point(99, 262);
            this.tbxTel.Name = "tbxTel";
            this.tbxTel.Size = new System.Drawing.Size(173, 20);
            this.tbxTel.TabIndex = 17;
            // 
            // dpkBirthday
            // 
            this.dpkBirthday.Location = new System.Drawing.Point(99, 224);
            this.dpkBirthday.Name = "dpkBirthday";
            this.dpkBirthday.Size = new System.Drawing.Size(174, 20);
            this.dpkBirthday.TabIndex = 13;
            this.dpkBirthday.TabStop = false;
            this.dpkBirthday.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(27, 262);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 18);
            this.radLabel5.TabIndex = 16;
            this.radLabel5.Text = "联系电话：";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(27, 224);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 12;
            this.radLabel3.Text = "出生日期：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(27, 188);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 18);
            this.radLabel2.TabIndex = 10;
            this.radLabel2.Text = "员工性别：";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(98, 152);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(174, 20);
            this.tbxAddress.TabIndex = 9;
            // 
            // tbxIDCard
            // 
            this.tbxIDCard.Location = new System.Drawing.Point(99, 117);
            this.tbxIDCard.Name = "tbxIDCard";
            this.tbxIDCard.Size = new System.Drawing.Size(174, 20);
            this.tbxIDCard.TabIndex = 7;
            // 
            // labAddress
            // 
            this.labAddress.Location = new System.Drawing.Point(27, 152);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(65, 18);
            this.labAddress.TabIndex = 8;
            this.labAddress.Text = "家庭地址：";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(27, 117);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "身份证号：";
            // 
            // btnExist
            // 
            this.btnExist.Location = new System.Drawing.Point(279, 50);
            this.btnExist.Name = "btnExist";
            this.btnExist.Size = new System.Drawing.Size(68, 22);
            this.btnExist.TabIndex = 2;
            this.btnExist.Text = "已有员工";
            this.btnExist.Visible = false;
            // 
            // tbAccount
            // 
            this.tbAccount.Location = new System.Drawing.Point(99, 85);
            this.tbAccount.Name = "tbAccount";
            this.tbAccount.Size = new System.Drawing.Size(174, 20);
            this.tbAccount.TabIndex = 4;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(99, 50);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(174, 20);
            this.tbName.TabIndex = 1;
            // 
            // ddlEmploee
            // 
            this.ddlEmploee.FormattingEnabled = true;
            this.ddlEmploee.Location = new System.Drawing.Point(98, 20);
            this.ddlEmploee.Name = "ddlEmploee";
            this.ddlEmploee.Size = new System.Drawing.Size(174, 20);
            this.ddlEmploee.TabIndex = 31;
            // 
            // UC_EmploeeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_EmploeeView";
            this.Size = new System.Drawing.Size(374, 331);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnResetPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dpkBirthday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxIDCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbName;
        private Telerik.WinControls.UI.RadTextBox tbAccount;
        private Telerik.WinControls.UI.RadButton btnExist;
        private Telerik.WinControls.UI.RadTextBox tbxAddress;
        private Telerik.WinControls.UI.RadTextBox tbxIDCard;
        private Telerik.WinControls.UI.RadLabel labAddress;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox tbxTel;
        private Telerik.WinControls.UI.RadDateTimePicker dpkBirthday;
        private Telerik.WinControls.UI.RadButton btnResetPassword;
        private Controls.RadDropDownList.RadDropDownList ddlGender;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadCheckBox IsEnabled;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private System.Windows.Forms.ComboBox ddlEmploee;
    }
}
