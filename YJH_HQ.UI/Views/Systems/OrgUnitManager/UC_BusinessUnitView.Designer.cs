﻿namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    partial class UC_BusinessUnitView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.IsStore = new Telerik.WinControls.UI.RadCheckBox();
            this.IsEntityBU = new Telerik.WinControls.UI.RadCheckBox();
            this.IsEnabled = new Telerik.WinControls.UI.RadCheckBox();
            this.IsDistribution = new Telerik.WinControls.UI.RadCheckBox();
            this.IsPurchase = new Telerik.WinControls.UI.RadCheckBox();
            this.IsFinance = new Telerik.WinControls.UI.RadCheckBox();
            this.IsAdmin = new Telerik.WinControls.UI.RadCheckBox();
            this.IsBusiness = new Telerik.WinControls.UI.RadCheckBox();
            this.ddlOrgType = new YJH_HQ.Controls.RadDropDownList.RadDropDownList();
            this.ddlRegion = new YJH_HQ.Controls.EntityPicker.EntityPicker();
            this.txtOrgID = new Telerik.WinControls.UI.RadTextBox();
            this.txtSort = new Telerik.WinControls.UI.RadTextBox();
            this.txtOrgCode = new Telerik.WinControls.UI.RadTextBox();
            this.tbxTel = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.tbxFax = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.tbxAddress = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.tbxZipCode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbxURL = new Telerik.WinControls.UI.RadTextBox();
            this.tbxShortName = new Telerik.WinControls.UI.RadTextBox();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IsStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEntityBU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFinance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBusiness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlOrgType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrgID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrgCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxZipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxShortName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.IsStore);
            this.radGroupBox1.Controls.Add(this.IsEntityBU);
            this.radGroupBox1.Controls.Add(this.IsEnabled);
            this.radGroupBox1.Controls.Add(this.IsDistribution);
            this.radGroupBox1.Controls.Add(this.IsPurchase);
            this.radGroupBox1.Controls.Add(this.IsFinance);
            this.radGroupBox1.Controls.Add(this.IsAdmin);
            this.radGroupBox1.Controls.Add(this.IsBusiness);
            this.radGroupBox1.Controls.Add(this.ddlOrgType);
            this.radGroupBox1.Controls.Add(this.ddlRegion);
            this.radGroupBox1.Controls.Add(this.txtOrgID);
            this.radGroupBox1.Controls.Add(this.txtSort);
            this.radGroupBox1.Controls.Add(this.txtOrgCode);
            this.radGroupBox1.Controls.Add(this.tbxTel);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.tbxFax);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.tbxAddress);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.tbxZipCode);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.tbxURL);
            this.radGroupBox1.Controls.Add(this.tbxShortName);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "公司信息";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(2, 17, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(446, 614);
            this.radGroupBox1.TabIndex = 4;
            this.radGroupBox1.Text = "公司信息";
            // 
            // IsStore
            // 
            this.IsStore.Location = new System.Drawing.Point(171, 449);
            this.IsStore.Name = "IsStore";
            this.IsStore.Size = new System.Drawing.Size(68, 18);
            this.IsStore.TabIndex = 29;
            this.IsStore.Text = "是否店铺";
            // 
            // IsEntityBU
            // 
            this.IsEntityBU.Location = new System.Drawing.Point(92, 449);
            this.IsEntityBU.Name = "IsEntityBU";
            this.IsEntityBU.Size = new System.Drawing.Size(68, 18);
            this.IsEntityBU.TabIndex = 29;
            this.IsEntityBU.Text = "是否实体";
            // 
            // IsEnabled
            // 
            this.IsEnabled.Location = new System.Drawing.Point(250, 449);
            this.IsEnabled.Name = "IsEnabled";
            this.IsEnabled.Size = new System.Drawing.Size(68, 18);
            this.IsEnabled.TabIndex = 28;
            this.IsEnabled.Text = "是否启用";
            // 
            // IsDistribution
            // 
            this.IsDistribution.Location = new System.Drawing.Point(171, 497);
            this.IsDistribution.Name = "IsDistribution";
            this.IsDistribution.Size = new System.Drawing.Size(68, 18);
            this.IsDistribution.TabIndex = 30;
            this.IsDistribution.Text = "物流组织";
            // 
            // IsPurchase
            // 
            this.IsPurchase.Location = new System.Drawing.Point(92, 497);
            this.IsPurchase.Name = "IsPurchase";
            this.IsPurchase.Size = new System.Drawing.Size(68, 18);
            this.IsPurchase.TabIndex = 30;
            this.IsPurchase.Text = "采购组织";
            // 
            // IsFinance
            // 
            this.IsFinance.Location = new System.Drawing.Point(251, 473);
            this.IsFinance.Name = "IsFinance";
            this.IsFinance.Size = new System.Drawing.Size(68, 18);
            this.IsFinance.TabIndex = 29;
            this.IsFinance.Text = "财务组织";
            // 
            // IsAdmin
            // 
            this.IsAdmin.Location = new System.Drawing.Point(172, 473);
            this.IsAdmin.Name = "IsAdmin";
            this.IsAdmin.Size = new System.Drawing.Size(68, 18);
            this.IsAdmin.TabIndex = 28;
            this.IsAdmin.Text = "行政组织";
            // 
            // IsBusiness
            // 
            this.IsBusiness.Location = new System.Drawing.Point(92, 473);
            this.IsBusiness.Name = "IsBusiness";
            this.IsBusiness.Size = new System.Drawing.Size(68, 18);
            this.IsBusiness.TabIndex = 27;
            this.IsBusiness.Text = "业务组织";
            // 
            // ddlOrgType
            // 
            this.ddlOrgType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlOrgType.Location = new System.Drawing.Point(92, 350);
            this.ddlOrgType.Name = "ddlOrgType";
            this.ddlOrgType.Size = new System.Drawing.Size(311, 20);
            this.ddlOrgType.TabIndex = 26;
            this.ddlOrgType.Text = "radDropDownList1";
            // 
            // ddlRegion
            // 
            this.ddlRegion.DisplayMember = null;
            this.ddlRegion.Location = new System.Drawing.Point(92, 423);
            this.ddlRegion.Name = "ddlRegion";
            this.ddlRegion.PickerView = null;
            this.ddlRegion.SelectedEntity = null;
            this.ddlRegion.Size = new System.Drawing.Size(311, 20);
            this.ddlRegion.TabIndex = 3;
            this.ddlRegion.TabStop = false;
            // 
            // txtOrgID
            // 
            this.txtOrgID.Location = new System.Drawing.Point(92, 319);
            this.txtOrgID.MaxLength = 16;
            this.txtOrgID.Name = "txtOrgID";
            this.txtOrgID.Size = new System.Drawing.Size(311, 20);
            this.txtOrgID.TabIndex = 8;
            // 
            // txtSort
            // 
            this.txtSort.Location = new System.Drawing.Point(92, 387);
            this.txtSort.MaxLength = 4;
            this.txtSort.Name = "txtSort";
            this.txtSort.Size = new System.Drawing.Size(311, 20);
            this.txtSort.TabIndex = 21;
            // 
            // txtOrgCode
            // 
            this.txtOrgCode.Location = new System.Drawing.Point(92, 284);
            this.txtOrgCode.MaxLength = 20;
            this.txtOrgCode.Name = "txtOrgCode";
            this.txtOrgCode.Size = new System.Drawing.Size(311, 20);
            this.txtOrgCode.TabIndex = 7;
            // 
            // tbxTel
            // 
            this.tbxTel.Location = new System.Drawing.Point(92, 249);
            this.tbxTel.MaxLength = 15;
            this.tbxTel.Name = "tbxTel";
            this.tbxTel.Size = new System.Drawing.Size(311, 20);
            this.tbxTel.TabIndex = 6;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(23, 423);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(63, 18);
            this.radLabel10.TabIndex = 25;
            this.radLabel10.Text = "所属区域：";
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(92, 214);
            this.tbxFax.MaxLength = 15;
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(311, 20);
            this.tbxFax.TabIndex = 5;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(23, 388);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(63, 18);
            this.radLabel11.TabIndex = 24;
            this.radLabel11.Text = "排序号码：";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(92, 144);
            this.tbxAddress.MaxLength = 50;
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(311, 20);
            this.tbxAddress.TabIndex = 3;
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(23, 353);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(63, 18);
            this.radLabel12.TabIndex = 23;
            this.radLabel12.Text = "机构类型：";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(23, 320);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(63, 18);
            this.radLabel9.TabIndex = 19;
            this.radLabel9.Text = "机构内码：";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(23, 285);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(63, 18);
            this.radLabel8.TabIndex = 18;
            this.radLabel8.Text = "机构编码：";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(23, 250);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(63, 18);
            this.radLabel7.TabIndex = 17;
            this.radLabel7.Text = "公司电话：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(23, 215);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(63, 18);
            this.radLabel6.TabIndex = 16;
            this.radLabel6.Text = "公司传真：";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(23, 145);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(63, 18);
            this.radLabel5.TabIndex = 15;
            this.radLabel5.Text = "公司地址：";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(23, 180);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(63, 18);
            this.radLabel4.TabIndex = 14;
            this.radLabel4.Text = "公司邮编：";
            // 
            // tbxZipCode
            // 
            this.tbxZipCode.Location = new System.Drawing.Point(92, 179);
            this.tbxZipCode.MaxLength = 10;
            this.tbxZipCode.Name = "tbxZipCode";
            this.tbxZipCode.Size = new System.Drawing.Size(311, 20);
            this.tbxZipCode.TabIndex = 4;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(23, 110);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(63, 18);
            this.radLabel1.TabIndex = 12;
            this.radLabel1.Text = "公司网址：";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(23, 75);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(63, 18);
            this.radLabel3.TabIndex = 11;
            this.radLabel3.Text = "公司简称：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(23, 40);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(63, 18);
            this.radLabel2.TabIndex = 10;
            this.radLabel2.Text = "公司名称：";
            // 
            // tbxURL
            // 
            this.tbxURL.Location = new System.Drawing.Point(92, 109);
            this.tbxURL.MaxLength = 100;
            this.tbxURL.Name = "tbxURL";
            this.tbxURL.Size = new System.Drawing.Size(311, 20);
            this.tbxURL.TabIndex = 2;
            // 
            // tbxShortName
            // 
            this.tbxShortName.Location = new System.Drawing.Point(92, 74);
            this.tbxShortName.MaxLength = 100;
            this.tbxShortName.Name = "tbxShortName";
            this.tbxShortName.Size = new System.Drawing.Size(311, 20);
            this.tbxShortName.TabIndex = 1;
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(92, 39);
            this.tbxName.MaxLength = 200;
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(311, 20);
            this.tbxName.TabIndex = 0;
            // 
            // UC_BusinessUnitView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_BusinessUnitView";
            this.Size = new System.Drawing.Size(446, 614);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IsStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEntityBU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFinance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsBusiness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlOrgType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrgID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrgCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxZipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxShortName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox txtOrgID;
        private Telerik.WinControls.UI.RadTextBox txtOrgCode;
        private Telerik.WinControls.UI.RadTextBox tbxTel;
        private Telerik.WinControls.UI.RadTextBox tbxFax;
        private Telerik.WinControls.UI.RadTextBox tbxAddress;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxZipCode;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox tbxURL;
        private Telerik.WinControls.UI.RadTextBox tbxShortName;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Controls.RadDropDownList.RadDropDownList ddlOrgType;
        private YJH_HQ.Controls.EntityPicker.EntityPicker ddlRegion;
        private Telerik.WinControls.UI.RadTextBox txtSort;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadCheckBox IsDistribution;
        private Telerik.WinControls.UI.RadCheckBox IsPurchase;
        private Telerik.WinControls.UI.RadCheckBox IsFinance;
        private Telerik.WinControls.UI.RadCheckBox IsAdmin;
        private Telerik.WinControls.UI.RadCheckBox IsBusiness;
        private Telerik.WinControls.UI.RadCheckBox IsEnabled;
        private Telerik.WinControls.UI.RadCheckBox IsStore;
        private Telerik.WinControls.UI.RadCheckBox IsEntityBU;

    }
}