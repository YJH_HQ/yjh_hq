﻿using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UC_WorkgroupView : UserControl, IView
    {
        public UC_WorkgroupView()
        {
            InitializeComponent();
        }

        public sys.Enums.BaseType ViewType
        {
            get { return sys.Enums.BaseType.WorkGroup; }
        }

        private dps.Data.Mapper.EntityBase _entityBase;
        public void BindData(dps.Data.Mapper.EntityBase _base)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_base;
            sys.Entities.WorkGroup bu = (sys.Entities.WorkGroup)(ou).Base;

            tbxName.Text = bu.Name;
            IsEnabled.Checked = ou.IsEnabled;
            IsBusiness.Checked = ou.IsBusiness.HasValue ? (bool)ou.IsBusiness : false;
            IsAdmin.Checked = ou.IsAdmin.HasValue ? (bool)ou.IsAdmin : false;
            IsPurchase.Checked = ou.IsPurchase.HasValue ? (bool)ou.IsPurchase : false;
            IsFinance.Checked = ou.IsFinance.HasValue ? (bool)ou.IsFinance : false;
            IsDistribution.Checked = ou.IsDistribution.HasValue ? (bool)ou.IsDistribution : false;

            _entityBase = _base;
        }

        public dps.Data.Mapper.EntityBase GetData(out string errorResult, out string text)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_entityBase;

            errorResult = Check();
            text = this.tbxName.Text;
            if (!string.IsNullOrWhiteSpace(errorResult))
                return null;

            var entity = (dps.Common.Data.Entity)_entityBase.Instance["Base"].Value;
            entity["Name"].Value = this.tbxName.Text;

            ou.IsEnabled = IsEnabled.Checked;
            ou.IsBusiness = IsBusiness.Checked;
            ou.IsAdmin = IsAdmin.Checked;
            ou.IsPurchase = IsPurchase.Checked;
            ou.IsFinance = IsFinance.Checked;
            ou.IsDistribution = IsDistribution.Checked;

            //新增模式
            if (ou.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                int serialNumber = sys.Services.OrgUnitService.GetMaxSerialNumber() + 1;
                ou.SerialNumber = serialNumber;
                ou.RoleNumber = ou.ParentID.HasValue ? ou.Parent.RoleNumber + "|" + serialNumber : "0";
                ou.Hierarchy = ou.ParentID.HasValue ? ou.Parent.Hierarchy + 1 : 0;
            }

            return _entityBase;
        }

        private string Check()
        {
            if (string.IsNullOrWhiteSpace(this.tbxName.Text))
                return "部门名称不能为空！";
            if (this.tbxName.Text.Length > 100)
                return "部门名称长度不能大于100！";

            return null;
        }
    }
}