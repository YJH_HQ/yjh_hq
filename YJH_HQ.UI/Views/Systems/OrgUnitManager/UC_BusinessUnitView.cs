﻿using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    public partial class UC_BusinessUnitView : UserControl, IView
    {
        public UC_BusinessUnitView()
        {
            InitializeComponent();

            ddlOrgType.EnumSource(typeof(sys.Enums.OrganizationCompanyType), true);
            txtSort.KeyPress += txtSort_KeyPress;

            this.ddlRegion.PickerView = new YJH_HQ.Controls.RegionTreePickerView.RegionTreePickerView();
            this.ddlRegion.DisplayMember = "Name";
        }

        public sys.Enums.BaseType ViewType
        {
            get { return sys.Enums.BaseType.BusinessUnit; }
        }

        private dps.Data.Mapper.EntityBase _entityBase;
        public void BindData(dps.Data.Mapper.EntityBase _base)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_base;
            sys.Entities.BusinessUnit bu = (sys.Entities.BusinessUnit)ou.Base;

            tbxName.Text = bu.Name;
            tbxShortName.Text = bu.ShortName;
            tbxAddress.Text = bu.Address;
            tbxZipCode.Text = bu.ZipCode;
            tbxFax.Text = bu.Fax;
            tbxTel.Text = bu.Tel1;
            tbxURL.Text = bu.URL;
            txtSort.Text = bu.Sort.HasValue ? bu.Sort.ToString() : "";
            txtOrgCode.Text = bu.OrgCode;
            IsEntityBU.Checked = ou.IsEntityBU.HasValue ? (bool)ou.IsEntityBU : false;
            IsStore.Checked = ou.IsStore.HasValue ? (bool)ou.IsStore : false; ;
            IsEnabled.Checked = ou.IsEnabled;
            IsBusiness.Checked = ou.IsBusiness.HasValue ? (bool)ou.IsBusiness : false;
            IsAdmin.Checked = ou.IsAdmin.HasValue ? (bool)ou.IsAdmin : false;
            IsPurchase.Checked = ou.IsPurchase.HasValue ? (bool)ou.IsPurchase : false;
            IsFinance.Checked = ou.IsFinance.HasValue ? (bool)ou.IsFinance : false;
            IsDistribution.Checked = ou.IsDistribution.HasValue ? (bool)ou.IsDistribution : false;
            ddlOrgType.SelectedValue = bu.OrgType.HasValue ? (int)bu.OrgType : -1;
            if (bu.RegionID.HasValue)
                ddlRegion.SelectedEntity = bu.Region;

            _entityBase = _base;
        }

        public dps.Data.Mapper.EntityBase GetData(out string errorResult, out string text)
        {
            sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)_entityBase;

            errorResult = Check();
            text = this.tbxName.Text;
            if (!string.IsNullOrWhiteSpace(errorResult))
                return null;

            var entity = (dps.Common.Data.Entity)_entityBase.Instance["Base"].Value;
            entity["Name"].Value = this.tbxName.Text;
            entity["ShortName"].Value = this.tbxShortName.Text;
            entity["URL"].Value = this.tbxURL.Text;
            entity["Address"].Value = this.tbxAddress.Text;
            entity["ZipCode"].Value = this.tbxZipCode.Text;
            entity["Fax"].Value = this.tbxFax.Text;
            entity["Tel1"].Value = this.tbxTel.Text;
            entity["OrgCode"].Value = this.txtOrgCode.Text;
            //entity["BankAccount"].Value = this.txtOrgID.Text;
            entity["Sort"].Value = string.IsNullOrWhiteSpace(this.txtSort.Text) ? 0 : int.Parse(this.txtSort.Text);
            entity["OrgType"].Value = (int)ddlOrgType.SelectedValue;
            entity["RegionID"].Value = ddlRegion.SelectedEntityID;
            ou.IsEntityBU = IsEntityBU.Checked;
            ou.IsStore = IsStore.Checked;
            ou.IsEnabled = IsEnabled.Checked;
            ou.IsBusiness = IsBusiness.Checked;
            ou.IsAdmin = IsAdmin.Checked;
            ou.IsPurchase = IsPurchase.Checked;
            ou.IsFinance = IsFinance.Checked;
            ou.IsDistribution = IsDistribution.Checked;

            //新增模式
            if (ou.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
            {
                int serialNumber = sys.Services.OrgUnitService.GetMaxSerialNumber() + 1;
                ou.SerialNumber = serialNumber;
                ou.RoleNumber = ou.ParentID.HasValue ? ou.Parent.RoleNumber + "|" + serialNumber : "0";
                ou.Hierarchy = ou.ParentID.HasValue ? ou.Parent.Hierarchy + 1 : 0;
            }

            return _entityBase;
        }

        private string Check()
        {
            if (string.IsNullOrWhiteSpace(this.tbxName.Text))
                return "公司名称不能为空！";
            return null;
        }

        void txtSort_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0') || (e.KeyChar > '9'))
                e.Handled = true;

            if (e.KeyChar == 8)
                e.Handled = false;
        }
    }
}