﻿
namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    internal class OrgUnitTreeNode : Telerik.WinControls.UI.RadTreeNode
    {
        public const string BUSINESS_UNIT = "sys.BusinessUnit";
        public const string WORK_GROUP = "sys.WorkGroup";
        public const string EMPLOEE = "sys.Emploee";

        /// <summary>
        /// 组织类型，sys.BusinessUnit：公司、sys.WorkGroup：部门、sys.Emploee：员工
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// 组织机构对应实体
        /// </summary>
        public dps.Data.Mapper.EntityBase EntityBase { get; private set; }

        public OrgUnitTreeNode()
        { }

        public OrgUnitTreeNode(string type, dps.Data.Mapper.EntityBase entityBase, string text)
        {
            Type = type;
            EntityBase = entityBase;
            base.Text = text;
            base.Tag = entityBase.Instance.ID;

            switch (type)
            {
                case BUSINESS_UNIT:
                    Image = YJH_HQ.UI.Properties.Resources.Home16;
                    break;
                case WORK_GROUP:
                    Image = YJH_HQ.UI.Properties.Resources.Group16;
                    break;
                case EMPLOEE:
                    Image = YJH_HQ.UI.Properties.Resources.User16;
                    break;
            }
        }
    }
}