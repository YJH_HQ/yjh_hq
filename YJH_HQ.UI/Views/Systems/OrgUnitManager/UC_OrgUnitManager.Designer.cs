﻿namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    partial class UC_OrgUnitManager
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.pv = new Telerik.WinControls.UI.RadPageView();
            this.pvInfo = new Telerik.WinControls.UI.RadPageViewPage();
            this.pvPermission = new Telerik.WinControls.UI.RadPageViewPage();
            this.tvPermissions = new Telerik.WinControls.UI.RadTreeView();
            this.pvProcess = new Telerik.WinControls.UI.RadPageViewPage();
            this.bmTree = new Telerik.WinControls.UI.RadTreeView();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.tvOrgUnits = new Telerik.WinControls.UI.RadTreeView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.btnNewCompany = new Telerik.WinControls.UI.RadMenuItem();
            this.btnNewGroup = new Telerik.WinControls.UI.RadMenuItem();
            this.btnEmploee = new Telerik.WinControls.UI.RadMenuItem();
            this.btnNewEmploee = new Telerik.WinControls.UI.RadMenuItem();
            this.btnExistEmployee = new Telerik.WinControls.UI.RadMenuItem();
            this.btnSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.btnBindExistEmp = new Telerik.WinControls.UI.CommandBarButton();
            this.btnResetPassword = new Telerik.WinControls.UI.CommandBarButton();
            this.btnBindRole = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.tpbInfo = new YJH_HQ.Controls.TipBar.TipBar();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pv)).BeginInit();
            this.pv.SuspendLayout();
            this.pvPermission.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissions)).BeginInit();
            this.pvProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bmTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.pv);
            this.splitPanel2.Location = new System.Drawing.Point(204, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(456, 397);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2378049F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(156, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            // 
            // pv
            // 
            this.pv.Controls.Add(this.pvInfo);
            this.pv.Controls.Add(this.pvPermission);
            this.pv.Controls.Add(this.pvProcess);
            this.pv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pv.Location = new System.Drawing.Point(0, 0);
            this.pv.Name = "pv";
            this.pv.SelectedPage = this.pvInfo;
            this.pv.Size = new System.Drawing.Size(456, 397);
            this.pv.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pv.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pvInfo
            // 
            this.pvInfo.ItemSize = new System.Drawing.SizeF(87F, 28F);
            this.pvInfo.Location = new System.Drawing.Point(10, 37);
            this.pvInfo.Name = "pvInfo";
            this.pvInfo.Size = new System.Drawing.Size(435, 349);
            this.pvInfo.Text = "组织单元属性";
            // 
            // pvPermission
            // 
            this.pvPermission.Controls.Add(this.tvPermissions);
            this.pvPermission.ItemSize = new System.Drawing.SizeF(64F, 28F);
            this.pvPermission.Location = new System.Drawing.Point(10, 33);
            this.pvPermission.Name = "pvPermission";
            this.pvPermission.Size = new System.Drawing.Size(435, 378);
            this.pvPermission.Text = "权限设置";
            // 
            // tvPermissions
            // 
            this.tvPermissions.CheckBoxes = true;
            this.tvPermissions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvPermissions.Location = new System.Drawing.Point(0, 0);
            this.tvPermissions.Name = "tvPermissions";
            this.tvPermissions.ShowLines = true;
            this.tvPermissions.Size = new System.Drawing.Size(435, 378);
            this.tvPermissions.SpacingBetweenNodes = -1;
            this.tvPermissions.TabIndex = 0;
            this.tvPermissions.Text = "radTreeView1";
            this.tvPermissions.TriStateMode = true;
            // 
            // pvProcess
            // 
            this.pvProcess.Controls.Add(this.bmTree);
            this.pvProcess.ItemSize = new System.Drawing.SizeF(87F, 28F);
            this.pvProcess.Location = new System.Drawing.Point(10, 33);
            this.pvProcess.Name = "pvProcess";
            this.pvProcess.Size = new System.Drawing.Size(435, 378);
            this.pvProcess.Text = "拓店过程权限";
            // 
            // bmTree
            // 
            this.bmTree.CheckBoxes = true;
            this.bmTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bmTree.Location = new System.Drawing.Point(0, 0);
            this.bmTree.Name = "bmTree";
            this.bmTree.ShowLines = true;
            this.bmTree.Size = new System.Drawing.Size(435, 378);
            this.bmTree.SpacingBetweenNodes = -1;
            this.bmTree.TabIndex = 0;
            this.bmTree.Text = "radTreeView1";
            this.bmTree.TriStateMode = true;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.tvOrgUnits);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(200, 397);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2378049F, 0F);
            this.splitPanel1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute;
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-156, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            // 
            // tvOrgUnits
            // 
            this.tvOrgUnits.AllowDragDrop = true;
            this.tvOrgUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvOrgUnits.Location = new System.Drawing.Point(0, 0);
            this.tvOrgUnits.Name = "tvOrgUnits";
            this.tvOrgUnits.ShowLines = true;
            this.tvOrgUnits.Size = new System.Drawing.Size(200, 397);
            this.tvOrgUnits.SpacingBetweenNodes = -1;
            this.tvOrgUnits.TabIndex = 0;
            this.tvOrgUnits.Text = "radTreeView1";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 55);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(660, 397);
            this.radSplitContainer1.SplitterWidth = 4;
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnSave,
            this.btnDelete,
            this.commandBarSeparator1,
            this.btnBindExistEmp,
            this.btnResetPassword,
            this.btnBindRole});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.StretchVertically = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新建";
            this.btnNew.AccessibleName = "新建";
            this.btnNew.DisplayName = "commandBarDropDownButton1";
            this.btnNew.DrawText = true;
            this.btnNew.Image = null;
            this.btnNew.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnNewCompany,
            this.btnNewGroup,
            this.btnEmploee});
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnNewCompany
            // 
            this.btnNewCompany.AccessibleDescription = "公司";
            this.btnNewCompany.AccessibleName = "公司";
            this.btnNewCompany.Image = null;
            this.btnNewCompany.Name = "btnNewCompany";
            this.btnNewCompany.Text = "公司";
            this.btnNewCompany.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnNewGroup
            // 
            this.btnNewGroup.AccessibleDescription = "部门";
            this.btnNewGroup.AccessibleName = "部门";
            this.btnNewGroup.Image = null;
            this.btnNewGroup.Name = "btnNewGroup";
            this.btnNewGroup.Text = "部门";
            this.btnNewGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnEmploee
            // 
            this.btnEmploee.AccessibleDescription = "员工";
            this.btnEmploee.AccessibleName = "员工";
            this.btnEmploee.Image = null;
            this.btnEmploee.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnNewEmploee,
            this.btnExistEmployee});
            this.btnEmploee.Name = "btnEmploee";
            this.btnEmploee.Text = "员工";
            this.btnEmploee.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnNewEmploee
            // 
            this.btnNewEmploee.AccessibleDescription = "新用户";
            this.btnNewEmploee.AccessibleName = "新用户";
            this.btnNewEmploee.Name = "btnNewEmploee";
            this.btnNewEmploee.Text = "新用户";
            this.btnNewEmploee.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnExistEmployee
            // 
            this.btnExistEmployee.AccessibleDescription = "绑定现有用户";
            this.btnExistEmployee.AccessibleName = "绑定现有用户";
            this.btnExistEmployee.Name = "btnExistEmployee";
            this.btnExistEmployee.Text = "绑定现有用户";
            this.btnExistEmployee.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleDescription = "保存";
            this.btnSave.AccessibleName = "保存";
            this.btnSave.DisplayName = "commandBarButton1";
            this.btnSave.DrawText = true;
            this.btnSave.Image = null;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "保存";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton2";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = null;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.BackColor = System.Drawing.Color.Silver;
            this.commandBarSeparator1.BackColor2 = System.Drawing.Color.Silver;
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.MinSize = new System.Drawing.Size(2, 0);
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.StretchHorizontally = false;
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // btnBindExistEmp
            // 
            this.btnBindExistEmp.AccessibleDescription = "绑定已存在员工";
            this.btnBindExistEmp.AccessibleName = "绑定已存在员工";
            this.btnBindExistEmp.DisplayName = "commandBarButton1";
            this.btnBindExistEmp.DrawText = true;
            this.btnBindExistEmp.Image = null;
            this.btnBindExistEmp.Name = "btnBindExistEmp";
            this.btnBindExistEmp.Text = "绑定已存在员工";
            this.btnBindExistEmp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBindExistEmp.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnResetPassword
            // 
            this.btnResetPassword.AccessibleDescription = "重置密码";
            this.btnResetPassword.AccessibleName = "重置密码";
            this.btnResetPassword.DisplayName = "commandBarButton2";
            this.btnResetPassword.DrawText = true;
            this.btnResetPassword.Image = null;
            this.btnResetPassword.Name = "btnResetPassword";
            this.btnResetPassword.Text = "重置密码";
            this.btnResetPassword.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnResetPassword.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnBindRole
            // 
            this.btnBindRole.AccessibleDescription = "绑定角色";
            this.btnBindRole.AccessibleName = "绑定角色";
            this.btnBindRole.DisplayName = "commandBarButton1";
            this.btnBindRole.DrawText = true;
            this.btnBindRole.Image = null;
            this.btnBindRole.Name = "btnBindRole";
            this.btnBindRole.Text = "绑定角色";
            this.btnBindRole.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBindRole.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(660, 55);
            this.radCommandBar1.TabIndex = 0;
            // 
            // tpbInfo
            // 
            this.tpbInfo.ClearTime = 10;
            this.tpbInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tpbInfo.Location = new System.Drawing.Point(0, 452);
            this.tpbInfo.Name = "tpbInfo";
            this.tpbInfo.OwnedWindow = null;
            this.tpbInfo.Size = new System.Drawing.Size(660, 25);
            this.tpbInfo.TabIndex = 0;
            // 
            // UC_OrgUnitManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radCommandBar1);
            this.Controls.Add(this.tpbInfo);
            this.Name = "UC_OrgUnitManager";
            this.Size = new System.Drawing.Size(660, 477);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pv)).EndInit();
            this.pv.ResumeLayout(false);
            this.pvPermission.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissions)).EndInit();
            this.pvProcess.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bmTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvOrgUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.RadPageView pv;
        private Telerik.WinControls.UI.RadPageViewPage pvInfo;
        private Telerik.WinControls.UI.RadPageViewPage pvPermission;
        private Telerik.WinControls.UI.RadTreeView tvPermissions;
        private Telerik.WinControls.UI.RadTreeView tvOrgUnits;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarDropDownButton btnNew;
        private Telerik.WinControls.UI.RadMenuItem btnNewCompany;
        private Telerik.WinControls.UI.RadMenuItem btnNewGroup;
        private Telerik.WinControls.UI.RadMenuItem btnEmploee;
        private Telerik.WinControls.UI.CommandBarButton btnSave;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarButton btnBindExistEmp;
        private Telerik.WinControls.UI.CommandBarButton btnResetPassword;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.RadMenuItem btnNewEmploee;
        private Telerik.WinControls.UI.RadMenuItem btnExistEmployee;
        private Telerik.WinControls.UI.CommandBarButton btnBindRole;
        private Controls.TipBar.TipBar tpbInfo;
        private Telerik.WinControls.UI.RadPageViewPage pvProcess;
        private Telerik.WinControls.UI.RadTreeView bmTree;
    }
}