﻿
namespace YJH_HQ.UI.Views.Systems.OrgUnitManager
{
    interface IView
    {
        /// <summary>
        /// 视图类型
        /// </summary>
        sys.Enums.BaseType ViewType { get; }

        /// <summary>
        /// 绑定数据
        /// </summary>
        void BindData(dps.Data.Mapper.EntityBase _base);

        /// <summary>
        /// 获取数据，里面要验证，失败返回null并把失败消息传给errorResult，验证成功则errorResult为null
        /// </summary>
        dps.Data.Mapper.EntityBase GetData(out string errorResult, out string text);
    }
}
