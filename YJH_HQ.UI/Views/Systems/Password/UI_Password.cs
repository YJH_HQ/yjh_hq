﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Views.Systems.Password
{
    public partial class UI_Password : Telerik.WinControls.UI.RadForm
    {
        #region ====属性=====

        private sys.Entities.User _entity;
        public sys.Entities.User Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region =====构造====

        public UI_Password()
        {
            InitializeComponent();
            this.Load += this.Loaded;
            this.btSave.Click += this.btSave_Click;
        }

        #endregion

        #region =====事件====

        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (!Judgement())
                {
                    return;
                }
                var result = sys.Services.UserService.GetUserByEmploeeAccount(Globle.CurrentEmployee.Account);
                sys.Services.UserService.ChangePassword(result, txtConfirmPassword.Text.Trim());
                this.Close();
                //RadMessageBox.Show("保存成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("保存成功！", TipBar.TipType.Information);

            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        private void Loaded(object sender, EventArgs e)
        {
            this.txtUserName.Text = (string)Globle.CurrentEmployee.Account.Replace(@"sys\", "");
            //this.txtPassword.Text = LMS.Services.UserService.GetUserByEmploeeAccount(LMS.UI.SystemService.CurrentEmploee.Account).Password.ToString();
            var result = sys.Services.UserService.GetUserByEmploeeAccount(Globle.CurrentEmployee.Account);
        }

        #endregion

        #region =====方法====

        private void BindData()
        {
            this.txtUserName.Text = (string)Globle.CurrentEmployee.Account.Replace(@"sys\", "");
            //  this.txtPassword.Text = LMS.Services.UserService.GetUserByEmploeeAccount(LMS.UI.SystemService.CurrentEmploee.Account).Password.ToString();
            sys.Services.UserService.GetUserByEmploeeAccount(Globle.CurrentEmployee.Account);
        }
        /// <summary>
        /// 判断
        /// </summary>
        /// <returns></returns>
        public bool Judgement()
        {
            if (string.IsNullOrWhiteSpace(this.txtPassword.Text))
            {
                RadMessageBox.Show("旧密码不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("旧密码不能为空", TipBar.TipType.Warning);
                return false;
            }

            if (!sys.Services.UserService.JudgeOldPwd(this.txtPassword.Text.Trim(), Globle.CurrentEmployee.Account))
            {
                RadMessageBox.Show("旧密码不正确", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("旧密码不正确", TipBar.TipType.Warning);
                return false;
            }
            if (string.IsNullOrWhiteSpace(this.txtNewPassword.Text))
            {
                RadMessageBox.Show("新密码不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("新密码不能为空", TipBar.TipType.Warning);
                return false;
            }
            if (string.IsNullOrWhiteSpace(this.txtConfirmPassword.Text))
            {
                RadMessageBox.Show("确认密码不能为空", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("确认密码不能为空", TipBar.TipType.Warning);
                return false;
            }
            if (this.txtConfirmPassword.Text != this.txtNewPassword.Text)
            {
                RadMessageBox.Show("两次密码不相同", "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipBar1.ShowTip("两次密码不相同", TipBar.TipType.Warning);
                return false;
            }
            return true;
        }

        #endregion
    }
}
