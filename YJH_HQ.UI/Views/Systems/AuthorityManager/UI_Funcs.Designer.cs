﻿namespace YJH_HQ.UI.Views.Systems.AuthorityManager
{
    partial class UI_Funcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnNew = new Telerik.WinControls.UI.CommandBarButton();
            this.btnSave = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.tpbInfo = new YJH_HQ.Controls.TipBar.TipBar();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbxSort = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddlType = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtURL = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(310, 55);
            this.radCommandBar1.TabIndex = 1;
            this.radCommandBar1.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnNew,
            this.btnSave,
            this.btnDelete});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnNew
            // 
            this.btnNew.AccessibleDescription = "新建";
            this.btnNew.AccessibleName = "新建";
            this.btnNew.DisplayName = "commandBarButton1";
            this.btnNew.DrawText = true;
            this.btnNew.Image = global::YJH_HQ.UI.Properties.Resources.Add16;
            this.btnNew.Name = "btnNew";
            this.btnNew.Text = "新建";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleDescription = "保存";
            this.btnSave.AccessibleName = "保存";
            this.btnSave.DisplayName = "commandBarButton2";
            this.btnSave.DrawText = true;
            this.btnSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "保存";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton3";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = global::YJH_HQ.UI.Properties.Resources.Delete16;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // tpbInfo
            // 
            this.tpbInfo.ClearTime = 15;
            this.tpbInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tpbInfo.Location = new System.Drawing.Point(0, 184);
            this.tpbInfo.Name = "tpbInfo";
            this.tpbInfo.OwnedWindow = null;
            this.tpbInfo.Size = new System.Drawing.Size(310, 25);
            this.tpbInfo.TabIndex = 2;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.txtURL);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.tbxSort);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddlType);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 55);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(310, 129);
            this.radGroupBox1.TabIndex = 3;
            // 
            // tbxSort
            // 
            this.tbxSort.Location = new System.Drawing.Point(75, 71);
            this.tbxSort.Mask = "d";
            this.tbxSort.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxSort.Name = "tbxSort";
            this.tbxSort.Size = new System.Drawing.Size(180, 20);
            this.tbxSort.TabIndex = 7;
            this.tbxSort.TabStop = false;
            this.tbxSort.Text = "0";
            // 
            // radLabel5
            // 
            this.radLabel5.ForeColor = System.Drawing.Color.Red;
            this.radLabel5.Location = new System.Drawing.Point(262, 45);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(11, 18);
            this.radLabel5.TabIndex = 5;
            this.radLabel5.Text = "*";
            // 
            // radLabel4
            // 
            this.radLabel4.ForeColor = System.Drawing.Color.Red;
            this.radLabel4.Location = new System.Drawing.Point(262, 21);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(11, 18);
            this.radLabel4.TabIndex = 2;
            this.radLabel4.Text = "*";
            // 
            // ddlType
            // 
            this.ddlType.Enabled = false;
            this.ddlType.Location = new System.Drawing.Point(75, 45);
            this.ddlType.Name = "ddlType";
            this.ddlType.Size = new System.Drawing.Size(180, 20);
            this.ddlType.TabIndex = 4;
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(75, 19);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(180, 20);
            this.tbxName.TabIndex = 1;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(27, 73);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "排序：";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(27, 47);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(40, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "类型：";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(27, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "名称：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(27, 100);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(40, 18);
            this.radLabel6.TabIndex = 8;
            this.radLabel6.Text = "地址：";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(75, 99);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(180, 20);
            this.txtURL.TabIndex = 2;
            // 
            // UI_Funcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 209);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.tpbInfo);
            this.Controls.Add(this.radCommandBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_Funcs";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "功能";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbxSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnNew;
        private Telerik.WinControls.UI.CommandBarButton btnSave;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private YJH_HQ.Controls.TipBar.TipBar tpbInfo;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDropDownList ddlType;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxSort;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox txtURL;
    }
}