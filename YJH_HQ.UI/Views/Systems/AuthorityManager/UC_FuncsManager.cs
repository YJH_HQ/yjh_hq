﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Systems.AuthorityManager
{
    public partial class UC_FuncsManager : YJH_HQ.Controls.UCPageBase.UCPageBase
    {
        List<sys.Entities.Funcs> list;

        #region ==== 构造 ====
        public UC_FuncsManager()
        {
            InitializeComponent();

            this.Load += UC_FuncsManager_Load;
            this.miNewModel.Click += miNewModel_Click;
            this.miNewView.Click += miNewView_Click;
            this.miNewFunc.Click += miNewFunc_Click;
            this.btnEdit.Click += btnEdit_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.btnLoadFuncFromFile.Click += btnLoadFuncFromFile_Click;
            this.tvFunc.SelectedNodeChanged += tvFunc_SelectedNodeChanged;
        }
        #endregion

        #region ==== 事件 ====
        void UC_FuncsManager_Load(object sender, EventArgs e)
        {
            this.miNewFunc.Enabled = false;
            this.miNewView.Enabled = false;
            this.btnLoadFuncFromFile.Enabled = false;
            LoadPermission();
            Search();
        }

        void miNewFunc_Click(object sender, EventArgs e)
        {
            var entity = new sys.Entities.Funcs();
            entity.FuncType = sys.Enums.FuncType.Funcs;
            entity.Parent = ((sys.Entities.Funcs)this.tvFunc.SelectedNode.Tag);
            int count = entity.Parent.SubItems.Count;
            entity.Sort = (count + 1) * 100;
            var ui = new UI_Funcs();
            ui.Owner = CommonService.FindForm(this);
            ui.Entity = entity;
            ui.tvFunc = this.tvFunc;
            ui.TipBar = this.tpbInfo;
            ui.ShowDialog();
        }

        void miNewView_Click(object sender, EventArgs e)
        {
            var entity = new sys.Entities.Funcs();
            entity.FuncType = sys.Enums.FuncType.View;
            entity.Parent = ((sys.Entities.Funcs)this.tvFunc.SelectedNode.Tag);
            int count = entity.Parent.SubItems.Count;
            entity.Sort = (count + 1) * 100;
            var ui = new UI_Funcs();
            ui.Owner = CommonService.FindForm(this);
            ui.Entity = entity;
            ui.tvFunc = this.tvFunc;
            ui.TipBar = this.tpbInfo;
            ui.ShowDialog();
        }

        void miNewModel_Click(object sender, EventArgs e)
        {
            var entity = new sys.Entities.Funcs();
            entity.FuncType = sys.Enums.FuncType.Model;
            int count = list == null ? 0 : list.Where(t => t.Parent == null).Count();
            entity.Sort = (count + 1) * 100;
            var ui = new UI_Funcs();

            ui.Owner = CommonService.FindForm(this);
            ui.Entity = entity;
            ui.tvFunc = this.tvFunc;
            ui.TipBar = this.tpbInfo;
            ui.ShowDialog();
        }

        void tvFunc_SelectedNodeChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            btnLoadFuncFromFile.Enabled = false;
            var entity = (sys.Entities.Funcs)e.Node.Tag;
            switch (entity.FuncType)
            {
                case sys.Enums.FuncType.Model:
                    this.miNewFunc.Enabled = false;
                    this.miNewView.Enabled = true;
                    break;
                case sys.Enums.FuncType.View:
                    this.miNewFunc.Enabled = true;
                    this.miNewView.Enabled = false;
                    btnLoadFuncFromFile.Enabled = true;
                    //注：排除“组织机构管理”，因为它奇葩特殊
                    if (entity.URL.Contains("OrgUnitManager.UC_OrgUnitManager"))
                        btnLoadFuncFromFile.Enabled = false;
                    break;
                case sys.Enums.FuncType.Funcs:
                    this.miNewFunc.Enabled = false;
                    this.miNewView.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.tvFunc.SelectedNode == null)
            {
                this.tpbInfo.ShowTip("请选择编辑的明细", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            Open();
        }

        void dgList_DoubleClick(object sender, EventArgs e)
        {
            if (this.tvFunc.SelectedNode == null)
                return;
            Open();
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.tvFunc.SelectedNode == null)
            {
                this.tpbInfo.ShowTip("请选择删除的记录", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            if (this.tvFunc.SelectedNode.Nodes.Count > 0)
            {
                this.tpbInfo.ShowTip("只允许删除最下级的记录", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            if (Telerik.WinControls.RadMessageBox.Show(this, "确定删除选中记录吗？", "提示信息", MessageBoxButtons.OKCancel, Telerik.WinControls.RadMessageIcon.Question) == DialogResult.Cancel)
                return;
            try
            {
                var row = this.tvFunc.SelectedNode.Tag as sys.Entities.Funcs;
                row.Instance.MarkDeleted();
                sys.Services.FuncsService.Save(row);
                if (row.FuncType == sys.Enums.FuncType.Model && tvFunc.Nodes.Count == 1)
                {
                }
                else
                    this.tvFunc.SelectedNode.Remove();
                this.tpbInfo.ShowTip("删除成功", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("删除失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private List<string> _loadFuncName;

        /// <summary>
        /// 从文件中读取选中的界面按钮功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btnLoadFuncFromFile_Click(object sender, EventArgs e)
        {
            var selectNode = this.tvFunc.SelectedNode;
            if (selectNode == null)
                this.tpbInfo.ShowTip("请先选择界面节点", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
            var entity = selectNode.Tag as sys.Entities.Funcs;
            if (entity == null)
                this.tpbInfo.ShowTip("节点Tag转换失败", YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            if (entity.FuncType != sys.Enums.FuncType.View)
            {
                this.tpbInfo.ShowTip("该功能只针对界面功能", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }
            if (string.IsNullOrWhiteSpace(entity.URL))
            {
                this.tpbInfo.ShowTip("该界面功能地址为空，不能操作", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }

            var viewInstance = Assembly.Load("YJH_HQ.UI").CreateInstance(entity.URL);
            var view = (System.Windows.Forms.UserControl)viewInstance;
            if (view == null)
            {
                this.tpbInfo.ShowTip("没有找到该界面", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return;
            }

            _loadFuncName = new List<string>();
            ReadButton(view);
            //加载完后记得要把反射处理的UserControl释放掉
            view.Dispose();

            //加载成功后，一次性保存到数据库中（原有的权限和已经绑定到员工的权限都会被删除）
            string result = sys.Services.FuncsService.SaveFromReadFile(_loadFuncName, entity.URL);
            this.tpbInfo.ShowTip(result, YJH_HQ.Controls.TipBar.TipBar.TipType.Information);

            //保存后，再次加载
            selectNode.Nodes.Clear();
            var newFuncList = sys.Services.FuncsService.GetSubItemsByURL(entity.URL);
            foreach (var item in newFuncList)
            {
                Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                node.Tag = item;
                node.Text = item.Name;
                selectNode.Nodes.Add(node);
            }
        }

        private void ReadButton(UserControl control)
        {
            var commandBarList = control.Controls.OfType<Telerik.WinControls.UI.RadCommandBar>();
            foreach (var item in commandBarList)
            {
                //先从Rows开始
                if (item.Rows.Count <= 0) continue;

                //一般Rows.Count为1，为了安全这里还是要遍历下
                foreach (var row in item.Rows)
                {
                    if (row.Strips.Count <= 0) continue;

                    //这里只取Strips[0]第一位，因为目前使用的没遇到过Strips.Count大于2的
                    CheckButton(row.Strips[0].Items);
                }
            }
        }
        private void CheckButton(Telerik.WinControls.RadCommandBarBaseItemCollection list)
        {
            //这里只处理CommandBarButton、以及CommandBarDropDownButton下的RadMenuItem，如遇到别的类型则不处理
            foreach (var item in list)
            {
                Type type = item.GetType();
                if (type == typeof(Telerik.WinControls.UI.CommandBarButton))
                    _loadFuncName.Add(item.Text);
                else if (type == typeof(Telerik.WinControls.UI.CommandBarDropDownButton))
                {
                    var dropDown = (Telerik.WinControls.UI.CommandBarDropDownButton)item;
                    foreach (var drop in dropDown.Items)
                        LoopMenuItem((Telerik.WinControls.UI.RadMenuItem)drop);
                }
            }
        }
        private void LoopMenuItem(Telerik.WinControls.UI.RadMenuItem item)
        {
            if (item.Items.Count > 0)
            {
                foreach (var menu in item.Items)
                    LoopMenuItem((Telerik.WinControls.UI.RadMenuItem)menu);
            }
            else
                _loadFuncName.Add(item.Text);
        }
        #endregion

        #region ==== 方法 ====
        void Search()
        {
            try
            {
                list = sys.Services.FuncsService.Search("", 0);
                if (list == null)
                    return;
                foreach (var item in list.Where(t => t.Parent == null).OrderBy(t => t.Sort))
                {
                    LoopAddTreeNode(item, null);
                }
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("查询失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        void LoopAddTreeNode(sys.Entities.Funcs entity, Telerik.WinControls.UI.RadTreeNode ParentNode)
        {
            Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
            node.Tag = entity;
            node.Text = entity.Name;
            if (ParentNode == null)
                this.tvFunc.Nodes.Add(node);
            else
                ParentNode.Nodes.Add(node);
            foreach (var item in list.Where(t => t.ParentID == entity.Instance.ID).OrderBy(t => t.Sort))
            {
                LoopAddTreeNode(item, node);
            }
        }

        void Open()
        {
            var row = this.tvFunc.SelectedNode.Tag as sys.Entities.Funcs;
            UI_Funcs ui = new UI_Funcs();
            ui.Owner = CommonService.FindForm(this);
            ui.Entity = row;
            ui.tvFunc = this.tvFunc;
            ui.TipBar = this.tpbInfo;
            ui.ShowDialog();
        }
        #endregion
    }
}