﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace YJH_HQ.UI.Views.Systems.AuthorityManager
{
    public partial class UI_Funcs : Telerik.WinControls.UI.RadForm
    {
        #region ==== 字段 ====
        internal Telerik.WinControls.UI.RadTreeView tvFunc;
        internal YJH_HQ.Controls.TipBar.TipBar TipBar;
        #endregion

        #region ==== 属性 ====
        private sys.Entities.Funcs _Entity;

        public sys.Entities.Funcs Entity
        {
            get { return _Entity; }
            set
            {
                _Entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ==== 构造 ====
        public UI_Funcs()
        {
            InitializeComponent();

            this.Load += UI_Funcs_Load;
            this.btnNew.Click += btnNew_Click;
            this.btnSave.Click += btnSave_Click;
            this.btnDelete.Click += btnDelete_Click;
        }
        #endregion

        #region ==== 事件 ====
        void UI_Funcs_Load(object sender, EventArgs e)
        {
            var q = from em in Enum.GetValues(typeof(sys.Enums.FuncType)).Cast<int>()
                    select new
                    {
                        Text = Enum.GetName(typeof(sys.Enums.FuncType), em),
                        Value = em
                    };
            this.ddlType.DataSource = q;
            this.ddlType.DisplayMember = "Text";
            this.ddlType.ValueMember = "Value";

            BindData();
        }

        void btnNew_Click(object sender, EventArgs e)
        {
            Entity = new sys.Entities.Funcs();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidData())
                    return;
                var entityStateBeforeSaved = dps.Common.Data.PersistentState.Detached;
                if (Entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
                    entityStateBeforeSaved = dps.Common.Data.PersistentState.Modified;
                FlushData();
                sys.Services.FuncsService.Save(Entity);
                Entity.Instance.AcceptChanges();
                RefreshList(Entity, entityStateBeforeSaved);
                if (TipBar != null)
                    TipBar.ShowTip("保存成功", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("保存失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (Entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                return;
            if (Telerik.WinControls.RadMessageBox.Show(this, "确定删除吗？", "提示信息", MessageBoxButtons.OKCancel, Telerik.WinControls.RadMessageIcon.Question) == DialogResult.Cancel)
                return;
            try
            {
                Entity.Instance.MarkDeleted();
                sys.Services.FuncsService.Save(Entity);
                Entity = new sys.Entities.Funcs();
                RefreshList(Entity, dps.Common.Data.PersistentState.Deleted);
                this.tpbInfo.ShowTip("删除成功", YJH_HQ.Controls.TipBar.TipBar.TipType.Information);
            }
            catch (Exception ex)
            {
                this.tpbInfo.ShowTip("删除失败\r\n" + ex.Message, YJH_HQ.Controls.TipBar.TipBar.TipType.Error);
            }
        }
        #endregion

        #region ==== 方法 ====
        void BindData()
        {
            this.tbxName.Text = Entity.Name;
            this.ddlType.SelectedValue = (int)Entity.FuncType;
            this.tbxSort.Value = Entity.Sort;

            txtURL.Text = Entity.URL;
            txtURL.Enabled = false;
            if (Entity.FuncType == sys.Enums.FuncType.View)
                txtURL.Enabled = true;
        }

        void FlushData()
        {
            Entity.Name = this.tbxName.Text;
            Entity.Sort = this.tbxSort.Value == null ? 0 : Convert.ToInt32(this.tbxSort.Value);
            Entity.FuncType = (sys.Enums.FuncType)this.ddlType.SelectedValue;
            if (Entity.FuncType == sys.Enums.FuncType.View)
                Entity.URL = txtURL.Text;
        }

        bool ValidData()
        {
            if (string.IsNullOrWhiteSpace(this.tbxName.Text))
            {
                this.tpbInfo.ShowTip("请填写权限名称", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return false;
            }
            if (this.ddlType.SelectedValue == null)
            {
                this.tpbInfo.ShowTip("请选择权限类型", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return false;
            }
            //视图模式下必须填写URL
            if (Entity.FuncType == sys.Enums.FuncType.View && string.IsNullOrWhiteSpace(txtURL.Text))
            {
                this.tpbInfo.ShowTip("请填写地址", YJH_HQ.Controls.TipBar.TipBar.TipType.Warning);
                return false;
            }

            return true;
        }

        void RefreshList(sys.Entities.Funcs entity, dps.Common.Data.PersistentState entityStateBeforeSaved)
        {
            if (this.tvFunc == null)
                return;
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                Telerik.WinControls.UI.RadTreeNode node = new Telerik.WinControls.UI.RadTreeNode();
                node.Text = Entity.Name;
                node.Tag = Entity;
                switch (Entity.FuncType)
                {
                    case sys.Enums.FuncType.Model:
                        this.tvFunc.Nodes.Add(node);
                        break;
                    case sys.Enums.FuncType.View:
                        this.tvFunc.SelectedNode.Nodes.Add(node);
                        break;
                    case sys.Enums.FuncType.Funcs:
                        this.tvFunc.SelectedNode.Nodes.Add(node);
                        break;
                    default:
                        break;
                }
                node.Selected = true;
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                this.tvFunc.SelectedNode.Text = Entity.Name;
            }
            else
            {
                tvFunc.SelectedNode.Remove();
            }
        }
        #endregion
    }
}