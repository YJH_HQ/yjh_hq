﻿namespace YJH_HQ.UI.Views.Systems.AuthorityManager
{
    partial class UC_FuncsManager
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarDropDownButton1 = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.miNewModel = new Telerik.WinControls.UI.RadMenuItem();
            this.miNewView = new Telerik.WinControls.UI.RadMenuItem();
            this.miNewFunc = new Telerik.WinControls.UI.RadMenuItem();
            this.btnEdit = new Telerik.WinControls.UI.CommandBarButton();
            this.btnDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.btnLoadFuncFromFile = new Telerik.WinControls.UI.CommandBarButton();
            this.tvFunc = new Telerik.WinControls.UI.RadTreeView();
            this.tpbInfo = new YJH_HQ.Controls.TipBar.TipBar();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvFunc)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(926, 55);
            this.radCommandBar1.TabIndex = 1;
            this.radCommandBar1.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Enabled = true;
            this.commandBarStripElement1.EnableDragging = false;
            this.commandBarStripElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            // 
            // 
            // 
            this.commandBarStripElement1.Grip.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarDropDownButton1,
            this.btnEdit,
            this.btnDelete,
            this.btnLoadFuncFromFile});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.commandBarStripElement1.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.commandBarStripElement1.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // commandBarDropDownButton1
            // 
            this.commandBarDropDownButton1.AccessibleDescription = "新建";
            this.commandBarDropDownButton1.AccessibleName = "新建";
            this.commandBarDropDownButton1.DisplayName = "commandBarDropDownButton1";
            this.commandBarDropDownButton1.DrawText = true;
            this.commandBarDropDownButton1.Image = global::YJH_HQ.UI.Properties.Resources.Add16;
            this.commandBarDropDownButton1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.miNewModel,
            this.miNewView,
            this.miNewFunc});
            this.commandBarDropDownButton1.Name = "commandBarDropDownButton1";
            this.commandBarDropDownButton1.Text = "新建";
            this.commandBarDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.commandBarDropDownButton1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // miNewModel
            // 
            this.miNewModel.AccessibleDescription = "新建模块功能";
            this.miNewModel.AccessibleName = "新建模块功能";
            this.miNewModel.Name = "miNewModel";
            this.miNewModel.Text = "新建模块功能";
            this.miNewModel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // miNewView
            // 
            this.miNewView.AccessibleDescription = "新建界面功能";
            this.miNewView.AccessibleName = "新建界面功能";
            this.miNewView.Name = "miNewView";
            this.miNewView.Text = "新建界面功能";
            this.miNewView.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // miNewFunc
            // 
            this.miNewFunc.AccessibleDescription = "新建功能";
            this.miNewFunc.AccessibleName = "新建功能";
            this.miNewFunc.Name = "miNewFunc";
            this.miNewFunc.Text = "新建功能";
            this.miNewFunc.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnEdit
            // 
            this.btnEdit.AccessibleDescription = "修改密码";
            this.btnEdit.AccessibleName = "修改密码";
            this.btnEdit.DisplayName = "commandBarButton2";
            this.btnEdit.DrawText = true;
            this.btnEdit.Image = global::YJH_HQ.UI.Properties.Resources.Notepad16;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Text = "编辑";
            this.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "删除";
            this.btnDelete.AccessibleName = "删除";
            this.btnDelete.DisplayName = "commandBarButton3";
            this.btnDelete.DrawText = true;
            this.btnDelete.Image = global::YJH_HQ.UI.Properties.Resources.Delete16;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "删除";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnLoadFuncFromFile
            // 
            this.btnLoadFuncFromFile.AccessibleDescription = "从文件中读取权限";
            this.btnLoadFuncFromFile.AccessibleName = "从文件中读取权限";
            this.btnLoadFuncFromFile.DisplayName = "commandBarButton1";
            this.btnLoadFuncFromFile.DrawText = true;
            this.btnLoadFuncFromFile.Image = global::YJH_HQ.UI.Properties.Resources.file16;
            this.btnLoadFuncFromFile.Name = "btnLoadFuncFromFile";
            this.btnLoadFuncFromFile.Text = "从文件中读取权限并保存";
            this.btnLoadFuncFromFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLoadFuncFromFile.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tvFunc
            // 
            this.tvFunc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFunc.Location = new System.Drawing.Point(0, 55);
            this.tvFunc.Name = "tvFunc";
            this.tvFunc.Size = new System.Drawing.Size(926, 489);
            this.tvFunc.SpacingBetweenNodes = -1;
            this.tvFunc.TabIndex = 8;
            this.tvFunc.Text = "radTreeView1";
            // 
            // tpbInfo
            // 
            this.tpbInfo.ClearTime = 15;
            this.tpbInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tpbInfo.Location = new System.Drawing.Point(0, 544);
            this.tpbInfo.Name = "tpbInfo";
            this.tpbInfo.OwnedWindow = null;
            this.tpbInfo.Size = new System.Drawing.Size(926, 25);
            this.tpbInfo.TabIndex = 7;
            // 
            // UC_FuncsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tvFunc);
            this.Controls.Add(this.tpbInfo);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "UC_FuncsManager";
            this.Size = new System.Drawing.Size(926, 569);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvFunc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnEdit;
        private Telerik.WinControls.UI.CommandBarButton btnDelete;
        private YJH_HQ.Controls.TipBar.TipBar tpbInfo;
        private Telerik.WinControls.UI.RadTreeView tvFunc;
        private Telerik.WinControls.UI.CommandBarDropDownButton commandBarDropDownButton1;
        private Telerik.WinControls.UI.RadMenuItem miNewModel;
        private Telerik.WinControls.UI.RadMenuItem miNewView;
        private Telerik.WinControls.UI.RadMenuItem miNewFunc;
        private Telerik.WinControls.UI.CommandBarButton btnLoadFuncFromFile;
    }
}
