﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Car
{
    public partial class UI_ad_car_use : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private List<YJH.Entities.hr_emploee> hr_emploeeList = YJH.Services.ad_car_useService.Gethr_emploee();
        private List<YJH.Entities.ad_car_info> carinfoList = YJH.Services.ad_car_useService.Getad_car_info();
        private YJH.Entities.ad_car_use _entity;
        public YJH.Entities.ad_car_use Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ====构造====

        public UI_ad_car_use()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddldriver.MouseUp += Ddldriver_MouseUp;
            this.ddllicence_no.MouseUp += Ddllicence_no_MouseUp;
            SetCombox();
        }

        private void Ddllicence_no_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ddllicence_no.Text))
            {
                this.ddllicence_no.DisplayMember = "licence_no";
                this.ddllicence_no.ValueMember = "licence_no";
                this.ddllicence_no.DataSource = carinfoList;
                this.ddllicence_no.SelectedIndex = -1;
            }
            else
            {
                var eList = carinfoList.Where(f => f.licence_no.Contains(this.ddllicence_no.Text));
                this.ddllicence_no.DisplayMember = "licence_no";
                this.ddllicence_no.ValueMember = "licence_no";
                this.ddllicence_no.DataSource = eList;
                this.ddllicence_no.SelectedIndex = -1;
            }
        }

        private void Ddldriver_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ddldriver.Text))
            {
                this.ddldriver.DisplayMember = "Name";
                this.ddldriver.ValueMember = "Name";
                this.ddldriver.DataSource = hr_emploeeList;
                this.ddldriver.SelectedIndex = -1;
            }
            else
            {
                var eList = hr_emploeeList.Where(f => f.Name.Contains(this.ddldriver.Text));
                this.ddldriver.DisplayMember = "Name";
                this.ddldriver.ValueMember = "Name";
                this.ddldriver.DataSource = eList;
                this.ddldriver.SelectedIndex = -1;
            }
        }

        #endregion

        #region ====方法====
        private void SetCombox()
        {
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.ddltype.SelectedIndex = 0;
            this.dtuse_date.Value = DateTime.Now;
            this.dtstart_time.Value = DateTime.Now;

        }

        private void BindData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                return;
            this.tbxCompany_id.Text = (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, _entity.Company_id))).Name;
            this.ddllicence_no.Text = _entity.licence_no;
            this.ddldriver.Text = _entity.driver;
            this.tbxuse_no.Text = _entity.use_no;
            this.dtuse_date.Value = _entity.use_date;
            if (_entity.type == 10)
                this.ddltype.Text = "本市";
            else
                this.ddltype.Text = "长途";

            this.tbxdest.Text = _entity.dest;
            this.tbxforWhat.Text = _entity.forWhat;
            this.tbxstart_km.Text = _entity.start_km.ToString();
            this.dtstart_time.Value = _entity.start_time;
            this.tbxend_km.Text = _entity.end_km.ToString();
            if (_entity.end_time != null)
                this.dtend_time.Value = (DateTime)_entity.end_time;
            this.tbxother.Text = _entity.other;
            this.tbxtravel_fee.Text = _entity.travel_fee.ToString();
            this.tbxremark.Text = _entity.remark;
        }

        private void FlushData()
        {
            _entity.licence_no = this.ddllicence_no.Text;
            _entity.driver = this.ddldriver.Text;
            _entity.use_no = this.tbxuse_no.Text;
            _entity.use_date = this.dtuse_date.Value;

            if (this.ddltype.Text == "本市")
                _entity.type = 10;
            else
                _entity.type = 20;
            _entity.dest = this.tbxdest.Text;
            _entity.forWhat = this.tbxforWhat.Text;
            _entity.start_km = Convert.ToDecimal(this.tbxstart_km.Text);
            _entity.start_time = this.dtstart_time.Value;
            _entity.end_km = Convert.ToDecimal(this.tbxend_km.Text);
            if (!string.IsNullOrWhiteSpace(this.dtend_time.Text))
                _entity.end_time = this.dtend_time.Value;
            _entity.other = this.tbxother.Text;
            _entity.travel_fee = Convert.ToDecimal(this.tbxtravel_fee.Text);
            _entity.remark = this.tbxremark.Text;
        }

        private void OnLoad()
        {
            this.LoadPermission();
            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.BindData();
            }
            else
            {
                this.New();
            }
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_car_use entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, entity.Company_id))).Name);
                data.Add("licence_no", entity.licence_no);
                data.Add("driver", entity.driver);
                data.Add("use_no", entity.use_no);
                data.Add("use_date", entity.use_date);
                data.Add("type", entity.type);
                data.Add("dest", entity.dest);
                data.Add("forWhat", entity.forWhat);
                data.Add("start_km", entity.start_km);
                data.Add("start_time", entity.start_time);
                data.Add("end_km", entity.end_km);
                if(entity.end_time!=null)
                    data.Add("end_time", entity.end_time);
                data.Add("other", entity.other);
                data.Add("travel_fee", entity.travel_fee);
                data.Add("remark", entity.remark);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, entity.Company_id))).Name);
                data.Add("licence_no", entity.licence_no);
                data.Add("driver", entity.driver);
                data.Add("use_no", entity.use_no);
                data.Add("use_date", entity.use_date);
                data.Add("type", entity.type);
                data.Add("dest", entity.dest);
                data.Add("forWhat", entity.forWhat);
                data.Add("start_km", entity.start_km);
                data.Add("start_time", entity.start_time);
                data.Add("end_km", entity.end_km);
                if (entity.end_time != null)
                    data.Add("end_time", entity.end_time);
                data.Add("other", entity.other);
                data.Add("travel_fee", entity.travel_fee);
                data.Add("remark", entity.remark);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        private void New()
        {
            _entity = new YJH.Entities.ad_car_use();
            this._entity.Company_id = Globle.CurrentBusinessUnit.Instance.ID;
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.ddllicence_no.Text = "";
            this.ddldriver.Text = "";
            this.tbxuse_no.Text = "";
            this.dtuse_date.Value = DateTime.Now;
            this.ddltype.SelectedIndex = 0;
            this.tbxdest.Text = "";
            this.tbxforWhat.Text = "";
            this.tbxstart_km.Text = "0.00";
            this.dtstart_time.Value= DateTime.Now;
            this.tbxend_km.Text = "0.00";
            this.dtend_time.DateTimePickerElement.SetToNullValue();
            this.tbxother.Text = "";
            this.tbxtravel_fee.Text = "0.00";
            this.tbxremark.Text = "";
        }

        private void Save()
        {
            try
            {
                if (Validate())
                {
                    this.FlushData();
                    //记录保存前的实体状态
                    var entityState = _entity.Instance.PersistentState;
                    YJH.Services.ad_car_useService.Save(_entity);
                    _entity.Instance.AcceptChanges();
                    //刷新列表
                    this.RefreshList(entityState, _entity);
                    this.Close();
                    //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        bool Validate()
        {
            if (string.IsNullOrWhiteSpace(this.ddllicence_no.Text))
            {
                MessageBox.Show("请选择车牌号码");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddldriver.Text))
            {
                MessageBox.Show("请选择驾驶人");
                return false;
            }
            else if(string.IsNullOrWhiteSpace(this.tbxuse_no.Text))
            {
                MessageBox.Show("请输入用车单号");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddltype.Text))
            {
                MessageBox.Show("请选择出车类型");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxdest.Text))
            {
                MessageBox.Show("请输入目的地");
                return false;
            }
            else if (Convert.ToDecimal(this.tbxstart_km.Text) < 0)
            {
                MessageBox.Show("出车时公里数必须大于0");
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
