﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Car
{
    public partial class UI_ad_car_info : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private List<YJH.Entities.hr_emploee> hr_emploeeList = YJH.Services.ad_car_infoService.Gethr_emploee();
        private YJH.Entities.ad_car_info _entity;
        public YJH.Entities.ad_car_info Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ====构造====

        public UI_ad_car_info()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddlliabler.MouseUp += Ddlliabler_MouseUp;
            SetCombox();
        }

        private void Ddlliabler_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ddlliabler.Text))
            {
                this.ddlliabler.DisplayMember = "Name";
                this.ddlliabler.ValueMember = "Name";
                this.ddlliabler.DataSource = hr_emploeeList;
                this.ddlliabler.SelectedIndex = -1;
            }
            else
            {
                var eList = hr_emploeeList.Where(f => f.Name.Contains(this.ddlliabler.Text));
                this.ddlliabler.DisplayMember = "Name";
                this.ddlliabler.ValueMember = "Name";
                this.ddlliabler.DataSource = eList;
                this.ddlliabler.SelectedIndex = -1;
            }
        }

        #endregion

        #region ====方法====
        private void SetCombox()
        {
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.tbxstatus.Text = "正常";
            this.ddltype.SelectedIndex = 0;
            this.dtreg_date.Value = DateTime.Now;

        }

        private void BindData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                return;
            this.tbxCompany_id.Text = (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID,_entity.Company_id))).Name;
            this.tbxlicence_no.Text = _entity.licence_no;

            if (_entity.status == 10)
                this.tbxstatus.Text = "正常";
            else
                this.tbxstatus.Text = "禁用";
            this.tbxengine.Text = _entity.engine;
            this.dtreg_date.Value = _entity.reg_date;

            if (_entity.type == 10)
                this.ddltype.Text = "小车";
            else if (_entity.type == 20)
                this.ddltype.Text = "面包车";
            else
                this.ddltype.Text = "其它";
            this.tbxbrand.Text = _entity.brand;
            this.tbxpersons.Text = _entity.persons.ToString();
            this.tbxcolor.Text = _entity.color;
            this.ddlliabler.Text = _entity.liabler;
        }

        private void FlushData()
        {
            _entity.licence_no = this.tbxlicence_no.Text;
            _entity.engine = this.tbxengine.Text;
            _entity.reg_date = this.dtreg_date.Value;

            if (this.ddltype.Text == "小车")
                _entity.type = 10;
            else if (this.ddltype.Text == "面包车")
                _entity.type = 20;
            else
                _entity.type = 30;
            _entity.brand = this.tbxbrand.Text;
            _entity.persons=Convert.ToInt32( this.tbxpersons.Text);
            _entity.color = this.tbxcolor.Text;
            _entity.liabler = this.ddlliabler.Text;
        }

        private void OnLoad()
        {
            this.LoadPermission();
            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.BindData();
            }
            else
            {
                this.New();
            }
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_car_info entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, entity.Company_id))).Name);
                data.Add("licence_no", entity.licence_no);
                data.Add("brand", entity.brand);
                data.Add("type", entity.type);
                data.Add("engine", entity.engine);
                data.Add("reg_date", entity.reg_date);
                data.Add("persons", entity.persons);
                data.Add("status", entity.status);
                data.Add("color", entity.color);
                data.Add("liabler", entity.liabler);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, entity.Company_id))).Name);
                data.Add("licence_no", entity.licence_no);
                data.Add("brand", entity.brand);
                data.Add("type", entity.type);
                data.Add("engine", entity.engine);
                data.Add("reg_date", entity.reg_date);
                data.Add("persons", entity.persons);
                data.Add("status", entity.status);
                data.Add("color", entity.color);
                data.Add("liabler", entity.liabler);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        private void New()
        {
            _entity = new YJH.Entities.ad_car_info();
            this._entity.Company_id = Globle.CurrentBusinessUnit.Instance.ID;
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.tbxlicence_no.Text = "";
            this._entity.status = 10;
            this.tbxstatus.Text = "正常";
            this.tbxengine.Text = "";
            this.dtreg_date.Value = DateTime.Now;
            this.ddltype.SelectedIndex = 0;
            this.tbxbrand.Text = "";
            this.tbxpersons.Text = "0";
            this.ddlliabler.Text = "";
        }

        private void Save()
        {
            try
            {
                if (Validate())
                {
                    this.FlushData();
                    //记录保存前的实体状态
                    var entityState = _entity.Instance.PersistentState;
                    YJH.Services.ad_car_infoService.Save(_entity);
                    _entity.Instance.AcceptChanges();
                    //刷新列表
                    this.RefreshList(entityState, _entity);
                    this.Close();
                    //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        bool Validate()
        {
            if (string.IsNullOrWhiteSpace(this.tbxlicence_no.Text))
            {
                MessageBox.Show("请输入车牌号码");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxengine.Text))
            {
                MessageBox.Show("请输入发动机号码");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddltype.Text))
            {
                MessageBox.Show("请选择车辆类型");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxbrand.Text))
            {
                MessageBox.Show("请输入厂牌型号");
                return false;
            }
            else if (Convert.ToInt32( this.tbxpersons.Text)<0)
            {
                MessageBox.Show("车载人数必须大于0");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.tbxcolor.Text))
            {
                MessageBox.Show("请输入车辆颜色");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddlliabler.Text))
            {
                MessageBox.Show("请选择责任人");
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
