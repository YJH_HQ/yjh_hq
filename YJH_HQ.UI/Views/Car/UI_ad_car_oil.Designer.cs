﻿namespace YJH_HQ.UI.Car
{
    partial class UI_ad_car_oil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddltype = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.tbxoil_price = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxremark = new Telerik.WinControls.UI.RadTextBox();
            this.tbxoil_qty = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.tbxoil_km = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.ddllicence_no = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.ddloperater = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtuse_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.ddloil_type = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tbxoil_amt = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxCompany_id = new Telerik.WinControls.UI.RadTextBox();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxremark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_km)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddllicence_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            this.radLabel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddloperater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtuse_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddloil_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_amt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(85, 82);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "油品：";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel25);
            this.radGroupBox1.Controls.Add(this.ddltype);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.radLabel24);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.tbxoil_price);
            this.radGroupBox1.Controls.Add(this.tbxremark);
            this.radGroupBox1.Controls.Add(this.tbxoil_qty);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel20);
            this.radGroupBox1.Controls.Add(this.tbxoil_km);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.ddllicence_no);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.ddloperater);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.dtuse_date);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.ddloil_type);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.tbxoil_amt);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbxCompany_id);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(529, 320);
            this.radGroupBox1.TabIndex = 3;
            // 
            // ddltype
            // 
            this.ddltype.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem5.Text = "计划加油";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "现金加油";
            radListDataItem6.TextWrap = true;
            this.ddltype.Items.Add(radListDataItem5);
            this.ddltype.Items.Add(radListDataItem6);
            this.ddltype.Location = new System.Drawing.Point(336, 80);
            this.ddltype.Name = "ddltype";
            this.ddltype.Size = new System.Drawing.Size(119, 20);
            this.ddltype.TabIndex = 43;
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(291, 82);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(42, 18);
            this.radLabel15.TabIndex = 42;
            this.radLabel15.Text = "类型：";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel13.ForeColor = System.Drawing.Color.Red;
            this.radLabel13.Location = new System.Drawing.Point(255, 108);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(13, 21);
            this.radLabel13.TabIndex = 47;
            this.radLabel13.Text = "*";
            // 
            // radLabel24
            // 
            this.radLabel24.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel24.ForeColor = System.Drawing.Color.Red;
            this.radLabel24.Location = new System.Drawing.Point(237, 55);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(13, 21);
            this.radLabel24.TabIndex = 47;
            this.radLabel24.Text = "*";
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(85, 188);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(42, 18);
            this.radLabel23.TabIndex = 16;
            this.radLabel23.Text = "备注：";
            // 
            // tbxoil_price
            // 
            this.tbxoil_price.Location = new System.Drawing.Point(131, 107);
            this.tbxoil_price.Mask = "f2";
            this.tbxoil_price.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxoil_price.Name = "tbxoil_price";
            this.tbxoil_price.Size = new System.Drawing.Size(104, 20);
            this.tbxoil_price.TabIndex = 57;
            this.tbxoil_price.TabStop = false;
            this.tbxoil_price.Text = "0.00";
            this.tbxoil_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxremark
            // 
            this.tbxremark.AutoSize = false;
            this.tbxremark.Location = new System.Drawing.Point(131, 186);
            this.tbxremark.MaxLength = 10;
            this.tbxremark.Multiline = true;
            this.tbxremark.Name = "tbxremark";
            this.tbxremark.Size = new System.Drawing.Size(324, 74);
            this.tbxremark.TabIndex = 15;
            // 
            // tbxoil_qty
            // 
            this.tbxoil_qty.Location = new System.Drawing.Point(336, 107);
            this.tbxoil_qty.Mask = "f2";
            this.tbxoil_qty.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxoil_qty.Name = "tbxoil_qty";
            this.tbxoil_qty.Size = new System.Drawing.Size(119, 20);
            this.tbxoil_qty.TabIndex = 55;
            this.tbxoil_qty.TabStop = false;
            this.tbxoil_qty.Text = "0.00";
            this.tbxoil_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(85, 109);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(42, 18);
            this.radLabel22.TabIndex = 56;
            this.radLabel22.Text = "单价：";
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(267, 109);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(65, 18);
            this.radLabel20.TabIndex = 54;
            this.radLabel20.Text = "加油升数：";
            // 
            // tbxoil_km
            // 
            this.tbxoil_km.Location = new System.Drawing.Point(131, 133);
            this.tbxoil_km.Mask = "f2";
            this.tbxoil_km.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxoil_km.Name = "tbxoil_km";
            this.tbxoil_km.Size = new System.Drawing.Size(104, 20);
            this.tbxoil_km.TabIndex = 51;
            this.tbxoil_km.TabStop = false;
            this.tbxoil_km.Text = "0.00";
            this.tbxoil_km.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(38, 135);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(89, 18);
            this.radLabel19.TabIndex = 50;
            this.radLabel19.Text = "加油时公里数：";
            // 
            // ddllicence_no
            // 
            this.ddllicence_no.Location = new System.Drawing.Point(131, 56);
            this.ddllicence_no.Name = "ddllicence_no";
            this.ddllicence_no.Size = new System.Drawing.Size(104, 20);
            this.ddllicence_no.TabIndex = 49;
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel17.ForeColor = System.Drawing.Color.Red;
            this.radLabel17.Location = new System.Drawing.Point(255, 132);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(13, 21);
            this.radLabel17.TabIndex = 46;
            this.radLabel17.Text = "*";
            // 
            // radLabel14
            // 
            this.radLabel14.Controls.Add(this.radLabel12);
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel14.ForeColor = System.Drawing.Color.Red;
            this.radLabel14.Location = new System.Drawing.Point(237, 84);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(13, 21);
            this.radLabel14.TabIndex = 46;
            this.radLabel14.Text = "*";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel12.ForeColor = System.Drawing.Color.Red;
            this.radLabel12.Location = new System.Drawing.Point(0, 21);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(13, 21);
            this.radLabel12.TabIndex = 46;
            this.radLabel12.Text = "*";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel11.ForeColor = System.Drawing.Color.Red;
            this.radLabel11.Location = new System.Drawing.Point(461, 56);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(13, 21);
            this.radLabel11.TabIndex = 46;
            this.radLabel11.Text = "*";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel10.ForeColor = System.Drawing.Color.Red;
            this.radLabel10.Location = new System.Drawing.Point(461, 82);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(13, 21);
            this.radLabel10.TabIndex = 46;
            this.radLabel10.Text = "*";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel9.ForeColor = System.Drawing.Color.Red;
            this.radLabel9.Location = new System.Drawing.Point(461, 159);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(13, 21);
            this.radLabel9.TabIndex = 46;
            this.radLabel9.Text = "*";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel8.ForeColor = System.Drawing.Color.Red;
            this.radLabel8.Location = new System.Drawing.Point(489, 107);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(13, 21);
            this.radLabel8.TabIndex = 46;
            this.radLabel8.Text = "*";
            // 
            // ddloperater
            // 
            this.ddloperater.Location = new System.Drawing.Point(336, 159);
            this.ddloperater.Name = "ddloperater";
            this.ddloperater.Size = new System.Drawing.Size(119, 20);
            this.ddloperater.TabIndex = 47;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(278, 161);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 18);
            this.radLabel2.TabIndex = 46;
            this.radLabel2.Text = "经办人：";
            // 
            // dtuse_date
            // 
            this.dtuse_date.Location = new System.Drawing.Point(336, 56);
            this.dtuse_date.Name = "dtuse_date";
            this.dtuse_date.Size = new System.Drawing.Size(119, 20);
            this.dtuse_date.TabIndex = 44;
            this.dtuse_date.TabStop = false;
            this.dtuse_date.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(268, 58);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "加油日期：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(62, 58);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(65, 18);
            this.radLabel6.TabIndex = 43;
            this.radLabel6.Text = "车牌号码：";
            // 
            // ddloil_type
            // 
            this.ddloil_type.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "92号";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "95号";
            radListDataItem2.TextWrap = true;
            radListDataItem8.Text = "其它";
            radListDataItem8.TextWrap = true;
            this.ddloil_type.Items.Add(radListDataItem1);
            this.ddloil_type.Items.Add(radListDataItem2);
            this.ddloil_type.Items.Add(radListDataItem8);
            this.ddloil_type.Location = new System.Drawing.Point(131, 80);
            this.ddloil_type.Name = "ddloil_type";
            this.ddloil_type.Size = new System.Drawing.Size(104, 20);
            this.ddloil_type.TabIndex = 41;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(291, 133);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(42, 18);
            this.radLabel5.TabIndex = 14;
            this.radLabel5.Text = "金额：";
            // 
            // tbxoil_amt
            // 
            this.tbxoil_amt.Enabled = false;
            this.tbxoil_amt.Location = new System.Drawing.Point(336, 133);
            this.tbxoil_amt.MaxLength = 10;
            this.tbxoil_amt.Name = "tbxoil_amt";
            this.tbxoil_amt.Size = new System.Drawing.Size(119, 20);
            this.tbxoil_amt.TabIndex = 13;
            this.tbxoil_amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(62, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "所属公司：";
            // 
            // tbxCompany_id
            // 
            this.tbxCompany_id.Enabled = false;
            this.tbxCompany_id.Location = new System.Drawing.Point(131, 32);
            this.tbxCompany_id.Name = "tbxCompany_id";
            this.tbxCompany_id.Size = new System.Drawing.Size(324, 20);
            this.tbxCompany_id.TabIndex = 11;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(529, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabel25
            // 
            this.radLabel25.Location = new System.Drawing.Point(461, 109);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(18, 18);
            this.radLabel25.TabIndex = 58;
            this.radLabel25.Text = "升";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(461, 133);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(18, 18);
            this.radLabel3.TabIndex = 59;
            this.radLabel3.Text = "元";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(237, 110);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(18, 18);
            this.radLabel4.TabIndex = 60;
            this.radLabel4.Text = "元";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(237, 133);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(22, 18);
            this.radLabel16.TabIndex = 61;
            this.radLabel16.Text = "km";
            // 
            // UI_ad_car_oil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(529, 350);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_ad_car_oil";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "车辆加油";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxremark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_km)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddllicence_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            this.radLabel14.ResumeLayout(false);
            this.radLabel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddloperater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtuse_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddloil_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxoil_amt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox tbxCompany_id;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox tbxoil_amt;
        private Telerik.WinControls.UI.RadDropDownList ddloil_type;
        private Telerik.WinControls.UI.RadDateTimePicker dtuse_date;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadDropDownList ddloperater;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList ddllicence_no;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxoil_price;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxoil_qty;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxoil_km;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbxremark;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadDropDownList ddltype;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel25;
    }
}