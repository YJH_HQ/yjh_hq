﻿namespace YJH_HQ.UI.Car
{
    partial class UI_ad_car_info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel51 = new Telerik.WinControls.UI.RadLabel();
            this.ddlliabler = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbxpersons = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.dtreg_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.tbxengine = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddltype = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxbrand = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxcolor = new Telerik.WinControls.UI.RadTextBox();
            this.tbxlicence_no = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxCompany_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstatus = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlliabler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxpersons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtreg_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxengine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbrand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcolor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlicence_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(602, 72);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "车辆类型：";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel51);
            this.radGroupBox1.Controls.Add(this.ddlliabler);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.tbxpersons);
            this.radGroupBox1.Controls.Add(this.dtreg_date);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.tbxengine);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddltype);
            this.radGroupBox1.Controls.Add(this.tbxbrand);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxcolor);
            this.radGroupBox1.Controls.Add(this.tbxlicence_no);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbxCompany_id);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.tbxstatus);
            this.radGroupBox1.Controls.Add(this.radLabel30);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(815, 202);
            this.radGroupBox1.TabIndex = 3;
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel17.ForeColor = System.Drawing.Color.Red;
            this.radLabel17.Location = new System.Drawing.Point(237, 107);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(13, 21);
            this.radLabel17.TabIndex = 46;
            this.radLabel17.Text = "*";
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel16.ForeColor = System.Drawing.Color.Red;
            this.radLabel16.Location = new System.Drawing.Point(785, 109);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(13, 21);
            this.radLabel16.TabIndex = 46;
            this.radLabel16.Text = "*";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel15.ForeColor = System.Drawing.Color.Red;
            this.radLabel15.Location = new System.Drawing.Point(785, 69);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(13, 21);
            this.radLabel15.TabIndex = 46;
            this.radLabel15.Text = "*";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel14.ForeColor = System.Drawing.Color.Red;
            this.radLabel14.Location = new System.Drawing.Point(785, 33);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(13, 21);
            this.radLabel14.TabIndex = 46;
            this.radLabel14.Text = "*";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel12.ForeColor = System.Drawing.Color.Red;
            this.radLabel12.Location = new System.Drawing.Point(588, 109);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(13, 21);
            this.radLabel12.TabIndex = 46;
            this.radLabel12.Text = "*";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel11.ForeColor = System.Drawing.Color.Red;
            this.radLabel11.Location = new System.Drawing.Point(588, 69);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(13, 21);
            this.radLabel11.TabIndex = 46;
            this.radLabel11.Text = "*";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel10.ForeColor = System.Drawing.Color.Red;
            this.radLabel10.Location = new System.Drawing.Point(588, 31);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(13, 21);
            this.radLabel10.TabIndex = 46;
            this.radLabel10.Text = "*";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel9.ForeColor = System.Drawing.Color.Red;
            this.radLabel9.Location = new System.Drawing.Point(389, 107);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(13, 21);
            this.radLabel9.TabIndex = 46;
            this.radLabel9.Text = "*";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel8.ForeColor = System.Drawing.Color.Red;
            this.radLabel8.Location = new System.Drawing.Point(389, 69);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(13, 21);
            this.radLabel8.TabIndex = 46;
            this.radLabel8.Text = "*";
            // 
            // radLabel51
            // 
            this.radLabel51.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel51.ForeColor = System.Drawing.Color.Red;
            this.radLabel51.Location = new System.Drawing.Point(389, 31);
            this.radLabel51.Name = "radLabel51";
            this.radLabel51.Size = new System.Drawing.Size(13, 21);
            this.radLabel51.TabIndex = 48;
            this.radLabel51.Text = "*";
            // 
            // ddlliabler
            // 
            this.ddlliabler.Location = new System.Drawing.Point(673, 110);
            this.ddlliabler.Name = "ddlliabler";
            this.ddlliabler.Size = new System.Drawing.Size(106, 20);
            this.ddlliabler.TabIndex = 47;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(615, 112);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 18);
            this.radLabel2.TabIndex = 46;
            this.radLabel2.Text = "责任人：";
            // 
            // tbxpersons
            // 
            this.tbxpersons.Location = new System.Drawing.Point(315, 108);
            this.tbxpersons.Mask = "d0";
            this.tbxpersons.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxpersons.Name = "tbxpersons";
            this.tbxpersons.Size = new System.Drawing.Size(70, 20);
            this.tbxpersons.TabIndex = 45;
            this.tbxpersons.TabStop = false;
            this.tbxpersons.Text = "0";
            this.tbxpersons.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dtreg_date
            // 
            this.dtreg_date.Location = new System.Drawing.Point(475, 70);
            this.dtreg_date.Name = "dtreg_date";
            this.dtreg_date.Size = new System.Drawing.Size(107, 20);
            this.dtreg_date.TabIndex = 44;
            this.dtreg_date.TabStop = false;
            this.dtreg_date.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(409, 72);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "注册日期：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(50, 72);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(77, 18);
            this.radLabel6.TabIndex = 43;
            this.radLabel6.Text = "发动机号码：";
            // 
            // tbxengine
            // 
            this.tbxengine.Location = new System.Drawing.Point(131, 70);
            this.tbxengine.MaxLength = 30;
            this.tbxengine.Name = "tbxengine";
            this.tbxengine.Size = new System.Drawing.Size(254, 20);
            this.tbxengine.TabIndex = 42;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(62, 110);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 18);
            this.radLabel4.TabIndex = 14;
            this.radLabel4.Text = "厂牌型号：";
            // 
            // ddltype
            // 
            this.ddltype.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "小车";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "面包车";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "其它";
            radListDataItem3.TextWrap = true;
            this.ddltype.Items.Add(radListDataItem1);
            this.ddltype.Items.Add(radListDataItem2);
            this.ddltype.Items.Add(radListDataItem3);
            this.ddltype.Location = new System.Drawing.Point(673, 70);
            this.ddltype.Name = "ddltype";
            this.ddltype.Size = new System.Drawing.Size(106, 20);
            this.ddltype.TabIndex = 41;
            // 
            // tbxbrand
            // 
            this.tbxbrand.Location = new System.Drawing.Point(131, 108);
            this.tbxbrand.MaxLength = 30;
            this.tbxbrand.Name = "tbxbrand";
            this.tbxbrand.Size = new System.Drawing.Size(105, 20);
            this.tbxbrand.TabIndex = 13;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(409, 110);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 18);
            this.radLabel5.TabIndex = 14;
            this.radLabel5.Text = "车辆颜色：";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(409, 34);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "车牌号码：";
            // 
            // tbxcolor
            // 
            this.tbxcolor.Location = new System.Drawing.Point(475, 110);
            this.tbxcolor.MaxLength = 10;
            this.tbxcolor.Name = "tbxcolor";
            this.tbxcolor.Size = new System.Drawing.Size(107, 20);
            this.tbxcolor.TabIndex = 13;
            // 
            // tbxlicence_no
            // 
            this.tbxlicence_no.Location = new System.Drawing.Point(475, 32);
            this.tbxlicence_no.MaxLength = 7;
            this.tbxlicence_no.Name = "tbxlicence_no";
            this.tbxlicence_no.Size = new System.Drawing.Size(107, 20);
            this.tbxlicence_no.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(62, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "所属公司：";
            // 
            // tbxCompany_id
            // 
            this.tbxCompany_id.Enabled = false;
            this.tbxCompany_id.Location = new System.Drawing.Point(131, 32);
            this.tbxCompany_id.Name = "tbxCompany_id";
            this.tbxCompany_id.Size = new System.Drawing.Size(254, 20);
            this.tbxCompany_id.TabIndex = 11;
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(256, 110);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(65, 18);
            this.radLabel13.TabIndex = 12;
            this.radLabel13.Text = "车载人数：";
            // 
            // tbxstatus
            // 
            this.tbxstatus.Enabled = false;
            this.tbxstatus.Location = new System.Drawing.Point(673, 34);
            this.tbxstatus.Name = "tbxstatus";
            this.tbxstatus.Size = new System.Drawing.Size(106, 20);
            this.tbxstatus.TabIndex = 1;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(625, 34);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(42, 18);
            this.radLabel30.TabIndex = 2;
            this.radLabel30.Text = "状态：";
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(815, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // UI_ad_car_info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(815, 232);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_ad_car_info";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "车辆信息";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlliabler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxpersons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtreg_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxengine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxbrand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxcolor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxlicence_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox tbxCompany_id;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox tbxstatus;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox tbxcolor;
        private Telerik.WinControls.UI.RadTextBox tbxlicence_no;
        private Telerik.WinControls.UI.RadDropDownList ddltype;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxpersons;
        private Telerik.WinControls.UI.RadDateTimePicker dtreg_date;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox tbxengine;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxbrand;
        private Telerik.WinControls.UI.RadDropDownList ddlliabler;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel51;
    }
}