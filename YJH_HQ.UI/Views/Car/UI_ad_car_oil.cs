﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Car
{
    public partial class UI_ad_car_oil : Telerik.WinControls.UI.RadForm
    {
        #region ====属性====
        internal YJH_HQ.Controls.DataGrid.DataGrid dgList = null;
        private List<YJH.Entities.hr_emploee> hr_emploeeList = YJH.Services.ad_car_oilService.Gethr_emploee();
        private List<YJH.Entities.ad_car_info> carinfoList = YJH.Services.ad_car_oilService.Getad_car_info();
        private YJH.Entities.ad_car_oil _entity;
        public YJH.Entities.ad_car_oil Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
                this.BindData();
            }
        }

        #endregion

        #region ====构造====

        public UI_ad_car_oil()
        {
            InitializeComponent();
            this.Load += (s, e) => { OnLoad(); };
            this.btSave.Click += (s, e) => { Save(); };
            this.ddloperater.MouseUp += Ddloperater_MouseUp;
            this.ddllicence_no.MouseUp += Ddllicence_no_MouseUp;
            this.tbxoil_qty.TextChanged += Tbxoil_qty_TextChanged;
            SetCombox();
        }

        private void Tbxoil_qty_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(this.tbxoil_price.Text) > 0 && Convert.ToDecimal(this.tbxoil_qty.Text) > 0)
            {
                this.tbxoil_amt.Text = (Convert.ToDecimal(this.tbxoil_price.Text) * Convert.ToDecimal(this.tbxoil_qty.Text)).ToString();
            }
        }

        private void Ddllicence_no_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ddllicence_no.Text))
            {
                this.ddllicence_no.DisplayMember = "licence_no";
                this.ddllicence_no.ValueMember = "licence_no";
                this.ddllicence_no.DataSource = carinfoList;
                this.ddllicence_no.SelectedIndex = -1;
            }
            else
            {
                var eList = carinfoList.Where(f => f.licence_no.Contains(this.ddllicence_no.Text));
                this.ddllicence_no.DisplayMember = "licence_no";
                this.ddllicence_no.ValueMember = "licence_no";
                this.ddllicence_no.DataSource = eList;
                this.ddllicence_no.SelectedIndex = -1;
            }
        }

        private void Ddloperater_MouseUp(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ddloperater.Text))
            {
                this.ddloperater.DisplayMember = "Name";
                this.ddloperater.ValueMember = "Name";
                this.ddloperater.DataSource = hr_emploeeList;
                this.ddloperater.SelectedIndex = -1;
            }
            else
            {
                var eList = hr_emploeeList.Where(f => f.Name.Contains(this.ddloperater.Text));
                this.ddloperater.DisplayMember = "Name";
                this.ddloperater.ValueMember = "Name";
                this.ddloperater.DataSource = eList;
                this.ddloperater.SelectedIndex = -1;
            }
        }

        #endregion

        #region ====方法====
        private void SetCombox()
        {
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.ddloil_type.SelectedIndex = 0;
            this.ddltype.SelectedIndex = 0;
            this.dtuse_date.Value = DateTime.Now;
            this.tbxoil_amt.Text = "0";
        }

        private void BindData()
        {
            if (_entity.Instance.PersistentState == dps.Common.Data.PersistentState.Detached)
                return;
            this.tbxCompany_id.Text = (new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.Data.Entity.Retrieve(sys.Entities.BusinessUnit.EntityModelID, _entity.Company_id))).Name;
            this.ddllicence_no.Text = _entity.licence_no;
            this.ddloperater.Text = _entity.operater;
            this.dtuse_date.Value = _entity.use_date;
            if (_entity.oil_type == 10)
                this.ddloil_type.Text = "92号";
            else if (_entity.oil_type == 20)
                this.ddloil_type.Text = "95号";
            else
                this.ddloil_type.Text = "其它";
            if (_entity.type == 10)
                this.ddltype.Text = "计划加油";
            else
                this.ddltype.Text = "现金加油";
            this.tbxoil_price.Text = _entity.oil_price.ToString();
            this.tbxoil_qty.Text = _entity.oil_qty.ToString();
            this.tbxoil_km.Text = _entity.oil_km.ToString();
            this.tbxoil_amt.Text = _entity.oil_amt.ToString();
            this.tbxremark.Text = _entity.remark;
        }

        private void FlushData()
        {
            _entity.licence_no = this.ddllicence_no.Text;
            _entity.use_date = this.dtuse_date.Value;
            if (this.ddloil_type.Text == "92号")
                _entity.oil_type = 10;
            else if (this.ddloil_type.Text == "95号")
                _entity.oil_type = 20;
            else
                _entity.oil_type = 30;
            if (this.ddltype.Text == "计划加油")
                _entity.type = 10;
            else
                _entity.type = 20;
            _entity.oil_km = Convert.ToDecimal(this.tbxoil_km.Text);
            _entity.oil_qty = Convert.ToDecimal(this.tbxoil_qty.Text);
            _entity.oil_price = Convert.ToDecimal(this.tbxoil_price.Text);
            _entity.oil_amt = Convert.ToDecimal(this.tbxoil_amt.Text);
            _entity.operater = this.ddloperater.Text;
            _entity.remark = this.tbxremark.Text;
        }

        private void OnLoad()
        {
            this.LoadPermission();
            if (_entity.Instance.PersistentState != dps.Common.Data.PersistentState.Detached)
            {
                this.BindData();
            }
            else
            {
                this.New();
            }
        }

        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = LMS.Services.OrgUnitService.GetFuncByParentName(LMS.UI.SystemService.CurrentEmpOu.Instance.ID, "行业类型设置");

            //this.btSave.Enabled = funcs.Exists(t => t.Name == "保存");
        }

        private void RefreshList(dps.Common.Data.PersistentState entityStateBeforeSaved, YJH.Entities.ad_car_oil entity)
        {
            //刷新列表
            if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Detached)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", this.tbxCompany_id.Text);
                data.Add("licence_no", entity.licence_no);
                data.Add("use_date", entity.use_date);
                data.Add("oil_type", entity.oil_type);
                data.Add("type", entity.type);
                data.Add("oil_km", entity.oil_km);
                data.Add("oil_qty", entity.oil_qty);
                data.Add("oil_price", entity.oil_price);
                data.Add("oil_amt", entity.oil_amt);
                data.Add("operater", entity.operater);
                data.Add("remark", entity.remark);
                this.dgList.AddRow(data);
            }
            else if (entityStateBeforeSaved == dps.Common.Data.PersistentState.Modified)
            {
                var data = new Dictionary<string, object>();
                data.Add("ID", entity.Instance.ID);
                data.Add("Company_id", this.tbxCompany_id.Text);
                data.Add("licence_no", entity.licence_no);
                data.Add("use_date", entity.use_date);
                data.Add("oil_type", entity.oil_type);
                data.Add("type", entity.type);
                data.Add("oil_km", entity.oil_km);
                data.Add("oil_qty", entity.oil_qty);
                data.Add("oil_price", entity.oil_price);
                data.Add("oil_amt", entity.oil_amt);
                data.Add("operater", entity.operater);
                data.Add("remark", entity.remark);
                this.dgList.RefreshSelectedRow(data);
            }
        }

        #endregion

        #region ====事件====
        private void New()
        {
            _entity = new YJH.Entities.ad_car_oil();
            this._entity.Company_id = Globle.CurrentBusinessUnit.Instance.ID;
            this.tbxCompany_id.Text = Globle.CurrentBusinessUnit.Name;
            this.ddllicence_no.Text = "";
            this.ddloperater.Text = "";
            this.dtuse_date.Value = DateTime.Now;
            this.ddloil_type.SelectedIndex = 0;
            this.ddltype.SelectedIndex = 0;
            this.tbxoil_km.Text = "0.00";
            this.tbxoil_qty.Text = "0.00";
            this.tbxoil_price.Text = "0.00";
            this.tbxoil_amt.Text = "0";
            this.tbxremark.Text = "";
        }

        private void Save()
        {
            try
            {
                if (Validate())
                {
                    this.FlushData();
                    //记录保存前的实体状态
                    var entityState = _entity.Instance.PersistentState;
                    YJH.Services.ad_car_oilService.Save(_entity);
                    _entity.Instance.AcceptChanges();
                    //刷新列表
                    this.RefreshList(entityState, _entity);
                    this.Close();
                    //this.tipbar.ShowTip("保存成功！", TipBar.TipType.Information);
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(this, "保存失败：\r\n" + ex.Message, "错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                //this.tipbar.ShowTip("保存失败！", ex.Message, TipBar.TipType.Error);
            }
        }

        bool Validate()
        {
            if (string.IsNullOrWhiteSpace(this.ddllicence_no.Text))
            {
                MessageBox.Show("请选择车牌号码");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddloil_type.Text))
            {
                MessageBox.Show("请选择油品");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddltype.Text))
            {
                MessageBox.Show("请选择类型");
                return false;
            }
            else if (Convert.ToDecimal(this.tbxoil_price.Text) < 0)
            {
                MessageBox.Show("单价必须大于0");
                return false;
            }
            else if (Convert.ToDecimal(this.tbxoil_qty.Text) < 0)
            {
                MessageBox.Show("加油升数必须大于0");
                return false;
            }
            else if (Convert.ToDecimal(this.tbxoil_km.Text) < 0)
            {
                MessageBox.Show("加油时公里数必须大于0");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(this.ddloperater.Text))
            {
                MessageBox.Show("请选择经办人");
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
