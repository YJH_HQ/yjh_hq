﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Car
{
    public partial class UC_ad_car_oil : UserControl
    {
        #region ===字段及属性===

        #endregion

        #region ===构造方法===
        public UC_ad_car_oil()
        {
            InitializeComponent();
            this.dgOil.QueryMethod = "YJH.ad_car_oilService.Query";
            this.dgOil.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += UC_ad_car_info_Load;
            this.btnModify.Click += (s, e) => { this.OnOpen(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnDelete.Click += (s, e) => { this.Delete(); };
            this.btnSearch.Click += (s, e) => { this.dgOil.LoadData(); };
            this.dgOil.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
            this.dgOil.GridView.CellFormatting += GridView_CellFormatting;
            this.LoadPermission();
        }

        #endregion

        #region ===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            //this.btnModify.Enabled = funcs.Exists(t => t.Name == "修改");
            //this.btnSend.Enabled = funcs.Exists(t => t.Name == "新单");
        }

        //修改
        private void OnOpen()
        {
            var id = this.dgOil.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ad_car_oil(dps.Common.Data.Entity.Retrieve(YJH.Entities.ad_car_oil.EntityModelID, id));
            var view = new UI_ad_car_oil();
            view.dgList = this.dgOil;
            view.Entity = entity;
            view.ShowDialog();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = this.tbxName.Text;
            return args;
        }

        /// <summary>
        /// 新建
        /// </summary>
        private void New()
        {
            var entity = new YJH.Entities.ad_car_oil();
            var view = new UI_ad_car_oil();
            view.dgList = this.dgOil;
            view.Entity = entity;
            view.ShowDialog();
        }
        //删除
        private void Delete()
        {
            var id = this.dgOil.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (RadMessageBox.Show("确定删除数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                YJH.Services.ad_car_oilService.Delete(id);
                this.dgOil.GridView.SelectedRows[0].Delete();
                this.dgOil.GridView.Refresh();
                RadMessageBox.Show("删除成功!");
            }
        }

        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载vava
        /// </summary>
        /// <remarks>建立人：张殿元 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_ad_car_info_Load(object sender, EventArgs e)
        {
            this.dgOil.LoadData();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgOil == null)
            {
                return;
            }
            switch (this.dgOil.GridView.Rows[e.RowIndex].Cells["type"].Value.ToString())
            {
                case "10":
                    this.dgOil.GridView.Rows[e.RowIndex].Cells["type1"].Value = "计划加油";
                    break;
                case "20":
                    this.dgOil.GridView.Rows[e.RowIndex].Cells["type1"].Value = "现金加油";
                    break;
            }
            switch (this.dgOil.GridView.Rows[e.RowIndex].Cells["oil_type"].Value.ToString())
            {
                case "10":
                    this.dgOil.GridView.Rows[e.RowIndex].Cells["oil_type1"].Value = "92号";
                    break;
                case "20":
                    this.dgOil.GridView.Rows[e.RowIndex].Cells["oil_type1"].Value = "95号";
                    break;
                case "30":
                    this.dgOil.GridView.Rows[e.RowIndex].Cells["oil_type1"].Value = "其它";
                    break;
            }
        }
        #endregion
    }
}
