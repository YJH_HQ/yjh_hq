﻿namespace YJH_HQ.UI.Car
{
    partial class UI_ad_car_use
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.tbxtravel_fee = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbxremark = new Telerik.WinControls.UI.RadTextBox();
            this.tbxend_km = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.dtend_time = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.tbxstart_km = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.dtstart_time = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbxdest = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.ddllicence_no = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.ddldriver = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtuse_date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddltype = new Telerik.WinControls.UI.RadDropDownList();
            this.tbxforWhat = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.tbxother = new Telerik.WinControls.UI.RadTextBox();
            this.tbxuse_no = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxCompany_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btSave = new Telerik.WinControls.UI.CommandBarButton();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxtravel_fee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxremark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxend_km)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtend_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstart_km)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtstart_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddllicence_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            this.radLabel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddldriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtuse_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxforWhat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxother)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxuse_no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(62, 108);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "出车类型：";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel26);
            this.radGroupBox1.Controls.Add(this.radLabel25);
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.radLabel24);
            this.radGroupBox1.Controls.Add(this.radLabel23);
            this.radGroupBox1.Controls.Add(this.tbxtravel_fee);
            this.radGroupBox1.Controls.Add(this.tbxremark);
            this.radGroupBox1.Controls.Add(this.tbxend_km);
            this.radGroupBox1.Controls.Add(this.radLabel22);
            this.radGroupBox1.Controls.Add(this.radLabel20);
            this.radGroupBox1.Controls.Add(this.dtend_time);
            this.radGroupBox1.Controls.Add(this.radLabel21);
            this.radGroupBox1.Controls.Add(this.tbxstart_km);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.dtstart_time);
            this.radGroupBox1.Controls.Add(this.tbxdest);
            this.radGroupBox1.Controls.Add(this.radLabel18);
            this.radGroupBox1.Controls.Add(this.ddllicence_no);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.ddldriver);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.dtuse_date);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddltype);
            this.radGroupBox1.Controls.Add(this.tbxforWhat);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.tbxother);
            this.radGroupBox1.Controls.Add(this.tbxuse_no);
            this.radGroupBox1.Controls.Add(this.label1);
            this.radGroupBox1.Controls.Add(this.tbxCompany_id);
            this.radGroupBox1.Controls.Add(this.radLabel30);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(531, 412);
            this.radGroupBox1.TabIndex = 3;
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel13.ForeColor = System.Drawing.Color.Red;
            this.radLabel13.Location = new System.Drawing.Point(237, 106);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(13, 21);
            this.radLabel13.TabIndex = 47;
            this.radLabel13.Text = "*";
            // 
            // radLabel24
            // 
            this.radLabel24.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel24.ForeColor = System.Drawing.Color.Red;
            this.radLabel24.Location = new System.Drawing.Point(237, 55);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(13, 21);
            this.radLabel24.TabIndex = 47;
            this.radLabel24.Text = "*";
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(85, 299);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(42, 18);
            this.radLabel23.TabIndex = 16;
            this.radLabel23.Text = "备注：";
            // 
            // tbxtravel_fee
            // 
            this.tbxtravel_fee.Location = new System.Drawing.Point(131, 273);
            this.tbxtravel_fee.Mask = "f2";
            this.tbxtravel_fee.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxtravel_fee.Name = "tbxtravel_fee";
            this.tbxtravel_fee.Size = new System.Drawing.Size(72, 20);
            this.tbxtravel_fee.TabIndex = 57;
            this.tbxtravel_fee.TabStop = false;
            this.tbxtravel_fee.Text = "0.00";
            this.tbxtravel_fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxremark
            // 
            this.tbxremark.AutoSize = false;
            this.tbxremark.Location = new System.Drawing.Point(131, 297);
            this.tbxremark.MaxLength = 10;
            this.tbxremark.Multiline = true;
            this.tbxremark.Name = "tbxremark";
            this.tbxremark.Size = new System.Drawing.Size(324, 74);
            this.tbxremark.TabIndex = 15;
            // 
            // tbxend_km
            // 
            this.tbxend_km.Location = new System.Drawing.Point(131, 223);
            this.tbxend_km.Mask = "f2";
            this.tbxend_km.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxend_km.Name = "tbxend_km";
            this.tbxend_km.Size = new System.Drawing.Size(72, 20);
            this.tbxend_km.TabIndex = 55;
            this.tbxend_km.TabStop = false;
            this.tbxend_km.Text = "0.00";
            this.tbxend_km.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(62, 275);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(65, 18);
            this.radLabel22.TabIndex = 56;
            this.radLabel22.Text = "出车费用：";
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(38, 225);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(89, 18);
            this.radLabel20.TabIndex = 54;
            this.radLabel20.Text = "回车时公里数：";
            // 
            // dtend_time
            // 
            this.dtend_time.Location = new System.Drawing.Point(325, 223);
            this.dtend_time.Name = "dtend_time";
            this.dtend_time.Size = new System.Drawing.Size(130, 20);
            this.dtend_time.TabIndex = 53;
            this.dtend_time.TabStop = false;
            this.dtend_time.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel21
            // 
            this.radLabel21.Location = new System.Drawing.Point(256, 225);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(65, 18);
            this.radLabel21.TabIndex = 52;
            this.radLabel21.Text = "回车时间：";
            // 
            // tbxstart_km
            // 
            this.tbxstart_km.Location = new System.Drawing.Point(131, 197);
            this.tbxstart_km.Mask = "f2";
            this.tbxstart_km.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.tbxstart_km.Name = "tbxstart_km";
            this.tbxstart_km.Size = new System.Drawing.Size(72, 20);
            this.tbxstart_km.TabIndex = 51;
            this.tbxstart_km.TabStop = false;
            this.tbxstart_km.Text = "0.00";
            this.tbxstart_km.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel19
            // 
            this.radLabel19.Location = new System.Drawing.Point(38, 199);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(89, 18);
            this.radLabel19.TabIndex = 50;
            this.radLabel19.Text = "出车时公里数：";
            // 
            // dtstart_time
            // 
            this.dtstart_time.Location = new System.Drawing.Point(325, 197);
            this.dtstart_time.Name = "dtstart_time";
            this.dtstart_time.Size = new System.Drawing.Size(130, 20);
            this.dtstart_time.TabIndex = 46;
            this.dtstart_time.TabStop = false;
            this.dtstart_time.Value = new System.DateTime(((long)(0)));
            // 
            // tbxdest
            // 
            this.tbxdest.Location = new System.Drawing.Point(325, 106);
            this.tbxdest.MaxLength = 7;
            this.tbxdest.Name = "tbxdest";
            this.tbxdest.Size = new System.Drawing.Size(130, 20);
            this.tbxdest.TabIndex = 14;
            // 
            // radLabel18
            // 
            this.radLabel18.Location = new System.Drawing.Point(256, 199);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(65, 18);
            this.radLabel18.TabIndex = 45;
            this.radLabel18.Text = "出车时间：";
            // 
            // ddllicence_no
            // 
            this.ddllicence_no.Location = new System.Drawing.Point(131, 56);
            this.ddllicence_no.Name = "ddllicence_no";
            this.ddllicence_no.Size = new System.Drawing.Size(104, 20);
            this.ddllicence_no.TabIndex = 49;
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel17.ForeColor = System.Drawing.Color.Red;
            this.radLabel17.Location = new System.Drawing.Point(237, 197);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(13, 21);
            this.radLabel17.TabIndex = 46;
            this.radLabel17.Text = "*";
            // 
            // radLabel14
            // 
            this.radLabel14.Controls.Add(this.radLabel12);
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel14.ForeColor = System.Drawing.Color.Red;
            this.radLabel14.Location = new System.Drawing.Point(237, 84);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(13, 21);
            this.radLabel14.TabIndex = 46;
            this.radLabel14.Text = "*";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel12.ForeColor = System.Drawing.Color.Red;
            this.radLabel12.Location = new System.Drawing.Point(0, 21);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(13, 21);
            this.radLabel12.TabIndex = 46;
            this.radLabel12.Text = "*";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel11.ForeColor = System.Drawing.Color.Red;
            this.radLabel11.Location = new System.Drawing.Point(461, 56);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(13, 21);
            this.radLabel11.TabIndex = 46;
            this.radLabel11.Text = "*";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel10.ForeColor = System.Drawing.Color.Red;
            this.radLabel10.Location = new System.Drawing.Point(461, 82);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(13, 21);
            this.radLabel10.TabIndex = 46;
            this.radLabel10.Text = "*";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel9.ForeColor = System.Drawing.Color.Red;
            this.radLabel9.Location = new System.Drawing.Point(461, 197);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(13, 21);
            this.radLabel9.TabIndex = 46;
            this.radLabel9.Text = "*";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel8.ForeColor = System.Drawing.Color.Red;
            this.radLabel8.Location = new System.Drawing.Point(461, 105);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(13, 21);
            this.radLabel8.TabIndex = 46;
            this.radLabel8.Text = "*";
            // 
            // ddldriver
            // 
            this.ddldriver.Location = new System.Drawing.Point(325, 56);
            this.ddldriver.Name = "ddldriver";
            this.ddldriver.Size = new System.Drawing.Size(130, 20);
            this.ddldriver.TabIndex = 47;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(267, 58);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 18);
            this.radLabel2.TabIndex = 46;
            this.radLabel2.Text = "驾驶人：";
            // 
            // dtuse_date
            // 
            this.dtuse_date.Location = new System.Drawing.Point(325, 82);
            this.dtuse_date.Name = "dtuse_date";
            this.dtuse_date.Size = new System.Drawing.Size(130, 20);
            this.dtuse_date.TabIndex = 44;
            this.dtuse_date.TabStop = false;
            this.dtuse_date.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(256, 84);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(65, 18);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.Text = "申请日期：";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(62, 58);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(65, 18);
            this.radLabel6.TabIndex = 43;
            this.radLabel6.Text = "车牌号码：";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(85, 134);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(42, 18);
            this.radLabel4.TabIndex = 14;
            this.radLabel4.Text = "用途：";
            // 
            // ddltype
            // 
            this.ddltype.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "本市";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "长途";
            radListDataItem2.TextWrap = true;
            this.ddltype.Items.Add(radListDataItem1);
            this.ddltype.Items.Add(radListDataItem2);
            this.ddltype.Location = new System.Drawing.Point(131, 106);
            this.ddltype.Name = "ddltype";
            this.ddltype.Size = new System.Drawing.Size(104, 20);
            this.ddltype.TabIndex = 41;
            // 
            // tbxforWhat
            // 
            this.tbxforWhat.AutoSize = false;
            this.tbxforWhat.Location = new System.Drawing.Point(131, 132);
            this.tbxforWhat.MaxLength = 200;
            this.tbxforWhat.Multiline = true;
            this.tbxforWhat.Name = "tbxforWhat";
            this.tbxforWhat.Size = new System.Drawing.Size(324, 59);
            this.tbxforWhat.TabIndex = 13;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(62, 249);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 18);
            this.radLabel5.TabIndex = 14;
            this.radLabel5.Text = "随便人员：";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(62, 84);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 18);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "用车单号：";
            // 
            // tbxother
            // 
            this.tbxother.Location = new System.Drawing.Point(131, 249);
            this.tbxother.MaxLength = 10;
            this.tbxother.Name = "tbxother";
            this.tbxother.Size = new System.Drawing.Size(324, 20);
            this.tbxother.TabIndex = 13;
            // 
            // tbxuse_no
            // 
            this.tbxuse_no.Location = new System.Drawing.Point(131, 82);
            this.tbxuse_no.MaxLength = 7;
            this.tbxuse_no.Name = "tbxuse_no";
            this.tbxuse_no.Size = new System.Drawing.Size(104, 20);
            this.tbxuse_no.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(62, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "所属公司：";
            // 
            // tbxCompany_id
            // 
            this.tbxCompany_id.Enabled = false;
            this.tbxCompany_id.Location = new System.Drawing.Point(131, 32);
            this.tbxCompany_id.Name = "tbxCompany_id";
            this.tbxCompany_id.Size = new System.Drawing.Size(324, 20);
            this.tbxCompany_id.TabIndex = 11;
            // 
            // radLabel30
            // 
            this.radLabel30.Location = new System.Drawing.Point(267, 108);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(54, 18);
            this.radLabel30.TabIndex = 2;
            this.radLabel30.Text = "目的地：";
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Text = "";
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.radCommandBar2.Size = new System.Drawing.Size(531, 30);
            this.radCommandBar2.TabIndex = 4;
            this.radCommandBar2.Text = "radCommandBar2";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.AutoSize = true;
            this.commandBarRowElement3.CanFocus = true;
            this.commandBarRowElement3.ClipDrawing = false;
            this.commandBarRowElement3.ClipText = false;
            this.commandBarRowElement3.DrawBorder = false;
            this.commandBarRowElement3.DrawFill = true;
            this.commandBarRowElement3.DrawText = false;
            this.commandBarRowElement3.Enabled = true;
            this.commandBarRowElement3.EnableImageTransparency = false;
            this.commandBarRowElement3.FlipText = false;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.ShouldPaint = true;
            this.commandBarRowElement3.ShowHorizontalLine = false;
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextWrap = false;
            this.commandBarRowElement3.UseCompatibleTextRendering = true;
            this.commandBarRowElement3.UseDefaultDisabledPaint = true;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = true;
            this.commandBarStripElement2.ClipDrawing = false;
            this.commandBarStripElement2.ClipText = false;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawBorder = true;
            this.commandBarStripElement2.DrawFill = true;
            this.commandBarStripElement2.DrawText = false;
            this.commandBarStripElement2.Enabled = true;
            this.commandBarStripElement2.EnableImageTransparency = false;
            this.commandBarStripElement2.FlipText = false;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btSave});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.commandBarStripElement2.RightToLeft = false;
            this.commandBarStripElement2.ShouldPaint = true;
            this.commandBarStripElement2.ShowHorizontalLine = false;
            this.commandBarStripElement2.StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btSave
            // 
            this.btSave.AccessibleDescription = "保存";
            this.btSave.AccessibleName = "保存";
            this.btSave.DisplayName = "commandBarButton2";
            this.btSave.DrawText = true;
            this.btSave.Image = global::YJH_HQ.UI.Properties.Resources.Save16;
            this.btSave.Name = "btSave";
            this.btSave.Text = "保存";
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radLabel15.ForeColor = System.Drawing.Color.Red;
            this.radLabel15.Location = new System.Drawing.Point(461, 132);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(13, 21);
            this.radLabel15.TabIndex = 47;
            this.radLabel15.Text = "*";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(209, 200);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(22, 18);
            this.radLabel16.TabIndex = 17;
            this.radLabel16.Text = "km";
            // 
            // radLabel25
            // 
            this.radLabel25.Location = new System.Drawing.Point(209, 223);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(22, 18);
            this.radLabel25.TabIndex = 18;
            this.radLabel25.Text = "km";
            // 
            // radLabel26
            // 
            this.radLabel26.Location = new System.Drawing.Point(209, 275);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(18, 18);
            this.radLabel26.TabIndex = 19;
            this.radLabel26.Text = "元";
            // 
            // UI_ad_car_use
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(531, 442);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radCommandBar2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "UI_ad_car_use";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "车辆使用";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxtravel_fee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxremark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxend_km)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtend_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxstart_km)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtstart_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxdest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddllicence_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            this.radLabel14.ResumeLayout(false);
            this.radLabel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddldriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtuse_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddltype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxforWhat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxother)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxuse_no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxCompany_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btSave;
        private Telerik.WinControls.UI.RadLabel label1;
        private Telerik.WinControls.UI.RadTextBox tbxCompany_id;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox tbxother;
        private Telerik.WinControls.UI.RadTextBox tbxuse_no;
        private Telerik.WinControls.UI.RadDropDownList ddltype;
        private Telerik.WinControls.UI.RadDateTimePicker dtuse_date;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox tbxforWhat;
        private Telerik.WinControls.UI.RadDropDownList ddldriver;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox tbxdest;
        private Telerik.WinControls.UI.RadDropDownList ddllicence_no;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxtravel_fee;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxend_km;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadDateTimePicker dtend_time;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadMaskedEditBox tbxstart_km;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadDateTimePicker dtstart_time;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox tbxremark;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
    }
}