﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Car
{
    public partial class UC_ad_car_info : UserControl
    {
        #region ===字段及属性===

        #endregion

        #region ===构造方法===
        public UC_ad_car_info()
        {
            InitializeComponent();
            this.dgCar.QueryMethod = "YJH.ad_car_infoService.Query";
            this.dgCar.GetQueryArgsFunc = this.GetQueryArgs;
            this.Load += UC_ad_car_info_Load;
            this.btnModify.Click += (s, e) => { this.OnOpen(); };
            this.btnNew.Click += (s, e) => { this.New(); };
            this.btnDelete.Click += (s, e) => { this.Delete(); };
            this.btnSearch.Click += (s, e) => { this.dgCar.LoadData(); };
            this.dgCar.GridView.CellDoubleClick += (s, e) => { this.OnOpen(); };
            this.dgCar.GridView.CellFormatting += GridView_CellFormatting;
            this.LoadPermission();
        }

        #endregion

        #region ===方法===
        /// <summary>
        /// 加载当前用户的权限
        /// </summary>
        void LoadPermission()
        {
            // 此方法用来获取当前用户给定的界面下的权限
            //var funcs = sys.Services.OrgUnitService.GetFuncByParentName(this.GetType().FullName);
            //this.btnModify.Enabled = funcs.Exists(t => t.Name == "修改");
            //this.btnSend.Enabled = funcs.Exists(t => t.Name == "新单");
        }

        //修改
        private void OnOpen()
        {
            var id = this.dgCar.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            var entity = new YJH.Entities.ad_car_info(dps.Common.Data.Entity.Retrieve(YJH.Entities.ad_car_info.EntityModelID, id));
            var view = new UI_ad_car_info();
            view.dgList = this.dgCar;
            view.Entity = entity;
            view.ShowDialog();
        }

        private object[] GetQueryArgs()
        {
            object[] args = new object[1];
            args[0] = this.tbxName.Text;
            return args;
        }

        /// <summary>
        /// 新建
        /// </summary>
        private void New()
        {
            var entity = new YJH.Entities.ad_car_info();
            var view = new UI_ad_car_info();
            view.dgList = this.dgCar;
            view.Entity = entity;
            view.ShowDialog();
        }
        //删除
        private void Delete()
        {
            var id = this.dgCar.SelectedEntityID;
            if (id == Guid.Empty)
                return;
            if (this.dgCar.GridView.SelectedRows[0].Cells["status1"].Value.ToString() != "禁用")
            {
                RadMessageBox.Show("只能删除状态为禁用的数据!");
                return;
            }
            if (RadMessageBox.Show("确定删除数据？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                YJH.Services.ad_car_infoService.Delete(id);
                this.dgCar.GridView.SelectedRows[0].Delete();
                this.dgCar.GridView.Refresh();
                RadMessageBox.Show("删除成功!");
            }
        }
     
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载vava
        /// </summary>
        /// <remarks>建立人：张殿元 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void UC_ad_car_info_Load(object sender, EventArgs e)
        {
            this.dgCar.LoadData();
        }

        void GridView_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (this.dgCar == null)
            {
                return;
            }
            switch (this.dgCar.GridView.Rows[e.RowIndex].Cells["type"].Value.ToString())
            {
                case "10":
                    this.dgCar.GridView.Rows[e.RowIndex].Cells["type1"].Value = "小车";
                    break;
                case "20":
                    this.dgCar.GridView.Rows[e.RowIndex].Cells["type1"].Value = "面包车";
                    break;
                case "30":
                    this.dgCar.GridView.Rows[e.RowIndex].Cells["type1"].Value = "其它";
                    break;
            }
            switch (this.dgCar.GridView.Rows[e.RowIndex].Cells["status"].Value.ToString())
            {
                case "10":
                    this.dgCar.GridView.Rows[e.RowIndex].Cells["status1"].Value = "正常";
                    break;
                case "20":
                    this.dgCar.GridView.Rows[e.RowIndex].Cells["status1"].Value = "禁用";
                    break;
            }
        }
        #endregion
    }
}
