﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH.Services;
using YJH.Entities;

namespace YJH_HQ.UI.Views.Sale
{
    public partial class UC_pi_order_detail : UserControl
    {
        UC_pi_order_head lotterySotck;
        //int no = 1;
        DataTable tempdt = null;
        public bool IsEdit = false;
        public string sotreid = "";
        public string exchangeid = "";
        public DateTime dd = OrgUnitService.GetDate();
        public string scale = "";
        #region ===构造===
        public UC_pi_order_detail(UC_pi_order_head lse)
        {
            InitializeComponent();
            lotterySotck = lse;
            this.Load += UC_pi_order_detail_Load;
            this.dgDetail.CellValidating += dgDetail_CellValidating;
            this.dgDetail.CellFormatting += dgDetail_CellFormatting;
            this.ddtStore.SelectedIndexChanged += ddtStore_SelectedIndexChanged;
            this.dgDetail.UserDeletedRow += dgDetail_UserDeletedRow;
            this.dtpDeliveryDate.Value = OrgUnitService.GetDate();
            this.tbxSale.Text = "0";
            this.dtpDeliveryDate.ValueChanged += dtpDeliveryDate_ValueChanged;
            this.tbxSale.TextChanged += tbxSale_TextChanged;
            this.dgDetail.UserAddedRow += dgDetail_UserAddedRow;
        }

        void dgDetail_UserAddedRow(object sender, GridViewRowEventArgs e)
        {
            int i = 1;
            foreach (var item in dgDetail.Rows)
            {
                item.Cells[1].Value = i;
                i++;
            }
        }

        void tbxSale_TextChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(tbxSale.Text.Trim()))
                lotterySotck.Scale = decimal.Parse(tbxSale.Text.Trim()) / 100;
        }

        void dtpDeliveryDate_ValueChanged(object sender, EventArgs e)
        {
            lotterySotck.DeliveryDate = dtpDeliveryDate.Value;
        }
        #endregion
        void UC_pi_order_detail_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            /*this.SetColumnLotteryClass();
            this.SetColumnLotteryType();
            SetColumnLotterySeries();*/
            if (lotterySotck.IsNew == true)
            {
                tempdt = new DataTable();
                tempdt.Columns.Add("ID");
                tempdt.Columns.Add("Number");
                tempdt.Columns.Add("ProductNumber");
                tempdt.Columns.Add("Bid", typeof(decimal));
                tempdt.Columns.Add("Count", typeof(decimal));
                tempdt.Columns.Add("Price", typeof(decimal));
                DataRow dr = tempdt.NewRow();
                //dr.Cells[1].Value = no;
                //no++;
                dr["Count"] = 0.00;
                dr["Price"] = 0.00;
                dr["Number"] = "1";
                dr["ProductNumber"] = "1";
                dr["Bid"] = 0.00;
                tempdt.Rows.Add(dr);
                dgDetail.DataSource = tempdt;
                this.dtpDeliveryDate.Value = OrgUnitService.GetDate();
                this.tbxSale.Text = "";
            }
            else if (IsEdit == false && lotterySotck.IsNew == false)
            {
                this.ddtStore.Enabled = false;
                this.ddtStore.SelectedValue = sotreid;
                this.dgDetail.ReadOnly = true;
                this.lbTop.Text = ddtStore.Text + "采购单";
                this.lbexchangeid.Text = "单号：" + exchangeid;
                DataTable ddt = YJH.Services.pi_orderService.Getpi_order_detail(exchangeid);
                this.dtpDeliveryDate.Value = this.dd;
                this.tbxSale.Text = (decimal.Parse(scale) * 100).ToString();
                decimal c = 0;
                decimal d = 0;
                foreach (DataRow dr in ddt.Rows)
                {
                    c += decimal.Parse(dr["Count"].ToString());
                    d += decimal.Parse(dr["Price"].ToString());
                }
                lbDetailCount.Text = "合计：采购总数 " + c.ToString("f0") + "  采购总额 " + d.ToString();
                this.dgDetail.DataSource = ddt;

            }
            else if (IsEdit == true && lotterySotck.IsNew == false)
            {
                this.ddtStore.Enabled = false;
                this.ddtStore.SelectedValue = sotreid;
                this.dgDetail.ReadOnly = false;
                this.dtpDeliveryDate.Value = this.dd;
                this.tbxSale.Text = (decimal.Parse(scale) * 100).ToString();
                this.lbTop.Text = ddtStore.Text + "采购单";
                this.lbexchangeid.Text = "单号：" + exchangeid;
                DataTable ddt = YJH.Services.pi_orderService.Getpi_order_detail(exchangeid);
                decimal c = 0;
                decimal d = 0;
                foreach (DataRow dr in ddt.Rows)
                {
                    c += decimal.Parse(dr["Count"].ToString());
                    d += decimal.Parse(dr["Price"].ToString());
                }
                lbDetailCount.Text = "合计：采购总数 " + c.ToString("f0") + "  采购总额 " + d.ToString();
                this.dgDetail.DataSource = ddt;
            }
            lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
        }
        #region ===事件===
        void dgDetail_UserDeletedRow(object sender, GridViewRowEventArgs e)
        {
            if (dgDetail.Rows.Count == 0)
            {
                lotterySotck.dt = null;
            }
            else
            {
                dgDetail.Refresh();
                lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
            }
            int i = 1;
            foreach (var item in dgDetail.Rows)
            {
                item.Cells[1].Value = i;
                i++;
            }
        }
        void ddtStore_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            lotterySotck.storeID = ddtStore.SelectedValue.ToString();
        }
        void dgDetail_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            //if(dgDetail.Rows.Count > 0)
            //{
            //    if((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_led_exchange_num" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166); 
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_led_exchange _amt" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247,90,166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166); 
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_lottery_type" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166);
            //    }
            //    else if ((((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Column).Name == "col_lottery_series" && (e.CellElement).Value == null)
            //    {
            //        e.CellElement.DrawFill = true; e.CellElement.GradientStyle = GradientStyles.Linear; e.CellElement.GradientPercentage = 0.45f; e.CellElement.GradientPercentage2 = 0.53f; e.CellElement.NumberOfColors = 4; e.CellElement.BackColor = Color.FromArgb(247, 90, 166); e.CellElement.BackColor2 = Color.FromArgb(247,90,166); e.CellElement.BackColor3 = Color.FromArgb(247,90,166); e.CellElement.BackColor4 = Color.FromArgb(247,90,166); e.CellElement.BorderGradientStyle = GradientStyles.Solid; e.CellElement.BorderColor = Color.FromArgb(247,90,166);
            //    }
            //}
        }
        void dgDetail_CellValidating(object sender, CellValidatingEventArgs e)
        {
            if (dgDetail.Rows.Count > 0)
            {
                if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_led_exchange_num" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请输入采购数量";
                    lotterySotck.dt = null;
                }
                else if ((((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column) != null && (((Telerik.WinControls.UI.GridViewCellCancelEventArgs)(e)).Column).Name == "col_led_exchange _amt" && e.Value == null)
                {
                    e.Cancel = true;
                    e.Row.ErrorText = "请输入采购金额";
                    lotterySotck.dt = null;
                }
                else
                {
                    lotterySotck.dt = (System.Data.DataTable)dgDetail.DataSource;
                }
            }
        }
        #endregion
        #region ===方法===
        /// <summary>
        /// 初始化彩票种类列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        /*private void SetColumnLotteryClass()
        {
            var columnClass = this.dgDetail.Columns["col_lottery_class"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryClass)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryClass), ot),
                            ValueMember = ot
                        }).ToList();
            columnClass.DisplayMember = "DisplayMember";
            columnClass.ValueMember = "ValueMember";
            columnClass.DataSource = list;
        }
        /// <summary>
        /// 初始化彩票类型列
        /// </summary>
        /// <remarks>建立人：张臻 建立日期：2014/12/22 最后修改日期：2014/12/22</remarks>
        private void SetColumnLotteryType()
        {
            var columnType = this.dgDetail.Columns["col_lottery_type"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotteryType)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotteryType), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }

        /// <summary>
        /// 初始化彩票系列列
        /// </summary>
        /// <remarks>建立人：陆佳伟 建立日期：2015/1/27 最后修改日期：2015/1/27</remarks>
        private void SetColumnLotterySeries()
        {
            var columnType = this.dgDetail.Columns["col_lottery_series"] as GridViewComboBoxColumn;
            var list = (from ot in Enum.GetValues(typeof(YJH.Enums.LotterySeries)).Cast<int>()
                        select new
                        {
                            DisplayMember = Enum.GetName(typeof(YJH.Enums.LotterySeries), ot),
                            ValueMember = ot
                        }).ToList();
            columnType.DisplayMember = "DisplayMember";
            columnType.ValueMember = "ValueMember";
            columnType.DataSource = list;
        }*/
        #endregion
    }
}
