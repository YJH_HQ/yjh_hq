﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using YJH.Enums;
using YJH.Services;
using YJH.Entities;

namespace YJH_HQ.UI.Views.Sale
{
    public partial class UC_pi_order_head : UserControl
    {
        #region ===字段===
        public System.Data.DataTable dt = null;
        public string storeID = null;
        public bool IsNew = false;
        private DataTable MainDt = null;
        private int no = 1;
        public decimal Scale = 0;
        public DateTime DeliveryDate = OrgUnitService.GetDate();
        #endregion
        #region ===构造===
        public UC_pi_order_head()
        {
            InitializeComponent();
            this.Load += UC_pi_order_head_Load;
            this.btnNew.Click += btnNew_Click;
            this.rpMain.SelectedPageChanged += rpMain_SelectedPageChanged;
            this.btnModify.Click += btnModify_Click;
            this.btnSearch.Click += (s, e) => { this.Search(); };
            this.dgMain.CellFormatting += dgMain_CellFormatting;
            this.btnAdudit.Click += btnAdudit_Click;
            this.dgMain.DoubleClick += dgMain_DoubleClick;
            this.btnSend.Click += btnSend_Click;
            this.btnDelete.Click += btnDelete_Click;
            this.rpMain.PageRemoving += rpMain_PageRemoving;
        }
        #endregion

        #region ===事件===
        void rpMain_PageRemoving(object sender, RadPageViewCancelEventArgs e)
        {
            if (((Telerik.WinControls.UI.RadPageViewEventArgs)(e)).Page.TabIndex == 0)
            {
                e.Cancel = true;
            }
            if (rpMain.Pages.Count == 2)
            {
                no = 1;
            }
        }
        void dgMain_DoubleClick(object sender, EventArgs e)
        {
            foreach (var item in rpMain.Pages)
            {
                if (item.TabIndex != 0)
                {
                    if (dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString() == item.Tag.ToString())
                    {
                        rpMain.SelectedPage = item;
                        return;
                    }
                }
            }
            RadPageViewPage page = new RadPageViewPage();
            page.Text = "单据" + no;
            no++;
            UC_pi_order_detail lsd = new UC_pi_order_detail(this);
            if (dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已修改")
                lsd.IsEdit = true;
            else
                lsd.IsEdit = false;
            lsd.sotreid = dgMain.SelectedRows[0].Cells["col_store_id"].Value.ToString();
            lsd.exchangeid = dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString();
            lsd.dd = DateTime.Parse(dgMain.SelectedRows[0].Cells["col_DeliveryDate"].Value.ToString());
            lsd.scale = dgMain.SelectedRows[0].Cells["col_Scale"].Value.ToString();
            page.Controls.Add(lsd);
            page.Tag = dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString();
            this.rpMain.Pages.Add(page);
            this.rpMain.SelectedPage = page;
        }
        void dgMain_CellFormatting(object sender, CellFormattingEventArgs e)
        {
        }
        void Search()
        {
            MainDt = YJH.Services.pi_orderService.Getpi_order_head(ddtStore.SelectedValue.ToString(), dtStartDate.Value, dtEndDate.Value);
            decimal a = 0;
            decimal b = 0;
            MainDt.Columns.Add("State1");
            MainDt.Columns.Add("send_status1");
            foreach (DataRow dr in MainDt.Rows)
            {
                dr["State1"] = Enum.GetName(typeof(YJH.Enums.leh_status), int.Parse(dr["State"].ToString()));
                dr["send_status1"] = Enum.GetName(typeof(YJH.Enums.send_status), int.Parse(dr["send_status"].ToString()));
                a += decimal.Parse(dr["Count"].ToString());
                b += decimal.Parse(dr["Price"].ToString());
            }
            labelCount.Text = "合计：采购总数 " + a.ToString("f0") + "  采购总额 " + b.ToString();
            this.dgMain.DataSource = MainDt;
        }
        void btnNew_Click(object sender, EventArgs e)
        {
            IsNew = true;
            RadPageViewPage page = new RadPageViewPage();
            page.Text = "单据" + no;
            no++;
            UC_pi_order_detail lsd = new UC_pi_order_detail(this);
            page.Controls.Add(lsd);
            var n = YJH.Services.pi_orderService.GetOrderNo(storeID);
            page.Tag = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + (Convert.ToInt32(n) + 1).ToString().PadLeft(3, '0');
            this.rpMain.Pages.Add(page);
            this.rpMain.SelectedPage = page;

            /*if(IsNew == false)
            {
                IsNew = true;
                
            }
            else
            {
                RadMessageBox.Show("请先保存新建的置换单后再操作", "提示");
            }*/
        }
        void btnAdudit_Click(object sender, EventArgs e)
        {
            if (dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已修改")
                {
                    if (RadMessageBox.Show("确认审核吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.pi_orderService.Audit(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()), Globle.CurrentEmployee.Base.Name);
                        RadMessageBox.Show("审核成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要审核的记录", "提示");
            }
        }
        void btnSend_Click(object sender, EventArgs e)
        {
            if (dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已审核")
                {
                    if (RadMessageBox.Show("确认发送吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.pi_orderService.Send(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()), Globle.CurrentEmployee.Base.Name);
                        RadMessageBox.Show("发送成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要发送的记录", "提示");
            }
        }
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgMain.SelectedRows.Count > 0)
            {
                if (dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已审核" || dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_leh_status"].Value.ToString() == "已修改")
                {
                    if (RadMessageBox.Show("确认删除吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        YJH.Services.pi_orderService.Delete(Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()));
                        RadMessageBox.Show("删除成功", "提示");
                        Search();
                    }
                }
            }
            else
            {
                RadMessageBox.Show("请先选择需要删除的记录", "提示");
            }
        }
        void btnModify_Click(object sender, EventArgs e)
        {
            if (btnModify.Text == "保存")
            {
                if (dt != null)
                {
                    if (RadMessageBox.Show("确认保存吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        try
                        {
                            pi_order_head head = null;
                            if (IsNew == true)
                            {
                                head = new pi_order_head();
                                var n = YJH.Services.pi_orderService.GetOrderNo(storeID);
                                if (n != "")
                                {
                                    head.OrderNo = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + (Convert.ToInt32(n) + 1).ToString().PadLeft(3, '0');
                                }
                                else
                                {
                                    head.OrderNo = storeID + OrgUnitService.GetDate().Year + OrgUnitService.GetDate().Month.ToString().PadLeft(2, '0') + OrgUnitService.GetDate().Day + "001";
                                }
                                head.store_id = storeID;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    head.Count += decimal.Parse(dr["Count"].ToString());
                                    head.Price += decimal.Parse(dr["Price"].ToString());
                                }
                                head.Company = Globle.CurrentBusinessUnit.OrgCode.ToString();
                                head.State = (int)leh_status.已录入;
                                head.Create = Globle.CurrentEmployee.Base.Name;
                                head.CreateTime = OrgUnitService.GetDate();
                                head.send_status = (int)send_status.未通讯;
                                head.DeliveryDate = this.DeliveryDate;
                                head.OrderDate = OrgUnitService.GetDate();
                                head.PurchaseNo = "1";
                                head.Scale = this.Scale;
                                head.Type = 10;
                                head.VendorNo = "1";
                                List<pi_order_detail> details = new List<pi_order_detail>();
                                int i = 1;
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    pi_order_detail detail = new pi_order_detail();
                                    detail.Count = decimal.Parse(dr1["Count"].ToString());
                                    detail.Price = decimal.Parse(dr1["Price"].ToString());
                                    detail.OrderNo = head.OrderNo;
                                    detail.Bid = decimal.Parse(dr1["Bid"].ToString());
                                    detail.Number = dr1["Number"].ToString();
                                    detail.ProductNumber = dr1["ProductNumber"].ToString();
                                    //detail.led_no = i;
                                    i++;
                                    //detail.lottery_class = dr1["lottery_class"].ToString().PadLeft(2, '0');
                                    //detail.lottery_series = dr1["lottery_series"].ToString().PadLeft(4, '0');
                                    //detail.lottery_type = dr1["lottery_type"].ToString().PadLeft(4, '0');
                                    details.Add(detail);
                                }
                                YJH.Services.pi_orderService.Save(details, head);
                                IsNew = false;
                            }
                            else
                            {
                                List<pi_order_detail> details = new List<pi_order_detail>();
                                int i = 1;
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if (dr1.RowState != DataRowState.Deleted)
                                    {
                                        pi_order_detail detail = new pi_order_detail();
                                        detail.Count = decimal.Parse(dr1["Count"].ToString());
                                        detail.Price = decimal.Parse(dr1["Price"].ToString());
                                        detail.OrderNo = dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString();
                                        //detail.led_no = i;
                                        i++;
                                        /*detail.lottery_class = dr1["lottery_class"].ToString().PadLeft(2, '0');
                                        detail.lottery_series = dr1["lottery_series"].ToString().PadLeft(4, '0');
                                        detail.lottery_type = dr1["lottery_type"].ToString().PadLeft(4, '0');*/
                                        details.Add(detail);
                                    }
                                }
                                YJH.Services.pi_orderService.SaveEdit(details, Guid.Parse(dgMain.SelectedRows[0].Cells["col_id"].Value.ToString()));
                            }


                            RadMessageBox.Show("保存成功", "提示");
                            IsNew = false;
                            rpMain.Pages.Remove(rpMain.SelectedPage);
                        }
                        catch (Exception ex)
                        {
                            RadMessageBox.Show(ex.Message, "提示");
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show("请填写必要项", "提示");
                }
            }
            else
            {
                if (dgMain.SelectedRows.Count > 0)
                {
                    if (dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已录入" || dgMain.SelectedRows[0].Cells["col_State"].Value.ToString() == "已修改")
                    {
                        foreach (var item in rpMain.Pages)
                        {
                            if (item.TabIndex != 0)
                            {
                                if (dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString() == item.Tag.ToString())
                                {
                                    rpMain.SelectedPage = item;
                                    return;
                                }
                            }
                        }
                        RadPageViewPage page = new RadPageViewPage();
                        page.Text = "单据" + no;
                        no++;
                        UC_pi_order_detail lsd = new UC_pi_order_detail(this);
                        lsd.IsEdit = true;
                        lsd.sotreid = dgMain.SelectedRows[0].Cells["col_store_id"].Value.ToString();
                        lsd.exchangeid = dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString();
                        page.Controls.Add(lsd);
                        page.Tag = dgMain.SelectedRows[0].Cells["col_OrderNo"].Value.ToString();
                        this.rpMain.Pages.Add(page);
                        this.rpMain.SelectedPage = page;
                    }
                }
                else
                {
                    RadMessageBox.Show("请先选择需要修改的记录", "提示");
                }
            }
        }

        void UC_pi_order_head_Load(object sender, EventArgs e)
        {
            var table = YJH.Services.ExpressBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;
            Search();
        }
        void rpMain_SelectedPageChanged(object sender, EventArgs e)
        {
            if (((Telerik.WinControls.UI.RadPageView)(sender)).SelectedPage.TabIndex == 0)
            {
                btnModify.Text = "修改";
                this.btnNew.Enabled = true;
                this.btnAdudit.Enabled = true;
                this.btnDelete.Enabled = true;
                this.btnSend.Enabled = true;
            }
            else
            {
                btnModify.Text = "保存";
                this.btnNew.Enabled = false;
                this.btnAdudit.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSend.Enabled = false;
            }
        }

        public void Save(System.Data.DataTable dt)
        {

        }
        #endregion
    }
}
