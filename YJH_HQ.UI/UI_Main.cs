﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using YJH.Services;

namespace YJH_HQ.UI
{
    public partial class UI_Main : Telerik.WinControls.UI.RadForm
    {
        private const string NULL_TREE_NODE = "NULL_TREE_NODE";
        System.Timers.Timer timer = new System.Timers.Timer();
        public UI_Main()
        {
            InitializeComponent();

            treeViewModule.NodeExpandedChanging += treeViewModule_NodeExpandedChanging;
            treeViewModule.SelectedNodeChanging += treeViewModule_SelectedNodeChanging;

            timer.Enabled = true;
            timer.Interval = 60000;//执行间隔时间,单位为毫秒  
            timer.Start();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer1_Elapsed);

            treeViewModule.Nodes.AddRange(TreeViewNode());

            BindAvailableOrg();

            //try
            //{
            //    int send = 0, sendmonth = 0;
            //    send = YJH.Services.ems_sendexpress_hqService.SendCount(System.DateTime.Now.Date);
            //    sendmonth = YJH.Services.ems_sendexpress_hqService.SendMonthCount(System.DateTime.Now.Date);
            //    //派(收)
            //    int deliver = 0, delivermonth;
            //    deliver = YJH.Services.ems_deliverExpress_hqService.DeliverCount(System.DateTime.Now.Date);
            //    delivermonth = YJH.Services.ems_deliverExpress_hqService.DeliverMonthCount(System.DateTime.Now.Date);
            //    Decimal sendday = 0, deliverday = 0;
            //    int day = System.DateTime.Now.Date.Day;
            //    sendday = sendmonth / day;
            //    deliverday = delivermonth / day;
            //    //亿佳汇1月12日当天收取包裹xxx件，寄出xxx件；本月累计收取xxxx件，寄出xxxx件，日均收取xxx件，寄出xxx件。每天9点发给戴建利，在程序中写死电话号码“13063632326”
            //    string msg = "亿佳汇" + System.DateTime.Now.AddDays(-1).Date.ToShortDateString() + "收取包裹" + deliver.ToString() + "件，寄出" + send.ToString() + "件；本月累计收取" + delivermonth.ToString() + "件，寄出" + sendmonth.ToString() + "件，日均收取" + deliverday.ToString() + "件，寄出" + sendday.ToString() + "件。";
            //    YJH.Services.SMSService.SendMessage("13616143025", msg);

            //}
            //catch (Exception)
            //{
                
            //    throw;
            //}
           
        }

        private static List<YJH_HQ.Controls.RadTreeNode.RadTreeNode> TreeViewNode()
        {
            //首先获取当前登录用户所属的模块功能
            var list = sys.Services.UserFuncService.GetCurrentUserFuncs(Guid.Empty);

            var nodes = new List<Controls.RadTreeNode.RadTreeNode>(list.Count);
            foreach (var item in list)
            {
                //模块下添加一空的视图节点，预留可以伸展的图标
                var childNodes = new Telerik.WinControls.UI.RadTreeNode[] { new Telerik.WinControls.UI.RadTreeNode(NULL_TREE_NODE) };

                var node = new Controls.RadTreeNode.RadTreeNode(item.Funcs.Name, null, childNodes);
                //模块功能ID
                node.Tag = item.FuncsID;
                nodes.Add(node);
            }

            return nodes;
        }

        /// <summary>
        /// 绑定归属组织机构
        /// </summary>
        private void BindAvailableOrg()
        {
            var list = Globle.AvailableOrganizations;

            BindCurrentOrg();

            ddlChangeOrg.Items.Clear();
            if (list.Count <= 1)
            {
                ddlChangeOrg.Enabled = false;
                return;
            }

            RadMenuItem menuItem;
            foreach (var item in list)
            {
                //item等于当前登录的组织则不绑定到控件上
                if (item.Value.ID != dps.Client.dpsClient.Default.CurrentOrgUnit.ID)
                {
                    menuItem = new RadMenuItem(item.Key.Substring(0, item.Key.LastIndexOf(" | ")), item.Value);
                    menuItem.Click += menuItem_Click;
                    ddlChangeOrg.Items.Add(menuItem);
                }
            }
        }

        void menuItem_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show(this, "确定切换机构吗？", "确认", MessageBoxButtons.OKCancel, RadMessageIcon.Question) == DialogResult.OK)
            {
                //先切换组织机构
                sys.Entities.OrgUnit ou = (sys.Entities.OrgUnit)(((RadMenuItem)sender).Tag);

                Globle.ChangeCurrentOrgUnit(ou);
                //清空该账户的功能权限，再次绑定
                treeViewModule.Nodes.Clear();
                treeViewModule.Nodes.AddRange(TreeViewNode());

                //获取当前已经打开的Tab页
                var controls = documentTabStrip1.Controls;
                //记录非“我的菜单”Tab也，也就是要移除的Tab
                List<Telerik.WinControls.UI.Docking.DocumentWindow> removeList = new List<Telerik.WinControls.UI.Docking.DocumentWindow>();

                //遍历整个已经打开的Tab，排除“我的菜单”Tab
                foreach (var control in controls)
                {
                    var window = (Telerik.WinControls.UI.Docking.DocumentWindow)control;
                    //排除我的菜单，关闭其它的tab
                    if (window.Name == "dWindowMenu" && window.Text == "我的菜单")
                        continue;
                    removeList.Add(window);
                }

                //移除要删除的Tab
                foreach (var item in removeList)
                    documentTabStrip1.Controls.Remove(item);

                //再次绑定归属组织
                BindAvailableOrg();
            }
        }

        void treeViewModule_NodeExpandedChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            var node = (Telerik.WinControls.UI.RadTreeNode)e.Node;
            if (node.Nodes.Count == 0 || node.Nodes[0].Text != NULL_TREE_NODE)
                return;

            node.Nodes[0].Remove();

            var list = sys.Services.UserFuncService.GetCurrentUserFuncs((Guid)node.Tag);
            foreach (var item in list)
                node.Nodes.Add(new Controls.RadTreeNode.RadTreeNode(item.Funcs.Name, item.Funcs.URL, null));
        }

        void treeViewModule_SelectedNodeChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {
            YJH_HQ.Controls.RadTreeNode.RadTreeNode node = (YJH_HQ.Controls.RadTreeNode.RadTreeNode)e.Node;
            if (node == null || node.AssemblyPath == null)
                return;

            //根据要显示的页面地址作为Key
            string key = node.AssemblyPath;
            if (documentTabStrip1.Controls.ContainsKey(key))
            {
                documentTabStrip1.Controls[key].Select();
                return;
            }

            //根据选中的模块功能节点，加载程序集生成对象
            Telerik.WinControls.UI.Docking.DocumentWindow window = new Telerik.WinControls.UI.Docking.DocumentWindow(node.Text);
            window.Name = key;
            documentTabStrip1.Controls.Add(window);

            var viewInstance = Assembly.Load("YJH_HQ.UI").CreateInstance(node.AssemblyPath);
            var view = (System.Windows.Forms.UserControl)viewInstance;
            if (view == null)
                return;

            view.Dock = System.Windows.Forms.DockStyle.Fill;
            window.Controls.Add(view);
        }

        private void BindCurrentOrg()
        {
            lblCurrentOrg.Text = string.Format("当前公司：{0} | 部门：{1} | 用户：{2}", Globle.CurrentBusinessUnit.Name, Globle.CurrentWorkGroup.Name, Globle.CurrentEmployeeOU.Name);
        }

        private void Timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            // 得到 hour minute second  如果等于某个值就开始执行某个程序。  
            int intHour = e.SignalTime.Hour;
            int intMinute = e.SignalTime.Minute;
            //int intSecond = e.SignalTime.Second;
            // 定制时间； 比如 在10：30 ：00 的时候执行某个函数  
            int iHour = 09;
            int iMinute = 00;
            //int iSecond = 00;
            // 设置　每天的 09：00：00开始执行程序  
            if (intHour == iHour && intMinute == iMinute)
            {
                //刘京EmploeeID      26DDF18E-A5B5-45D9-8740-C24FCD491509
                if (YJH_HQ.UI.Globle.CurrentEmployee.Instance.ID == System.Guid.Parse("26DDF18E-A5B5-45D9-8740-C24FCD491509"))
                {
                    DateTime d = OrgUnitService.GetDate();
                    //寄
                    int send = 0, sendmonth = 0;
                    send = YJH.Services.ems_sendexpress_hqService.SendCount(d.Date);
                    sendmonth = YJH.Services.ems_sendexpress_hqService.SendMonthCount(d.Date);
                    //派(收)
                    int deliver = 0, delivermonth;
                    deliver = YJH.Services.ems_deliverExpress_hqService.DeliverCount(d.Date);
                    delivermonth = YJH.Services.ems_deliverExpress_hqService.DeliverMonthCount(d.Date);
                    Decimal sendday = 0, deliverday = 0;
                    int day = d.Date.Day;
                    sendday = sendmonth / day;
                    deliverday = delivermonth / day;
                    //亿佳汇1月12日当天收取包裹xxx件，寄出xxx件；本月累计收取xxxx件，寄出xxxx件，日均收取xxx件，寄出xxx件。每天9点发给戴建利，在程序中写死电话号码“13063632326”
                    string msg = "亿佳汇" + d.AddDays(-1).Date.ToShortDateString() + "收取包裹" + deliver.ToString() + "件，寄出" + send.ToString() + "件；本月累计收取" + delivermonth.ToString() + "件，寄出" + sendmonth.ToString() + "件，日均收取" + deliverday.ToString() + "件，寄出" + sendday.ToString() + "件。";
                    YJH.Services.SMSService.SendMessage("13063632326", msg);

                }    
            }
        }
    }
}