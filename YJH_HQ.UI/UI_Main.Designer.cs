﻿namespace YJH_HQ.UI
{
    partial class UI_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.toolWindowModule = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.treeViewModule = new Telerik.WinControls.UI.RadTreeView();
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.documentTabStrip1 = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.dWindowMenu = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCurrentOrg = new Telerik.WinControls.UI.RadLabel();
            this.ddlChangeOrg = new Telerik.WinControls.UI.RadDropDownButton();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            this.toolWindowModule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewModule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            this.toolTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            this.documentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).BeginInit();
            this.documentTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlChangeOrg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.toolWindowModule;
            this.radDock1.BackColor = System.Drawing.Color.Transparent;
            this.radDock1.Controls.Add(this.toolTabStrip1);
            this.radDock1.Controls.Add(this.documentContainer1);
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(0, 0);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radDock1.Size = new System.Drawing.Size(1193, 536);
            this.radDock1.TabIndex = 0;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            // 
            // toolWindowModule
            // 
            this.toolWindowModule.Caption = null;
            this.toolWindowModule.Controls.Add(this.treeViewModule);
            this.toolWindowModule.Location = new System.Drawing.Point(1, 24);
            this.toolWindowModule.Name = "toolWindowModule";
            this.toolWindowModule.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindowModule.Size = new System.Drawing.Size(198, 500);
            this.toolWindowModule.Text = "模块列表";
            // 
            // treeViewModule
            // 
            this.treeViewModule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewModule.Location = new System.Drawing.Point(0, 0);
            this.treeViewModule.Name = "treeViewModule";
            this.treeViewModule.ShowLines = true;
            this.treeViewModule.Size = new System.Drawing.Size(198, 500);
            this.treeViewModule.SpacingBetweenNodes = -1;
            this.treeViewModule.TabIndex = 0;
            this.treeViewModule.Text = "radTreeView1";
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.CanUpdateChildIndex = true;
            this.toolTabStrip1.Controls.Add(this.toolWindowModule);
            this.toolTabStrip1.Location = new System.Drawing.Point(5, 5);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip1.SelectedIndex = 0;
            this.toolTabStrip1.Size = new System.Drawing.Size(200, 526);
            this.toolTabStrip1.TabIndex = 1;
            this.toolTabStrip1.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Controls.Add(this.documentTabStrip1);
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.TabIndex = 2;
            // 
            // documentTabStrip1
            // 
            this.documentTabStrip1.CanUpdateChildIndex = true;
            this.documentTabStrip1.Controls.Add(this.dWindowMenu);
            this.documentTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.documentTabStrip1.Name = "documentTabStrip1";
            // 
            // 
            // 
            this.documentTabStrip1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentTabStrip1.SelectedIndex = 0;
            this.documentTabStrip1.Size = new System.Drawing.Size(979, 526);
            this.documentTabStrip1.TabIndex = 0;
            this.documentTabStrip1.TabStop = false;
            // 
            // dWindowMenu
            // 
            this.dWindowMenu.Cursor = System.Windows.Forms.Cursors.Default;
            this.dWindowMenu.DocumentButtons = Telerik.WinControls.UI.Docking.DocumentStripButtons.None;
            this.dWindowMenu.Location = new System.Drawing.Point(6, 29);
            this.dWindowMenu.Name = "dWindowMenu";
            this.dWindowMenu.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.dWindowMenu.Size = new System.Drawing.Size(967, 491);
            this.dWindowMenu.Text = "我的菜单";
            this.dWindowMenu.ToolCaptionButtons = Telerik.WinControls.UI.Docking.ToolStripCaptionButtons.None;
            // 
            // radPanel1
            // 
            this.radPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radPanel1.Controls.Add(this.panel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1193, 80);
            this.radPanel1.TabIndex = 1;
            this.radPanel1.Text = "radPanel1";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::YJH_HQ.UI.Properties.Resources.headerBg;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lblCurrentOrg);
            this.panel1.Controls.Add(this.ddlChangeOrg);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1193, 80);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::YJH_HQ.UI.Properties.Resources.logo;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(409, 80);
            this.panel2.TabIndex = 2;
            // 
            // lblCurrentOrg
            // 
            this.lblCurrentOrg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentOrg.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrentOrg.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblCurrentOrg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblCurrentOrg.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCurrentOrg.Location = new System.Drawing.Point(612, 48);
            this.lblCurrentOrg.Name = "lblCurrentOrg";
            this.lblCurrentOrg.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCurrentOrg.Size = new System.Drawing.Size(60, 19);
            this.lblCurrentOrg.TabIndex = 1;
            this.lblCurrentOrg.Text = "radLabel1";
            // 
            // ddlChangeOrg
            // 
            this.ddlChangeOrg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlChangeOrg.Location = new System.Drawing.Point(1048, 40);
            this.ddlChangeOrg.Name = "ddlChangeOrg";
            this.ddlChangeOrg.Size = new System.Drawing.Size(140, 26);
            this.ddlChangeOrg.TabIndex = 0;
            this.ddlChangeOrg.Text = "切换机构";
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radDock1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(0, 80);
            this.radPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(1193, 536);
            this.radPanel2.TabIndex = 2;
            this.radPanel2.Text = "radPanel2";
            // 
            // UI_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 616);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.Name = "UI_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainForm";
            this.ThemeName = "ControlDefault";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            this.toolWindowModule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeViewModule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            this.toolTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            this.documentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip1)).EndInit();
            this.documentTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlChangeOrg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindowModule;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.RadTreeView treeViewModule;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip documentTabStrip1;
        private Telerik.WinControls.UI.Docking.DocumentWindow dWindowMenu;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadDropDownButton ddlChangeOrg;
        private Telerik.WinControls.UI.RadLabel lblCurrentOrg;
        private System.Windows.Forms.Panel panel2;
    }
}
