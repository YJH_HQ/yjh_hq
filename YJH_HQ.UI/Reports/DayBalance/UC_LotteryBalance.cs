﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Reports.DayBalance
{
    public partial class UC_LotteryBalance : UserControl
    {
        #region ===构造方法===
        public UC_LotteryBalance()
        {
            InitializeComponent();
            this.Load += UC_LotteryBalance_Load;
            this.btSearch.Click += (s, e) => { this.Search(); };
        }       
        #endregion

        #region ===事件===
        /// <summary>
        /// 初始化加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UC_LotteryBalance_Load(object sender, EventArgs e)
        {
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;
            var table = YJH.Services.LotteryBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
        }
        #endregion

        #region ===方法===
        /// <summary>
        /// 查询
        /// </summary>
        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var start = this.dtStartDate.Value.Date;
                var end = this.dtEndDate.Value.Date;
                if (start > end)
                {
                    RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                    return;
                }
                var store = this.ddtStore.SelectedValue.ToString();

                try
                {
                    var result = YJH.Services.LotteryBalanceService.Search(start.Date, end.AddDays(1).Date, store);

                    this.Invoke((MethodInvoker)delegate()
                    {
                        var report = new YJH_HQ.UI.Reports.DayBalance.RP_LotteryBalance();
                        report.ReportParameters["StoreName"].Value = this.ddtStore.SelectedItem.Text;
                        report.ReportParameters["PrintDate"].Value = DateTime.Now.ToString("yyyy-MM-dd");
                        report.ReportParameters["StartDate"].Value = start.ToString("yyyy-MM-dd");
                        report.ReportParameters["EndDate"].Value = end.ToString("yyyy-MM-dd");
                        report.LotterySource.DataSource = result;
                        var reportSource = new Telerik.Reporting.InstanceReportSource();
                        reportSource.ReportDocument = report;
                        this.rpvLottery.ReportSource = reportSource;
                        this.rpvLottery.RefreshReport();
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }

            });
        }
        #endregion
    }
}
