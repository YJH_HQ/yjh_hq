﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace YJH_HQ.UI.Reports.Schedule
{
    public partial class UC_ScheduleSearch : UserControl
    {
        private int year;
        private int month;
        private string store;

        public UC_ScheduleSearch()
        {
            InitializeComponent();
            this.btnExcel.Click += btnExcel_Click;
            this.Load += UC_ScheduleSearch_Load;

        }

        void UC_ScheduleSearch_Load(object sender, EventArgs e)
        {
            //this.ddlYear.Text = 
        }

        void btnExcel_Click(object sender, EventArgs e)
        {
            if (dgSchedule.Rows.Count == 0)
                return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl   files   (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "导出Excel文件到";

            saveFileDialog.FileName = year.ToString() + "年" + month.ToString() + "月" + store + "排班报表";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream myStream;
                myStream = saveFileDialog.OpenFile();
                StreamWriter sw = new StreamWriter(myStream, System.Text.Encoding.GetEncoding("gb2312"));
                string str = "";
                try
                {
                    //写标题     
                    for (int i = 0; i < this.dgSchedule.ColumnCount; i++)
                    {
                        if (i > 0)
                        {
                            str += "\t";
                        }
                        str += dgSchedule.Columns[i].HeaderText;
                    }

                    sw.WriteLine(str);
                    //写内容   
                    for (int j = 0; j < dgSchedule.Rows.Count; j++)
                    {
                        string tempStr = "";
                        for (int k = 0; k < dgSchedule.Columns.Count; k++)
                        {
                            if (k > 0)
                            {
                                tempStr += "\t";
                            }
                        }
                        sw.WriteLine(tempStr);
                    }
                    sw.Close();
                    myStream.Close();
                    MessageBox.Show("导出成功", "提示");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    sw.Close();
                    myStream.Close();
                }
            }
        }
    }
}
