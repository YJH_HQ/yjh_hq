﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH_HQ.UI.Services;
using System.Data.SqlClient;
using Telerik.WinControls;
using YJH.Services;
using Telerik.WinControls.UI;
using System.IO;
using Telerik.WinControls.UI.Export;
using System.Transactions;
using System.Threading;
using Telerik.WinControls.Enumerations;

namespace YJH_HQ.UI.Reports.ChangeShifts
{
    public partial class UC_AttendanceReport : UserControl
    {
        private int year = 0;
        private int month = 0;
        private bool openExportFile = false;
        private DataTable dt3 = new DataTable();
        public UC_AttendanceReport()
        {
            InitializeComponent();
            this.Load += UC_AttendanceReport_Load;
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.dgAttendance.CellClick += dgAttendance_CellClick;
            this.radGridView2.CellFormatting += radGridView2_CellFormatting;
            this.radGridView3.CellFormatting += radGridView3_CellFormatting;
            this.btnFinish.Click += btnFinish_Click;
            this.excel.Click += excel_Click;
            this.dgAttendance.CellFormatting += dgAttendance_CellFormatting;
            this.html.Click += html_Click;
            this.btnprint.Click += btnprint_Click;
            this.btnQuery.Click += btnQuery_Click;
            tb.ToggleStateChanged += tb_ToggleStateChanged;
        }

        void tb_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if(dgAttendance.DataSource != null)
            {
                if (!(tb.ToggleState == ToggleState.On))
                {
                    tb.Text = "收起";
                    this.dgAttendance.MasterTemplate.ExpandAll();
                }
                else
                {
                    tb.Text = "展开";
                    this.dgAttendance.MasterTemplate.CollapseAllGroups();
                } 
            }
        }

        void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlYear.Text == "" || ddlMonth.Text == "")
                {
                    RadMessageBox.Show(this, "请选择年份以及日期", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }

                year = int.Parse(ddlYear.Text);
                month = int.Parse(ddlMonth.Text);
                string isdetail = "0";
                if (cbxIsDetail.Checked)
                    isdetail = "1";
                else
                    isdetail = "0";
                DataTable dt1 = new DataTable();
                dt1 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", ddlYear.Text + ddlMonth.Text), new SqlParameter("@is_detail", isdetail), new SqlParameter("@transaction", "report"));
                for (int i = 0; i < dt1.Columns.Count; i++)
                {
                    if (dt1.Columns.Count % 2 != 0)
                    {
                        if (i > 6 && i < 39 && i % 2 == 0)
                        {
                            dt1.Columns.RemoveAt(i);
                            dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                        }
                    }
                    else
                    {
                        if (i > 6 && i < 39 && i % 2 != 0)
                        {
                            dt1.Columns.RemoveAt(i);
                            dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                        }
                    }

                }
                DataTable dt2 = new DataTable();
                dt2 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", ddlYear.Text + ddlMonth.Text), new SqlParameter("@is_detail", isdetail), new SqlParameter("@transaction", "report"));
                for (int i = 0; i < dt2.Columns.Count; i++)
                {
                    if (dt2.Columns.Count % 2 == 0)
                    {
                        if (i > 6 && i < 38 && i % 2 == 0)
                        {
                            dt2.Columns.RemoveAt(i);
                            dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                        }
                    }
                    else
                    {
                        if (i > 6 && i < 38 && i % 2 != 0)
                        {
                            dt2.Columns.RemoveAt(i);
                            dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                        }
                    }
                }
                foreach (DataRow dr in dt2.Rows)
                {
                    dt1.ImportRow(dr);
                }
                DataView dv = dt1.DefaultView;
                dv.Sort = "store_id,emp_id Asc";
                DataTable dt3 = dv.ToTable();
                dt3.Columns[0].ColumnName = "店名";
                dt3.Columns[1].ColumnName = "姓名";
                dt3.Columns[2].ColumnName = "职务";
                dt3.Columns[3].ColumnName = "店号";
                dt3.Columns[4].ColumnName = "年";
                dt3.Columns[5].ColumnName = "月";
                dt3.Columns[6].ColumnName = "工号";
                dt3.Columns[38].ColumnName = "事假";
                dt3.Columns[39].ColumnName = "病假";
                dt3.Columns[40].ColumnName = "产假";
                dt3.Columns[41].ColumnName = "年假";
                dt3.Columns[42].ColumnName = "婚假";
                dt3.Columns[43].ColumnName = "丧假";
                dt3.Columns[44].ColumnName = "调休";
                dt3.Columns[45].ColumnName = "加班";
                dt3.Columns[46].ColumnName = "公休";
                dt3.Columns[47].ColumnName = "公干";
                dt3.Columns[48].ColumnName = "迟到";
                dt3.Columns[49].ColumnName = "早退";
                dt3.Columns[50].ColumnName = "旷工";
                dt3.Columns[51].ColumnName = "离岗";
                dt3.Columns[52].ColumnName = "计划出勤";
                dt3.Columns[53].ColumnName = "实际出勤";
                if (cbxTotal.Checked == false)
                {
                    for (int i = 53; i > 37; i--)
                    {
                        dt3.Columns.RemoveAt(i);
                    }
                }
                if (cbxDetail.Checked == false)
                {
                    for (int i = 37; i > 6; i--)
                    {
                        dt3.Columns.RemoveAt(i);
                    }
                }
                /*dt3.Columns.Add("店铺").SetOrdinal(0);
                dt3.Columns.Add("员工").SetOrdinal(1);
                foreach (DataRow dr in dt3.Rows)
                {
                    dr["店铺"] = dr["店号"] + " " + dr["店名"];
                    dr["员工"] = dr["工号"] + " " + dr["姓名"] + " " + dr["职务"] + "   " + "事假：" + Decimal.Parse(dr["事假"].ToString()).ToString("f1") + " " + "病假：" + Decimal.Parse(dr["病假"].ToString()).ToString("f1") + " " + "产假：" + Decimal.Parse(dr["产假"].ToString()).ToString("f1") + " " + "年假：" + Decimal.Parse(dr["年假"].ToString()).ToString("f1") + " " + "婚假：" + Decimal.Parse(dr["婚假"].ToString()).ToString("f1") + " " + "丧假：" + Decimal.Parse(dr["丧假"].ToString()).ToString("f1") + " " + "调休：" + Decimal.Parse(dr["调休"].ToString()).ToString("f1") + " " + "加班：" + Decimal.Parse(dr["加班"].ToString()).ToString("f1") + " " + "公休：" + Decimal.Parse(dr["公休"].ToString()).ToString("f1") + " " + "公干：" + Decimal.Parse(dr["公干"].ToString()).ToString("f1") + " " + "迟到：" + Decimal.Parse(dr["迟到"].ToString()).ToString("f1") + " " + "早退：" + Decimal.Parse(dr["早退"].ToString()).ToString("f1") + " " + "旷工：" + Decimal.Parse(dr["旷工"].ToString()).ToString("f1") + " " + "离岗：" + Decimal.Parse(dr["离岗"].ToString()).ToString("f1") + " " + "实际/计划出勤：" + Decimal.Parse(dr["实际出勤"].ToString()).ToString("f1") + "/" + Decimal.Parse(dr["计划出勤"].ToString()).ToString("f1");
                }
                dt3.Columns.Remove("店名");
                dt3.Columns.Remove("姓名");
                dt3.Columns.Remove("职务");
                dt3.Columns.Remove("店号");
                dt3.Columns.Remove("年");
                dt3.Columns.Remove("月");
                dt3.Columns.Remove("工号");
                dt3.Columns.Remove("事假");
                dt3.Columns.Remove("病假");
                dt3.Columns.Remove("产假");
                dt3.Columns.Remove("年假");
                dt3.Columns.Remove("婚假");
                dt3.Columns.Remove("丧假");
                dt3.Columns.Remove("调休");
                dt3.Columns.Remove("加班");
                dt3.Columns.Remove("公休");
                dt3.Columns.Remove("公干");
                dt3.Columns.Remove("迟到");
                dt3.Columns.Remove("早退");
                dt3.Columns.Remove("旷工");
                dt3.Columns.Remove("离岗");
                dt3.Columns.Remove("计划出勤");
                dt3.Columns.Remove("实际出勤");*/
                this.dgAttendance.DataSource = dt3;/*
                dgAttendance.ShowGroupPanel = true;
                this.dgAttendance.AutoExpandGroups = true;
                this.dgAttendance.GroupDescriptors.Clear();
                this.dgAttendance.SortDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
                this.dgAttendance.GroupDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
                this.dgAttendance.GroupDescriptors.Add("员工", System.ComponentModel.ListSortDirection.Ascending);*/
                //this.dgAttendance.MasterTemplate.DataView.PagingBeforeGrouping = true;
                //this.dgAttendance.BestFitColumns();
                //this.dgAttendance.MasterTemplate.CollapseAllGroups();
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                });
            }
        }

        void btnprint_Click(object sender, EventArgs e)
        {
            if (dgAttendance.Rows.Count == 0)
                return;
            this.dgAttendance.Print(true, radPrintDocument);
            MessageBox.Show("打印成功", "提示");

        }

        void html_Click(object sender, EventArgs e)
        {
            if (dgAttendance.Rows.Count == 0)
                return;

            /*SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Html File (*.htm)|*.htm";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "导出HTML文件到";

            saveFileDialog.FileName = year.ToString() + "年" + month.ToString() + "月考勤报表";
            saveFileDialog.RestoreDirectory = true;
            LoadDate();
            this.dgAttendance.ShowGroupPanel = false;
            this.dgAttendance.MasterTemplate.ExpandAll();
            RunExportToHTML(saveFileDialog.FileName, ref openExportFile);
            this.dgAttendance.ShowGroupPanel = true;
            if (openExportFile)
            {
                try
                {
                    System.Diagnostics.Process.Start(saveFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    string message = String.Format("The file cannot be opened on your system.\nError message: {0}", ex.Message);
                    RadMessageBox.Show(message, "Open File", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }*/
        }

        private void RunExportToHTML(string fileName, ref bool openExportFile)
        {
            ExportToHTML htmlExporter = new ExportToHTML(this.dgAttendance);

            htmlExporter.SummariesExportOption = SummariesOption.ExportAll;
            //set export settings
            htmlExporter.ExportVisualSettings = true;
            htmlExporter.ExportHierarchy = true;
            htmlExporter.HiddenColumnOption = HiddenOption.DoNotExport;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                htmlExporter.RunExport(fileName);

                RadMessageBox.SetThemeName(this.dgAttendance.ThemeName);
                DialogResult dr = RadMessageBox.Show("是否要导出到HTML并打开浏览？",
                    "Export to HTML", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    openExportFile = true;
                }
            }
            catch (IOException ex)
            {
                RadMessageBox.SetThemeName(this.dgAttendance.ThemeName);
                RadMessageBox.Show(this, ex.Message, "I/O Error", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LoadDate()
        {
            DataTable dt1 = new DataTable();
            dt1 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", year.ToString() + month.ToString()), new SqlParameter("@is_detail", "1"), new SqlParameter("@transaction", "query"));
            for (int i = 0; i < dt1.Columns.Count; i++)
            {
                if (dt1.Columns.Count % 2 != 0)
                {
                    if (i > 6 && i < 39 && i % 2 == 0)
                    {
                        dt1.Columns.RemoveAt(i);
                        dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                    }
                }
                else
                {
                    if (i > 6 && i < 39 && i % 2 != 0)
                    {
                        dt1.Columns.RemoveAt(i);
                        dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                    }
                }

            }
            DataTable dt2 = new DataTable();
            dt2 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", year.ToString() + month.ToString()), new SqlParameter("@is_detail", "1"), new SqlParameter("@transaction", "query"));
            for (int i = 0; i < dt2.Columns.Count; i++)
            {
                if (dt2.Columns.Count % 2 == 0)
                {
                    if (i > 6 && i < 38 && i % 2 == 0)
                    {
                        dt2.Columns.RemoveAt(i);
                        dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                    }
                }
                else
                {
                    if (i > 6 && i < 38 && i % 2 != 0)
                    {
                        dt2.Columns.RemoveAt(i);
                        dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                    }
                }
            }
            foreach (DataRow dr in dt2.Rows)
            {
                dt1.ImportRow(dr);
            }
            DataView dv = dt1.DefaultView;
            dv.Sort = "emp_id Asc";
            DataTable dt3 = dv.ToTable();
            dt3.Columns[0].ColumnName = "店名";
            dt3.Columns[1].ColumnName = "姓名";
            dt3.Columns[2].ColumnName = "职务";
            dt3.Columns[3].ColumnName = "店号";
            dt3.Columns[4].ColumnName = "年";
            dt3.Columns[5].ColumnName = "月";
            dt3.Columns[6].ColumnName = "工号";
            dt3.Columns[38].ColumnName = "事假";
            dt3.Columns[39].ColumnName = "病假";
            dt3.Columns[40].ColumnName = "产假";
            dt3.Columns[41].ColumnName = "年假";
            dt3.Columns[42].ColumnName = "婚假";
            dt3.Columns[43].ColumnName = "丧假";
            dt3.Columns[44].ColumnName = "调休";
            dt3.Columns[45].ColumnName = "加班";
            dt3.Columns[46].ColumnName = "公休";
            dt3.Columns[47].ColumnName = "公干";
            dt3.Columns[48].ColumnName = "迟到";
            dt3.Columns[49].ColumnName = "早退";
            dt3.Columns[50].ColumnName = "旷工";
            dt3.Columns[51].ColumnName = "离岗";
            dt3.Columns[52].ColumnName = "计划出勤";
            dt3.Columns[53].ColumnName = "实际出勤";
            dt3.Columns.Add("店铺").SetOrdinal(0);
            dt3.Columns.Add("员工").SetOrdinal(1);
            foreach (DataRow dr in dt3.Rows)
            {
                dr["店铺"] = dr["店号"] + " " + dr["店名"];
                dr["员工"] = dr["工号"] + " " + dr["姓名"] + " " + dr["职务"] + "   " + "事假：" + dr["事假"] + " " + "病假：" + dr["病假"] + " " + "产假：" + dr["产假"] + " " + "年假：" + dr["年假"] + " " + "婚假：" + dr["婚假"] + " " + "丧假：" + dr["丧假"] + " " + "调休：" + dr["调休"] + " " + "加班：" + dr["加班"] + " " + "公休：" + dr["公休"] + " " + "公干：" + dr["公干"] + " " + "迟到：" + dr["迟到"] + " " + "早退：" + dr["早退"] + " " + "旷工：" + dr["旷工"] + " " + "离岗：" + dr["离岗"] + " " + "计划出勤：" + dr["计划出勤"] + " " + "实际出勤：" + dr["实际出勤"] + " ";
            }
            dt3.Columns.Remove("店名");
            dt3.Columns.Remove("姓名");
            dt3.Columns.Remove("职务");
            dt3.Columns.Remove("店号");
            dt3.Columns.Remove("年");
            dt3.Columns.Remove("月");
            dt3.Columns.Remove("工号");
            dt3.Columns.Remove("事假");
            dt3.Columns.Remove("病假");
            dt3.Columns.Remove("产假");
            dt3.Columns.Remove("年假");
            dt3.Columns.Remove("婚假");
            dt3.Columns.Remove("丧假");
            dt3.Columns.Remove("调休");
            dt3.Columns.Remove("加班");
            dt3.Columns.Remove("公休");
            dt3.Columns.Remove("公干");
            dt3.Columns.Remove("迟到");
            dt3.Columns.Remove("早退");
            dt3.Columns.Remove("旷工");
            dt3.Columns.Remove("离岗");
            dt3.Columns.Remove("计划出勤");
            dt3.Columns.Remove("实际出勤");
            this.dgAttendance.DataSource = dt3;
            dgAttendance.ShowGroupPanel = true;
            this.dgAttendance.AutoExpandGroups = true;
            this.dgAttendance.GroupDescriptors.Clear();
            this.dgAttendance.SortDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
            this.dgAttendance.GroupDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
            this.dgAttendance.GroupDescriptors.Add("员工", System.ComponentModel.ListSortDirection.Ascending);
            this.dgAttendance.MasterTemplate.DataView.PagingBeforeGrouping = true;
        }

        private void LoadDateDetail()
        {
            DataTable dt1 = new DataTable();
            dt1 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", year.ToString() + month.ToString()), new SqlParameter("@is_detail", "1"), new SqlParameter("@transaction", "query"));
            for (int i = 0; i < dt1.Columns.Count; i++)
            {
                if (dt1.Columns.Count % 2 != 0)
                {
                    if (i > 6 && i < 39 && i % 2 == 0)
                    {
                        dt1.Columns.RemoveAt(i);
                        dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                    }
                }
                else
                {
                    if (i > 6 && i < 39 && i % 2 != 0)
                    {
                        dt1.Columns.RemoveAt(i);
                        dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                    }
                }

            }
            DataTable dt2 = new DataTable();
            dt2 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", year.ToString() + month.ToString()), new SqlParameter("@is_detail", "1"), new SqlParameter("@transaction", "query"));
            for (int i = 0; i < dt2.Columns.Count; i++)
            {
                if (dt2.Columns.Count % 2 == 0)
                {
                    if (i > 6 && i < 38 && i % 2 == 0)
                    {
                        dt2.Columns.RemoveAt(i);
                        dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                    }
                }
                else
                {
                    if (i > 6 && i < 38 && i % 2 != 0)
                    {
                        dt2.Columns.RemoveAt(i);
                        dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                    }
                }
            }
            foreach (DataRow dr in dt2.Rows)
            {
                dt1.ImportRow(dr);
            }
            DataView dv = dt1.DefaultView;
            dv.Sort = "emp_id Asc";
            DataTable dt3 = dv.ToTable();
            dt3.Columns[0].ColumnName = "店名";
            dt3.Columns[1].ColumnName = "姓名";
            dt3.Columns[2].ColumnName = "职务";
            dt3.Columns[3].ColumnName = "店号";
            dt3.Columns[4].ColumnName = "年";
            dt3.Columns[5].ColumnName = "月";
            dt3.Columns[6].ColumnName = "工号";
            dt3.Columns[38].ColumnName = "事假";
            dt3.Columns[39].ColumnName = "病假";
            dt3.Columns[40].ColumnName = "产假";
            dt3.Columns[41].ColumnName = "年假";
            dt3.Columns[42].ColumnName = "婚假";
            dt3.Columns[43].ColumnName = "丧假";
            dt3.Columns[44].ColumnName = "调休";
            dt3.Columns[45].ColumnName = "加班";
            dt3.Columns[46].ColumnName = "公休";
            dt3.Columns[47].ColumnName = "公干";
            dt3.Columns[48].ColumnName = "迟到";
            dt3.Columns[49].ColumnName = "早退";
            dt3.Columns[50].ColumnName = "旷工";
            dt3.Columns[51].ColumnName = "离岗";
            dt3.Columns[52].ColumnName = "计划出勤";
            dt3.Columns[53].ColumnName = "实际出勤";
        }
        void dgAttendance_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement.RowElement is GridDataRowElement)
            {
                if (e.Row.Cells[e.ColumnIndex].Value != null)
                {
                    if (e.Row.Cells[e.ColumnIndex].Value.ToString().Contains("迟") || e.Row.Cells[e.ColumnIndex].Value.ToString().Contains("早"))
                    {
                        e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local); e.CellElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local); e.CellElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local); e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local); 
                        e.CellElement.DrawFill = true; 
                        e.CellElement.ForeColor = Color.Red ;
                        e.CellElement.ZIndex = 100;

                    }
                    else if (e.Row.Cells[e.ColumnIndex].Value.ToString().Contains("×"))
                    {
                        e.CellElement.DrawFill = true;
                        e.CellElement.GradientStyle = GradientStyles.Linear;
                        e.CellElement.GradientPercentage = 0.45f;
                        e.CellElement.GradientPercentage2 = 0.53f;
                        e.CellElement.NumberOfColors = 4;
                        e.CellElement.BackColor = Color.FromArgb(253, 141, 142);
                        e.CellElement.BackColor2 = Color.FromArgb(254, 86, 86);
                        e.CellElement.BackColor3 = Color.FromArgb(254, 55, 55);
                        e.CellElement.BackColor4 = Color.FromArgb(254, 31, 32);
                        e.CellElement.BorderGradientStyle = GradientStyles.Solid;
                        e.CellElement.BorderColor = Color.FromArgb(254, 31, 32);
                        e.CellElement.ZIndex = 100;
                    }
                    else 
                    {
                        //e.CellElement.ResetValue(RadItem.BackColorProperty);
                        //e.CellElement.ResetValue(LightVisualElement.DrawFillProperty);
                        //e.CellElement.ResetValue(LightVisualElement.GradientStyleProperty);
                        //e.CellElement.ResetValue(LightVisualElement.GradientPercentageProperty);
                        //e.CellElement.ResetValue(LightVisualElement.GradientPercentage2Property);
                        //e.CellElement.ResetValue(LightVisualElement.NumberOfColorsProperty);
                        //e.CellElement.ResetValue(LightVisualElement.BackColor2Property);
                        //e.CellElement.ResetValue(LightVisualElement.BackColor3Property);
                        //e.CellElement.ResetValue(LightVisualElement.BackColor4Property);
                        //e.CellElement.ResetValue(LightVisualElement.BorderGradientStyleProperty);
                        //e.CellElement.ResetValue(LightVisualElement.BorderColorProperty);
                        //e.CellElement.ResetValue(RadElement.ZIndexProperty);
                        e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local); 
                    }
                }
            }
        }

        void excel_Click(object sender, EventArgs e)
        {
            if (dgAttendance.Rows.Count == 0)
                return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl   files   (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "导出Excel文件到";

            saveFileDialog.FileName = year.ToString() + "年" + month.ToString() + "月考勤报表";
            saveFileDialog.RestoreDirectory = true;
            //LoadDateDetail();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream myStream;
                myStream = saveFileDialog.OpenFile();
                StreamWriter sw = new StreamWriter(myStream, System.Text.Encoding.GetEncoding("gb2312"));
                string str = "";
                try
                {
                    //写标题     
                    for (int i = 0; i < dgAttendance.Columns.Count; i++)
                    {
                        if (i > 0)
                        {
                            str += "\t";
                        }
                        str += dgAttendance.Columns[i].HeaderText;
                    }

                    sw.WriteLine(str);
                    //写内容   
                    for (int j = 0; j < dgAttendance.Rows.Count; j++)
                    {
                        string tempStr = "";
                        for (int k = 0; k < dgAttendance.Columns.Count; k++)
                        {
                            if (k > 0)
                            {
                                tempStr += "\t";
                            }
                            if (dgAttendance.Rows[j].Cells[k].Value == null)
                            {
                                    tempStr += "";
                            }
                            else
                                tempStr += dgAttendance.Rows[j].Cells[k].Value.ToString().Replace("\"", "");
                        }
                        sw.WriteLine(tempStr);
                    }
                    sw.Close();
                    myStream.Close();
                    MessageBox.Show("导出成功", "提示");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    sw.Close();
                    myStream.Close();
                }
            }
        }

        void btnFinish_Click(object sender, EventArgs e)
        {
            if(dgAttendance.Rows.Count > 0)
            {
                if (RadMessageBox.Show("确认生效吗？", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    try
                    {
                        int r = SqlHelper.TakeStoredProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", year.ToString() + month.ToString().PadLeft(2, '0')), new SqlParameter("@is_detail", "1"), new SqlParameter("@transaction", "update"));
                        RadMessageBox.Show(this, "生效成功", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show(this, "生效失败" + ex.Message, "提示", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
                
            }
        }

        void radGridView3_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.Row.Cells[1].Value != null)
            {
                switch (e.Row.Cells[1].Value.ToString())
                {
                    case "1":
                        e.Row.Cells[2].Value = "事假";
                        break;
                    case "2":
                        e.Row.Cells[2].Value = "病假";
                        break;
                    case "3":
                        e.Row.Cells[2].Value = "调休";
                        break;
                    case "4":
                        e.Row.Cells[2].Value = "加班";
                        break;
                    case "5":
                        e.Row.Cells[2].Value = "公休";
                        break;
                    case "6":
                        e.Row.Cells[2].Value = "公干";
                        break;
                    case "7":
                        e.Row.Cells[2].Value = "产假";
                        break;
                    case "8":
                        e.Row.Cells[2].Value = "年假";
                        break;
                    case "9":
                        e.Row.Cells[2].Value = "婚假";
                        break;
                    case "10":
                        e.Row.Cells[2].Value = "丧假";
                        break;
                    case "11":
                        e.Row.Cells[2].Value = "离岗";
                        break;
                    case "12":
                        e.Row.Cells[2].Value = "补签";
                        break;
                }
            }
        }

        void radGridView2_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.ColumnIndex > 2 && e.Row.Cells[e.ColumnIndex].Value != null && e.Row.Cells[e.ColumnIndex].Value.ToString() != "休")
            {
                e.Row.Cells[e.ColumnIndex].Value = DateTime.Parse(e.Row.Cells[e.ColumnIndex].Value.ToString()).ToString("HH:mm:ss");
                if (e.Row.Cells[e.ColumnIndex].Value.ToString() == "00:00:00")
                    e.Row.Cells[e.ColumnIndex].Value = "休";
            }
        }

        void dgAttendance_CellClick(object sender, GridViewCellEventArgs e)
        {
            //string head = (((Telerik.WinControls.UI.GridViewCellEventArgsBase)(e)).Row).Group.Header;

            //if (e..Column.HeaderText.Contains("工号"))
            //{
            if (dgAttendance.Rows.Count == 0 || e.RowIndex == -1)
                return;

                radGridView1.DataSource = YJH.Services.AttendanceService.GetattendanceByemp(dgAttendance.Rows[e.RowIndex].Cells["工号"].Value.ToString(), new DateTime(year, month, 1), new DateTime(year, month, 1).AddMonths(1));
                radGridView3.DataSource = YJH.Services.AttendanceService.Gethr_vacation_stByemp(dgAttendance.Rows[e.RowIndex].Cells["工号"].Value.ToString());
                DataTable dt1 = YJH.Services.AttendanceService.Gethr_schedule_stByemp(dgAttendance.Rows[e.RowIndex].Cells["工号"].Value.ToString(), year, month);
                if(dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Columns.Count; i++)
                    {
                        if (dt1.Columns.Count % 2 != 0)
                        {
                            if (i > 2 && i % 2 == 0)
                            {
                                dt1.Columns.RemoveAt(i);
                                dt1.Columns[i - 1].ColumnName = (i - 3).ToString() + "号";
                            }
                        }
                        else
                        {
                            if (i > 2 && i % 2 != 0)
                            {
                                dt1.Columns.RemoveAt(i);
                                dt1.Columns[i - 1].ColumnName = (i - 3).ToString() + "号";
                            }
                        }

                    }
                    DataTable dt2 = YJH.Services.AttendanceService.Gethr_schedule_stByemp(dgAttendance.Rows[e.RowIndex].Cells["工号"].Value.ToString(), year, month);
                    for (int i = 0; i < dt2.Columns.Count; i++)
                    {
                        if (dt2.Columns.Count % 2 == 0)
                        {
                            if (i > 2 && i % 2 == 0)
                            {
                                dt2.Columns.RemoveAt(i);
                                dt2.Columns[i].ColumnName = (i - 2).ToString() + "号";
                            }
                        }
                        else
                        {
                            if (i > 2 && i % 2 != 0)
                            {
                                dt2.Columns.RemoveAt(i);
                                dt2.Columns[i].ColumnName = (i - 2).ToString() + "号";
                            }
                        }
                    }
                    foreach (DataRow dr in dt2.Rows)
                    {
                        dt1.ImportRow(dr);
                    }
                    DataView dv = dt1.DefaultView;
                    DataTable dt4 = dv.ToTable();
                    dt4.Columns[0].ColumnName = "店铺";
                    dt4.Columns[1].ColumnName = "上班";
                    dt4.Columns[2].ColumnName = "下班";
                    radGridView2.DataSource = dt4;
                }
            //}
            //else
            //{
            //    radGridView1.DataSource = null;
            //    radGridView2.DataSource = null;
            //    radGridView3.DataSource = null;
            //}
        }

        void UC_AttendanceReport_Load(object sender, EventArgs e)
        {
            int y = OrgUnitService.GetDate().Year - 1;
            for (int i = 0; i < 5; i++)
            {
                ddlYear.Items.Add(y.ToString());
                y++;
            }
        }

        private void Search()
        {
            using (TransactionScope trans = new TransactionScope())
            {
                try
                {
                    if (ddlYear.Text == "" || ddlMonth.Text == "")
                    {
                        RadMessageBox.Show(this, "请选择年份以及日期", "提示", MessageBoxButtons.OK, RadMessageIcon.Info);
                        return;
                    }
                    year = int.Parse(ddlYear.Text);
                    month = int.Parse(ddlMonth.Text);
                    string isdetail = "0";
                    if (cbxIsDetail.Checked)
                        isdetail = "1";
                    else
                        isdetail = "0";
                    DataTable dt1 = new DataTable();
                    dt1 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", ddlYear.Text + ddlMonth.Text), new SqlParameter("@is_detail", isdetail), new SqlParameter("@transaction", "query"));
                    for (int i = 0; i < dt1.Columns.Count; i++)
                    {
                        if (dt1.Columns.Count % 2 != 0)
                        {
                            if (i > 6 && i < 39 && i % 2 == 0)
                            {
                                dt1.Columns.RemoveAt(i);
                                dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                            }
                        }
                        else
                        {
                            if (i > 6 && i < 39 && i % 2 != 0)
                            {
                                dt1.Columns.RemoveAt(i);
                                dt1.Columns[i - 1].ColumnName = (i - 7).ToString() + "号";
                            }
                        }

                    }
                    DataTable dt2 = new DataTable();
                    dt2 = SqlHelper.TableProcedure("[dbo].[p_st_attendance]", new SqlParameter("@yyyymm", ddlYear.Text + ddlMonth.Text), new SqlParameter("@is_detail", isdetail), new SqlParameter("@transaction", "query"));
                    for (int i = 0; i < dt2.Columns.Count; i++)
                    {
                        if (dt2.Columns.Count % 2 == 0)
                        {
                            if (i > 6 && i < 38 && i % 2 == 0)
                            {
                                dt2.Columns.RemoveAt(i);
                                dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                            }
                        }
                        else
                        {
                            if (i > 6 && i < 38 && i % 2 != 0)
                            {
                                dt2.Columns.RemoveAt(i);
                                dt2.Columns[i].ColumnName = (i - 6).ToString() + "号";
                            }
                        }
                    }
                    foreach (DataRow dr in dt2.Rows)
                    {
                        dt1.ImportRow(dr);
                    }
                    DataView dv = dt1.DefaultView;
                    dv.Sort = "store_id,emp_id Asc";
                    DataTable dt3 = dv.ToTable();
                    dt3.Columns[0].ColumnName = "店名";
                    dt3.Columns[1].ColumnName = "姓名";
                    dt3.Columns[2].ColumnName = "职务";
                    dt3.Columns[3].ColumnName = "店号";
                    dt3.Columns[4].ColumnName = "年";
                    dt3.Columns[5].ColumnName = "月";
                    dt3.Columns[6].ColumnName = "工号";
                    dt3.Columns[38].ColumnName = "事假";
                    dt3.Columns[39].ColumnName = "病假";
                    dt3.Columns[40].ColumnName = "产假";
                    dt3.Columns[41].ColumnName = "年假";
                    dt3.Columns[42].ColumnName = "婚假";
                    dt3.Columns[43].ColumnName = "丧假";
                    dt3.Columns[44].ColumnName = "调休";
                    dt3.Columns[45].ColumnName = "加班";
                    dt3.Columns[46].ColumnName = "公休";
                    dt3.Columns[47].ColumnName = "公干";
                    dt3.Columns[48].ColumnName = "迟到";
                    dt3.Columns[49].ColumnName = "早退";
                    dt3.Columns[50].ColumnName = "旷工";
                    dt3.Columns[51].ColumnName = "离岗";
                    dt3.Columns[52].ColumnName = "计划出勤";
                    dt3.Columns[53].ColumnName = "实际出勤";
                    if(cbxTotal.Checked == false)
                    {
                        for (int i = 53; i > 37; i--)
                        {
                            dt3.Columns.RemoveAt(i);
                        }
                    }
                    if(cbxDetail.Checked == false)
                    {
                        for (int i = 37; i > 6; i--)
                        {
                            dt3.Columns.RemoveAt(i);
                        }
                    }
                    /*dt3.Columns.Add("店铺").SetOrdinal(0);
                    dt3.Columns.Add("员工").SetOrdinal(1);
                    foreach (DataRow dr in dt3.Rows)
                    {
                        dr["店铺"] = dr["店号"] + " " + dr["店名"];
                        dr["员工"] = dr["工号"] + " " + dr["姓名"] + " " + dr["职务"] + "   " + "事假：" + Decimal.Parse(dr["事假"].ToString()).ToString("f1") + " " + "病假：" + Decimal.Parse(dr["病假"].ToString()).ToString("f1") + " " + "产假：" + Decimal.Parse(dr["产假"].ToString()).ToString("f1") + " " + "年假：" + Decimal.Parse(dr["年假"].ToString()).ToString("f1") + " " + "婚假：" + Decimal.Parse(dr["婚假"].ToString()).ToString("f1") + " " + "丧假：" + Decimal.Parse(dr["丧假"].ToString()).ToString("f1") + " " + "调休：" + Decimal.Parse(dr["调休"].ToString()).ToString("f1") + " " + "加班：" + Decimal.Parse(dr["加班"].ToString()).ToString("f1") + " " + "公休：" + Decimal.Parse(dr["公休"].ToString()).ToString("f1") + " " + "公干：" + Decimal.Parse(dr["公干"].ToString()).ToString("f1") + " " + "迟到：" + Decimal.Parse(dr["迟到"].ToString()).ToString("f1") + " " + "早退：" + Decimal.Parse(dr["早退"].ToString()).ToString("f1") + " " + "旷工：" + Decimal.Parse(dr["旷工"].ToString()).ToString("f1") + " " + "离岗：" + Decimal.Parse(dr["离岗"].ToString()).ToString("f1") + " " + "实际/计划出勤：" + Decimal.Parse(dr["实际出勤"].ToString()).ToString("f1") + "/" + Decimal.Parse(dr["计划出勤"].ToString()).ToString("f1");
                    }
                    dt3.Columns.Remove("店名");
                    dt3.Columns.Remove("姓名");
                    dt3.Columns.Remove("职务");
                    dt3.Columns.Remove("店号");
                    dt3.Columns.Remove("年");
                    dt3.Columns.Remove("月");
                    dt3.Columns.Remove("工号");
                    dt3.Columns.Remove("事假");
                    dt3.Columns.Remove("病假");
                    dt3.Columns.Remove("产假");
                    dt3.Columns.Remove("年假");
                    dt3.Columns.Remove("婚假");
                    dt3.Columns.Remove("丧假");
                    dt3.Columns.Remove("调休");
                    dt3.Columns.Remove("加班");
                    dt3.Columns.Remove("公休");
                    dt3.Columns.Remove("公干");
                    dt3.Columns.Remove("迟到");
                    dt3.Columns.Remove("早退");
                    dt3.Columns.Remove("旷工");
                    dt3.Columns.Remove("离岗");
                    dt3.Columns.Remove("计划出勤");
                    dt3.Columns.Remove("实际出勤");*/
                    this.dgAttendance.DataSource = dt3;
                    /*dgAttendance.ShowGroupPanel = true;
                    this.dgAttendance.AutoExpandGroups = true;
                    this.dgAttendance.GroupDescriptors.Clear();
                    this.dgAttendance.SortDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
                    this.dgAttendance.GroupDescriptors.Add("店铺", System.ComponentModel.ListSortDirection.Ascending);
                    this.dgAttendance.GroupDescriptors.Add("员工", System.ComponentModel.ListSortDirection.Ascending);
                    this.dgAttendance.MasterTemplate.DataView.PagingBeforeGrouping = true;*/
                    //this.dgAttendance.BestFitColumns();
                    //this.dgAttendance.MasterTemplate.CollapseAllGroups();
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }
                trans.Complete();
            }
        } 
    }
}
