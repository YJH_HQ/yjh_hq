﻿namespace YJH_HQ.UI.Reports.ChangeShifts
{
    partial class UC_AttendanceReport
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.RadPrintWatermark radPrintWatermark1 = new Telerik.WinControls.UI.RadPrintWatermark();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbxDetail = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxTotal = new Telerik.WinControls.UI.RadCheckBox();
            this.tb = new Telerik.WinControls.UI.RadToggleButton();
            this.btnQuery = new Telerik.WinControls.UI.RadButton();
            this.btnprint = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnExcel = new Telerik.WinControls.UI.RadDropDownButton();
            this.excel = new Telerik.WinControls.UI.RadMenuItem();
            this.html = new Telerik.WinControls.UI.RadMenuItem();
            this.btnFinish = new Telerik.WinControls.UI.RadButton();
            this.cbxIsDetail = new Telerik.WinControls.UI.RadCheckBox();
            this.ddlMonth = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlYear = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.dgAttendance = new Telerik.WinControls.UI.RadGridView();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.vp1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.vp2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.vp3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridView3 = new Telerik.WinControls.UI.RadGridView();
            this.radPrintDocument = new Telerik.WinControls.UI.RadPrintDocument();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFinish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxIsDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttendance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttendance.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.vp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.vp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            this.vp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.cbxDetail);
            this.radGroupBox1.Controls.Add(this.cbxTotal);
            this.radGroupBox1.Controls.Add(this.tb);
            this.radGroupBox1.Controls.Add(this.btnQuery);
            this.radGroupBox1.Controls.Add(this.btnprint);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.btnExcel);
            this.radGroupBox1.Controls.Add(this.btnFinish);
            this.radGroupBox1.Controls.Add(this.cbxIsDetail);
            this.radGroupBox1.Controls.Add(this.ddlMonth);
            this.radGroupBox1.Controls.Add(this.ddlYear);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1323, 80);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "查询条件";
            // 
            // cbxDetail
            // 
            this.cbxDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxDetail.Location = new System.Drawing.Point(246, 36);
            this.cbxDetail.Name = "cbxDetail";
            this.cbxDetail.Size = new System.Drawing.Size(115, 18);
            this.cbxDetail.TabIndex = 76;
            this.cbxDetail.Text = "是否显示考勤明细";
            this.cbxDetail.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // cbxTotal
            // 
            this.cbxTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxTotal.Location = new System.Drawing.Point(246, 11);
            this.cbxTotal.Name = "cbxTotal";
            this.cbxTotal.Size = new System.Drawing.Size(115, 18);
            this.cbxTotal.TabIndex = 75;
            this.cbxTotal.Text = "是否显示考勤汇总";
            this.cbxTotal.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // tb
            // 
            this.tb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tb.Location = new System.Drawing.Point(713, 35);
            this.tb.Name = "tb";
            this.tb.Size = new System.Drawing.Size(79, 20);
            this.tb.TabIndex = 74;
            this.tb.Text = "展开/收起";
            this.tb.Visible = false;
            // 
            // btnQuery
            // 
            this.btnQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuery.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btnQuery.Location = new System.Drawing.Point(890, 35);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnQuery.Size = new System.Drawing.Size(86, 20);
            this.btnQuery.TabIndex = 73;
            this.btnQuery.Text = "正式";
            // 
            // btnprint
            // 
            this.btnprint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnprint.Image = global::YJH_HQ.UI.Properties.Resources.Print16;
            this.btnprint.Location = new System.Drawing.Point(1166, 35);
            this.btnprint.Name = "btnprint";
            this.btnprint.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnprint.Size = new System.Drawing.Size(86, 20);
            this.btnprint.TabIndex = 72;
            this.btnprint.Text = "打印";
            // 
            // radLabel1
            // 
            this.radLabel1.ForeColor = System.Drawing.Color.Red;
            this.radLabel1.Location = new System.Drawing.Point(380, 35);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(318, 18);
            this.radLabel1.TabIndex = 71;
            this.radLabel1.Text = "√：正常，×：未打卡，迟：迟到，早：早退，空白：未排班";
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Image = global::YJH_HQ.UI.Properties.Resources.CancleButtonImage;
            this.btnExcel.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.excel,
            this.html});
            this.btnExcel.Location = new System.Drawing.Point(1074, 35);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(86, 20);
            this.btnExcel.TabIndex = 70;
            this.btnExcel.Text = "导出";
            // 
            // excel
            // 
            this.excel.AccessibleDescription = "导出EXCEL";
            this.excel.AccessibleName = "导出EXCEL";
            this.excel.Name = "excel";
            this.excel.Text = "导出EXCEL";
            this.excel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // html
            // 
            this.html.AccessibleDescription = "导出HTML";
            this.html.AccessibleName = "导出HTML";
            this.html.Name = "html";
            this.html.Text = "导出HTML";
            this.html.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Image = global::YJH_HQ.UI.Properties.Resources.Acceptance;
            this.btnFinish.Location = new System.Drawing.Point(982, 35);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnFinish.Size = new System.Drawing.Size(86, 20);
            this.btnFinish.TabIndex = 69;
            this.btnFinish.Text = "生效";
            // 
            // cbxIsDetail
            // 
            this.cbxIsDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxIsDetail.Location = new System.Drawing.Point(246, 60);
            this.cbxIsDetail.Name = "cbxIsDetail";
            this.cbxIsDetail.Size = new System.Drawing.Size(115, 18);
            this.cbxIsDetail.TabIndex = 68;
            this.cbxIsDetail.Text = "是否显示考勤差异";
            this.cbxIsDetail.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // ddlMonth
            // 
            this.ddlMonth.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "01";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "02";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "03";
            radListDataItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "04";
            radListDataItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "05";
            radListDataItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "06";
            radListDataItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "07";
            radListDataItem7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "08";
            radListDataItem8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "09";
            radListDataItem9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "10";
            radListDataItem10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "11";
            radListDataItem11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "12";
            radListDataItem12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem12.TextWrap = true;
            this.ddlMonth.Items.Add(radListDataItem1);
            this.ddlMonth.Items.Add(radListDataItem2);
            this.ddlMonth.Items.Add(radListDataItem3);
            this.ddlMonth.Items.Add(radListDataItem4);
            this.ddlMonth.Items.Add(radListDataItem5);
            this.ddlMonth.Items.Add(radListDataItem6);
            this.ddlMonth.Items.Add(radListDataItem7);
            this.ddlMonth.Items.Add(radListDataItem8);
            this.ddlMonth.Items.Add(radListDataItem9);
            this.ddlMonth.Items.Add(radListDataItem10);
            this.ddlMonth.Items.Add(radListDataItem11);
            this.ddlMonth.Items.Add(radListDataItem12);
            this.ddlMonth.Location = new System.Drawing.Point(157, 33);
            this.ddlMonth.Name = "ddlMonth";
            this.ddlMonth.Size = new System.Drawing.Size(61, 20);
            this.ddlMonth.TabIndex = 67;
            // 
            // ddlYear
            // 
            this.ddlYear.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlYear.Location = new System.Drawing.Point(54, 33);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(61, 20);
            this.ddlYear.TabIndex = 66;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(18, 35);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(30, 18);
            this.radLabel2.TabIndex = 65;
            this.radLabel2.Text = "年：";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(121, 35);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(30, 18);
            this.radLabel5.TabIndex = 57;
            this.radLabel5.Text = "月：";
            // 
            // btSearch
            // 
            this.btSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btSearch.Location = new System.Drawing.Point(798, 35);
            this.btSearch.Name = "btSearch";
            this.btSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btSearch.Size = new System.Drawing.Size(86, 20);
            this.btSearch.TabIndex = 50;
            this.btSearch.Text = "试算";
            // 
            // dgAttendance
            // 
            this.dgAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAttendance.Location = new System.Drawing.Point(0, 0);
            // 
            // dgAttendance
            // 
            this.dgAttendance.MasterTemplate.AllowAddNewRow = false;
            this.dgAttendance.MasterTemplate.AllowCellContextMenu = false;
            this.dgAttendance.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.dgAttendance.MasterTemplate.AllowDragToGroup = false;
            this.dgAttendance.MasterTemplate.AllowEditRow = false;
            this.dgAttendance.MasterTemplate.EnableGrouping = false;
            this.dgAttendance.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgAttendance.Name = "dgAttendance";
            this.dgAttendance.ReadOnly = true;
            this.dgAttendance.ShowGroupPanel = false;
            this.dgAttendance.Size = new System.Drawing.Size(1323, 495);
            this.dgAttendance.TabIndex = 9;
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.vp1);
            this.radPageView1.Controls.Add(this.vp2);
            this.radPageView1.Controls.Add(this.vp3);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.vp1;
            this.radPageView1.Size = new System.Drawing.Size(1323, 228);
            this.radPageView1.TabIndex = 10;
            this.radPageView1.Text = "明细";
            // 
            // vp1
            // 
            this.vp1.Controls.Add(this.radGridView1);
            this.vp1.ItemSize = new System.Drawing.SizeF(40F, 28F);
            this.vp1.Location = new System.Drawing.Point(10, 37);
            this.vp1.Name = "vp1";
            this.vp1.Size = new System.Drawing.Size(1302, 180);
            this.vp1.Text = "考勤";
            // 
            // radGridView1
            // 
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowColumnHeaderContextMenu = false;
            gridViewTextBoxColumn1.FieldName = "store_name";
            gridViewTextBoxColumn1.HeaderText = "店铺";
            gridViewTextBoxColumn1.Name = "clstore_name";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 200;
            gridViewDateTimeColumn1.FieldName = "check_time";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn1.FormatString = "{0:yyyy-MM-dd HH:mm:ss}";
            gridViewDateTimeColumn1.HeaderText = "打卡时间";
            gridViewDateTimeColumn1.Name = "clcheck_time";
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 120;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn1});
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.ShowGroupPanel = false;
            this.radGridView1.Size = new System.Drawing.Size(1302, 180);
            this.radGridView1.TabIndex = 0;
            this.radGridView1.Text = "radGridView1";
            // 
            // vp2
            // 
            this.vp2.Controls.Add(this.radGridView2);
            this.vp2.ItemSize = new System.Drawing.SizeF(40F, 28F);
            this.vp2.Location = new System.Drawing.Point(10, 37);
            this.vp2.Name = "vp2";
            this.vp2.Size = new System.Drawing.Size(924, 80);
            this.vp2.Text = "排班";
            // 
            // radGridView2
            // 
            this.radGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView2.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView2.MasterTemplate.AllowAddNewRow = false;
            this.radGridView2.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.ReadOnly = true;
            this.radGridView2.ShowGroupPanel = false;
            this.radGridView2.Size = new System.Drawing.Size(924, 80);
            this.radGridView2.TabIndex = 0;
            this.radGridView2.Text = "radGridView2";
            // 
            // vp3
            // 
            this.vp3.Controls.Add(this.radGridView3);
            this.vp3.ItemSize = new System.Drawing.SizeF(40F, 28F);
            this.vp3.Location = new System.Drawing.Point(10, 37);
            this.vp3.Name = "vp3";
            this.vp3.Size = new System.Drawing.Size(1302, 180);
            this.vp3.Text = "请假";
            // 
            // radGridView3
            // 
            this.radGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView3.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView3
            // 
            this.radGridView3.MasterTemplate.AllowAddNewRow = false;
            this.radGridView3.MasterTemplate.AllowColumnHeaderContextMenu = false;
            gridViewTextBoxColumn2.FieldName = "store_name";
            gridViewTextBoxColumn2.HeaderText = "店铺";
            gridViewTextBoxColumn2.Name = "clstore_name";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.FieldName = "vacation_type";
            gridViewTextBoxColumn3.HeaderText = "请假类型";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "clvacation_type";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "vacation_type1";
            gridViewTextBoxColumn4.HeaderText = "请假类型";
            gridViewTextBoxColumn4.Name = "clvacation_type1";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 80;
            gridViewDateTimeColumn2.FieldName = "vacation_s";
            gridViewDateTimeColumn2.FormatString = "{0:yyyy-MM-dd HH:mm:ss}";
            gridViewDateTimeColumn2.HeaderText = "开始时间";
            gridViewDateTimeColumn2.Name = "clvacation_s";
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 120;
            gridViewDateTimeColumn3.FieldName = "vacation_e";
            gridViewDateTimeColumn3.FormatString = "{0:yyyy-MM-dd HH:mm:ss}";
            gridViewDateTimeColumn3.HeaderText = "结束时间";
            gridViewDateTimeColumn3.Name = "cl_vacation_e";
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn3.Width = 120;
            this.radGridView3.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewDateTimeColumn2,
            gridViewDateTimeColumn3});
            this.radGridView3.Name = "radGridView3";
            this.radGridView3.ReadOnly = true;
            this.radGridView3.ShowGroupPanel = false;
            this.radGridView3.Size = new System.Drawing.Size(1302, 180);
            this.radGridView3.TabIndex = 0;
            this.radGridView3.Text = "radGridView3";
            // 
            // radPrintDocument
            // 
            this.radPrintDocument.FooterFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radPrintDocument.HeaderFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            radPrintWatermark1.Font = new System.Drawing.Font("宋体", 144F);
            this.radPrintDocument.Watermark = radPrintWatermark1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgAttendance);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 80);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1323, 495);
            this.panel1.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radPageView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 575);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1323, 228);
            this.panel2.TabIndex = 12;
            // 
            // UC_AttendanceReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_AttendanceReport";
            this.Size = new System.Drawing.Size(1323, 803);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFinish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxIsDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttendance.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAttendance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.vp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.vp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            this.vp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.WinControls.UI.RadGridView dgAttendance;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlMonth;
        private Telerik.WinControls.UI.RadDropDownList ddlYear;
        private Telerik.WinControls.UI.RadCheckBox cbxIsDetail;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage vp1;
        private Telerik.WinControls.UI.RadPageViewPage vp2;
        private Telerik.WinControls.UI.RadPageViewPage vp3;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadGridView radGridView3;
        private Telerik.WinControls.UI.RadButton btnFinish;
        private Telerik.WinControls.UI.RadDropDownButton btnExcel;
        private Telerik.WinControls.UI.RadMenuItem excel;
        private Telerik.WinControls.UI.RadMenuItem html;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton btnprint;
        private Telerik.WinControls.UI.RadPrintDocument radPrintDocument;
        private Telerik.WinControls.UI.RadButton btnQuery;
        private Telerik.WinControls.UI.RadToggleButton tb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadCheckBox cbxDetail;
        private Telerik.WinControls.UI.RadCheckBox cbxTotal;
    }
}
