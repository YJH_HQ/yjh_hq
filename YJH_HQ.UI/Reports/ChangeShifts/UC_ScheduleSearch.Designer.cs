﻿namespace YJH_HQ.UI.Reports.Schedule
{
    partial class UC_ScheduleSearch
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btnExcel = new Telerik.WinControls.UI.RadButton();
            this.tbxName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.dgSchedule = new Telerik.WinControls.UI.RadGridView();
            this.ddlYear = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlMonth = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddlMonth);
            this.radGroupBox1.Controls.Add(this.ddlYear);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.btnExcel);
            this.radGroupBox1.Controls.Add(this.tbxName);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(945, 75);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "查询条件";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(18, 35);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(30, 18);
            this.radLabel2.TabIndex = 65;
            this.radLabel2.Text = "年：";
            // 
            // btnExcel
            // 
            this.btnExcel.Image = global::YJH_HQ.UI.Properties.Resources.CancleButtonImage;
            this.btnExcel.Location = new System.Drawing.Point(827, 35);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnExcel.Size = new System.Drawing.Size(86, 20);
            this.btnExcel.TabIndex = 64;
            this.btnExcel.Text = "导出";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(446, 33);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(120, 20);
            this.tbxName.TabIndex = 63;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(398, 35);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(42, 18);
            this.radLabel1.TabIndex = 62;
            this.radLabel1.Text = "姓名：";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(121, 35);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(30, 18);
            this.radLabel5.TabIndex = 57;
            this.radLabel5.Text = "月：";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(272, 33);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(120, 20);
            this.ddtStore.TabIndex = 54;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(224, 35);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 18);
            this.radLabel3.TabIndex = 53;
            this.radLabel3.Text = "门店：";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btSearch.Location = new System.Drawing.Point(735, 35);
            this.btSearch.Name = "btSearch";
            this.btSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btSearch.Size = new System.Drawing.Size(86, 20);
            this.btSearch.TabIndex = 50;
            this.btSearch.Text = "查询";
            // 
            // dgSchedule
            // 
            this.dgSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSchedule.Location = new System.Drawing.Point(0, 75);
            // 
            // dgSchedule
            // 
            this.dgSchedule.MasterTemplate.AllowAddNewRow = false;
            this.dgSchedule.Name = "dgSchedule";
            this.dgSchedule.ReadOnly = true;
            this.dgSchedule.ShowGroupPanel = false;
            this.dgSchedule.Size = new System.Drawing.Size(945, 557);
            this.dgSchedule.TabIndex = 9;
            // 
            // ddlYear
            // 
            radListDataItem13.Text = "2015";
            radListDataItem13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem13.TextWrap = true;
            radListDataItem14.Text = "2016";
            radListDataItem14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem14.TextWrap = true;
            radListDataItem15.Text = "2017";
            radListDataItem15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem15.TextWrap = true;
            radListDataItem16.Text = "2018";
            radListDataItem16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem16.TextWrap = true;
            radListDataItem17.Text = "2020";
            radListDataItem17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem17.TextWrap = true;
            this.ddlYear.Items.Add(radListDataItem13);
            this.ddlYear.Items.Add(radListDataItem14);
            this.ddlYear.Items.Add(radListDataItem15);
            this.ddlYear.Items.Add(radListDataItem16);
            this.ddlYear.Items.Add(radListDataItem17);
            this.ddlYear.Location = new System.Drawing.Point(54, 33);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(61, 20);
            this.ddlYear.TabIndex = 66;
            // 
            // ddlMonth
            // 
            radListDataItem1.Text = "1";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "2";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "3";
            radListDataItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "4";
            radListDataItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "5";
            radListDataItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "6";
            radListDataItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "7";
            radListDataItem7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "8";
            radListDataItem8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "9";
            radListDataItem9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "10";
            radListDataItem10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "11";
            radListDataItem11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "12";
            radListDataItem12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem12.TextWrap = true;
            this.ddlMonth.Items.Add(radListDataItem1);
            this.ddlMonth.Items.Add(radListDataItem2);
            this.ddlMonth.Items.Add(radListDataItem3);
            this.ddlMonth.Items.Add(radListDataItem4);
            this.ddlMonth.Items.Add(radListDataItem5);
            this.ddlMonth.Items.Add(radListDataItem6);
            this.ddlMonth.Items.Add(radListDataItem7);
            this.ddlMonth.Items.Add(radListDataItem8);
            this.ddlMonth.Items.Add(radListDataItem9);
            this.ddlMonth.Items.Add(radListDataItem10);
            this.ddlMonth.Items.Add(radListDataItem11);
            this.ddlMonth.Items.Add(radListDataItem12);
            this.ddlMonth.Location = new System.Drawing.Point(157, 33);
            this.ddlMonth.Name = "ddlMonth";
            this.ddlMonth.Size = new System.Drawing.Size(61, 20);
            this.ddlMonth.TabIndex = 67;
            // 
            // UC_ScheduleSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgSchedule);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_ScheduleSearch";
            this.Size = new System.Drawing.Size(945, 632);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox tbxName;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.WinControls.UI.RadGridView dgSchedule;
        private Telerik.WinControls.UI.RadButton btnExcel;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlMonth;
        private Telerik.WinControls.UI.RadDropDownList ddlYear;
    }
}
