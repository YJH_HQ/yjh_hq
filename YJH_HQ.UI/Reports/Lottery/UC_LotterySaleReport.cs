﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Reports
{
    public partial class UC_LotterySaleReport : UserControl
    {
        public UC_LotterySaleReport()
        {
            InitializeComponent();
            this.Load += UC_LotterySaleReport_Load;
            this.btSearch.Click += (s, e) => { this.Search(); };
        }

        void UC_LotterySaleReport_Load(object sender, EventArgs e)
        {
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;
            var table = YJH.Services.LotteryBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var start = this.dtStartDate.Value.Date;
                var end = this.dtEndDate.Value.Date;
                if (start > end)
                {
                    RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                    return;
                }
                if(ddtStore.Text == "全部")
                {
                    RadMessageBox.Show("请选择需要查询的门店", "提示");
                    return;
                }
                var store = this.ddtStore.SelectedValue.ToString();

                try
                {
                    //var result = YJH.Services.ems_deliverExpress_hqService.Search(start.Date, end.AddDays(1).Date, store,"");
                    this.Invoke((MethodInvoker)delegate()
                    {
                        var report = new YJH_HQ.UI.Reports.RP_LotterySale();
                        report.ReportParameters["StoreName"].Value = this.ddtStore.SelectedItem.Text + "彩票销售报表";
                        report.ReportParameters["PrintDate"].Value = DateTime.Now.ToString("yyyy-MM-dd");
                        report.ReportParameters["StartDate"].Value = start.ToString("yyyy-MM-dd");
                        report.ReportParameters["EndDate"].Value = end.ToString("yyyy-MM-dd");
                        //report.LotterySaleSource.DataSource = result;
                        var reportSource = new Telerik.Reporting.InstanceReportSource();
                        reportSource.ReportDocument = report;
                        this.rpvExpress.ReportSource = reportSource;
                        this.rpvExpress.RefreshReport();
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }

            });
        }
    }
}
