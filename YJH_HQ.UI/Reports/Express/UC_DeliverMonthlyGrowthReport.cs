﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH_HQ.UI.Services;
using System.Data.SqlClient;
using YJH.Services;
using System.IO;

namespace YJH_HQ.UI.Reports.Express
{
    public partial class UC_DeliverMonthlyGrowthReport : UserControl
    {
        public UC_DeliverMonthlyGrowthReport()
        {
            InitializeComponent();
            List<string> list = new List<string>();
            int year = OrgUnitService.GetDate().Year -1;
            for (int i = 0; i < 6; i++)
            {
                list.Add(year.ToString());
                year = year + 1;
            }
            ddlYear.DataSource = list;
            ddlYear.SelectedIndex = 0;
            ddlMonth.Text = OrgUnitService.GetDate().Month.ToString();
            this.btSearch.Click += btSearch_Click;
            this.dgDeliverExpress.CellFormatting += dgDeliverExpress_CellFormatting;
            this.btnExcel.Click += btnExcel_Click;
        }

        void btnExcel_Click(object sender, EventArgs e)
        {
            if (dgDeliverExpress.Rows.Count == 0)
                return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl   files   (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "导出Excel文件到";

            saveFileDialog.FileName = ddlYear.Text + ddlMonth.Text + "快递日增长月报";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream myStream;
                myStream = saveFileDialog.OpenFile();
                StreamWriter sw = new StreamWriter(myStream, System.Text.Encoding.GetEncoding("gb2312"));
                string str = "";
                try
                {
                    //写标题     
                    for (int i = 0; i < dgDeliverExpress.ColumnCount; i++)
                    {
                        if (i > 0)
                        {
                            str += "\t";
                        }
                        str += dgDeliverExpress.Columns[i].HeaderText;
                    }

                    sw.WriteLine(str);
                    //写内容   
                    for (int j = 0; j < dgDeliverExpress.Rows.Count; j++)
                    {
                        string tempStr = "";
                        for (int k = 0; k < dgDeliverExpress.Columns.Count; k++)
                        {
                            if (k > 0)
                            {
                                tempStr += "\t";
                            }
                            if (dgDeliverExpress.Rows[j].Cells[k].Value == null)
                            {
                                tempStr += "";
                            }
                            else
                                tempStr += dgDeliverExpress.Rows[j].Cells[k].Value.ToString().Replace("\"", "");
                        }
                        sw.WriteLine(tempStr);
                    }
                    sw.Close();
                    myStream.Close();
                    MessageBox.Show("导出成功", "提示");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    sw.Close();
                    myStream.Close();
                }
            }
        }

        void dgDeliverExpress_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0){
                e.Column.Width = 70;
                e.Column.TextAlignment = ContentAlignment.MiddleCenter;
            }
            else if(e.ColumnIndex == 1)
                e.Column.Width = 120;
            else if (e.ColumnIndex == 2)
                e.Column.Width = 80;
            else if (e.ColumnIndex == 3)
                e.Column.Width = 80;
            else if (e.ColumnIndex == 4)
                e.Column.Width = 80;
            else if (e.ColumnIndex == 5)
                e.Column.Width = 80;
        }

        void btSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                string p1 = "";
                if(ddlMonth.Text.Length > 1)
                    p1 = ddlYear.Text + ddlMonth.Text;
                else
                    p1 = ddlYear.Text + ddlMonth.Text.PadLeft(2, '0');
                dt = SqlHelper.TableProcedure("[dbo].[p_st_ems_growth_and_days]", new SqlParameter("@month", p1));

                dgDeliverExpress.DataSource = dt;
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                });
            }
        }
    }
}
