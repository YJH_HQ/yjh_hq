﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace YJH_HQ.UI.Reports
{
    public partial class UC_SendExpressReport : UserControl
    {
        public UC_SendExpressReport()
        {
            InitializeComponent();
            this.Load += UC_SendExpressReport_Load;
            this.btSearch.Click += (s, e) => { this.Search(); };
        }

        void UC_SendExpressReport_Load(object sender, EventArgs e)
        {
            this.dtStartDate.Value = DateTime.Now;
            this.dtEndDate.Value = DateTime.Now;
            var table = YJH.Services.LotteryBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
            ddlExpressCompany.ValueMember = "ems_id";
            ddlExpressCompany.DisplayMember = "ems_name";
            var list = YJH.Services.ems_deliverExpress_hqService.GetExpresscompany();
            ddlExpressCompany.DataSource = list;
            ddlExpressCompany.SelectedIndex = -1;
        }

        private void Search()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var start = this.dtStartDate.Value.Date;
                var end = this.dtEndDate.Value.Date;
                if (start > end)
                {
                    RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                    return;
                }
                string store = "";
                if(ddtStore.Text != "全部")
                    store = this.ddtStore.SelectedValue.ToString();
                string emsid = "";
                if(ddlExpressCompany.SelectedValue != null)
                    emsid = ddlExpressCompany.SelectedValue.ToString();
                try
                {
                    var result = YJH.Services.ems_sendexpress_hqService.Search(start.Date, end.AddDays(1).Date, store, emsid);

                    this.Invoke((MethodInvoker)delegate()
                    {
                        var report = new YJH_HQ.UI.Reports.RP_SendExpress();
                        if(store != "")
                            report.ReportParameters["StoreName"].Value = this.ddtStore.SelectedItem.Text + "寄件报表";
                        else
                            report.ReportParameters["StoreName"].Value = "寄件报表";
                        report.ReportParameters["PrintDate"].Value = DateTime.Now.ToString("yyyy-MM-dd");
                        report.ReportParameters["StartDate"].Value = start.ToString("yyyy-MM-dd");
                        report.ReportParameters["EndDate"].Value = end.ToString("yyyy-MM-dd");
                        report.SendExpressSource.DataSource = result;
                        var reportSource = new Telerik.Reporting.InstanceReportSource();
                        reportSource.ReportDocument = report;
                        this.rpvExpress.ReportSource = reportSource;
                        this.rpvExpress.RefreshReport();
                    });
                }
                catch (Exception ex)
                {
                    this.Invoke((MethodInvoker)delegate()
                    {
                        RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                    });
                }

            });
        }
    }
}
