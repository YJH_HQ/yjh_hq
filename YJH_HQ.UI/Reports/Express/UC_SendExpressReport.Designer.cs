﻿namespace YJH_HQ.UI.Reports
{
    partial class UC_SendExpressReport
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddlExpressCompany = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.dtEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.dtStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.rpvExpress = new Telerik.ReportViewer.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlExpressCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddlExpressCompany);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.dtEndDate);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Controls.Add(this.dtStartDate);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(958, 57);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "查询条件";
            // 
            // ddlExpressCompany
            // 
            this.ddlExpressCompany.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlExpressCompany.Location = new System.Drawing.Point(634, 24);
            this.ddlExpressCompany.Name = "ddlExpressCompany";
            this.ddlExpressCompany.Size = new System.Drawing.Size(120, 20);
            this.ddlExpressCompany.TabIndex = 47;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(563, 24);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 18);
            this.radLabel4.TabIndex = 40;
            this.radLabel4.Text = "快递公司：";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(422, 24);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(125, 20);
            this.ddtStore.TabIndex = 37;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(379, 24);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 36;
            this.radLabel3.Text = "门店：";
            // 
            // dtEndDate
            // 
            this.dtEndDate.AutoSize = false;
            this.dtEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDate.Location = new System.Drawing.Point(236, 24);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(120, 20);
            this.dtEndDate.TabIndex = 35;
            this.dtEndDate.TabStop = false;
            this.dtEndDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(212, 24);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(18, 18);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "至";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btSearch.Location = new System.Drawing.Point(846, 24);
            this.btSearch.Name = "btSearch";
            this.btSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btSearch.Size = new System.Drawing.Size(86, 20);
            this.btSearch.TabIndex = 33;
            this.btSearch.Text = "查询";
            // 
            // dtStartDate
            // 
            this.dtStartDate.AutoSize = false;
            this.dtStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDate.Location = new System.Drawing.Point(88, 24);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Size = new System.Drawing.Size(120, 20);
            this.dtStartDate.TabIndex = 32;
            this.dtStartDate.TabStop = false;
            this.dtStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(10, 24);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(75, 18);
            this.radLabel1.TabIndex = 31;
            this.radLabel1.Text = "查询日期段：";
            // 
            // rpvExpress
            // 
            this.rpvExpress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpvExpress.Location = new System.Drawing.Point(0, 57);
            this.rpvExpress.Name = "rpvExpress";
            this.rpvExpress.Size = new System.Drawing.Size(958, 481);
            this.rpvExpress.TabIndex = 1;
            this.rpvExpress.ViewMode = Telerik.ReportViewer.WinForms.ViewMode.PrintPreview;
            // 
            // UC_SendExpressReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rpvExpress);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_SendExpressReport";
            this.Size = new System.Drawing.Size(958, 538);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlExpressCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDateTimePicker dtStartDate;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.ReportViewer.WinForms.ReportViewer rpvExpress;
        private Telerik.WinControls.UI.RadDateTimePicker dtEndDate;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList ddlExpressCompany;
    }
}

