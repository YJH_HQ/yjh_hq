﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using YJH.Services;

namespace YJH_HQ.UI.Reports
{
    public partial class UC_DeliverExpressReport : UserControl
    {
        DateTime st;
        DateTime et;
        DateTime start;
        DateTime end;
        public UC_DeliverExpressReport()
        {
            InitializeComponent();
            this.Load += UC_DeliverExpressReport_Load;
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.cbxDeliverStock.CheckStateChanged += cbxDeliverStock_CheckStateChanged;
            this.cbxAgentDeliver.CheckStateChanged += cbxAgentDeliver_CheckStateChanged;
        }

        void cbxAgentDeliver_CheckStateChanged(object sender, EventArgs e)
        {
            if (cbxAgentDeliver.Checked == true)
            {
                this.dtAgentStartDate.Enabled = true;
                this.dtAgentEndDate.Enabled = true;
            }
            else
            {
                this.dtAgentStartDate.Enabled = false;
                this.dtAgentEndDate.Enabled = false;
            }
        }

        void cbxDeliverStock_CheckStateChanged(object sender, EventArgs e)
        {
            if (cbxDeliverStock.Checked == true)
            {
                this.dtStockStartDate.Enabled = true;
                this.dtStockEndDate.Enabled = true;
            }
            else
            {
                this.dtStockStartDate.Enabled = false;
                this.dtStockEndDate.Enabled = false;
            }
        }

        void UC_DeliverExpressReport_Load(object sender, EventArgs e)
        {
            this.dtStockStartDate.Enabled = true;
            this.dtStockEndDate.Enabled = true;
            this.dtAgentStartDate.Enabled = false;
            this.dtAgentEndDate.Enabled = false;
            cbxDeliverStock.Checked = true;
            cbxAgentDeliver.Checked = false;

            this.dtStockStartDate.Value = OrgUnitService.GetDate();
            this.dtStockEndDate.Value = OrgUnitService.GetDate();
            this.dtAgentStartDate.Value = OrgUnitService.GetDate();
            this.dtAgentEndDate.Value = OrgUnitService.GetDate();

            st = this.dtStockStartDate.Value.Date;
            et = this.dtStockEndDate.Value.Date;
            start = DateTime.MinValue;
            end = DateTime.MinValue;
            ddlExpressCompany.ValueMember = "ems_id";
            ddlExpressCompany.DisplayMember = "ems_name";
            var list = YJH.Services.ems_deliverExpress_hqService.GetExpresscompany();
            ddlExpressCompany.DataSource = list;
            ddlExpressCompany.SelectedIndex = -1;
            var table = YJH.Services.LotteryBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
        }

        private void Search()
        {
            //System.Threading.Tasks.Task.Factory.StartNew(() =>
            //{
            if (cbxDeliverStock.Checked)
            {
                st = this.dtStockStartDate.Value.Date;
                et = this.dtStockEndDate.Value.Date;
            }
            else
            {
                st = DateTime.MinValue;
                et = DateTime.MinValue;
            }
            if (cbxAgentDeliver.Checked)
            {
                start = this.dtAgentStartDate.Value.Date;
                end = this.dtAgentEndDate.Value.Date;
            }
            else
            {
                start = DateTime.MinValue;
                end = DateTime.MinValue;
            }
            if (st > et)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            if (start > end)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            string store = "";
            if (ddtStore.Text != "全部")
                store = this.ddtStore.SelectedValue.ToString();

            try
            {
                var result = YJH.Services.ems_deliverExpress_hqService.Search(st, et, start, end, store, this.ddlExpressCompany.Text.Trim(),tbxDeliver.Text.Trim());

                this.Invoke((MethodInvoker)delegate()
                {
                    var report = new YJH_HQ.UI.Reports.RP_DeliverExpress();
                    if (store != "")
                        report.ReportParameters["StoreName"].Value = this.ddtStore.SelectedItem.Text + "派件报表";
                    else
                        report.ReportParameters["StoreName"].Value = "派件报表";
                    report.ReportParameters["PrintDate"].Value = DateTime.Now.ToString("yyyy-MM-dd");
                    if (start == DateTime.MinValue)
                    {
                        report.ReportParameters["StartDate"].Value = "---";
                        report.ReportParameters["EndDate"].Value = "---";
                    }
                    else
                    {
                        report.ReportParameters["StartDate"].Value = start.ToString("yyyy-MM-dd");
                        report.ReportParameters["EndDate"].Value = end.ToString("yyyy-MM-dd");
                    }
                    if (st == DateTime.MinValue)
                    {
                        report.ReportParameters["st"].Value = "---";
                        report.ReportParameters["et"].Value = "---";
                    }
                    else
                    {
                        report.ReportParameters["st"].Value = st.ToString("yyyy-MM-dd");
                        report.ReportParameters["et"].Value = et.ToString("yyyy-MM-dd");
                    }
                    report.DeliverExpressSource.DataSource = result;
                    var reportSource = new Telerik.Reporting.InstanceReportSource();
                    reportSource.ReportDocument = report;
                    this.rpvExpress.ReportSource = reportSource;
                    this.rpvExpress.RefreshReport();
                });
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                });
            }

            // });
        }
    }
}
