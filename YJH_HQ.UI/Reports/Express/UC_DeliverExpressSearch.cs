﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YJH.Services;
using Telerik.WinControls;
using YJH_HQ.UI.Services;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

namespace YJH_HQ.UI.Reports.Express
{
    public partial class UC_DeliverExpressSearch : UserControl
    {
        DateTime st;
        DateTime et;
        DateTime start;
        DateTime end;
        string store = "";
        public UC_DeliverExpressSearch()
        {
            InitializeComponent();
            this.btSearch.Click += (s, e) => { this.Search(); };
            this.cbxDeliverStock.CheckStateChanged += cbxDeliverStock_CheckStateChanged;
            this.cbxAgentDeliver.CheckStateChanged += cbxAgentDeliver_CheckStateChanged;
            this.Load += UC_DeliverExpressSearch_Load;
            btnExcel.Click += btnExcel_Click;
        }

        void btnExcel_Click(object sender, EventArgs e)
        {
            if (dgDeliverExpress.Rows.Count == 0)
                return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl   files   (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "导出Excel文件到";

            saveFileDialog.FileName = DateTime.Now.ToString("yyyy-MM-dd") + "导出派件明细";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream myStream;
                myStream = saveFileDialog.OpenFile();
                StreamWriter sw = new StreamWriter(myStream, System.Text.Encoding.GetEncoding("gb2312"));
                string str = "";
                try
                {
                    //写标题     
                    for (int i = 0; i < dgDeliverExpress.ColumnCount; i++)
                    {
                        if (i > 0)
                        {
                            str += "\t";
                        }
                        str += dgDeliverExpress.Columns[i].HeaderText;
                    }

                    sw.WriteLine(str);
                    //写内容   
                    for (int j = 0; j < dgDeliverExpress.Rows.Count; j++)
                    {
                        string tempStr = "";
                        for (int k = 0; k < dgDeliverExpress.Columns.Count; k++)
                        {
                            if (k > 0)
                            {
                                tempStr += "\t";
                            }
                            if (dgDeliverExpress.Rows[j].Cells[k].Value == null)
                            {
                                if (k == 6)
                                {
                                    switch (dgDeliverExpress.Rows[j].Cells[9].Value.ToString())
                                    {
                                        case "10":
                                            tempStr += "未派件";
                                            break;
                                        case "20":
                                            tempStr += "已派件";
                                            break;
                                        case "30":
                                            tempStr += "派件异常";
                                            break;
                                    }
                                }
                                else
                                    tempStr += "";
                            }
                            else
                                tempStr += dgDeliverExpress.Rows[j].Cells[k].Value.ToString().Replace("\"","");
                        }
                        sw.WriteLine(tempStr);
                    }
                    sw.Close();
                    myStream.Close();
                    MessageBox.Show("导出成功", "提示");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    sw.Close();
                    myStream.Close();
                }
            }
        }

        void UC_DeliverExpressSearch_Load(object sender, EventArgs e)
        {
            this.dtStockStartDate.Enabled = true;
            this.dtStockEndDate.Enabled = true;
            this.dtAgentStartDate.Enabled = false;
            this.dtAgentEndDate.Enabled = false;
            cbxDeliverStock.Checked = true;
            cbxAgentDeliver.Checked = false;

            this.dtStockStartDate.Value = OrgUnitService.GetDate();
            this.dtStockEndDate.Value = OrgUnitService.GetDate();
            this.dtAgentStartDate.Value = OrgUnitService.GetDate();
            this.dtAgentEndDate.Value = OrgUnitService.GetDate();

            st = this.dtStockStartDate.Value.Date;
            et = this.dtStockEndDate.Value.Date;
            start = DateTime.MinValue;
            end = DateTime.MinValue;
            ddlExpressCompany.ValueMember = "ems_id";
            ddlExpressCompany.DisplayMember = "ems_name";
            var list = YJH.Services.ems_deliverExpress_hqService.GetExpresscompany();
            ddlExpressCompany.DataSource = list;
            ddlExpressCompany.SelectedIndex = -1;
            var table = YJH.Services.LotteryBalanceService.GetStores();
            DataRow newRow = table.NewRow();
            newRow["store_id"] = "";
            newRow["store_name"] = "全部";
            table.Rows.InsertAt(newRow, 0);
            this.ddtStore.DisplayMember = "store_name";
            this.ddtStore.ValueMember = "store_id";
            this.ddtStore.DataSource = table;
            this.ddtStore.SelectedIndex = 0;
        }

        void cbxAgentDeliver_CheckStateChanged(object sender, EventArgs e)
        {
            if (cbxAgentDeliver.Checked == true)
            {
                this.dtAgentStartDate.Enabled = true;
                this.dtAgentEndDate.Enabled = true;
            }
            else
            {
                this.dtAgentStartDate.Enabled = false;
                this.dtAgentEndDate.Enabled = false;
            }
        }

        void cbxDeliverStock_CheckStateChanged(object sender, EventArgs e)
        {
            if (cbxDeliverStock.Checked == true)
            {
                this.dtStockStartDate.Enabled = true;
                this.dtStockEndDate.Enabled = true;
            }
            else
            {
                this.dtStockStartDate.Enabled = false;
                this.dtStockEndDate.Enabled = false;
            }
        }

        private void Search()
        {
            if (cbxDeliverStock.Checked)
            {
                st = this.dtStockStartDate.Value.Date;
                et = this.dtStockEndDate.Value.AddDays(1).Date;
            }
            else
            {
                st = DateTime.Parse("1753/1/1 12:00:00");
                et =DateTime.Parse("9999/12/30 11:59:59");
            }
            if (cbxAgentDeliver.Checked)
            {
                start = this.dtAgentStartDate.Value.Date;
                end = this.dtAgentEndDate.Value.AddDays(1).Date;
            }
            else
            {
                start = DateTime.Parse("1753/1/1 12:00:00");
                end = DateTime.Parse("9999/12/30 11:59:59");
            }
            if (st > et)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            if (start > end)
            {
                RadMessageBox.Show("查询起始时间不得小于结束时间", "提示");
                return;
            }
            
            if (ddtStore.Text != "全部")
                store = this.ddtStore.SelectedValue.ToString();
            string ems = "";
            if(ddlExpressCompany.SelectedValue != null)
                 ems = ddlExpressCompany.SelectedValue.ToString();
            try
            {
                DataTable dt = new DataTable();
                if(cbxAgentDeliver.Checked == true)
                 dt = SqlHelper.TableProcedure("[dbo].[DeliverExpressSearch]", new SqlParameter("@DateFrom1", st), new SqlParameter("@DateTo1", et), new SqlParameter("@DateFrom2", start),
                     new SqlParameter("@DateTo2", end), new SqlParameter("@StoreID", store), new SqlParameter("@Deliverier", tbxDeliver.Text.Trim()), new SqlParameter("@ems", ems));
                else
                    dt = SqlHelper.TableProcedure("[dbo].[DeliverExpressQuery]", new SqlParameter("@DateFrom1", st), new SqlParameter("@DateTo1", et), 
                     new SqlParameter("@StoreID", store), new SqlParameter("@Deliverier", tbxDeliver.Text.Trim()), new SqlParameter("@ems", ems));
                //dgDeliverExpress.DataSource = YJH.Services.ems_deliverExpress_hqService.Search(st, et, start, end, store, this.ddlExpressCompany.Text.Trim(), tbxDeliver.Text.Trim());
                //dt.Columns.Add("State", typeof(string));
                //foreach (DataRow dr in dt.Rows)
                //{
                //    if (dr["State1"].ToString() == "10")
                //        dr["State"] = "未派件";
                //    else if(dr["State1"].ToString() == "20")
                //        dr["State"] = "未派件";
                //    else if (dr["State1"].ToString() == "30")
                //        dr["State"] = "派件异常";
                //    dgDeliverExpress.DataSource = dt;
                //}
                dgDeliverExpress.DataSource = dt;
            }
            catch (Exception ex)
            {
                this.Invoke((MethodInvoker)delegate()
                {
                    RadMessageBox.Show(this, "查询出现异常：\r\n" + ex.Message, "查询错误", MessageBoxButtons.OK, RadMessageIcon.Error);
                });
            }
        }
        
    }
}
