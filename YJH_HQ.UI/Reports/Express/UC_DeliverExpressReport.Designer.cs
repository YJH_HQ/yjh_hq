﻿namespace YJH_HQ.UI.Reports
{
    partial class UC_DeliverExpressReport
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddlExpressCompany = new Telerik.WinControls.UI.RadDropDownList();
            this.cbxAgentDeliver = new Telerik.WinControls.UI.RadCheckBox();
            this.cbxDeliverStock = new Telerik.WinControls.UI.RadCheckBox();
            this.dtStockEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.dtStockStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddtStore = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.dtAgentEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.dtAgentStartDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.rpvExpress = new Telerik.ReportViewer.WinForms.ReportViewer();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tbxDeliver = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlExpressCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxAgentDeliver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDeliverStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStockEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStockStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtAgentEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtAgentStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxDeliver)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.tbxDeliver);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.ddlExpressCompany);
            this.radGroupBox1.Controls.Add(this.cbxAgentDeliver);
            this.radGroupBox1.Controls.Add(this.cbxDeliverStock);
            this.radGroupBox1.Controls.Add(this.dtStockEndDate);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.dtStockStartDate);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.ddtStore);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.dtAgentEndDate);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Controls.Add(this.dtAgentStartDate);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(943, 95);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "查询条件";
            // 
            // ddlExpressCompany
            // 
            this.ddlExpressCompany.Location = new System.Drawing.Point(316, 59);
            this.ddlExpressCompany.Name = "ddlExpressCompany";
            this.ddlExpressCompany.Size = new System.Drawing.Size(120, 20);
            this.ddlExpressCompany.TabIndex = 46;
            // 
            // cbxAgentDeliver
            // 
            this.cbxAgentDeliver.Location = new System.Drawing.Point(417, 24);
            this.cbxAgentDeliver.Name = "cbxAgentDeliver";
            this.cbxAgentDeliver.Size = new System.Drawing.Size(91, 18);
            this.cbxAgentDeliver.TabIndex = 45;
            this.cbxAgentDeliver.Text = "派件日期段：";
            // 
            // cbxDeliverStock
            // 
            this.cbxDeliverStock.Location = new System.Drawing.Point(22, 24);
            this.cbxDeliverStock.Name = "cbxDeliverStock";
            this.cbxDeliverStock.Size = new System.Drawing.Size(91, 18);
            this.cbxDeliverStock.TabIndex = 44;
            this.cbxDeliverStock.Text = "收件日期段：";
            // 
            // dtStockEndDate
            // 
            this.dtStockEndDate.AutoSize = false;
            this.dtStockEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtStockEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStockEndDate.Location = new System.Drawing.Point(267, 24);
            this.dtStockEndDate.Name = "dtStockEndDate";
            this.dtStockEndDate.Size = new System.Drawing.Size(120, 20);
            this.dtStockEndDate.TabIndex = 43;
            this.dtStockEndDate.TabStop = false;
            this.dtStockEndDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(243, 24);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(18, 18);
            this.radLabel5.TabIndex = 42;
            this.radLabel5.Text = "至";
            // 
            // dtStockStartDate
            // 
            this.dtStockStartDate.AutoSize = false;
            this.dtStockStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtStockStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStockStartDate.Location = new System.Drawing.Point(119, 24);
            this.dtStockStartDate.Name = "dtStockStartDate";
            this.dtStockStartDate.Size = new System.Drawing.Size(120, 20);
            this.dtStockStartDate.TabIndex = 41;
            this.dtStockStartDate.TabStop = false;
            this.dtStockStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(245, 61);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 18);
            this.radLabel4.TabIndex = 38;
            this.radLabel4.Text = "快递公司：";
            // 
            // ddtStore
            // 
            this.ddtStore.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddtStore.Location = new System.Drawing.Point(119, 59);
            this.ddtStore.Name = "ddtStore";
            this.ddtStore.Size = new System.Drawing.Size(120, 20);
            this.ddtStore.TabIndex = 37;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(71, 61);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 18);
            this.radLabel3.TabIndex = 36;
            this.radLabel3.Text = "门店：";
            // 
            // dtAgentEndDate
            // 
            this.dtAgentEndDate.AutoSize = false;
            this.dtAgentEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtAgentEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAgentEndDate.Location = new System.Drawing.Point(662, 23);
            this.dtAgentEndDate.Name = "dtAgentEndDate";
            this.dtAgentEndDate.Size = new System.Drawing.Size(120, 20);
            this.dtAgentEndDate.TabIndex = 35;
            this.dtAgentEndDate.TabStop = false;
            this.dtAgentEndDate.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(638, 23);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(18, 18);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "至";
            // 
            // btSearch
            // 
            this.btSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btSearch.Location = new System.Drawing.Point(838, 23);
            this.btSearch.Name = "btSearch";
            this.btSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btSearch.Size = new System.Drawing.Size(86, 20);
            this.btSearch.TabIndex = 33;
            this.btSearch.Text = "查询";
            // 
            // dtAgentStartDate
            // 
            this.dtAgentStartDate.AutoSize = false;
            this.dtAgentStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtAgentStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAgentStartDate.Location = new System.Drawing.Point(514, 23);
            this.dtAgentStartDate.Name = "dtAgentStartDate";
            this.dtAgentStartDate.Size = new System.Drawing.Size(120, 20);
            this.dtAgentStartDate.TabIndex = 32;
            this.dtAgentStartDate.TabStop = false;
            this.dtAgentStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // rpvExpress
            // 
            this.rpvExpress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpvExpress.Location = new System.Drawing.Point(0, 95);
            this.rpvExpress.Name = "rpvExpress";
            this.rpvExpress.Size = new System.Drawing.Size(943, 443);
            this.rpvExpress.TabIndex = 1;
            this.rpvExpress.ViewMode = Telerik.ReportViewer.WinForms.ViewMode.PrintPreview;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(454, 61);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(54, 18);
            this.radLabel1.TabIndex = 47;
            this.radLabel1.Text = "派件员：";
            // 
            // tbxDeliver
            // 
            this.tbxDeliver.Location = new System.Drawing.Point(514, 59);
            this.tbxDeliver.Name = "tbxDeliver";
            this.tbxDeliver.Size = new System.Drawing.Size(120, 20);
            this.tbxDeliver.TabIndex = 48;
            // 
            // UC_DeliverExpressReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.rpvExpress);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_DeliverExpressReport";
            this.Size = new System.Drawing.Size(943, 538);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlExpressCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxAgentDeliver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDeliverStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStockEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStockStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddtStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtAgentEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtAgentStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbxDeliver)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDateTimePicker dtAgentStartDate;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.ReportViewer.WinForms.ReportViewer rpvExpress;
        private Telerik.WinControls.UI.RadDateTimePicker dtAgentEndDate;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddtStore;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadCheckBox cbxAgentDeliver;
        private Telerik.WinControls.UI.RadCheckBox cbxDeliverStock;
        private Telerik.WinControls.UI.RadDateTimePicker dtStockEndDate;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDateTimePicker dtStockStartDate;
        private Telerik.WinControls.UI.RadDropDownList ddlExpressCompany;
        private Telerik.WinControls.UI.RadTextBox tbxDeliver;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}

