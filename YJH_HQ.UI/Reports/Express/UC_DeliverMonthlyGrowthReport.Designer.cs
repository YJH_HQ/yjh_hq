﻿namespace YJH_HQ.UI.Reports.Express
{
    partial class UC_DeliverMonthlyGrowthReport
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddlMonth = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlYear = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnExcel = new Telerik.WinControls.UI.RadButton();
            this.btSearch = new Telerik.WinControls.UI.RadButton();
            this.dgDeliverExpress = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliverExpress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliverExpress.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddlMonth);
            this.radGroupBox1.Controls.Add(this.ddlYear);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.btnExcel);
            this.radGroupBox1.Controls.Add(this.btSearch);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "查询条件";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(945, 72);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "查询条件";
            // 
            // ddlMonth
            // 
            this.ddlMonth.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "1";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "2";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "3";
            radListDataItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "4";
            radListDataItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "5";
            radListDataItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "6";
            radListDataItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "7";
            radListDataItem7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "8";
            radListDataItem8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "9";
            radListDataItem9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "10";
            radListDataItem10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "11";
            radListDataItem11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "12";
            radListDataItem12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem12.TextWrap = true;
            this.ddlMonth.Items.Add(radListDataItem1);
            this.ddlMonth.Items.Add(radListDataItem2);
            this.ddlMonth.Items.Add(radListDataItem3);
            this.ddlMonth.Items.Add(radListDataItem4);
            this.ddlMonth.Items.Add(radListDataItem5);
            this.ddlMonth.Items.Add(radListDataItem6);
            this.ddlMonth.Items.Add(radListDataItem7);
            this.ddlMonth.Items.Add(radListDataItem8);
            this.ddlMonth.Items.Add(radListDataItem9);
            this.ddlMonth.Items.Add(radListDataItem10);
            this.ddlMonth.Items.Add(radListDataItem11);
            this.ddlMonth.Items.Add(radListDataItem12);
            this.ddlMonth.Location = new System.Drawing.Point(161, 30);
            this.ddlMonth.Name = "ddlMonth";
            this.ddlMonth.Size = new System.Drawing.Size(55, 20);
            this.ddlMonth.TabIndex = 68;
            // 
            // ddlYear
            // 
            this.ddlYear.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlYear.Location = new System.Drawing.Point(56, 30);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(73, 20);
            this.ddlYear.TabIndex = 67;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(135, 32);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(30, 18);
            this.radLabel2.TabIndex = 66;
            this.radLabel2.Text = "月：";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(20, 32);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(30, 18);
            this.radLabel1.TabIndex = 65;
            this.radLabel1.Text = "年：";
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Image = global::YJH_HQ.UI.Properties.Resources.CancleButtonImage;
            this.btnExcel.Location = new System.Drawing.Point(829, 30);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnExcel.Size = new System.Drawing.Size(86, 20);
            this.btnExcel.TabIndex = 64;
            this.btnExcel.Text = "导出";
            // 
            // btSearch
            // 
            this.btSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSearch.Image = global::YJH_HQ.UI.Properties.Resources.Search16;
            this.btSearch.Location = new System.Drawing.Point(737, 30);
            this.btSearch.Name = "btSearch";
            this.btSearch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btSearch.Size = new System.Drawing.Size(86, 20);
            this.btSearch.TabIndex = 50;
            this.btSearch.Text = "查询";
            // 
            // dgDeliverExpress
            // 
            this.dgDeliverExpress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDeliverExpress.Location = new System.Drawing.Point(0, 72);
            // 
            // dgDeliverExpress
            // 
            this.dgDeliverExpress.MasterTemplate.AllowAddNewRow = false;
            this.dgDeliverExpress.MasterTemplate.ShowFilteringRow = false;
            this.dgDeliverExpress.Name = "dgDeliverExpress";
            this.dgDeliverExpress.ReadOnly = true;
            this.dgDeliverExpress.ShowGroupPanel = false;
            this.dgDeliverExpress.Size = new System.Drawing.Size(945, 560);
            this.dgDeliverExpress.TabIndex = 9;
            // 
            // UC_DeliverMonthlyGrowthReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgDeliverExpress);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "UC_DeliverMonthlyGrowthReport";
            this.Size = new System.Drawing.Size(945, 632);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliverExpress.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliverExpress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton btSearch;
        private Telerik.WinControls.UI.RadGridView dgDeliverExpress;
        private Telerik.WinControls.UI.RadButton btnExcel;
        private Telerik.WinControls.UI.RadDropDownList ddlMonth;
        private Telerik.WinControls.UI.RadDropDownList ddlYear;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}

