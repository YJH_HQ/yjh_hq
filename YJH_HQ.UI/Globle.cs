﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YJH_HQ.UI
{
    public class Globle
    {
        public const string BUSINESS_UNIT = "sys.BusinessUnit";
        public const string WORK_GROUP = "sys.WorkGroup";
        public const string EMPLOEE = "sys.Emploee";
        public const string NULL_TREE_NODE = "NULL_NODE";

        /// <summary>
        /// 用户登录的组织机构类型
        /// </summary>
        public static sys.Enums.OrganizationType CurrentOrganizationType { get; private set; }

        /// <summary>
        /// 用户归属组织机构（OrgUnit中的BaseType指向的是Employee）
        /// </summary>
        public static Dictionary<string, sys.Entities.OrgUnit> AvailableOrganizations { get; private set; }

        /// <summary>
        /// 当前公司OU
        /// </summary>
        public static sys.Entities.OrgUnit CurrentBusinessUnitOU { get; private set; }
        /// <summary>
        /// 当前公司
        /// </summary>
        public static sys.Entities.BusinessUnit CurrentBusinessUnit { get; private set; }

        /// <summary>
        /// 当前部门OU
        /// </summary>
        public static sys.Entities.OrgUnit CurrentWorkGroupOU { get; private set; }
        /// <summary>
        /// 当前部门
        /// </summary>
        public static sys.Entities.WorkGroup CurrentWorkGroup { get; private set; }

        /// <summary>
        /// 当前员工OU
        /// </summary>
        public static sys.Entities.OrgUnit CurrentEmployeeOU { get; private set; }
        /// <summary>
        /// 当前员工
        /// </summary>
        public static sys.Entities.Emploee CurrentEmployee { get; private set; }

        public Globle(sys.Enums.OrganizationType orgType, List<sys.Entities.OrgUnit> availableOrgs)
        {
            if (availableOrgs == null || availableOrgs.Count <= 0)
                throw new Exception("用户归属组织机构不能为空，并且个数要大于0");

            CurrentOrganizationType = orgType;
            AvailableOrganizations = new Dictionary<string, sys.Entities.OrgUnit>();

            //availableOrgs保存的是该用户对应多个OU的关系，这里要把该ou对应关系的组织名转换出来
            for (int i = 0; i < availableOrgs.Count; i++)
                LoopParent(availableOrgs[i], true);
        }

        /// <summary>
        /// 遍历父节点，并绑定公司、部门
        /// </summary>
        /// <param name="ou">要遍历的用户OU</param>
        /// <param name="isInit">是否是初始化，注意：只有该类的构造函数方法里才使用true，其它地方禁止使用true</param>
        private static void LoopParent(sys.Entities.OrgUnit ou, bool isInit = false)
        {
            //当前登录的用户OU
            sys.Entities.OrgUnit currentUserOU = new sys.Entities.OrgUnit(dps.Client.dpsClient.Default.CurrentOrgUnit);

            //这里要遍历，直到遍历到该ou的直属公司位置，并且记录下来
            sys.Entities.OrgUnit ouUnit = ou;
            StringBuilder sb = new StringBuilder();
            //标示部门是否已绑定
            bool isBindWG = false;
            do
            {
                if (ouUnit == null)
                    break;
                if (ouUnit.BaseType == "sys.BusinessUnit")
                {
                    sys.Entities.BusinessUnit unit = (sys.Entities.BusinessUnit)ouUnit.Base;
                    sb.Insert(0, unit.Name + " | ");

                    //绑定当前公司，只有当前ou是当前登录的用户OU才可绑定
                    if (currentUserOU.ID == ou.ID)
                    {
                        CurrentBusinessUnitOU = ouUnit;
                        CurrentBusinessUnit = unit;
                    }
                    break;
                }

                if (ouUnit.BaseType == "sys.WorkGroup")
                {
                    sys.Entities.WorkGroup group = (sys.Entities.WorkGroup)ouUnit.Base;
                    sb.Insert(0, group.Name + " | ");

                    //这里部门会出现多次，当前部门只获取最后一部门，只有当前ou是当前登录的用户OU才可绑定
                    if (currentUserOU.ID == ou.ID && !isBindWG)
                    {
                        CurrentWorkGroupOU = ouUnit;
                        CurrentWorkGroup = group;
                        isBindWG = true;
                    }
                }
                ouUnit = ouUnit.Parent;
            } while (true);

            if (isInit)
                AvailableOrganizations.Add(sb.ToString(), ou);

             //绑定当前与员工
            CurrentEmployeeOU = currentUserOU;
            CurrentEmployee = (sys.Entities.Emploee)currentUserOU.Base;
        }

        /// <summary>
        /// 切换登录的组织机构
        /// </summary>
        /// <param name="ou"></param>
        public static void ChangeCurrentOrgUnit(sys.Entities.OrgUnit ou)
        {
            dps.Client.dpsClient.Default.ChangeCurrentOrgUnit(ou.Instance);
            LoopParent(ou);
        }
    }
}
