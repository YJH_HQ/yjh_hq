﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_schedule_st : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_schedule_st";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Int32 year
        {
            get { return Instance["year"].IntegerValue; }
            set { Instance["year"].IntegerValue = value; }
        }

        public Int32 month
        {
            get { return Instance["month"].IntegerValue; }
            set { Instance["month"].IntegerValue = value; }
        }

        public String emp_id
        {
            get { return Instance["emp_id"].StringValue; }
            set { Instance["emp_id"].StringValue = value; }
        }

        public String start_time
        {
            get { return Instance["start_time"].StringValue; }
            set { Instance["start_time"].StringValue = value; }
        }

        public String end_time
        {
            get { return Instance["end_time"].StringValue; }
            set { Instance["end_time"].StringValue = value; }
        }

        public String day1_am
        {
            get { return Instance["day1_am"].StringValue; }
            set { Instance["day1_am"].StringValue = value; }
        }

        public String day1_pm
        {
            get { return Instance["day1_pm"].StringValue; }
            set { Instance["day1_pm"].StringValue = value; }
        }

        public String day2_am
        {
            get { return Instance["day2_am"].StringValue; }
            set { Instance["day2_am"].StringValue = value; }
        }

        public String day2_pm
        {
            get { return Instance["day2_pm"].StringValue; }
            set { Instance["day2_pm"].StringValue = value; }
        }

        public String day3_am
        {
            get { return Instance["day3_am"].StringValue; }
            set { Instance["day3_am"].StringValue = value; }
        }

        public String day3_pm
        {
            get { return Instance["day3_pm"].StringValue; }
            set { Instance["day3_pm"].StringValue = value; }
        }

        public String day4_am
        {
            get { return Instance["day4_am"].StringValue; }
            set { Instance["day4_am"].StringValue = value; }
        }

        public String day4_pm
        {
            get { return Instance["day4_pm"].StringValue; }
            set { Instance["day4_pm"].StringValue = value; }
        }

        public String day5_am
        {
            get { return Instance["day5_am"].StringValue; }
            set { Instance["day5_am"].StringValue = value; }
        }

        public String day5_pm
        {
            get { return Instance["day5_pm"].StringValue; }
            set { Instance["day5_pm"].StringValue = value; }
        }

        public String day6_am
        {
            get { return Instance["day6_am"].StringValue; }
            set { Instance["day6_am"].StringValue = value; }
        }

        public String day6_pm
        {
            get { return Instance["day6_pm"].StringValue; }
            set { Instance["day6_pm"].StringValue = value; }
        }

        public String day7_am
        {
            get { return Instance["day7_am"].StringValue; }
            set { Instance["day7_am"].StringValue = value; }
        }

        public String day7_pm
        {
            get { return Instance["day7_pm"].StringValue; }
            set { Instance["day7_pm"].StringValue = value; }
        }

        public String day8_am
        {
            get { return Instance["day8_am"].StringValue; }
            set { Instance["day8_am"].StringValue = value; }
        }

        public String day8_pm
        {
            get { return Instance["day8_pm"].StringValue; }
            set { Instance["day8_pm"].StringValue = value; }
        }

        public String day9_am
        {
            get { return Instance["day9_am"].StringValue; }
            set { Instance["day9_am"].StringValue = value; }
        }

        public String day9_pm
        {
            get { return Instance["day9_pm"].StringValue; }
            set { Instance["day9_pm"].StringValue = value; }
        }

        public String day10_am
        {
            get { return Instance["day10_am"].StringValue; }
            set { Instance["day10_am"].StringValue = value; }
        }

        public String day10_pm
        {
            get { return Instance["day10_pm"].StringValue; }
            set { Instance["day10_pm"].StringValue = value; }
        }

        public String day11_am
        {
            get { return Instance["day11_am"].StringValue; }
            set { Instance["day11_am"].StringValue = value; }
        }

        public String day11_pm
        {
            get { return Instance["day11_pm"].StringValue; }
            set { Instance["day11_pm"].StringValue = value; }
        }

        public String day12_am
        {
            get { return Instance["day12_am"].StringValue; }
            set { Instance["day12_am"].StringValue = value; }
        }

        public String day12_pm
        {
            get { return Instance["day12_pm"].StringValue; }
            set { Instance["day12_pm"].StringValue = value; }
        }

        public String day13_am
        {
            get { return Instance["day13_am"].StringValue; }
            set { Instance["day13_am"].StringValue = value; }
        }

        public String day13_pm
        {
            get { return Instance["day13_pm"].StringValue; }
            set { Instance["day13_pm"].StringValue = value; }
        }

        public String day14_am
        {
            get { return Instance["day14_am"].StringValue; }
            set { Instance["day14_am"].StringValue = value; }
        }

        public String day14_pm
        {
            get { return Instance["day14_pm"].StringValue; }
            set { Instance["day14_pm"].StringValue = value; }
        }

        public String day15_am
        {
            get { return Instance["day15_am"].StringValue; }
            set { Instance["day15_am"].StringValue = value; }
        }

        public String day15_pm
        {
            get { return Instance["day15_pm"].StringValue; }
            set { Instance["day15_pm"].StringValue = value; }
        }

        public String day16_am
        {
            get { return Instance["day16_am"].StringValue; }
            set { Instance["day16_am"].StringValue = value; }
        }

        public String day16_pm
        {
            get { return Instance["day16_pm"].StringValue; }
            set { Instance["day16_pm"].StringValue = value; }
        }

        public String day17_am
        {
            get { return Instance["day17_am"].StringValue; }
            set { Instance["day17_am"].StringValue = value; }
        }

        public String day17_pm
        {
            get { return Instance["day17_pm"].StringValue; }
            set { Instance["day17_pm"].StringValue = value; }
        }

        public String day18_am
        {
            get { return Instance["day18_am"].StringValue; }
            set { Instance["day18_am"].StringValue = value; }
        }

        public String day18_pm
        {
            get { return Instance["day18_pm"].StringValue; }
            set { Instance["day18_pm"].StringValue = value; }
        }

        public String day19_am
        {
            get { return Instance["day19_am"].StringValue; }
            set { Instance["day19_am"].StringValue = value; }
        }

        public String day19_pm
        {
            get { return Instance["day19_pm"].StringValue; }
            set { Instance["day19_pm"].StringValue = value; }
        }

        public String day20_am
        {
            get { return Instance["day20_am"].StringValue; }
            set { Instance["day20_am"].StringValue = value; }
        }

        public String day20_pm
        {
            get { return Instance["day20_pm"].StringValue; }
            set { Instance["day20_pm"].StringValue = value; }
        }

        public String day21_am
        {
            get { return Instance["day21_am"].StringValue; }
            set { Instance["day21_am"].StringValue = value; }
        }

        public String day21_pm
        {
            get { return Instance["day21_pm"].StringValue; }
            set { Instance["day21_pm"].StringValue = value; }
        }

        public String day22_am
        {
            get { return Instance["day22_am"].StringValue; }
            set { Instance["day22_am"].StringValue = value; }
        }

        public String day22_pm
        {
            get { return Instance["day22_pm"].StringValue; }
            set { Instance["day22_pm"].StringValue = value; }
        }

        public String day23_am
        {
            get { return Instance["day23_am"].StringValue; }
            set { Instance["day23_am"].StringValue = value; }
        }

        public String day23_pm
        {
            get { return Instance["day23_pm"].StringValue; }
            set { Instance["day23_pm"].StringValue = value; }
        }

        public String day24_am
        {
            get { return Instance["day24_am"].StringValue; }
            set { Instance["day24_am"].StringValue = value; }
        }

        public String day24_pm
        {
            get { return Instance["day24_pm"].StringValue; }
            set { Instance["day24_pm"].StringValue = value; }
        }

        public String day25_am
        {
            get { return Instance["day25_am"].StringValue; }
            set { Instance["day25_am"].StringValue = value; }
        }

        public String day25_pm
        {
            get { return Instance["day25_pm"].StringValue; }
            set { Instance["day25_pm"].StringValue = value; }
        }

        public String day26_am
        {
            get { return Instance["day26_am"].StringValue; }
            set { Instance["day26_am"].StringValue = value; }
        }

        public String day26_pm
        {
            get { return Instance["day26_pm"].StringValue; }
            set { Instance["day26_pm"].StringValue = value; }
        }

        public String day27_am
        {
            get { return Instance["day27_am"].StringValue; }
            set { Instance["day27_am"].StringValue = value; }
        }

        public String day27_pm
        {
            get { return Instance["day27_pm"].StringValue; }
            set { Instance["day27_pm"].StringValue = value; }
        }

        public String day28_am
        {
            get { return Instance["day28_am"].StringValue; }
            set { Instance["day28_am"].StringValue = value; }
        }

        public String day28_pm
        {
            get { return Instance["day28_pm"].StringValue; }
            set { Instance["day28_pm"].StringValue = value; }
        }

        public String day29_am
        {
            get { return Instance["day29_am"].StringValue; }
            set { Instance["day29_am"].StringValue = value; }
        }

        public String day29_pm
        {
            get { return Instance["day29_pm"].StringValue; }
            set { Instance["day29_pm"].StringValue = value; }
        }

        public String day30_am
        {
            get { return Instance["day30_am"].StringValue; }
            set { Instance["day30_am"].StringValue = value; }
        }

        public String day30_pm
        {
            get { return Instance["day30_pm"].StringValue; }
            set { Instance["day30_pm"].StringValue = value; }
        }

        public String day31_am
        {
            get { return Instance["day31_am"].StringValue; }
            set { Instance["day31_am"].StringValue = value; }
        }

        public String day31_pm
        {
            get { return Instance["day31_pm"].StringValue; }
            set { Instance["day31_pm"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_schedule_st() : base() { }

        public hr_schedule_st(Entity instance) : base(instance) { }
        #endregion

    }
}
