﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_contract_rent : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_contract_rent";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Int32 house_property
        {
            get { return Instance["house_property"].IntegerValue; }
            set { Instance["house_property"].IntegerValue = value; }
        }

        public Int32 landlord_type
        {
            get { return Instance["landlord_type"].IntegerValue; }
            set { Instance["landlord_type"].IntegerValue = value; }
        }

        public String rent_contract_id
        {
            get { return Instance["rent_contract_id"].StringValue; }
            set { Instance["rent_contract_id"].StringValue = value; }
        }

        public Decimal rent_month_amt_tax
        {
            get { return Instance["rent_month_amt_tax"].DecimalValue; }
            set { Instance["rent_month_amt_tax"].DecimalValue = value; }
        }

        public Int32 is_tax
        {
            get { return Instance["is_tax"].IntegerValue; }
            set { Instance["is_tax"].IntegerValue = value; }
        }

        public Int32 rent_audit
        {
            get { return Instance["rent_audit"].IntegerValue; }
            set { Instance["rent_audit"].IntegerValue = value; }
        }

        public Decimal lease_period
        {
            get { return Instance["lease_period"].DecimalValue; }
            set { Instance["lease_period"].DecimalValue = value; }
        }

        public DateTime contract_startdate
        {
            get { return Instance["contract_startdate"].DateValue; }
            set { Instance["contract_startdate"].DateValue = value; }
        }

        public DateTime contract_enddate
        {
            get { return Instance["contract_enddate"].DateValue; }
            set { Instance["contract_enddate"].DateValue = value; }
        }

        public Int32 lease_audit
        {
            get { return Instance["lease_audit"].IntegerValue; }
            set { Instance["lease_audit"].IntegerValue = value; }
        }

        public Decimal rent_period
        {
            get { return Instance["rent_period"].DecimalValue; }
            set { Instance["rent_period"].DecimalValue = value; }
        }

        public DateTime contract_date
        {
            get { return Instance["contract_date"].DateValue; }
            set { Instance["contract_date"].DateValue = value; }
        }

        public Int32 rent_payment
        {
            get { return Instance["rent_payment"].IntegerValue; }
            set { Instance["rent_payment"].IntegerValue = value; }
        }

        public Decimal deposit_amt
        {
            get { return Instance["deposit_amt"].DecimalValue; }
            set { Instance["deposit_amt"].DecimalValue = value; }
        }

        public Int32 deposit_audit
        {
            get { return Instance["deposit_audit"].IntegerValue; }
            set { Instance["deposit_audit"].IntegerValue = value; }
        }

        public Decimal rent_free_period
        {
            get { return Instance["rent_free_period"].DecimalValue; }
            set { Instance["rent_free_period"].DecimalValue = value; }
        }

        public Int32 is_increase
        {
            get { return Instance["is_increase"].IntegerValue; }
            set { Instance["is_increase"].IntegerValue = value; }
        }

        public String increase_type
        {
            get { return Instance["increase_type"].StringValue; }
            set { Instance["increase_type"].StringValue = value; }
        }

        public Decimal transfer_amt
        {
            get { return Instance["transfer_amt"].DecimalValue; }
            set { Instance["transfer_amt"].DecimalValue = value; }
        }

        public String transfer_reason
        {
            get { return Instance["transfer_reason"].StringValue; }
            set { Instance["transfer_reason"].StringValue = value; }
        }

        public Int32 transfer_audit
        {
            get { return Instance["transfer_audit"].IntegerValue; }
            set { Instance["transfer_audit"].IntegerValue = value; }
        }

        public Decimal property_amt
        {
            get { return Instance["property_amt"].DecimalValue; }
            set { Instance["property_amt"].DecimalValue = value; }
        }

        public Decimal agency_amt
        {
            get { return Instance["agency_amt"].DecimalValue; }
            set { Instance["agency_amt"].DecimalValue = value; }
        }

        public Decimal other_amt
        {
            get { return Instance["other_amt"].DecimalValue; }
            set { Instance["other_amt"].DecimalValue = value; }
        }

        public Int32 is_doc_ok
        {
            get { return Instance["is_doc_ok"].IntegerValue; }
            set { Instance["is_doc_ok"].IntegerValue = value; }
        }

        public String doc_note
        {
            get { return Instance["doc_note"].StringValue; }
            set { Instance["doc_note"].StringValue = value; }
        }

        public Int32 rent_pay_status
        {
            get { return Instance["rent_pay_status"].IntegerValue; }
            set { Instance["rent_pay_status"].IntegerValue = value; }
        }

        public Decimal rent_pay
        {
            get { return Instance["rent_pay"].DecimalValue; }
            set { Instance["rent_pay"].DecimalValue = value; }
        }

        public Int32 is_bill
        {
            get { return Instance["is_bill"].IntegerValue; }
            set { Instance["is_bill"].IntegerValue = value; }
        }

        public Int32 is_card
        {
            get { return Instance["is_card"].IntegerValue; }
            set { Instance["is_card"].IntegerValue = value; }
        }

        public Int32 is_key
        {
            get { return Instance["is_key"].IntegerValue; }
            set { Instance["is_key"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_contract_rent() : base() { }

        public bm_process_store_contract_rent(Entity instance) : base(instance) { }
        #endregion

    }
}
