﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_store_contract : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_store_contract";

        #region ====Properties====
        private YJH.Entities.bm_store_expand _expand;
        public YJH.Entities.bm_store_expand expand
        {
            get
            {
                if (_expand == null)
                {
                    Entity value = Instance["expand"].EntityValue;
                    if (value == null)
                        return null;
                    _expand = new YJH.Entities.bm_store_expand(value);
                }
                return _expand;
            }
            set
            {
                _expand = value;
                if (value == null)
                    Instance["expand"].EntityValue = null;
                else
                    Instance["expand"].EntityValue = _expand.Instance;
            }
        }

        public Guid expandID
        {
            get { return Instance["expandID"].GuidValue; }
            set { Instance["expandID"].GuidValue = value; }
        }

        public String contract_no
        {
            get { return Instance["contract_no"].StringValue; }
            set { Instance["contract_no"].StringValue = value; }
        }

        public DateTime startDate
        {
            get { return Instance["startDate"].DateValue; }
            set { Instance["startDate"].DateValue = value; }
        }

        public DateTime endDate
        {
            get { return Instance["endDate"].DateValue; }
            set { Instance["endDate"].DateValue = value; }
        }

        private YJH.Entities.hr_emploee _recorder;
        public YJH.Entities.hr_emploee recorder
        {
            get
            {
                if (_recorder == null)
                {
                    Entity value = Instance["recorder"].EntityValue;
                    if (value == null)
                        return null;
                    _recorder = new YJH.Entities.hr_emploee(value);
                }
                return _recorder;
            }
            set
            {
                _recorder = value;
                if (value == null)
                    Instance["recorder"].EntityValue = null;
                else
                    Instance["recorder"].EntityValue = _recorder.Instance;
            }
        }

        public Guid? recorderID
        {
            get
            {
                if (Instance["recorderID"].HasValue)
                    return Instance["recorderID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["recorderID"].Value = null;
                else
                    Instance["recorderID"].GuidValue = value.Value;
            }
        }

        public DateTime? recorderdate
        {
            get
            {
                if (Instance["recorderdate"].HasValue)
                    return Instance["recorderdate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["recorderdate"].Value = null;
                else
                    Instance["recorderdate"].DateValue = value.Value;
            }
        }

        public String contractRemark
        {
            get { return Instance["contractRemark"].StringValue; }
            set { Instance["contractRemark"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_store_contract() : base() { }

        public bm_store_contract(Entity instance) : base(instance) { }
        #endregion

    }
}
