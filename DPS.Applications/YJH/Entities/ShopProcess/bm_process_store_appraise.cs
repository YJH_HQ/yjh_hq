﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_appraise : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_appraise";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public String store_appraise
        {
            get { return Instance["store_appraise"].StringValue; }
            set { Instance["store_appraise"].StringValue = value; }
        }

        public String store_prompt
        {
            get { return Instance["store_prompt"].StringValue; }
            set { Instance["store_prompt"].StringValue = value; }
        }

        public String store_low_estimate
        {
            get { return Instance["store_low_estimate"].StringValue; }
            set { Instance["store_low_estimate"].StringValue = value; }
        }

        public Decimal? store_low_rent_rate_estimate
        {
            get
            {
                if (Instance["store_low_rent_rate_estimate"].HasValue)
                    return Instance["store_low_rent_rate_estimate"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_low_rent_rate_estimate"].Value = null;
                else
                    Instance["store_low_rent_rate_estimate"].DecimalValue = value.Value;
            }
        }

        public String store_busy_estimate
        {
            get { return Instance["store_busy_estimate"].StringValue; }
            set { Instance["store_busy_estimate"].StringValue = value; }
        }

        public Decimal? store_busy_rent_rate_estimate
        {
            get
            {
                if (Instance["store_busy_rent_rate_estimate"].HasValue)
                    return Instance["store_busy_rent_rate_estimate"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_busy_rent_rate_estimate"].Value = null;
                else
                    Instance["store_busy_rent_rate_estimate"].DecimalValue = value.Value;
            }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_appraise() : base() { }

        public bm_process_store_appraise(Entity instance) : base(instance) { }
        #endregion

    }
}
