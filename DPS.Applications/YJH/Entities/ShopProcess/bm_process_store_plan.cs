﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_plan : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_plan";

        #region ====Properties====
        public String IID
        {
            get { return Instance["IID"].StringValue; }
            set { Instance["IID"].StringValue = value; }
        }

        public String plan_id
        {
            get { return Instance["plan_id"].StringValue; }
            set { Instance["plan_id"].StringValue = value; }
        }

        public Int32 plan_month
        {
            get { return Instance["plan_month"].IntegerValue; }
            set { Instance["plan_month"].IntegerValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Int32 store_check
        {
            get { return Instance["store_check"].IntegerValue; }
            set { Instance["store_check"].IntegerValue = value; }
        }

        public DateTime open_date
        {
            get { return Instance["open_date"].DateValue; }
            set { Instance["open_date"].DateValue = value; }
        }

        public Int32 express_vehicle
        {
            get { return Instance["express_vehicle"].IntegerValue; }
            set { Instance["express_vehicle"].IntegerValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String updater
        {
            get { return Instance["updater"].StringValue; }
            set { Instance["updater"].StringValue = value; }
        }

        public DateTime? update_date
        {
            get
            {
                if (Instance["update_date"].HasValue)
                    return Instance["update_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["update_date"].Value = null;
                else
                    Instance["update_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_plan() : base() { }

        public bm_process_store_plan(Entity instance) : base(instance) { }
        #endregion

    }
}
