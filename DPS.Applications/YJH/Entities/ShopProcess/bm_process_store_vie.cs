﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_vie : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_vie";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public Int32? is_store
        {
            get
            {
                if (Instance["is_store"].HasValue)
                    return Instance["is_store"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_store"].Value = null;
                else
                    Instance["is_store"].IntegerValue = value.Value;
            }
        }

        public String store_addr_original
        {
            get { return Instance["store_addr_original"].StringValue; }
            set { Instance["store_addr_original"].StringValue = value; }
        }

        public String store_turnover_original
        {
            get { return Instance["store_turnover_original"].StringValue; }
            set { Instance["store_turnover_original"].StringValue = value; }
        }

        public Decimal? store_area_original
        {
            get
            {
                if (Instance["store_area_original"].HasValue)
                    return Instance["store_area_original"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_area_original"].Value = null;
                else
                    Instance["store_area_original"].DecimalValue = value.Value;
            }
        }

        public Decimal? store_distance_original
        {
            get
            {
                if (Instance["store_distance_original"].HasValue)
                    return Instance["store_distance_original"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_distance_original"].Value = null;
                else
                    Instance["store_distance_original"].DecimalValue = value.Value;
            }
        }

        public Decimal? store_rent_original
        {
            get
            {
                if (Instance["store_rent_original"].HasValue)
                    return Instance["store_rent_original"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_rent_original"].Value = null;
                else
                    Instance["store_rent_original"].DecimalValue = value.Value;
            }
        }

        public Int32? is_vie
        {
            get
            {
                if (Instance["is_vie"].HasValue)
                    return Instance["is_vie"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_vie"].Value = null;
                else
                    Instance["is_vie"].IntegerValue = value.Value;
            }
        }

        public String vie_name
        {
            get { return Instance["vie_name"].StringValue; }
            set { Instance["vie_name"].StringValue = value; }
        }

        public Decimal? vie_area
        {
            get
            {
                if (Instance["vie_area"].HasValue)
                    return Instance["vie_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["vie_area"].Value = null;
                else
                    Instance["vie_area"].DecimalValue = value.Value;
            }
        }

        public Decimal? vie_turnover
        {
            get
            {
                if (Instance["vie_turnover"].HasValue)
                    return Instance["vie_turnover"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["vie_turnover"].Value = null;
                else
                    Instance["vie_turnover"].DecimalValue = value.Value;
            }
        }

        public String vie_discribe
        {
            get { return Instance["vie_discribe"].StringValue; }
            set { Instance["vie_discribe"].StringValue = value; }
        }

        public String vie_addr_contrast
        {
            get { return Instance["vie_addr_contrast"].StringValue; }
            set { Instance["vie_addr_contrast"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_vie() : base() { }

        public bm_process_store_vie(Entity instance) : base(instance) { }
        #endregion

    }
}
