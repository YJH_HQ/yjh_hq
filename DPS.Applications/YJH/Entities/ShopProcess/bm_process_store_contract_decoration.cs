﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_contract_decoration : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_contract_decoration";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String decoration_contract_id
        {
            get { return Instance["decoration_contract_id"].StringValue; }
            set { Instance["decoration_contract_id"].StringValue = value; }
        }

        public Decimal decoration_amt_tax
        {
            get { return Instance["decoration_amt_tax"].DecimalValue; }
            set { Instance["decoration_amt_tax"].DecimalValue = value; }
        }

        public String decoration_company_id
        {
            get { return Instance["decoration_company_id"].StringValue; }
            set { Instance["decoration_company_id"].StringValue = value; }
        }

        public DateTime contract_date
        {
            get { return Instance["contract_date"].DateValue; }
            set { Instance["contract_date"].DateValue = value; }
        }

        public DateTime decoration_startdate
        {
            get { return Instance["decoration_startdate"].DateValue; }
            set { Instance["decoration_startdate"].DateValue = value; }
        }

        public DateTime decoration_enddate
        {
            get { return Instance["decoration_enddate"].DateValue; }
            set { Instance["decoration_enddate"].DateValue = value; }
        }

        public Decimal decoration_period
        {
            get { return Instance["decoration_period"].DecimalValue; }
            set { Instance["decoration_period"].DecimalValue = value; }
        }

        public Int32 decoration_audit
        {
            get { return Instance["decoration_audit"].IntegerValue; }
            set { Instance["decoration_audit"].IntegerValue = value; }
        }

        public DateTime decoration_date
        {
            get { return Instance["decoration_date"].DateValue; }
            set { Instance["decoration_date"].DateValue = value; }
        }

        public DateTime expect_complete_date
        {
            get { return Instance["expect_complete_date"].DateValue; }
            set { Instance["expect_complete_date"].DateValue = value; }
        }

        public DateTime infact_complete_date
        {
            get { return Instance["infact_complete_date"].DateValue; }
            set { Instance["infact_complete_date"].DateValue = value; }
        }

        public Decimal contract_amt
        {
            get { return Instance["contract_amt"].DecimalValue; }
            set { Instance["contract_amt"].DecimalValue = value; }
        }

        public Decimal pay_amt
        {
            get { return Instance["pay_amt"].DecimalValue; }
            set { Instance["pay_amt"].DecimalValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_contract_decoration() : base() { }

        public bm_process_store_contract_decoration(Entity instance) : base(instance) { }
        #endregion

    }
}
