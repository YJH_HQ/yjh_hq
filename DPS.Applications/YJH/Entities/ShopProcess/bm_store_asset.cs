﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_store_asset : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_store_asset";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String asset_id
        {
            get { return Instance["asset_id"].StringValue; }
            set { Instance["asset_id"].StringValue = value; }
        }

        public String asset_name
        {
            get { return Instance["asset_name"].StringValue; }
            set { Instance["asset_name"].StringValue = value; }
        }

        public Decimal asset_cost
        {
            get { return Instance["asset_cost"].DecimalValue; }
            set { Instance["asset_cost"].DecimalValue = value; }
        }

        public Int32 asset_class
        {
            get { return Instance["asset_class"].IntegerValue; }
            set { Instance["asset_class"].IntegerValue = value; }
        }

        public Int32 asset_type
        {
            get { return Instance["asset_type"].IntegerValue; }
            set { Instance["asset_type"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_store_asset() : base() { }

        public bm_store_asset(Entity instance) : base(instance) { }
        #endregion

    }
}
