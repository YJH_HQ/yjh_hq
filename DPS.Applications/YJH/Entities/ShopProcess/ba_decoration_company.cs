﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ba_decoration_company : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ba_decoration_company";

        #region ====Properties====
        public String decoration_company_id
        {
            get { return Instance["decoration_company_id"].StringValue; }
            set { Instance["decoration_company_id"].StringValue = value; }
        }

        public String decoration_company_name
        {
            get { return Instance["decoration_company_name"].StringValue; }
            set { Instance["decoration_company_name"].StringValue = value; }
        }

        public DateTime in_date
        {
            get { return Instance["in_date"].DateValue; }
            set { Instance["in_date"].DateValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ba_decoration_company() : base() { }

        public ba_decoration_company(Entity instance) : base(instance) { }
        #endregion

    }
}
