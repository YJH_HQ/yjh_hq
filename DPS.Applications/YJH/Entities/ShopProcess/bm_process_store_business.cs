﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_business : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_business";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public DateTime morning_starttime_num
        {
            get { return Instance["morning_starttime_num"].DateValue; }
            set { Instance["morning_starttime_num"].DateValue = value; }
        }

        public DateTime morning_endtime_num
        {
            get { return Instance["morning_endtime_num"].DateValue; }
            set { Instance["morning_endtime_num"].DateValue = value; }
        }

        public Int32? morning_num
        {
            get
            {
                if (Instance["morning_num"].HasValue)
                    return Instance["morning_num"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["morning_num"].Value = null;
                else
                    Instance["morning_num"].IntegerValue = value.Value;
            }
        }

        public String weather_morning
        {
            get { return Instance["weather_morning"].StringValue; }
            set { Instance["weather_morning"].StringValue = value; }
        }

        public DateTime noon_starttime_num
        {
            get { return Instance["noon_starttime_num"].DateValue; }
            set { Instance["noon_starttime_num"].DateValue = value; }
        }

        public DateTime noon_endtime_num
        {
            get { return Instance["noon_endtime_num"].DateValue; }
            set { Instance["noon_endtime_num"].DateValue = value; }
        }

        public Int32? noon_num
        {
            get
            {
                if (Instance["noon_num"].HasValue)
                    return Instance["noon_num"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["noon_num"].Value = null;
                else
                    Instance["noon_num"].IntegerValue = value.Value;
            }
        }

        public String weather_noon
        {
            get { return Instance["weather_noon"].StringValue; }
            set { Instance["weather_noon"].StringValue = value; }
        }

        public DateTime night_starttime_num
        {
            get { return Instance["night_starttime_num"].DateValue; }
            set { Instance["night_starttime_num"].DateValue = value; }
        }

        public DateTime night_endtime_num
        {
            get { return Instance["night_endtime_num"].DateValue; }
            set { Instance["night_endtime_num"].DateValue = value; }
        }

        public Int32? night_num
        {
            get
            {
                if (Instance["night_num"].HasValue)
                    return Instance["night_num"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["night_num"].Value = null;
                else
                    Instance["night_num"].IntegerValue = value.Value;
            }
        }

        public String weather_night
        {
            get { return Instance["weather_night"].StringValue; }
            set { Instance["weather_night"].StringValue = value; }
        }

        public Int32? is_concentrated_residential
        {
            get
            {
                if (Instance["is_concentrated_residential"].HasValue)
                    return Instance["is_concentrated_residential"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_concentrated_residential"].Value = null;
                else
                    Instance["is_concentrated_residential"].IntegerValue = value.Value;
            }
        }

        public Decimal? household_num
        {
            get
            {
                if (Instance["household_num"].HasValue)
                    return Instance["household_num"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["household_num"].Value = null;
                else
                    Instance["household_num"].DecimalValue = value.Value;
            }
        }

        public Int32? is_near_residential
        {
            get
            {
                if (Instance["is_near_residential"].HasValue)
                    return Instance["is_near_residential"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_near_residential"].Value = null;
                else
                    Instance["is_near_residential"].IntegerValue = value.Value;
            }
        }

        public Decimal? occupancy_rate
        {
            get
            {
                if (Instance["occupancy_rate"].HasValue)
                    return Instance["occupancy_rate"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["occupancy_rate"].Value = null;
                else
                    Instance["occupancy_rate"].DecimalValue = value.Value;
            }
        }

        public Int32? is_mature_community
        {
            get
            {
                if (Instance["is_mature_community"].HasValue)
                    return Instance["is_mature_community"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_mature_community"].Value = null;
                else
                    Instance["is_mature_community"].IntegerValue = value.Value;
            }
        }

        public Int32? living_level
        {
            get
            {
                if (Instance["living_level"].HasValue)
                    return Instance["living_level"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["living_level"].Value = null;
                else
                    Instance["living_level"].IntegerValue = value.Value;
            }
        }

        public String left_first_store_major_business
        {
            get { return Instance["left_first_store_major_business"].StringValue; }
            set { Instance["left_first_store_major_business"].StringValue = value; }
        }

        public Decimal? left_first_store_area
        {
            get
            {
                if (Instance["left_first_store_area"].HasValue)
                    return Instance["left_first_store_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_first_store_area"].Value = null;
                else
                    Instance["left_first_store_area"].DecimalValue = value.Value;
            }
        }

        public Decimal? left_first_store_rent
        {
            get
            {
                if (Instance["left_first_store_rent"].HasValue)
                    return Instance["left_first_store_rent"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_first_store_rent"].Value = null;
                else
                    Instance["left_first_store_rent"].DecimalValue = value.Value;
            }
        }

        public Int32? left_first_store_turnover
        {
            get
            {
                if (Instance["left_first_store_turnover"].HasValue)
                    return Instance["left_first_store_turnover"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_first_store_turnover"].Value = null;
                else
                    Instance["left_first_store_turnover"].IntegerValue = value.Value;
            }
        }

        public String left_second_store_major_business
        {
            get { return Instance["left_second_store_major_business"].StringValue; }
            set { Instance["left_second_store_major_business"].StringValue = value; }
        }

        public Decimal? left_second_store_area
        {
            get
            {
                if (Instance["left_second_store_area"].HasValue)
                    return Instance["left_second_store_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_second_store_area"].Value = null;
                else
                    Instance["left_second_store_area"].DecimalValue = value.Value;
            }
        }

        public Decimal? left_second_store_rent
        {
            get
            {
                if (Instance["left_second_store_rent"].HasValue)
                    return Instance["left_second_store_rent"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_second_store_rent"].Value = null;
                else
                    Instance["left_second_store_rent"].DecimalValue = value.Value;
            }
        }

        public Decimal? left_second_store_turnover
        {
            get
            {
                if (Instance["left_second_store_turnover"].HasValue)
                    return Instance["left_second_store_turnover"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["left_second_store_turnover"].Value = null;
                else
                    Instance["left_second_store_turnover"].DecimalValue = value.Value;
            }
        }

        public String right_first_store_major_business
        {
            get { return Instance["right_first_store_major_business"].StringValue; }
            set { Instance["right_first_store_major_business"].StringValue = value; }
        }

        public Decimal? right_first_store_area
        {
            get
            {
                if (Instance["right_first_store_area"].HasValue)
                    return Instance["right_first_store_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_first_store_area"].Value = null;
                else
                    Instance["right_first_store_area"].DecimalValue = value.Value;
            }
        }

        public Decimal? right_first_store_rent
        {
            get
            {
                if (Instance["right_first_store_rent"].HasValue)
                    return Instance["right_first_store_rent"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_first_store_rent"].Value = null;
                else
                    Instance["right_first_store_rent"].DecimalValue = value.Value;
            }
        }

        public Decimal? right_first_store_turnover
        {
            get
            {
                if (Instance["right_first_store_turnover"].HasValue)
                    return Instance["right_first_store_turnover"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_first_store_turnover"].Value = null;
                else
                    Instance["right_first_store_turnover"].DecimalValue = value.Value;
            }
        }

        public String right_second_store_major_business
        {
            get { return Instance["right_second_store_major_business"].StringValue; }
            set { Instance["right_second_store_major_business"].StringValue = value; }
        }

        public Decimal? right_second_store_area
        {
            get
            {
                if (Instance["right_second_store_area"].HasValue)
                    return Instance["right_second_store_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_second_store_area"].Value = null;
                else
                    Instance["right_second_store_area"].DecimalValue = value.Value;
            }
        }

        public Decimal? right_second_store_rent
        {
            get
            {
                if (Instance["right_second_store_rent"].HasValue)
                    return Instance["right_second_store_rent"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_second_store_rent"].Value = null;
                else
                    Instance["right_second_store_rent"].DecimalValue = value.Value;
            }
        }

        public Decimal? right_second_store_turnover
        {
            get
            {
                if (Instance["right_second_store_turnover"].HasValue)
                    return Instance["right_second_store_turnover"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["right_second_store_turnover"].Value = null;
                else
                    Instance["right_second_store_turnover"].DecimalValue = value.Value;
            }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_business() : base() { }

        public bm_process_store_business(Entity instance) : base(instance) { }
        #endregion

    }
}
