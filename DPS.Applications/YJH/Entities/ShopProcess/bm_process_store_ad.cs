﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_ad : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_ad";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public Int32? store_ad_type
        {
            get
            {
                if (Instance["store_ad_type"].HasValue)
                    return Instance["store_ad_type"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_ad_type"].Value = null;
                else
                    Instance["store_ad_type"].IntegerValue = value.Value;
            }
        }

        public Decimal? ad_length
        {
            get
            {
                if (Instance["ad_length"].HasValue)
                    return Instance["ad_length"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ad_length"].Value = null;
                else
                    Instance["ad_length"].DecimalValue = value.Value;
            }
        }

        public Decimal? ad_width
        {
            get
            {
                if (Instance["ad_width"].HasValue)
                    return Instance["ad_width"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ad_width"].Value = null;
                else
                    Instance["ad_width"].DecimalValue = value.Value;
            }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime? create_date
        {
            get
            {
                if (Instance["create_date"].HasValue)
                    return Instance["create_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["create_date"].Value = null;
                else
                    Instance["create_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_ad() : base() { }

        public bm_process_store_ad(Entity instance) : base(instance) { }
        #endregion

    }
}
