﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_decoration_detail : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_decoration_detail";

        #region ====Properties====
        public String check_id
        {
            get { return Instance["check_id"].StringValue; }
            set { Instance["check_id"].StringValue = value; }
        }

        public String decoration_item_id
        {
            get { return Instance["decoration_item_id"].StringValue; }
            set { Instance["decoration_item_id"].StringValue = value; }
        }

        public Decimal decoration_item_amt
        {
            get { return Instance["decoration_item_amt"].DecimalValue; }
            set { Instance["decoration_item_amt"].DecimalValue = value; }
        }

        public DateTime decoration_enddate
        {
            get { return Instance["decoration_enddate"].DateValue; }
            set { Instance["decoration_enddate"].DateValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public Guid dept
        {
            get { return Instance["dept"].GuidValue; }
            set { Instance["dept"].GuidValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_decoration_detail() : base() { }

        public bm_process_store_decoration_detail(Entity instance) : base(instance) { }
        #endregion

    }
}
