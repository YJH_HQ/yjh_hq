﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_hr : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_hr";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Int32 master
        {
            get { return Instance["master"].IntegerValue; }
            set { Instance["master"].IntegerValue = value; }
        }

        public Int32 emp
        {
            get { return Instance["emp"].IntegerValue; }
            set { Instance["emp"].IntegerValue = value; }
        }

        public Int32 couriter
        {
            get { return Instance["couriter"].IntegerValue; }
            set { Instance["couriter"].IntegerValue = value; }
        }

        public Int32 train
        {
            get { return Instance["train"].IntegerValue; }
            set { Instance["train"].IntegerValue = value; }
        }

        public Int32 examine
        {
            get { return Instance["examine"].IntegerValue; }
            set { Instance["examine"].IntegerValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String updater
        {
            get { return Instance["updater"].StringValue; }
            set { Instance["updater"].StringValue = value; }
        }

        public DateTime? update_date
        {
            get
            {
                if (Instance["update_date"].HasValue)
                    return Instance["update_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["update_date"].Value = null;
                else
                    Instance["update_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_hr() : base() { }

        public bm_process_store_hr(Entity instance) : base(instance) { }
        #endregion

    }
}
