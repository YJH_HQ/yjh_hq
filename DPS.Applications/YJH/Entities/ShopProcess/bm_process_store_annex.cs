﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_annex : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_annex";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public String annex_id
        {
            get { return Instance["annex_id"].StringValue; }
            set { Instance["annex_id"].StringValue = value; }
        }

        public Int32? annex_type
        {
            get
            {
                if (Instance["annex_type"].HasValue)
                    return Instance["annex_type"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["annex_type"].Value = null;
                else
                    Instance["annex_type"].IntegerValue = value.Value;
            }
        }

        public String uploader
        {
            get { return Instance["uploader"].StringValue; }
            set { Instance["uploader"].StringValue = value; }
        }

        public String dept
        {
            get { return Instance["dept"].StringValue; }
            set { Instance["dept"].StringValue = value; }
        }

        public DateTime upload_date
        {
            get { return Instance["upload_date"].DateValue; }
            set { Instance["upload_date"].DateValue = value; }
        }

        public String version
        {
            get { return Instance["version"].StringValue; }
            set { Instance["version"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_annex() : base() { }

        public bm_process_store_annex(Entity instance) : base(instance) { }
        #endregion

    }
}
