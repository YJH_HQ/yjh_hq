﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_expand : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_expand";

        #region ====Properties====
        public String expand_no
        {
            get { return Instance["expand_no"].StringValue; }
            set { Instance["expand_no"].StringValue = value; }
        }

        public String province
        {
            get { return Instance["province"].StringValue; }
            set { Instance["province"].StringValue = value; }
        }

        public String city
        {
            get { return Instance["city"].StringValue; }
            set { Instance["city"].StringValue = value; }
        }

        public String area
        {
            get { return Instance["area"].StringValue; }
            set { Instance["area"].StringValue = value; }
        }

        public String street
        {
            get { return Instance["street"].StringValue; }
            set { Instance["street"].StringValue = value; }
        }

        public String community_name
        {
            get { return Instance["community_name"].StringValue; }
            set { Instance["community_name"].StringValue = value; }
        }

        public String owner
        {
            get { return Instance["owner"].StringValue; }
            set { Instance["owner"].StringValue = value; }
        }

        public String landlord_tel
        {
            get { return Instance["landlord_tel"].StringValue; }
            set { Instance["landlord_tel"].StringValue = value; }
        }

        public Int32 room_property
        {
            get { return Instance["room_property"].IntegerValue; }
            set { Instance["room_property"].IntegerValue = value; }
        }

        public DateTime contract_enddate
        {
            get { return Instance["contract_enddate"].DateValue; }
            set { Instance["contract_enddate"].DateValue = value; }
        }

        public Int32 is_bill
        {
            get { return Instance["is_bill"].IntegerValue; }
            set { Instance["is_bill"].IntegerValue = value; }
        }

        public DateTime expand_date
        {
            get { return Instance["expand_date"].DateValue; }
            set { Instance["expand_date"].DateValue = value; }
        }

        public String addr
        {
            get { return Instance["addr"].StringValue; }
            set { Instance["addr"].StringValue = value; }
        }

        public String description
        {
            get { return Instance["description"].StringValue; }
            set { Instance["description"].StringValue = value; }
        }

        public String format_near
        {
            get { return Instance["format_near"].StringValue; }
            set { Instance["format_near"].StringValue = value; }
        }

        public String appraise
        {
            get { return Instance["appraise"].StringValue; }
            set { Instance["appraise"].StringValue = value; }
        }

        public String competitor
        {
            get { return Instance["competitor"].StringValue; }
            set { Instance["competitor"].StringValue = value; }
        }

        public String expand_emp
        {
            get { return Instance["expand_emp"].StringValue; }
            set { Instance["expand_emp"].StringValue = value; }
        }

        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_expand() : base() { }

        public bm_process_store_expand(Entity instance) : base(instance) { }
        #endregion

    }
}
