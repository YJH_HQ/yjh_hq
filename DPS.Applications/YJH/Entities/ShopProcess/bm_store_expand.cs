﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_store_expand : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_store_expand";

        #region ====Properties====
        public String expand_no
        {
            get { return Instance["expand_no"].StringValue; }
            set { Instance["expand_no"].StringValue = value; }
        }

        public String province
        {
            get { return Instance["province"].StringValue; }
            set { Instance["province"].StringValue = value; }
        }

        public String city
        {
            get { return Instance["city"].StringValue; }
            set { Instance["city"].StringValue = value; }
        }

        public String area
        {
            get { return Instance["area"].StringValue; }
            set { Instance["area"].StringValue = value; }
        }

        public String street
        {
            get { return Instance["street"].StringValue; }
            set { Instance["street"].StringValue = value; }
        }

        public String addr
        {
            get { return Instance["addr"].StringValue; }
            set { Instance["addr"].StringValue = value; }
        }

        public String store_name
        {
            get { return Instance["store_name"].StringValue; }
            set { Instance["store_name"].StringValue = value; }
        }

        public String property_rights
        {
            get { return Instance["property_rights"].StringValue; }
            set { Instance["property_rights"].StringValue = value; }
        }

        public String linkman
        {
            get { return Instance["linkman"].StringValue; }
            set { Instance["linkman"].StringValue = value; }
        }

        public String link_tel
        {
            get { return Instance["link_tel"].StringValue; }
            set { Instance["link_tel"].StringValue = value; }
        }

        public Decimal? floor_space
        {
            get
            {
                if (Instance["floor_space"].HasValue)
                    return Instance["floor_space"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["floor_space"].Value = null;
                else
                    Instance["floor_space"].DecimalValue = value.Value;
            }
        }

        public Decimal? use_area
        {
            get
            {
                if (Instance["use_area"].HasValue)
                    return Instance["use_area"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["use_area"].Value = null;
                else
                    Instance["use_area"].DecimalValue = value.Value;
            }
        }

        public Int32? storeProperty
        {
            get
            {
                if (Instance["storeProperty"].HasValue)
                    return Instance["storeProperty"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["storeProperty"].Value = null;
                else
                    Instance["storeProperty"].IntegerValue = value.Value;
            }
        }

        private YJH.Entities.hr_emploee _expander;
        public YJH.Entities.hr_emploee expander
        {
            get
            {
                if (_expander == null)
                {
                    Entity value = Instance["expander"].EntityValue;
                    if (value == null)
                        return null;
                    _expander = new YJH.Entities.hr_emploee(value);
                }
                return _expander;
            }
            set
            {
                _expander = value;
                if (value == null)
                    Instance["expander"].EntityValue = null;
                else
                    Instance["expander"].EntityValue = _expander.Instance;
            }
        }

        public Guid expanderID
        {
            get { return Instance["expanderID"].GuidValue; }
            set { Instance["expanderID"].GuidValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public DateTime? decorationPredate
        {
            get
            {
                if (Instance["decorationPredate"].HasValue)
                    return Instance["decorationPredate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["decorationPredate"].Value = null;
                else
                    Instance["decorationPredate"].DateValue = value.Value;
            }
        }

        public DateTime? decorationdate
        {
            get
            {
                if (Instance["decorationdate"].HasValue)
                    return Instance["decorationdate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["decorationdate"].Value = null;
                else
                    Instance["decorationdate"].DateValue = value.Value;
            }
        }

        public String decorationStatus
        {
            get { return Instance["decorationStatus"].StringValue; }
            set { Instance["decorationStatus"].StringValue = value; }
        }

        public DateTime? store_plan_date
        {
            get
            {
                if (Instance["store_plan_date"].HasValue)
                    return Instance["store_plan_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_plan_date"].Value = null;
                else
                    Instance["store_plan_date"].DateValue = value.Value;
            }
        }

        public DateTime? store_act_date
        {
            get
            {
                if (Instance["store_act_date"].HasValue)
                    return Instance["store_act_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_act_date"].Value = null;
                else
                    Instance["store_act_date"].DateValue = value.Value;
            }
        }

        public String Remark
        {
            get { return Instance["Remark"].StringValue; }
            set { Instance["Remark"].StringValue = value; }
        }

        public String attachmentExpand
        {
            get { return Instance["attachmentExpand"].StringValue; }
            set { Instance["attachmentExpand"].StringValue = value; }
        }

        public String attachmentReview
        {
            get { return Instance["attachmentReview"].StringValue; }
            set { Instance["attachmentReview"].StringValue = value; }
        }

        public String attachmentPic
        {
            get { return Instance["attachmentPic"].StringValue; }
            set { Instance["attachmentPic"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime? create_date
        {
            get
            {
                if (Instance["create_date"].HasValue)
                    return Instance["create_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["create_date"].Value = null;
                else
                    Instance["create_date"].DateValue = value.Value;
            }
        }

        private YJH.Entities.hr_emploee _reviewer;
        public YJH.Entities.hr_emploee reviewer
        {
            get
            {
                if (_reviewer == null)
                {
                    Entity value = Instance["reviewer"].EntityValue;
                    if (value == null)
                        return null;
                    _reviewer = new YJH.Entities.hr_emploee(value);
                }
                return _reviewer;
            }
            set
            {
                _reviewer = value;
                if (value == null)
                    Instance["reviewer"].EntityValue = null;
                else
                    Instance["reviewer"].EntityValue = _reviewer.Instance;
            }
        }

        public Guid reviewerID
        {
            get { return Instance["reviewerID"].GuidValue; }
            set { Instance["reviewerID"].GuidValue = value; }
        }

        public DateTime? reviewer_date
        {
            get
            {
                if (Instance["reviewer_date"].HasValue)
                    return Instance["reviewer_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["reviewer_date"].Value = null;
                else
                    Instance["reviewer_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_store_expand() : base() { }

        public bm_store_expand(Entity instance) : base(instance) { }
        #endregion

    }
}
