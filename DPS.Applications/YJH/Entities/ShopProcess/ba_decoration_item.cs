﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ba_decoration_item : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ba_decoration_item";

        #region ====Properties====
        public String decoration_subject_id
        {
            get { return Instance["decoration_subject_id"].StringValue; }
            set { Instance["decoration_subject_id"].StringValue = value; }
        }

        public String decoration_subject_name
        {
            get { return Instance["decoration_subject_name"].StringValue; }
            set { Instance["decoration_subject_name"].StringValue = value; }
        }

        public String decoration_item_id
        {
            get { return Instance["decoration_item_id"].StringValue; }
            set { Instance["decoration_item_id"].StringValue = value; }
        }

        public String decoration_item_name
        {
            get { return Instance["decoration_item_name"].StringValue; }
            set { Instance["decoration_item_name"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ba_decoration_item() : base() { }

        public ba_decoration_item(Entity instance) : base(instance) { }
        #endregion

    }
}
