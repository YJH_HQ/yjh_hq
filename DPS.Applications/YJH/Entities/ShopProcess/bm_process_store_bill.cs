﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_bill : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_bill";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public String province
        {
            get { return Instance["province"].StringValue; }
            set { Instance["province"].StringValue = value; }
        }

        public String city
        {
            get { return Instance["city"].StringValue; }
            set { Instance["city"].StringValue = value; }
        }

        public String area
        {
            get { return Instance["area"].StringValue; }
            set { Instance["area"].StringValue = value; }
        }

        public String street
        {
            get { return Instance["street"].StringValue; }
            set { Instance["street"].StringValue = value; }
        }

        public String addr
        {
            get { return Instance["addr"].StringValue; }
            set { Instance["addr"].StringValue = value; }
        }

        public String near
        {
            get { return Instance["near"].StringValue; }
            set { Instance["near"].StringValue = value; }
        }

        public String property_addr
        {
            get { return Instance["property_addr"].StringValue; }
            set { Instance["property_addr"].StringValue = value; }
        }

        public String property_rights
        {
            get { return Instance["property_rights"].StringValue; }
            set { Instance["property_rights"].StringValue = value; }
        }

        public String lessor
        {
            get { return Instance["lessor"].StringValue; }
            set { Instance["lessor"].StringValue = value; }
        }

        public String certificate_infor
        {
            get { return Instance["certificate_infor"].StringValue; }
            set { Instance["certificate_infor"].StringValue = value; }
        }

        public Decimal floor_space
        {
            get { return Instance["floor_space"].DecimalValue; }
            set { Instance["floor_space"].DecimalValue = value; }
        }

        public Decimal use_area
        {
            get { return Instance["use_area"].DecimalValue; }
            set { Instance["use_area"].DecimalValue = value; }
        }

        public Int32 area_audit
        {
            get { return Instance["area_audit"].IntegerValue; }
            set { Instance["area_audit"].IntegerValue = value; }
        }

        public Decimal door_width
        {
            get { return Instance["door_width"].DecimalValue; }
            set { Instance["door_width"].DecimalValue = value; }
        }

        public Decimal door_inside_width
        {
            get { return Instance["door_inside_width"].DecimalValue; }
            set { Instance["door_inside_width"].DecimalValue = value; }
        }

        public Decimal door_depth
        {
            get { return Instance["door_depth"].DecimalValue; }
            set { Instance["door_depth"].DecimalValue = value; }
        }

        public Decimal floor
        {
            get { return Instance["floor"].DecimalValue; }
            set { Instance["floor"].DecimalValue = value; }
        }

        public Int32 size_audit
        {
            get { return Instance["size_audit"].IntegerValue; }
            set { Instance["size_audit"].IntegerValue = value; }
        }

        public Decimal room_num
        {
            get { return Instance["room_num"].DecimalValue; }
            set { Instance["room_num"].DecimalValue = value; }
        }

        public String community_name
        {
            get { return Instance["community_name"].StringValue; }
            set { Instance["community_name"].StringValue = value; }
        }

        public Int32 community_type
        {
            get { return Instance["community_type"].IntegerValue; }
            set { Instance["community_type"].IntegerValue = value; }
        }

        public Int32 building_num
        {
            get { return Instance["building_num"].IntegerValue; }
            set { Instance["building_num"].IntegerValue = value; }
        }

        public Decimal building_floor
        {
            get { return Instance["building_floor"].DecimalValue; }
            set { Instance["building_floor"].DecimalValue = value; }
        }

        public Int32 structure
        {
            get { return Instance["structure"].IntegerValue; }
            set { Instance["structure"].IntegerValue = value; }
        }

        public Int32 structure_audit
        {
            get { return Instance["structure_audit"].IntegerValue; }
            set { Instance["structure_audit"].IntegerValue = value; }
        }

        public Decimal occupancy_rate
        {
            get { return Instance["occupancy_rate"].DecimalValue; }
            set { Instance["occupancy_rate"].DecimalValue = value; }
        }

        public Int32 rate_audit
        {
            get { return Instance["rate_audit"].IntegerValue; }
            set { Instance["rate_audit"].IntegerValue = value; }
        }

        public Int32 household_num
        {
            get { return Instance["household_num"].IntegerValue; }
            set { Instance["household_num"].IntegerValue = value; }
        }

        public Int32 household_audit
        {
            get { return Instance["household_audit"].IntegerValue; }
            set { Instance["household_audit"].IntegerValue = value; }
        }

        public Int32 people_num
        {
            get { return Instance["people_num"].IntegerValue; }
            set { Instance["people_num"].IntegerValue = value; }
        }

        public Int32 people_audit
        {
            get { return Instance["people_audit"].IntegerValue; }
            set { Instance["people_audit"].IntegerValue = value; }
        }

        public Int32 floor_household_num
        {
            get { return Instance["floor_household_num"].IntegerValue; }
            set { Instance["floor_household_num"].IntegerValue = value; }
        }

        public Int32 age_level
        {
            get { return Instance["age_level"].IntegerValue; }
            set { Instance["age_level"].IntegerValue = value; }
        }

        public String people_type
        {
            get { return Instance["people_type"].StringValue; }
            set { Instance["people_type"].StringValue = value; }
        }

        public String exit_main
        {
            get { return Instance["exit_main"].StringValue; }
            set { Instance["exit_main"].StringValue = value; }
        }

        public String exit_second
        {
            get { return Instance["exit_second"].StringValue; }
            set { Instance["exit_second"].StringValue = value; }
        }

        public String other_community
        {
            get { return Instance["other_community"].StringValue; }
            set { Instance["other_community"].StringValue = value; }
        }

        public Decimal door_length
        {
            get { return Instance["door_length"].DecimalValue; }
            set { Instance["door_length"].DecimalValue = value; }
        }

        public Int32 door_audit
        {
            get { return Instance["door_audit"].IntegerValue; }
            set { Instance["door_audit"].IntegerValue = value; }
        }

        public Decimal facade_length
        {
            get { return Instance["facade_length"].DecimalValue; }
            set { Instance["facade_length"].DecimalValue = value; }
        }

        public Int32 facade_audit
        {
            get { return Instance["facade_audit"].IntegerValue; }
            set { Instance["facade_audit"].IntegerValue = value; }
        }

        public Int32 parking_num
        {
            get { return Instance["parking_num"].IntegerValue; }
            set { Instance["parking_num"].IntegerValue = value; }
        }

        public Int32 parking_audit
        {
            get { return Instance["parking_audit"].IntegerValue; }
            set { Instance["parking_audit"].IntegerValue = value; }
        }

        public Decimal exit_distance
        {
            get { return Instance["exit_distance"].DecimalValue; }
            set { Instance["exit_distance"].DecimalValue = value; }
        }

        public Int32 water_supply
        {
            get { return Instance["water_supply"].IntegerValue; }
            set { Instance["water_supply"].IntegerValue = value; }
        }

        public Int32 power_supply
        {
            get { return Instance["power_supply"].IntegerValue; }
            set { Instance["power_supply"].IntegerValue = value; }
        }

        public Int32 toilet_supply
        {
            get { return Instance["toilet_supply"].IntegerValue; }
            set { Instance["toilet_supply"].IntegerValue = value; }
        }

        public Int32 tel_supply
        {
            get { return Instance["tel_supply"].IntegerValue; }
            set { Instance["tel_supply"].IntegerValue = value; }
        }

        public Int32 network_supply
        {
            get { return Instance["network_supply"].IntegerValue; }
            set { Instance["network_supply"].IntegerValue = value; }
        }

        public String visual_range
        {
            get { return Instance["visual_range"].StringValue; }
            set { Instance["visual_range"].StringValue = value; }
        }

        public String infrastructure
        {
            get { return Instance["infrastructure"].StringValue; }
            set { Instance["infrastructure"].StringValue = value; }
        }

        public String bus_main
        {
            get { return Instance["bus_main"].StringValue; }
            set { Instance["bus_main"].StringValue = value; }
        }

        public String operating_original
        {
            get { return Instance["operating_original"].StringValue; }
            set { Instance["operating_original"].StringValue = value; }
        }

        public Int32 house_property
        {
            get { return Instance["house_property"].IntegerValue; }
            set { Instance["house_property"].IntegerValue = value; }
        }

        public String linkman
        {
            get { return Instance["linkman"].StringValue; }
            set { Instance["linkman"].StringValue = value; }
        }

        public String link_tel
        {
            get { return Instance["link_tel"].StringValue; }
            set { Instance["link_tel"].StringValue = value; }
        }

        public String bank_near
        {
            get { return Instance["bank_near"].StringValue; }
            set { Instance["bank_near"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String expand_emp
        {
            get { return Instance["expand_emp"].StringValue; }
            set { Instance["expand_emp"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_bill() : base() { }

        public bm_process_store_bill(Entity instance) : base(instance) { }
        #endregion

    }
}
