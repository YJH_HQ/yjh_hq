﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_rectify_detail : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_rectify_detail";

        #region ====Properties====
        public String rectify_id
        {
            get { return Instance["rectify_id"].StringValue; }
            set { Instance["rectify_id"].StringValue = value; }
        }

        public String rectify_item_id
        {
            get { return Instance["rectify_item_id"].StringValue; }
            set { Instance["rectify_item_id"].StringValue = value; }
        }

        public String rectify_describe
        {
            get { return Instance["rectify_describe"].StringValue; }
            set { Instance["rectify_describe"].StringValue = value; }
        }

        public Guid recheck_dept
        {
            get { return Instance["recheck_dept"].GuidValue; }
            set { Instance["recheck_dept"].GuidValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_rectify_detail() : base() { }

        public bm_process_store_rectify_detail(Entity instance) : base(instance) { }
        #endregion

    }
}
