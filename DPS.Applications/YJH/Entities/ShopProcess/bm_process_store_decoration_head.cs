﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_decoration_head : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_decoration_head";

        #region ====Properties====
        public String check_id
        {
            get { return Instance["check_id"].StringValue; }
            set { Instance["check_id"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String decoration_company_id
        {
            get { return Instance["decoration_company_id"].StringValue; }
            set { Instance["decoration_company_id"].StringValue = value; }
        }

        public DateTime start_date
        {
            get { return Instance["start_date"].DateValue; }
            set { Instance["start_date"].DateValue = value; }
        }

        public DateTime end_date
        {
            get { return Instance["end_date"].DateValue; }
            set { Instance["end_date"].DateValue = value; }
        }

        public DateTime check_date
        {
            get { return Instance["check_date"].DateValue; }
            set { Instance["check_date"].DateValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public Decimal avg_score
        {
            get { return Instance["avg_score"].DecimalValue; }
            set { Instance["avg_score"].DecimalValue = value; }
        }

        public Int32 door_setup
        {
            get { return Instance["door_setup"].IntegerValue; }
            set { Instance["door_setup"].IntegerValue = value; }
        }

        public Decimal door_amt
        {
            get { return Instance["door_amt"].DecimalValue; }
            set { Instance["door_amt"].DecimalValue = value; }
        }

        public Int32 door_pay_status
        {
            get { return Instance["door_pay_status"].IntegerValue; }
            set { Instance["door_pay_status"].IntegerValue = value; }
        }

        public Int32 adv_setup
        {
            get { return Instance["adv_setup"].IntegerValue; }
            set { Instance["adv_setup"].IntegerValue = value; }
        }

        public Decimal adv_amt
        {
            get { return Instance["adv_amt"].DecimalValue; }
            set { Instance["adv_amt"].DecimalValue = value; }
        }

        public Int32 adv_pay_status
        {
            get { return Instance["adv_pay_status"].IntegerValue; }
            set { Instance["adv_pay_status"].IntegerValue = value; }
        }

        public Int32 led_setup
        {
            get { return Instance["led_setup"].IntegerValue; }
            set { Instance["led_setup"].IntegerValue = value; }
        }

        public Decimal led_amt
        {
            get { return Instance["led_amt"].DecimalValue; }
            set { Instance["led_amt"].DecimalValue = value; }
        }

        public Int32 led_pay_status
        {
            get { return Instance["led_pay_status"].IntegerValue; }
            set { Instance["led_pay_status"].IntegerValue = value; }
        }

        public Decimal decoration_pay
        {
            get { return Instance["decoration_pay"].DecimalValue; }
            set { Instance["decoration_pay"].DecimalValue = value; }
        }

        public DateTime floor_date
        {
            get { return Instance["floor_date"].DateValue; }
            set { Instance["floor_date"].DateValue = value; }
        }

        public DateTime top_date
        {
            get { return Instance["top_date"].DateValue; }
            set { Instance["top_date"].DateValue = value; }
        }

        public DateTime wall_date
        {
            get { return Instance["wall_date"].DateValue; }
            set { Instance["wall_date"].DateValue = value; }
        }

        public DateTime water_power_date
        {
            get { return Instance["water_power_date"].DateValue; }
            set { Instance["water_power_date"].DateValue = value; }
        }

        public DateTime prop_date
        {
            get { return Instance["prop_date"].DateValue; }
            set { Instance["prop_date"].DateValue = value; }
        }

        public Int32 check_status
        {
            get { return Instance["check_status"].IntegerValue; }
            set { Instance["check_status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String updater
        {
            get { return Instance["updater"].StringValue; }
            set { Instance["updater"].StringValue = value; }
        }

        public DateTime? update_date
        {
            get
            {
                if (Instance["update_date"].HasValue)
                    return Instance["update_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["update_date"].Value = null;
                else
                    Instance["update_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_decoration_head() : base() { }

        public bm_process_store_decoration_head(Entity instance) : base(instance) { }
        #endregion

    }
}
