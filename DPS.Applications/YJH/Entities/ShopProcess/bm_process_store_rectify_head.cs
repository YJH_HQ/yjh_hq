﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_rectify_head : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_rectify_head";

        #region ====Properties====
        public String check_id
        {
            get { return Instance["check_id"].StringValue; }
            set { Instance["check_id"].StringValue = value; }
        }

        public String rectify_id
        {
            get { return Instance["rectify_id"].StringValue; }
            set { Instance["rectify_id"].StringValue = value; }
        }

        public String decoration_company_id
        {
            get { return Instance["decoration_company_id"].StringValue; }
            set { Instance["decoration_company_id"].StringValue = value; }
        }

        public DateTime start_date
        {
            get { return Instance["start_date"].DateValue; }
            set { Instance["start_date"].DateValue = value; }
        }

        public DateTime end_date
        {
            get { return Instance["end_date"].DateValue; }
            set { Instance["end_date"].DateValue = value; }
        }

        public DateTime recheck_date
        {
            get { return Instance["recheck_date"].DateValue; }
            set { Instance["recheck_date"].DateValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_rectify_head() : base() { }

        public bm_process_store_rectify_head(Entity instance) : base(instance) { }
        #endregion

    }
}
