﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_road : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_road";

        #region ====Properties====
        public String expand_id
        {
            get { return Instance["expand_id"].StringValue; }
            set { Instance["expand_id"].StringValue = value; }
        }

        public Decimal? road_width
        {
            get
            {
                if (Instance["road_width"].HasValue)
                    return Instance["road_width"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["road_width"].Value = null;
                else
                    Instance["road_width"].DecimalValue = value.Value;
            }
        }

        public Int32? lane_num
        {
            get
            {
                if (Instance["lane_num"].HasValue)
                    return Instance["lane_num"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["lane_num"].Value = null;
                else
                    Instance["lane_num"].IntegerValue = value.Value;
            }
        }

        public Decimal? crossroads_distance
        {
            get
            {
                if (Instance["crossroads_distance"].HasValue)
                    return Instance["crossroads_distance"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["crossroads_distance"].Value = null;
                else
                    Instance["crossroads_distance"].DecimalValue = value.Value;
            }
        }

        public Int32? is_step
        {
            get
            {
                if (Instance["is_step"].HasValue)
                    return Instance["is_step"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_step"].Value = null;
                else
                    Instance["is_step"].IntegerValue = value.Value;
            }
        }

        public Int32? is_isolation_belt
        {
            get
            {
                if (Instance["is_isolation_belt"].HasValue)
                    return Instance["is_isolation_belt"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_isolation_belt"].Value = null;
                else
                    Instance["is_isolation_belt"].IntegerValue = value.Value;
            }
        }

        public Int32? is_shade
        {
            get
            {
                if (Instance["is_shade"].HasValue)
                    return Instance["is_shade"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["is_shade"].Value = null;
                else
                    Instance["is_shade"].IntegerValue = value.Value;
            }
        }

        public String shade
        {
            get { return Instance["shade"].StringValue; }
            set { Instance["shade"].StringValue = value; }
        }

        public Int32? status_out_period
        {
            get
            {
                if (Instance["status_out_period"].HasValue)
                    return Instance["status_out_period"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["status_out_period"].Value = null;
                else
                    Instance["status_out_period"].IntegerValue = value.Value;
            }
        }

        public Int32? status_in_period
        {
            get
            {
                if (Instance["status_in_period"].HasValue)
                    return Instance["status_in_period"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["status_in_period"].Value = null;
                else
                    Instance["status_in_period"].IntegerValue = value.Value;
            }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_road() : base() { }

        public bm_process_store_road(Entity instance) : base(instance) { }
        #endregion

    }
}
