﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store_admin : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store_admin";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String rent_contract_id
        {
            get { return Instance["rent_contract_id"].StringValue; }
            set { Instance["rent_contract_id"].StringValue = value; }
        }

        public String owner
        {
            get { return Instance["owner"].StringValue; }
            set { Instance["owner"].StringValue = value; }
        }

        public String identification_id
        {
            get { return Instance["identification_id"].StringValue; }
            set { Instance["identification_id"].StringValue = value; }
        }

        public String landlord_tel
        {
            get { return Instance["landlord_tel"].StringValue; }
            set { Instance["landlord_tel"].StringValue = value; }
        }

        public Int32 is_property
        {
            get { return Instance["is_property"].IntegerValue; }
            set { Instance["is_property"].IntegerValue = value; }
        }

        public String property_id
        {
            get { return Instance["property_id"].StringValue; }
            set { Instance["property_id"].StringValue = value; }
        }

        public String property_provement
        {
            get { return Instance["property_provement"].StringValue; }
            set { Instance["property_provement"].StringValue = value; }
        }

        public String bussiness_license
        {
            get { return Instance["bussiness_license"].StringValue; }
            set { Instance["bussiness_license"].StringValue = value; }
        }

        public String food_circulation_id
        {
            get { return Instance["food_circulation_id"].StringValue; }
            set { Instance["food_circulation_id"].StringValue = value; }
        }

        public String org_code
        {
            get { return Instance["org_code"].StringValue; }
            set { Instance["org_code"].StringValue = value; }
        }

        public String tax_id
        {
            get { return Instance["tax_id"].StringValue; }
            set { Instance["tax_id"].StringValue = value; }
        }

        public String express_record_id
        {
            get { return Instance["express_record_id"].StringValue; }
            set { Instance["express_record_id"].StringValue = value; }
        }

        public String tobacco_id
        {
            get { return Instance["tobacco_id"].StringValue; }
            set { Instance["tobacco_id"].StringValue = value; }
        }

        public Int32 door_audit
        {
            get { return Instance["door_audit"].IntegerValue; }
            set { Instance["door_audit"].IntegerValue = value; }
        }

        public Int32 official_seal
        {
            get { return Instance["official_seal"].IntegerValue; }
            set { Instance["official_seal"].IntegerValue = value; }
        }

        public Int32 financial_seal
        {
            get { return Instance["financial_seal"].IntegerValue; }
            set { Instance["financial_seal"].IntegerValue = value; }
        }

        public Int32 bussiness_seal
        {
            get { return Instance["bussiness_seal"].IntegerValue; }
            set { Instance["bussiness_seal"].IntegerValue = value; }
        }

        public Int32 door
        {
            get { return Instance["door"].IntegerValue; }
            set { Instance["door"].IntegerValue = value; }
        }

        public String door_spec
        {
            get { return Instance["door_spec"].StringValue; }
            set { Instance["door_spec"].StringValue = value; }
        }

        public Decimal door_amt
        {
            get { return Instance["door_amt"].DecimalValue; }
            set { Instance["door_amt"].DecimalValue = value; }
        }

        public Int32 background_wall
        {
            get { return Instance["background_wall"].IntegerValue; }
            set { Instance["background_wall"].IntegerValue = value; }
        }

        public String wall_spec
        {
            get { return Instance["wall_spec"].StringValue; }
            set { Instance["wall_spec"].StringValue = value; }
        }

        public Decimal wall_amt
        {
            get { return Instance["wall_amt"].DecimalValue; }
            set { Instance["wall_amt"].DecimalValue = value; }
        }

        public Int32 advertiting
        {
            get { return Instance["advertiting"].IntegerValue; }
            set { Instance["advertiting"].IntegerValue = value; }
        }

        public String adv_spec
        {
            get { return Instance["adv_spec"].StringValue; }
            set { Instance["adv_spec"].StringValue = value; }
        }

        public Decimal adv_amt
        {
            get { return Instance["adv_amt"].DecimalValue; }
            set { Instance["adv_amt"].DecimalValue = value; }
        }

        public Int32 effect_pic
        {
            get { return Instance["effect_pic"].IntegerValue; }
            set { Instance["effect_pic"].IntegerValue = value; }
        }

        public String pic_spec
        {
            get { return Instance["pic_spec"].StringValue; }
            set { Instance["pic_spec"].StringValue = value; }
        }

        public Decimal pic_amt
        {
            get { return Instance["pic_amt"].DecimalValue; }
            set { Instance["pic_amt"].DecimalValue = value; }
        }

        public Int32 newwork
        {
            get { return Instance["newwork"].IntegerValue; }
            set { Instance["newwork"].IntegerValue = value; }
        }

        public Int32 purchase
        {
            get { return Instance["purchase"].IntegerValue; }
            set { Instance["purchase"].IntegerValue = value; }
        }

        public Int32 device_deploy
        {
            get { return Instance["device_deploy"].IntegerValue; }
            set { Instance["device_deploy"].IntegerValue = value; }
        }

        public Int32 device_setup
        {
            get { return Instance["device_setup"].IntegerValue; }
            set { Instance["device_setup"].IntegerValue = value; }
        }

        public Int32 system_setup
        {
            get { return Instance["system_setup"].IntegerValue; }
            set { Instance["system_setup"].IntegerValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String updater
        {
            get { return Instance["updater"].StringValue; }
            set { Instance["updater"].StringValue = value; }
        }

        public DateTime? update_date
        {
            get
            {
                if (Instance["update_date"].HasValue)
                    return Instance["update_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["update_date"].Value = null;
                else
                    Instance["update_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effecter
        {
            get { return Instance["effecter"].StringValue; }
            set { Instance["effecter"].StringValue = value; }
        }

        public DateTime? effect_date
        {
            get
            {
                if (Instance["effect_date"].HasValue)
                    return Instance["effect_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effect_date"].Value = null;
                else
                    Instance["effect_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store_admin() : base() { }

        public bm_process_store_admin(Entity instance) : base(instance) { }
        #endregion

    }
}
