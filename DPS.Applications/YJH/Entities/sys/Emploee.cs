﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class Emploee : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.Emploee";

        #region ====Properties====
        public Guid ID
        {
            get { return Instance["ID"].GuidValue; }
            set { Instance["ID"].GuidValue = value; }
        }

        public Guid BaseID
        {
            get { return Instance["BaseID"].GuidValue; }
            set { Instance["BaseID"].GuidValue = value; }
        }

        private sys.Entities.Person _Base;
        public sys.Entities.Person Base
        {
            get
            {
                if (_Base == null)
                {
                    Entity value = Instance["Base"].EntityValue;
                    if (value == null)
                        return null;
                    _Base = new sys.Entities.Person(value);
                }
                return _Base;
            }
            set
            {
                _Base = value;
                if (value == null)
                    Instance["Base"].EntityValue = null;
                else
                    Instance["Base"].EntityValue = _Base.Instance;
            }
        }

        public Guid BusinessUnitID
        {
            get { return Instance["BusinessUnitID"].GuidValue; }
            set { Instance["BusinessUnitID"].GuidValue = value; }
        }

        private sys.Entities.BusinessUnit _BusinessUnit;
        public sys.Entities.BusinessUnit BusinessUnit
        {
            get
            {
                if (_BusinessUnit == null)
                {
                    Entity value = Instance["BusinessUnit"].EntityValue;
                    if (value == null)
                        return null;
                    _BusinessUnit = new sys.Entities.BusinessUnit(value);
                }
                return _BusinessUnit;
            }
            set
            {
                _BusinessUnit = value;
                if (value == null)
                    Instance["BusinessUnit"].EntityValue = null;
                else
                    Instance["BusinessUnit"].EntityValue = _BusinessUnit.Instance;
            }
        }

        public String Account
        {
            get { return Instance["Account"].StringValue; }
            set { Instance["Account"].StringValue = value; }
        }

        public Int32? Sort
        {
            get
            {
                if (Instance["Sort"].HasValue)
                    return Instance["Sort"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Sort"].Value = null;
                else
                    Instance["Sort"].IntegerValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Emploee() : base() { }

        public Emploee(Entity instance) : base(instance) { }
        #endregion
    }
}