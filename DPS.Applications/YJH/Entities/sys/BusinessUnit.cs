﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class BusinessUnit : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.BusinessUnit";

        #region ====Properties====
        public Guid ID
        {
            get { return Instance["ID"].GuidValue; }
            set { Instance["ID"].GuidValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String ShortName
        {
            get { return Instance["ShortName"].StringValue; }
            set { Instance["ShortName"].StringValue = value; }
        }

        public String Tel1
        {
            get { return Instance["Tel1"].StringValue; }
            set { Instance["Tel1"].StringValue = value; }
        }

        public String Fax
        {
            get { return Instance["Fax"].StringValue; }
            set { Instance["Fax"].StringValue = value; }
        }

        public String Address
        {
            get { return Instance["Address"].StringValue; }
            set { Instance["Address"].StringValue = value; }
        }

        public String ZipCode
        {
            get { return Instance["ZipCode"].StringValue; }
            set { Instance["ZipCode"].StringValue = value; }
        }

        public String OrgCode
        {
            get { return Instance["OrgCode"].StringValue; }
            set { Instance["OrgCode"].StringValue = value; }
        }

        public Guid? OrgID
        {
            get
            {
                if (Instance["OrgID"].HasValue)
                    return Instance["OrgID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["OrgID"].Value = null;
                else
                    Instance["OrgID"].GuidValue = value.Value;
            }
        }

        public sys.Enums.OrganizationCompanyType? OrgType
        {
            get
            {
                if (Instance["OrgType"].HasValue)
                    return (sys.Enums.OrganizationCompanyType)Instance["OrgType"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["OrgType"].Value = null;
                else
                    Instance["OrgType"].IntegerValue = (int)value.Value;
            }
        }

        public Int32? Sort
        {
            get
            {
                if (Instance["Sort"].HasValue)
                    return Instance["Sort"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Sort"].Value = null;
                else
                    Instance["Sort"].IntegerValue = value.Value;
            }
        }

        private YJH.Entities.Region _Region;
        public YJH.Entities.Region Region
        {
            get
            {
                if (_Region == null)
                {
                    Entity value = Instance["Region"].EntityValue;
                    if (value == null)
                        return null;
                    _Region = new YJH.Entities.Region(value);
                }
                return _Region;
            }
            set
            {
                _Region = value;
                if (value == null)
                    Instance["Region"].EntityValue = null;
                else
                    Instance["Region"].EntityValue = _Region.Instance;
            }
        }

        public Guid? RegionID
        {
            get
            {
                if (Instance["RegionID"].HasValue)
                    return Instance["RegionID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["RegionID"].Value = null;
                else
                    Instance["RegionID"].GuidValue = value.Value;
            }
        }

        public String URL
        {
            get { return Instance["URL"].StringValue; }
            set { Instance["URL"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public BusinessUnit() : base() { }

        public BusinessUnit(Entity instance) : base(instance) { }
        #endregion
    }
}