﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class RoleFunc : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.RoleFunc";

        #region ====Properties====
        private sys.Entities.Funcs _Funcs;
        public sys.Entities.Funcs Funcs
        {
            get
            {
                if (_Funcs == null)
                {
                    Entity value = Instance["Funcs"].EntityValue;
                    if (value == null)
                        return null;
                    _Funcs = new sys.Entities.Funcs(value);
                }
                return _Funcs;
            }
            set
            {
                _Funcs = value;
                if (value == null)
                    Instance["Funcs"].EntityValue = null;
                else
                    Instance["Funcs"].EntityValue = _Funcs.Instance;
            }
        }

        public Guid FuncsID
        {
            get { return Instance["FuncsID"].GuidValue; }
            set { Instance["FuncsID"].GuidValue = value; }
        }

        private sys.Entities.Roles _Role;
        public sys.Entities.Roles Role
        {
            get
            {
                if (_Role == null)
                {
                    Entity value = Instance["Role"].EntityValue;
                    if (value == null)
                        return null;
                    _Role = new sys.Entities.Roles(value);
                }
                return _Role;
            }
            set
            {
                _Role = value;
                if (value == null)
                    Instance["Role"].EntityValue = null;
                else
                    Instance["Role"].EntityValue = _Role.Instance;
            }
        }

        public Guid RoleID
        {
            get { return Instance["RoleID"].GuidValue; }
            set { Instance["RoleID"].GuidValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public RoleFunc() : base() { }

        public RoleFunc(Entity instance) : base(instance) { }
        #endregion
    }
}