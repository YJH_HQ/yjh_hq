﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class UserFunc : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.UserFunc";

        #region ====Properties====
        private sys.Entities.Funcs _Funcs;
        public sys.Entities.Funcs Funcs
        {
            get
            {
                if (_Funcs == null)
                {
                    Entity value = Instance["Funcs"].EntityValue;
                    if (value == null)
                        return null;
                    _Funcs = new sys.Entities.Funcs(value);
                }
                return _Funcs;
            }
            set
            {
                _Funcs = value;
                if (value == null)
                    Instance["Funcs"].EntityValue = null;
                else
                    Instance["Funcs"].EntityValue = _Funcs.Instance;
            }
        }

        public Guid FuncsID
        {
            get { return Instance["FuncsID"].GuidValue; }
            set { Instance["FuncsID"].GuidValue = value; }
        }

        private sys.Entities.OrgUnit _OrgUnit;
        public sys.Entities.OrgUnit OrgUnit
        {
            get
            {
                if (_OrgUnit == null)
                {
                    Entity value = Instance["OrgUnit"].EntityValue;
                    if (value == null)
                        return null;
                    _OrgUnit = new sys.Entities.OrgUnit(value);
                }
                return _OrgUnit;
            }
            set
            {
                _OrgUnit = value;
                if (value == null)
                    Instance["OrgUnit"].EntityValue = null;
                else
                    Instance["OrgUnit"].EntityValue = _OrgUnit.Instance;
            }
        }

        public Guid OrgUnitID
        {
            get { return Instance["OrgUnitID"].GuidValue; }
            set { Instance["OrgUnitID"].GuidValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public UserFunc() : base() { }

        public UserFunc(Entity instance) : base(instance) { }
        #endregion
    }
}