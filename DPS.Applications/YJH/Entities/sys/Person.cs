﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class Person : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.Person";

        #region ====Properties====
        public Guid ID
        {
            get { return Instance["ID"].GuidValue; }
            set { Instance["ID"].GuidValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public sys.Enums.Gender Gender
        {
            get { return (sys.Enums.Gender)Instance["Gender"].IntegerValue; }
            set { Instance["Gender"].IntegerValue = (int)value; }
        }

        public DateTime? Birthday
        {
            get
            {
                if (Instance["Birthday"].HasValue)
                    return Instance["Birthday"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Birthday"].Value = null;
                else
                    Instance["Birthday"].DateValue = value.Value;
            }
        }

        public String IdentityCardNO
        {
            get { return Instance["IdentityCardNO"].StringValue; }
            set { Instance["IdentityCardNO"].StringValue = value; }
        }

        public String HomeAddress
        {
            get { return Instance["HomeAddress"].StringValue; }
            set { Instance["HomeAddress"].StringValue = value; }
        }

        public String Tel
        {
            get { return Instance["Tel"].StringValue; }
            set { Instance["Tel"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Person() : base() { }

        public Person(Entity instance) : base(instance) { }
        #endregion
    }
}