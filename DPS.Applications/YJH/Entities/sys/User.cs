﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
	public class User : dps.Data.Mapper.EntityBase
	{
		public const string EntityModelID = "sys.User";

		#region ====Properties====
		public String Account
		{
			get { return Instance["Account"].StringValue; }
			set { Instance["Account"].StringValue = value; }
		}

		public Byte[] Password
		{
			get { return Instance["Password"].BinaryValue; }
			set { Instance["Password"].BinaryValue = value; }
		}

		public Byte[] Salt
		{
			get { return Instance["Salt"].BinaryValue; }
			set { Instance["Salt"].BinaryValue = value; }
		}

		private dps.Data.Mapper.EntityList<sys.Entities.UserFunc> _UserFuncs;
		public dps.Data.Mapper.EntityList<sys.Entities.UserFunc> UserFuncs
		{
			get
			{
				if (_UserFuncs == null)
					_UserFuncs = new dps.Data.Mapper.EntityList<sys.Entities.UserFunc>(Instance["UserFuncs"].EntityListValue);
				return _UserFuncs;
			}
		}

		public override string ModelID
		{ get { return EntityModelID; } }

		#endregion

		#region ====Ctor====
		public User() : base() { }

		public User(Entity instance) : base(instance) { }
		#endregion
	}
}
