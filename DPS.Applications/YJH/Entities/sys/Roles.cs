﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class Roles : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.Roles";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Boolean Enabled
        {
            get { return Instance["Enabled"].BooleanValue; }
            set { Instance["Enabled"].BooleanValue = value; }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.RoleFunc> _RoleFuncs;
        public dps.Data.Mapper.EntityList<sys.Entities.RoleFunc> RoleFuncs
        {
            get
            {
                if (_RoleFuncs == null)
                    _RoleFuncs = new dps.Data.Mapper.EntityList<sys.Entities.RoleFunc>(Instance["RoleFuncs"].EntityListValue);
                return _RoleFuncs;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Roles() : base() { }

        public Roles(Entity instance) : base(instance) { }
        #endregion
    }
}