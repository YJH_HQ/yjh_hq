﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class Funcs : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.Funcs";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public sys.Enums.FuncType FuncType
        {
            get { return (sys.Enums.FuncType)Instance["FuncType"].IntegerValue; }
            set { Instance["FuncType"].IntegerValue = (int)value; }
        }

        public Int32 Sort
        {
            get { return Instance["Sort"].IntegerValue; }
            set { Instance["Sort"].IntegerValue = value; }
        }

        private sys.Entities.Funcs _Parent;
        public sys.Entities.Funcs Parent
        {
            get
            {
                if (_Parent == null)
                {
                    Entity value = Instance["Parent"].EntityValue;
                    if (value == null)
                        return null;
                    _Parent = new sys.Entities.Funcs(value);
                }
                return _Parent;
            }
            set
            {
                _Parent = value;
                if (value == null)
                    Instance["Parent"].EntityValue = null;
                else
                    Instance["Parent"].EntityValue = _Parent.Instance;
            }
        }

        public Guid? ParentID
        {
            get
            {
                if (Instance["ParentID"].HasValue)
                    return Instance["ParentID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ParentID"].Value = null;
                else
                    Instance["ParentID"].GuidValue = value.Value;
            }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.Funcs> _SubItems;
        public dps.Data.Mapper.EntityList<sys.Entities.Funcs> SubItems
        {
            get
            {
                if (_SubItems == null)
                    _SubItems = new dps.Data.Mapper.EntityList<sys.Entities.Funcs>(Instance["SubItems"].EntityListValue);
                return _SubItems;
            }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.RoleFunc> _RoleFuncs;
        public dps.Data.Mapper.EntityList<sys.Entities.RoleFunc> RoleFuncs
        {
            get
            {
                if (_RoleFuncs == null)
                    _RoleFuncs = new dps.Data.Mapper.EntityList<sys.Entities.RoleFunc>(Instance["RoleFuncs"].EntityListValue);
                return _RoleFuncs;
            }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.UserFunc> _UserFuncs;
        public dps.Data.Mapper.EntityList<sys.Entities.UserFunc> UserFuncs
        {
            get
            {
                if (_UserFuncs == null)
                    _UserFuncs = new dps.Data.Mapper.EntityList<sys.Entities.UserFunc>(Instance["UserFuncs"].EntityListValue);
                return _UserFuncs;
            }
        }

        public String URL
        {
            get { return Instance["URL"].StringValue; }
            set { Instance["URL"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Funcs() : base() { }

        public Funcs(Entity instance) : base(instance) { }
        #endregion
    }
}