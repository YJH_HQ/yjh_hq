﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class OrgUnit : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.OrgUnit";

        #region ====Properties====
        public Guid ID
        {
            get { return Instance["ID"].GuidValue; }
            set { Instance["ID"].GuidValue = value; }
        }

        public Guid BaseID
        {
            get { return Instance["BaseID"].GuidValue; }
            set { Instance["BaseID"].GuidValue = value; }
        }

        public String BaseType
        {
            get { return Instance["BaseType"].StringValue; }
            set { Instance["BaseType"].StringValue = value; }
        }

        public Object Name
        {
            get { return Instance["Name"].Value; }
        }

        private sys.Entities.OrgUnit _Parent;
        public sys.Entities.OrgUnit Parent
        {
            get
            {
                if (_Parent == null)
                {
                    Entity value = Instance["Parent"].EntityValue;
                    if (value == null)
                        return null;
                    _Parent = new sys.Entities.OrgUnit(value);
                }
                return _Parent;
            }
            set
            {
                _Parent = value;
                if (value == null)
                    Instance["Parent"].EntityValue = null;
                else
                    Instance["Parent"].EntityValue = _Parent.Instance;
            }
        }

        public Guid? ParentID
        {
            get
            {
                if (Instance["ParentID"].HasValue)
                    return Instance["ParentID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ParentID"].Value = null;
                else
                    Instance["ParentID"].GuidValue = value.Value;
            }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.OrgUnit> _SubItems;
        public dps.Data.Mapper.EntityList<sys.Entities.OrgUnit> SubItems
        {
            get
            {
                if (_SubItems == null)
                    _SubItems = new dps.Data.Mapper.EntityList<sys.Entities.OrgUnit>(Instance["SubItems"].EntityListValue);
                return _SubItems;
            }
        }

        private sys.Entities.OrgUnit _Manager;
        public sys.Entities.OrgUnit Manager
        {
            get
            {
                if (_Manager == null)
                {
                    Entity value = Instance["Manager"].EntityValue;
                    if (value == null)
                        return null;
                    _Manager = new sys.Entities.OrgUnit(value);
                }
                return _Manager;
            }
            set
            {
                _Manager = value;
                if (value == null)
                    Instance["Manager"].EntityValue = null;
                else
                    Instance["Manager"].EntityValue = _Manager.Instance;
            }
        }

        public Guid? ManagerID
        {
            get
            {
                if (Instance["ManagerID"].HasValue)
                    return Instance["ManagerID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ManagerID"].Value = null;
                else
                    Instance["ManagerID"].GuidValue = value.Value;
            }
        }

        public Int32 Sort
        {
            get { return Instance["Sort"].IntegerValue; }
            set { Instance["Sort"].IntegerValue = value; }
        }

        public Boolean IsEnabled
        {
            get { return Instance["IsEnabled"].BooleanValue; }
            set { Instance["IsEnabled"].BooleanValue = value; }
        }

        private dps.Data.Mapper.EntityList<sys.Entities.UserFunc> _UserFuncs;
        public dps.Data.Mapper.EntityList<sys.Entities.UserFunc> UserFuncs
        {
            get
            {
                if (_UserFuncs == null)
                    _UserFuncs = new dps.Data.Mapper.EntityList<sys.Entities.UserFunc>(Instance["UserFuncs"].EntityListValue);
                return _UserFuncs;
            }
        }

        public Int32 Hierarchy
        {
            get { return Instance["Hierarchy"].IntegerValue; }
            set { Instance["Hierarchy"].IntegerValue = value; }
        }

        public Boolean? IsBusiness
        {
            get
            {
                if (Instance["IsBusiness"].HasValue)
                    return Instance["IsBusiness"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsBusiness"].Value = null;
                else
                    Instance["IsBusiness"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsAdmin
        {
            get
            {
                if (Instance["IsAdmin"].HasValue)
                    return Instance["IsAdmin"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsAdmin"].Value = null;
                else
                    Instance["IsAdmin"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsPurchase
        {
            get
            {
                if (Instance["IsPurchase"].HasValue)
                    return Instance["IsPurchase"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsPurchase"].Value = null;
                else
                    Instance["IsPurchase"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsDistribution
        {
            get
            {
                if (Instance["IsDistribution"].HasValue)
                    return Instance["IsDistribution"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsDistribution"].Value = null;
                else
                    Instance["IsDistribution"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsFinance
        {
            get
            {
                if (Instance["IsFinance"].HasValue)
                    return Instance["IsFinance"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsFinance"].Value = null;
                else
                    Instance["IsFinance"].BooleanValue = value.Value;
            }
        }

        public Boolean IsDeleted
        {
            get { return Instance["IsDeleted"].BooleanValue; }
            set { Instance["IsDeleted"].BooleanValue = value; }
        }

        public String RoleNumber
        {
            get { return Instance["RoleNumber"].StringValue; }
            set { Instance["RoleNumber"].StringValue = value; }
        }

        /// <summary>
        /// 流水号（insert前先获取该值的最大值，然后加1再绑定）
        /// </summary>
        public Int32 SerialNumber
        {
            get { return Instance["SerialNumber"].IntegerValue; }
            set { Instance["SerialNumber"].IntegerValue = value; }
        }

        public Boolean? IsEntityBU
        {
            get
            {
                if (Instance["IsEntityBU"].HasValue)
                    return Instance["IsEntityBU"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsEntityBU"].Value = null;
                else
                    Instance["IsEntityBU"].BooleanValue = value.Value;
            }
        }

        public Boolean? IsStore
        {
            get
            {
                if (Instance["IsStore"].HasValue)
                    return Instance["IsStore"].BooleanValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["IsStore"].Value = null;
                else
                    Instance["IsStore"].BooleanValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public OrgUnit() : base() { }

        public OrgUnit(Entity instance) : base(instance) { }
        #endregion

        #region extend
        public OrgUnit(BusinessUnit businessUnit)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = businessUnit; }

        public OrgUnit(WorkGroup workgroup)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = workgroup; }

        public OrgUnit(Emploee emploee)
            : base(new Entity("sys.OrgUnit"))
        { this.Base = emploee; }

        private dps.Data.Mapper.EntityBase _base;
        public dps.Data.Mapper.EntityBase Base
        {
            get
            {
                if (_base == null)
                {
                    Entity value = Instance["Base"].EntityValue;
                    if (value == null)
                        return null;

                    switch (value.ModelID)
                    {
                        case "sys.BusinessUnit":
                            _base = new BusinessUnit(value);
                            break;
                        case "sys.WorkGroup":
                            _base = new WorkGroup(value);
                            break;
                        case "sys.Emploee":
                            _base = new Emploee(value);
                            break;
                        default:
                            throw new System.Exception("Invalid Entity Model ID");
                    }
                }
                return _base;
            }
            set
            {
                _base = value;
                if (value == null)
                    Instance["Base"].EntityValue = null;
                else
                    Instance["Base"].EntityValue = _base.Instance;
            }
        }
        #endregion
    }
}