﻿using dps.Common.Data;
using System;

namespace sys.Entities
{
    public class WorkGroup : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "sys.WorkGroup";

        #region ====Properties====
        public Guid BusinessUnitID
        {
            get { return Instance["BusinessUnitID"].GuidValue; }
            set { Instance["BusinessUnitID"].GuidValue = value; }
        }

        private sys.Entities.BusinessUnit _BusinessUnit;
        public sys.Entities.BusinessUnit BusinessUnit
        {
            get
            {
                if (_BusinessUnit == null)
                {
                    Entity value = Instance["BusinessUnit"].EntityValue;
                    if (value == null)
                        return null;
                    _BusinessUnit = new sys.Entities.BusinessUnit(value);
                }
                return _BusinessUnit;
            }
            set
            {
                _BusinessUnit = value;
                if (value == null)
                    Instance["BusinessUnit"].EntityValue = null;
                else
                    Instance["BusinessUnit"].EntityValue = _BusinessUnit.Instance;
            }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Int32? Sort
        {
            get
            {
                if (Instance["Sort"].HasValue)
                    return Instance["Sort"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Sort"].Value = null;
                else
                    Instance["Sort"].IntegerValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public WorkGroup() : base() { }

        public WorkGroup(Entity instance) : base(instance) { }
        #endregion
    }
}
