﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ba_district : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ba_district";

        #region ====Properties====
        public Int32 ownerid
        {
            get { return Instance["ownerid"].IntegerValue; }
            set { Instance["ownerid"].IntegerValue = value; }
        }

        public String city_name
        {
            get { return Instance["city_name"].StringValue; }
            set { Instance["city_name"].StringValue = value; }
        }

        public Int32 city_level
        {
            get { return Instance["city_level"].IntegerValue; }
            set { Instance["city_level"].IntegerValue = value; }
        }

        public Int32? upid
        {
            get
            {
                if (Instance["upid"].HasValue)
                    return Instance["upid"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["upid"].Value = null;
                else
                    Instance["upid"].IntegerValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ba_district() : base() { }

        public ba_district(Entity instance) : base(instance) { }
        #endregion

    }
}
