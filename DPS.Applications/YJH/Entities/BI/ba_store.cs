﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ba_store : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ba_store";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String store_name
        {
            get { return Instance["store_name"].StringValue; }
            set { Instance["store_name"].StringValue = value; }
        }

        public Int32? store_type
        {
            get
            {
                if (Instance["store_type"].HasValue)
                    return Instance["store_type"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_type"].Value = null;
                else
                    Instance["store_type"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_order
        {
            get
            {
                if (Instance["store_is_order"].HasValue)
                    return Instance["store_is_order"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_order"].Value = null;
                else
                    Instance["store_is_order"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_distribution
        {
            get
            {
                if (Instance["store_is_distribution"].HasValue)
                    return Instance["store_is_distribution"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_distribution"].Value = null;
                else
                    Instance["store_is_distribution"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_return
        {
            get
            {
                if (Instance["store_is_return"].HasValue)
                    return Instance["store_is_return"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_return"].Value = null;
                else
                    Instance["store_is_return"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_income
        {
            get
            {
                if (Instance["store_is_income"].HasValue)
                    return Instance["store_is_income"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_income"].Value = null;
                else
                    Instance["store_is_income"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_inventory
        {
            get
            {
                if (Instance["store_is_inventory"].HasValue)
                    return Instance["store_is_inventory"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_inventory"].Value = null;
                else
                    Instance["store_is_inventory"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_stock
        {
            get
            {
                if (Instance["store_is_stock"].HasValue)
                    return Instance["store_is_stock"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_stock"].Value = null;
                else
                    Instance["store_is_stock"].IntegerValue = value.Value;
            }
        }

        public Int32? store_is_pos
        {
            get
            {
                if (Instance["store_is_pos"].HasValue)
                    return Instance["store_is_pos"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_is_pos"].Value = null;
                else
                    Instance["store_is_pos"].IntegerValue = value.Value;
            }
        }

        public String store_group_contract
        {
            get { return Instance["store_group_contract"].StringValue; }
            set { Instance["store_group_contract"].StringValue = value; }
        }

        public String store_group_ems
        {
            get { return Instance["store_group_ems"].StringValue; }
            set { Instance["store_group_ems"].StringValue = value; }
        }

        public String store_group_servies
        {
            get { return Instance["store_group_servies"].StringValue; }
            set { Instance["store_group_servies"].StringValue = value; }
        }

        public String store_group_price
        {
            get { return Instance["store_group_price"].StringValue; }
            set { Instance["store_group_price"].StringValue = value; }
        }

        public String store_parent_org
        {
            get { return Instance["store_parent_org"].StringValue; }
            set { Instance["store_parent_org"].StringValue = value; }
        }

        public String store_business_org
        {
            get { return Instance["store_business_org"].StringValue; }
            set { Instance["store_business_org"].StringValue = value; }
        }

        public Byte? store_issms
        {
            get
            {
                if (Instance["store_issms"].HasValue)
                    return Instance["store_issms"].ByteValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_issms"].Value = null;
                else
                    Instance["store_issms"].ByteValue = value.Value;
            }
        }

        public String store_tel
        {
            get { return Instance["store_tel"].StringValue; }
            set { Instance["store_tel"].StringValue = value; }
        }

        public String store_fixedtel
        {
            get { return Instance["store_fixedtel"].StringValue; }
            set { Instance["store_fixedtel"].StringValue = value; }
        }

        public String store_address
        {
            get { return Instance["store_address"].StringValue; }
            set { Instance["store_address"].StringValue = value; }
        }

        public String store_zipcode
        {
            get { return Instance["store_zipcode"].StringValue; }
            set { Instance["store_zipcode"].StringValue = value; }
        }

        public String store_buslicensecode
        {
            get { return Instance["store_buslicensecode"].StringValue; }
            set { Instance["store_buslicensecode"].StringValue = value; }
        }

        public Int32? store_employeecount
        {
            get
            {
                if (Instance["store_employeecount"].HasValue)
                    return Instance["store_employeecount"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["store_employeecount"].Value = null;
                else
                    Instance["store_employeecount"].IntegerValue = value.Value;
            }
        }

        public String store_busstarttime
        {
            get { return Instance["store_busstarttime"].StringValue; }
            set { Instance["store_busstarttime"].StringValue = value; }
        }

        public String store_busendtime
        {
            get { return Instance["store_busendtime"].StringValue; }
            set { Instance["store_busendtime"].StringValue = value; }
        }

        public Guid? BUID
        {
            get
            {
                if (Instance["BUID"].HasValue)
                    return Instance["BUID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["BUID"].Value = null;
                else
                    Instance["BUID"].GuidValue = value.Value;
            }
        }

        public YJH.Enums.StoreState State
        {
            get { return (YJH.Enums.StoreState)Instance["State"].IntegerValue; }
            set { Instance["State"].IntegerValue = (int)value; }
        }

        public String AuditBy
        {
            get { return Instance["AuditBy"].StringValue; }
            set { Instance["AuditBy"].StringValue = value; }
        }

        public DateTime? AuditDate
        {
            get
            {
                if (Instance["AuditDate"].HasValue)
                    return Instance["AuditDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["AuditDate"].Value = null;
                else
                    Instance["AuditDate"].DateValue = value.Value;
            }
        }

        public String versionnumber
        {
            get { return Instance["versionnumber"].StringValue; }
            set { Instance["versionnumber"].StringValue = value; }
        }

        public Int32? data_check
        {
            get
            {
                if (Instance["data_check"].HasValue)
                    return Instance["data_check"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["data_check"].Value = null;
                else
                    Instance["data_check"].IntegerValue = value.Value;
            }
        }
        public String province
        {
            get { return Instance["province"].StringValue; }
            set { Instance["province"].StringValue = value; }
        }

        public String city
        {
            get { return Instance["city"].StringValue; }
            set { Instance["city"].StringValue = value; }
        }

        public String area
        {
            get { return Instance["area"].StringValue; }
            set { Instance["area"].StringValue = value; }
        }

        public String street
        {
            get { return Instance["street"].StringValue; }
            set { Instance["street"].StringValue = value; }
        }

        public String baidulon
        {
            get { return Instance["baidulon"].StringValue; }
            set { Instance["baidulon"].StringValue = value; }
        }

        public String baidulat
        {
            get { return Instance["baidulat"].StringValue; }
            set { Instance["baidulat"].StringValue = value; }
        }

        public String marslon
        {
            get { return Instance["marslon"].StringValue; }
            set { Instance["marslon"].StringValue = value; }
        }

        public String marslat
        {
            get { return Instance["marslat"].StringValue; }
            set { Instance["marslat"].StringValue = value; }
        }
        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ba_store() : base() { }

        public ba_store(Entity instance) : base(instance) { }
        #endregion

    }
}
