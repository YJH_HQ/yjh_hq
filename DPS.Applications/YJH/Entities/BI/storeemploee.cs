﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class storeemploee : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.storeemploee";

        #region ====Properties====
        private YJH.Entities.hr_emploee _hr_emploee;
        public YJH.Entities.hr_emploee hr_emploee
        {
            get
            {
                if (_hr_emploee == null)
                {
                    Entity value = Instance["hr_emploee"].EntityValue;
                    if (value == null)
                        return null;
                    _hr_emploee = new YJH.Entities.hr_emploee(value);
                }
                return _hr_emploee;
            }
            set
            {
                _hr_emploee = value;
                if (value == null)
                    Instance["hr_emploee"].EntityValue = null;
                else
                    Instance["hr_emploee"].EntityValue = _hr_emploee.Instance;
            }
        }

        public Guid hr_emploeeID
        {
            get { return Instance["hr_emploeeID"].GuidValue; }
            set { Instance["hr_emploeeID"].GuidValue = value; }
        }

        private YJH.Entities.ba_store _ba_store;
        public YJH.Entities.ba_store ba_store
        {
            get
            {
                if (_ba_store == null)
                {
                    Entity value = Instance["ba_store"].EntityValue;
                    if (value == null)
                        return null;
                    _ba_store = new YJH.Entities.ba_store(value);
                }
                return _ba_store;
            }
            set
            {
                _ba_store = value;
                if (value == null)
                    Instance["ba_store"].EntityValue = null;
                else
                    Instance["ba_store"].EntityValue = _ba_store.Instance;
            }
        }

        public Guid ba_storeID
        {
            get { return Instance["ba_storeID"].GuidValue; }
            set { Instance["ba_storeID"].GuidValue = value; }
        }

        private YJH.Entities.hr_job1 _hr_job;
        public YJH.Entities.hr_job1 hr_job
        {
            get
            {
                if (_hr_job == null)
                {
                    Entity value = Instance["hr_job"].EntityValue;
                    if (value == null)
                        return null;
                    _hr_job = new YJH.Entities.hr_job1(value);
                }
                return _hr_job;
            }
            set
            {
                _hr_job = value;
                if (value == null)
                    Instance["hr_job"].EntityValue = null;
                else
                    Instance["hr_job"].EntityValue = _hr_job.Instance;
            }
        }

        public Guid hr_jobID
        {
            get { return Instance["hr_jobID"].GuidValue; }
            set { Instance["hr_jobID"].GuidValue = value; }
        }

        public Boolean IsHome
        {
            get { return Instance["IsHome"].BooleanValue; }
            set { Instance["IsHome"].BooleanValue = value; }
        }

        public Int32 send_status
        {
            get { return Instance["send_status"].IntegerValue; }
            set { Instance["send_status"].IntegerValue = value; }
        }

        public DateTime? send_date
        {
            get
            {
                if (Instance["send_date"].HasValue)
                    return Instance["send_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_date"].Value = null;
                else
                    Instance["send_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public storeemploee() : base() { }

        public storeemploee(Entity instance) : base(instance) { }
        #endregion

    }
}
