﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ba_class : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ba_class";

        #region ====Properties====
        public Decimal item_order_id
        {
            get { return Instance["item_order_id"].DecimalValue; }
            set { Instance["item_order_id"].DecimalValue = value; }
        }

        public Int32 item_level
        {
            get { return Instance["item_level"].IntegerValue; }
            set { Instance["item_level"].IntegerValue = value; }
        }

        public Int32 item_no
        {
            get { return Instance["item_no"].IntegerValue; }
            set { Instance["item_no"].IntegerValue = value; }
        }

        public String class_id
        {
            get { return Instance["class_id"].StringValue; }
            set { Instance["class_id"].StringValue = value; }
        }

        public String class_name
        {
            get { return Instance["class_name"].StringValue; }
            set { Instance["class_name"].StringValue = value; }
        }

        public Int32 class_additional
        {
            get { return Instance["class_additional"].IntegerValue; }
            set { Instance["class_additional"].IntegerValue = value; }
        }

        public String item_parent
        {
            get { return Instance["item_parent"].StringValue; }
            set { Instance["item_parent"].StringValue = value; }
        }

        public Int32 item_isfinal
        {
            get { return Instance["item_isfinal"].IntegerValue; }
            set { Instance["item_isfinal"].IntegerValue = value; }
        }

        public Int32 item_type
        {
            get { return Instance["item_type"].IntegerValue; }
            set { Instance["item_type"].IntegerValue = value; }
        }

        public Int32 item_status
        {
            get { return Instance["item_status"].IntegerValue; }
            set { Instance["item_status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ba_class() : base() { }

        public ba_class(Entity instance) : base(instance) { }
        #endregion

    }
}
