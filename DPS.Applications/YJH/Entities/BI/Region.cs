﻿using dps.Common.Data;
using System;

namespace YJH.Entities
{
    public class Region : dps.Data.Mapper.EntityBase
    {
        public const string EntityModelID = "YJH.Region";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String Code
        {
            get { return Instance["Code"].StringValue; }
            set { Instance["Code"].StringValue = value; }
        }

        public YJH.Enums.RegionScope? Scope
        {
            get
            {
                if (Instance["Scope"].HasValue)
                    return (YJH.Enums.RegionScope)Instance["Scope"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Scope"].Value = null;
                else
                    Instance["Scope"].IntegerValue = (int)value.Value;
            }
        }

        public Int32 Sort
        {
            get { return Instance["Sort"].IntegerValue; }
            set { Instance["Sort"].IntegerValue = value; }
        }

        private YJH.Entities.Region _Parent;
        public YJH.Entities.Region Parent
        {
            get
            {
                if (_Parent == null)
                {
                    Entity value = Instance["Parent"].EntityValue;
                    if (value == null)
                        return null;
                    _Parent = new YJH.Entities.Region(value);
                }
                return _Parent;
            }
            set
            {
                _Parent = value;
                if (value == null)
                    Instance["Parent"].EntityValue = null;
                else
                    Instance["Parent"].EntityValue = _Parent.Instance;
            }
        }

        public Guid? ParentID
        {
            get
            {
                if (Instance["ParentID"].HasValue)
                    return Instance["ParentID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["ParentID"].Value = null;
                else
                    Instance["ParentID"].GuidValue = value.Value;
            }
        }

        private dps.Data.Mapper.EntityList<YJH.Entities.Region> _SubRegions;
        public dps.Data.Mapper.EntityList<YJH.Entities.Region> SubRegions
        {
            get
            {
                if (_SubRegions == null)
                    _SubRegions = new dps.Data.Mapper.EntityList<YJH.Entities.Region>(Instance["SubRegions"].EntityListValue);
                return _SubRegions;
            }
        }

        public Boolean Enabled
        {
            get { return Instance["Enabled"].BooleanValue; }
            set { Instance["Enabled"].BooleanValue = value; }
        }

        public String FullName
        {
            get { return Instance["FullName"].StringValue; }
            set { Instance["FullName"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public Region() : base() { }

        public Region(Entity instance) : base(instance) { }
        #endregion
    }
}