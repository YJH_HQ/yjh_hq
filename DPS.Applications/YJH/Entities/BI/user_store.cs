﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class user_store : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.user_store";

        #region ====Properties====
        public String user_id
        {
            get { return Instance["user_id"].StringValue; }
            set { Instance["user_id"].StringValue = value; }
        }

        public String user_name
        {
            get { return Instance["user_name"].StringValue; }
            set { Instance["user_name"].StringValue = value; }
        }

        public String user_pass
        {
            get { return Instance["user_pass"].StringValue; }
            set { Instance["user_pass"].StringValue = value; }
        }

        public Int32 user_job
        {
            get { return Instance["user_job"].IntegerValue; }
            set { Instance["user_job"].IntegerValue = value; }
        }

        public Int32 user_status
        {
            get { return Instance["user_status"].IntegerValue; }
            set { Instance["user_status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public user_store() : base() { }

        public user_store(Entity instance) : base(instance) { }
        #endregion

    }
}
