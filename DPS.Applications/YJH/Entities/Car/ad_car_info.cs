﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_car_info : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_car_info";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String licence_no
        {
            get { return Instance["licence_no"].StringValue; }
            set { Instance["licence_no"].StringValue = value; }
        }

        public String brand
        {
            get { return Instance["brand"].StringValue; }
            set { Instance["brand"].StringValue = value; }
        }

        public Int32 type
        {
            get { return Instance["type"].IntegerValue; }
            set { Instance["type"].IntegerValue = value; }
        }

        public String engine
        {
            get { return Instance["engine"].StringValue; }
            set { Instance["engine"].StringValue = value; }
        }

        public DateTime reg_date
        {
            get { return Instance["reg_date"].DateValue; }
            set { Instance["reg_date"].DateValue = value; }
        }

        public Int32 persons
        {
            get { return Instance["persons"].IntegerValue; }
            set { Instance["persons"].IntegerValue = value; }
        }

        public String color
        {
            get { return Instance["color"].StringValue; }
            set { Instance["color"].StringValue = value; }
        }

        public String liabler
        {
            get { return Instance["liabler"].StringValue; }
            set { Instance["liabler"].StringValue = value; }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_car_info() : base() { }

        public ad_car_info(Entity instance) : base(instance) { }
        #endregion

    }
}
