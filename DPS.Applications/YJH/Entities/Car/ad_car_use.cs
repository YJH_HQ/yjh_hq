﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_car_use : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_car_use";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String use_no
        {
            get { return Instance["use_no"].StringValue; }
            set { Instance["use_no"].StringValue = value; }
        }

        public DateTime use_date
        {
            get { return Instance["use_date"].DateValue; }
            set { Instance["use_date"].DateValue = value; }
        }

        public String licence_no
        {
            get { return Instance["licence_no"].StringValue; }
            set { Instance["licence_no"].StringValue = value; }
        }

        public String dest
        {
            get { return Instance["dest"].StringValue; }
            set { Instance["dest"].StringValue = value; }
        }

        public String forWhat
        {
            get { return Instance["forWhat"].StringValue; }
            set { Instance["forWhat"].StringValue = value; }
        }

        public Int32 type
        {
            get { return Instance["type"].IntegerValue; }
            set { Instance["type"].IntegerValue = value; }
        }

        public DateTime start_time
        {
            get { return Instance["start_time"].DateValue; }
            set { Instance["start_time"].DateValue = value; }
        }

        public Decimal start_km
        {
            get { return Instance["start_km"].DecimalValue; }
            set { Instance["start_km"].DecimalValue = value; }
        }

        public DateTime? end_time
        {
            get
            {
                if (Instance["end_time"].HasValue)
                    return Instance["end_time"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["end_time"].Value = null;
                else
                    Instance["end_time"].DateValue = value.Value;
            }
        }

        public Decimal? end_km
        {
            get
            {
                if (Instance["end_km"].HasValue)
                    return Instance["end_km"].DecimalValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["end_km"].Value = null;
                else
                    Instance["end_km"].DecimalValue = value.Value;
            }
        }

        public Decimal travel_fee
        {
            get { return Instance["travel_fee"].DecimalValue; }
            set { Instance["travel_fee"].DecimalValue = value; }
        }

        public String driver
        {
            get { return Instance["driver"].StringValue; }
            set { Instance["driver"].StringValue = value; }
        }

        public String other
        {
            get { return Instance["other"].StringValue; }
            set { Instance["other"].StringValue = value; }
        }

        public String remark
        {
            get { return Instance["remark"].StringValue; }
            set { Instance["remark"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_car_use() : base() { }

        public ad_car_use(Entity instance) : base(instance) { }
        #endregion

    }
}
