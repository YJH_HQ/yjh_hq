﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_car_oil : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_car_oil";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public DateTime use_date
        {
            get { return Instance["use_date"].DateValue; }
            set { Instance["use_date"].DateValue = value; }
        }

        public String licence_no
        {
            get { return Instance["licence_no"].StringValue; }
            set { Instance["licence_no"].StringValue = value; }
        }

        public Decimal oil_km
        {
            get { return Instance["oil_km"].DecimalValue; }
            set { Instance["oil_km"].DecimalValue = value; }
        }

        public Decimal oil_qty
        {
            get { return Instance["oil_qty"].DecimalValue; }
            set { Instance["oil_qty"].DecimalValue = value; }
        }

        public Int32 oil_type
        {
            get { return Instance["oil_type"].IntegerValue; }
            set { Instance["oil_type"].IntegerValue = value; }
        }

        public Decimal oil_price
        {
            get { return Instance["oil_price"].DecimalValue; }
            set { Instance["oil_price"].DecimalValue = value; }
        }

        public Decimal oil_amt
        {
            get { return Instance["oil_amt"].DecimalValue; }
            set { Instance["oil_amt"].DecimalValue = value; }
        }

        public String operater
        {
            get { return Instance["operater"].StringValue; }
            set { Instance["operater"].StringValue = value; }
        }

        public Int32 type
        {
            get { return Instance["type"].IntegerValue; }
            set { Instance["type"].IntegerValue = value; }
        }

        public String remark
        {
            get { return Instance["remark"].StringValue; }
            set { Instance["remark"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_car_oil() : base() { }

        public ad_car_oil(Entity instance) : base(instance) { }
        #endregion

    }
}
