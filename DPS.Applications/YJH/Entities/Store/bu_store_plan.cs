﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bu_store_plan : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bu_store_plan";

        #region ====Properties====
        public String plan_id
        {
            get { return Instance["plan_id"].StringValue; }
            set { Instance["plan_id"].StringValue = value; }
        }

        public Int32 plan_month
        {
            get { return Instance["plan_month"].IntegerValue; }
            set { Instance["plan_month"].IntegerValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public DateTime? open_date
        {
            get
            {
                if (Instance["open_date"].HasValue)
                    return Instance["open_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["open_date"].Value = null;
                else
                    Instance["open_date"].DateValue = value.Value;
            }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bu_store_plan() : base() { }

        public bu_store_plan(Entity instance) : base(instance) { }
        #endregion

    }
}
