﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_schedule_type : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_schedule_type";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String class_id
        {
            get { return Instance["class_id"].StringValue; }
            set { Instance["class_id"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_schedule_type() : base() { }

        public bm_schedule_type(Entity instance) : base(instance) { }
        #endregion

    }
}
