﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_user : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_user";

        #region ====Properties====
        public String user_id
        {
            get { return Instance["user_id"].StringValue; }
            set { Instance["user_id"].StringValue = value; }
        }

        public String process_id
        {
            get { return Instance["process_id"].StringValue; }
            set { Instance["process_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_user() : base() { }

        public bm_process_user(Entity instance) : base(instance) { }
        #endregion

    }
}
