﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class bm_process_store : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.bm_process_store";

        #region ====Properties====
        public Decimal process_order_id
        {
            get { return Instance["process_order_id"].DecimalValue; }
            set { Instance["process_order_id"].DecimalValue = value; }
        }

        public Int32 process_level
        {
            get { return Instance["process_level"].IntegerValue; }
            set { Instance["process_level"].IntegerValue = value; }
        }

        public Int32 process_no
        {
            get { return Instance["process_no"].IntegerValue; }
            set { Instance["process_no"].IntegerValue = value; }
        }

        public String process_id
        {
            get { return Instance["process_id"].StringValue; }
            set { Instance["process_id"].StringValue = value; }
        }

        public String process_name
        {
            get { return Instance["process_name"].StringValue; }
            set { Instance["process_name"].StringValue = value; }
        }

        public String process_parent
        {
            get { return Instance["process_parent"].StringValue; }
            set { Instance["process_parent"].StringValue = value; }
        }

        public Int32 process_isfinal
        {
            get { return Instance["process_isfinal"].IntegerValue; }
            set { Instance["process_isfinal"].IntegerValue = value; }
        }

        public Int32 process_type
        {
            get { return Instance["process_type"].IntegerValue; }
            set { Instance["process_type"].IntegerValue = value; }
        }

        public Int32 process_status
        {
            get { return Instance["process_status"].IntegerValue; }
            set { Instance["process_status"].IntegerValue = value; }
        }

        public Int32 process_additional
        {
            get { return Instance["process_additional"].IntegerValue; }
            set { Instance["process_additional"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public bm_process_store() : base() { }

        public bm_process_store(Entity instance) : base(instance) { }
        #endregion

    }
}
