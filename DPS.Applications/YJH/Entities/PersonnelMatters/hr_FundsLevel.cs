﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_FundsLevel : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_FundsLevel";

        #region ====Properties====
        public Guid companyId
        {
            get { return Instance["companyId"].GuidValue; }
            set { Instance["companyId"].GuidValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Decimal Base
        {
            get { return Instance["Base"].DecimalValue; }
            set { Instance["Base"].DecimalValue = value; }
        }

        public Boolean IsDisabled
        {
            get { return Instance["IsDisabled"].BooleanValue; }
            set { Instance["IsDisabled"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_FundsLevel() : base() { }

        public hr_FundsLevel(Entity instance) : base(instance) { }
        #endregion

    }
}
