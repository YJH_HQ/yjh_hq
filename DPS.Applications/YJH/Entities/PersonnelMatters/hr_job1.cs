﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_job1 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_job1";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Boolean IsDisabled
        {
            get { return Instance["IsDisabled"].BooleanValue; }
            set { Instance["IsDisabled"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_job1() : base() { }

        public hr_job1(Entity instance) : base(instance) { }
        #endregion

    }
}
