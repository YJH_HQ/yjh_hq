﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_InsuranceLevel : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_InsuranceLevel";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Decimal Base
        {
            get { return Instance["Base"].DecimalValue; }
            set { Instance["Base"].DecimalValue = value; }
        }

        public Boolean IsDisabled
        {
            get { return Instance["IsDisabled"].BooleanValue; }
            set { Instance["IsDisabled"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_InsuranceLevel() : base() { }

        public hr_InsuranceLevel(Entity instance) : base(instance) { }
        #endregion

    }
}
