﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_emp_salary : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_emp_salary";

        #region ====Properties====
        public Guid EmploeeId
        {
            get { return Instance["EmploeeId"].GuidValue; }
            set { Instance["EmploeeId"].GuidValue = value; }
        }

        public Int32 Type
        {
            get { return Instance["Type"].IntegerValue; }
            set { Instance["Type"].IntegerValue = value; }
        }

        public Decimal wageProbation
        {
            get { return Instance["wageProbation"].DecimalValue; }
            set { Instance["wageProbation"].DecimalValue = value; }
        }

        public Decimal wageBasic
        {
            get { return Instance["wageBasic"].DecimalValue; }
            set { Instance["wageBasic"].DecimalValue = value; }
        }

        public Decimal wageKPIPer
        {
            get { return Instance["wageKPIPer"].DecimalValue; }
            set { Instance["wageKPIPer"].DecimalValue = value; }
        }

        public Boolean IsInsurance
        {
            get { return Instance["IsInsurance"].BooleanValue; }
            set { Instance["IsInsurance"].BooleanValue = value; }
        }

        public String InsuranceNumber
        {
            get { return Instance["InsuranceNumber"].StringValue; }
            set { Instance["InsuranceNumber"].StringValue = value; }
        }

        public Int32 InsuranceLevel
        {
            get { return Instance["InsuranceLevel"].IntegerValue; }
            set { Instance["InsuranceLevel"].IntegerValue = value; }
        }

        public Boolean IsFunds
        {
            get { return Instance["IsFunds"].BooleanValue; }
            set { Instance["IsFunds"].BooleanValue = value; }
        }

        public String FundsNumber
        {
            get { return Instance["FundsNumber"].StringValue; }
            set { Instance["FundsNumber"].StringValue = value; }
        }

        public Int32 FundsLevel
        {
            get { return Instance["FundsLevel"].IntegerValue; }
            set { Instance["FundsLevel"].IntegerValue = value; }
        }

        public Decimal sub_comm
        {
            get { return Instance["sub_comm"].DecimalValue; }
            set { Instance["sub_comm"].DecimalValue = value; }
        }

        public Decimal sub_traffic
        {
            get { return Instance["sub_traffic"].DecimalValue; }
            set { Instance["sub_traffic"].DecimalValue = value; }
        }

        public Decimal sub_meals
        {
            get { return Instance["sub_meals"].DecimalValue; }
            set { Instance["sub_meals"].DecimalValue = value; }
        }

        public Decimal sub_house
        {
            get { return Instance["sub_house"].DecimalValue; }
            set { Instance["sub_house"].DecimalValue = value; }
        }

        public Decimal sub_vehicle
        {
            get { return Instance["sub_vehicle"].DecimalValue; }
            set { Instance["sub_vehicle"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_emp_salary() : base() { }

        public hr_emp_salary(Entity instance) : base(instance) { }
        #endregion

    }
}
