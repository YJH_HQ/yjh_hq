﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_job_adj1 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_job_adj1";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public Guid EmploeeId
        {
            get { return Instance["EmploeeId"].GuidValue; }
            set { Instance["EmploeeId"].GuidValue = value; }
        }

        public Guid DeptId
        {
            get { return Instance["DeptId"].GuidValue; }
            set { Instance["DeptId"].GuidValue = value; }
        }

        public Guid? job_id
        {
            get
            {
                if (Instance["job_id"].HasValue)
                    return Instance["job_id"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["job_id"].Value = null;
                else
                    Instance["job_id"].GuidValue = value.Value;
            }
        }

        public String Remark
        {
            get { return Instance["Remark"].StringValue; }
            set { Instance["Remark"].StringValue = value; }
        }

        public DateTime DateEffect
        {
            get { return Instance["DateEffect"].DateValue; }
            set { Instance["DateEffect"].DateValue = value; }
        }

        public Guid creater
        {
            get { return Instance["creater"].GuidValue; }
            set { Instance["creater"].GuidValue = value; }
        }


        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_job_adj1() : base() { }

        public hr_job_adj1(Entity instance) : base(instance) { }
        #endregion

    }
}
