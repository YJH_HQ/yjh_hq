﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_contract1 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_contract1";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public Guid EmploeeId
        {
            get { return Instance["EmploeeId"].GuidValue; }
            set { Instance["EmploeeId"].GuidValue = value; }
        }

        public Int32 Type
        {
            get { return Instance["Type"].IntegerValue; }
            set { Instance["Type"].IntegerValue = value; }
        }

        public DateTime? DateStart
        {
            get
            {
                if (Instance["DateStart"].HasValue)
                    return Instance["DateStart"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["DateStart"].Value = null;
                else
                    Instance["DateStart"].DateValue = value.Value;
            }
        }

        public DateTime? DateEnd
        {
            get
            {
                if (Instance["DateEnd"].HasValue)
                    return Instance["DateEnd"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["DateEnd"].Value = null;
                else
                    Instance["DateEnd"].DateValue = value.Value;
            }
        }

        public DateTime? DateDestroy
        {
            get
            {
                if (Instance["DateDestroy"].HasValue)
                    return Instance["DateDestroy"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["DateDestroy"].Value = null;
                else
                    Instance["DateDestroy"].DateValue = value.Value;
            }
        }

        public Decimal ContractSalary
        {
            get { return Instance["ContractSalary"].DecimalValue; }
            set { Instance["ContractSalary"].DecimalValue = value; }
        }

        public String jobType
        {
            get { return Instance["jobType"].StringValue; }
            set { Instance["jobType"].StringValue = value; }
        }

        public DateTime? DateSign
        {
            get
            {
                if (Instance["DateSign"].HasValue)
                    return Instance["DateSign"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["DateSign"].Value = null;
                else
                    Instance["DateSign"].DateValue = value.Value;
            }
        }

        public String Remark
        {
            get { return Instance["Remark"].StringValue; }
            set { Instance["Remark"].StringValue = value; }
        }

        public Int32 Status
        {
            get { return Instance["Status"].IntegerValue; }
            set { Instance["Status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_contract1() : base() { }

        public hr_contract1(Entity instance) : base(instance) { }
        #endregion

    }
}
