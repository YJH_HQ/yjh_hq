﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_salary_adj1 : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_salary_adj1";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public Guid EmploeeId
        {
            get { return Instance["EmploeeId"].GuidValue; }
            set { Instance["EmploeeId"].GuidValue = value; }
        }

        public Guid DeptId
        {
            get { return Instance["DeptId"].GuidValue; }
            set { Instance["DeptId"].GuidValue = value; }
        }

        public Guid? Job_id
        {
            get
            {
                if (Instance["Job_id"].HasValue)
                    return Instance["Job_id"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Job_id"].Value = null;
                else
                    Instance["Job_id"].GuidValue = value.Value;
            }
        }

        public Decimal wageBasic
        {
            get { return Instance["wageBasic"].DecimalValue; }
            set { Instance["wageBasic"].DecimalValue = value; }
        }

        public Decimal wageKPIPer
        {
            get { return Instance["wageKPIPer"].DecimalValue; }
            set { Instance["wageKPIPer"].DecimalValue = value; }
        }

        public Decimal sub_comm
        {
            get { return Instance["sub_comm"].DecimalValue; }
            set { Instance["sub_comm"].DecimalValue = value; }
        }

        public Decimal sub_traffic
        {
            get { return Instance["sub_traffic"].DecimalValue; }
            set { Instance["sub_traffic"].DecimalValue = value; }
        }

        public Decimal sub_meals
        {
            get { return Instance["sub_meals"].DecimalValue; }
            set { Instance["sub_meals"].DecimalValue = value; }
        }

        public Decimal sub_house
        {
            get { return Instance["sub_house"].DecimalValue; }
            set { Instance["sub_house"].DecimalValue = value; }
        }

        public Decimal sub_vehicle
        {
            get { return Instance["sub_vehicle"].DecimalValue; }
            set { Instance["sub_vehicle"].DecimalValue = value; }
        }

        public Int32 InsuranceLevel
        {
            get { return Instance["InsuranceLevel"].IntegerValue; }
            set { Instance["InsuranceLevel"].IntegerValue = value; }
        }

        public Int32 FundsLevel
        {
            get { return Instance["FundsLevel"].IntegerValue; }
            set { Instance["FundsLevel"].IntegerValue = value; }
        }

        public String Remark
        {
            get { return Instance["Remark"].StringValue; }
            set { Instance["Remark"].StringValue = value; }
        }

        public DateTime DateEffect
        {
            get { return Instance["DateEffect"].DateValue; }
            set { Instance["DateEffect"].DateValue = value; }
        }

        public Guid creater
        {
            get { return Instance["creater"].GuidValue; }
            set { Instance["creater"].GuidValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_salary_adj1() : base() { }

        public hr_salary_adj1(Entity instance) : base(instance) { }
        #endregion

    }
}
