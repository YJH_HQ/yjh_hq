﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class hr_emploee : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.hr_emploee";

        #region ====Properties====
        public String Number
        {
            get { return Instance["Number"].StringValue; }
            set { Instance["Number"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Int32 Sex
        {
            get { return Instance["Sex"].IntegerValue; }
            set { Instance["Sex"].IntegerValue = value; }
        }

        public String Tel
        {
            get { return Instance["Tel"].StringValue; }
            set { Instance["Tel"].StringValue = value; }
        }

        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public Guid Dept_id
        {
            get { return Instance["Dept_id"].GuidValue; }
            set { Instance["Dept_id"].GuidValue = value; }
        }

        public Guid? Job_id
        {
            get
            {
                if (Instance["Job_id"].HasValue)
                    return Instance["Job_id"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["Job_id"].Value = null;
                else
                    Instance["Job_id"].GuidValue = value.Value;
            }
        }

        public Int32 Type
        {
            get { return Instance["Type"].IntegerValue; }
            set { Instance["Type"].IntegerValue = value; }
        }

        public Int32 Status
        {
            get { return Instance["Status"].IntegerValue; }
            set { Instance["Status"].IntegerValue = value; }
        }

        public Int32 Polity
        {
            get { return Instance["Polity"].IntegerValue; }
            set { Instance["Polity"].IntegerValue = value; }
        }

        public Int32 Degree
        {
            get { return Instance["Degree"].IntegerValue; }
            set { Instance["Degree"].IntegerValue = value; }
        }

        public String School
        {
            get { return Instance["School"].StringValue; }
            set { Instance["School"].StringValue = value; }
        }

        public DateTime? jobFirstDate
        {
            get
            {
                if (Instance["jobFirstDate"].HasValue)
                    return Instance["jobFirstDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobFirstDate"].Value = null;
                else
                    Instance["jobFirstDate"].DateValue = value.Value;
            }
        }

        public DateTime? jobJoinDate
        {
            get
            {
                if (Instance["jobJoinDate"].HasValue)
                    return Instance["jobJoinDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobJoinDate"].Value = null;
                else
                    Instance["jobJoinDate"].DateValue = value.Value;
            }
        }

        public DateTime? jobTrialDate
        {
            get
            {
                if (Instance["jobTrialDate"].HasValue)
                    return Instance["jobTrialDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobTrialDate"].Value = null;
                else
                    Instance["jobTrialDate"].DateValue = value.Value;
            }
        }

        public DateTime? jobFormalDate
        {
            get
            {
                if (Instance["jobFormalDate"].HasValue)
                    return Instance["jobFormalDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobFormalDate"].Value = null;
                else
                    Instance["jobFormalDate"].DateValue = value.Value;
            }
        }

        public DateTime? jobLeaveApplyDate
        {
            get
            {
                if (Instance["jobLeaveApplyDate"].HasValue)
                    return Instance["jobLeaveApplyDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobLeaveApplyDate"].Value = null;
                else
                    Instance["jobLeaveApplyDate"].DateValue = value.Value;
            }
        }

        public DateTime? jobLeaveDate
        {
            get
            {
                if (Instance["jobLeaveDate"].HasValue)
                    return Instance["jobLeaveDate"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["jobLeaveDate"].Value = null;
                else
                    Instance["jobLeaveDate"].DateValue = value.Value;
            }
        }

        public String jobLeaveReason
        {
            get { return Instance["jobLeaveReason"].StringValue; }
            set { Instance["jobLeaveReason"].StringValue = value; }
        }

        public String IdentityId
        {
            get { return Instance["IdentityId"].StringValue; }
            set { Instance["IdentityId"].StringValue = value; }
        }

        public String Native
        {
            get { return Instance["Native"].StringValue; }
            set { Instance["Native"].StringValue = value; }
        }

        public Int32 NativeType
        {
            get { return Instance["NativeType"].IntegerValue; }
            set { Instance["NativeType"].IntegerValue = value; }
        }

        public String addrNative
        {
            get { return Instance["addrNative"].StringValue; }
            set { Instance["addrNative"].StringValue = value; }
        }

        public String addrLive
        {
            get { return Instance["addrLive"].StringValue; }
            set { Instance["addrLive"].StringValue = value; }
        }

        public String RemarkOfFamily
        {
            get { return Instance["RemarkOfFamily"].StringValue; }
            set { Instance["RemarkOfFamily"].StringValue = value; }
        }

        public Int32 jobLeaveClass
        {
            get { return Instance["jobLeaveClass"].IntegerValue; }
            set { Instance["jobLeaveClass"].IntegerValue = value; }
        }

        public Int32 jobLeaveType
        {
            get { return Instance["jobLeaveType"].IntegerValue; }
            set { Instance["jobLeaveType"].IntegerValue = value; }
        }

        public Guid creater
        {
            get { return Instance["creater"].GuidValue; }
            set { Instance["creater"].GuidValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public Boolean IsCreateAccount
        {
            get { return Instance["IsCreateAccount"].BooleanValue; }
            set { Instance["IsCreateAccount"].BooleanValue = value; }
        }

        public String Professional
        {
            get { return Instance["Professional"].StringValue; }
            set { Instance["Professional"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public hr_emploee() : base() { }

        public hr_emploee(Entity instance) : base(instance) { }
        #endregion

    }
}
