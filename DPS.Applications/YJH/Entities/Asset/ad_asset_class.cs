﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_class : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_class";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String class_id
        {
            get { return Instance["class_id"].StringValue; }
            set { Instance["class_id"].StringValue = value; }
        }

        public String class_name
        {
            get { return Instance["class_name"].StringValue; }
            set { Instance["class_name"].StringValue = value; }
        }

        private YJH.Entities.ad_asset_class _parent_id;
        public YJH.Entities.ad_asset_class parent_id
        {
            get
            {
                if (_parent_id == null)
                {
                    Entity value = Instance["parent_id"].EntityValue;
                    if (value == null)
                        return null;
                    _parent_id = new YJH.Entities.ad_asset_class(value);
                }
                return _parent_id;
            }
            set
            {
                _parent_id = value;
                if (value == null)
                    Instance["parent_id"].EntityValue = null;
                else
                    Instance["parent_id"].EntityValue = _parent_id.Instance;
            }
        }

        public Guid? parent_idID
        {
            get
            {
                if (Instance["parent_idID"].HasValue)
                    return Instance["parent_idID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["parent_idID"].Value = null;
                else
                    Instance["parent_idID"].GuidValue = value.Value;
            }
        }

        public Int32? status
        {
            get
            {
                if (Instance["status"].HasValue)
                    return Instance["status"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["status"].Value = null;
                else
                    Instance["status"].IntegerValue = value.Value;
            }
        }

        public Boolean IsDisabled
        {
            get { return Instance["IsDisabled"].BooleanValue; }
            set { Instance["IsDisabled"].BooleanValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_class() : base() { }

        public ad_asset_class(Entity instance) : base(instance) { }
        #endregion

    }
}
