﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_manage : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_manage";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String class_id
        {
            get { return Instance["class_id"].StringValue; }
            set { Instance["class_id"].StringValue = value; }
        }

        public String spec
        {
            get { return Instance["spec"].StringValue; }
            set { Instance["spec"].StringValue = value; }
        }

        public Decimal price
        {
            get { return Instance["price"].DecimalValue; }
            set { Instance["price"].DecimalValue = value; }
        }

        public String sup_name
        {
            get { return Instance["sup_name"].StringValue; }
            set { Instance["sup_name"].StringValue = value; }
        }

        public String item_no
        {
            get { return Instance["item_no"].StringValue; }
            set { Instance["item_no"].StringValue = value; }
        }

        public DateTime reg_date
        {
            get { return Instance["reg_date"].DateValue; }
            set { Instance["reg_date"].DateValue = value; }
        }

        public String keeper
        {
            get { return Instance["keeper"].StringValue; }
            set { Instance["keeper"].StringValue = value; }
        }

        public Guid keep_dept
        {
            get { return Instance["keep_dept"].GuidValue; }
            set { Instance["keep_dept"].GuidValue = value; }
        }

        public Guid? parent_id
        {
            get
            {
                if (Instance["parent_id"].HasValue)
                    return Instance["parent_id"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["parent_id"].Value = null;
                else
                    Instance["parent_id"].GuidValue = value.Value;
            }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public DateTime? destroy_date
        {
            get
            {
                if (Instance["destroy_date"].HasValue)
                    return Instance["destroy_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["destroy_date"].Value = null;
                else
                    Instance["destroy_date"].DateValue = value.Value;
            }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_manage() : base() { }

        public ad_asset_manage(Entity instance) : base(instance) { }
        #endregion

    }
}
