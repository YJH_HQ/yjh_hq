﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_send : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_send";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String send_id
        {
            get { return Instance["send_id"].StringValue; }
            set { Instance["send_id"].StringValue = value; }
        }

        public String item_id
        {
            get { return Instance["item_id"].StringValue; }
            set { Instance["item_id"].StringValue = value; }
        }

        public Decimal send_qty
        {
            get { return Instance["send_qty"].DecimalValue; }
            set { Instance["send_qty"].DecimalValue = value; }
        }

        public DateTime send_date
        {
            get { return Instance["send_date"].DateValue; }
            set { Instance["send_date"].DateValue = value; }
        }

        public Guid get_dept
        {
            get { return Instance["get_dept"].GuidValue; }
            set { Instance["get_dept"].GuidValue = value; }
        }

        public String geter
        {
            get { return Instance["geter"].StringValue; }
            set { Instance["geter"].StringValue = value; }
        }

        public String forWhat
        {
            get { return Instance["forWhat"].StringValue; }
            set { Instance["forWhat"].StringValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_send() : base() { }

        public ad_asset_send(Entity instance) : base(instance) { }
        #endregion

    }
}
