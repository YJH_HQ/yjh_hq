﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_stock : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_stock";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String rece_id
        {
            get { return Instance["rece_id"].StringValue; }
            set { Instance["rece_id"].StringValue = value; }
        }

        public String item_id
        {
            get { return Instance["item_id"].StringValue; }
            set { Instance["item_id"].StringValue = value; }
        }

        public Decimal stock
        {
            get { return Instance["stock"].DecimalValue; }
            set { Instance["stock"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_stock() : base() { }

        public ad_asset_stock(Entity instance) : base(instance) { }
        #endregion

    }
}
