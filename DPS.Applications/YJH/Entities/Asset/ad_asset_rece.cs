﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_rece : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_rece";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String rece_id
        {
            get { return Instance["rece_id"].StringValue; }
            set { Instance["rece_id"].StringValue = value; }
        }

        public String item_id
        {
            get { return Instance["item_id"].StringValue; }
            set { Instance["item_id"].StringValue = value; }
        }

        public String sup_id
        {
            get { return Instance["sup_id"].StringValue; }
            set { Instance["sup_id"].StringValue = value; }
        }

        public Int32 rece_type
        {
            get { return Instance["rece_type"].IntegerValue; }
            set { Instance["rece_type"].IntegerValue = value; }
        }

        public String order_unit
        {
            get { return Instance["order_unit"].StringValue; }
            set { Instance["order_unit"].StringValue = value; }
        }

        public String warehouse
        {
            get { return Instance["warehouse"].StringValue; }
            set { Instance["warehouse"].StringValue = value; }
        }

        public Decimal rece_qty
        {
            get { return Instance["rece_qty"].DecimalValue; }
            set { Instance["rece_qty"].DecimalValue = value; }
        }

        public Decimal rece_price
        {
            get { return Instance["rece_price"].DecimalValue; }
            set { Instance["rece_price"].DecimalValue = value; }
        }

        public Decimal rece_amt
        {
            get { return Instance["rece_amt"].DecimalValue; }
            set { Instance["rece_amt"].DecimalValue = value; }
        }

        public DateTime? rece_date
        {
            get
            {
                if (Instance["rece_date"].HasValue)
                    return Instance["rece_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["rece_date"].Value = null;
                else
                    Instance["rece_date"].DateValue = value.Value;
            }
        }

        public Int32 status
        {
            get { return Instance["status"].IntegerValue; }
            set { Instance["status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_rece() : base() { }

        public ad_asset_rece(Entity instance) : base(instance) { }
        #endregion

    }
}
