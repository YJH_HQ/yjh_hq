﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class ad_asset_item : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.ad_asset_item";

        #region ====Properties====
        public Guid Company_id
        {
            get { return Instance["Company_id"].GuidValue; }
            set { Instance["Company_id"].GuidValue = value; }
        }

        public String item_id
        {
            get { return Instance["item_id"].StringValue; }
            set { Instance["item_id"].StringValue = value; }
        }

        public String class_id
        {
            get { return Instance["class_id"].StringValue; }
            set { Instance["class_id"].StringValue = value; }
        }

        public String item_name
        {
            get { return Instance["item_name"].StringValue; }
            set { Instance["item_name"].StringValue = value; }
        }

        public String spec
        {
            get { return Instance["spec"].StringValue; }
            set { Instance["spec"].StringValue = value; }
        }

        public Boolean IsDisabled
        {
            get { return Instance["IsDisabled"].BooleanValue; }
            set { Instance["IsDisabled"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public ad_asset_item() : base() { }

        public ad_asset_item(Entity instance) : base(instance) { }
        #endregion

    }
}
