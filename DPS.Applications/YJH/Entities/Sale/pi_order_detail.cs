﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class pi_order_detail : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.pi_order_detail";

        #region ====Properties====
        public String OrderNo
        {
            get { return Instance["OrderNo"].StringValue; }
            set { Instance["OrderNo"].StringValue = value; }
        }

        public String Number
        {
            get { return Instance["Number"].StringValue; }
            set { Instance["Number"].StringValue = value; }
        }

        public String ProductNumber
        {
            get { return Instance["ProductNumber"].StringValue; }
            set { Instance["ProductNumber"].StringValue = value; }
        }

        public Decimal Bid
        {
            get { return Instance["Bid"].DecimalValue; }
            set { Instance["Bid"].DecimalValue = value; }
        }

        public Decimal Count
        {
            get { return Instance["Count"].DecimalValue; }
            set { Instance["Count"].DecimalValue = value; }
        }

        public Decimal Price
        {
            get { return Instance["Price"].DecimalValue; }
            set { Instance["Price"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public pi_order_detail() : base() { }

        public pi_order_detail(Entity instance) : base(instance) { }
        #endregion

    }
}
