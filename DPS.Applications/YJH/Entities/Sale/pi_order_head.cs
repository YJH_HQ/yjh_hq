﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class pi_order_head : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.pi_order_head";

        #region ====Properties====
        public String OrderNo
        {
            get { return Instance["OrderNo"].StringValue; }
            set { Instance["OrderNo"].StringValue = value; }
        }

        public Int32 Type
        {
            get { return Instance["Type"].IntegerValue; }
            set { Instance["Type"].IntegerValue = value; }
        }

        public String Company
        {
            get { return Instance["Company"].StringValue; }
            set { Instance["Company"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String VendorNo
        {
            get { return Instance["VendorNo"].StringValue; }
            set { Instance["VendorNo"].StringValue = value; }
        }

        public String PurchaseNo
        {
            get { return Instance["PurchaseNo"].StringValue; }
            set { Instance["PurchaseNo"].StringValue = value; }
        }

        public Decimal Scale
        {
            get { return Instance["Scale"].DecimalValue; }
            set { Instance["Scale"].DecimalValue = value; }
        }

        public DateTime DeliveryDate
        {
            get { return Instance["DeliveryDate"].DateValue; }
            set { Instance["DeliveryDate"].DateValue = value; }
        }

        public DateTime OrderDate
        {
            get { return Instance["OrderDate"].DateValue; }
            set { Instance["OrderDate"].DateValue = value; }
        }

        public Decimal Count
        {
            get { return Instance["Count"].DecimalValue; }
            set { Instance["Count"].DecimalValue = value; }
        }

        public Decimal Price
        {
            get { return Instance["Price"].DecimalValue; }
            set { Instance["Price"].DecimalValue = value; }
        }

        public String Create
        {
            get { return Instance["Create"].StringValue; }
            set { Instance["Create"].StringValue = value; }
        }

        public DateTime CreateTime
        {
            get { return Instance["CreateTime"].DateValue; }
            set { Instance["CreateTime"].DateValue = value; }
        }

        public String OperateBy
        {
            get { return Instance["OperateBy"].StringValue; }
            set { Instance["OperateBy"].StringValue = value; }
        }

        public DateTime? OperateTime
        {
            get
            {
                if (Instance["OperateTime"].HasValue)
                    return Instance["OperateTime"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["OperateTime"].Value = null;
                else
                    Instance["OperateTime"].DateValue = value.Value;
            }
        }

        public String Audit
        {
            get { return Instance["Audit"].StringValue; }
            set { Instance["Audit"].StringValue = value; }
        }

        public DateTime? AuditTime
        {
            get
            {
                if (Instance["AuditTime"].HasValue)
                    return Instance["AuditTime"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["AuditTime"].Value = null;
                else
                    Instance["AuditTime"].DateValue = value.Value;
            }
        }

        public String effectiver
        {
            get { return Instance["effectiver"].StringValue; }
            set { Instance["effectiver"].StringValue = value; }
        }

        public DateTime? effective_date
        {
            get
            {
                if (Instance["effective_date"].HasValue)
                    return Instance["effective_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effective_date"].Value = null;
                else
                    Instance["effective_date"].DateValue = value.Value;
            }
        }

        public Int32? send_status
        {
            get
            {
                if (Instance["send_status"].HasValue)
                    return Instance["send_status"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_status"].Value = null;
                else
                    Instance["send_status"].IntegerValue = value.Value;
            }
        }

        public DateTime? send_date
        {
            get
            {
                if (Instance["send_date"].HasValue)
                    return Instance["send_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_date"].Value = null;
                else
                    Instance["send_date"].DateValue = value.Value;
            }
        }

        public Int32 State
        {
            get { return Instance["State"].IntegerValue; }
            set { Instance["State"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public pi_order_head() : base() { }

        public pi_order_head(Entity instance) : base(instance) { }
        #endregion

    }
}
