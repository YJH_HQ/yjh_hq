﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_DayBalance : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_DayBalance";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String ExpressCom
        {
            get { return Instance["ExpressCom"].StringValue; }
            set { Instance["ExpressCom"].StringValue = value; }
        }

        public Int32 ReceiveNum
        {
            get { return Instance["ReceiveNum"].IntegerValue; }
            set { Instance["ReceiveNum"].IntegerValue = value; }
        }

        public Int32 DeliverNum
        {
            get { return Instance["DeliverNum"].IntegerValue; }
            set { Instance["DeliverNum"].IntegerValue = value; }
        }

        public Int32 DeliverStock
        {
            get { return Instance["DeliverStock"].IntegerValue; }
            set { Instance["DeliverStock"].IntegerValue = value; }
        }

        public Int32 SendNum
        {
            get { return Instance["SendNum"].IntegerValue; }
            set { Instance["SendNum"].IntegerValue = value; }
        }

        public Int32 AssociateNum
        {
            get { return Instance["AssociateNum"].IntegerValue; }
            set { Instance["AssociateNum"].IntegerValue = value; }
        }

        public Int32 SendStock
        {
            get { return Instance["SendStock"].IntegerValue; }
            set { Instance["SendStock"].IntegerValue = value; }
        }

        public DateTime BalanceDate
        {
            get { return Instance["BalanceDate"].DateValue; }
            set { Instance["BalanceDate"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_DayBalance() : base() { }

        public T_DayBalance(Entity instance) : base(instance) { }
        #endregion

    }
}

