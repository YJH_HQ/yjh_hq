﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_Community : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_Community";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String Number
        {
            get { return Instance["Number"].StringValue; }
            set { Instance["Number"].StringValue = value; }
        }

        public String Address
        {
            get { return Instance["Address"].StringValue; }
            set { Instance["Address"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_Community() : base() { }

        public T_Community(Entity instance) : base(instance) { }
        #endregion

    }
}


