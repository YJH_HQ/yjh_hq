﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_DeliverExpress : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_DeliverExpress";

        #region ====Properties====
        public String CumName
        {
            get { return Instance["CumName"].StringValue; }
            set { Instance["CumName"].StringValue = value; }
        }

        public String CumNum
        {
            get { return Instance["CumNum"].StringValue; }
            set { Instance["CumNum"].StringValue = value; }
        }

        public String CumAddress
        {
            get { return Instance["CumAddress"].StringValue; }
            set { Instance["CumAddress"].StringValue = value; }
        }

        public String ReceiveNO
        {
            get { return Instance["ReceiveNO"].StringValue; }
            set { Instance["ReceiveNO"].StringValue = value; }
        }

        public String Receiver
        {
            get { return Instance["Receiver"].StringValue; }
            set { Instance["Receiver"].StringValue = value; }
        }

        public String ReceiveTel
        {
            get { return Instance["ReceiveTel"].StringValue; }
            set { Instance["ReceiveTel"].StringValue = value; }
        }

        public DateTime ReceiveDate
        {
            get { return Instance["ReceiveDate"].DateValue; }
            set { Instance["ReceiveDate"].DateValue = value; }
        }

        public Decimal ExpressFee
        {
            get { return Instance["ExpressFee"].DecimalValue; }
            set { Instance["ExpressFee"].DecimalValue = value; }
        }

        public String ExpressCompany
        {
            get { return Instance["ExpressCompany"].StringValue; }
            set { Instance["ExpressCompany"].StringValue = value; }
        }

        public String Deliverier
        {
            get { return Instance["Deliverier"].StringValue; }
            set { Instance["Deliverier"].StringValue = value; }
        }

        public String DeliverDate
        {
            get { return Instance["DeliverDate"].StringValue; }
            set { Instance["DeliverDate"].StringValue = value; }
        }

        public Guid OperateBy
        {
            get { return Instance["OperateBy"].GuidValue; }
            set { Instance["OperateBy"].GuidValue = value; }
        }

        public DateTime OperateDate
        {
            get { return Instance["OperateDate"].DateValue; }
            set { Instance["OperateDate"].DateValue = value; }
        }

        public Int32 State
        {
            get { return Instance["State"].IntegerValue; }
            set { Instance["State"].IntegerValue = value; }
        }

        public String DeliveryRemarks
        {
            get { return Instance["DeliveryRemarks"].StringValue; }
            set { Instance["DeliveryRemarks"].StringValue = value; }
        }

        public DateTime ModifyDate
        {
            get { return Instance["ModifyDate"].DateValue; }
            set { Instance["ModifyDate"].DateValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_DeliverExpress() : base() { }

        public T_DeliverExpress(Entity instance) : base(instance) { }
        #endregion

    }
}

