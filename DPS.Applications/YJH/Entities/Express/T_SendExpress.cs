﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_SendExpress : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_SendExpress";

        #region ====Properties====
        public String CumName
        {
            get { return Instance["CumName"].StringValue; }
            set { Instance["CumName"].StringValue = value; }
        }

        public String CumNum
        {
            get { return Instance["CumNum"].StringValue; }
            set { Instance["CumNum"].StringValue = value; }
        }

        public String CumAddress
        {
            get { return Instance["CumAddress"].StringValue; }
            set { Instance["CumAddress"].StringValue = value; }
        }

        public String SendNO
        {
            get { return Instance["SendNO"].StringValue; }
            set { Instance["SendNO"].StringValue = value; }
        }

        public String Sender
        {
            get { return Instance["Sender"].StringValue; }
            set { Instance["Sender"].StringValue = value; }
        }

        public String SenderTel
        {
            get { return Instance["SenderTel"].StringValue; }
            set { Instance["SenderTel"].StringValue = value; }
        }

        public Decimal ExpressFee
        {
            get { return Instance["ExpressFee"].DecimalValue; }
            set { Instance["ExpressFee"].DecimalValue = value; }
        }

        public String ExpressCompany
        {
            get { return Instance["ExpressCompany"].StringValue; }
            set { Instance["ExpressCompany"].StringValue = value; }
        }

        public String Courier
        {
            get { return Instance["Courier"].StringValue; }
            set { Instance["Courier"].StringValue = value; }
        }

        public DateTime SendDate
        {
            get { return Instance["SendDate"].DateValue; }
            set { Instance["SendDate"].DateValue = value; }
        }

        public String AssociateDate
        {
            get { return Instance["AssociateDate"].StringValue; }
            set { Instance["AssociateDate"].StringValue = value; }
        }

        public Guid OperateBy
        {
            get { return Instance["OperateBy"].GuidValue; }
            set { Instance["OperateBy"].GuidValue = value; }
        }

        public DateTime OperateDate
        {
            get { return Instance["OperateDate"].DateValue; }
            set { Instance["OperateDate"].DateValue = value; }
        }

        public Int32 State
        {
            get { return Instance["State"].IntegerValue; }
            set { Instance["State"].IntegerValue = value; }
        }

        public DateTime ModifyDate
        {
            get { return Instance["ModifyDate"].DateValue; }
            set { Instance["ModifyDate"].DateValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_SendExpress() : base() { }

        public T_SendExpress(Entity instance) : base(instance) { }
        #endregion

    }
}

