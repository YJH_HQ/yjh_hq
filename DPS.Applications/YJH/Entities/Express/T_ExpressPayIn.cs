﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_ExpressPayIn : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_ExpressPayIn";

        #region ====Properties====
        public DateTime PayTime
        {
            get { return Instance["PayTime"].DateValue; }
            set { Instance["PayTime"].DateValue = value; }
        }

        public Decimal PayMoney
        {
            get { return Instance["PayMoney"].DecimalValue; }
            set { Instance["PayMoney"].DecimalValue = value; }
        }

        public Decimal HandlingCharge
        {
            get { return Instance["HandlingCharge"].DecimalValue; }
            set { Instance["HandlingCharge"].DecimalValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public String ExpressCompany
        {
            get { return Instance["ExpressCompany"].StringValue; }
            set { Instance["ExpressCompany"].StringValue = value; }
        }

        public Guid OperateBy
        {
            get { return Instance["OperateBy"].GuidValue; }
            set { Instance["OperateBy"].GuidValue = value; }
        }

        public DateTime OperateDate
        {
            get { return Instance["OperateDate"].DateValue; }
            set { Instance["OperateDate"].DateValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_ExpressPayIn() : base() { }

        public T_ExpressPayIn(Entity instance) : base(instance) { }
        #endregion

    }
}


