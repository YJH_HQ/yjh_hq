﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_Expresscompany : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_Expresscompany";

        #region ====Properties====
        public String ems_id
        {
            get { return Instance["ems_id"].StringValue; }
            set { Instance["ems_id"].StringValue = value; }
        }

        public String ems_name
        {
            get { return Instance["ems_name"].StringValue; }
            set { Instance["ems_name"].StringValue = value; }
        }

        public Int32 ems_enable
        {
            get { return Instance["ems_enable"].IntegerValue; }
            set { Instance["ems_enable"].IntegerValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_Expresscompany() : base() { }

        public T_Expresscompany(Entity instance) : base(instance) { }
        #endregion

    }
}
