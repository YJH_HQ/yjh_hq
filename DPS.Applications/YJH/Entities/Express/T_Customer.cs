﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_Customer : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_Customer";

        #region ====Properties====
        public String CumName
        {
            get { return Instance["CumName"].StringValue; }
            set { Instance["CumName"].StringValue = value; }
        }

        public String CumNum
        {
            get { return Instance["CumNum"].StringValue; }
            set { Instance["CumNum"].StringValue = value; }
        }

        public String CumAddress
        {
            get { return Instance["CumAddress"].StringValue; }
            set { Instance["CumAddress"].StringValue = value; }
        }

        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public String Tel
        {
            get { return Instance["Tel"].StringValue; }
            set { Instance["Tel"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_Customer() : base() { }

        public T_Customer(Entity instance) : base(instance) { }
        #endregion

    }
}

