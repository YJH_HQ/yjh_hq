﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class SendStoreTable : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.SendStoreTable";

        #region ====Properties====
        public String StoreID
        {
            get { return Instance["StoreID"].StringValue; }
            set { Instance["StoreID"].StringValue = value; }
        }

        private YJH.Entities.SendTables _SendTables;
        public YJH.Entities.SendTables SendTables
        {
            get
            {
                if (_SendTables == null)
                {
                    Entity value = Instance["SendTables"].EntityValue;
                    if (value == null)
                        return null;
                    _SendTables = new YJH.Entities.SendTables(value);
                }
                return _SendTables;
            }
            set
            {
                _SendTables = value;
                if (value == null)
                    Instance["SendTables"].EntityValue = null;
                else
                    Instance["SendTables"].EntityValue = _SendTables.Instance;
            }
        }

        public Guid? SendTablesID
        {
            get
            {
                if (Instance["SendTablesID"].HasValue)
                    return Instance["SendTablesID"].GuidValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["SendTablesID"].Value = null;
                else
                    Instance["SendTablesID"].GuidValue = value.Value;
            }
        }

        public Int32 send_status
        {
            get { return Instance["send_status"].IntegerValue; }
            set { Instance["send_status"].IntegerValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public SendStoreTable() : base() { }

        public SendStoreTable(Entity instance) : base(instance) { }
        #endregion

    }
}
