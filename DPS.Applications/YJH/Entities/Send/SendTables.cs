﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class SendTables : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.SendTables";

        #region ====Properties====
        public String Name
        {
            get { return Instance["Name"].StringValue; }
            set { Instance["Name"].StringValue = value; }
        }

        public Boolean Enabled
        {
            get { return Instance["Enabled"].BooleanValue; }
            set { Instance["Enabled"].BooleanValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public SendTables() : base() { }

        public SendTables(Entity instance) : base(instance) { }
        #endregion

    }
}
