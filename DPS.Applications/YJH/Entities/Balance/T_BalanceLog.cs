﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_BalanceLog : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_BalanceLog";

        #region ====Properties====
        public String Module
        {
            get { return Instance["Module"].StringValue; }
            set { Instance["Module"].StringValue = value; }
        }

        public DateTime StartTime
        {
            get { return Instance["StartTime"].DateValue; }
            set { Instance["StartTime"].DateValue = value; }
        }

        public DateTime EndTime
        {
            get { return Instance["EndTime"].DateValue; }
            set { Instance["EndTime"].DateValue = value; }
        }

        public String Result
        {
            get { return Instance["Result"].StringValue; }
            set { Instance["Result"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_BalanceLog() : base() { }

        public T_BalanceLog(Entity instance) : base(instance) { }
        #endregion

    }
}

