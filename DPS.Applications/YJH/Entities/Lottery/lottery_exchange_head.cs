﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class lottery_exchange_head : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.lottery_exchange_head";

        #region ====Properties====
        public String leh_store_id
        {
            get { return Instance["leh_store_id"].StringValue; }
            set { Instance["leh_store_id"].StringValue = value; }
        }

        public String leh_exchange_id
        {
            get { return Instance["leh_exchange_id"].StringValue; }
            set { Instance["leh_exchange_id"].StringValue = value; }
        }

        public Decimal leh_exchange_num_total
        {
            get { return Instance["leh_exchange_num_total"].DecimalValue; }
            set { Instance["leh_exchange_num_total"].DecimalValue = value; }
        }

        public Decimal leh_exchange_amt_total
        {
            get { return Instance["leh_exchange_amt_total"].DecimalValue; }
            set { Instance["leh_exchange_amt_total"].DecimalValue = value; }
        }

        public String leh_company
        {
            get { return Instance["leh_company"].StringValue; }
            set { Instance["leh_company"].StringValue = value; }
        }

        public Int32 leh_status
        {
            get { return Instance["leh_status"].IntegerValue; }
            set { Instance["leh_status"].IntegerValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String aduiter
        {
            get { return Instance["aduiter"].StringValue; }
            set { Instance["aduiter"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String effectiver
        {
            get { return Instance["effectiver"].StringValue; }
            set { Instance["effectiver"].StringValue = value; }
        }

        public DateTime? effective_date
        {
            get
            {
                if (Instance["effective_date"].HasValue)
                    return Instance["effective_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effective_date"].Value = null;
                else
                    Instance["effective_date"].DateValue = value.Value;
            }
        }

        public Int32? send_status
        {
            get
            {
                if (Instance["send_status"].HasValue)
                    return Instance["send_status"].IntegerValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_status"].Value = null;
                else
                    Instance["send_status"].IntegerValue = value.Value;
            }
        }

        public DateTime? send_date
        {
            get
            {
                if (Instance["send_date"].HasValue)
                    return Instance["send_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_date"].Value = null;
                else
                    Instance["send_date"].DateValue = value.Value;
            }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public lottery_exchange_head() : base() { }

        public lottery_exchange_head(Entity instance) : base(instance) { }
        #endregion

    }
}
