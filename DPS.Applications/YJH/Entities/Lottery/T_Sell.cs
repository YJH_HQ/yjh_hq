﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_Sell : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_Sell";

        #region ====Properties====
        public DateTime SellTime
        {
            get { return Instance["SellTime"].DateValue; }
            set { Instance["SellTime"].DateValue = value; }
        }

        public Decimal SellMoney
        {
            get { return Instance["SellMoney"].DecimalValue; }
            set { Instance["SellMoney"].DecimalValue = value; }
        }

        public String StoreNO
        {
            get { return Instance["StoreNO"].StringValue; }
            set { Instance["StoreNO"].StringValue = value; }
        }

        public String PrintType
        {
            get { return Instance["PrintType"].StringValue; }
            set { Instance["PrintType"].StringValue = value; }
        }

        public String LotteryIssue
        {
            get { return Instance["LotteryIssue"].StringValue; }
            set { Instance["LotteryIssue"].StringValue = value; }
        }

        public Guid MakeBy
        {
            get { return Instance["MakeBy"].GuidValue; }
            set { Instance["MakeBy"].GuidValue = value; }
        }

        public DateTime MakeTime
        {
            get { return Instance["MakeTime"].DateValue; }
            set { Instance["MakeTime"].DateValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_Sell() : base() { }

        public T_Sell(Entity instance) : base(instance) { }
        #endregion

    }
}

