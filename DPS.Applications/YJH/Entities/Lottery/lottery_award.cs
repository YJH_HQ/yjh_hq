﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class lottery_award : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.lottery_award";

        #region ====Properties====
        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public Decimal bonuses_return_amt
        {
            get { return Instance["bonuses_return_amt"].DecimalValue; }
            set { Instance["bonuses_return_amt"].DecimalValue = value; }
        }

        public Int32 lottery_class
        {
            get { return Instance["lottery_class"].IntegerValue; }
            set { Instance["lottery_class"].IntegerValue = value; }
        }

        public Int32 lottery_type
        {
            get { return Instance["lottery_type"].IntegerValue; }
            set { Instance["lottery_type"].IntegerValue = value; }
        }

        public DateTime create_date
        {
            get { return Instance["create_date"].DateValue; }
            set { Instance["create_date"].DateValue = value; }
        }

        public String creater
        {
            get { return Instance["creater"].StringValue; }
            set { Instance["creater"].StringValue = value; }
        }

        public DateTime? update_date
        {
            get
            {
                if (Instance["update_date"].HasValue)
                    return Instance["update_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["update_date"].Value = null;
                else
                    Instance["update_date"].DateValue = value.Value;
            }
        }

        public String updater
        {
            get { return Instance["updater"].StringValue; }
            set { Instance["updater"].StringValue = value; }
        }

        public DateTime? audit_date
        {
            get
            {
                if (Instance["audit_date"].HasValue)
                    return Instance["audit_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["audit_date"].Value = null;
                else
                    Instance["audit_date"].DateValue = value.Value;
            }
        }

        public String auditer
        {
            get { return Instance["auditer"].StringValue; }
            set { Instance["auditer"].StringValue = value; }
        }

        public DateTime? effective_date
        {
            get
            {
                if (Instance["effective_date"].HasValue)
                    return Instance["effective_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["effective_date"].Value = null;
                else
                    Instance["effective_date"].DateValue = value.Value;
            }
        }

        public String effectiver
        {
            get { return Instance["effectiver"].StringValue; }
            set { Instance["effectiver"].StringValue = value; }
        }

        public Int32 send_status
        {
            get { return Instance["send_status"].IntegerValue; }
            set { Instance["send_status"].IntegerValue = value; }
        }

        public Int32 entity_status
        {
            get { return Instance["entity_status"].IntegerValue; }
            set { Instance["entity_status"].IntegerValue = value; }
        }

        public DateTime? send_date
        {
            get
            {
                if (Instance["send_date"].HasValue)
                    return Instance["send_date"].DateValue;
                else
                    return null;
            }
            set
            {
                if (value == null)
                    Instance["send_date"].Value = null;
                else
                    Instance["send_date"].DateValue = value.Value;
            }
        }

        public String lottery_account
        {
            get { return Instance["lottery_account"].StringValue; }
            set { Instance["lottery_account"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public lottery_award() : base() { }

        public lottery_award(Entity instance) : base(instance) { }
        #endregion

    }
}

