﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class lottery_exchange_detail : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.lottery_exchange_detail";

        #region ====Properties====
        public String led_exchange_id
        {
            get { return Instance["led_exchange_id"].StringValue; }
            set { Instance["led_exchange_id"].StringValue = value; }
        }

        public String lottery_class
        {
            get { return Instance["lottery_class"].StringValue; }
            set { Instance["lottery_class"].StringValue = value; }
        }

        public String lottery_type
        {
            get { return Instance["lottery_type"].StringValue; }
            set { Instance["lottery_type"].StringValue = value; }
        }

        public String lottery_series
        {
            get { return Instance["lottery_series"].StringValue; }
            set { Instance["lottery_series"].StringValue = value; }
        }

        public Int32 led_no
        {
            get { return Instance["led_no"].IntegerValue; }
            set { Instance["led_no"].IntegerValue = value; }
        }

        public Decimal led_exchange_num
        {
            get { return Instance["led_exchange_num"].DecimalValue; }
            set { Instance["led_exchange_num"].DecimalValue = value; }
        }

        public Decimal led_exchange_amt
        {
            get { return Instance["led_exchange_amt"].DecimalValue; }
            set { Instance["led_exchange_amt"].DecimalValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public lottery_exchange_detail() : base() { }

        public lottery_exchange_detail(Entity instance) : base(instance) { }
        #endregion

    }
}
