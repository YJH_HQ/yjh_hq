﻿using System;
using dps.Common.Data;
using System.Collections.Generic;

namespace YJH.Entities
{
    public class T_User : dps.Data.Mapper.EntityBase
    {

        public const string EntityModelID = "YJH.T_User";

        #region ====Properties====
        public String UserName
        {
            get { return Instance["UserName"].StringValue; }
            set { Instance["UserName"].StringValue = value; }
        }

        public String Account
        {
            get { return Instance["Account"].StringValue; }
            set { Instance["Account"].StringValue = value; }
        }

        public String Password
        {
            get { return Instance["Password"].StringValue; }
            set { Instance["Password"].StringValue = value; }
        }

        public String store_id
        {
            get { return Instance["store_id"].StringValue; }
            set { Instance["store_id"].StringValue = value; }
        }

        public override string ModelID
        { get { return EntityModelID; } }

        #endregion

        #region ====Ctor====
        public T_User() : base() { }

        public T_User(Entity instance) : base(instance) { }
        #endregion

    }
}

