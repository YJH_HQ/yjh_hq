﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
   public class ad_car_infoService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "ad_car_infoService";
        #endregion

        public static void Save(YJH.Entities.ad_car_info car)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", car.Instance);
        }

        public static void Delete(System.Guid CarID)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", CarID);
        }

        /// <summary>
        /// 获取员工列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<YJH.Entities.hr_emploee> Gethr_emploee()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_emploee");
            List<YJH.Entities.hr_emploee> lists = new List<YJH.Entities.hr_emploee>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_emploee(item));
            }
            return lists;
        }
    }
}
