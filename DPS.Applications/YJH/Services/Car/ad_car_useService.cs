﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
   public class ad_car_useService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "ad_car_useService";
        #endregion

        public static void Save(YJH.Entities.ad_car_use use)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", use.Instance);
        }

        public static void Delete(System.Guid useID)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", useID);
        }

        /// <summary>
        /// 获取员工列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<YJH.Entities.hr_emploee> Gethr_emploee()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_emploee");
            List<YJH.Entities.hr_emploee> lists = new List<YJH.Entities.hr_emploee>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_emploee(item));
            }
            return lists;
        }

        /// <summary>
        /// 获取车牌号码列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<YJH.Entities.ad_car_info> Getad_car_info()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getad_car_info");
            List<YJH.Entities.ad_car_info> lists = new List<YJH.Entities.ad_car_info>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.ad_car_info(item));
            }
            return lists;
        }
    }
}
