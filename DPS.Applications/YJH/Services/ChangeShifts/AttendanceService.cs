﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class AttendanceService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "AttendanceService";
        #endregion

        #region ==== 方法 ====
        /// <summary>
        /// 获取列表
        /// </summary>     
        public static System.Data.DataTable GetattendanceByemp(string emp,DateTime st,DateTime et)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetattendanceByemp", emp,st,et);
        }

        public static System.Data.DataTable Gethr_vacation_stByemp(string emp)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_vacation_stByemp", emp);
        }


        public static System.Data.DataTable Gethr_schedule_stByemp(string emp, int year, int month)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_schedule_stByemp", emp, year, month);
        }
     
        #endregion
    }
}
