﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace sys.Services
{
    public class RoleService
    {
        private static string appID = "sys";
        private static string serviceID = "RoleService";

        public static void Save(sys.Entities.Roles entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static ObservableCollection<sys.Entities.Roles> Search(bool IsAll)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Search", IsAll);
            ObservableCollection<sys.Entities.Roles> list = new ObservableCollection<sys.Entities.Roles>();
            res.ForEach(t => list.Add(new sys.Entities.Roles(t)));
            return list;
        }

        /// <summary>
        /// 获取角色的所有功能
        /// </summary>
        /// <param name="ids">角色ID集合</param>
        /// <returns></returns>
        public static List<sys.Entities.Funcs> GetFuncByRoleIDs(List<System.Guid> ids)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetFuncByRoleIDs", ids);
            List<sys.Entities.Funcs> list = new List<sys.Entities.Funcs>();
            res.ForEach(t => list.Add(new sys.Entities.Funcs(t)));
            return list;
        }
    }
}