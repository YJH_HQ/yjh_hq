﻿using System.Collections.Generic;

namespace sys.Services
{
    public class LoginService
    {
        private static string appID = "sys";
        private static string serviceID = "LoginService";

        /// <summary>
        /// 登录成功后，验证账户和选择的组织机构是否匹配（是否匹配验证规则根据组织机构下是否存在该账户）
        /// </summary>
        /// <param name="userName">要验证的用户名</param>
        /// <param name="orgType">组织机构：业务组织机构、行政组织机构、财务组织机构、配送组织机构、采购组织机构</param>
        public static List<sys.Entities.OrgUnit> CheckAccountWithOrg(string userName, int orgType)
        {
            var result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Instance.Invoke(appID, serviceID, "CheckAccountWithOrg", new object[] { userName, orgType });
            List<sys.Entities.OrgUnit> list = new List<Entities.OrgUnit>();
            result.ForEach(t => list.Add(new Entities.OrgUnit(t)));

            return list;
        }
    }
}