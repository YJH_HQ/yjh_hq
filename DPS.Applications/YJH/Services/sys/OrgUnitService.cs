﻿using System;
using System.Collections.Generic;

namespace sys.Services
{
    public class OrgUnitService
    {
        private static string appID = "sys";
        private static string serviceID = "OrgUnitService";

        /// <summary>
        /// 获取公司组织机构树（管理员权限）
        /// </summary>
        /// <returns></returns>
        public static IList<sys.Entities.OrgUnit> GetOrgUnitTreeList(Guid id)
        {
            var result = dps.Common.SysService.Invoke(appID, serviceID, "GetOrgUnitTreeList", id);
            if (result == null) return null;
            return ((List<dps.Common.Data.Entity>)result).ToEntityList<sys.Entities.OrgUnit>();
        }

        /// <summary>
        /// 获取公司组织机构树（非管理员权限）
        /// </summary>
        /// <param name="id"></param>
        /// <param name="orgType"></param>
        /// <returns></returns>
        public static IList<sys.Entities.OrgUnit> GetOrganization(Guid id, string orgType)
        {
            var result = dps.Common.SysService.Invoke(appID, serviceID, "GetOrganization", id, orgType);
            if (result == null) return null;
            return ((List<dps.Common.Data.Entity>)result).ToEntityList<sys.Entities.OrgUnit>();
        }

        /// <summary>
        /// 获取公司组织机构树
        /// </summary>
        /// <returns></returns>
        public static IList<sys.Entities.OrgUnit> GetCompanyTreeList()
        {
            var result = dps.Common.SysService.Invoke(appID, serviceID, "GetCompanyTreeList");

            if (result == null) return null;
            return ((List<dps.Common.Data.Entity>)result).ToEntityList<sys.Entities.OrgUnit>();
        }

        /// <summary>
        /// 获取当前OU表中的最大流水号
        /// </summary>
        /// <returns></returns>
        public static int GetMaxSerialNumber()
        {
            return (int)dps.Common.SysService.Invoke(appID, serviceID, "GetMaxSerialNumber");
        }

        public static List<sys.Entities.Funcs> GetFuncByParentName(string funcURL)
        {
            var result = dps.Common.SysService.Invoke(appID, serviceID, "GetFuncByParentName", funcURL);

            if (result == null) return null;
            var tempList = (List<dps.Common.Data.Entity>)result;
            List<sys.Entities.Funcs> list = new List<Entities.Funcs>();
            tempList.ForEach(t => list.Add(new Entities.Funcs(t)));

            return list;
        }

        /// <summary>
        /// 获取当前登录人公司的根节点
        /// </summary>
        /// <returns></returns>
        public static dps.Data.Mapper.EntityList<sys.Entities.OrgUnit> GetNodesById(Guid currentId)
        {
            return ((List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetNodesById", currentId)).ToEntityList<sys.Entities.OrgUnit>();
        }

        /// <summary>
        /// 获取没有建立帐号的员工列表
        /// </summary>
        /// <returns></returns>
        public static List<YJH.Entities.hr_emploee> Gethr_emploeeList(System.Guid workID)
        {
            var result = dps.Common.SysService.Invoke(appID, serviceID, "Gethr_emploeeList", workID);

            if (result == null) return null;
            var tempList = (List<dps.Common.Data.Entity>)result;
            List<YJH.Entities.hr_emploee> list = new List<YJH.Entities.hr_emploee>();
            tempList.ForEach(t => list.Add(new YJH.Entities.hr_emploee(t)));

            return list;
        }

        /// <summary>
        /// 标注员工已建立帐户
        /// </summary>
        /// <param name="empID"></param>
        public static void SaveHr_emploee(System.Guid empID)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveHr_emploee",empID);
        }
    }
}
