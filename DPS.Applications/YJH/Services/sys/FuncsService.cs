﻿using System;
using System.Collections.Generic;

namespace sys.Services
{
    /// <summary>
    /// 功能
    /// </summary>
    public class FuncsService
    {
        private static string appID = "sys";
        private static string serviceID = "FuncsService";

        public static void Save(sys.Entities.Funcs entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static List<sys.Entities.Funcs> Search(string Name, int funcType)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Search", Name, funcType);
            List<sys.Entities.Funcs> list = new List<sys.Entities.Funcs>();
            res.ForEach(t => list.Add(new sys.Entities.Funcs(t)));
            return list;
        }

        /// <summary>
        /// 根据用户OU.ID获取该用户的Func权限
        /// </summary>
        /// <param name="ouID"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<sys.Entities.UserFunc> GetFuncByOrgUnitID(Guid ouID)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetFuncByOrgUnitID", ouID);
            List<sys.Entities.UserFunc> list = new List<sys.Entities.UserFunc>();
            res.ForEach(t => list.Add(new sys.Entities.UserFunc(t)));
            return list;
        }

        /// <summary>
        /// 根据用户OU.ID获取该用户的View权限
        /// </summary>
        /// <param name="ouID"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<sys.Entities.UserFunc> GetViewByOrgUnitID(Guid ouID)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetViewByOrgUnitID", ouID);
            List<sys.Entities.UserFunc> list = new List<sys.Entities.UserFunc>();
            res.ForEach(t => list.Add(new sys.Entities.UserFunc(t)));
            return list;
        }

        /// <summary>
        /// 删除员工ou时删除对应的权限
        /// </summary>
        /// <param name="ouids"></param>
        public static void deleteFuncByEmpOuIDs(System.Collections.Generic.List<System.Guid> ouids)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "deleteFuncByEmpOuIDs", ouids);
        }

        public static List<sys.Entities.Funcs> GetFuncsTreeList()
        {
            var result = (dps.Common.Data.EntityList)dps.Common.SysService.Invoke(appID, serviceID, "GetFuncsTreeList");
            var temp = result.ToEntityList<sys.Entities.Funcs>();

            List<sys.Entities.Funcs> list = new List<Entities.Funcs>();
            foreach (var item in temp)
                list.Add(item);

            result.Clear();
            temp.Clear();

            return list;
        }

        /// <summary>
        /// 保存从文件中读取出来的权限
        /// </summary>
        /// <param name="list"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string SaveFromReadFile(List<string> list, string url)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "SaveFromReadFile", list, url);
        }

        /// <summary>
        /// 根据URL获取该界面权限的子节点（功能）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static dps.Data.Mapper.EntityList<sys.Entities.Funcs> GetSubItemsByURL(string url)
        {
            var result = (dps.Common.Data.EntityList)dps.Common.SysService.Invoke(appID, serviceID, "GetSubItemsByURL", url);
            return result.ToEntityList<sys.Entities.Funcs>();
        }
    }
}