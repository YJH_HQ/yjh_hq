﻿
namespace sys.Services
{
    public class UserService
    {
        public static string appID = "sys";
        public static string serviceID = "UserService";

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="Account">账户名</param>
        public static void ChangePassword(string Account)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "ChangePassword", Account);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="user">账户</param>
        /// <param name="newpwd">新密码</param>
        public static void ChangePassword(sys.Entities.User user, string newpwd)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "ChangePassword", user.Instance, newpwd);
        }

        /// <summary>
        ///根据账户获取User.
        /// </summary>
        public static sys.Entities.User GetUserByEmploeeAccount(string account)
        {
            dps.Common.Data.Entity entity = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetUserByEmploeeAccount", account);
            if (entity == null)
                return null;

            return new sys.Entities.User(entity);
        }

        /// <summary>
        /// 创建 User
        /// </summary>
        public static void CreateUser(string account)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "CreateUser", account);
        }

        /// <summary>
        /// 判断是否存在相同账号
        /// </summary>
        public static bool IsExistAccount(string account)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsExistAccount", account);
        }

        /// <summary>
        /// 判断是否存在相同账号
        /// </summary>
        public static bool IsExistSameAccount(sys.Entities.User user)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsExistSameAccount", user.Instance);
        }

        /// <summary>
        /// 判断是否存在相同账号
        /// </summary>
        public static bool IsExistSameAccountByEmploee(sys.Entities.Emploee emploee)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsExistSameAccountByEmploee", emploee.Instance);
        }

        // <summary>
        // 验证旧密码的一致性
        // </summary>               
        public static bool JudgeOldPwd(string oldpwd, string account)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "JudgeOldPwd", oldpwd, account);
        }
    }
}