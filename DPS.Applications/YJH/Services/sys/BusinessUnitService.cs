﻿
namespace sys.Services
{
	public class BusinessUnitService
	{
		private static string appID = "sys";
		private static string serviceID = "BusinessUnitService";

		/// <summary>
		/// 保存公司信息
		/// </summary>
		/// <returns>返回组织机构</returns>
		public static sys.Entities.OrgUnit CreateCompany(sys.Entities.BusinessUnit bu)
		{
			dps.Common.Data.Entity entity = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "CreateCompany", bu.Instance);

			return new sys.Entities.OrgUnit(entity);
		}

		public static void DeleteCompany(sys.Entities.OrgUnit ou)
		{
			dps.Common.SysService.Invoke(appID, serviceID, "DeleteCompany", ou.Instance);
		}

        public static void DeleteCompanyAndStore(sys.Entities.OrgUnit ou)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteCompanyAndStore", ou.Instance);
        }


        /// <summary>
        /// 保存店铺信息
        /// </summary>
        /// <returns>返回组织机构</returns>
        public static void SaveStore(YJH.Entities.ba_store store)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveStore", store.Instance);
        }

        /// <summary>
        /// 修改店铺信息
        /// </summary>
        /// <returns>返回组织机构</returns>
        public static void UpdataStore(System.Guid BUID,string BUName)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "UpdataStore", BUID, BUName);
        }

        /// <summary>
        /// 根据公司ID返回门店实体
        /// </summary>
        /// <param name="BUID"></param>
        /// <returns></returns>
        public static YJH.Entities.ba_store SelectStore(System.Guid BUID)
        {
            return new YJH.Entities.ba_store((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "SelectStore",BUID));
        }

        public static sys.Entities.BusinessUnit GetBuByID(System.Guid BUID)
        {
            return new sys.Entities.BusinessUnit((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetBuByID", BUID));
        }
	}
}