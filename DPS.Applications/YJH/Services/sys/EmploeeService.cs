﻿using System;

namespace sys.Services
{
    public class EmploeeService
    {
        private static string appID = "sys";
        private static string serviceID = "EmploeeService";

        /// <summary>
        /// 保存公司信息
        /// </summary>
        /// <returns>返回组织机构</returns>
        public static sys.Entities.OrgUnit CreateEmploee(sys.Entities.Person person, Guid buID, Guid parentID, string account)
        {
            dps.Common.Data.Entity entity = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "CreateEmploee", person.Instance, buID, parentID, account);

            return new sys.Entities.OrgUnit(entity);
        }

        public static void DeleteEmploee(sys.Entities.OrgUnit ou)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteEmploee", ou.Instance);
        }
    }
}