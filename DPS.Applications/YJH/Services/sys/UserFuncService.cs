﻿using System;
using System.Collections.Generic;

namespace sys.Services
{
    public class UserFuncService
    {
        private static string appID = "sys";
        private static string serviceID = "UserFuncService";

        public static void Save(Guid OrgUnitID, List<Guid> FuncIDs)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", OrgUnitID, FuncIDs);
        }

        /// <summary>
        /// 根据当前用户ouID获取该用户的功能权限
        /// </summary>
        /// <param name="funcID">功能权限ID，ID=NULL则获取最顶级权限节点</param>
        /// <returns></returns>
        public static List<sys.Entities.UserFunc> GetCurrentUserFuncs(Guid funcID)
        {
            var res = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetCurrentUserFuncs", funcID);
            List<sys.Entities.UserFunc> list = new List<sys.Entities.UserFunc>();
            res.ForEach(t => list.Add(new sys.Entities.UserFunc(t)));
            return list;
        }
    }
}
