﻿using System;

namespace sys.Services
{
	public class WorkGroupService
	{
		private static string appID = "sys";
		private static string serviceID = "WorkGroupService";

		/// <summary>
		/// 保存部门信息
		/// </summary>
		/// <returns>返回组织机构</returns>
		public static sys.Entities.OrgUnit CreateWorkGroup(sys.Entities.WorkGroup group, Guid parentID)
		{
			dps.Common.Data.Entity entity = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "CreateWorkGroup", group.Instance, parentID);

			return new sys.Entities.OrgUnit(entity);
		}

		public static void DeleteWorkGroup(sys.Entities.OrgUnit ou)
		{
			dps.Common.SysService.Invoke(appID, serviceID, "DeleteWorkGroup", ou.Instance);
		}

        public static sys.Entities.WorkGroup GetWorkGroupByID(Guid ID)
        {
            dps.Common.Data.Entity entity = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetWorkGroupByID", ID);

            return new sys.Entities.WorkGroup(entity);
        }
	}
}