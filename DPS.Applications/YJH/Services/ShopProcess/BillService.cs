﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
   public class BillService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "BillService";
        #endregion

        #region ==== 方法 ====

        public static void Save(YJH.Entities.bm_process_store_bill bill,string expandno)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", bill.Instance, expandno);
        }

        public static void Audit(YJH.Entities.bm_process_store_bill bill)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Audit", bill.Instance);
        }

        public static void Effecter(YJH.Entities.bm_process_store_bill bill)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Effecter", bill.Instance);
        }
        public static void Delete(System.Guid billID)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", billID);
        }

        //通过扫街人员帐号返回扫街人Emploee
        public static sys.Entities.Emploee GetEmploee(string expandemp)
        {
            return new sys.Entities.Emploee((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetEmploee", expandemp));
        }
        //查流水号
        public static string GetNO()
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetNO");
        }

        //获取扫街列表
        public static List<YJH.Entities.bm_process_store_expand> GetExpandList()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetExpandList");
            List<YJH.Entities.bm_process_store_expand> lists = new List<YJH.Entities.bm_process_store_expand>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.bm_process_store_expand(item));
            }
            return lists;
        }
        //通过拓展单号返回扫街单
        public static YJH.Entities.bm_process_store_expand GetExpand(string expand_id)
        {
            return new YJH.Entities.bm_process_store_expand((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetExpand", expand_id));
        }

        //通过扫街单号返回扫街单
        public static YJH.Entities.bm_process_store_expand GetExpandNo(string expand_no)
        {
            return new YJH.Entities.bm_process_store_expand((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetExpandNo", expand_no));
        }
        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        //public static List<Entities.lottery_return> Search(string storeID, DateTime start, DateTime end)
        //{
        //    List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
        //    List<Entities.lottery_return> lists = new List<Entities.lottery_return>();
        //    foreach (var item in result)
        //    {
        //        lists.Add(new Entities.lottery_return(item));
        //    }
        //    return lists;
        //}

        #endregion
    }
}
