﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class bm_store_contractService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "bm_store_contractService";
        #endregion

        #region ==== 方法 ====
        public static System.Data.DataTable Search(string No, int status)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search", No, status);
        }

        public static YJH.Entities.bm_store_contract GetByID(Guid id)
        {
            dps.Common.Data.Entity e = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetByID", id);
            if (e != null)
            {
                return new YJH.Entities.bm_store_contract(e);
            }
            else
            {
                return null;
            }
        }
        public static void Save(YJH.Entities.bm_store_contract entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static void Audit(YJH.Entities.bm_store_contract entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Audit", entity.Instance);
        }

        public static void Effect(YJH.Entities.bm_store_contract entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Effect", entity.Instance);
        }

        public static void Cancel(YJH.Entities.bm_store_contract entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Cancel", entity.Instance);
        }

        public static System.Data.DataTable GetExpand()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetExpand");
        }

        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }
        #endregion
    }
}
