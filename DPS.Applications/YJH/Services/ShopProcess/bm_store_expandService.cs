﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class bm_store_expandService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "bm_store_expandService";
        #endregion

        #region ==== 方法 ====
        public static System.Data.DataTable GetEmp()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetEmp");
        }

        public static string GetMaxNo()
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetMaxNo");
        }

        public static void Save(Entities.bm_store_expand entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static System.Data.DataTable Search(string NoName,int status)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search", NoName, status);
        }
        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }

        public static void Audit(YJH.Entities.bm_store_expand entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Audit", entity.Instance);
        }
        public static void Effect(YJH.Entities.bm_store_expand entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Effect", entity.Instance);
        }

        public static YJH.Entities.bm_store_expand GetByID(Guid id)
        {
            dps.Common.Data.Entity e = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetByID", id);
            if (e != null)
            {
                return new YJH.Entities.bm_store_expand(e);
            }
            else
            {
                return null;
            }
        }

        public static void Cancel(YJH.Entities.bm_store_expand entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Cancel", entity.Instance);
        }

        public static void Savebm_store_contract(YJH.Entities.bm_store_contract entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Savebm_store_contract", entity.Instance);
        }

        public static YJH.Entities.bm_store_contract Getbm_store_contractByExID(Guid id)
        {
            dps.Common.Data.Entity e = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_store_contractByExID", id);
            if (e != null)
            {
                return new YJH.Entities.bm_store_contract(e);
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
