﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YJH.Entities;

namespace YJH.Services
{
     public class ExpandService
    {
          #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "ExpandService";
        #endregion

        #region ==== 方法 ====

        public static void Save(YJH.Entities.bm_process_store_expand expand)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", expand.Instance);
        }

        public static void Delete(System.Guid ExpandID)
        {
            dps.Common.SysService.Invoke(appID,serviceID,"Delete",ExpandID);
        }

         //通过扫街人员帐号返回扫街人Emploee
        public static sys.Entities.Emploee GetEmploee(string expandemp)
        {
            return new sys.Entities.Emploee((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetEmploee", expandemp));
        }
         //查流水号
        public static string GetNO()
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetNO");
        }

        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        //public static List<Entities.lottery_return> Search(string storeID, DateTime start, DateTime end)
        //{
        //    List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
        //    List<Entities.lottery_return> lists = new List<Entities.lottery_return>();
        //    foreach (var item in result)
        //    {
        //        lists.Add(new Entities.lottery_return(item));
        //    }
        //    return lists;
        //}

        #endregion
    }
}
