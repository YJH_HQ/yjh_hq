﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class assetsendService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "assetsendService";
        #endregion

        #region ==== 方法 ====
        public static string Getsend_id(DateTime dt)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "Getsend_id", dt);
        }

        public static void Save(Entities.ad_asset_send entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static System.Data.DataTable Search(string name)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search", name);
        }

        public static YJH.Entities.ad_asset_send GetByID(Guid id)
        {
            return new YJH.Entities.ad_asset_send((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetByID", id));
        }

        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }

        public static System.Data.DataTable GetEmp()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetEmp");
        }

        public static bool IsSendRece(string item_id, decimal sum)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsSendRece", item_id, sum);
        }

        public static System.Data.DataTable Searchstock(string item_id)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Searchstock", item_id);
        }
        #endregion
    }
}
