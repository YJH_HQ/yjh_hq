﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class assetreceService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "assetreceService";
        #endregion

        #region ==== 方法 ====
        public static System.Data.DataTable GetItem_id()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetItem_id");
        }
        public static string Getrece_id(DateTime dt)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "Getrece_id",dt);
        }

        public static void Save(Entities.ad_asset_rece entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static System.Data.DataTable Search(string name,int status)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search",name,status);
        }

        public static YJH.Entities.ad_asset_rece GetByID(Guid id)
        {
            return new YJH.Entities.ad_asset_rece((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetByID", id));
        }

        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }

        public static void Audit(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Audit", id);
        }
        #endregion
    }
}
