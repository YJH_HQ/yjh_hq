﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YJH.Entities;

namespace YJH.Services
{
    public class assetService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "assetService";
        #endregion

        #region ==== 方法 ====
        public static List<ad_asset_class> Getassetclass()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getassetclass");
            List<ad_asset_class> lists = new List<ad_asset_class>();
            foreach (var item in result)
            {
                lists.Add(new ad_asset_class(item));
            }
            return lists;
        }

        public static YJH.Entities.ad_asset_class GetassetclassByID(Guid id)
        {
            return new YJH.Entities.ad_asset_class((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetassetclassByID", id));
        }

        public static void DeleteBa_Class(Entities.ad_asset_class entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteBa_Class", entity.Instance);
        }

        public static bool IsHasClass_id(Entities.ad_asset_class entity)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsHasClass_id", entity.Instance);
        }

        public static void Saveassetclass(Entities.ad_asset_class entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Saveassetclass", entity.Instance);
        }

        public static System.Data.DataTable asset_itemSearch(string name,int status)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "asset_itemSearch",name,status);
        }

        public static System.Data.DataTable GetEnableassetclass()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetEnableassetclass");
        }

        public static void Saveassetitem(Entities.ad_asset_item entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Saveassetitem", entity.Instance);
        }

        public static string GetClassNameByClassid(string class_id)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetClassNameByClassid", class_id);
        }

        public static YJH.Entities.ad_asset_item GetassetitemByID(Guid id)
        {
            return new YJH.Entities.ad_asset_item((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetassetitemByID", id));
        }

        public static void DeleteassetitemByID(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteassetitemByID", id);
        }

        public static System.Data.DataTable GetAssetManger(string name,int status)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetAssetManger",name,status);
        }

        public static YJH.Entities.ad_asset_manage GetAssetMangerByID(Guid id)
        {
            return new YJH.Entities.ad_asset_manage((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetAssetMangerByID", id));
        }

        public static void SaveAssetManger(Entities.ad_asset_manage entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveAssetManger", entity.Instance);
        }

        public static System.Data.DataTable GetWorkGroup()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetWorkGroup");
        }

        public static System.Data.DataTable Getspec(string classid)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Getspec",classid);
        }
        #endregion
    }
}
