﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class LotteryBalanceService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "LotteryBalanceService";
        #endregion

        /// <summary>
        /// 获取列表
        /// </summary>     
        public static System.Data.DataTable Search(DateTime start, DateTime end, string store)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "SearchLotteryBalance", start, end, store);
        }

        /// <summary>
        /// 获取门店
        /// </summary>     
        public static System.Data.DataTable GetStores()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
        }
    }
}
