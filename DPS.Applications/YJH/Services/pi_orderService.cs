﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class pi_orderService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "pi_orderService";
        #endregion

        public static string GetOrderNo(string storeID)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetOrderNo", storeID);
        }

        public static Entities.pi_order_head Audit(Guid id, string name)
        {
            return new Entities.pi_order_head((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Audit", id, name));
        }

        public static Entities.pi_order_head Send(Guid id, string name)
        {
            return new Entities.pi_order_head((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Send", id, name));
        }

        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }

        public static void Save(List<Entities.pi_order_detail> details, Entities.pi_order_head entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", details.ToEntityList<Entities.pi_order_detail>(), entity.Instance);
        }

        public static void SaveEdit(List<Entities.pi_order_detail> details, Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveEdit", details.ToEntityList<Entities.pi_order_detail>(), id);
        }

        public static System.Data.DataTable Getpi_order_detail(string No)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Getpi_order_detail", No);
        }

        public static System.Data.DataTable Getpi_order_head(string storeid, System.DateTime st, System.DateTime et)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Getpi_order_head", storeid, st, et);
        }
    }
}
