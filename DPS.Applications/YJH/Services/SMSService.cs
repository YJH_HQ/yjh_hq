﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class SMSService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "SMSService";
        #endregion

        #region ==== 方法 ====
        private static bool SendMessageCore(string url)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "SendMessageCore", url);
        }

        public static void SendMessage(string[] phones, string msg)
        {
           dps.Common.SysService.Invoke(appID, serviceID, "SendMessage", phones,msg);
        }

        public static bool SendMessage(string phone, string msg)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "SendMessage", phone, msg);
        }
        #endregion

    }


}
