﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YJH.Entities;

namespace YJH.Services
{
   public class StoreMngService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "StoreMngService";
        #endregion

        #region ==== 方法 ====

        public static void Save(YJH.Entities.ba_store store,string expand_id,string expand_id_old,int i)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", store.Instance, expand_id, expand_id_old,i);
        }

        public static void Audit(YJH.Entities.ba_store store)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Audit", store.Instance);
        }

        public static YJH.Entities.bm_process_store_bill GetBill(string store_id)
        {
            dps.Common.Data.Entity e = (dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetBill", store_id);
            if (e != null)
            {
                return new YJH.Entities.bm_process_store_bill(e);
            }
            else
            {
                return null;
            }
        }
       /// <summary>
       /// 没有返回True
       /// </summary>
       /// <param name="store_id"></param>
       /// <returns></returns>
        public static bool Store_idIsNull(string store_id)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "Store_idIsNull", store_id);
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="entity"></param>
        public static void SendData(YJH.Entities.ba_store store,Guid id)
        {
           dps.Common.SysService.Invoke(appID, serviceID, "SendData", store.Instance,id);
        }


        public static YJH.Entities.ba_store Cancel(YJH.Entities.ba_store store)
        {
           return new YJH.Entities.ba_store((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Cancel", store.Instance));
        }
        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        //public static List<Entities.lottery_return> Search(string storeID, DateTime start, DateTime end)
        //{
        //    List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
        //    List<Entities.lottery_return> lists = new List<Entities.lottery_return>();
        //    foreach (var item in result)
        //    {
        //        lists.Add(new Entities.lottery_return(item));
        //    }
        //    return lists;
        //}
        public static void UpdateStore(string store,string vnumber)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "UpdateStore", store, vnumber);
        }
        public static void UpdateAllStore(string vnumber)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "UpdateAllStore",vnumber);
        }
        public static List<ba_store> GetStore()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetStore");
            List<ba_store> lists = new List<ba_store>();
            foreach (var item in result)
            {
                lists.Add(new ba_store(item));
            }
            return lists;
        }

        public static void SaveUser(YJH.Entities.user_store userstore)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveUser", userstore.Instance);
        }

        public static System.Data.DataTable GetUser(string store_id)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetUser", store_id);
        }

        public static YJH.Entities.user_store GetUserByID(Guid id)
        {
            return new YJH.Entities.user_store((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetUserByID", id));
        }

        public static void EditUser(YJH.Entities.user_store userstore)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "EditUser", userstore.Instance);
        }

        public static void DeleteUser(Guid userid, string store_id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteUser", userid, store_id);
        }

        public static void BindUser(Guid userid, string store_id, Guid job, bool enabled)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "BindUser", userid, store_id, job, enabled);
        }

        public static bool IsExitUser(Guid userid, string store_id)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsExitUser", userid, store_id);
        }

        public static bool IsExitEmp(Guid userid)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsExitEmp", userid);
        }

        public static string GetStoreByEmp(Guid userid)
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "GetStoreByEmp", userid);
        }
        public static bool IsHasDZ(string sid,Guid userid)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsHasDZ", sid, userid);
        }
        public static void DeleteAllUser(Guid userid)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteAllUser", userid);
        }

        public static void ChangeJob(Guid userid, string store_id, System.Guid job)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "ChangeJob", userid, store_id, job);
        }

        public static void ChangeEnabled(Guid userid, string store_id, bool enabled)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "ChangeEnabled", userid, store_id, enabled);
        }

        public static System.Data.DataTable WaitingUser()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "WaitingUser");
        }

        public static void SetNewDowload()
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SetNewDowload");
        }

        public static List<ba_class> Getba_class()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getba_class");
            List<ba_class> lists = new List<ba_class>();
            foreach (var item in result)
            {
                lists.Add(new ba_class(item));
            }
            return lists;
        }

        public static YJH.Entities.ba_class Getba_classByID(Guid id)
        {
            return new YJH.Entities.ba_class((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Getba_classByID", id));
        }

        public static void SaveBa_Class(Entities.ba_class entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveBa_Class",entity.Instance);
        }

        public static void DeleteBa_Class(Entities.ba_class entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteBa_Class", entity.Instance);
        }

       //启用
        public static List<bm_process_store> Getbm_process()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_process");
            List<bm_process_store> lists = new List<bm_process_store>();
            foreach (var item in result)
            {
                lists.Add(new bm_process_store(item));
            }
            return lists;
        }
        public static List<bm_schedule_store> Getbm_schedule_store(string store_id)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_schedule_store", store_id);
            List<bm_schedule_store> lists = new List<bm_schedule_store>();
            foreach (var item in result)
            {
                lists.Add(new bm_schedule_store(item));
            }
            return lists;
        }

        public static List<bm_process_store> Getbm_processByUser(string user_id)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_processByUser", user_id);
            List<bm_process_store> lists = new List<bm_process_store>();
            foreach (var item in result)
            {
                lists.Add(new bm_process_store(item));
            }
            return lists;
        }
        public static YJH.Entities.bm_process_store Getbm_processByID(Guid id)
        {
            return new YJH.Entities.bm_process_store((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_processByID", id));
        }

        public static void Savebm_process_store(Entities.bm_process_store entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Savebm_process_store", entity.Instance);
        }

        public static void Deletebm_process_store(Entities.bm_process_store entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Deletebm_process_store", entity.Instance);
        }

        public static bool IsParentCheck(string process_id, string store_id)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsParentCheck", process_id, store_id);
        }

        public static bool IsChildCheck(string process_id, string store_id)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsChildCheck", process_id, store_id);
        }

        public static void SaveProcess(List<string> list, string store_id, string creater)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveProcess", list,store_id,creater);
        }

        public static bool IsHasProess_id(Entities.bm_process_store entity)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsHasProess_id", entity.Instance);
        }

        public static bool IsHasClass_id(Entities.ba_class entity)
        {
            return (bool)dps.Common.SysService.Invoke(appID, serviceID, "IsHasClass_id", entity.Instance);
        }

        public static void Savebm_schedule_type(List<string> list, string store_id, string creater)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Savebm_schedule_type", list, store_id, creater);
        }

        public static List<bm_schedule_type> Getbm_schedule_type(string store_id)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_schedule_type", store_id);
            List<bm_schedule_type> lists = new List<bm_schedule_type>();
            foreach (var item in result)
            {
                lists.Add(new bm_schedule_type(item));
            }
            return lists;
        }

        public static void SaveProcessemp(string store_id, string process_id, string emp_id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveProcessemp", store_id, process_id, emp_id);
        }

        public static YJH.Entities.bm_schedule_store GetscheduleByspid(string store_id, string process_id)
        {
            return new YJH.Entities.bm_schedule_store((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetscheduleByspid", store_id,process_id));
        }

        public static void SaveUserProcess(List<string> list, string user_id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveUserProcess", list, user_id);
        }

        public static List<bm_process_user> Getbm_schedule_storeByUser(string user_id)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getbm_schedule_storeByUser", user_id);
            List<bm_process_user> lists = new List<bm_process_user>();
            foreach (var item in result)
            {
                lists.Add(new bm_process_user(item));
            }
            return lists;
        }
        //获取省
        public static List<ba_district> Getprovince()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getprovince");
            List<ba_district> lists = new List<ba_district>();
            foreach (var item in result)
            {
                lists.Add(new ba_district(item));
            }
            return lists;
        }

        //根据父级ID获取子级--行政区域
        public static List<ba_district> Getchild(int uid)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Getchild", uid);
            List<ba_district> lists = new List<ba_district>();
            foreach (var item in result)
            {
                lists.Add(new ba_district(item));
            }
            return lists;
        }

        /// <summary>
        /// 获取拓展单
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetExpand_IDTable()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetExpand_IDTable");
        }

        public static System.Data.DataTable GetExpand()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetExpand");
        }

        public static System.Data.DataTable GetTest()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetTest");
        }
        #endregion
    }
}
