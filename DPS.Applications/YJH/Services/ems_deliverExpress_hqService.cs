﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YJH.Entities;

namespace YJH.Services
{
    public class ems_deliverExpress_hqService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "ems_deliverExpress_hqService";
        #endregion

        /// <summary>
        /// 获取列表
        /// </summary>     
        public static System.Data.DataTable Search(DateTime st, DateTime et, DateTime start, DateTime end, string store, string emsid,string deliver)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search", st, et, start, end, store, emsid, deliver);
        }

        public static List<T_Expresscompany> GetExpresscompany()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetExpresscompany");
            List<T_Expresscompany> lists = new List<T_Expresscompany>();
            foreach (var item in result)
            {
                lists.Add(new T_Expresscompany(item));
            }
            return lists;
        }

        public static int DeliverCount(DateTime dt)
        {
            return (int)dps.Common.SysService.Invoke(appID, serviceID, "DeliverCount", dt);
        }

        public static int DeliverMonthCount(DateTime dt)
        {
            return (int)dps.Common.SysService.Invoke(appID, serviceID, "DeliverMonthCount", dt);
        }
    }
}
