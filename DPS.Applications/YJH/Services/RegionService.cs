﻿
namespace YJH.Services
{
    public class RegionService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "RegionService";
        #endregion

        /// <summary>
        /// 获取行政区域实体集合树
        /// </summary>
        /// <returns>返回集合树</returns>
        /// <remarks>建立人：张臻 建立日期：2014/1/24 最后修改日期：2014/1/24</remarks>
        public static dps.Data.Mapper.EntityList<YJH.Entities.Region> GetRegionTreeList()
        {
            return ((dps.Common.Data.EntityList)dps.Common.SysService.Invoke(appID, serviceID, "GetRegionTreeList")).ToEntityList<YJH.Entities.Region>();
        }
    }
}
