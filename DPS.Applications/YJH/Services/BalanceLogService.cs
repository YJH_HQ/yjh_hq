﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class BalanceLogService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "BalanceLogService";
        #endregion

        /// <summary>
        /// 获取列表
        /// </summary>     
        public static System.Data.DataTable Search(string store)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "SearchLog", store);
        }

        /// <summary>
        /// 获取门店
        /// </summary>     
        public static System.Data.DataTable GetStores()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
        }
    }
}
