﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public static class ExtendMethod
    {
        /// <summary>
        /// 将集合转换成 dps.Data.Mapper.EntityList<T>
        /// </summary>
        /// <typeparam name="T">继承自 dps.Data.Mapper.EntityBase</typeparam>
        /// <param name="source">转换的集合</param>
        /// <returns></returns>
        public static dps.Data.Mapper.EntityList<T> ToEntityList<T>(this List<dps.Common.Data.Entity> source) where T : dps.Data.Mapper.EntityBase
        {
            var cov = new dps.Common.Data.EntityList();
            source.ForEach(t => cov.Add(t));
            var res = new dps.Data.Mapper.EntityList<T>(cov);
            res.AcceptChanges();
            return res;
        }

        public static dps.Data.Mapper.EntityList<T> ToEntityList<T>(this dps.Common.Data.EntityList source) where T : dps.Data.Mapper.EntityBase
        {
            var cov = new dps.Common.Data.EntityList();
            foreach (var t in source)
            {
                cov.Add(t);
            }
            var res = new dps.Data.Mapper.EntityList<T>(cov);
            res.AcceptChanges();
            return res;
        }

        public static List<dps.Common.Data.Entity> ToEntityList<T>(this List<T> source) where T : dps.Data.Mapper.EntityBase
        {
            var cov = new List<dps.Common.Data.Entity>();
            foreach (var t in source)
            {
                cov.Add(t.Instance);
            }
            return cov;
        }

        /// <summary>
        /// 去除DataTable中重复的值
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static System.Data.DataTable Distinct(this System.Data.DataTable dt)
        {
            if (dt == null)
                return null;    // throw new ArgumentNullException("传入参数不能为空");

            List<string> Names = new List<string>();
            foreach (System.Data.DataColumn item in dt.Columns)
            {
                Names.Add(item.ColumnName);
            }
            return dt.DefaultView.ToTable(true, Names.ToArray());
        }      
    }
}
