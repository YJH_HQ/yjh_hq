﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class ems_sendexpress_hqService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "ems_sendexpress_hqService";
        #endregion

        /// <summary>
        /// 获取列表
        /// </summary>     
        public static System.Data.DataTable Search(DateTime start, DateTime end, string store,string emsid)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Search", start, end, store, emsid);
        }

        public static int SendCount(DateTime dt)
        {
            return (int)dps.Common.SysService.Invoke(appID, serviceID, "SendCount", dt);
        }

        public static int SendMonthCount(DateTime dt)
        {
            return (int)dps.Common.SysService.Invoke(appID, serviceID, "SendMonthCount", dt);
        }
    }
}
