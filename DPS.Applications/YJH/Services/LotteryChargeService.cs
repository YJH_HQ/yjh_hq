﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class LotteryChargeService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "LotteryChargeService";
        #endregion

        /// <summary>
        /// 保存集合
        /// </summary>
        /// <param name="list"></param>
        public static void SaveList(List<Entities.lottery_charge> list)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveList", list.ToEntityList<Entities.lottery_charge>());
        }
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteEntity(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteEntity", id);
        }
        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        public static List<Entities.lottery_charge> Search(string storeID, DateTime start, DateTime end)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
            List<Entities.lottery_charge> lists = new List<Entities.lottery_charge>();
            foreach (var item in result)
            {
                lists.Add(new Entities.lottery_charge(item));
            }
            return lists;
        }
        /// <summary>
        /// 获取门店
        /// </summary>     
        public static List<Entities.ba_store> GetStores()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
            List<Entities.ba_store> lists = new List<Entities.ba_store>();
            foreach (var item in result)
            {
                lists.Add(new Entities.ba_store(item));
            }
            return lists;
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="entity"></param>
        public static Entities.lottery_charge AuditData(Entities.lottery_charge entity, string name)
        {
            return new Entities.lottery_charge((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "AuditData", entity.Instance, name));
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="entity"></param>
        public static Entities.lottery_charge SendData(Entities.lottery_charge entity, string name)
        {
            return new Entities.lottery_charge((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "SendData", entity.Instance, name));
        }
        /// <summary>
        /// 获取相应彩票账户数据
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static System.Data.DataTable GetAccountList(string type)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetAccountList", type);
        }
    }
}
