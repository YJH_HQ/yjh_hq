﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
    public class StorePlanService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "StorePlanService";
        #endregion

        public static List<Entities.ba_store> GetStores()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
            List<Entities.ba_store> lists = new List<Entities.ba_store>();
            foreach (var item in result)
            {
                lists.Add(new Entities.ba_store(item));
            }
            return lists;
        }

        public static void Save(YJH.Entities.bu_store_plan entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", entity.Instance);
        }

        public static System.Data.DataTable Query(string plan_id)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Query", plan_id);
        }
    }
}
