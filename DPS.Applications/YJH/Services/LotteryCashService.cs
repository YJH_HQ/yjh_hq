﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class LotteryCashService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "LotteryCashService";
        #endregion

        /// <summary>
        /// 保存集合
        /// </summary>
        /// <param name="list"></param>
        public static void SaveList(List<Entities.lottery_cash> list)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveList", list.ToEntityList<Entities.lottery_cash>());
        }
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteEntity(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteEntity", id);
        }
        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        public static List<Entities.lottery_cash> Search(string storeID, DateTime start, DateTime end)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
            List<Entities.lottery_cash> lists = new List<Entities.lottery_cash>();
            foreach (var item in result)
            {
                lists.Add(new Entities.lottery_cash(item));
            }
            return lists;
        }
        /// <summary>
        /// 获取门店
        /// </summary>     
        public static List<Entities.ba_store> GetStores()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
            List<Entities.ba_store> lists = new List<Entities.ba_store>();
            foreach (var item in result)
            {
                lists.Add(new Entities.ba_store(item));
            }
            return lists;
        }
    }
}
