﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Services
{
   public class hr_emploeeService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "hr_emploeeService";
        #endregion

        #region ==== 方法 ====
        public static void Save(YJH.Entities.hr_emploee hremp, YJH.Entities.hr_emp_salary hrempsa)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save",hremp.Instance,hrempsa.Instance);
        }

        /// <summary>
       /// 获取公司列表
       /// </summary>
       /// <param name="uid"></param>
       /// <returns></returns>
        public static List<sys.Entities.BusinessUnit> GetBu()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetBu");
            List<sys.Entities.BusinessUnit> lists = new List<sys.Entities.BusinessUnit>();
            foreach (var item in result)
            {
                lists.Add(new sys.Entities.BusinessUnit(item));
            }
            return lists;
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<sys.Entities.OrgUnit> GetDept(System.Guid BUID)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetDept", BUID);
            List<sys.Entities.OrgUnit> lists = new List<sys.Entities.OrgUnit>();
            foreach (var item in result)
            {
                lists.Add(new sys.Entities.OrgUnit(item));
            }
            return lists;
        }

        /// <summary>
        /// 获取职务列表
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static List<YJH.Entities.hr_job1> Gethr_Job1()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_Job1");
            List<YJH.Entities.hr_job1> lists = new List<YJH.Entities.hr_job1>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_job1(item));
            }
            return lists;
        }

        public static System.Data.DataTable Gethr_Job1Table()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_Job1Table");
        }

        /// <summary>
        /// 获取门店
        /// </summary>     
        public static System.Data.DataTable GetStores()
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
        }

       /// <summary>
       /// 获取组织树的顶节点
       /// </summary>
       /// <returns></returns>
        public static sys.Entities.OrgUnit GetTreeFirstNode(System.Guid BUID)
        {
            return new sys.Entities.OrgUnit((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetTreeFirstNode", BUID));
        }

       /// <summary>
       ///获取员工列表 
       /// </summary>
       /// <param name="DeptID"></param>
       /// <returns></returns>
        public static List<YJH.Entities.hr_emploee> Gethr_EmploeeList(System.Guid DeptID)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_EmploeeList", DeptID);
            List<YJH.Entities.hr_emploee> lists = new List<YJH.Entities.hr_emploee>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_emploee(item));
            }
            return lists;
        }
       /// <summary>
       /// 获取员工薪资
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public static YJH.Entities.hr_emp_salary Gethr_emploee_salary(System.Guid id)
       {
           return new Entities.hr_emp_salary((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_emploee_salary", id));
       }

       /// <summary>
       ///根据查询条件获取员工列表 
       /// </summary>
       /// <param name="DeptID"></param>
       /// <returns></returns>
       public static List<YJH.Entities.hr_emploee> Gethr_EmploeeList2(string condition,string info)
       {
           List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_EmploeeList2", condition,info);
           List<YJH.Entities.hr_emploee> lists = new List<YJH.Entities.hr_emploee>();
           foreach (var item in result)
           {
               lists.Add(new YJH.Entities.hr_emploee(item));
           }
           return lists;
       }

        /// <summary>
        ///获取员工调薪列表 
        /// </summary>
        /// <param name="DeptID"></param>
        /// <returns></returns>
        public static List<YJH.Entities.hr_salary_adj1> Gethr_salary_adj1List(System.Guid emploeeID)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_salary_adj1List", emploeeID);
            List<YJH.Entities.hr_salary_adj1> lists = new List<YJH.Entities.hr_salary_adj1>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_salary_adj1(item));
            }
            return lists;
        }

        /// <summary>
        ///获取员工调岗列表 
        /// </summary>
        /// <param name="DeptID"></param>
        /// <returns></returns>
        public static List<YJH.Entities.hr_job_adj1> Gethr_job_adj1List(System.Guid emploeeID)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "Gethr_job_adj1List", emploeeID);
            List<YJH.Entities.hr_job_adj1> lists = new List<YJH.Entities.hr_job_adj1>();
            foreach (var item in result)
            {
                lists.Add(new YJH.Entities.hr_job_adj1(item));
            }
            return lists;
        }

        /// <summary>
        /// 保存hr_salary_adj1集合
        /// </summary>
        /// <param name="list"></param>
        public static void Savehr_salary_adj1(List<Entities.hr_salary_adj1> list)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Savehr_salary_adj1", list.ToEntityList<Entities.hr_salary_adj1>());
        }

        /// <summary>
        /// 保存hr_job_adj1集合
        /// </summary>
        /// <param name="list"></param>
        public static void Savehr_job_adj1(List<Entities.hr_job_adj1> list)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Savehr_job_adj1", list.ToEntityList<Entities.hr_job_adj1>());
        }

        /// <summary>
        /// 根据workid返回一个对应的GetWorkOU
        /// </summary>
        /// <param name="workid"></param>
        /// <returns></returns>
        public static sys.Entities.OrgUnit GetWorkOU(System.Guid workid)
        {
            return new sys.Entities.OrgUnit((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "GetWorkOU", workid));
        }

        #endregion
    }
}
