﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class OrgUnitService
    {
        #region ==== 字段 ====
        private static string appID = "sys";
        private static string serviceID = "OrgUnitService";
        #endregion

        public static System.DateTime GetDate()
        {
            return (System.DateTime)dps.Common.SysService.Invoke(appID, serviceID, "GetDate");
        }
    }
}
