﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Services
{
    public class LotteryExchangeService
    {
        #region ==== 字段 ====
        private static string appID = "YJH";
        private static string serviceID = "LotteryExchangeService";
        #endregion

        /// <summary>
        /// 保存集合
        /// </summary>
        /// <param name="list"></param>
        public static void SaveList(List<Entities.lottery_exchange> list)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveList", list.ToEntityList<Entities.lottery_exchange>());
        }
        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteEntity(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "DeleteEntity", id);
        }
        /// <summary>
        /// 查询符合条件的数据集合
        /// </summary>
        /// <param name="storeID"></param>
        /// <returns></returns>
        public static List<Entities.lottery_exchange> Search(string storeID, DateTime start, DateTime end)
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "SearchList", storeID, start, end);
            List<Entities.lottery_exchange> lists = new List<Entities.lottery_exchange>();
            foreach (var item in result)
            {
                lists.Add(new Entities.lottery_exchange(item));
            }
            return lists;
        }
        /// <summary>
        /// 获取门店
        /// </summary>     
        public static List<Entities.ba_store> GetStores()
        {
            List<dps.Common.Data.Entity> result = (List<dps.Common.Data.Entity>)dps.Common.SysService.Invoke(appID, serviceID, "GetStores");
            List<Entities.ba_store> lists = new List<Entities.ba_store>();
            foreach (var item in result)
            {
                lists.Add(new Entities.ba_store(item));
            }
            return lists;
        }
        /// <summary>
        /// 审核数据
        /// </summary>
        /// <param name="entity"></param>
        public static Entities.lottery_exchange AuditData(Entities.lottery_exchange entity, string name)
        {
            return new Entities.lottery_exchange((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "AuditData", entity.Instance, name));
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="entity"></param>
        public static Entities.lottery_exchange SendData(Entities.lottery_exchange entity, string name)
        {
            return new Entities.lottery_exchange((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "SendData", entity.Instance, name));
        }

        public static System.Data.DataTable Getlottery_exchange_head(string storeid,System.DateTime st,System.DateTime et)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Getlottery_exchange_head", storeid,st,et);
        }

        public static System.Data.DataTable Getlottery_exchange_detail(string rece_id)
        {
            return (System.Data.DataTable)dps.Common.SysService.Invoke(appID, serviceID, "Getlottery_exchange_detail", rece_id);
        }

        public static void Save(List<Entities.lottery_exchange_detail> details, Entities.lottery_exchange_head entity)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Save", details.ToEntityList<Entities.lottery_exchange_detail>(), entity.Instance);
        }

        public static void SaveEdit(List<Entities.lottery_exchange_detail> details, Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "SaveEdit", details.ToEntityList<Entities.lottery_exchange_detail>(), id);
        }

        public static string Getleh_exchange_id()
        {
            return (string)dps.Common.SysService.Invoke(appID, serviceID, "Getleh_exchange_id");
        }

        public static Entities.lottery_exchange_head Audit(Guid id,string name)
        {
            return  new Entities.lottery_exchange_head((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Audit", id, name));
        }
        public static Entities.lottery_exchange_head Send(Guid id, string name)
        {
            return new Entities.lottery_exchange_head((dps.Common.Data.Entity)dps.Common.SysService.Invoke(appID, serviceID, "Send", id, name));
        }

        public static void Delete(Guid id)
        {
            dps.Common.SysService.Invoke(appID, serviceID, "Delete", id);
        }
    }
}
