﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Enums
{
    public enum send_status
    {
        未通讯 = 10,
        已通讯 = 20,
        通讯失败 = 30
    }
}
