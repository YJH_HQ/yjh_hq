﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Enums
{
    public enum contractstatus
    {
        合同待签 = 10,
        稽核 = 20,
        合同生效 = 30,
        合同失效 = 40
    }

}
