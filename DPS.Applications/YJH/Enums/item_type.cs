﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Enums
{
    public enum item_type
    {
        无业务类型 = 0,
        单品流通 = 1,
        单件流通 = 2,
        单类事务 = 3,
        单件事务 = 4
    }
}
