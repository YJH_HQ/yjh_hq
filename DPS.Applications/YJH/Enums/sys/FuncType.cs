﻿
namespace sys.Enums
{
    public enum FuncType
    {
        /// <summary>
        /// 模块
        /// </summary>
        Model = 10,
        
        /// <summary>
        /// 视图
        /// </summary>
        View = 20,

        /// <summary>
        /// 功能
        /// </summary>
        Funcs = 30
    }
}
