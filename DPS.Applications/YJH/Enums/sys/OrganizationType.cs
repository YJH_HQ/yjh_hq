﻿
namespace sys.Enums
{
    /// <summary>
    /// 组织机构类型
    /// </summary>
    public enum OrganizationType : int
    {
        业务组织机构 = 10,
        行政组织机构 = 20,
        财务组织机构 = 30,
        物流组织机构 = 40,
        采购组织机构 = 50
    }
}
