﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Enums
{
    public enum LotteryState
    {
        新建 = 10,
        已修改 = 20,
        已审核 = 30,
        已生效 = 40
    }
}
