﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Enums
{
    public enum expandstatus
    {
        新建 = 10,
        稽核 = 20,
        审批 = 30,
        合同已签 = 40,
        装修完成 = 50,
        计划开店 = 60,
        开业 = 70
    }
}
