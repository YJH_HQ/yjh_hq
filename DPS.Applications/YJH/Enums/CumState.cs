﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Enums
{
    public enum CumState
    {
        不可通讯 = 10,
        未通讯 = 20,
        通讯成功 = 30,
        通讯失败 = 40
    }
}
