﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Enums
{
    public enum StoreState
    {
        新建 = 10,
        审核 = 20,
        生效 = 30
    }
}
