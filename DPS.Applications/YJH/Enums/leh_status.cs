﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YJH.Enums
{
    public enum leh_status
    {
        已录入 = 10,
        已修改 = 20,
        已审核 = 30,
        已生效 = 40,
        已入库 = 50
    }
}
