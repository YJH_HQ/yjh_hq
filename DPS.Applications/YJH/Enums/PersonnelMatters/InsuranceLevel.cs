﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YJH.Enums
{
    public enum InsuranceLevel
    {
        一级 = 2700,
        二级 = 3500,
        三级 = 5000
    }

}
