﻿
namespace YJH.Enums
{
    public enum RegionScope
    {
        国家 = 10,
        区域 = 20,
        省 = 30,
        直辖市 = 35,
        城市 = 40,
        区 = 50
    }
}
